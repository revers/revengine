#version 120 // This will always get moved to the top of the code in pre-processing.

const float WORLD_RADIUS         = 250.0;
const float WORLD_RADIUS_SQUARED = 62500.0;

#ifdef _ENABLE_GL_TEXTURE_2D
centroid varying vec4 texCoord;
#endif

uniform int worldTime;
varying vec4 vertColor;

void main() {
	vec4 position = gl_ModelViewMatrix * gl_Vertex;
	
	if (gl_Color.a != 0.8) {
		// Not a cloud.
		
		float distanceSquared = position.x * position.x + position.z * position.z;
		position.y += 5*sin(distanceSquared*sin(float(worldTime)/143.0)/1000);
		float y = position.y;
		float x = position.x;
		float om = sin(distanceSquared*sin(float(worldTime)/256.0)/5000) * sin(float(worldTime)/200.0);
		position.y = x*sin(om)+y*cos(om);
		position.x = x*cos(om)-y*sin(om);
	}
	
	gl_Position = gl_ProjectionMatrix * position;

#ifdef _ENABLE_GL_TEXTURE_2D
	texCoord = gl_MultiTexCoord0;
#endif
		
	vertColor = gl_Color;
	
	gl_FogFragCoord = gl_Position.z;
}