#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec3 VertexNormal;

out vec3 Normal;
//out vec2 Depth;

uniform mat3 NormalMatrix;
uniform mat4 MVP;

void main() {
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	Normal = normalize(NormalMatrix * VertexNormal);

	// Position's z and w components correspond to the distance
	// from camera and distance of the far plane respectively
	//Depth = gl_Position.zw;
}

//---------------------------------------------------------------------
//-- Fragment
#version 400

in vec3 Normal;
//in vec2 Depth;

layout(location = 0) out vec4 FragNormal;
//layout(location = 1) out vec4 FragDepth;

void main() {
//	// Depth is stored as distance from camera / far plane distance
//	// to get value between 0 and 1
//	FragDepth = Depth.x / Depth.y;

	// Normal map simply stores X, Y and Z components of normal
	// shifted from (-1 to 1) range to (0 to 1) range
	FragNormal.xyz = (normalize(Normal).xyz / 2.0) + 0.5;
	FragNormal.a = 1.0;
}
