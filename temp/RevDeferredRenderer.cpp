/*
 * RevDeferredRenderer.cpp
 *
 *  Created on: 30 lip 2013
 *      Author: Revers
 */

#include <glm/glm.hpp>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/drawables/RevVBOQuadVT.h>
#include <rev/engine/drawables/RevVBOSphereVN.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include <rev/engine/effects/RevDebugEffect.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>

#include "RevDeferredRenderer.h"

using namespace glm;

#define GEOM_PASS_SHADER "shaders/DeferredGeomPass.glsl"
#define LIGHT_PASS_SHADER "shaders/DeferredLightPass.glsl"
#define DEPTH_TEX_SHADER "shaders/QuadDepthTexture.glsl"
#define NORMAL_TEX_SHADER "shaders/QuadNormalTexture.glsl"

namespace rev {

DeferredRenderer::~DeferredRenderer() {
	if (depthNormalFbo != 0) {
		glDeleteFramebuffers(1, &depthNormalFbo);
	}
	if (lightFbo != 0) {
		glDeleteFramebuffers(1, &lightFbo);
	}
	if (normalTex != 0) {
		glDeleteTextures(1, &normalTex);
	}
	if (depthTex != 0) {
		glDeleteTextures(1, &depthTex);
	}
	if (lightTex != 0) {
		glDeleteTextures(1, &lightTex);
	}
	if (quad) {
		delete quad;
	}
	if (sphereComponent) {
		delete sphereComponent;
	}

}

bool DeferredRenderer::init() {
	// Create the FBO
	glGenFramebuffers(1, &depthNormalFbo);
	glGenFramebuffers(1, &lightFbo);
	if (!createGeomGLSLProgram()) {
		return false;
	}
	if (!createLightGLSLProgram()) {
		return false;
	}
	if (!createDepthTexGLSLProgram()) {
		return false;
	}
	if (!createNormalTexGLSLProgram()) {
		return false;
	}
	quad = new VBOQuadVT(1.0f);

	DebugEffect* debugEffect = new rev::DebugEffect();
	debugEffect->setColor(color3(1.0f, 1.0f, 1.0f));
	IVBODrawable* drawable = new rev::VBOSphereVN(1.0, 50);//12);
	sphereComponent = new rev::Simple3DComponent(debugEffect,
			false, drawable, true);
	sphereComponent->setWireframe(true);

	inited = true;
	return true;
}

bool DeferredRenderer::init(int windowWidth, int windowHeight) {
	REV_TRACE_MSG("w = " << windowWidth << "; h = " << windowHeight);
	if (inited) {
		glDeleteTextures(1, &normalTex);
		glDeleteTextures(1, &depthTex);
		glDeleteTextures(1, &lightTex);
	} else if (!init()) {
		return false;
	}

	if (!initDepthNormalFbo(windowWidth, windowHeight)) {
		return false;
	}
	if (!initLightFbo(windowWidth, windowHeight)) {
		return false;
	}

	return true;
}

bool DeferredRenderer::initDepthNormalFbo(int windowWidth, int windowHeight) {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, depthNormalFbo);

	// Create the gbuffer textures
	glGenTextures(1, &normalTex);
	glGenTextures(1, &depthTex);

	glBindTexture(GL_TEXTURE_2D, normalTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, windowWidth, windowHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
//
//	float color[4] { 0.0, 0.0, 0.0, 1.0 };
//	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);

	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			normalTex, 0);

	// depth
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, windowWidth, windowHeight, 0,
	GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex,
			0);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		REV_ERROR_MSG("Framebuffer is not complete!");
		return false;
	}

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glAssert;

	return glGetError() == GL_NO_ERROR;
}

bool DeferredRenderer::initLightFbo(int windowWidth, int windowHeight) {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lightFbo);

	glGenTextures(1, &lightTex);

	glBindTexture(GL_TEXTURE_2D, lightTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, windowWidth, windowHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			lightTex, 0);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		REV_ERROR_MSG("Framebuffer is not complete!");
		return false;
	}

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glAssert;

	return glGetError() == GL_NO_ERROR;
}

bool DeferredRenderer::createLightGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(LIGHT_PASS_SHADER);
	if (!lightProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}
	if (!lightProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	lightProgram.use();
	lightMvpLocaction = lightProgram.getUniformLocation("MVP");
	lightInvViewProjLocation = lightProgram.getUniformLocation("InvViewProjection");
	lightDepthTexLocation = lightProgram.getUniformLocation("DepthTexture");
	lightNormalTexLocation = lightProgram.getUniformLocation("NormalTexture");
	lightLightColorLocation = lightProgram.getUniformLocation("LightColor");
	lightLightPositionLocation = lightProgram.getUniformLocation("LightPosition");
	lightLightAttenLocation = lightProgram.getUniformLocation("LightAttenuation");
	lightViewportLocation = lightProgram.getUniformLocation("ViewportSize");

//	revAssert(lightMvpLocaction != -1
//			&& lightInvViewProjLocation != -1
//			&& lightDepthTexLocation != -1
//			&& lightNormalTexLocation != -1
//			&& lightLightColorLocation != -1
//			&& lightLightPositionLocation != -1
//			&& lightLightAttenLocation != -1
//			&& lightViewportLocation != -1);
	lightProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

bool DeferredRenderer::createGeomGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(GEOM_PASS_SHADER);
	if (!geomProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	if (!geomProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	geomProgram.use();
	mvpLocation = geomProgram.getUniformLocation("MVP");
	normalMatrixLocation = geomProgram.getUniformLocation("NormalMatrix");
	revAssert(mvpLocation != -1);	// && normalMatrixLocation != -1);
	geomProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

bool DeferredRenderer::createTexGLSLProgram(GLSLProgram& prog, const char* filename) {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(filename);
	if (!prog.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	glBindAttribLocation(prog.getHandle(), 0, "VertexPosition");
	glBindAttribLocation(prog.getHandle(), 1, "VertexTexCoord");
	glBindAttribLocation(prog.getHandle(), 0, "FragColor");
	glAssert;

	if (!prog.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	return glGetError() == GL_NO_ERROR;
}

bool DeferredRenderer::createDepthTexGLSLProgram() {
	REV_TRACE_FUNCTION;
	if (!createTexGLSLProgram(depthTexProgram, DEPTH_TEX_SHADER)) {
		return false;
	}
	depthTexProgram.use();
	depthTexLocation = depthTexProgram.getUniformLocation("Tex");
	revAssert(depthTexLocation != -1);
	depthTexProgram.unuse();

	return true;
}

bool DeferredRenderer::createNormalTexGLSLProgram() {
	REV_TRACE_FUNCTION;
	if (!createTexGLSLProgram(normalTexProgram, NORMAL_TEX_SHADER)) {
		return false;
	}
	normalTexProgram.use();
	normalTexLocation = normalTexProgram.getUniformLocation("Tex");
	revAssert(normalTexLocation != -1);
	normalTexProgram.unuse();

	return true;
}

void DeferredRenderer::renderQuadWithDepthTexture() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	depthTexProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glAssert;

	depthTexProgram.setUniform(depthTexLocation, 0);
	quad->render();

	depthTexProgram.unuse();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DeferredRenderer::renderQuadWithNormalTexture() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	normalTexProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, normalTex);
	glAssert;

	normalTexProgram.setUniform(normalTexLocation, 0);
	quad->render();

	normalTexProgram.unuse();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DeferredRenderer::renderQuadWithLightTexture() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	normalTexProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lightTex);
	glAssert;

	normalTexProgram.setUniform(normalTexLocation, 0);
	quad->render();

	normalTexProgram.unuse();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DeferredRenderer::drawLightSpheres() {
	DRPointLightVect& drPointLightVect = LightingManager::ref().getDRPointLightVect();
	ICamera* camera = CameraManager::ref().getCamera();
	DebugEffect* effect = (DebugEffect*) sphereComponent->getEffect();

	for (DRPointLightComponent*& light : drPointLightVect) {
		float atten = light->getAttenuation();
		mat4 scale = glm::scale(atten, atten, atten);
		mat4 m = light->getParent()->getModelMatrix() * scale;
		effect->setColor(light->getColor());

		sphereComponent->applyEffect(m, camera->getViewMatrix(),
				camera->getProjectionMatrix(), *camera);
		sphereComponent->render();
	}

}

void DeferredRenderer::drawDepthNormalMap() {
	glBindFramebuffer(GL_FRAMEBUFFER, depthNormalFbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//geomProgram.use();

	Renderer3D& renderer = Renderer3D::ref();
	ICamera* camera = CameraManager::ref().getCamera();
	mat4 viewProjMatrix = camera->getProjectionMatrix() * camera->getViewMatrix();

	for (IVisual3DComponent*& c : renderer.getComponents()) {
		if (!renderer.isProperForRender(c)) {
			continue;
		}

		mat4 modelMat = c->getParent()->getModelMatrix();
		mat4 mv = camera->getViewMatrix() * modelMat;

		mat3 normalMatrix = mat3(vec3(modelMat[0]), vec3(modelMat[1]), vec3(modelMat[2]));

		// FIXME: move before the for loop, after fixing issue with particle effects:
		geomProgram.use();

		geomProgram.setUniform(normalMatrixLocation, normalMatrix);
		mat4 mvp = camera->getProjectionMatrix() * mv;
		geomProgram.setUniform(mvpLocation, mvp);
		c->render();
	}

	geomProgram.unuse();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderer::drawLightMap() {

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTex);
	glActiveTexture(GL_TEXTURE0);

	glAssert;
	lightProgram.use();
	lightProgram.setUniform(lightDepthTexLocation, 0);
	lightProgram.setUniform(lightNormalTexLocation, 1);

	int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	revAssert(contextIndex == 0);
	Renderer3D& renderer = Renderer3D::ref();

	ICamera* camera = CameraManager::ref().getCamera();
	ivec2 iSize = renderer.getViewportSize(contextIndex);
	vec2 size(iSize.x, iSize.y);
	lightProgram.setUniform(lightViewportLocation, size);

	// Calculate the view * projection matrix
	mat4 viewProjection = camera->getProjectionMatrix() * camera->getViewMatrix();

	// Set the inverse of the view * projection matrix to the effect
	mat4 invViewProjection = glm::inverse(viewProjection);
	lightProgram.setUniform(lightInvViewProjLocation, invViewProjection);

	glAssert;
	glBindFramebuffer(GL_FRAMEBUFFER, lightFbo);
	glClear(GL_COLOR_BUFFER_BIT);	// | GL_DEPTH_BUFFER_BIT);

	// Set render states to additive (lights will add their influences)
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glEnable(GL_CULL_FACE);
	glAssert;
	DRPointLightVect& drPointLightVect = LightingManager::ref().getDRPointLightVect();
	for (DRPointLightComponent*& light : drPointLightVect) {
		const vec3& position = light->getParent()->getPosition();

		float atten = light->getAttenuation();
		mat4 mvp = viewProjection
				* glm::translate(position)
				* glm::scale(atten, atten, atten);

		// FIXME: move before the for loop, after fixing issue with particle effects:
		lightProgram.use();

		lightProgram.setUniform(lightMvpLocaction, mvp);
		lightProgram.setUniform(lightLightColorLocation, light->getColor());
		lightProgram.setUniform(lightLightPositionLocation, position);
		lightProgram.setUniform(lightLightAttenLocation, atten);

		// Determine the distance between the light and camera
		float dist = glm::distance(camera->getPosition(), position);

		// If the camera is inside the light-sphere, invert the cull mode
		// to draw the inside of the sphere instead of the outside
		if (dist < atten) {
			glCullFace(GL_BACK);
		} else {
			glCullFace(GL_FRONT);
		}

		// Draw the point-light-sphere
		sphereComponent->render();
	}
	glAssert;
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	lightProgram.unuse();
}

void DeferredRenderer::render() {
	drawDepthNormalMap();
	drawLightMap();
}

} /* namespace rev */
