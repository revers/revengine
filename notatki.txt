dodac deferred point light
--------------------------------------------------------------------------
D:\Revers\OpenGL_MODERN\ssao\src\ca\nutty\Geometry - Torus Knot
sprawdzic czy moge naraz zrobic Deferred Lighting i SSAO. Wystarczy ze zamiast tylko depth buffer
bede przechowywal Position (x,y,z) + dept (w) w jednej teksturze, tak jak w SSAO.
--------------------------------------------------------------------------
TODO: zrobic step dla wszystkich spin boxow: 0.01 lub 0.05
--------------------------------------------------------------------------
TODO: dodac zakladke "Scene", a w niej:
- nazwa plik (Read-Only)
- background color (Color chooser) - to zapisywne w SceneConfig

--------------------------------------------------------------------------
- jak zrobic CollisionEvent? Czy trigerowanie go przy kazdej kolizji to dobry pomysl? Mogloby to przeciez 
   byc nawet kilka razy na ramke dla kilku kontaktow tych samych obiektow... a co wtedy z wydajnoscia?
   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
|- jak mialby byc rozwiazywane kolizje pomiedzy MassParticleComponent a RigidBodyComponent a NonPhysics? |
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- jak rozwiazac kolizje pomiedzy obiektami nie posiadajacymi PhysicsComponent a tymi posiadajacymi?
   (wiem, obiekty bez PhysicsComponent traktowac jako obszary zabronione [o nieskonczonej masie])
- dwa obiekty NonPhysics powinny byc odsuwane od siebie o taka sama odleglosc, chyba
   ze jeden ma ustawiana flage moveable na false (CollisionComponent.moveable).
   
  
 

--------------------------------------------------------------------------
Dodac do PointParticle:
- inverseMass, hasFiniteMass()

--------------------------------------------------------------------------
- jak zrobic staly krok czasowy dla komponentow fizycznych?
- jak "zdrutowac" komponenty fizyczne z komponentami 3D?
- jak zrobic wybieranie komponentow fizycznych w NewObjectWidget?
- PointParticles3DComponent? - komponent IVisual3DComponent ktory uzywa TPointParticleSet

--------------------------------------------------------------------------
parametry poczatkowe
(do tego okienko dialogowe: ) 
- wymiary m x n gdzie m >=4, n >= 4
- typ krzywej. Bezier, BSpline
- jak bardzo maja zachodzic na siebie poszczegolne bloki. Wartosci domyslne dla bezier/bspline

- operowanie punktami kontrolnymi we wszystkich kierunkach
- zaznaczanie wielu punktow kontrolnych na raz
- dodawanie dodatkowych punktow na wybranch krancach myszka. Rozne kolory punktow przy dodawaniu

- pomyslec jak ma wygladac edit panel
- jak bedziemy przelaczac tryby manipulowania myszka (dodawnie punkow/manipulacja punktami kontrolnymi)

- koniecznie zastanowic sie jak triggerowac okna dialogowe. Plik -> Nowy (?)

--------------------------------------------------------------------------
BUG: jak wcisne jakikolwiek przycisk to pojawiaja sie wszystkie osie
(moze zrobic jakis przycisk ktory tak robi? np U/R)
--------------------------------------------------------------------------
TODO: ITool powinno miec jakas funkcje std::getInfo czy cos zeby
mozna bylo w status bar'ze umieszczac informacje (np. ile % skalowanie)
--------------------------------------------------------------------------
ja sprawdzam pozycje punktow kotrolnych w lokalnym ukladzie zamiast w ukladzie Swiata
--------------------------------------------------------------------------
pointSpheres powinny byc widoczne w exclusive mode
--------------------------------------------------------------------------
TODO: zrobic zeby color byl w efekcie a nie w drawable?
--------------------------------------------------------------------------
TODO: zrobic aby suwaki byly posrodku (dzialaly w dwie strony). Moze dodac tez, 
zeby bylo napisana wartosc suwaka?
--------------------------------------------------------------------------
BUG: dlaczego sciezki przy skryptach sa tylko bezwzgledne?
--------------------------------------------------------------------------
CameraTransitionAnimator powinna byc ICamera typu TRANSITIONAL
--------------------------------------------------------------------------
TODO: przy zmianie propert od kamery nie powinno sygnalizowac ze scena jest nie zapisana
--------------------------------------------------------------------------
Reload script powinien usuwac intervale i timeouty
--------------------------------------------------------------------------
Czy przy open script podstawiana jest resolvenieta sciezka????
Powinna byc koniecznie resolvnieta z katalogiem z programem exe!! (a nie "prefferedPath").
--------------------------------------------------------------------------
TODO: mainscript do SceneConfig.cpp
--------------------------------------------------------------------------
http://www.qtforum.org/article/24195/qpushbutton-as-delegate-in-a-qtableview.html

http://stackoverflow.com/questions/4412796/qt-qtableview-clickable-button-in-table-row

dodac timeouts i intervals etykiety w scripting
--------------------------------------------------------------------------
sprawdzic jsLog bez zadnego wypisywania 
--------------------------------------------------------------------------

- zrobic aby file chooser z properties pamietal folder
--------------------------------------------------------------------------
IBindable->wrap() niech bedzie cache'owane
--------------------------------------------------------------------------
Przeniesc ile sie da ze ScriptingMacros do jakiej klasy ze statycznymi metodami.
--------------------------------------------------------------------------
Do SceneConfig dodac pole version
Dodac file filter do IBindable.bindFilepath()
--------------------------------------------------------------------------
BUG: przy zamykaniu aplikacji bedac w edit/new mode pojawia sie blad
--------------------------------------------------------------------------
BUG: w eclusive mode, po zmianie komponentu czy efektu, znika caly exclusive
--------------------------------------------------------------------------
chyba w ogole wywale trzymanie bindera w bindable. To binder bedzie
odpowiedzialny za monitorowanie zycia obiektu a nie na odwrot
mam przeciez eventy (objectremoved) - NAPEWNO REZYGNUJEMY Z BINDERA W BINDABLE!
- moze zastosowac cache do GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject(); ?
--------------------------------------------------------------------------
zastanowic sie jak rejestrowac drawables i effects w newObjectPanel'u.
czy tak jak teraz plus isPublic()/isRegistrable(), czy moze 
jednak zrobic jawna funkcje rejestrujaca?
--------------------------------------------------------------------------
- TODO: zamiast przyciskow "Deselect" i "Camera Lock" zrobic 4 ikony:
	a) deselect
	b) remove
	c) edit components
	d) camera lock
--------------------------------------------------------------------------
edytowanie obiektow
- co z id? przydaloby sie takie same
- co z pozycja na liscie? przydalaby sie taka sama
- gameObject->destroy() wysle mylny event, przydaloby sie to jakos zablokowac
- TODO: dobrze przetestowac DeepCopyBinder - lepiej zrobic to od razu niz potem szukac glupich bledow 
--------------------------------------------------------------------------
QMessageBox niemodalne: 
http://stackoverflow.com/questions/3211490/how-to-make-unblocking-qmessagebox
--------------------------------------------------------------------------
TODO: zrobic aby metody IBinder::refresh() byly opcjonalne.
TODO: Dodac metody IBinder::bindInfo() (opcjonalne) zamiast flagi infoOnly
--------------------------------------------------------------------------
TODO: wszystkie unique_ptr'y zamienic na moje wewnetrzne implementacje
--------------------------------------------------------------------------
zrobic aby obiekty listy mialy metode size() i obslugiwaly operator indeksowy[]
tak zeby mozna bylo przejsc petla for:

for(var i = 0; i < list.size(); i++) {
	var bindable = list[i];
	log("" + i + ") name = " + bindable.name);
}
--------------------------------------------------------------------------
- zrobic aby w panelu Properties byly odswiezane wartosci co kazda klatke
--------------------------------------------------------------------------
- w IBinder dodac oddzielne metody bindInfo() (zaimplementowane jako puste) zamiast jako czwarty parametr "infoOnly".
--------------------------------------------------------------------------
- obiekt weak pointer tylko jesli zostal utworzony za pomoca konstruktora a nie Wrap!!-
- byc moze oddzielne makra SCRIPTABLE i BINDABLE_FACTORY (?)
--------------------------------------------------------------------------
D:\Revers\NetBeansCppProjects\cproxyv8\cproxyv8-class.h
objectTemplate_->SetAccessor(String::New(propertyName), getterAccessor, setterAccessor, data);
data?
void ProxyClass<T>::Expose( TProperty T::*property, char* propertyName,
                            AccessorProperty* accessor, bool getter, bool setter)
{
  ASSERT(propertyName);
  ASSERT(setter || getter);

	AccessorGetter getterAccessor = getter ? Accessor_GetProperty : 0;
	AccessorSetter setterAccessor = setter ? Accessor_SetProperty : 0;
  Handle<Value> data = External::New(accessor);

	objectTemplate_->SetAccessor(String::New(propertyName), getterAccessor, setterAccessor, data);
}

-------------------------------------------------------------------------
std::function<...> getJSFunctionFromScript(...) {

}
-------------------------------------------------------------------------
Przy zapisywaniu plikow powinien znikac properties panel
(a raczej powinien on znikac przy unbindObject())
-------------------------------------------------------------------------
jak poinformowac ze niektore bindable ktore stworzylismy za pomoca fabryki
nie mozemy skasowac za pomoca delete?
-------------------------------------------------------------------------
zrobic w error streamie setOutputToFile(false)
-------------------------------------------------------------------------
moze w VBO obiektach zamiast zmieniac konekst i tworzyc obiekty VAO to oznaczac tylko 
do przebudowania. a moze nie potrzeba w ogole przebudowwywac? bo zapamietany jest bufer?

sprawdzic czy glBufferData() gdy juz byl bufor to czy go usuwa i tworzy nowy
-------------------------------------------------------------------------
ZAKLADAMY ZE LICZBA KONTEKSTOW SIE NIE ZMIENIA OD URUCHOMIENIA APLIKACJI.
- Wywalic listenery. w takim przypadku beda niepotrzebne.

-------------------------------------------------------------------------
http://graphics.cs.williams.edu/data/meshes.xml

http://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection

#include <GL/glew.h>
#include <GL/gl.h>

	static GLuint getCurrentGLContext() {
#ifdef _WIN32
        return (GLuint) wglGetCurrentContext();
#else //!_WIN32
        return (GLuint) glXGetCurrentContext();
		#endif
	}
-------------------------------------------------------------------------------	
http://stackoverflow.com/questions/8496728/multiple-qglwidgets-with-a-single-opengl-context-in-c	
-------------------------------------------------------------------------------
http://stackoverflow.com/questions/5812961/sharing-a-vbo-across-multiple-qwidgets-in-jogl

Although I'm not using JOGL, I am doing a similar thing here and here. The basic 
idea is that you create a hidden QGLWidget, make it current and compile all your shaders,
 then pass it as the shareWidget to your child viewports. Whenever you want to upload 
 geometry make the hidden QGLWidget current and do your glBufferData calls - 
 the data becomes available to the other viewport contexts.
-------------------------------------------------------------------------------	

folder core. przeniesc tam w ogole rzeczy z folderu root	
#include <map>

	class GLContextManager {
		std::map<GLuint, int> contextMap;
		std::vector<std::function<void(int)>> contextRegistrationCallbacks;
		int currentContextIndex = 0;
		
		void setMainContext(...);
		void makeMainContextCurrent(...);
		
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		koniecznie flaga kompilacji (define) ustawiajaca ilosc obslugiwanych kontekstow
		SPRAWDZIC JAK KOSZTOWNE JEST PRZELACZANIE KONTEKSTU. Czy jak bede robil IVBODrawable::generate() dla 3 kontekstow to czy nie bedzie to w ch kosztowne?
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		void makeContextCurrent(int index);
		
		void registerCurrentContext() {
			registerContext(
		}
		
		void registerContext(GLuint ctx) {
			int index = (int) contextMap.size();
			contextMap[ctx] = index;
			for (auto& callback : contextRegistrationCallbacks) {
			}
		}
		
		void unregisterContext(GLuint ctx);
		void setActiveContextCurrent() {
			// moze jakies assercje czy istnieje
			currentContextIndex = contextMap[getCurrentContextHandle()];
		}
	    /**
		 * Returns GL context index. First registered context has 0 index, second has 1, etc.
		 */
		int getContextIndex() {
			currentContextIndex;
		}
		
		GLuint getCurrentContextHandle() {
			#ifdef _WIN32
					return (GLuint) wglGetCurrentContext();
			#else //!_WIN32
					return (GLuint) glXGetCurrentContext();
			#endif
		}
		
	
		void registerGLContextSensitive(IGLContextSensitive* sensitive);
	}
-------------------------------------------------------------------------------
SPRAWDZIC KONIECZNIE CZY KAZDY WIDGET MA INNY CONTEXT JAK KOZYSTA Z DZIELONEGO!

- w resize(w,h) z silnika niech zawsze bedzie sprawdzane GLContextManager::getCurrentContextHandle();

- widget qt w metodach initializeGL() niech maja GLContextManager::registerCurrentContext(). 
 Przy okazji niech ustawiaja swoje lokalne pole m_Ctx na swoj context.
 w destruktorach natomiast(!), GLContextManager::registerCurrentContext(m_Ctx) ;

- w widgetach gl od qt w repaint() niech zawsze na poczatku bedzie wolane  	GLContextManager::setActiveContextCurrent().

- w GameEditorWindow::updateEngine() odpalac w wybranej kolejnosci repaitny

//----------------------------------------------------------------------------------------
// zrobic flage REV_ENGINE_MULTIPLE_CONTEXTS
// i wszedzie ifdef
//----------------------------------------------------------------------------------------	

class IGLContextListener {
protected:
	IGLContextListener() {
		GLContextManager::ref().registerGLContextIGLContextListener(this);
	}
	
	virtual void contextCreated(int index) = 0;
	virtual void contextDestroyed(int index) = 0;
}

//----------------------------------------------------------------------------------------	
na samym poczatku tworzymy 3 GLWidget'y i sterujemy tylko ich widocznoscia

-------------------------------------------------------------------------

QWidget *window = new QWidget;
     QPushButton *button1 = new QPushButton("One");
     QPushButton *button2 = new QPushButton("Two");
     QPushButton *button3 = new QPushButton("Three");
     QPushButton *button4 = new QPushButton("Four");
     QPushButton *button5 = new QPushButton("Five");

     QHBoxLayout *layout = new QHBoxLayout;
     layout->addWidget(button1);
     layout->addWidget(button2);
     layout->addWidget(button3);
     layout->addWidget(button4);
     layout->addWidget(button5);

     window->setLayout(layout);
	 
	 delete window->layout();
	 
	 QGridLayout *gridLayout = new QGridLayout;
     gridLayout->addWidget(button1, 0, 0);
     gridLayout->addWidget(button2, 0, 1);
     gridLayout->addWidget(button3, 1, 0, 1, 2);
     gridLayout->addWidget(button4, 2, 0);
     gridLayout->addWidget(button5, 2, 1);

     window->setLayout(gridLayout);
	 
     window->show()
-------------------------------------------------------------------------
QSplitter *window = new QSplitter;
     QPushButton *button1 = new QPushButton("One");
     QPushButton *button2 = new QPushButton("Two");
     QPushButton *button3 = new QPushButton("Three");
     QPushButton *button4 = new QPushButton("Four");
     QPushButton *button5 = new QPushButton("Five");

	 window->addWidget(button1);
	 
	 QSplitter* rightPanel = new QSplitter(Qt::Vertical);
	 rightPanel->addWidget(button2);
	 rightPanel->addWidget(button3);
	 
	 window->addWidget(rightPanel);

dodac show all bounding spheres

vec3 v[24] {
         /* 0    1    2    3  */
         // 0,   1,   2,   3  // NEAR
            ntl, ntr, nbl, nbr,

         /* 4    5    6    7  */
         // 4,   5,   6,   7  // FAR
            ftl, ftr, fbl, fbr,

         /* 8    9    10   11  */
         // 0,   2,   4,   6  // LEFT
            ntl, nbl, ftl, fbl,

         /* 12   13   14   15  */
         // 1,   3,   5,   7  // RIGHT
            ntr, nbr, ftr, fbr,

         /* 16   17   18   19  */
         // 0,   1,   4,   5  // TOP
            ntl, ntr, ftl, ftr,

         /* 20   21   22   23  */
         // 2,   3,   6,   7  // BOTTOM
            nbl, nbr, fbl, fbr
    };



-- zrobic VBOFrustum ktore ma opcje sold/frame. mozna dynamicznie zmieniac czy 
sklada sie z trojkatow czy z linii. podajemy albo 8 punktow albo parametry dla glm::perspective

-- multiple viewports

----------------------------------------------------
- zmienic IDrawable na IVBODrawable
- dodac getPolygonCount() do IVBODrawable

- dodac IBindable* parent do IBindable. Zrobic tak ze jak zrobimy unbind na IBindable to tylko ten bindable i jego dzieci 
zostaja "odwiazane"

----------------------------------------------------

STATUS.

dodac zakladke Info?
- Liczba wszystkich obiektow na scienie
- liczba wszystkich polygonow na scenie

----------------------------------------------------

Visual3D component nie powinien zaiwerac
geteffect i getdrawable bo przeciez potencjalnie moze zawierac ich wiele

----------------------------------------------------

zrobic w pasku statusow qt game editor window:
- Total Game Objects
- Total Polygons Rendered
