#include <iostream>
#include <sstream>
#include <rev/common/RevAssert.h>
#include <rev/common/RevSleep.h>
#include <rev/engine/console/RevConsoleConfig.h>
#include <rev/engine/console/RevConsoleClient.h>
#include <rev/engine/console/RevConsoleServer.h>

using namespace std;
using namespace rev;

ConsoleClient client(REV_MM_IN_FILE_NAME, REV_MM_FILE_SIZE);
ConsoleServer server(REV_MM_OUT_FILE_NAME, REV_MM_FILE_SIZE);

void getServerResponse() {
	int argc;
	const char** argv = server.getInput(argc);
	if (argv) {
		ostringstream oss;
		for (int i = 0; i < argc; i++) {
			oss << argv[i] << " ";
		}
		cout << "[SERVER] " << oss.str() << endl;
	}
}

bool process() {
	getServerResponse();

	bool ret = false;
	char buf[256];
	bool exit = client.getInputString(buf, 256);
	if (!exit) {
		if (strcmp(buf, "exit") == 0) {
			return false;
		}
		client.send(buf);
		//cout << "Sending: " << buf << endl;
		ret = true; // keep processing...
	}

	return ret;
}

int main(int argc, char** argv) {
	if (argc > 1) {
		std::ostringstream oss;
		for (int i = 1; i < argc; i++) {
			oss << argv[i] << " ";
		}
		std::string cmd = oss.str();
		cout << "Sending command:\n" << cmd << endl;
		revAssert(cmd.size() < REV_MM_FILE_SIZE - 2);
		client.send(cmd.c_str());
		return 0;
	}

	cout << "Welcome to RevEngine console! :)" << endl;
	while (process()) {
		rev::sleep(50);
	}

	return 0;
}
