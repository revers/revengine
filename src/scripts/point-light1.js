var totalTime = 0;

self.update = function() {
    var elapsedMs = Engine.getFrameTime() * 0.01;
    totalTime += elapsedMs * 0.5;
    
    var angle = 0.1 * elapsedMs;

    var pos = self.position;
    var x = Math.cos(angle) * pos[0] - Math.sin(angle) * pos[2];
    var y = (Math.sin(totalTime) * 0.5 + 1.0) * 10.0;
    var z = Math.sin(angle) * pos[0] + Math.cos(angle) * pos[2];
    self.position = [x, y, z];
};