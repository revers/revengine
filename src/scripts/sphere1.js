
function ble() {
    log(self.name + ": ble");
}

self.init = function() {
    log("init " + self.name + " :) ############################################");
    ble();
};

self.destroy = function() {
    log("destroy1 " + self.name + " :(");
};

var totalTime = 0;

self.update = function() {
    totalTime += Engine.getFrameTime() * 0.01;
    var pos = self.position;
    self.position = [pos[0], Math.sin(totalTime), pos[2]];
};
