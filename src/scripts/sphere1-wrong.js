
function init() {
    log("init " + self.name + " :)");
}

function dest roy() {
    log("destroy " + self.name + " :(");
}

var totalTime = 0;

function update() {
    totalTime += Engine.getFrameTime() * 0.01;
    var pos = self.position;
    self.position = [pos[0], Math.sin(totalTime), pos[2]];
}
