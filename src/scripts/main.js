var totalTime = 0;

function ble() {
    log("MainScript: ble");
}

function init() {
    log("init :) $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
    ble();
    //  testBasicTypeArray();
    testBindableArray();
    //testBasicTypeArrayIterator();
    testBindableArrayIterator();
}

function destroy() {
    log("destroy :(");
}

function testBasicTypeArray() {
    var arrow = Engine.get("Arrow");
    var list = arrow.visual3DComponent.drawable.tempVector;
    log("testList = " + list);
    list[1] = [7, 8, 9];
    var size = list.getSize();
    for (var i = 0; i < size; i++) {
        log("list[" + i + "] = " + list[i]);
    }
}

function testBasicTypeArrayIterator() {
    var arrow = Engine.get("Arrow");
    var list = arrow.visual3DComponent.drawable.tempVector;
    var iter = list.iterator();
    log("testList = " + iter);
    while (iter.hasNext()) {
        var value = iter.next();
        log("value = " + value + " :)");
    }
}

function testBindableArray() {
    var gameObjects = Engine.gameObjects;
    var size = gameObjects.getSize();

    log("gameObjects.getSize() = " + size);
    for (var i = 0; i < size; i++) {
        log("gameObjects[" + i + "]: " + gameObjects[i].name);
    }
}

function testBindableArrayIterator() {
    var gameObjects = Engine.gameObjects;
    var iter = gameObjects.iterator();
    log("gameObjects.iter = " + iter + "; Engine.getFrameTime() = " + Engine.getFrameTime());

    while (iter.hasNext()) {
        var value = iter.next();
        log("value.name = " + value.name + " :)");
    }
}

function temp() {
    var sphere2 = Engine.get("Sphere #2");
    var scaling = Math.sin(totalTime * 0.001 + 2) + 1.0;
    sphere2.scaling = [scaling, scaling, scaling];

    scaling = Math.cos(totalTime * 0.001 + 2) + 1.0;
    var sphere3 = Engine.get("Sphere #3");
    sphere3.scaling = [scaling, scaling, scaling];
}

function shift(val) {
    val = val * 0.5 + 0.5;
    if (val < 0)
        val = 0;
    if (val > 1)
        val = 1;
    return val;
}

function rotate(name, x, z, speed) {
    var object = Engine.get(name);
    var angle = totalTime * 0.0001 * speed;
    object.position = [Math.cos(angle) * x - Math.sin(angle) * z, 0,
        Math.sin(angle) * x + Math.cos(angle) * z];
}

function update1() {
}

function update() {
    //var f1 = new Float32Array([1343232,2927872,1343488,2928128]);
    var elapsedMs = Engine.getFrameTime();

    var arrow = Engine.get("Arrow");
    arrow.rotate(0.01 * elapsedMs, 0, 0);

    var teapot = Engine.get("Teapot");
    var scaling = Math.sin(totalTime * 0.001) + 1.0;
    teapot.scaling = [scaling, scaling, scaling];

    var sphere = Engine.get("Sphere");
    var aa = 0.001 * elapsedMs;
    //sphere.move(aa, 0, 0);
    var v = [1, 1, 0];
    var q = quat.setAxisAngle(quat.create(), vec3.normalize(v, v), aa);

    sphere.position = vec3.transformQuat(v, sphere.position, q);
    //sphere.rotation = q;//quat.normalize(q, q);
    //temp();

    //log("sphere = " + sphere.visual3DComponent.effect.diffuseColor);
    sphere.visual3DComponent.effect.diffuseColor = [
        shift(scaling),
        shift(Math.cos(totalTime * 0.001)),
        shift(Math.cos(totalTime * 0.001 + 1))
    ];

    rotate("Cone", 25, 40, 1);
    rotate("Cuboid", -30, 45, -2);
    rotate("Torus", 2, 3, 20);

    totalTime += elapsedMs;
}