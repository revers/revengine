
var $g = {};
$g.objectList = [];

function init() {
    log("init :)");
    var i = 0;
    var maxBoxes = 10;
    var maxSpheres = 10;
    
    for (var j = 0; j < maxBoxes; j++) {
        $g.objectList[i++] = RigidBodyFactory.createRandomBox();
    }
    
    for (var j = 0; j < maxSpheres; j++) {
        $g.objectList[i++] = RigidBodyFactory.createRandomSphere();
    }
}

function destroy() {
    log("destroy :(");
    for (var i = 0; i < $g.objectList.length; i++) {
        var obj = $g.objectList[i];
        Engine.remove(obj);
    }
}

function update() {
}
