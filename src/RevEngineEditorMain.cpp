/*
 * File:   TestQtAppMain.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>
#include <GL/glew.h>

#include <QDesktopWidget>
#include <QApplication>
#include <QGLFormat>

#include <glm/glm.hpp>

#include <rev/common/RevConfigReader.h>
#include <rev/common/RevResourcePath.h>
#include <rev/engine/RevGameManager.h>
#include <rev/engine/RevEngine.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/qt/GameEditorWindow.h>
#include <rev/engine/glcontext/RevGLContextManager.h>

using namespace std;

void center(QWidget &widget) {

	int width = widget.width();
	int height = widget.height();

	QDesktopWidget* desktop = QApplication::desktop();

	int screenWidth = desktop->width();
	int screenHeight = desktop->height();

	int x = (screenWidth - width) / 2;
	int y = 25; //(screenHeight - height) / 2;

	widget.setGeometry(x, y, width, height);
}

#define REV_ENGINE_PROPERTIES "RevEngine.properties"

int main(int argc, char* argv[]) {
	if (!rev::ConfigReader::load(REV_ENGINE_PROPERTIES)) {
		REV_ERROR_MSG("Failed loading '" REV_ENGINE_PROPERTIES "' file!");
	}

	rev::ResourcePath::init(argc, argv);

	QApplication app(argc, argv);
	app.setApplicationName("RevEngine");
	app.setOrganizationName("revers");

	// The GL view
	QGLFormat format;
	// format.setVersion(3, 2);
	// Object instancing requires OpenGL >= 4.0:
	format.setVersion(4, 0);
	format.setProfile(QGLFormat::CoreProfile);
	format.setSampleBuffers(true);
	format.setSamples(16);

	rev::GameManager::createSingleton();

	//RevEngineWindow window(format);
	rev::GameEditorWindow window(format);
	window.show();
	center(window);

	rev::GLContextManager::ref().useMainContext();

	if (!rev::GameManager::ref().init()) {
		REV_ERROR_MSG("rev::GameManager::init() FAILED!");
		return -1;
	}

	window.initEngineRelatedStuff();

	rev::Engine::ref().start();

	int qtStatus = app.exec();

	rev::GameManager::destroySingleton();
	return qtStatus;
}
