/*
 * RevGameFactory.cpp
 *
 *  Created on: 01-12-2012
 *      Author: Revers
 */

#include <glm/gtc/matrix_transform.hpp>

#include "RevGameFactory.h"
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

#include <rev/engine/drawables/RevVBODrawables.h>
#include <rev/engine/components/experimental/RevParticleFountainComponent.h>
#include <rev/engine/components/experimental/RevParticleInstancingComponent.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/effects/RevDebugEffect.h>
#include <rev/engine/effects/RevStripesTextureEffect.h>
#include <rev/engine/effects/RevCheckboardTextureEffect.h>
#include <rev/engine/effects/RevFlatColorEffect.h>
#include <rev/engine/effects/RevBilinearSurfaceEffect.h>
#include <rev/engine/effects/RevPointSpheresEffect.h>
#include <rev/engine/RevGameObject.h>

#include <rev/engine/components/RevPhysicsPPSComponent.h>
#include <rev/engine/factory/RevPPSEventListener.h>
#include <rev/engine/factory/RevRigidBodyFactory.h>

using namespace rev;
using namespace glm;

rev::GameFactory* rev::GameFactory::gameFactory = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(GameFactory)

void GameFactory::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(!gameFactory);
	gameFactory = new GameFactory();

	RigidBodyFactory::createSingleton();
	REV_TRACE_FUNCTION_OUT;
}

void GameFactory::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(gameFactory);
	RigidBodyFactory::destroySingleton();

	delete gameFactory;
	gameFactory = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

GameFactory::~GameFactory() {
	// FIXME:
	// Temporary fix. When closing whole app GLContextManger is destroyed
	// earlier than GameFactory and ~IVBODrawables cannot schedule VAOs to destroy.
	if (GLContextManager::getInstance()) {
		deletePredefinedDrawables();
		deletePredefinedGLSLPrograms();
	}
}

bool GameFactory::createPredefinedGLSLPrograms() {
	predefinedGLSLPrograms[(int) PredefinedGLSLProgram::PHONG_SHADING] =
			PhongShadingEffect::createGLSLProgram();
	return true;
}

bool GameFactory::createPredefinedDrawables() {
	predefinedDrawables[(int) PredefinedDrawable::CUBE] = new VBOCubeVNT(2.0f);
	predefinedDrawables[(int) PredefinedDrawable::TORUS] = new VBOTorusVNT(
			7.0f / 2.0f, 3.0f / 2.0f, 40, 40);

	return true;
}

void GameFactory::deletePredefinedGLSLPrograms() {
	for (int i = 0; i < (int) PredefinedGLSLProgram::SIZE_OF_ENUM; i++) {
		delete predefinedGLSLPrograms[i];
	}
}

void GameFactory::deletePredefinedDrawables() {
	for (int i = 0; i < (int) PredefinedDrawable::SIZE_OF_ENUM; i++) {
		delete predefinedDrawables[i];
	}
}

rev::GameObject* GameFactory::createSimpleTorusGameObject() {
	rev::IVBODrawable* drawable = getPredefinedDrawable(
			PredefinedDrawable::TORUS);
	rev::IEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, false);

	GameObject* gameObject = new GameObject("Torus");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleCubeGameObject() {
	rev::IVBODrawable* drawable = getPredefinedDrawable(
			PredefinedDrawable::CUBE);
	rev::IEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, false);

	GameObject* gameObject = new GameObject("Cube");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleCageGameObject() {
	rev::IVBODrawable* drawable = new VBOTriangularCageVN(1.0f, 5);
	rev::PhongShadingEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, true);
	visualComponent->setCullFace(true);
	phongShadingEffect->setLightColor(
			color3(1.0, 155.0 / 255.0, 73.0 / 255.0));
	phongShadingEffect->setDiffuseColor(
			color3(1.0, 155.0 / 255.0, 73.0 / 255.0));
	phongShadingEffect->setAmbientColor(color3(0.1, 0.1, 0.1));
	visualComponent->setWireframe(true);

	GameObject* gameObject = new GameObject("Cage");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleFrustumGameObject() {

	glm::mat4 m = glm::perspective(60.0f, 800.0f / 600.0f, 2.0f, 20.0f);

	rev::IVBODrawable* drawable = new VBOFrustumVN(m);

	rev::PhongShadingEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, true);
//    phongShadingEffect->setLightColor(
//            glm::vec3(1.0, 155.0 / 255.0, 73.0 / 255.0));
//    phongShadingEffect->setDiffuseColor(
//            glm::vec3(1.0, 155.0 / 255.0, 73.0 / 255.0));
	phongShadingEffect->setAmbientColor(color3(0.1, 0.1, 0.1));

	GameObject* gameObject = new GameObject("Frustum");
	gameObject->setPosition(glm::vec3(-22, 8, -19));
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleCuboidGameObject() {
	rev::IVBODrawable* drawable = new VBOCuboidVNT(1.0f, 2.0f, 3.0f);
	rev::PhongShadingEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, true);
	phongShadingEffect->setLightColor(
			color3(1.0, 155.0 / 255.0, 73.0 / 255.0));
	phongShadingEffect->setDiffuseColor(
			color3(1.0, 155.0 / 255.0, 73.0 / 255.0));
	phongShadingEffect->setAmbientColor(color3(0.1, 0.1, 0.1));

	GameObject* gameObject = new GameObject("Cuboid");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimplePlaneGameObject() {
	rev::IVBODrawable* drawable = new VBOPlaneVNT(100.0, 100.0, 20, 20);
	rev::StripesTextureEffect* effect = new StripesTextureEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	effect->setLightColor(color3(1.0, 1.0, 1.0));
	effect->setStripeColor(color3(0.6, 1.0, 0.6));
	effect->setBackColor(color3(0.0, 0.8, 0.4));

	GameObject* gameObject = new GameObject("Plane");
	gameObject->setPosition(glm::vec3(0.0, -5.0, 0.0));
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimplePlane2GameObject() {
	rev::IVBODrawable* drawable = new VBOPlaneVNT(100.0, 100.0, 20, 20);
	rev::CheckboardTextureEffect* effect = new CheckboardTextureEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	effect->setLightColor(color3(1.0, 1.0, 1.0));
	effect->setColorA(color3(0.6, 1.0, 0.6));
	effect->setColorB(color3(0.0, 0.8, 0.4));

	GameObject* gameObject = new GameObject("Plane 2");
	gameObject->setPosition(glm::vec3(0.0, -5.0, 0.0));
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleAxesGameObject(
		float length/* = 20.0f */) {
	rev::IVBODrawable* drawable = new VBOAxesVC(length);
	rev::FlatColorEffect* effect = new FlatColorEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Axes");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleBilinearSurfaceGameObject(
		int xTesselation, int yTesselation, float scaleFactor) {
	using namespace glm;

	vec3 shift(0.5, 0.5, 0.5);
	//vec3 shift(0, 0, 0);
	rev::IVBODrawable* drawable = new VBOBilinearSurfaceVNT(
			(vec3(0, 0, 1) - shift) * scaleFactor,
			(vec3(1, 0, 0) - shift) * scaleFactor,
			(vec3(1, 1, 1) - shift) * scaleFactor,
			(vec3(0, 1, 0) - shift) * scaleFactor,
			xTesselation, yTesselation);

	//rev::FlatColorEffect* effect = new FlatColorEffect();
	rev::IEffect* effect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Bilinear Surface");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleBiquadraticSurfaceGameObject(
		int xTesselation,
		int yTesselation,
		float scaleFactor) {

	using namespace glm;

	vec3 p00(0, 0, 0);
	vec3 p01(1, 0, 0);
	vec3 p02(2, 0, 0);
	vec3 p10(0, 1, 0);
	vec3 p11(1, 1, 1);
	vec3 p12(2, 1, -.5);
	vec3 p20(0, 2, 0);
	vec3 p21(1, 2, 0);
	vec3 p22(2, 2, 0);

	typedef VBOBiquadraticSurfaceVNT::BasisMatrix BasisMatrix;

	vec3 shift(1.0, 1.0, 0.5);
	rev::IVBODrawable* drawable = new VBOBiquadraticSurfaceVNT(
			BasisMatrix::LAGRANGE,
			(p00 - shift) * scaleFactor,
			(p01 - shift) * scaleFactor,
			(p02 - shift) * scaleFactor,
			(p10 - shift) * scaleFactor,
			(p11 - shift) * scaleFactor,
			(p12 - shift) * scaleFactor,
			(p20 - shift) * scaleFactor,
			(p21 - shift) * scaleFactor,
			(p22 - shift) * scaleFactor,
			xTesselation, yTesselation);

	rev::IEffect* effect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Biquadratic Surface");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleBicubicSurfaceGameObject(
		int xTesselation,
		int yTesselation,
		float scaleFactor) {

	using namespace glm;

	vec3 p00(0, 0, 0);
	vec3 p01(1, 0, 0);
	vec3 p02(2, 0, 0);
	vec3 p03(3, 0, 0);
	vec3 p10(0, 1, 0);
	vec3 p11(1, 1, 1);
	vec3 p12(2, 1, -.5);
	vec3 p13(3, 1, 0);
	vec3 p20(0, 2, -.5);
	vec3 p21(1, 2, 0);
	vec3 p22(2, 2, .5);
	vec3 p23(3, 2, 0);
	vec3 p30(0, 3, 0);
	vec3 p31(1, 3, 0);
	vec3 p32(2, 3, 0);
	vec3 p33(3, 3, 0);

	typedef VBOBicubicSurfaceVNT::BasisMatrix BasisMatrix;

	vec3 shift(0.0, 0.0, 0.0);
	rev::IVBODrawable* drawable = new VBOBicubicSurfaceVNT(
			BasisMatrix::BSPLINE,
			(p00 - shift) * scaleFactor, (p01 - shift) * scaleFactor,
			(p02 - shift) * scaleFactor, (p03 - shift) * scaleFactor,
			(p10 - shift) * scaleFactor, (p11 - shift) * scaleFactor,
			(p12 - shift) * scaleFactor, (p13 - shift) * scaleFactor,
			(p20 - shift) * scaleFactor, (p21 - shift) * scaleFactor,
			(p22 - shift) * scaleFactor, (p23 - shift) * scaleFactor,
			(p30 - shift) * scaleFactor, (p31 - shift) * scaleFactor,
			(p32 - shift) * scaleFactor, (p33 - shift) * scaleFactor,
			xTesselation, yTesselation);

	rev::IEffect* effect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Bicubic Surface");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleSphereGameObject(float radius/* = 1.0f*/,
		int tessellation/*= 30*/) {
	rev::IVBODrawable* drawable = new VBOSphereVN(radius, tessellation);
	rev::IEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, false);

	GameObject* gameObject = new GameObject("Sphere");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleCylinderGameObject(
		float height/* = 2.0f*/, float radius/* = 1.0f*/,
		int tessellation/* = 30*/) {
	rev::IVBODrawable* drawable = new VBOCylinderVN(height, radius, tessellation);
	rev::IEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, false);

	GameObject* gameObject = new GameObject("Cylinder");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleTeapotGameObject() {
	rev::IVBODrawable* drawable = new VBOTeapotVNT(10);
	rev::IEffect* phongShadingEffect = createPhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, false);

	GameObject* gameObject = new GameObject("Teapot");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleBilinearEffectGameObject() {
	rev::IVBODrawable* drawable = new VBOPlaneVNT(10.0, 10.0, 20, 20);
	rev::BilinearSurfaceEffect* effect = new BilinearSurfaceEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);

	GameObject* gameObject = new GameObject("Bilinear Effect");
	//gameObject->setPosition(glm::vec3(0.0, -5.0, 0.0));
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleConeGameObject() {
	rev::IVBODrawable* drawable = new VBOConeVN(10, 20, 100);
	rev::IEffect* effect = new PhongShadingEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Cone");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSimpleArrowGameObject() {
	rev::IVBODrawable* drawable = new VBOArrowVN(0.5, 5.0, 100);
	rev::IEffect* effect = new PhongShadingEffect();
	//new DebugEffect();
	rev::IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);
	GameObject* gameObject = new GameObject("Arrow");
	gameObject->setVisual3DComponent(visualComponent);

	return gameObject;
}

bool GameFactory::init() {
	REV_TRACE_FUNCTION;
	if (!createPredefinedGLSLPrograms()) {
		return false;
	}
	if (!createPredefinedDrawables()) {
		return false;
	}
	if (!RigidBodyFactory::ref().init()) {
		return false;
	}
	return true;
}

rev::GameObject* GameFactory::createSampleParticleObject() {
	GameObject* gameObject = new GameObject("Sample particle");
	ParticleFountainComponent* particleComponent = new ParticleFountainComponent(1000);
	gameObject->setVisual3DComponent(particleComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSampleParticleInstancingObject() {
	GameObject* gameObject = new GameObject("Sample Instancing");
	ParticleInstancingComponent* particleComponent = new ParticleInstancingComponent(500);
	gameObject->setVisual3DComponent(particleComponent);

	return gameObject;
}

rev::GameObject* GameFactory::createSamplePhysicsObject() {
	GameObject* gameObject = new GameObject("Sample Physics");

	PhysicsPPSComponent* physics = new PhysicsPPSComponent();
	gameObject->setPhysicsComponent(physics);

	VBOPointSpheresVC* drawable = new VBOPointSpheresVC(physics->getParticlesCount(),
			rev::color3(0.28, 1, 0.44));

	for (int i = 0; i < physics->getParticlesCount(); i++) {
		drawable->setPoint(i, physics->getParticlePosition(i));
	}
	drawable->reinit();

	PointSpheresEffect* effect = new PointSpheresEffect();
	effect->setPointRadius(physics->getParticleRadius(0));

	IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			effect, true, drawable, true);

	gameObject->setVisual3DComponent(visualComponent);

	PPSEventListener* eventListener = new PPSEventListener();
	gameObject->setEventListenerComponent(eventListener);

	return gameObject;
}
