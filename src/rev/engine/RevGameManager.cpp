/*
 * RevGameManager.cpp
 *
 *  Created on: 01-12-2012
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include "RevGameManager.h"
#include "RevEngine.h"
#include "RevGameFactory.h"

using namespace rev;
using namespace glm;

rev::GameManager* rev::GameManager::gameManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(GameManager)

void GameManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!gameManager);
	gameManager = new GameManager();
	GameFactory::createSingleton();
	Engine::createSingleton();
	REV_TRACE_FUNCTION_OUT;
}

void GameManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(gameManager);
	Engine::destroySingleton();
	GameFactory::destroySingleton();
	delete gameManager;
	gameManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void GameManager::createScene() {
	GameObject* simpleAxes = GameFactory::ref().createSimpleAxesGameObject();
	Engine::ref().addGameObject(simpleAxes);

	GameObject* simpleCone = GameFactory::ref().createSimpleConeGameObject();
	simpleCone->setPosition(glm::vec3(30.0, 7.0, -30.0));
	Engine::ref().addGameObject(simpleCone);

	GameObject* simpleArrow = GameFactory::ref().createSimpleArrowGameObject();
	simpleArrow->setPosition(glm::vec3(0.0, 7.0, 10.0));
	Engine::ref().addGameObject(simpleArrow);

	GameObject* simpleBilinearSurface =
			GameFactory::ref().createSimpleBilinearSurfaceGameObject(40, 40, 10.0);
	simpleBilinearSurface->setPosition(vec3(0.0, 0.0, 25.0));
	Engine::ref().addGameObject(simpleBilinearSurface);

	GameObject* simpleBiquadraticSurface =
			GameFactory::ref().createSimpleBiquadraticSurfaceGameObject(40, 40,
					50.0);
	simpleBiquadraticSurface->setPosition(vec3(5.0, 1.0, -10.0));
	Engine::ref().addGameObject(simpleBiquadraticSurface);

	GameObject* simpleBicubicSurface =
			GameFactory::ref().createSimpleBicubicSurfaceGameObject(40, 40, 5.0);
	simpleBicubicSurface->setPosition(vec3(10.0, 1.0, -3.0));
	Engine::ref().addGameObject(simpleBicubicSurface);

	GameObject* simpleCage = GameFactory::ref().createSimpleCageGameObject();
	simpleCage->setScaling(vec3(10.0));
	simpleCage->setPosition(vec3(-20.0, 1.0, -3.0));
	Engine::ref().addGameObject(simpleCage);

	GameObject* simpleTorus = GameFactory::ref().createSimpleTorusGameObject();
	simpleTorus->setPosition(vec3(0.0, 1.0, 7.0));
	Engine::ref().addGameObject(simpleTorus);

	GameObject* simpleCube = GameFactory::ref().createSimpleCubeGameObject();
	simpleCube->setPosition(glm::vec3(5.0, 5.0, 5.0));
	Engine::ref().addGameObject(simpleCube);

	GameObject* simpleCuboid = GameFactory::ref().createSimpleCuboidGameObject();
	simpleCuboid->setPosition(glm::vec3(8.0, 0.0, 0.0));
	Engine::ref().addGameObject(simpleCuboid);

	GameObject* simplePlane = GameFactory::ref().createSimplePlane2GameObject();
	Engine::ref().addGameObject(simplePlane);

	GameObject* simpleSphere = GameFactory::ref().createSimpleSphereGameObject();
	simpleSphere->setPosition(glm::vec3(0.0, 7.0, 0.0));
	Engine::ref().addGameObject(simpleSphere);

	GameObject* simpleCylinder =
			GameFactory::ref().createSimpleCylinderGameObject();
	simpleCylinder->setPosition(vec3(-5.0, 1.0, -5.0));
	Engine::ref().addGameObject(simpleCylinder);

	GameObject* simpleTeapot =
			GameFactory::ref().createSimpleTeapotGameObject();
	simpleTeapot->setPosition(vec3(15.0, 2.0, -15.0));
	simpleTeapot->setOrientation(
			glm::quat_cast(glm::rotate(-90.0f, 1.0f, 0.0f, 0.0f)));
	Engine::ref().addGameObject(simpleTeapot);

	GameObject* simpleBilinearEffect =
			GameFactory::ref().createSimpleBilinearEffectGameObject();
	simpleBilinearEffect->setPosition(vec3(3.0, 14.0, 25.0));
	Engine::ref().addGameObject(simpleBilinearEffect);

	GameObject* simpleFrustum =
			GameFactory::ref().createSimpleFrustumGameObject();
	//simpleCylinder->setPosition(vec3(-5.0, 1.0, -5.0));
	Engine::ref().addGameObject(simpleFrustum);
}

void GameManager::bind(IBinder& binder) {
}

bool GameManager::init() {
	if (!GameFactory::ref().init()) {
		REV_ERROR_MSG("GameFactory::ref().init() FAILED!");
		return false;
	}

	//if (!Engine::ref().init()) {
	//	REV_ERROR_MSG("Engine::ref().init() FAILED!");
	//	return false;
	//}

	EventManager::ref().addComponent(this);

	//createScene();
	//addSamplePhysicsObject();

	return true;
}

void GameManager::addSamplePhysicsObject() {
	GameObject* go = GameFactory::ref().createSamplePhysicsObject();
	Engine::ref().addGameObject(go);
}

void GameManager::addSampleParticleObject() {
	GameObject* go = GameFactory::ref().createSampleParticleObject();
	Engine::ref().addGameObject(go);
}

void GameManager::keyPressed(const KeyEvent& e) {
	if (e.getKey() == Key::KEY_SPACE) {
		ScriptingManager& m = ScriptingManager::ref();
		bool pause = !m.isPaused();
		m.setPaused(pause);
		REV_TRACE_MSG("ScriptingManager.paused = " << (pause ? "true" : "false"));
	}
}
