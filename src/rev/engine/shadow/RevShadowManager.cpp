/*
 * RevShadowManager.cpp
 *
 *  Created on: 27 lip 2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/drawables/RevVBOQuadVT.h>
#include <rev/engine/components/RevIVisual3DComponent.h>

#include "RevShadowManager.h"

#define SHADOW_MAP_DEPTH_SHADER "shaders/ShadowMapDepth.glsl"
#define QUAD_TEXTURE_SHADER "shaders/QuadDepthTexture.glsl"

using namespace glm;

namespace rev {

rev::ShadowManager* rev::ShadowManager::shadowManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(ShadowManager)

ShadowManager::ShadowManager() :
		bias(vec4(0.5f, 0.0f, 0.0f, 0.0f),
				vec4(0.0f, 0.5f, 0.0f, 0.0f),
				vec4(0.0f, 0.0f, 0.5f, 0.0f),
				vec4(0.5f, 0.5f, 0.5f, 1.0f)) {
}

ShadowManager::~ShadowManager() {
	if (quad) {
		delete quad;
	}
}

void ShadowManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!shadowManager);
	shadowManager = new ShadowManager();
	REV_TRACE_FUNCTION_OUT;
}

void ShadowManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(shadowManager);

	delete shadowManager;
	shadowManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

bool ShadowManager::init() {
	REV_TRACE_FUNCTION;

	if (!createDepthGLSLProgram()) {
		return false;
	}
	if (!createQuadTexGLSLProgram()) {
		return false;
	}
	if (!createFBO()) {
		return false;
	}
	if (!buildJitterTex()) {
		return false;
	}
	quad = new VBOQuadVT(1.0f);

	inited = true;
	return true;
}

void ShadowManager::renderQuadWithDepthTexture() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	quadTexProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glAssert;

	quadTexProgram.setUniform(texLocation, 0);
	quad->render();

	quadTexProgram.unuse();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void ShadowManager::renderObjects() {
	Renderer3D& renderer = Renderer3D::ref();

	for (IVisual3DComponent*& c : renderer.getComponents()) {
		if (!renderer.isProperForRender(c)) {
			continue;
		}
		mat4 mvp = viewProjMatrix * c->getParent()->getModelMatrix();
		depthProgram.setUniform(mvpLocation, mvp);
		c->render();
	}
}

void ShadowManager::renderDepthMap() {
	ShadowSourceComponent* shadowSource = getActiveShadowSource();
	if (!shadowSource) {
		return;
	}
	GameObject* parent = shadowSource->getParent();

	const vec3& position = parent->getPosition();
	const quat& rot = parent->getOrientation();
	vec3 up = MathHelper::rotate(vec3(0, 1, 0), rot);
	//vec3 up = vec3(0, 1, 0);
	vec3 direction = MathHelper::rotate(vec3(1, 0, 0), rot);

	viewMatrix = glm::lookAt(position, position + direction, up);
	const mat4& projectionMatrix = shadowSource->getProjectionMatrix();
	viewProjMatrix = projectionMatrix * viewMatrix;
	biasedViewProjMatrix = bias * viewProjMatrix;

	depthProgram.use();

	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, shadowMapWidth, shadowMapHeight);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	renderObjects();

	glFlush();
	glFinish();

	depthProgram.unuse();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDisable(GL_CULL_FACE);
}

bool ShadowManager::createFBO() {
	GLfloat border[] = { 1.0f, 0.0f, 0.0f, 0.0f };
	// The depth buffer texture
	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapWidth,
			shadowMapHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);

	// Assign the depth buffer texture to texture channel 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);

	// Create and set up the FBO
	glGenFramebuffers(1, &shadowFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
	GL_TEXTURE_2D, depthTex, 0);

	GLenum drawBuffers[] = { GL_NONE };
	glDrawBuffers(1, drawBuffers);

	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (result != GL_FRAMEBUFFER_COMPLETE) {
		REV_ERROR_MSG("Framebuffer is not complete!");
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glAssert;

	return glGetError() == GL_NO_ERROR;
}
bool ShadowManager::createDepthGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(SHADOW_MAP_DEPTH_SHADER);
	if (!depthProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	// glBindAttribLocation(depthProgram.getHandle(), 0, "VertexPosition");
	// glAssert;

	if (!depthProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	depthProgram.use();
	mvpLocation = depthProgram.getUniformLocation("MVP");
	revAssert(mvpLocation != -1);
	depthProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

bool ShadowManager::createQuadTexGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(QUAD_TEXTURE_SHADER);
	if (!quadTexProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	glBindAttribLocation(quadTexProgram.getHandle(), 0, "VertexPosition");
	glBindAttribLocation(quadTexProgram.getHandle(), 1, "VertexTexCoord");
	glBindAttribLocation(quadTexProgram.getHandle(), 0, "FragColor");
	glAssert;

	if (!quadTexProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	quadTexProgram.use();
	texLocation = quadTexProgram.getUniformLocation("Tex");
	revAssert(texLocation != -1);
	quadTexProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

ShadowSourceComponent* ShadowManager::getActiveShadowSource() {
	if (components.empty()) {
		return nullptr;
	}

	return components[0];
}

void ShadowManager::bindDepthTexture() {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);
}

void ShadowManager::bindJitterTexture() {
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_3D, jitterTex);
}

bool ShadowManager::buildJitterTex() {
	int size = jitterMapSize;
	int samples = samplesU * samplesV;
	int bufSize = size * size * samples * 2;
	float *data = new float[bufSize];

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < samples; k += 2) {
				int x1, y1, x2, y2;
				x1 = k % (samplesU);
				y1 = (samples - 1 - k) / samplesU;
				x2 = (k + 1) % samplesU;
				y2 = (samples - 1 - k - 1) / samplesU;

				vec4 v;
				// Center on grid and jitter
				v.x = (x1 + 0.5f) + jitter();
				v.y = (y1 + 0.5f) + jitter();
				v.z = (x2 + 0.5f) + jitter();
				v.w = (y2 + 0.5f) + jitter();

				// Scale between 0 and 1
				v.x /= samplesU;
				v.y /= samplesV;
				v.z /= samplesU;
				v.w /= samplesV;

				// Warp to disk
				int cell = ((k / 2) * size * size + j * size + i) * 4;
				data[cell + 0] = sqrtf(v.y) * cosf( TWO_PI * v.x);
				data[cell + 1] = sqrtf(v.y) * sinf( TWO_PI * v.x);
				data[cell + 2] = sqrtf(v.w) * cosf( TWO_PI * v.z);
				data[cell + 3] = sqrtf(v.w) * sinf( TWO_PI * v.z);
			}
		}
	}

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &jitterTex);

	glBindTexture(GL_TEXTURE_3D, jitterTex);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, size, size, samples / 2, 0, GL_RGBA, GL_FLOAT, data);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);

	delete[] data;

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

float ShadowManager::jitter() {
	return ((float) rand() / RAND_MAX) - 0.5f;
}

} /* namespace rev */

