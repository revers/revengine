/*
 * RevShadowManager.h
 *
 *  Created on: 27 lip 2013
 *      Author: Revers
 */

#ifndef REVSHADOWMANAGER_H_
#define REVSHADOWMANAGER_H_
#include <vector>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/engine/components/RevShadowSourceComponent.h>

#define DEFAULT_SHADOW_MAP_SIZE 512

namespace rev {

class VBOQuadVT;

typedef std::vector<ShadowSourceComponent*> ShadowSourceComponentVect;

class ShadowManager final: public IBindable {
	static ShadowManager* shadowManager;
	ShadowSourceComponentVect components;

	ShadowManager();
	ShadowManager(const ShadowManager&) = delete;

private:
	glm::mat4 bias;
	glm::mat4 viewProjMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 biasedViewProjMatrix;
	GLuint shadowFBO = 0;
	GLuint depthTex = 0;
	GLuint jitterTex = 0;
	int mvpLocation = -1;
	int texLocation = -1;
	GLSLProgram depthProgram;
	GLSLProgram quadTexProgram;
	VBOQuadVT* quad = nullptr;
	int shadowMapWidth = DEFAULT_SHADOW_MAP_SIZE;
	int shadowMapHeight = DEFAULT_SHADOW_MAP_SIZE;
	bool inited = false;

	int samplesU = 4;
	int samplesV = 8;
	int jitterMapSize = 8;
	glm::vec3 offsetTexSize = glm::vec3(jitterMapSize, jitterMapSize, samplesU * samplesV / 2.0f);

public:
	DECLARE_BINDABLE_SINGLETON(ShadowManager)

	~ShadowManager();

	static void createSingleton();
	static void destroySingleton();

	static ShadowManager& ref() {
		return *shadowManager;
	}

	static ShadowManager* getInstance() {
		return shadowManager;
	}

	bool init();

	bool isInited() {
		return inited;
	}

	void update() {
	}

	bool hasShadowSource() {
		return components.size() > 0;
	}

	void setDepthTextureModelMatrix(const glm::mat4& m);

	void renderQuadWithDepthTexture();

	ShadowSourceComponent* getActiveShadowSource();

	void renderDepthMap();

	/**
	 * ShadowManager is NOT owner of added component.
	 */
	void addComponent(ShadowSourceComponent* comp) {
		components.push_back(comp);
	}

	void removeComponent(ShadowSourceComponent* comp) {
		components.erase(std::remove(components.begin(), components.end(), comp),
				components.end());
	}

	void removeAllComponents() {
		components.clear();
	}

	const glm::mat4& getViewMatrix() const {
		return viewMatrix;
	}

	const glm::mat4& getViewProjMatrix() const {
		return viewProjMatrix;
	}

	const glm::mat4& getBiasedViewProjMatrix() const {
		return biasedViewProjMatrix;
	}

	const glm::vec3& getOffsetTexSize() const {
		return offsetTexSize;
	}

	ShadowSourceComponentVect& getComponents() {
		return components;
	}

	void bindDepthTexture();
	void bindJitterTexture();

private:
	void renderObjects();
	bool createDepthGLSLProgram();
	bool createQuadTexGLSLProgram();
	bool createFBO();
	bool buildJitterTex();
	float jitter();

};

} /* namespace rev */
#endif /* REVSHADOWMANAGER_H_ */
