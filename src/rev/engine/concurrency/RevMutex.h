/*
 * RevMutex.h
 *
 *  Created on: 23-03-2013
 *      Author: Revers
 */

#ifndef REVMUTEX_H_
#define REVMUTEX_H_

namespace rev {

	class MutexImpl;

	class Mutex {
		MutexImpl* mutexImpl;
		public:
		Mutex();
		~Mutex();

		void lock();

		void unlock();
	};

	class MutexGuard {
		Mutex* mutex;

	public:
		MutexGuard(Mutex* mutex) :
				mutex(mutex) {
			mutex->lock();
		}

		~MutexGuard() {
			mutex->unlock();
		}
	};

} /* namespace rev */
#endif /* REVMUTEX_H_ */
