/*
 * RevMutex.cpp
 *
 *  Created on: 23-03-2013
 *      Author: Revers
 */

#include <rev/engine/config/RevEngineConfig.h>
#include "RevMutex.h"

using namespace rev;

#ifdef REV_ENGINE_QT

#include <QMutex>

namespace rev {
	class MutexImpl {
		QMutex mutex;

	public:
		inline void lock() {
			mutex.lock();
		}

		inline void unlock() {
			mutex.unlock();
		}
	};
}
#endif

Mutex::Mutex() {
	mutexImpl = new MutexImpl();
}

Mutex::~Mutex() {
	delete mutexImpl;
}

void Mutex::lock() {
	mutexImpl->lock();
}

void Mutex::unlock() {
	mutexImpl->unlock();
}
