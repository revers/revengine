/*
 * RevGameObject.h
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#ifndef REVGAMEOBJECT_H_
#define REVGAMEOBJECT_H_

#include <string>
#include <ostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <rev/engine/components/RevIComponent.h>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

class IVisual3DComponent;
class IEventListenerComponent;
class IPhysicsComponent;
class ScriptingComponent;
class CollisionComponent;
class ILightComponent;
class ShadowSourceComponent;

enum class GameObjectState {
	/**
	 * Default state. Alive game object.
	 */
	ALIVE,

	/**
	 * GameObject just before removing.
	 * You can clean your references to this game object
	 * because soon it will be deleted.
	 */
	BEFORE_DEATH,

	/**
	 * In a next frame of the game loop this game object
	 * will be deleted. You should immediately remove all
	 * your references to this game object!
	 */
	DEATH
};

class GameObject: public IBindable {
	friend class Engine;

	static int staticGlobalId;

	int id;
	std::string name;
	GameObjectState state = GameObjectState::ALIVE;
	bool visible = true;
	bool saveable = true;

protected:
	IVisual3DComponent* visual3DComponent = nullptr;
	IEventListenerComponent* eventListenerComponent = nullptr;
	IPhysicsComponent* physicsComponent = nullptr;
	ScriptingComponent* scriptingComponent = nullptr;
	CollisionComponent* collisionComponent = nullptr;
	ILightComponent* lightComponent = nullptr;
	ShadowSourceComponent* shadowComponent = nullptr;

	glm::vec3 position;
	glm::quat orientation;
	glm::vec3 scaling;

public:
	DECLARE_BINDABLE(GameObject)

	static const glm::vec3 FORWARD;

	GameObject();
	GameObject(const std::string& name);

	virtual ~GameObject();

	int getId() const {
		return id;
	}
	const std::string& getName() const {
		return name;
	}
	const glm::vec3& getPosition() const {
		return position;
	}
	void setPosition(const glm::vec3& position) {
		this->position = position;
	}
	void setPosition(float x, float y, float z) {
		position.x = x;
		position.y = y;
		position.z = z;
	}
	const glm::quat& getOrientation() const {
		return orientation;
	}
	void setOrientation(const glm::quat& orientation) {
		this->orientation = orientation;
	}
	void setOrientation(float w, float x, float y, float z) {
		orientation.w = w;
		orientation.x = x;
		orientation.y = y;
		orientation.z = z;
	}
	void rotate(const glm::quat& rotation) {
		this->orientation = rotation * this->orientation;

	}
	void rotate(const glm::vec3& yawPitchRoll) {
		orientation = glm::quat(yawPitchRoll) * orientation;
		orientation = glm::normalize(orientation);
	}
	void rotate(float yaw, float pitch, float roll) {
		orientation = glm::quat(glm::vec3(yaw, pitch, roll)) * orientation;
		orientation = glm::normalize(orientation);
	}
	void translate(float x, float y, float z) {
		position.x += x;
		position.y += y;
		position.z += z;
	}
	void translate(const glm::vec3& translation) {
		position += translation;
	}
	void scale(float x, float y, float z) {
		scaling.x *= x;
		scaling.y *= y;
		scaling.z *= z;
	}
	void scale(const glm::vec3& scaling) {
		this->scaling *= scaling;
	}
	const glm::vec3& getScaling() const {
		return scaling;
	}
	void setScaling(const glm::vec3& scaling) {
		this->scaling = scaling;
	}

	/**
	 * Returns matrix T * R * S.
	 * (Translation * Rotation * Scale).
	 *
	 * You can decompose it by rev::MathUtil::decomposeTRS().
	 *
	 */
	glm::mat4 getModelMatrix() {
		return glm::translate(position) * glm::mat4_cast(orientation) * glm::scale(scaling);
	}
	GameObjectState getState() {
		return state;
	}
	bool isAlive() const {
		return state == GameObjectState::ALIVE;
	}
	bool isBeforeDeath() const {
		return state == GameObjectState::BEFORE_DEATH;
	}
	bool isDeath() const {
		return state == GameObjectState::DEATH;
	}
	bool isVisible() const {
		return visible;
	}
	void setVisible(bool visible) {
		this->visible = visible;
	}
	static void initScripting();

	IEventListenerComponent* getEventListenerComponent() {
		return eventListenerComponent;
	}
	void setEventListenerComponent(IEventListenerComponent* eventListenerComponent);

	IPhysicsComponent* getPhysicsComponent() {
		return physicsComponent;
	}
	void setPhysicsComponent(IPhysicsComponent* physicsComponent);

	IVisual3DComponent* getVisual3DComponent() {
		return visual3DComponent;
	}
	void setVisual3DComponent(IVisual3DComponent* visual3DComponent);

	ScriptingComponent* getScriptingComponent() {
		return scriptingComponent;
	}
	void setScriptingComponent(ScriptingComponent* scriptingComponent);

	CollisionComponent* getCollisionComponent() {
		return collisionComponent;
	}
	void setCollisionComponent(CollisionComponent* collisionComponent);

	ILightComponent* getLightComponent() {
		return lightComponent;
	}
	void setLightComponent(ILightComponent* lightComponent);

	ShadowSourceComponent* getShadowComponent() {
		return shadowComponent;
	}
	void setShadowComponent(ShadowSourceComponent* shadowComponent);

	void destroy() {
		if (state == GameObjectState::ALIVE) {
			state = GameObjectState::BEFORE_DEATH;
		}
	}
	friend std::ostream& operator<<(std::ostream& out, const GameObject& go) {
		out << go.getName() << " (" << go.getId() << ")";
		return out;
	}
	friend std::ostream& operator<<(std::ostream& out, const GameObject* go) {
		out << go->getName() << " (" << go->getId() << ")";
		return out;
	}
	std::string toString() const;

	bool isSaveable() const {
		return saveable;
	}

	void setSaveable(bool saveable) {
		this->saveable = saveable;
	}

private:
	void setState(GameObjectState state) {
		this->state = state;
	}

protected:
	void bind(IBinder& binder) override;

private:
	static v8::Handle<v8::Value> jsRotate(const v8::Arguments& args);
	static v8::Handle<v8::Value> jsScale(const v8::Arguments& args);
	static v8::Handle<v8::Value> jsTranslate(const v8::Arguments& args);

};

} /* namespace rev */
#endif /* REVGAMEOBJECT_H_ */
