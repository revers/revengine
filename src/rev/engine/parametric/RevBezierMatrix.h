/*
 * RevBezierMatrix.h
 *
 *  Created on: 20-04-2013
 *      Author: Revers
 */

#ifndef REVBEZIERMATRIX_H_
#define REVBEZIERMATRIX_H_

#include <glm/glm.hpp>

namespace rev {

	template<typename T>
	class BezierMatrix {
		BezierMatrix() = delete;
		~BezierMatrix() = delete;
	public:
		typedef T scalar_t;

		/**
		 * M = | -1  1 |
		 *     |  1  0 |
		 */
		static const scalar_t M2[2][2];

		/**
		 *     |  1 -2  1 |
		 * M = | -2  2  0 |
		 *     |  1  0  0 |
		 */
		static const scalar_t M3[3][3];

		/**
		 *     | -1   3  -3   1 |
		 * M = |  3  -6   3   0 |
		 *     | -3   3   0   0 |
		 *     |  1   0   0   0 |
		 */
		static const scalar_t M4[4][4];

		/**
		 *     |  1  -4   6  -4  1 |
		 *     | -4  12 -12   4  0 |
		 * M = |  6 -12   6   0  0 |
		 *     | -4   4   0   0  0 |
		 *     |  1   0   0   0  0 |
		 */
		static const scalar_t M5[5][5];

		/**
		 *     |  -1   5 -10  10  -5   1  |
		 *     |   5 -20  30 -20   5   0  |
		 * M = | -10  30 -30  10   0   0  |
		 *     |  10 -20  10   0   0   0  |
		 *     |  -5   5   0   0   0   0  |
		 *     |   1   0   0   0   0   0  |
		 */
		static const scalar_t M6[6][6];

		/**
		 *     |   1  -6  15 -20  15  -6   1  |
		 *     |  -6  30 -60  60 -30   6   0  |
		 *     |  15 -60  90 -60  15   0   0  |
		 * M = | -20  60 -60  20   0   0   0  |
		 *     |  15 -30  15   0   0   0   0  |
		 *     |  -6   6   0   0   0   0   0  |
		 *     |   1   0   0   0   0   0   0  |
		 */
		static const scalar_t M7[7][7];

		/**
		 *     |  -1    7  -21   35  -35   21   -7    1  |
		 *     |   7  -42  105 -140  105  -42    7    0  |
		 *     | -21  105 -210  210 -105   21    0    0  |
		 * M = |  35 -140  210 -140   35    0    0    0  |
		 *     | -35  105 -105   35    0    0    0    0  |
		 *     |  21  -42   21    0    0    0    0    0  |
		 *     |  -7    7    0    0    0    0    0    0  |
		 *     |   1    0    0    0    0    0    0    0  |
		 */
		static const scalar_t M8[8][8];
	};

	//----------------------------------------------------------------
	// Double specializations:
	//================================================================
	template<>
	const double BezierMatrix<double>::M2[2][2];

	template<>
	const double BezierMatrix<double>::M3[3][3];

	template<>
	const double BezierMatrix<double>::M4[4][4];

	template<>
	const double BezierMatrix<double>::M5[5][5];

	template<>
	const double BezierMatrix<double>::M6[6][6];

	template<>
	const double BezierMatrix<double>::M7[7][7];

	template<>
	const double BezierMatrix<double>::M8[8][8];

	//----------------------------------------------------------------
	// Float specializations:
	//================================================================
	template<>
	const float BezierMatrix<float>::M2[2][2];

	template<>
	const float BezierMatrix<float>::M3[3][3];

	template<>
	const float BezierMatrix<float>::M4[4][4];

	template<>
	const float BezierMatrix<float>::M5[5][5];

	template<>
	const float BezierMatrix<float>::M6[6][6];

	template<>
	const float BezierMatrix<float>::M7[7][7];

	template<>
	const float BezierMatrix<float>::M8[8][8];

} /* namespace rev */
#endif /* REVBEZIERMATRIX_H_ */
