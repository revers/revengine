/*
 * RevBezierCurve.h
 *
 *  Created on: 20-04-2013
 *      Author: Revers
 */

#ifndef REVBEZIERCURVE_H_
#define REVBEZIERCURVE_H_

#include <cmath>
#include <glm/glm.hpp>

namespace rev {

    /**
     * Unoptimized arbitrary degree Bezier curve.
     */
    //----------------------------------------------------------------
    // TBezierCurve
    //================================================================

    template<typename ScalarType, template<typename U> class VecType>
    struct TBezierCurve {
        typedef ScalarType scalar_t;
        typedef VecType<scalar_t> vec_t;

        vec_t* P;
        int N;

    public:

        TBezierCurve(vec_t* points, int pointsLength) :
        P(points), N(pointsLength) {
        }

        vec_t eval(scalar_t t) {
            vec_t result(0);
            for (int i = 0; i < N; i++) {
                result += P[i] * bernstein(N - 1, i, t);
            }

            return result;
        }

        /**
         * First derivative.
         * http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
         */
        vec_t dt(scalar_t t) {
            vec_t result(0);

            for (int i = 0; i < N - 1; i++) {
                result += (scalar_t(N - 1) * (P[i + 1] - P[i])) * bernstein(N - 2, i, t);
            }

            return result;
        }

    private:

        scalar_t biCoefficient(int n, int k) {
            if (k > n - k) {
                k = n - k;
            }

            scalar_t c(1);
            for (int i = 0; i < k; i++) {
                c = c * scalar_t(n - i);
                c = c / scalar_t(i + 1);
            }
            return c;
        }

        scalar_t bernstein(int n, int i, scalar_t t) {
            return biCoefficient(n, i) * pow(t, i) * pow(1.0 - t, n - i);
        }
    };

    //----------------------------------------------------------------
    // Type Definitions:
    //================================================================
    template<typename T>
    using TBezierCurve2D = TBezierCurve<T, glm::detail::tvec2>;

    template<typename T>
    using TBezierCurve3D = TBezierCurve<T, glm::detail::tvec3>;

    typedef TBezierCurve2D<float> BezierCurve2Df;
    typedef TBezierCurve2D<double> BezierCurve2Dd;

    typedef TBezierCurve3D<float> BezierCurve3Df;
    typedef TBezierCurve3D<double> BezierCurve3Dd;

} /* namespace rev */
#endif /* REVBEZIERCURVE_H_ */
