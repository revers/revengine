/*
 * RevTMultiCubicSurface.h
 *
 *  Created on: 24-04-2013
 *      Author: Revers
 */

#ifndef REVMULTICUBICSURFACE_H_
#define REVMULTICUBICSURFACE_H_

#include <vector>
#include <utility>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include "RevISurfacePatch.h"
#include "RevBicubicSurface.h"

namespace rev {

	template<typename T>
	class TMultiCubicSurface: public TSurfacePatch<T> {
	public:
		enum class Direction {
			POSITIVE_X, NEGATIVE_X, POSITIVE_Z, NEGATIVE_Z
		};
		typedef typename TSurfacePatch<T>::scalar_t scalar_t;
		typedef typename TSurfacePatch<T>::vec3_t vec3_t;
		typedef std::pair<vec3_t, vec3_t> vec3_pair;

		int m = 0;
		int n = 0;
		int pointShift = 1;
		scalar_t pointDistance = scalar_t(1);
		std::vector<vec3_t> points;

		TMultiCubicSurface() :
				m(4), n(4) {
			recreate();
		}

		TMultiCubicSurface(int n, int m) :
				m(m), n(n) {
			revAssert(m >= 4);
			revAssert(n >= 4);
			recreate();
		}
		virtual ~TMultiCubicSurface() {
		}

		int getUPointsCount() const override {
			return m;
		}
		int getVPointsCount() const override {
			return n;
		}

		void recalculateSurface() override {
		}

		vec3_t& getPoint(int u, int v) override {
			revAssertMsg(n * u + v < m * n, a2s(R(u), R(v), R(m), R(n)));
			//revAssert(n * u + v < m * n);
			return points[n * u + v];
		}

		/**
		 * Calling this method is a lot faster than calling first eval()
		 * and then normal().
		 */
		vec3_pair evalAndNormal(scalar_t u, scalar_t v) {
			int uSurfaces = m - 3;
			int vSurfaces = n - 3;

			scalar_t uSurfPercent = (1.0 / uSurfaces);
			scalar_t vSurfPercent = (1.0 / vSurfaces);
			int ui = floor(u / uSurfPercent);
			int vi = floor(v / vSurfPercent);

			vec3_t P[4][4] {
					{ getPoint(ui, vi), getPoint(ui, vi + 1),
							getPoint(ui, vi + 2), getPoint(ui, vi + 3) },
					{ getPoint(ui + 1, vi), getPoint(ui + 1, vi + 1),
							getPoint(ui + 1, vi + 2), getPoint(ui + 1, vi + 3) },
					{ getPoint(ui + 2, vi), getPoint(ui + 2, vi + 1),
							getPoint(ui + 2, vi + 2), getPoint(ui + 2, vi + 3) },
					{ getPoint(ui + 3, vi), getPoint(ui + 3, vi + 1),
							getPoint(ui + 3, vi + 2), getPoint(ui + 3, vi + 3) },
			};

			TBicubicSurface<scalar_t> surface(TCubicBaseMatrix<scalar_t>::BSPLINE, P);

			u -= scalar_t(ui) * uSurfPercent;
			v -= scalar_t(vi) * vSurfPercent;
			u /= uSurfPercent;
			v /= vSurfPercent;

			return {surface.eval(u, v), surface.normal(u, v)};
		}

		vec3_t eval(scalar_t u, scalar_t v) {
			int uSurfaces = m - 3;
			int vSurfaces = n - 3;

			scalar_t uSurfPercent = (1.0 / uSurfaces);
			scalar_t vSurfPercent = (1.0 / vSurfaces);
			int ui = floor(u / uSurfPercent);
			int vi = floor(v / vSurfPercent);

			vec3_t P[4][4] {
					{ getPoint(ui, vi), getPoint(ui, vi + 1),
							getPoint(ui, vi + 2), getPoint(ui, vi + 3) },
					{ getPoint(ui + 1, vi), getPoint(ui + 1, vi + 1),
							getPoint(ui + 1, vi + 2), getPoint(ui + 1, vi + 3) },
					{ getPoint(ui + 2, vi), getPoint(ui + 2, vi + 1),
							getPoint(ui + 2, vi + 2), getPoint(ui + 2, vi + 3) },
					{ getPoint(ui + 3, vi), getPoint(ui + 3, vi + 1),
							getPoint(ui + 3, vi + 2), getPoint(ui + 3, vi + 3) },
			};

			TBicubicSurface<scalar_t> surface(TCubicBaseMatrix<scalar_t>::BSPLINE, P);

			u -= scalar_t(ui) * uSurfPercent;
			v -= scalar_t(vi) * vSurfPercent;
			u /= uSurfPercent;
			v /= vSurfPercent;
			// revAssertMsg((u >= 0.0 && u <= 1.0), R(u));
			// revAssertMsg((v >= 0.0 && v <= 1.0), R(v));

			return surface.eval(u, v);
		}

		vec3_t normal(scalar_t u, scalar_t v) {
			int uSurfaces = m - 3;
			int vSurfaces = n - 3;

			scalar_t uSurfPercent = (1.0 / uSurfaces);
			scalar_t vSurfPercent = (1.0 / vSurfaces);
			int ui = floor(u / uSurfPercent);
			int vi = floor(v / vSurfPercent);

			vec3_t P[4][4] {
					{ getPoint(ui, vi), getPoint(ui, vi + 1),
							getPoint(ui, vi + 2), getPoint(ui, vi + 3) },
					{ getPoint(ui + 1, vi), getPoint(ui + 1, vi + 1),
							getPoint(ui + 1, vi + 2), getPoint(ui + 1, vi + 3) },
					{ getPoint(ui + 2, vi), getPoint(ui + 2, vi + 1),
							getPoint(ui + 2, vi + 2), getPoint(ui + 2, vi + 3) },
					{ getPoint(ui + 3, vi), getPoint(ui + 3, vi + 1),
							getPoint(ui + 3, vi + 2), getPoint(ui + 3, vi + 3) },
			};

			TBicubicSurface<scalar_t> surface(TCubicBaseMatrix<scalar_t>::BSPLINE, P);

			u -= scalar_t(ui) * uSurfPercent;
			v -= scalar_t(vi) * vSurfPercent;
			u /= uSurfPercent;
			v /= vSurfPercent;
			// revAssertMsg((u >= 0.0 && u <= 1.0), R(u));
			// revAssertMsg((v >= 0.0 && v <= 1.0), R(v));

			return surface.normal(u, v);
		}

		void resize(Direction direction, bool centralize = true) {
		}

		void shrink(Direction direction, bool centralize = true) {
		}

		void recreate() {
			if (m < 2) {
				m = 2;
			}
			if (n < 2) {
				n = 2;
			}

			points.clear();
			points.resize(m * n, vec3_t(0));

			scalar_t uBegin = -pointDistance * m * 0.5;
			scalar_t wBegin = -pointDistance * n * 0.5;
			;

			for (int u = 0; u < m; u++) {
				scalar_t uValue = uBegin + pointDistance * u;
				for (int w = 0; w < n; w++) {
					scalar_t wValue = wBegin + pointDistance * w;
					getPoint(u, w) = vec3_t(uValue, 0.0, wValue);
				}
			}
		}
	};

	typedef TMultiCubicSurface<float> MultiCubicSurface;

} /* namespace rev */
#endif /* REVMULTICUBICSURFACE_H_ */
