/*
 * RevControlPointPicker.cpp
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */

#include <cfloat>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/collision/primitives/RevSphere.h>
#include "RevControlPointPicker.h"

using namespace glm;
using namespace rev;

float ControlPointPicker::CONTROL_POINT_RADIUS = 0.18f;

ControlPointPicker::UV ControlPointPicker::getControlPoint(int x, int y, int contextIndex) {
	if (!surfacePatch) {
		return UV();
	}
	ICamera* camera = CameraManager::ref().getCamera(contextIndex);
	Ray cameraRay = camera->getRay(x, y);

	int uCount = surfacePatch->getUPointsCount();
	int vCount = surfacePatch->getVPointsCount();

	Sphere sphere(vec3(0), CONTROL_POINT_RADIUS);

	float distance = FLT_MAX;
	UV uv;
	for (int u = 0; u < uCount; u++) {
		for (int v = 0; v < vCount; v++) {
			glm::mat4 m = parent->getModelMatrix();
			sphere.c = mul(m, surfacePatch->getPoint(u, v));

			float d;
			if (sphere.intersect(cameraRay, &d)) {
				if (d < distance) {
					distance = d;
					uv.u = u;
					uv.v = v;
				}
			}
		}
	}

	return uv;
}
