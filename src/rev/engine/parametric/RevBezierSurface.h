/*
 * RevBezierSurface.h
 *
 *  Created on: 20-04-2013
 *      Author: Revers
 */

#ifndef REVBEZIERSURFACE_H_
#define REVBEZIERSURFACE_H_

#include <cmath>
#include <glm/glm.hpp>

namespace rev {

	/**
	 * Unoptimized arbitrary degree Bezier curve.
	 */
	//----------------------------------------------------------------
	// TBezierSurface
	//================================================================
	template<typename ScalarType>
	struct TBezierSurface {
		typedef ScalarType scalar_t;
		typedef glm::detail::tvec3<scalar_t> vec_t;

		/**
		 * Array N*M of points.
		 */
		vec_t* P;
		int M;
		int N;

	public:

		/**
		 * @param points - array M * N of points.
		 * There is M points in "u" direction and
		 * N points in "w" direction
		 */
		TBezierSurface(vec_t* points, int M, int N) :
				P(points), M(M), N(N) {
		}

		vec_t eval(scalar_t u, scalar_t w) {
			vec_t result(0);
			for (int i = 0; i < M; i++) {
				scalar_t b_m = bernstein(M - 1, i, u);
				for (int j = 0; j < N; j++) {
					result += b_m * P[i * N + j] * bernstein(N - 1, j, w);
				}
			}
			return result;
		}

		/**
		 * Surface normal might need to be negated in some shading algorithms,
		 * since I'm using cross(dw, du) instead of cross(du, dw).
		 */
		vec_t normal(scalar_t u, scalar_t w) {
			vec_t du = dtU(u, w);
			vec_t dw = dtW(u, w);
			return glm::normalize(glm::cross(dw, du));
		}

	private:
		/**
		 * http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
		 */
		vec_t dtW(scalar_t u, scalar_t w) {
			vec_t result(0);

			for (int i = 0; i < N - 1; i++) {
				result += (scalar_t(N - 1) * (pointW(i + 1, u) - pointW(i, u)))
						* bernstein(N - 2, i, w);
			}

			return result;
		}

		/**
		 * http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
		 */
		vec_t dtU(scalar_t u, scalar_t w) {
			vec_t result(0);

			for (int i = 0; i < M - 1; i++) {
				result += (scalar_t(M - 1) * (pointU(i + 1, w) - pointU(i, w)))
						* bernstein(M - 2, i, u);
			}

			return result;
		}

		vec_t pointU(int i, scalar_t w) {
			vec_t result(0);
			for (int j = 0; j < N; j++) {
				result += P[i * N + j] * bernstein(N - 1, j, w);
			}
			return result;
		}

		vec_t pointW(int j, scalar_t u) {
			vec_t result(0);
			for (int i = 0; i < M; i++) {
				result += P[i * N + j] * bernstein(M - 1, i, u);
			}
			return result;
		}

		scalar_t biCoefficient(int n, int k) {
			if (k > n - k) {
				k = n - k;
			}

			scalar_t c(1);
			for (int i = 0; i < k; i++) {
				c = c * scalar_t(n - i);
				c = c / scalar_t(i + 1);
			}
			return c;
		}

		scalar_t bernstein(int n, int i, scalar_t t) {
			return biCoefficient(n, i) * pow(t, i) * pow(1.0 - t, n - i);
		}
	};

	typedef TBezierSurface<float> BezierSurfacef;
	typedef TBezierSurface<double> BezierSurfaced;
}

#endif /* REVBEZIERSURFACE_H_ */
