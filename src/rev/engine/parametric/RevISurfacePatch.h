/*
 * RevISurfacePatch.h
 *
 *  Created on: 10-04-2013
 *      Author: Revers
 */

#ifndef REVISURFACEPATCH_H_
#define REVISURFACEPATCH_H_

#include <functional>
#include <glm/glm.hpp>

namespace rev {

	template<typename T>
	class TSurfacePatch {
	protected:
		std::function<void()> pointChangedCallback;

	public:
		typedef T scalar_t;
		typedef glm::detail::tvec3<scalar_t> vec3_t;

		virtual ~TSurfacePatch() {
		}

		virtual int getUPointsCount() const = 0;
		virtual int getVPointsCount() const = 0;
		virtual vec3_t& getPoint(int u, int v) = 0;
		virtual void recalculateSurface() = 0;

		inline const vec3_t& getPoint(int u, int v) const {
			return getPoint(u, v);
		}

		virtual void setPoint(int u, int v, const vec3_t& value) final {
			vec3_t& point = getPoint(u, v);
			point = value;
		}

		std::function<void()> getPointChangedCallback() const {
			return pointChangedCallback;
		}

		void setPointChangedCallback(std::function<void()> pointChangedCallback) {
			this->pointChangedCallback = pointChangedCallback;
		}
	};

	typedef TSurfacePatch<float> ISurfacePatch;

} /* namespace rev */

#endif /* REVISURFACEPATCH_H_ */
