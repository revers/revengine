/*
 * RevControlPointPicker.h
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */

#ifndef REVCONTROLPOINTPICKER_H_
#define REVCONTROLPOINTPICKER_H_

#include "RevISurfacePatch.h"

namespace rev {

	class GameObject;

	class ControlPointPicker {
	public:
		static float CONTROL_POINT_RADIUS;

		struct UV {
			int u;
			int v;

			UV() :
					u(-1), v(-1) {
			}
			UV(int u, int v) :
					u(u), v(v) {
			}
			bool isValid() {
				return u >= 0 && v >= 0;
			}
		};

	private:
		ControlPointPicker(const ControlPointPicker&) = delete;
		ControlPointPicker(ControlPointPicker&&) = delete;
		ControlPointPicker& operator=(const ControlPointPicker&) = delete;

		ISurfacePatch* surfacePatch = nullptr;;
		GameObject* parent = nullptr;

	public:
		ControlPointPicker() :
				surfacePatch(nullptr) {
		}

		/**
		 * ControlPointPicker is not the owner of surfacePatch.
		 */
		ControlPointPicker(ISurfacePatch* surfacePatch) :
				surfacePatch(surfacePatch) {
		}
		~ControlPointPicker() {
		}

		UV getControlPoint(int x, int y, int contextIndex);

		/**
		 * ControlPointPicker is not the owner of surfacePatch.
		 */
		void setSurfacePatch(ISurfacePatch* surfacePatch, GameObject* go) {
			this->surfacePatch = surfacePatch;
			this->parent = go;
		}
		ISurfacePatch* getSurfacePatch() {
			return surfacePatch;
		}
	};

} /* namespace rev */
#endif /* REVCONTROLPOINTPICKER_H_ */
