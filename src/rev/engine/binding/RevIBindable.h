/*
 * RevIBindable.h
 *
 *  Created on: 17-01-2013
 *      Author: Revers
 */

#ifndef REVIBINDABLE_H_
#define REVIBINDABLE_H_

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <v8/v8.h>
#include "RevBindableUtil.h"

namespace rev {

	struct PersistentGuard {
		v8::Persistent<v8::Value> persistent;
		~PersistentGuard() {
			persistent.Dispose();
			persistent.Clear();
		}
	};

	class IBinder;

	class IBindable {
	public:
		virtual ~IBindable() {
		}

		/**
		 * Makes new instance of this bindable, or returns
		 * pointer to this bindable, in case of singleton.
		 *
		 * New instance is created using default constructor
		 * so you might (and should) have to call initAfterDeserialization()
		 * to fully initialize the new object.
		 */
		virtual IBindable* makeBindableInstance() = 0;

		/**
		 * Makes deep copy of this bindable. It will be fully initialized.
		 * In case of initialization failure, the nullptr will be returned.
		 */
		IBindable* makeDeepCopy() {
			return rev::BindableUtil::makeDeepCopyOf(this);
		}

		/**
		 * IMPORTANT: You should override it in a derived class
		 * if initialization given by your default constructor is not
		 * enough for full object initialization.
		 *
		 * VERY IMPORTANT: This should NOT be overriden in singleton bindables.
		 * Macros take care of that.
		 */
		virtual bool initAfterDeserialization() {
			return true;
		}

		/**
		 * Callback method called when created in JavaScript object
		 * is removed (garbage collected) from JS context.
		 *
		 * "Weak" means that object was created in JavaScript.
		 * Objects created in C++ and then wrapped in JavaScript objects,
		 * are considered "not weak", and hence this method will never be
		 * called on them.
		 *
		 * Default implementation (IBindable::weakJSObjectDestroyed())
		 * simply calls "delete this" or does nothing, in case of singleton.
		 * Override it in a subclass if you wish different behavior.
		 */
		virtual void weakJSObjectDestroyed() {
			REV_TRACE_FUNCTION;
			if (!isSingleton()) {
				delete this;
			}
		}

		/**
		 * Do not implement this!
		 * This method is always implemented by macro
		 * DECLARE_BINDABLE_WITH_NAME
		 * or
		 * DECLARE_BINDABLE.
		 */
		virtual const char* getBindableName() const = 0;

		/**
		 * When binder want to notify this bindable, that
		 * value behind the pointer "ptr" was changed,
		 * it have to call this method.
		 * Of course values are changed automatically, but
		 * bindable isn't aware when and if its values are
		 * changed, hence this method.
		 *
		 * Implementing this method is optional.
		 *
		 * @param ptr pointer to changed item.
		 */
		virtual void bindableValueChanged(void* ptr) {
		}

		/**
		 * If this bindable is registered in the BindableFactory,
		 * this function determines whether a new instance of
		 * the bindable will be returned by BindableFactory::get()
		 * or if it will by just a pointer to a singleton object.
		 *
		 * @return true  - this bindable is singleton,
		 *         false - it could be returned by new instance.
		 */
		virtual bool isSingleton() const {
			return false;
		}

		/**
		 * Wrap as JavaScript object.
		 */
		virtual v8::Handle<v8::Value> wrap() = 0;

		virtual void bind(IBinder& binder) {
		}
	};

} // namespace rev

#include "RevIBindableMacros.h"

#endif /* REVIBINDABLE_H_ */
