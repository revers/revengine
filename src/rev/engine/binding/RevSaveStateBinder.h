/*
 * RevStateKeeperBinder.h
 *
 *  Created on: 20-03-2013
 *      Author: Revers
 */

#ifndef REVSAVESTATERBINDER_H_
#define REVSAVESTATERBINDER_H_

#include <string>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

	class SaveStateBinder;
	typedef std::vector<SaveStateBinder*> SaveStateBinderVector;

	class SaveStateBinder: public IBinder {
		IBindable* copyBindable = nullptr;
		IBindable* restoreBindable = nullptr;

		SaveStateBinderVector childBindables;

	public:
		SaveStateBinder(IBindable* bindable);

		virtual ~SaveStateBinder();

		/**
		 * IMPORTANT: This works only once.
		 * After that SaveStateBinder is in invalid state.
		 */
		bool resore(IBindable* targetBindable);

		IBindable* getCopyBindable() {
			return copyBindable;
		}

	private:
		void bindObject(IBindable* bindable) override;
		void unbindObject(IBindable* bindable) override;
		bool recursiveInitBindable(SaveStateBinder* binder);

		template<typename T>
		void copyBasicTypeValue(IBindable* bindable, const char* name,
				MemberWrapper<T> wrapper, bool editable, bool visible, bool expanded) {

			T* copyFromPtr = wrapper.getMemberPointer(bindable);
			T* copyToPtr;
			if (restoreBindable) {
				copyToPtr = wrapper.getMemberPointer(restoreBindable);
			} else {
				copyToPtr = wrapper.getMemberPointer(copyBindable);
			}
			*copyToPtr = *copyFromPtr;
		}

	private:
		// Inherited methods:

		/**
		 * IBindable.
		 */
		virtual void bind(IBindable* bindable, const char* name, MemberWrapper<IBindable*> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * BasicTypeListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name,
				BasicTypeListMemberWrapper wrapper, bool editable,
				bool visible, bool expanded) override;

		/**
		 * BindableListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name, BindableListMemberWrapper wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * filepath (std::string)
		 */
		virtual void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter,
				bool editable, bool visible) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					true);
		}

		/**
		 * int
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<int> wrapper, bool editable,
				bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * float
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<float> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * double
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<double> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * bool
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<bool> wrapper, bool editable,
				bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * std::string
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<std::string> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * quat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::quat> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dquat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dquat> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color3> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color4> wrapper,
				bool editable, bool visible, bool expanded) override {
			copyBasicTypeValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}
	};

} /* namespace rev */
#endif /* REVSAVESTATERBINDER_H_ */
