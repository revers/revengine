/*
 * RevListTypeUtil.h
 *
 *  Created on: 14-03-2013
 *      Author: Revers
 */

#ifndef REVLISTTYPEUTIL_H_
#define REVLISTTYPEUTIL_H_

#include <list>
#include <vector>

namespace rev {

	namespace ListUtil {

		/**
		 * Indexed access to a collection (sequence) class.
		 */
		template<class T, template<class U, class = std::allocator<U>> class Seq>
		T& get(Seq<T>& seq, int index);

		/**
		 * Specialization for std::vector.
		 */
		template<class T>
		inline T& get(std::vector<T>& seq, int index) {
			return seq[index];
		}

		/**
		 * Specialization for std::list.
		 * Be awere that indexed access to a std::list will be
		 * much slower than indexed access to a std::vector.
		 */
		template<class T>
		inline T& get(std::list<T>& seq, int index) {
			typedef typename std::list<T>::iterator Iter;
			Iter it = seq.begin();
			for (int i = 0; i < index; i++) {
				++it;
			}
			return *it;
		}
	} /* namespace ListUtil */

} /* namespace rev */

#endif /* REVLISTTYPEUTIL_H_ */
