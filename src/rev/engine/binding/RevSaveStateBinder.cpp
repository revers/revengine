/*
 * RevStateKeeperBinder.cpp
 *
 *  Created on: 20-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevSaveStateBinder.h"

using namespace rev;

SaveStateBinder::SaveStateBinder(IBindable* bindable) {
	if (bindable) {
		if (bindable->isSingleton()) {
			//copyBindable = bindable;
			revAssertMsg(false, "Singletons are not handled!");
		} else {
			copyBindable = bindable->makeBindableInstance();
			bindObject(bindable);
		}
	}
}

SaveStateBinder::~SaveStateBinder() {
	if (copyBindable) {
		delete copyBindable;
	}
}

bool SaveStateBinder::resore(IBindable* targetBindable) {
	if (!copyBindable) {
		return false;
	}
	bool succ = recursiveInitBindable(this);
	if (!succ) {
		return false;
	}

	restoreBindable = targetBindable;
	copyBindable->bind(*this);
	restoreBindable = nullptr;

	delete copyBindable;
	copyBindable = nullptr;

	return true;
}

void SaveStateBinder::unbindObject(IBindable* bindable) {
//	if (bindable && bindable == baseBindable && !bindable->isSingleton()) {
//		clearAll();
//		IBindable* b = baseBindable;
//		baseBindable = nullptr;
//		b->unbindObject();
//	}
}

void SaveStateBinder::bindObject(IBindable* bindable) {
	if (!bindable) {
		REV_DEBUG_MSG("null bindable");
		return;
	}
	if (bindable->isSingleton()) {
		REV_WARN_MSG("Trying to bind singleton bindable: " << bindable->getBindableName());
		return;
	}
	//unbindObject(baseBindable);
	bindable->bind(*this);
	//baseBindable = bindable;
}

//void SaveStateBinder::clearAll() {
//	for (auto& ptr : childBindables) {
//		delete ptr;
//	}
//
//	childBindables.clear();
//}

void SaveStateBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<IBindable*> wrapper, bool editable, bool visible, bool expanded) {
	if (restoreBindable) {
		IBindable** copyFrom = wrapper.getMemberPointer(bindable);
		IBindable** copyTo = wrapper.getMemberPointer(restoreBindable);
		IBindable* copyToBindable = *copyTo;
		if (copyToBindable && !copyToBindable->isSingleton()) {
			delete copyToBindable;
		}
		*copyTo = *copyFrom;
		*copyFrom = nullptr;
	} else {
		IBindable** copyFrom = wrapper.getMemberPointer(bindable);
		IBindable** copyTo = wrapper.getMemberPointer(copyBindable);
		SaveStateBinder* copyBinder = new SaveStateBinder(*copyFrom);
		*copyTo = copyBinder->getCopyBindable();
		childBindables.push_back(copyBinder);
	}
}

/**
 * BasicTypeListMemberWrapper
 */
void SaveStateBinder::bindList(IBindable* bindable, const char* name,
		BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {

	IBindable* bindableTo;
	if (restoreBindable) {
		bindableTo = restoreBindable;
	} else {
		bindableTo = copyBindable;
	}
	BasicTypeListBinderIterator begin = wrapper.begin(bindable);
	BasicTypeListBinderIterator end = wrapper.end(bindable);
	for (; begin != end; ++begin) {
		wrapper.pushBackFromIter(bindableTo, begin);
	}
}

static void clearBindableList(BindableListMemberWrapper& wrapper, IBindable* owner) {
	BindableListBinderIterator begin = wrapper.begin(owner);
	BindableListBinderIterator end = wrapper.end(owner);
	for (; begin != end; ++begin) {
		IBindable* bindable = begin.getPointer();
		if (bindable && !bindable->isSingleton()) {
			delete bindable;
		}
	}
	wrapper.clear(owner);
}

/**
 * BindableListMemberWrapper
 */
void SaveStateBinder::bindList(IBindable* bindable, const char* name,
		BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {

	if (restoreBindable) {
		clearBindableList(wrapper, bindable);
		BindableListBinderIterator begin = wrapper.begin(bindable);
		BindableListBinderIterator end = wrapper.end(bindable);
		int index = 0;
		for (; begin != end; ++begin) {
			IBindable* member = begin.getPointer();
			wrapper.pushBack(restoreBindable, member);
		}
		wrapper.clear(bindable);
	} else {
		BindableListBinderIterator begin = wrapper.begin(bindable);
		BindableListBinderIterator end = wrapper.end(bindable);
		int index = 0;
		for (; begin != end; ++begin) {
			IBindable* member = begin.getPointer();
			SaveStateBinder* copyBinder = new SaveStateBinder(member);
			childBindables.push_back(copyBinder);

			wrapper.pushBack(copyBindable, copyBinder->getCopyBindable());
		}
	}
}

bool SaveStateBinder::recursiveInitBindable(SaveStateBinder* binder) {
	IBindable* copy = binder->copyBindable;

	SaveStateBinderVector childBindables = binder->childBindables;
	for (SaveStateBinder*& b : childBindables) {
		if (!recursiveInitBindable(b)) {
			return false;
		}
	}
	if (copy) {
		bool succ = copy->initAfterDeserialization();
		if (!succ) {
			REV_ERROR_MSG(copy->getBindableName()
					<< ") -> initAfterDeserialization() FAILED!!");
		}
		return succ;
	}
	return true;
}
//
//void SaveStateBinder::destroyCopyBindable(SaveStateBinder* copyBinder) {
//	IBindable* copy = copyBinder->copyBindable;
//
//	SaveStateBinderVector childBindables = copyBinder->childBindables;
//	for (SaveStateBinder*& b : childBindables) {
//		destroyCopyBindable(b);
//	}
//	if (copy) {
//		if (copy->isSingleton()) {
//			delete copy;
//		}
//		copyBinder->copyBindable = nullptr;
//	}
//}
//
//bool SaveStateBinder::initCopyBindable() {
//	revAssert(copyBindable);
//	return initCopyBindable(this);
//}
//
//void SaveStateBinder::destroyCopyBindable() {
//	destroyCopyBindable(this);
//	copyBindable = nullptr;
//}
