/*
 * RevBindableListMemberBinder.h
 *
 *  Created on: 13-03-2013
 *      Author: Revers
 */

#ifndef REVBINDABLELISTMEMBERWRAPPER_H_
#define REVBINDABLELISTMEMBERWRAPPER_H_

#include <rev/common/RevAssert.h>
#include "RevBindableListBinder.h"

namespace rev {

	class IBindable;

	//----------------------------------------------------------------------------------
	// IBindableListMemberWrapper
	//==================================================================================
	class IBindableListMemberWrapper {
	public:
		virtual ~IBindableListMemberWrapper() {
		}

		virtual const std::string& getName() const = 0;
		virtual BindableListBinderIterator begin(IBindable* owner) = 0;
		virtual BindableListBinderIterator end(IBindable* owner) = 0;
		virtual void pushBack(IBindable* owner, IBindable* bindable) = 0;
		virtual void clear(IBindable* owner) = 0;
		virtual int getSize(IBindable* owner) const = 0;
		virtual v8::Handle<v8::Value> get(IBindable* owner, int index) const = 0;
		virtual BindableListBinder getBinder(IBindable* owner) = 0;
	};

	//----------------------------------------------------------------------------------
	// BindableListMemberWrapper
	//==================================================================================
	/**
	 * This class acts like std::unique_ptr for IBindableListMemberWrapper*,
	 * i.e. you can move it (via std::move()), but not copy it.
	 */
	class BindableListMemberWrapper {
		IBindableListMemberWrapper* binderBase;

		// copy constructor:
		BindableListMemberWrapper(const BindableListMemberWrapper&) = delete;
		// assignment operator:
		BindableListMemberWrapper& operator=(const BindableListMemberWrapper&) = delete;
		// move assignment operator:
		BindableListMemberWrapper& operator=(BindableListMemberWrapper&&) = delete;

	public:
		// move constructor:
		BindableListMemberWrapper(BindableListMemberWrapper&& other) {
			this->binderBase = other.binderBase;
			other.binderBase = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BindableListMemberWrapper(IBindableListMemberWrapper* binderBase) :
				binderBase(binderBase) {
		}

		~BindableListMemberWrapper() {
			if (binderBase) {
				delete binderBase;
			}
		}

		/**
		 * Like the release() from std::unique_ptr.
		 */
		inline IBindableListMemberWrapper* release() {
			IBindableListMemberWrapper* b = binderBase;
			binderBase = nullptr;
			return b;
		}
		inline const std::string& getName() const {
			return binderBase->getName();
		}
		inline BindableListBinderIterator begin(IBindable* owner) {
			return binderBase->begin(owner);
		}
		inline BindableListBinderIterator end(IBindable* owner) {
			return binderBase->end(owner);
		}
		inline void pushBack(IBindable* owner, IBindable* bindable) {
			binderBase->pushBack(owner, bindable);
		}
		inline void clear(IBindable* owner) {
			binderBase->clear(owner);
		}
		inline int getSize(IBindable* owner) const {
			return binderBase->getSize(owner);
		}
		inline v8::Handle<v8::Value> get(IBindable* owner, int index) const {
			return binderBase->get(owner, index);
		}
		inline BindableListBinder getBinder(IBindable* owner) {
			return binderBase->getBinder(owner);
		}
	};

	namespace prv {

		//----------------------------------------------------------------------------------
		// TBindableListMemberWrapper
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		class TBindableListMemberWrapper: public IBindableListMemberWrapper {
			typedef Seq<T*> ListType;
			typedef ListType Bindable::*ListMember;

			ListMember listMember;
			std::string name;

		public:
			TBindableListMemberWrapper(const char* name, ListMember listMember) :
					name(name), listMember(listMember) {
			}

			const std::string& getName() const override {
				return name;
			}

			BindableListBinderIterator begin(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(listRef.begin()));
			}

			BindableListBinderIterator end(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(listRef.end()));
			}

			void pushBack(IBindable* owner, IBindable* bindable) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef.push_back(dynamic_cast<T*>(bindable));
			}

			void clear(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef.clear();
			}

			int getSize(IBindable* owner) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return listRef.size();
			}

			v8::Handle<v8::Value> get(IBindable* owner, int index) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef.size());
				T*& value = ListUtil::get(listRef, index);
				return static_cast<IBindable*>(value)->wrap();
			}

			BindableListBinder getBinder(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinder(
						new prv::TBindableListBinder<T, Seq>(name.c_str(), &listRef));
			}
		};

		//----------------------------------------------------------------------------------
		// TBindableListMemberPtrWrapper
		//==================================================================================
		/**
		 * For use with std::vector* and std::list*:
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		class TBindableListMemberPtrWrapper: public IBindableListMemberWrapper {
			typedef Seq<T*>* ListType;
			typedef ListType Bindable::*ListMember;

			ListMember listMember;
			std::string name;

		public:
			TBindableListMemberPtrWrapper(const char* name, ListMember listMember) :
					name(name), listMember(listMember) {
			}

			const std::string& getName() const override {
				return name;
			}

			BindableListBinderIterator begin(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(listRef->begin()));
			}

			BindableListBinderIterator end(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(listRef->end()));
			}

			void pushBack(IBindable* owner, IBindable* bindable) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef->push_back(dynamic_cast<T*>(bindable));
			}

			void clear(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef->clear();
			}

			int getSize(IBindable* owner) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return listRef->size();
			}

			v8::Handle<v8::Value> get(IBindable* owner, int index) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef->size());
				T*& value = ListUtil::get(*listRef, index);
				return static_cast<IBindable*>(value)->wrap();
			}

			BindableListBinder getBinder(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BindableListBinder(
						new prv::TBindableListBinder<T, Seq>(name.c_str(), listRef));
			}
		};

	} // end namespace prv
}
// namespace rev

#endif /* REVBINDABLELISTMEMBERWRAPPER_H_ */
