/*
 * RevTypeUtil.cpp
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#include <rev/common/RevStringUtil.h>
#include <rev/engine/util/RevCharSetStringTokenizer.h>
#include "RevTypeUtil.h"

using namespace rev;

/**
 * toString<vec2>
 */
template<>
std::string TypeUtil<glm::vec2>::toString(
		const glm::vec2& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y;
	return out.str();
}

/**
 * stringTo<vec2>
 */
template<>
glm::vec2 TypeUtil<glm::vec2>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::vec2 v;
	v.x = TypeUtil<float>::stringTo(tokenizer.next());
	v.y = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<vec3>
 */
template<>
std::string TypeUtil<glm::vec3>::toString(
		const glm::vec3& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z;
	return out.str();
}

/**
 * stringTo<vec3>
 */
template<>
glm::vec3 TypeUtil<glm::vec3>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::vec3 v;
	v.x = TypeUtil<float>::stringTo(tokenizer.next());
	v.y = TypeUtil<float>::stringTo(tokenizer.next());
	v.z = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<vec4>
 */
template<>
std::string TypeUtil<glm::vec4>::toString(
		const glm::vec4& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z << REV_NUMBER_SEPARATOR << val.w;
	return out.str();
}

/**
 * stringTo<vec4>
 */
template<>
glm::vec4 TypeUtil<glm::vec4>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::vec4 v;
	v.x = TypeUtil<float>::stringTo(tokenizer.next());
	v.y = TypeUtil<float>::stringTo(tokenizer.next());
	v.z = TypeUtil<float>::stringTo(tokenizer.next());
	v.w = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<mat2>
 */
template<>
std::string TypeUtil<glm::mat2>::toString(
		const glm::mat2& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1];
	return out.str();
}

/**
 * stringTo<mat2>
 */
template<>
glm::mat2 TypeUtil<glm::mat2>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::mat2 m;
	m[0][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<float>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<mat3>
 */
template<>
std::string TypeUtil<glm::mat3>::toString(
		const glm::mat3& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR
			<< val[2][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1] << REV_NUMBER_SEPARATOR
			<< val[2][1] << REV_NUMBER_SEPARATOR

			<< val[0][2] << REV_NUMBER_SEPARATOR
			<< val[1][2] << REV_NUMBER_SEPARATOR
			<< val[2][2];
	return out.str();
}

/**
 * stringTo<mat3>
 */
template<>
glm::mat3 TypeUtil<glm::mat3>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::mat3 m;
	m[0][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][0] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][1] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][2] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][2] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][2] = TypeUtil<float>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<mat4>
 */
template<>
std::string TypeUtil<glm::mat4>::toString(
		const glm::mat4& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR
			<< val[2][0] << REV_NUMBER_SEPARATOR
			<< val[3][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1] << REV_NUMBER_SEPARATOR
			<< val[2][1] << REV_NUMBER_SEPARATOR
			<< val[3][1] << REV_NUMBER_SEPARATOR

			<< val[0][2] << REV_NUMBER_SEPARATOR
			<< val[1][2] << REV_NUMBER_SEPARATOR
			<< val[2][2] << REV_NUMBER_SEPARATOR
			<< val[3][2] << REV_NUMBER_SEPARATOR

			<< val[0][3] << REV_NUMBER_SEPARATOR
			<< val[1][3] << REV_NUMBER_SEPARATOR
			<< val[2][3] << REV_NUMBER_SEPARATOR
			<< val[3][3];
	return out.str();
}

/**
 * stringTo<mat4>
 */
template<>
glm::mat4 TypeUtil<glm::mat4>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::mat4 m;
	m[0][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][0] = TypeUtil<float>::stringTo(tokenizer.next());
	m[3][0] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][1] = TypeUtil<float>::stringTo(tokenizer.next());
	m[3][1] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][2] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][2] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][2] = TypeUtil<float>::stringTo(tokenizer.next());
	m[3][2] = TypeUtil<float>::stringTo(tokenizer.next());

	m[0][3] = TypeUtil<float>::stringTo(tokenizer.next());
	m[1][3] = TypeUtil<float>::stringTo(tokenizer.next());
	m[2][3] = TypeUtil<float>::stringTo(tokenizer.next());
	m[3][3] = TypeUtil<float>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<quat>
 */
template<>
std::string TypeUtil<glm::quat>::toString(
		const glm::quat& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z << REV_NUMBER_SEPARATOR << val.w;
	return out.str();
}

/**
 * stringTo<quat>
 */
template<>
glm::quat TypeUtil<glm::quat>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::quat v;
	v.x = TypeUtil<float>::stringTo(tokenizer.next());
	v.y = TypeUtil<float>::stringTo(tokenizer.next());
	v.z = TypeUtil<float>::stringTo(tokenizer.next());
	v.w = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<color3>
 */
template<>
std::string TypeUtil<rev::color3>::toString(
		const rev::color3& val) {
	std::ostringstream out;
	out << val.r << REV_NUMBER_SEPARATOR << val.g << REV_NUMBER_SEPARATOR
			<< val.b;
	return out.str();
}

/**
 * stringTo<color3>
 */
template<>
rev::color3 TypeUtil<rev::color3>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	rev::color3 v;
	v.r = TypeUtil<float>::stringTo(tokenizer.next());
	v.g = TypeUtil<float>::stringTo(tokenizer.next());
	v.b = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<color4>
 */
template<>
std::string TypeUtil<rev::color4>::toString(
		const rev::color4& val) {
	std::ostringstream out;
	out << val.r << REV_NUMBER_SEPARATOR << val.g << REV_NUMBER_SEPARATOR
			<< val.b << REV_NUMBER_SEPARATOR << val.a;
	return out.str();
}

/**
 * stringTo<color4>
 */
template<>
rev::color4 TypeUtil<rev::color4>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	rev::color4 v;
	v.r = TypeUtil<float>::stringTo(tokenizer.next());
	v.g = TypeUtil<float>::stringTo(tokenizer.next());
	v.b = TypeUtil<float>::stringTo(tokenizer.next());
	v.a = TypeUtil<float>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<dvec2>
 */
template<>
std::string TypeUtil<glm::dvec2>::toString(
		const glm::dvec2& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y;
	return out.str();
}

/**
 * stringTo<dvec2>
 */
template<>
glm::dvec2 TypeUtil<glm::dvec2>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dvec2 v;
	v.x = TypeUtil<double>::stringTo(tokenizer.next());
	v.y = TypeUtil<double>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<dvec3>
 */
template<>
std::string TypeUtil<glm::dvec3>::toString(
		const glm::dvec3& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z;
	return out.str();
}

/**
 * stringTo<dvec3>
 */
template<>
glm::dvec3 TypeUtil<glm::dvec3>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dvec3 v;
	v.x = TypeUtil<double>::stringTo(tokenizer.next());
	v.y = TypeUtil<double>::stringTo(tokenizer.next());
	v.z = TypeUtil<double>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<dvec4>
 */
template<>
std::string TypeUtil<glm::dvec4>::toString(
		const glm::dvec4& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z << REV_NUMBER_SEPARATOR << val.w;
	return out.str();
}

/**
 * stringTo<dvec4>
 */
template<>
glm::dvec4 TypeUtil<glm::dvec4>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dvec4 v;
	v.x = TypeUtil<double>::stringTo(tokenizer.next());
	v.y = TypeUtil<double>::stringTo(tokenizer.next());
	v.z = TypeUtil<double>::stringTo(tokenizer.next());
	v.w = TypeUtil<double>::stringTo(tokenizer.next());
	return v;
}

/**
 * toString<dmat2>
 */
template<>
std::string TypeUtil<glm::dmat2>::toString(
		const glm::dmat2& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1];
	return out.str();
}

/**
 * stringTo<dmat2>
 */
template<>
glm::dmat2 TypeUtil<glm::dmat2>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dmat2 m;
	m[0][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<double>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<dmat3>
 */
template<>
std::string TypeUtil<glm::dmat3>::toString(
		const glm::dmat3& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR
			<< val[2][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1] << REV_NUMBER_SEPARATOR
			<< val[2][1] << REV_NUMBER_SEPARATOR

			<< val[0][2] << REV_NUMBER_SEPARATOR
			<< val[1][2] << REV_NUMBER_SEPARATOR
			<< val[2][2];
	return out.str();
}

/**
 * stringTo<dmat3>
 */
template<>
glm::dmat3 TypeUtil<glm::dmat3>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dmat3 m;
	m[0][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][0] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][1] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][2] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][2] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][2] = TypeUtil<double>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<dmat4>
 */
template<>
std::string TypeUtil<glm::dmat4>::toString(
		const glm::dmat4& val) {
	std::ostringstream out;
	out << val[0][0] << REV_NUMBER_SEPARATOR
			<< val[1][0] << REV_NUMBER_SEPARATOR
			<< val[2][0] << REV_NUMBER_SEPARATOR
			<< val[3][0] << REV_NUMBER_SEPARATOR

			<< val[0][1] << REV_NUMBER_SEPARATOR
			<< val[1][1] << REV_NUMBER_SEPARATOR
			<< val[2][1] << REV_NUMBER_SEPARATOR
			<< val[3][1] << REV_NUMBER_SEPARATOR

			<< val[0][2] << REV_NUMBER_SEPARATOR
			<< val[1][2] << REV_NUMBER_SEPARATOR
			<< val[2][2] << REV_NUMBER_SEPARATOR
			<< val[3][2] << REV_NUMBER_SEPARATOR

			<< val[0][3] << REV_NUMBER_SEPARATOR
			<< val[1][3] << REV_NUMBER_SEPARATOR
			<< val[2][3] << REV_NUMBER_SEPARATOR
			<< val[3][3];
	return out.str();
}

/**
 * stringTo<dmat4>
 */
template<>
glm::dmat4 TypeUtil<glm::dmat4>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dmat4 m;
	m[0][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][0] = TypeUtil<double>::stringTo(tokenizer.next());
	m[3][0] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][1] = TypeUtil<double>::stringTo(tokenizer.next());
	m[3][1] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][2] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][2] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][2] = TypeUtil<double>::stringTo(tokenizer.next());
	m[3][2] = TypeUtil<double>::stringTo(tokenizer.next());

	m[0][3] = TypeUtil<double>::stringTo(tokenizer.next());
	m[1][3] = TypeUtil<double>::stringTo(tokenizer.next());
	m[2][3] = TypeUtil<double>::stringTo(tokenizer.next());
	m[3][3] = TypeUtil<double>::stringTo(tokenizer.next());
	return m;
}

/**
 * toString<dquat>
 */
template<>
std::string TypeUtil<glm::dquat>::toString(
		const glm::dquat& val) {
	std::ostringstream out;
	out << val.x << REV_NUMBER_SEPARATOR << val.y << REV_NUMBER_SEPARATOR
			<< val.z << REV_NUMBER_SEPARATOR << val.w;
	return out.str();
}

/**
 * stringTo<dquat>
 */
template<>
glm::dquat TypeUtil<glm::dquat>::stringTo(
		const std::string& str) {
	CharSetStringTokenizer tokenizer(
			StringUtil::removeWhitespaces(str), REV_NUMBER_SEPARATOR);

	glm::dquat v;
	v.x = TypeUtil<double>::stringTo(tokenizer.next());
	v.y = TypeUtil<double>::stringTo(tokenizer.next());
	v.z = TypeUtil<double>::stringTo(tokenizer.next());
	v.w = TypeUtil<double>::stringTo(tokenizer.next());
	return v;
}
