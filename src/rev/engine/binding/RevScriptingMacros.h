/*
 * RevScriptingMacros.h
 *
 *  Created on: 03-03-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTINGMACROS_H_
#define REVSCRIPTINGMACROS_H_

#include <v8/v8.h>
#include <rev/engine/scripting/RevScriptObjectBinder.h>
#include "RevScriptingBasis.h"

#define DECLARE_SCRIPTING_METHODS(className) \
    private: \
        static rev::ScriptObjectBinder objectBinder; \
        static v8::Persistent<v8::FunctionTemplate> funcTemplate; \
    private: \
        static v8::Handle<v8::FunctionTemplate> makeTemplate(); \
        static void weakJSObjectCallback(v8::Persistent<v8::Value> object, void* parameter); \
        static v8::Handle<v8::Value> constructorCall(const v8::Arguments& args); \
    public: \
        static v8::Handle<v8::Value> wrap(className* v); \
        static className* unwrap(v8::Handle<v8::Value>& val); \
        static v8::Persistent<v8::FunctionTemplate>& getFunctionTemplate(); \
        v8::Handle<v8::Value> wrap() override {\
        	return wrap(this);\
        }

//==========================================================================================

#define IMPLEMENT_SCRIPTING_METHODS(className) \
	rev::ScriptObjectBinder className::objectBinder; \
	v8::Persistent<v8::FunctionTemplate> className::funcTemplate; \
	\
	className* className::unwrap(v8::Handle<v8::Value>& val) { \
		  return static_cast<className*>(ScriptingBasis::unwrap(val)); \
	} \
	v8::Handle<v8::Value> className::wrap(className* v) { \
		return ScriptingBasis::wrap(v, className::getFunctionTemplate); \
	} \
	v8::Handle<v8::Value> className::constructorCall(const v8::Arguments& args) { \
		return ScriptingBasis::constructorCall(className::staticCreateBindable, \
				className::staticGetClassName(), args);\
	} \
	v8::Handle<v8::FunctionTemplate> className::makeTemplate() { \
		return ScriptingBasis::makeTemplate(className::staticCreateBindable, \
				className::staticGetClassName(), objectBinder, \
				className::constructorCall); \
	} \
	v8::Persistent<v8::FunctionTemplate>& className::getFunctionTemplate() { \
		if (funcTemplate.IsEmpty()) { \
			v8::HandleScope handleScope; \
			funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate()); \
		} \
		return funcTemplate; \
	}

#endif /* REVSCRIPTINGMACROS_H_ */
