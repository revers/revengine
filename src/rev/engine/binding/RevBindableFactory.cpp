/*
 * RevBindableFactory.cpp
 *
 *  Created on: 19-01-2013
 *      Author: Revers
 */

#include "RevBindableFactory.h"
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

#include "RevIBindable.h"

using namespace rev;

BindableFactory* BindableFactory::getInstance() {
	static BindableFactory registry;

	return &registry;
}

void BindableFactory::registerFactory(const char* name,
		BindableFactoryFunc factoryFunc,
		GetFunctionTemplateFunc templateFunc,
		ScriptingInitFunc scriptingInitFunc,
		bool isSingleton) {
	REV_TRACE_MSG("Registering '" << name << "' factory...");

	assertMsg(bindableStructMap.find(name) == bindableStructMap.end(),
			"Factory already registered: '" << name << "'!");

	bindableStructMap[name] = {factoryFunc, templateFunc, scriptingInitFunc, isSingleton};
}

BindableFactoryFunc BindableFactory::getFactory(const char* name) {
	assertMsg(bindableStructMap.find(name) != bindableStructMap.end(),
			"There is no such factory: '" << name << "'!");

	if (bindableStructMap.find(name) == bindableStructMap.end()) {
		return nullptr;
	}

	return bindableStructMap[name].factoryFunc;
}

IBindable* BindableFactory::get(const char* name) {
	BindableFactoryFunc func = getFactory(name);
	if (!func) {
		return nullptr;
	}

	return func();
}
