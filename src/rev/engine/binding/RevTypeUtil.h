/*
 * RevTypeUtil.h
 *
 *  Created on: 05-02-2013
 *      Author: Revers
 */

#ifndef REVTYPEUTIL_H_
#define REVTYPEUTIL_H_

#include <string>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <rev/gl/RevColor.h>

#define REV_NUMBER_SEPARATOR ","
#define REV_ELEMENT_SEPARATOR "|"

namespace rev {

	template<typename T>
	class TypeUtil {
		TypeUtil() = delete;
		TypeUtil(const TypeUtil&) = delete;

	public:

		static const char* getName();

		static std::string toString(const T& val) {
			std::ostringstream out;
			out << val;
			return out.str();
		}

		static T stringTo(const std::string& str) {
			T output;
			std::istringstream in(str);
			in >> output;
			return output;
		}

		static const std::type_info& getTypeInfo() {
			return typeid(T);
		}
	};

	template<>
	inline const char* TypeUtil<int>::getName() {
		return "int";
	}
	template<>
	inline const char* TypeUtil<float>::getName() {
		return "float";
	}
	template<>
	inline const char* TypeUtil<double>::getName() {
		return "double";
	}
	template<>
	inline const char* TypeUtil<bool>::getName() {
		return "bool";
	}
	template<>
	inline const char* TypeUtil<std::string>::getName() {
		return "string";
	}
	template<>
	inline const char* TypeUtil<glm::vec2>::getName() {
		return "vec2";
	}
	template<>
	inline const char* TypeUtil<glm::vec3>::getName() {
		return "vec3";
	}
	template<>
	inline const char* TypeUtil<glm::vec4>::getName() {
		return "vec4";
	}
	template<>
	inline const char* TypeUtil<glm::mat2>::getName() {
		return "mat2";
	}
	template<>
	inline const char* TypeUtil<glm::mat3>::getName() {
		return "mat3";
	}
	template<>
	inline const char* TypeUtil<glm::mat4>::getName() {
		return "mat4";
	}
	template<>
	inline const char* TypeUtil<glm::quat>::getName() {
		return "quat";
	}
	template<>
	inline const char* TypeUtil<rev::color3>::getName() {
		return "color3";
	}
	template<>
	inline const char* TypeUtil<rev::color4>::getName() {
		return "color4";
	}
	template<>
	inline const char* TypeUtil<glm::dvec2>::getName() {
		return "dvec2";
	}
	template<>
	inline const char* TypeUtil<glm::dvec3>::getName() {
		return "dvec3";
	}
	template<>
	inline const char* TypeUtil<glm::dvec4>::getName() {
		return "dvec4";
	}
	template<>
	inline const char* TypeUtil<glm::dmat2>::getName() {
		return "dmat2";
	}
	template<>
	inline const char* TypeUtil<glm::dmat3>::getName() {
		return "dmat3";
	}
	template<>
	inline const char* TypeUtil<glm::dmat4>::getName() {
		return "dmat4";
	}
	template<>
	inline const char* TypeUtil<glm::dquat>::getName() {
		return "dquat";
	}

	/**
	 * toString<std::string>
	 */
	template<>
	inline std::string TypeUtil<std::string>::toString(const std::string& val) {
		return val;
	}

	/**
	 * stringTo<std::string>
	 */
	template<>
	inline std::string TypeUtil<std::string>::stringTo(const std::string& str) {
		return str;
	}

	/**
	 * toString<bool>
	 */
	template<>
	inline std::string TypeUtil<bool>::toString(const bool& val) {
		return val ? "true" : "false";
	}

	/**
	 * stringTo<bool>
	 */
	template<>
	inline bool TypeUtil<bool>::stringTo(const std::string& str) {
		return str == "true" ? true : false;
	}

	/**
	 * toString<vec2>
	 */
	template<>
	std::string TypeUtil<glm::vec2>::toString(const glm::vec2& val);

	/**
	 * stringTo<vec2>
	 */
	template<>
	glm::vec2 TypeUtil<glm::vec2>::stringTo(const std::string& str);

	/**
	 * toString<vec3>
	 */
	template<>
	std::string TypeUtil<glm::vec3>::toString(const glm::vec3& val);

	/**
	 * stringTo<vec3>
	 */
	template<>
	glm::vec3 TypeUtil<glm::vec3>::stringTo(const std::string& str);

	/**
	 * toString<vec4>
	 */
	template<>
	std::string TypeUtil<glm::vec4>::toString(const glm::vec4& val);

	/**
	 * stringTo<vec4>
	 */
	template<>
	glm::vec4 TypeUtil<glm::vec4>::stringTo(const std::string& str);

	/**
	 * toString<mat2>
	 */
	template<>
	std::string TypeUtil<glm::mat2>::toString(const glm::mat2& val);

	/**
	 * stringTo<mat2>
	 */
	template<>
	glm::mat2 TypeUtil<glm::mat2>::stringTo(const std::string& str);

	/**
	 * toString<mat3>
	 */
	template<>
	std::string TypeUtil<glm::mat3>::toString(const glm::mat3& val);

	/**
	 * stringTo<mat3>
	 */
	template<>
	glm::mat3 TypeUtil<glm::mat3>::stringTo(const std::string& str);

	/**
	 * toString<mat4>
	 */
	template<>
	std::string TypeUtil<glm::mat4>::toString(const glm::mat4& val);

	/**
	 * stringTo<mat4>
	 */
	template<>
	glm::mat4 TypeUtil<glm::mat4>::stringTo(const std::string& str);

	/**
	 * toString<quat>
	 */
	template<>
	std::string TypeUtil<glm::quat>::toString(const glm::quat& val);

	/**
	 * stringTo<quat>
	 */
	template<>
	glm::quat TypeUtil<glm::quat>::stringTo(const std::string& str);

	/**
	 * toString<dvec2>
	 */
	template<>
	std::string TypeUtil<glm::dvec2>::toString(const glm::dvec2& val);

	/**
	 * stringTo<dvec2>
	 */
	template<>
	glm::dvec2 TypeUtil<glm::dvec2>::stringTo(const std::string& str);

	/**
	 * toString<dvec3>
	 */
	template<>
	std::string TypeUtil<glm::dvec3>::toString(const glm::dvec3& val);

	/**
	 * stringTo<dvec3>
	 */
	template<>
	glm::dvec3 TypeUtil<glm::dvec3>::stringTo(const std::string& str);

	/**
	 * toString<dvec4>
	 */
	template<>
	std::string TypeUtil<glm::dvec4>::toString(const glm::dvec4& val);

	/**
	 * stringTo<dvec4>
	 */
	template<>
	glm::dvec4 TypeUtil<glm::dvec4>::stringTo(const std::string& str);

	/**
	 * toString<dmat2>
	 */
	template<>
	std::string TypeUtil<glm::dmat2>::toString(const glm::dmat2& val);

	/**
	 * stringTo<dmat2>
	 */
	template<>
	glm::dmat2 TypeUtil<glm::dmat2>::stringTo(const std::string& str);

	/**
	 * toString<dmat3>
	 */
	template<>
	std::string TypeUtil<glm::dmat3>::toString(const glm::dmat3& val);

	/**
	 * stringTo<dmat3>
	 */
	template<>
	glm::dmat3 TypeUtil<glm::dmat3>::stringTo(const std::string& str);

	/**
	 * toString<dmat4>
	 */
	template<>
	std::string TypeUtil<glm::dmat4>::toString(const glm::dmat4& val);

	/**
	 * stringTo<dmat4>
	 */
	template<>
	glm::dmat4 TypeUtil<glm::dmat4>::stringTo(const std::string& str);

	/**
	 * toString<dquat>
	 */
	template<>
	std::string TypeUtil<glm::dquat>::toString(const glm::dquat& val);

	/**
	 * stringTo<dquat>
	 */
	template<>
	glm::dquat TypeUtil<glm::dquat>::stringTo(const std::string& str);

	/**
	 * toString<color3>
	 */
	template<>
	std::string TypeUtil<rev::color3>::toString(const rev::color3& val);

	/**
	 * stringTo<color3>
	 */
	template<>
	rev::color3 TypeUtil<rev::color3>::stringTo(const std::string& str);

	/**
	 * toString<color4>
	 */
	template<>
	std::string TypeUtil<rev::color4>::toString(const rev::color4& val);

	/**
	 * stringTo<color4>
	 */
	template<>
	rev::color4 TypeUtil<rev::color4>::stringTo(const std::string& str);

} // namespace rev

#endif /* REVTYPEUTIL_H_ */
