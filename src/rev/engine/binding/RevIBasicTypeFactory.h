/*
 * RevIBasicTypeFactory.h
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#ifndef REVIBASICTYPEFACTORY_H_
#define REVIBASICTYPEFACTORY_H_

#include <string>
#include <typeinfo>

namespace rev {
    class IBasicTypeFactory {
    public:
        virtual ~IBasicTypeFactory() {
        }
        virtual std::string getType() const = 0;
        virtual const std::type_info& getTypeInfo() const = 0;
        virtual size_t getSizeOf() const = 0;
        virtual std::string toString(const void* ptr) const = 0;
        virtual void stringTo(const std::string& str, void* targetPtr) const = 0;
        virtual void copy(const void* src, void* dest) const = 0;
    };
}

#endif /* REVIBASICTYPEFACTORY_H_ */
