/*
 * RevTBasicTypeFactory.h
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#ifndef REVTBASICTYPEFACTORY_H_
#define REVTBASICTYPEFACTORY_H_

#include "RevTypeUtil.h"
#include "RevIBasicTypeFactory.h"
#include <rev/engine/scripting/RevScriptType.h>

namespace rev {

	template<typename T>
	class TBasicTypeFactory: public IBasicTypeFactory {
	public:
		size_t getSizeOf() const override {
			return sizeof(T);
		}

		std::string getType() const override {
			return std::string(TypeUtil<T>::getName());
		}

		const std::type_info& getTypeInfo() const override {
			return typeid(T);
		}

		std::string toString(const void* ptr) const override {
			return rev::TypeUtil<T>::toString(*static_cast<const T*>(ptr));
		}

		void stringTo(const std::string& str, void* targetPtr) const override {
			*static_cast<T*>(targetPtr) = rev::TypeUtil<T>::stringTo(str);
		}

		void copy(const void* src, void* dest) const override {
			*static_cast<T*>(dest) = *static_cast<const T*>(src);
		}

		T castFromJS(const v8::Handle<v8::Value>& value) {
			return rev::ScriptType<T>::castFromJS(value);
		}

		v8::Handle<v8::Value> castToJS(const T& value) {
			return rev::ScriptType<T>::castToJS(value);
		}
	};

} // namespace rev

#endif /* REVTBASICTYPEFACTORY_H_ */
