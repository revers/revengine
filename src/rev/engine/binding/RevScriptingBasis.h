/*
 * RevScriptingBasis.h
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTINGBASIS_H_
#define REVSCRIPTINGBASIS_H_

#include <v8/v8.h>

namespace rev {

	class IBindable;
	class ScriptObjectBinder;
	typedef IBindable* (*BindableFactoryFunc)();
	typedef v8::Persistent<v8::FunctionTemplate>& (*FuncTemplateFactory)();

	/**
	 * This class moves away logic from "RevScriptingMacros.h".
	 */
	class ScriptingBasis {
		ScriptingBasis() = delete;
		ScriptingBasis(const ScriptingBasis&) = delete;
		~ScriptingBasis() = delete;

	public:
		static v8::Handle<v8::FunctionTemplate> makeTemplate(
				BindableFactoryFunc factoryFunc,
				const char* bindableName,
				rev::ScriptObjectBinder& objectBinder,
				v8::InvocationCallback callback);

		static void weakJSObjectCallback(v8::Persistent<v8::Value> object,
				void* parameter);

		static v8::Handle<v8::Value> constructorCall(
				BindableFactoryFunc factoryFunc,
				const char* bindableName,
				const v8::Arguments& args);

		static v8::Handle<v8::Value> wrap(IBindable* bindable,
				FuncTemplateFactory funcTemplateFactory);

		static IBindable* unwrap(v8::Handle<v8::Value>& val);

	};

} /* namespace rev */
#endif /* REVSCRIPTINGBASIS_H_ */
