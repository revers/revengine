/*
 * RevIBindableMacros.h
 *
 *  Created on: 07-03-2013
 *      Author: Revers
 */

#ifndef REVIBINDABLEMACROS_H_
#define REVIBINDABLEMACROS_H_

#include "RevBindableFactory.h"
#include "RevScriptingMacros.h"

//=====================================================================================

#define DECLARE_BINDABLE(className) \
    static const char* staticGetClassName(); \
    static rev::IBindable* staticCreateBindable(); \
    const char* getClassName() { \
        return className::staticGetClassName(); \
    } \
    const char* getBindableName() const override { \
        return staticGetClassName(); \
    } \
    IBindable* makeBindableInstance() override {\
    	return className::staticCreateBindable();\
    }\
    DECLARE_SCRIPTING_METHODS(className)

//=====================================================================================

#define DECLARE_WEAK_BINDABLE(className) \
	IBindable* makeBindableInstance() override { \
		return nullptr; \
	} \
	const char* getBindableName() const override { \
		return #className; \
	} \
	v8::Handle<v8::Value> wrap() override { \
		return v8::Undefined(); \
	}

//=====================================================================================

#define IMPLEMENT_BINDABLE_FULL(className, scriptingInitFunc) \
    const char* className::staticGetClassName() { \
        static const char* name_ = #className; \
        return name_; \
    } \
    rev::IBindable* className::staticCreateBindable() { \
        return new className(); \
    } \
    rev::BindableFactoryRegisterObject regObject_##className ( \
                            #className, \
                            className::staticCreateBindable, \
                            className::getFunctionTemplate, \
                            scriptingInitFunc, \
                            false);\
    IMPLEMENT_SCRIPTING_METHODS(className)

//=====================================================================================

/**
 * In case of that macro, you must additionally provide definition
 * of className::staticCreateBindable() (somewhere outside the macro).
 *
 * If you don't want to provide "scriptingInitFunc", use "nullptr" as the argument,
 * like IMPLEMENT_BINDABLE_WITHOUT_CREATE(MyClass, nullptr)
 */
#define IMPLEMENT_BINDABLE_WITHOUT_CREATE(className, scriptingInitFunc) \
    const char* className::staticGetClassName() { \
        return #className; \
    } \
    rev::BindableFactoryRegisterObject regObject_##className ( \
                            #className, \
                            className::staticCreateBindable, \
                            className::getFunctionTemplate, \
                            scriptingInitFunc, \
                            false);\
    IMPLEMENT_SCRIPTING_METHODS(className)

//=====================================================================================

#define IMPLEMENT_BINDABLE(className) \
	IMPLEMENT_BINDABLE_FULL(className, nullptr)

//=====================================================================================

/**
 * For use with SINGLETON bindables:
 */
#define DECLARE_BINDABLE_SINGLETON(className) \
        static const char* staticGetClassName(); \
        static rev::IBindable* staticCreateBindable(); \
        const char* getClassName() { \
            return className::staticGetClassName(); \
        } \
        const char* getBindableName() const override { \
            return staticGetClassName(); \
        } \
        bool isSingleton() const override { \
            return true; \
        } \
        IBindable* makeBindableInstance() override {\
        	return className::staticCreateBindable();\
        }\
        bool initAfterDeserialization() override { \
        	return true; \
        } \
        DECLARE_SCRIPTING_METHODS(className)

//=====================================================================================

/**
 * For use with SINGLETON bindables.
 * The second parameter is a return statement for  staticCreateBindable().
 * It should be a pointer to the singleton bindable.
 */
#define IMPLEMENT_BINDABLE_SINGLETON_FULL(className, returnStatement, scriptingInitFunc) \
    const char* className::staticGetClassName() { \
        return #className; \
    } \
    rev::IBindable* className::staticCreateBindable() { \
            return returnStatement; \
    } \
    rev::BindableFactoryRegisterObject regObject_##className ( \
                                #className, \
                                className::staticCreateBindable, \
                                className::getFunctionTemplate, \
                                scriptingInitFunc, \
                                true);\
    IMPLEMENT_SCRIPTING_METHODS(className)

//=====================================================================================

#define IMPLEMENT_BINDABLE_SINGLETON(className) \
	IMPLEMENT_BINDABLE_SINGLETON_FULL(className, className::getInstance(), nullptr)

//=====================================================================================


#endif /* REVIBINDABLEMACROS_H_ */
