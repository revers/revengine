/*
 * RevBindableFactory.h
 *
 *  Created on: 19-01-2013
 *      Author: Revers
 */

#ifndef REVBINDABLEFACTORY_H_
#define REVBINDABLEFACTORY_H_

#include <map>
#include <string>
#include <v8/v8.h>
#include "RevIBindable.h"

namespace rev {

	class IBindable;
	typedef IBindable* (*BindableFactoryFunc)();
	typedef v8::Persistent<v8::FunctionTemplate>& (*GetFunctionTemplateFunc)();
	typedef void (*ScriptingInitFunc)();

	//------------------------------------------------------
	// BindableStruct
	//------------------------------------------------------
	struct BindableStruct {
		BindableFactoryFunc factoryFunc;
		GetFunctionTemplateFunc templateFunc;
		ScriptingInitFunc scriptingInitFunc;
		bool isSingleton;
	};

	typedef std::map<std::string, BindableStruct> BindableStructMap;
	typedef BindableStructMap::iterator BindableStructMapIter;

	//------------------------------------------------------
	// BindableFactory
	//------------------------------------------------------
	class BindableFactory {
		BindableStructMap bindableStructMap;

		BindableFactory() {
		}
		BindableFactory(const BindableFactory& orig) = delete;
		BindableFactory& operator=(const BindableFactory&) = delete;

	public:
		static BindableFactory* getInstance();

		static BindableFactory& ref() {
			return *getInstance();
		}

		~BindableFactory() {
		}

		void registerFactory(const char* name,
				BindableFactoryFunc factoryFunc,
				GetFunctionTemplateFunc templateFunc,
				ScriptingInitFunc scriptingInitFunc,
				bool isSingleton);

		BindableStructMap& getBindableStructMap() {
			return bindableStructMap;
		}

		/**
		 * Creates new bindable or, in case of singleton,
		 * returns pointer to an existing bindable. (This depends of
		 * how given requested was registered).
		 *
		 * You can check if a particular bindable is singleton or not
		 * by IBindable::isSingleton();
		 *
		 * IMPORTANT: Never delete an instance given by this function
		 * by operator "delete"! Use BindableFactory::free() instead.
		 * This is necesessry since you can get non-deleteable (by design)
		 * sigleton object.
		 *
		 * @return new instance of requested bindable, or pointer to an existing
		 * singleton-bindable.
		 */
		IBindable* get(const char* name);

		/**
		 * Always delete bindable obtained by BindableFactory::get()
		 * using this function - never by operator "delete" or C's free().
		 *
		 * This function (apart from deletion) always set 'bindable' argument
		 * to nullptr.
		 */
		template<typename BindableType>
		void free(BindableType*& bindable) {
			if (!bindable->isSingleton()) {
				delete bindable;
			}
			bindable = nullptr;
		}

	private:
		BindableFactoryFunc getFactory(const char* name);

	};

	//------------------------------------------------------
	// BindableFactoryRegisterObject
	//------------------------------------------------------
	class BindableFactoryRegisterObject {
	public:
		BindableFactoryRegisterObject(const char* name,
				BindableFactoryFunc factoryFunc,
				GetFunctionTemplateFunc templateFunc,
				ScriptingInitFunc scriptingInitFunc,
				bool isSingleton) {
			BindableFactory::ref().registerFactory(name, factoryFunc, templateFunc,
					scriptingInitFunc, isSingleton);
		}
	};

} // end namespace rev
#endif /* REVBINDABLEFACTORY_H_ */
