/*
 * RevScriptingBasis.cpp
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevIBindable.h"
#include <rev/engine/scripting/RevScriptObjectBinder.h>
#include "RevScriptingBasis.h"

using namespace rev;
using namespace v8;

IBindable* rev::ScriptingBasis::unwrap(v8::Handle<v8::Value>& val) {
	Handle<External> field = Handle<External>::Cast(
			Handle<Object>::Cast(val)->GetInternalField(0));
	void* ptr = field->Value();

	return static_cast<IBindable*>(ptr);
}

v8::Handle<v8::Value> rev::ScriptingBasis::wrap(IBindable* bindable,
		FuncTemplateFactory funcTemplateFactory) {
	HandleScope handleScope;

	v8::Handle<Value> external = v8::External::New(bindable);
	v8::Handle<Value> result = funcTemplateFactory()->GetFunction()->NewInstance(
			1, &external);

	return handleScope.Close(result);
}

v8::Handle<v8::Value> rev::ScriptingBasis::constructorCall(
		BindableFactoryFunc factoryFunc,
		const char* bindableName,
		const v8::Arguments& args) {
	if (!args.IsConstructCall()) {
		std::string msg("Cannot call constructor of class '");
		msg += bindableName;
		msg += "' as function!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg.c_str()));
	}

	HandleScope scope;
	Local<Value> external;
	if (args[0]->IsExternal()) {
		external = args[0];
		args.This()->SetInternalField(0, external);
	} else {
		IBindable* bindable = factoryFunc();
		bool succ = bindable->initAfterDeserialization();
		revAssert(succ);
		external = External::New(bindable);
		args.This()->SetInternalField(0, external);

		Persistent<Object> persistent = Persistent<Object>::New(args.This());
		persistent.MakeWeak(bindable, weakJSObjectCallback);
	}

	return args.This();
}

v8::Handle<v8::FunctionTemplate> rev::ScriptingBasis::makeTemplate(
		BindableFactoryFunc factoryFunc,
		const char* bindableName,
		ScriptObjectBinder& objectBinder,
		v8::InvocationCallback callback) {
	HandleScope handleScope;
	IBindable* dummyBindable = factoryFunc();
	objectBinder.bindObject(dummyBindable);
	objectBinder.unbindWithoutClear();

	if (!dummyBindable->isSingleton()) {
		delete dummyBindable;
	}

	Handle<FunctionTemplate> templ = FunctionTemplate::New(callback);
	templ->SetClassName(String::New(bindableName));
	Handle<ObjectTemplate> instance = templ->InstanceTemplate();
	instance->SetInternalFieldCount(1);

	for (ScriptPropertyAccessor*& a : objectBinder.getChildProperties()) {
		Handle<Value> data = External::New(a);

		AccessorSetter setter;
		if (a->isReadOnly()) {
			setter = nullptr;
		} else {
			setter = ScriptPropertyAccessor::setPropertyCallback;
		}

		instance->SetAccessor(String::New(a->getPropertyName()),
				ScriptPropertyAccessor::getPropertyCallback,
				setter, data);
	}

	return handleScope.Close(templ);
}

void rev::ScriptingBasis::weakJSObjectCallback(v8::Persistent<v8::Value> object,
		void* parameter) {
	IBindable* bindable = static_cast<IBindable*>(parameter);

	bindable->weakJSObjectDestroyed();

//	REV_SCRIPT_DEBUG(
//#IBindable "::weakJSObjectCallback", parameter);

	object.Dispose();
	object.Clear();
}
