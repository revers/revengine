/*
 * RevIMemberWrapper.h
 *
 *  Created on: 02-03-2013
 *      Author: Revers
 */

#ifndef REVIMEMBERWRAPPER_H_
#define REVIMEMBERWRAPPER_H_

#include <rev/common/RevUnref.h>

namespace rev {

	class IBindable;

	//----------------------------------------------------------------------------------
	// IMemberWrapper
	//==================================================================================
	template<typename TProperty>
	class IMemberWrapper {
	public:

		virtual ~IMemberWrapper() {
		}

		virtual TProperty* getMemberPointer(void* thisPointer) const = 0;
	};

	//----------------------------------------------------------------------------------
	// MemberWrapper
	//==================================================================================
	/**
	 * This class acts like std::unique_ptr for IMemberWrapper*,
	 * i.e. you can move it (via std::move()), but not copy it.
	 */
	template<typename T>
	class MemberWrapper {
		IMemberWrapper<T>* baseWrapper;

		// copy constructor:
		MemberWrapper(const MemberWrapper&) = delete;
		// assignment operator:
		MemberWrapper& operator=(const MemberWrapper&) = delete;
		// move assignment operator:
		MemberWrapper& operator=(MemberWrapper&&) = delete;

	public:
		// move constructor:
		MemberWrapper(MemberWrapper&& other) {
			this->baseWrapper = other.baseWrapper;
			other.baseWrapper = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit MemberWrapper(IMemberWrapper<T>* binderBase) :
				baseWrapper(binderBase) {
		}

		~MemberWrapper() {
			if (baseWrapper) {
				delete baseWrapper;
			}
		}

		/**
		 * Like the release() from std::unique_ptr.
		 */
		inline IMemberWrapper<T>* release() {
			IMemberWrapper<T>* b = baseWrapper;
			baseWrapper = nullptr;
			return b;
		}
		inline T* getMemberPointer(void* thisPointer) const {
			return baseWrapper->getMemberPointer(thisPointer);
		}
	};

	namespace prv {
		//----------------------------------------------------------------------------------
		// TMemberWrapper
		//==================================================================================
		template<typename T, typename TProperty>
		class TMemberWrapper: public IMemberWrapper<TProperty> {
		public:
			typedef TProperty T::*Member;
			Member member;

			TMemberWrapper(Member member) :
					member(member) {
			}

			TProperty* getMemberPointer(void* thisPointer) const {
				return &(static_cast<T*>(thisPointer)->*member);
			}
		};

		//----------------------------------------------------------------------------------
		// TPtrMemberWrapper
		//==================================================================================
		template<typename T, typename TPropertyPtr>
		class TPtrMemberWrapper: public IMemberWrapper<typename rev::unref<TPropertyPtr>::type> {
		public:
			typedef TPropertyPtr T::*Member;
			Member member;

			TPtrMemberWrapper(Member member) :
					member(member) {
			}

			TPropertyPtr getMemberPointer(void* thisPointer) const {
				return static_cast<T*>(thisPointer)->*member;
			}
		};

		//----------------------------------------------------------------------------------
		// TBindableMemberWrapper
		//==================================================================================
		template<typename T, typename TProperty>
		class TBindableMemberWrapper: public IMemberWrapper<IBindable*> {
		public:
			typedef TProperty T::*Member;
			Member member;

			TBindableMemberWrapper(Member member) :
					member(member) {
			}

			IBindable** getMemberPointer(void* thisPointer) const override {
				return (IBindable**) &(static_cast<T*>(thisPointer)->*member);
			}
		};
	} /* namespace prv */

} /* namespace rev */

#endif /* REVIMEMBERWRAPPER_H_ */
