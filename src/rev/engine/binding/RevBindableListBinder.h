/*
 * RevIBindableListBinder.h
 *
 *  Created on: 11-02-2013
 *      Author: Revers
 */

#ifndef REVIBINDABLELISTBINDER_H_
#define REVIBINDABLELISTBINDER_H_

#include <string>
#include <v8/v8.h>
#include <rev/common/RevAssert.h>
#include "RevListTypeUtil.h"

namespace rev {

	class IBindable;

	namespace prv {
		//----------------------------------------------------------------------------------
		// IBindableListBinderIterBase
		//==================================================================================
		class IBindableListBinderIterBase {
		public:
			virtual ~IBindableListBinderIterBase() {
			}

			virtual const IBindableListBinderIterBase& operator++() const = 0;
			virtual bool operator==(const IBindableListBinderIterBase& rhs) const = 0;
			virtual bool operator!=(const IBindableListBinderIterBase& rhs) const = 0;
			virtual IBindable* getPointer() const = 0;
			virtual v8::Handle<v8::Value> getValue() const = 0;
		};
	} // end namespace prv

	//----------------------------------------------------------------------------------
	// BindableListBinderIterator
	//==================================================================================
	/**
	 * This class has similar properties to std::unique_ptr, i.e. you can move it
	 * (via std::move()), but not copy it.
	 */
	class BindableListBinderIterator {
		prv::IBindableListBinderIterBase* iter;

		// copy constructor:
		BindableListBinderIterator(const BindableListBinderIterator&) = delete;
		// assignment operator:
		BindableListBinderIterator& operator=(const BindableListBinderIterator&) = delete;
		// move assignment operator:
		BindableListBinderIterator& operator=(BindableListBinderIterator&&) = delete;

	public:
		// move constructor:
		BindableListBinderIterator(BindableListBinderIterator&& other) {
			this->iter = other.iter;
			other.iter = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BindableListBinderIterator(prv::IBindableListBinderIterBase* iter) :
				iter(iter) {
		}

		~BindableListBinderIterator() {
			delete iter;
		}

		inline const BindableListBinderIterator& operator++() const {
			iter->operator ++();
			return *this;
		}
		inline bool operator==(const BindableListBinderIterator& rhs) const {
			return iter->operator ==(*(rhs.iter));
		}
		inline bool operator!=(const BindableListBinderIterator& rhs) const {
			return iter->operator !=(*(rhs.iter));
		}
		inline IBindable* getPointer() const {
			return iter->getPointer();
		}
		inline v8::Handle<v8::Value> getValue() const {
			return iter->getValue();
		}
	};

	//----------------------------------------------------------------------------------
	// IBindableListBinder
	//==================================================================================
	class IBindableListBinder {
	public:
		virtual ~IBindableListBinder() {
		}

		virtual const std::string& getName() const = 0;
		virtual BindableListBinderIterator begin() = 0;
		virtual BindableListBinderIterator end() = 0;
		virtual void pushBack(IBindable* bindable) = 0;
		virtual void clear() = 0;
		virtual int getSize() const = 0;
		virtual v8::Handle<v8::Value> get(int index) const = 0;
	};

	//----------------------------------------------------------------------------------
	// BindableListBinder
	//==================================================================================
	/**
	 * This class acts like std::unique_ptr for IBindableListBinder*,
	 * i.e. you can move it (via std::move()), but not copy it.
	 */
	class BindableListBinder {
		IBindableListBinder* binderBase;

		// copy constructor:
		BindableListBinder(const BindableListBinder&) = delete;
		// assignment operator:
		BindableListBinder& operator=(const BindableListBinder&) = delete;
		// move assignment operator:
		BindableListBinder& operator=(BindableListBinder&&) = delete;

	public:
		// move constructor:
		BindableListBinder(BindableListBinder&& other) {
			this->binderBase = other.binderBase;
			other.binderBase = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BindableListBinder(IBindableListBinder* binderBase) :
				binderBase(binderBase) {
		}

		~BindableListBinder() {
			if (binderBase) {
				delete binderBase;
			}
		}

		/**
		 * Like the release() from std::unique_ptr.
		 */
		inline IBindableListBinder* release() {
			IBindableListBinder* b = binderBase;
			binderBase = nullptr;
			return b;
		}

		inline const std::string& getName() const {
			return binderBase->getName();
		}
		inline BindableListBinderIterator begin() {
			return binderBase->begin();
		}
		inline BindableListBinderIterator end() {
			return binderBase->end();
		}
		inline void pushBack(IBindable* bindable) {
			binderBase->pushBack(bindable);
		}
		inline void clear() {
			binderBase->clear();
		}
		inline int getSize() const {
			return binderBase->getSize();
		}
		inline v8::Handle<v8::Value> get(int index) const {
			return binderBase->get(index);
		}
	};

	namespace prv {
		//----------------------------------------------------------------------------------
		// TBindableListBinderIterBase
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class T, template<class U, class = std::allocator<U>> class Seq>
		class TBindableListBinderIterBase: public IBindableListBinderIterBase {
			typedef TBindableListBinderIterBase<T, Seq> ThisIter;
			typedef Seq<T*> ListType;
			typedef typename ListType::iterator ListTypeIterator;

			mutable ListTypeIterator iter;

		public:
			TBindableListBinderIterBase(ListTypeIterator iter) :
					iter(iter) {
			}

			const IBindableListBinderIterBase& operator++() const override {
				++iter;
				return *this;
			}

			bool operator==(const IBindableListBinderIterBase& rhs) const override {
				return iter == static_cast<const ThisIter*>(&rhs)->iter;
			}

			bool operator!=(const IBindableListBinderIterBase& rhs) const override {
				return iter != static_cast<const ThisIter*>(&rhs)->iter;
			}

			IBindable* getPointer() const override {
				return *iter;
			}

			v8::Handle<v8::Value> getValue() const override {
				T*& val = *iter;
				return static_cast<IBindable*>(val)->wrap();
			}
		};

		//----------------------------------------------------------------------------------
		// TBindableListBinder
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class T, template<class U, class = std::allocator<U>> class Seq>
		class TBindableListBinder: public IBindableListBinder {
			typedef Seq<T*> ListType;
			ListType* list;
			std::string name;

		public:
			TBindableListBinder(const char* name, ListType* list) :
					name(name), list(list) {
			}

			const std::string& getName() const override {
				return name;
			}

			BindableListBinderIterator begin() override {
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(list->begin()));
			}

			BindableListBinderIterator end() override {
				return BindableListBinderIterator(
						new TBindableListBinderIterBase<T, Seq>(list->end()));
			}

			void pushBack(IBindable* bindable) override {
				list->push_back(dynamic_cast<T*>(bindable));
			}

			void clear() override {
				list->clear();
			}

			int getSize() const override {
				return list->size();
			}

			v8::Handle<v8::Value> get(int index) const override {
				revAssert(index >= 0 && index < list->size());
				T*& value = ListUtil::get(*list, index);
				return static_cast<IBindable*>(value)->wrap();
			}
		};
	} // end namespace prv

}
// namespace rev

#endif /* REVIBINDABLELISTBINDER_H_ */
