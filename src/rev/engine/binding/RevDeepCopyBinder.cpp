/*
 * RevDeepCopyBinder.cpp
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevDeepCopyBinder.h"

using namespace rev;

DeepCopyBinder::DeepCopyBinder(const char* name, IBindable* bindable) :
		name(name) {
	if (bindable) {
		if (bindable->isSingleton()) {
			copyBindable = bindable;
		} else {
			copyBindable = bindable->makeBindableInstance();
			bindObject(bindable);
		}
	}
}

DeepCopyBinder::~DeepCopyBinder() {
	unbindObject(baseBindable);
	for (DeepCopyBinder*& b : childBindables) {
		delete b;
	}
}

void DeepCopyBinder::unbindObject(IBindable* bindable) {
	if (bindable && bindable == baseBindable) {
		clearAll();
		baseBindable = nullptr;
	}
}

void DeepCopyBinder::bindObject(IBindable* bindable) {
	if (!bindable) {
		REV_DEBUG_MSG("null bindable");
		return;
	}
	if (bindable->isSingleton()) {
		REV_WARN_MSG("Trying to bind singleton bindable: " << bindable->getBindableName());
		return;
	}
	unbindObject(baseBindable);
	bindable->bind(*this);
	baseBindable = bindable;
}

void DeepCopyBinder::clearAll() {
	for (DeepCopyBinder*& b : childBindables) {
		delete b;
	}

	childBindables.clear();
}

void DeepCopyBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<IBindable*> wrapper, bool editable, bool visible, bool expanded) {
	IBindable* member = *wrapper.getMemberPointer(bindable);
	IBindable** copyMemberPtr = wrapper.getMemberPointer(copyBindable);

	DeepCopyBinder* copyBinder = new DeepCopyBinder(name, member);
	*copyMemberPtr = copyBinder->getCopyBindable();
	childBindables.push_back(copyBinder);
}

/**
 * BasicTypeListMemberWrapper
 */
void DeepCopyBinder::bindList(IBindable* bindable, const char* name,
		BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	BasicTypeListBinderIterator begin = wrapper.begin(bindable);
	BasicTypeListBinderIterator end = wrapper.end(bindable);
	for (; begin != end; ++begin) {
		wrapper.pushBackFromIter(copyBindable, begin);
	}
}

/**
 * BindableListMemberWrapper
 */
void DeepCopyBinder::bindList(IBindable* bindable, const char* name,
		BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {

	BindableListBinderIterator begin = wrapper.begin(bindable);
	BindableListBinderIterator end = wrapper.end(bindable);
	int index = 0;
	for (; begin != end; ++begin) {
		std::ostringstream oss;
		oss << name << "[" << (index++) << "]";
		IBindable* member = begin.getPointer();
		DeepCopyBinder* copyBinder = new DeepCopyBinder(oss.str().c_str(), member);
		childBindables.push_back(copyBinder);

		wrapper.pushBack(copyBindable, copyBinder->getCopyBindable());
	}
}

bool DeepCopyBinder::initCopyBindable(DeepCopyBinder* copyBinder) {
	IBindable* copy = copyBinder->copyBindable;

	DeepCopyBinderVector childBindables = copyBinder->childBindables;
	for (DeepCopyBinder*& b : childBindables) {
		if (!initCopyBindable(b)) {
			return false;
		}
	}
	if (copy) {
		bool succ = copy->initAfterDeserialization();
		if (!succ) {
			REV_ERROR_MSG(copyBinder->getName()
					<< " (" << copy->getBindableName()
					<< ") -> initAfterDeserialization() FAILED!!");
		}
		return succ;
	}
	return true;
}

bool DeepCopyBinder::initCopyBindable() {
	revAssert(copyBindable);
	return initCopyBindable(this);
}

void DeepCopyBinder::destroyCopyBindable() {
	if (copyBindable && !copyBindable->isSingleton()) {
		delete copyBindable;
	}
	copyBindable = nullptr;
}
