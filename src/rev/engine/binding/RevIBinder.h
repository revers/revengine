/*
 * RevIBinder.h
 *
 *  Created on: 17-01-2013
 *      Author: Revers
 */

#ifndef REVIBINDER_H_
#define REVIBINDER_H_

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <rev/common/RevUnref.h>
#include <rev/gl/RevColor.h>

#include "RevIBindable.h"
#include "RevBasicTypeListMemberWrapper.h"
#include "RevBindableListMemberWrapper.h"
#include "RevMemberWrapper.h"

/**
 *  Shortcut for TMemberWrapper.
 */
#define MEMBER_WRAPPER(name) MemberWrapper<decltype(name)>(new prv::TMemberWrapper<THIS_TYPE, decltype(name)>(&THIS_TYPE::name))

/**
 *  Shortcut for TPtrMemberWrapper.
 */
#define MEMBER_PTR_WRAPPER(name) MemberWrapper<typename rev::unref<decltype(name)>::type>(new prv::TPtrMemberWrapper<THIS_TYPE, decltype(name)>(&THIS_TYPE::name))

/**
 *  Shortcut for TBindableMemberWrapper.
 */
#define MEMBER_OBJ_WRAPPER(name) MemberWrapper<IBindable*>(new prv::TBindableMemberWrapper<THIS_TYPE, decltype(name)>(&THIS_TYPE::name))

/**
 * Name-Value-Pair.
 */
#define NVP(name) #name, name

/**
 * Name-MemberWrapper-Pair.
 */
#define NMWP(name) #name, MEMBER_WRAPPER(name)
#define NMWP_PTR(name) #name, MEMBER_PTR_WRAPPER(name)
#define NMWP_OBJ(name) #name, MEMBER_OBJ_WRAPPER(name)

/**
 * Bind with default flags.
 */
#define bindSimple(name) bind(this, NMWP(name))
#define bindSimplePtr(name) bind(this, NMWP_PTR(name))
#define bindSimpleObj(name) bind(this, NMWP_OBJ(name))

/**
 * Bind with default flags, except that "expanded" is set to "true".
 */
#define bindSimpleExpanded(name) bind(this, NMWP(name), true, true, true)
#define bindSimplePtrExpanded(name) bind(this, NMWP_PTR(name), true, true, true)
#define bindSimpleObjExpanded(name) bind(this, NMWP_OBJ(name), true, true, true)

/**
 * Bind Read-Only.
 */
#define bindSimpleRO(name) bind(this, NMWP(name), false, true, false)
#define bindSimplePtrRO(name) bind(this, NMWP_PTR(name), false, true, false)
#define bindSimpleObjRO(name) bind(this, NMWP_OBJ(name), false, true, false)

/**
 * Bind invisible.
 */
#define bindSimpleInvisible(name) bind(this, NMWP(name), false, false, false)
#define bindSimplePtrInvisible(name) bind(this, NMWP_PTR(name), false, false, false)
#define bindSimpleObjInvisible(name) bind(this, NMWP_OBJ(name), false, false, false)

namespace rev {

	class IBindable;

	/**
	 * IMPORTANT: Binder should NEVER be an owner of a bindable!
	 */
	class IBinder {
	public:

		virtual ~IBinder() {
		}

		virtual void bindObject(IBindable* bindable) = 0;

		virtual void unbindObject(IBindable* bindable) = 0;

		/**
		 * IBindable.
		 * IMPORTANT: Remember to handle nullptr values!
		 */
		virtual void bind(IBindable* bindable, const char* name, MemberWrapper<IBindable*> wrapper,
				bool editable, bool visible, bool expanded) = 0;

		/**
		 * int
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<int> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * float
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<float> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * double
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<double> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * bool
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<bool> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * std::string
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * vec2
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::vec2> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * vec3
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::vec3> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * vec4
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::vec4> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * mat2
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::mat2> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * mat3
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::mat3> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * mat4
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::mat4> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * quat
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::quat> wrapper, bool editable, bool visible, bool expanded) = 0;

		//-------------------------------------------------------------------------------------------------------

		/**
		 * dvec2
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dvec2> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dvec3
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dvec3> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dvec4
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dvec4> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dmat2
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dmat2> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dmat3
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dmat3> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dmat4
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dmat4> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * dquat
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<glm::dquat> wrapper, bool editable, bool visible, bool expanded) = 0;

		//-------------------------------------------------------------------------------------------------------

		/**
		 * color3
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<rev::color3> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * color4
		 */
		virtual void bind(IBindable* bindable, const char* name,
				MemberWrapper<rev::color4> wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * filepath (std::string)
		 *
		 * @param fileFilter - should be in the Qt QFileDialog's format, eg.:
		 * 		"JavaScript files (*.js)"
		 */
		virtual void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter,
				bool editable, bool visible) = 0;

		/**
		 * Simple filepath (std::string)
		 *
		 * @param fileFilter - should be in the Qt QFileDialog's format, eg.:
		 * 		"JavaScript files (*.js)"
		 */
		void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter) {
			bindFilepath(bindable, name, std::move(wrapper), fileFilter, true, true);
		}
		//=========================================================================

		/**
		 * Simple IBindable
		 */
		template<typename Bindable>
		void bind(IBindable* bindable, const char* name, Bindable*& val) {
			bind(bindable, name, val, true, true, false);
		}

		/**
		 * Simple reference.
		 */
		template<typename T>
		void bind(IBindable* bindable, const char* name, MemberWrapper<T> wrapper) {
			bind(bindable, name, std::move(wrapper), true, true, false);
		}

		//-----------------------------------------------------------------------------
		// BEGIN Lists
		//-----------------------------------------------------------------------------

		/**
		 * BasicTypeListMemberWrapper
		 */
		virtual void bindList(IBindable* bindable, const char* name,
				BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * BindableListMemberWrapper
		 */
		virtual void bindList(IBindable* bindable, const char* name,
				BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) = 0;

		/**
		 * Basic Type List Binder: std::vector<T> or std::list<T>
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T> Bindable::*listMember,
				bool editable, bool visible, bool expanded) {

			bindList(bindable, name,
					BasicTypeListMemberWrapper(
							new prv::TBasicTypeListMemberWrapper<Bindable, T, Seq>(name,
									listMember)), editable, visible, expanded);
		}

		/**
		 * Basic Type List Binder: Simple std::vector<T> or std::list<T>
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T> Bindable::*listMember) {
			bindList<Bindable, T, Seq>(bindable, name, listMember, false, true, false);
		}

		/**
		 * Basic Type List Binder: std::vector<T>* or std::list<T>*
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T>* Bindable::*listMember,
				bool editable, bool visible, bool expanded) {

			bindList(bindable, name,
					BasicTypeListMemberWrapper(
							new prv::TBasicTypeListMemberPtrWrapper<Bindable, T, Seq>(name,
									listMember)), editable, visible, expanded);
		}

		/**
		 * Basic Type List Binder: Simple std::vector<T>* or std::list<T>*
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T>* Bindable::*listMember) {
			bindList<Bindable, T, Seq>(bindable, name, listMember, false, true, false);
		}

		/**
		 * Bindable List Binder: std::vector<T> or std::list<T>
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T*> Bindable::*listMember,
				bool editable, bool visible, bool expanded) {

			bindList(bindable, name, BindableListMemberWrapper(
					new prv::TBindableListMemberWrapper<Bindable, T, Seq>(name, listMember)),
					editable, visible, expanded);
		}

		/**
		 * Bindable List Binder: Simple std::vector<T> or std::list<T>
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T*> Bindable::*listMember) {
			bindList<Bindable, T, Seq>(bindable, name, listMember, false, true, false);
		}

		/**
		 * Bindable List Binder: std::vector<T>* or std::list<T>*
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T*>* Bindable::*listMember,
				bool editable, bool visible, bool expanded) {
			bindList(bindable, name, BindableListMemberWrapper(
					new prv::TBindableListMemberPtrWrapper<Bindable, T, Seq>(name, listMember)),
					editable, visible, expanded);
		}

		/**
		 * Bindable List Binder: Simple std::vector<T>* or std::list<T>*
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		void bindList(IBindable* bindable, const char* name, Seq<T*>* Bindable::*listMember) {
			bindList<Bindable, T, Seq>(bindable, name, listMember, false, true, false);
		}

		//=============================================================================
		// END Lists
		//=============================================================================

		/**
		 * int
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, int& value) {
		}

		/**
		 * float
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, float& value) {
		}

		/**
		 * double
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, double& value) {
		}

		/**
		 * bool
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, bool& value) {
		}

		/**
		 * std::string
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, std::string& value) {
		}

		/**
		 * glm::vec2
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::vec2& value) {
		}

		/**
		 * glm::vec3
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::vec3& value) {
		}

		/**
		 * glm::vec4
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::vec4& value) {
		}

		/**
		 * glm::mat2
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::mat2& value) {
		}

		/**
		 * glm::mat3
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::mat3& value) {
		}

		/**
		 * glm::mat4
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::mat4& value) {
		}

		/**
		 * glm::quat
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::quat& value) {
		}

		/**
		 * glm::dvec2
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dvec2& value) {
		}

		/**
		 * glm::dvec3
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dvec3& value) {
		}

		/**
		 * glm::dvec4
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dvec4& value) {
		}

		/**
		 * glm::dmat2
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dmat2& value) {
		}

		/**
		 * glm::dmat3
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dmat3& value) {
		}

		/**
		 * glm::dmat4
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dmat4& value) {
		}

		/**
		 * glm::dquat
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, glm::dquat& value) {
		}

		/**
		 * color3
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, rev::color3& value) {
		}

		/**
		 * color4
		 */
		virtual void bindInfo(IBindable* bindable, const char* name, rev::color4& value) {
		}
	};

} // namespace rev
#endif /* REVIBINDER_H_ */
