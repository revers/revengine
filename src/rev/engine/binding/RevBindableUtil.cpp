/*
 * RevBindableUtil.cpp
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#include "RevIBindable.h"
#include "RevBindableUtil.h"
#include "RevDeepCopyBinder.h"

using namespace rev;
using namespace rev::BindableUtil;

IBindable* rev::BindableUtil::makeDeepCopyOf(IBindable* bindable) {
	if (!bindable) {
		REV_WARN_MSG("Trying to copy nullptr bindable");
		return nullptr;
	}

	DeepCopyBinder deepCopyBinder("<<root>>", bindable);
	if (!deepCopyBinder.initCopyBindable()) {
		deepCopyBinder.destroyCopyBindable();
		return nullptr;
	}

	return deepCopyBinder.getCopyBindable();
}

