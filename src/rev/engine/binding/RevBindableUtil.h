/*
 * RevBindableUtil.h
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#ifndef REVBINDABLEUTIL_H_
#define REVBINDABLEUTIL_H_

namespace rev {

	class IBindable;

	namespace BindableUtil {

		IBindable* makeDeepCopyOf(IBindable* bindable);

	} /* namespace BindableUtil */

} /* namespace rev */
#endif /* REVBINDABLEUTIL_H_ */
