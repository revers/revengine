/*
 * RevIBasicTypeListBinder.h
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#ifndef REVIBASICTYPELISTBINDER_H_
#define REVIBASICTYPELISTBINDER_H_

#include <string>
#include <typeinfo>
#include <v8/v8.h>
#include <rev/common/RevAssert.h>
#include <rev/engine/scripting/RevScriptType.h>
#include "RevTBasicTypeFactory.h"
#include "RevListTypeUtil.h"

namespace rev {

	namespace prv {
		//----------------------------------------------------------------------------------
		// IBasicTypeListBinderIterBase
		//==================================================================================
		class IBasicTypeListBinderIterBase {
		public:
			virtual ~IBasicTypeListBinderIterBase() {
			}

			virtual const IBasicTypeListBinderIterBase& operator++() const = 0;
			virtual bool operator==(const IBasicTypeListBinderIterBase& rhs) const = 0;
			virtual bool operator!=(const IBasicTypeListBinderIterBase& rhs) const = 0;
			virtual std::string toString() const = 0;
			virtual void* getPointer() const = 0;
			virtual v8::Handle<v8::Value> getValue() const = 0;
			virtual void setValue(const v8::Handle<v8::Value>& value) = 0;
		};
	} // namespace prv

	//----------------------------------------------------------------------------------
	// BasicTypeListBinderIterator
	//==================================================================================
	/**
	 * This class has similar properties to std::unique_ptr, i.e. you can move it
	 * (via std::move()), but not copy it.
	 */
	class BasicTypeListBinderIterator {
		prv::IBasicTypeListBinderIterBase* iter;

		// copy constructor:
		BasicTypeListBinderIterator(const BasicTypeListBinderIterator&) = delete;
		// assignment operator:
		BasicTypeListBinderIterator& operator=(const BasicTypeListBinderIterator&) = delete;
		// move assignment operator:
		BasicTypeListBinderIterator& operator=(BasicTypeListBinderIterator&&) = delete;

	public:
		// move constructor:
		BasicTypeListBinderIterator(BasicTypeListBinderIterator&& other) {
			this->iter = other.iter;
			other.iter = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BasicTypeListBinderIterator(prv::IBasicTypeListBinderIterBase* iter) :
				iter(iter) {
		}

		~BasicTypeListBinderIterator() {
			delete iter;
		}

		inline const BasicTypeListBinderIterator& operator++() const {
			iter->operator ++();
			return *this;
		}
		inline bool operator==(const BasicTypeListBinderIterator& rhs) const {
			return iter->operator ==(*(rhs.iter));
		}
		inline bool operator!=(const BasicTypeListBinderIterator& rhs) const {
			return iter->operator !=(*(rhs.iter));
		}
		inline std::string toString() const {
			return iter->toString();
		}

		inline void* getPointer() const {
			return iter->getPointer();
		}

		inline v8::Handle<v8::Value> getValue() const {
			return iter->getValue();
		}

		inline void setValue(const v8::Handle<v8::Value>& value) {
			iter->setValue(value);
		}
	};

	//----------------------------------------------------------------------------------
	// IBasicTypeListBinder
	//==================================================================================
	class IBasicTypeListBinder {
	public:
		virtual ~IBasicTypeListBinder() {
		}

		virtual const std::string& getName() const = 0;
		virtual const std::string& getType() const = 0;
		virtual const std::type_info& getTypeInfo() const = 0;
		virtual BasicTypeListBinderIterator begin() = 0;
		virtual BasicTypeListBinderIterator end() = 0;
		virtual void pushBackFromString(const std::string& str) = 0;
		virtual void pushBackFromIter(const BasicTypeListBinderIterator& iter) = 0;
		virtual void clear() = 0;
		virtual int getSize() const = 0;
		virtual v8::Handle<v8::Value> get(int index) const = 0;
		virtual void set(int index, const v8::Handle<v8::Value>& value) = 0;
	};

	//----------------------------------------------------------------------------------
	// BasicTypeListBinder
	//==================================================================================
	/**
	 * This class acts like std::unique_ptr for IBasicTypeListBinder*,
	 * i.e. you can move it (via std::move()), but not copy it.
	 */
	class BasicTypeListBinder {
		IBasicTypeListBinder* binderBase;

		// copy constructor:
		BasicTypeListBinder(const BasicTypeListBinder&) = delete;
		// assignment operator:
		BasicTypeListBinder& operator=(const BasicTypeListBinder&) = delete;
		// move assignment operator:
		BasicTypeListBinder& operator=(BasicTypeListBinder&&) = delete;

	public:
		// move constructor:
		BasicTypeListBinder(BasicTypeListBinder&& other) {
			this->binderBase = other.binderBase;
			other.binderBase = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BasicTypeListBinder(IBasicTypeListBinder* binderBase) :
				binderBase(binderBase) {
		}

		~BasicTypeListBinder() {
			if (binderBase) {
				delete binderBase;
			}
		}

		/**
		 * Like the release() from std::unique_ptr.
		 */
		inline IBasicTypeListBinder* release() {
			IBasicTypeListBinder* b = binderBase;
			binderBase = nullptr;
			return b;
		}
		inline const std::string& getName() const {
			return binderBase->getName();
		}
		inline const std::string& getType() const {
			return binderBase->getType();
		}
		inline const std::type_info& getTypeInfo() const {
			return binderBase->getTypeInfo();
		}
		inline BasicTypeListBinderIterator begin() {
			return binderBase->begin();
		}
		inline BasicTypeListBinderIterator end() {
			return binderBase->end();
		}
		inline void pushBackFromString(const std::string& str) {
			binderBase->pushBackFromString(str);
		}
		inline void pushBackFromIter(const BasicTypeListBinderIterator& iter) {
			binderBase->pushBackFromIter(iter);
		}
		inline void clear() {
			binderBase->clear();
		}
		inline int getSize() const {
			return binderBase->getSize();
		}
		inline v8::Handle<v8::Value> get(int index) const {
			return binderBase->get(index);
		}
		inline void set(int index, const v8::Handle<v8::Value>& value) {
			binderBase->set(index, value);
		}
	};

	namespace prv {
		//----------------------------------------------------------------------------------
		// TBasicTypeListBinderIterBase
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class T, template<class U, class = std::allocator<U>> class Seq>
		class TBasicTypeListBinderIterBase: public IBasicTypeListBinderIterBase {
			typedef TBasicTypeListBinderIterBase<T, Seq> ThisIter;
			typedef TBasicTypeFactory<T> BasicTypeFactory;
			typedef Seq<T> ListType;
			typedef typename ListType::iterator ListTypeIterator;

			BasicTypeFactory* typeFactory;
			mutable ListTypeIterator iter;

		public:
			TBasicTypeListBinderIterBase(BasicTypeFactory* typeFactory,
					ListTypeIterator iter) :
					typeFactory(typeFactory), iter(iter) {
			}

			const IBasicTypeListBinderIterBase& operator++() const override {
				++iter;
				return *this;
			}

			bool operator==(const IBasicTypeListBinderIterBase& rhs) const override {
				return iter == static_cast<const ThisIter*>(&rhs)->iter;
			}

			bool operator!=(const IBasicTypeListBinderIterBase& rhs) const override {
				return iter != static_cast<const ThisIter*>(&rhs)->iter;
			}

			std::string toString() const override {
				T& val = *iter;
				return typeFactory->toString(&val);
			}

			void* getPointer() const override {
				T& val = *iter;
				return &val;
			}

			v8::Handle<v8::Value> getValue() const override {
				T& val = *iter;
				return ScriptType<T>::castToJS(val);
			}

			void setValue(const v8::Handle<v8::Value>& value) override {
				T& val = *iter;
				val = ScriptType<T>::castFromJS(value);
			}
		};

		//----------------------------------------------------------------------------------
		// TBasicTypeListBinder
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class T, template<class U, class = std::allocator<U>> class Seq>
		class TBasicTypeListBinder: public IBasicTypeListBinder {
			typedef TBasicTypeFactory<T> BasicTypeFactory;
			typedef Seq<T> ListType;

			BasicTypeFactory typeFactory;
			ListType* list;
			std::string name;
			std::string type;

		public:
			TBasicTypeListBinder(const char* name, ListType* list) :
					name(name), list(list) {
				type = typeFactory.getType();
			}
			const std::string& getName() const override {
				return name;
			}
			const std::string& getType() const override {
				return type;
			}
			const std::type_info& getTypeInfo() const override {
				return typeid(T);
			}
			BasicTypeListBinderIterator begin() override {
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								list->begin()));
			}
			BasicTypeListBinderIterator end() override {
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								list->end()));
			}
			void pushBackFromString(const std::string& str) override {
				T value;
				typeFactory.stringTo(str, &value);
				list->push_back(value);
			}
			void pushBackFromIter(const BasicTypeListBinderIterator& iter) override {
				T* valuePtr = static_cast<T*>(iter.getPointer());
				list->push_back(*valuePtr);
			}
			void clear() override {
				list->clear();
			}
			int getSize() const override {
				return list->size();
			}

			v8::Handle<v8::Value> get(int index) const override {
				revAssert(index >= 0 && index < list->size());
				T& value = ListUtil::get(*list, index);
				return ScriptType<T>::castToJS(value);
			}

			void set(int index, const v8::Handle<v8::Value>& value) override {
				revAssert(index >= 0 && index < list->size());
				T& val = ListUtil::get(*list, index);
				val = ScriptType<T>::castFromJS(value);
			}
		};
	} // end namespace prv
}
// namespace rev

#endif /* REVIBASICTYPELISTBINDER_H_ */
