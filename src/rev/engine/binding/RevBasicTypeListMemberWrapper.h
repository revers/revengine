/*
 * RevBasicTypeListMemberWrapper.h
 *
 *  Created on: 13-03-2013
 *      Author: Revers
 */

#ifndef REVBASICTYPELISTMEMBERWRAPPER_H_
#define REVBASICTYPELISTMEMBERWRAPPER_H_

#include <typeinfo>
#include "RevBasicTypeListBinder.h"

namespace rev {

	class IBindable;

	//----------------------------------------------------------------------------------
	// IBasicTypeListMemberWrapper
	//==================================================================================
	class IBasicTypeListMemberWrapper {
	public:
		virtual ~IBasicTypeListMemberWrapper() {
		}

		virtual const std::string& getName() const = 0;
		virtual const std::string& getType() const = 0;
		virtual const std::type_info& getTypeInfo() const = 0;
		virtual BasicTypeListBinderIterator begin(IBindable* owner) = 0;
		virtual BasicTypeListBinderIterator end(IBindable* owner) = 0;
		virtual void pushBackFromString(IBindable* owner, const std::string& str) = 0;
		virtual void pushBackFromIter(IBindable* owner,
				const BasicTypeListBinderIterator& iter) = 0;
		virtual void clear(IBindable* owner) = 0;
		virtual int getSize(IBindable* owner) const = 0;
		virtual v8::Handle<v8::Value> get(IBindable* owner, int index) const = 0;
		virtual void set(IBindable* owner, int index, const v8::Handle<v8::Value>& value) = 0;
		virtual BasicTypeListBinder getBinder(IBindable* owner) = 0;
	};

	//----------------------------------------------------------------------------------
	// BasicTypeListMemberWrapper
	//==================================================================================
	/**
	 * This class acts like std::unique_ptr for IBasicTypeListMemberWrapper*,
	 * i.e. you can move it (via std::move()), but not copy it.
	 */
	class BasicTypeListMemberWrapper {
		IBasicTypeListMemberWrapper* binderBase;

		// copy constructor:
		BasicTypeListMemberWrapper(const BasicTypeListMemberWrapper&) = delete;
		// assignment operator:
		BasicTypeListMemberWrapper& operator=(const BasicTypeListMemberWrapper&) = delete;
		// move assignment operator:
		BasicTypeListMemberWrapper& operator=(BasicTypeListMemberWrapper&&) = delete;

	public:
		// move constructor:
		BasicTypeListMemberWrapper(BasicTypeListMemberWrapper&& other) {
			this->binderBase = other.binderBase;
			other.binderBase = nullptr;
		}

		/**
		 * Main constructor:
		 */
		explicit BasicTypeListMemberWrapper(IBasicTypeListMemberWrapper* binderBase) :
				binderBase(binderBase) {
		}

		~BasicTypeListMemberWrapper() {
			if (binderBase) {
				delete binderBase;
			}
		}

		/**
		 * Like the release() from std::unique_ptr.
		 */
		inline IBasicTypeListMemberWrapper* release() {
			IBasicTypeListMemberWrapper* b = binderBase;
			binderBase = nullptr;
			return b;
		}
		inline const std::string& getName() const {
			return binderBase->getName();
		}
		inline const std::string& getType() const {
			return binderBase->getType();
		}
		inline const std::type_info& getTypeInfo() const {
			return binderBase->getTypeInfo();
		}
		inline BasicTypeListBinderIterator begin(IBindable* owner) {
			return binderBase->begin(owner);
		}
		inline BasicTypeListBinderIterator end(IBindable* owner) {
			return binderBase->end(owner);
		}
		inline void pushBackFromString(IBindable* owner, const std::string& str) {
			binderBase->pushBackFromString(owner, str);
		}
		inline void pushBackFromIter(IBindable* owner, const BasicTypeListBinderIterator& iter) {

		}
		inline void clear(IBindable* owner) {
			binderBase->clear(owner);
		}
		inline int getSize(IBindable* owner) const {
			return binderBase->getSize(owner);
		}
		inline v8::Handle<v8::Value> get(IBindable* owner, int index) const {
			return binderBase->get(owner, index);
		}
		inline void set(IBindable* owner, int index, const v8::Handle<v8::Value>& value) {
			binderBase->set(owner, index, value);
		}
		inline BasicTypeListBinder getBinder(IBindable* owner) {
			return binderBase->getBinder(owner);
		}
	};

	namespace prv {

		//----------------------------------------------------------------------------------
		// TBasicTypeListMemberWrapper
		//==================================================================================
		/**
		 * For use with std::vector and std::list:
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		class TBasicTypeListMemberWrapper: public IBasicTypeListMemberWrapper {
			typedef TBasicTypeFactory<T> BasicTypeFactory;
			typedef Seq<T> ListType;
			typedef ListType Bindable::*ListMember;

			BasicTypeFactory typeFactory;
			ListMember listMember;
			std::string name;
			std::string type;

		public:
			TBasicTypeListMemberWrapper(const char* name, ListMember listMember) :
					name(name), listMember(listMember) {
				type = typeFactory.getType();
			}

			const std::string& getName() const override {
				return name;
			}
			const std::string& getType() const override {
				return type;
			}
			const std::type_info& getTypeInfo() const override {
				return typeid(T);
			}

			BasicTypeListBinderIterator begin(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								listRef.begin()));
			}

			BasicTypeListBinderIterator end(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								listRef.end()));
			}

			void pushBackFromString(IBindable* owner, const std::string& str) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				T value;
				typeFactory.stringTo(str, &value);
				listRef.push_back(value);
			}

			void pushBackFromIter(IBindable* owner, const BasicTypeListBinderIterator& iter)
					override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				T* valuePtr = static_cast<T*>(iter.getPointer());
				listRef.push_back(*valuePtr);
			}

			void clear(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef.clear();
			}

			int getSize(IBindable* owner) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return listRef.size();
			}

			v8::Handle<v8::Value> get(IBindable* owner, int index) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef.size());
				T& value = ListUtil::get(listRef, index);
				return ScriptType<T>::castToJS(value);
			}

			void set(IBindable* owner, int index, const v8::Handle<v8::Value>& value) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef.size());
				T& val = ListUtil::get(listRef, index);
				val = ScriptType<T>::castFromJS(value);
			}

			BasicTypeListBinder getBinder(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinder(
						new prv::TBasicTypeListBinder<T, Seq>(name.c_str(), &listRef));
			}
		};

		//----------------------------------------------------------------------------------
		// TBasicTypeListMemberPtrWrapper
		//==================================================================================
		/**
		 * For use with std::vector* and std::list*:
		 */
		template<class Bindable, class T, template<class U, class = std::allocator<U>> class Seq>
		class TBasicTypeListMemberPtrWrapper: public IBasicTypeListMemberWrapper {
			typedef TBasicTypeFactory<T> BasicTypeFactory;
			typedef Seq<T>* ListType;
			typedef ListType Bindable::*ListMember;

			BasicTypeFactory typeFactory;
			ListMember listMember;
			std::string name;
			std::string type;

		public:
			TBasicTypeListMemberPtrWrapper(const char* name, ListMember listMember) :
					name(name), listMember(listMember) {
				type = typeFactory.getType();
			}

			const std::string& getName() const override {
				return name;
			}
			const std::string& getType() const override {
				return type;
			}
			const std::type_info& getTypeInfo() const override {
				return typeid(T);
			}

			BasicTypeListBinderIterator begin(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								listRef->begin()));
			}

			BasicTypeListBinderIterator end(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinderIterator(
						new prv::TBasicTypeListBinderIterBase<T, Seq>(&typeFactory,
								listRef->end()));
			}

			void pushBackFromString(IBindable* owner, const std::string& str) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				T value;
				typeFactory.stringTo(str, &value);
				listRef->push_back(value);
			}

			void pushBackFromIter(IBindable* owner, const BasicTypeListBinderIterator& iter)
					override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				T* valuePtr = static_cast<T*>(iter.getPointer());
				listRef->push_back(*valuePtr);
			}

			void clear(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				listRef->clear();
			}

			int getSize(IBindable* owner) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return listRef->size();
			}

			v8::Handle<v8::Value> get(IBindable* owner, int index) const override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef->size());
				T& value = ListUtil::get(*listRef, index);
				return ScriptType<T>::castToJS(value);
			}

			void set(IBindable* owner, int index, const v8::Handle<v8::Value>& value) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				revAssert(index >= 0 && index < listRef->size());
				T& val = ListUtil::get(*listRef, index);
				val = ScriptType<T>::castFromJS(value);
			}

			BasicTypeListBinder getBinder(IBindable* owner) override {
				ListType& listRef = static_cast<Bindable*>(owner)->*listMember;
				return BasicTypeListBinder(
						new prv::TBasicTypeListBinder<T, Seq>(name.c_str(), listRef));
			}
		};

	} // end namespace prv
}
// namespace rev

#endif /* REVBASICTYPELISTMEMBERWRAPPER_H_ */
