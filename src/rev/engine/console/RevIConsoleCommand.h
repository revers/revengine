/*
 * RevIConsoleCommand.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVICONSOLECOMMAND_H_
#define REVICONSOLECOMMAND_H_

namespace rev {
    class IConsoleCommand {
    public:
        virtual ~IConsoleCommand() {
        }

        virtual const char* getName() = 0;
        virtual bool execute(int argc, const char** argv) = 0;
    };
}

#endif /* REVICONSOLECOMMAND_H_ */
