/* 
 * File:   RevConsoleServer.h
 * Author: Revers
 *
 * Created on 9 marzec 2013, 23:15
 *
 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#ifndef REVCONSOLESERVER_H
#define	REVCONSOLESERVER_H

#include <rev/engine/memory/RevMemoryMappedFile.h>

#define REV_CONSOLE_MAX_ARGS 32

namespace rev {

	class ConsoleServer {
		MemoryMappedFile receiveFile;
		const char* argv[REV_CONSOLE_MAX_ARGS];
		char* buffer;
		int receiveCount;

		ConsoleServer(const ConsoleServer& orig) = delete;

	public:
		ConsoleServer(const char* virtualFilename, int size);

		~ConsoleServer() {
			delete[] buffer;
		}

		const char** getInput(int& argc);
	};

}

#endif	/* REVCONSOLESERVER_H */

