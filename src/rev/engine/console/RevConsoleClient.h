/* 
 * File:   RevConsoleClient.h
 * Author: Revers
 *
 * Created on 10 marzec 2013, 07:26
 *
 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#ifndef REVCONSOLECLIENT_H
#define	REVCONSOLECLIENT_H

#include <string>
#include <vector>
#include <rev/engine/memory/RevMemoryMappedFile.h>

namespace rev {

	class ConsoleClient {
		size_t commandIndex = 0;
		std::vector<std::string> commands;
		MemoryMappedFile sendFile;
		int sendCount;
		int size;

		ConsoleClient(const ConsoleClient& orig) = delete;

	public:

		ConsoleClient(const char* virtualFilename, int size);

		~ConsoleClient() {
		}

		void send(const char* msg);
		bool getInputString(char* buf, int len);

	private:
		void pushString(const char* str);
	};

}
#endif	/* REVCONSOLECLIENT_H */

