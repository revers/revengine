/*
 * RevConsoleManager.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVCONSOLEMANAGER_H_
#define REVCONSOLEMANAGER_H_

#include <map>
#include <string>
#include <rev/engine/binding/RevIBindable.h>
#include "RevIConsoleCommand.h"
#include "RevConsoleConfig.h"
#include "RevConsoleClient.h"
#include "RevConsoleServer.h"

namespace rev {

	class ConsoleManager: public IBindable {
		static ConsoleManager* consoleManager;
		typedef std::map<std::string, IConsoleCommand*> CommandMap;

		CommandMap commandMap;
		ConsoleServer consoleServer;
		ConsoleClient consoleClient;

		ConsoleManager() :
				consoleServer(REV_MM_IN_FILE_NAME, REV_MM_FILE_SIZE),
						consoleClient(REV_MM_OUT_FILE_NAME, REV_MM_FILE_SIZE) {
		}

		ConsoleManager(const ConsoleManager&);
		~ConsoleManager();
		
	public:
		DECLARE_BINDABLE_SINGLETON(ConsoleManager)

		static void createSingleton();
		static void destroySingleton();

		static ConsoleManager& ref() {
			return *consoleManager;
		}

		static ConsoleManager* getInstance() {
			return consoleManager;
		}

		void update();
		bool init();

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCONSOLEMANAGER_H_ */
