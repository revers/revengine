/*
 * RevConsoleManager.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevErrorStream.h>
#include "RevConsoleManager.h"
#include "commands/RevListCommand.h"
#include "commands/RevScriptCommand.h"

using namespace std;
using namespace rev;

rev::ConsoleManager* rev::ConsoleManager::consoleManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(ConsoleManager)

ConsoleManager::~ConsoleManager() {
	for (auto& pair : commandMap) {
		delete pair.second;
	}
	commandMap.clear();
}

void ConsoleManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!consoleManager);
	consoleManager = new ConsoleManager();
	bool succ = consoleManager->init();
	revAssert(succ);
	REV_TRACE_FUNCTION_OUT;
}

void ConsoleManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(consoleManager);

	delete consoleManager;
	consoleManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void ConsoleManager::update() {
	int argc;
	const char** argv = consoleServer.getInput(argc);
	if (argv) {
		ostringstream oss;
		oss << "[SERVER] Received: ";
		for (int i = 0; i < argc; i++) {
			oss << argv[i] << " ";
		}
		std::string cmd = oss.str();
		REV_INFO_MSG(cmd);
		// consoleClient.send(cmd.c_str());

		CommandMap::iterator it = commandMap.find(argv[0]);
		if (it != commandMap.end()) {
			it->second->execute(argc - 1, argv + 1);
			consoleClient.send("OK");
		} else {
			consoleClient.send("ERROR: Unknown command!");
		}
	}
}

void ConsoleManager::bind(IBinder& binder) {
}

bool ConsoleManager::init() {
	ListCommand* listCommand = new ListCommand();
	commandMap[listCommand->getName()] = listCommand;

	ScriptCommand* scriptCommand = new ScriptCommand();
	commandMap[scriptCommand->getName()] = scriptCommand;

	return true;
}

