/*
 * RevConsoleOutputStream.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVCONSOLEOUTPUTSTREAM_H_
#define REVCONSOLEOUTPUTSTREAM_H_

namespace rev {

    class ConsoleOutputStream {
    public:
        ConsoleOutputStream();
        virtual ~ConsoleOutputStream();
    };

} /* namespace rev */
#endif /* REVCONSOLEOUTPUTSTREAM_H_ */
