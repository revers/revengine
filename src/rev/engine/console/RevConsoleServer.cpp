/* 
 * File:   RevConsoleServer.cpp
 * Author: Revers
 * 
 * Created on 9 marzec 2013, 23:15

 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#include <rev/common/RevErrorStream.h>
#include "RevConsoleServer.h"

using namespace rev;

#define HEADER_SIZE (sizeof(int)+1)

ConsoleServer::ConsoleServer(const char* virtualFilename, int size) :
		receiveFile(virtualFilename, size), receiveCount(0), buffer(new char[size]) {
	revAssert(receiveFile.getBaseAddress());

	if (!receiveFile.wasCreated()) {
		char* baseAddress = (char*) receiveFile.getBaseAddress();
		int* dest = (int*) baseAddress;
		receiveCount = *dest;
	}
}

const char** ConsoleServer::getInput(int& argc) {
	const char** ret = nullptr;
	argc = 0;

	const char* baseAddress = (const char*) receiveFile.getBaseAddress();
	const int* source = (const int*) baseAddress;
	baseAddress += sizeof(int);
	if (*source != receiveCount) {
		receiveCount = *source;
		memcpy(buffer, baseAddress, (receiveFile.getSize() - sizeof(int)));

		char* scan = buffer;
		while (*scan == 32) {
			scan++; // skip leading spaces
		}
		if (*scan) { // if not EOS
			argc = 1;
			argv[0] = scan; // set the first argument

			while (*scan && argc < REV_CONSOLE_MAX_ARGS) { // while still data and we haven't exceeded the maximum argument count.
				while (*scan && *scan != 32) {
					scan++; // scan forward to the next space
				}
				if (*scan == 32) { // if we hit a space
					*scan = 0; // null-terminate the argument
					scan++; // skip to the next character
					while (*scan == 32) {
						scan++; // skip any leading spaces
					}
					if (*scan) { // if there is still a valid non-space character process that as the next argument
						argv[argc] = scan;
						argc++;
					}
				}
			}
			ret = argv;
		}
	}

	return ret;
}
