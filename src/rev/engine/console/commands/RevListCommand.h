/*
 * RevListCommand.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVLISTCOMMAND_H_
#define REVLISTCOMMAND_H_

#include "../RevIConsoleCommand.h"

namespace rev {

	class ListCommand: public IConsoleCommand {
	public:
		ListCommand() {
		}

		virtual ~ListCommand() {
		}

		const char* getName() override {
			return "ls";
		}

		bool execute(int argc, const char** argv) override;
	};

} /* namespace rev */
#endif /* REVLISTCOMMAND_H_ */
