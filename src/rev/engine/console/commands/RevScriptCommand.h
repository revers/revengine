/*
 * RevScriptCommand.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTCOMMAND_H_
#define REVSCRIPTCOMMAND_H_

#include "../RevIConsoleCommand.h"

namespace rev {

	class ScriptCommand: public IConsoleCommand {
	public:
		ScriptCommand() {
		}

		virtual ~ScriptCommand() {
		}

		const char* getName() override {
			return "script";
		}

		bool execute(int argc, const char** argv) override;
	};

} /* namespace rev */
#endif /* REVSCRIPTCOMMAND_H_ */
