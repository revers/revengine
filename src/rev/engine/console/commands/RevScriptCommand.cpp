/*
 * RevScriptCommand.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include "RevScriptCommand.h"

using namespace rev;
using namespace std;

bool ScriptCommand::execute(int argc, const char** argv) {
	if (argc == 1 && strcmp("-r", argv[0]) == 0) {
		ScriptingManager::ref().reloadMainScript();
		return true;
	}

	REV_WARN_MSG("Unknown or wrong parameters!");

	return true;
}
