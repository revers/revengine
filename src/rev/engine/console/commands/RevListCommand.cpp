/*
 * RevListCommand.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevErrorStream.h>
#include "RevListCommand.h"

using namespace rev;
using namespace std;

bool ListCommand::execute(int argc, const char** argv) {
	ostringstream oss;
	oss << "Executing command ls with arguments: " << endl;
	for (int i = 0; i < argc; i++) {
		oss << (i + 1) << ") " << argv[i] << endl;
	}
	if (argc == 0) {
		oss << "No arguments." << endl;
	}

	REV_DEBUG_MSG(oss.str());

	return true;
}
