/* 
 * File:   RevConsoleClient.cpp
 * Author: Revers
 * 
 * Created on 10 marzec 2013, 07:26
 *
 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <iostream>
#include <rev/common/RevErrorStream.h>
#include "RevConsoleClient.h"

using namespace rev;
using namespace std;

#define HEADER_SIZE (sizeof(int)+1)

ConsoleClient::ConsoleClient(const char* virtualFilename, int size) :
		sendFile(virtualFilename, size), sendCount(0) {
	revAssert(sendFile.getBaseAddress());

	if (!sendFile.wasCreated()) {
		char* baseAddress = (char*) sendFile.getBaseAddress();
		int* dest = (int*) baseAddress;
		sendCount = *dest;
	}
}

void ConsoleClient::send(const char* msg) {
	sendCount++; // Increment the send counter.
	size_t len = strlen(msg);

	const int maxCmdLength = sendFile.getSize() - HEADER_SIZE;

	if (len < maxCmdLength) {
		char* baseAddress = (char*) sendFile.getBaseAddress(); // get the base address of the shared memory
		int* dest = (int*) baseAddress;
		baseAddress += sizeof(int);
		memcpy(baseAddress, msg, len + 1); // First copy the message.
		*dest = sendCount; // Now revised the send count semaphore so the other process knows there is a new message to processs.
	} else {
		cout << "Command too long. Length: " << len << ", MaxLength: " << maxCmdLength << endl;
	}
}

void ConsoleClient::pushString(const char* str) {
	std::string s = str;
	commands.push_back(s);
	commandIndex = commands.size();
}

bool ConsoleClient::getInputString(char* buf, int len) {
	bool ret = false;
	int index = 0;
	bool exit = false;

	while (!exit) {
		if (_kbhit()) {
			int c = (int) _getch();
			if (c == 224) {
				c = 256 + (int) _getch();
			}

			if (c == 10 || c == 13) { // \r\n
				buf[index] = 0;
				pushString(buf);
				exit = true;
			} else if (c == 336) { // Down-Arrow
				commandIndex++;
				if (commandIndex < commands.size()) {
					for (int i = 0; i < index; i++) {
						cout << ((char) 8) << " " << ((char) 8);
					}
					const char* cmd = commands[commandIndex].c_str();
					strcpy(buf, cmd);
					index = strlen(buf);
					cout << buf;
				} else {
					commandIndex = commands.size();
				}
			} else if (c == 328) { // Up-Arrow
				if (commandIndex > 0) {
					commandIndex--;
					for (int i = 0; i < index; i++) {
						cout << ((char) 8) << " " << ((char) 8);
					}
					const char* cmd = commands[commandIndex].c_str();
					strcpy(buf, cmd);
					index = strlen(buf);
					cout << buf;
				}
			} else if (c == 27) { // Escape
				buf[index] = 0;
				exit = true;
				ret = true;
			} else if (c == 8) { // backspace
				if (index) {
					index--;
					cout << ((char) 8) << " " << ((char) 8);
				}
			} else {
				if (c >= 32 && c <= 127) { // normal letters
					buf[index] = c;
					index++;
					cout << ((char) c);
					if (index == (len - 1))
						break;
				}
			}
		}
	}

	buf[index] = 0;

	cout << endl;

	return ret;
}
