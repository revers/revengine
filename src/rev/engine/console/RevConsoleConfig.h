/*
 * RevConsoleConfig.h
 *
 *  Created on: 10-03-2013
 *      Author: Revers
 */

#ifndef REVCONSOLECONFIG_H_
#define REVCONSOLECONFIG_H_

#define REV_MM_IN_FILE_NAME "RevEngineFileIn"
#define REV_MM_OUT_FILE_NAME "RevEngineFileOut"
#define REV_MM_FILE_SIZE 1024

#endif /* REVCONSOLECONFIG_H_ */
