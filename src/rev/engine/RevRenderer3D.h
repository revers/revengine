/*
 * RevRenderer3D.h
 *
 *  Created on: 27-11-2012
 *      Author: Revers
 */

#ifndef REVRENDERER3D_H_
#define REVRENDERER3D_H_

#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <rev/common/RevDelete.hpp>
#include <rev/common/RevAssert.h>

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/picking/RevColorCodingPicker.h>
#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/concurrency/RevMutex.h>

namespace rev {
class PhongShadingEffect;
class NormalVisualizerEffect;
class GameObject;
class DebugEffect;
class CollisionComponent;
class CollisionSphere;
class CollisionPlane;
class CollisionBox;
}

namespace rev {

typedef std::vector<IVisual3DComponent*> Visual3DComponentVect;

class Renderer3D: public IBindable {
	static Renderer3D* renderer;
	static bool glewInited;

	Visual3DComponentVect components;
	rev::ColorCodingPicker picker;
	rev::NormalVisualizerEffect* normalVisualizerEffect = nullptr;
	rev::DebugEffect* debugEffect = nullptr;
	rev::Simple3DComponent* sphereComponent = nullptr;
	rev::Simple3DComponent* cuboidComponent = nullptr;
	rev::Simple3DComponent* planeComponent = nullptr;
	rev::Simple3DComponent* frustumComponent = nullptr;
	rev::GameObject* exclusiveGameObject = nullptr;
	rev::Mutex mutex;

	bool inited = false;
	int widths[REV_ENGINE_MAX_CONTEXTS];
	int heights[REV_ENGINE_MAX_CONTEXTS];
	rev::color3 backgroundColor = rev::color3(0.8f, 0.84f, 0.9f);

	bool wireframeMode = false;
	bool cullFaceMode = false;
	bool debugNormals = false;
	bool drawObjects = true;

	bool drawDebugElements = true;
	bool frustumCullingEnabled = true;
	bool drawAllBoundingSpheres = false;
	bool drawAllCollisionBoundingSpheres = false;
	bool drawAllCollisionGeometry = false;

	bool shadowsEnabled = true;
	bool drawShadowDepthMap = false;
	bool drawShadowSourceFrustum = false;

	bool deferredLightingEnabled = true;
	bool drawDeferredDepthMap = false;
	bool drawDeferredNormalMap = false;
	bool drawDeferredLightSpheres = false;
	bool drawDeferredLightMap = false;

	bool ssaoEnabled = true;
	bool drawSSAOMap = false;

	/**
	 * Currently rendered total polygon count.
	 */
	int totalRenderedPolygons[REV_ENGINE_MAX_CONTEXTS];

	/**
	 * Total components currently rendered.
	 */
	int totalRenderedObjects[REV_ENGINE_MAX_CONTEXTS];

	/**
	 * Total polygons on scene.
	 */
	int totalPolygons;

	Renderer3D();

	Renderer3D(const Renderer3D&);

public:
	DECLARE_BINDABLE_SINGLETON(Renderer3D)

	~Renderer3D();

	static void createSingleton();
	static void destroySingleton();

	static Renderer3D& ref() {
		return *renderer;
	}

	static Renderer3D* getInstance() {
		return renderer;
	}

	void update();
	void resize(int width, int height);
	void render();

	/**
	 * Renderer3D is NOT owner of added component.
	 */
	void addComponent(IVisual3DComponent* comp) {
		MutexGuard guard(&mutex);

		totalPolygons += comp->getPrimitiveCount();
		components.push_back(comp);
	}

	void removeComponent(IVisual3DComponent* comp) {
		MutexGuard guard(&mutex);

		totalPolygons -= comp->getPrimitiveCount();
		//components.remove(comp);
		components.erase(std::remove(components.begin(), components.end(), comp),
				components.end());
	}

	Visual3DComponentVect& getComponents() {
		return components;
	}

	void removeAllComponents() {
		components.clear();
	}

	bool isWireframeMode() const {
		return wireframeMode;
	}

	void setWireframeMode(bool wireframeMode) {
		this->wireframeMode = wireframeMode;
	}

	bool isCullFaceMode() const {
		return cullFaceMode;
	}

	void setCullFaceMode(bool cullFaceMode) {
		this->cullFaceMode = cullFaceMode;
	}

	bool isDebugNormals() const {
		return debugNormals;
	}

	void setDebugNormals(bool debugNormals) {
		this->debugNormals = debugNormals;
	}

	rev::ColorCodingPicker& getPicker() {
		return picker;
	}

	bool isDrawDebugElements() const {
		return drawDebugElements;
	}

	void setDrawDebugElements(bool drawDebugElements) {
		this->drawDebugElements = drawDebugElements;
	}

	/**
	 * Currently rendered total polygon count.
	 */
	int getTotalRenderedPolygons(int contextIndex) const {
		return totalRenderedPolygons[contextIndex];
	}

	/**
	 * Total components currently rendered.
	 */
	int getTotalRenderedObjects(int contextIndex) const {
		return totalRenderedObjects[contextIndex];
	}

	/**
	 * Total polygons in Renderer3D components.
	 */
	int getTotalPolygons() const {
		return totalPolygons;
	}

	/**
	 * Total components in Renderer3D.
	 */
	int getTotalObjects() const {
		return components.size();
	}

	bool isFrustumCullingEnabled() const {
		return frustumCullingEnabled;
	}

	void setFrustumCullingEnabled(bool frustumCullingEnabled) {
		this->frustumCullingEnabled = frustumCullingEnabled;
	}

	bool isDrawAllBoundingSpheres() const {
		return drawAllBoundingSpheres;
	}

	void setDrawAllBoundingSpheres(bool drawAllBoundingSpheres) {
		this->drawAllBoundingSpheres = drawAllBoundingSpheres;
	}

	bool isDrawAllCollisionBoundingSpheres() const {
		return drawAllCollisionBoundingSpheres;
	}

	void setDrawAllCollisionBoundingSpheres(bool draw) {
		this->drawAllCollisionBoundingSpheres = draw;
	}

	bool isDrawAllCollisionGeometry() const {
		return drawAllCollisionGeometry;
	}

	void setDrawAllCollisionGeometry(bool drawAllCollisionGeometry) {
		this->drawAllCollisionGeometry = drawAllCollisionGeometry;
	}

	bool isDrawObjects() const {
		return drawObjects;
	}

	void setDrawObjects(bool drawObjects) {
		this->drawObjects = drawObjects;
	}

	int getWidth(int contextIndex = 0) {
		return widths[contextIndex];
	}

	int getHeight(int contextIndex = 0) {
		return heights[contextIndex];
	}
	glm::ivec2 getViewportSize(int contextIndex = 0) {
		return glm::ivec2(widths[contextIndex], heights[contextIndex]);
	}
	bool isInited() {
		return inited;
	}

	int getGameObjectDrawId(GameObject* go);
	bool initContext(int ctx);
	bool init();

	rev::GameObject* getExclusiveGameObject() {
		return exclusiveGameObject;
	}

	void setExclusiveGameObject(rev::GameObject* exclusiveGameObject) {
		this->exclusiveGameObject = exclusiveGameObject;
	}

	bool isExclusiveMode() {
		return exclusiveGameObject != nullptr;
	}

	void turnOffExclusiveMode() {
		setExclusiveGameObject(nullptr);
	}

	bool isProperForRender(IVisual3DComponent* c);

	const rev::color3& getBackgroundColor() const {
		return backgroundColor;
	}

	void setBackgroundColor(const rev::color3& backgroundColor) {
		glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1.0f);
		this->backgroundColor = backgroundColor;
	}

	void setBackgroundColor(float r, float g, float b) {
		this->backgroundColor = rev::color3(r, g, b);
	}

	void setDefaultClearColor() {
		glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1.0f);
	}

	bool isShadowsEnabled() const {
		return shadowsEnabled;
	}

	void setShadowsEnabled(bool shadowsEnabled) {
		this->shadowsEnabled = shadowsEnabled;
	}

	bool isDrawShadowDepthMap() const {
		return drawShadowDepthMap;
	}

	void setDrawShadowDepthMap(bool drawShadowDepthMap) {
		this->drawShadowDepthMap = drawShadowDepthMap;
	}

	bool isDrawShadowSourceFrustum() const {
		return drawShadowSourceFrustum;
	}

	void setDrawShadowSourceFrustum(bool drawShadowSourceFrustum) {
		this->drawShadowSourceFrustum = drawShadowSourceFrustum;
	}

	bool isDeferredLightingEnabled() const {
		return deferredLightingEnabled;
	}

	void setDeferredLightingEnabled(bool deferredLightingEnabled) {
		this->deferredLightingEnabled = deferredLightingEnabled;
	}

	bool isDrawDeferredDepthMap() const {
		return drawDeferredDepthMap;
	}

	void setDrawDeferredDepthMap(bool drawDeferredDepthMap) {
		this->drawDeferredDepthMap = drawDeferredDepthMap;
	}

	bool isDrawDeferredNormalMap() const {
		return drawDeferredNormalMap;
	}

	void setDrawDeferredNormalMap(bool drawDeferredNormalMap) {
		this->drawDeferredNormalMap = drawDeferredNormalMap;
	}

	bool isDrawDeferredLightSpheres() const {
		return drawDeferredLightSpheres;
	}

	void setDrawDeferredLightSpheres(bool drawDeferredLightSpheres) {
		this->drawDeferredLightSpheres = drawDeferredLightSpheres;
	}

	bool isDrawDeferredLightMap() const {
		return drawDeferredLightMap;
	}

	void setDrawDeferredLightMap(bool drawDeferredLightMap) {
		this->drawDeferredLightMap = drawDeferredLightMap;
	}

	bool isDrawSSAOMap() const {
		return drawSSAOMap;
	}

	void setDrawSSAOMap(bool drawSSAOMap) {
		this->drawSSAOMap = drawSSAOMap;
	}

	bool isSSAOEnabled() const {
		return ssaoEnabled;
	}

	void setSSAOEnabled(bool ssaoEnabled) {
		this->ssaoEnabled = ssaoEnabled;
	}

private:
	void standardRender();
	void pickingRender();
	void normalsRender();
	void shadowRender();
	void ssaoRender();
	void deferredLightingRender();
	void frustumCullingTest();
	void drawCollisionSphere(CollisionSphere* sphere, CollisionComponent* comp);
	void drawCollisionBox(CollisionBox* box, CollisionComponent* comp);
	void drawCollisionPlane(CollisionPlane* plane, CollisionComponent* comp);
	void drawCameraFrustum(ICamera* camera, int contextIndex);
	void drawShadowFrustum(ICamera* camera, int contextIndex);

	void renderBoundingSphere(const rev::BoundingSphere* bs,
			const rev::ICamera& camera, rev::IVisual3DComponent& c);
	void renderCollisionBoundingSpheres();
	void renderCollisionGeometry();
	bool contextSensitiveRender();

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVRENDERER3D_H_ */
