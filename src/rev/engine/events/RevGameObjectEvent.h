/*
 * RevGameObjectEvent.h
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#ifndef REVGAMEOBJECTEVENT_H_
#define REVGAMEOBJECTEVENT_H_

#include "RevEvent.h"
#include <rev/engine/RevGameObject.h>

namespace rev {

    class GameObjectEvent: public Event {
        GameObject* gameObject;

        GameObjectEvent(EventType eventType, GameObject* gameObject,
                int contextIndex) :
                Event(eventType, contextIndex), gameObject(gameObject) {
        }

    public:
        using Event::operator new;
        using Event::operator delete;

        static GameObjectEvent* createGameObjectAddedEvent(GameObject* gameObject,
                int contextIndex) {
            return new GameObjectEvent(EventType::GAME_OBJECT_ADDED, gameObject,
                    contextIndex);
        }

        static GameObjectEvent* createGameObjectRemovedEvent(
                GameObject* gameObject, int contextIndex) {
            return new GameObjectEvent(EventType::GAME_OBJECT_REMOVED, gameObject,
                    contextIndex);
        }

        static GameObjectEvent* createAllGameObjectsRemovedEvent(int contextIndex) {
            return new GameObjectEvent(EventType::ALL_GAME_OBJECTS_REMOVED,
                    nullptr, contextIndex);
        }

        virtual ~GameObjectEvent() {
        }

        GameObject* getGameObject() const {
            return gameObject;
        }

    };

} /* namespace rev */

#endif /* REVGAMEOBJECTEVENT_H_ */
