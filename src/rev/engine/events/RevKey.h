/*
 * RevKey.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVKEY_H_
#define REVKEY_H_

#include <rev/engine/config/RevEngineConfig.h>

#ifdef REV_ENGINE_QT
#include <qnamespace.h>
#elif defined(REV_ENGINE_GLFW)
// TODO: GLFW

#endif

namespace rev {

#ifdef REV_ENGINE_QT
    enum class Key {
        NO_KEY = 0,
        KEY_ESCAPE = Qt::Key_Escape,
        KEY_TAB = Qt::Key_Tab,
        KEY_BACKTAB = Qt::Key_Backtab,
        KEY_BACKSPACE = Qt::Key_Backspace,
        KEY_RETURN = Qt::Key_Return,
        KEY_ENTER = Qt::Key_Enter,
        KEY_INSERT = Qt::Key_Insert,
        KEY_DELETE = Qt::Key_Delete,
        KEY_PAUSE = Qt::Key_Pause,
        KEY_PRINT = Qt::Key_Print,
        KEY_SYS_REQ = Qt::Key_SysReq,
        KEY_CLEAR = Qt::Key_Clear,
        KEY_HOME = Qt::Key_Home,
        KEY_END = Qt::Key_End,
        KEY_LEFT = Qt::Key_Left,
        KEY_UP = Qt::Key_Up,
        KEY_RIGHT = Qt::Key_Right,
        KEY_DOWN = Qt::Key_Down,
        KEY_PAGE_UP = Qt::Key_PageUp,
        KEY_PAGE_DOWN = Qt::Key_PageDown,
        KEY_SHIFT = Qt::Key_Shift,
        KEY_CONTROL = Qt::Key_Control,
        KEY_META = Qt::Key_Meta,
        KEY_ALT = Qt::Key_Alt,
        KEY_CAPS_LOCK = Qt::Key_CapsLock,
        KEY_NUM_LOCK = Qt::Key_NumLock,
        KEY_SCROLL_LOCK = Qt::Key_ScrollLock,
        KEY_F1 = Qt::Key_F1,
        KEY_F2 = Qt::Key_F2,
        KEY_F3 = Qt::Key_F3,
        KEY_F4 = Qt::Key_F4,
        KEY_F5 = Qt::Key_F5,
        KEY_F6 = Qt::Key_F6,
        KEY_F7 = Qt::Key_F7,
        KEY_F8 = Qt::Key_F8,
        KEY_F9 = Qt::Key_F9,
        KEY_F10 = Qt::Key_F10,
        KEY_F11 = Qt::Key_F11,
        KEY_F12 = Qt::Key_F12,
        KEY_MENU = Qt::Key_Menu,
        KEY_HELP = Qt::Key_Help,
        KEY_SPACE = Qt::Key_Space,
        KEY_ANY = Qt::Key_Any,
        KEY_EXCLAM = Qt::Key_Exclam,
        KEY_QUOTE_DOUBLE = Qt::Key_QuoteDbl,
        KEY_NUMBER_SIGN = Qt::Key_NumberSign,
        KEY_DOLLAR = Qt::Key_Dollar,
        KEY_PERCENT = Qt::Key_Percent,
        KEY_AMPERSAND = Qt::Key_Ampersand,
        KEY_APOSTROPHE = Qt::Key_Apostrophe,
        KEY_PEREN_LEFT = Qt::Key_ParenLeft,
        KEY_PAREN_RIGHT = Qt::Key_ParenRight,
        KEY_ASTERISK = Qt::Key_Asterisk,
        KEY_PLUS = Qt::Key_Plus,
        KEY_COMMA = Qt::Key_Comma,
        KEY_MINUS = Qt::Key_Minus,
        KEY_PERIOD = Qt::Key_Period,
        KEY_SLASH = Qt::Key_Slash,
        KEY_KEY_0 = Qt::Key_0,
        KEY_KEY_1 = Qt::Key_1,
        KEY_KEY_2 = Qt::Key_2,
        KEY_KEY_3 = Qt::Key_3,
        KEY_KEY_4 = Qt::Key_4,
        KEY_KEY_5 = Qt::Key_5,
        KEY_KEY_6 = Qt::Key_6,
        KEY_KEY_7 = Qt::Key_7,
        KEY_KEY_8 = Qt::Key_8,
        KEY_KEY_9 = Qt::Key_9,
        KEY_COLON = Qt::Key_Colon,
        KEY_SEMICOLON = Qt::Key_Semicolon,
        KEY_LESS = Qt::Key_Less,
        KEY_EQUAL = Qt::Key_Equal,
        KEY_GREATER = Qt::Key_Greater,
        KEY_QUESTION = Qt::Key_Question,
        KEY_AT = Qt::Key_At,
        KEY_A = Qt::Key_A,
        KEY_B = Qt::Key_B,
        KEY_C = Qt::Key_C,
        KEY_D = Qt::Key_D,
        KEY_E = Qt::Key_E,
        KEY_F = Qt::Key_F,
        KEY_G = Qt::Key_G,
        KEY_H = Qt::Key_H,
        KEY_I = Qt::Key_I,
        KEY_J = Qt::Key_J,
        KEY_K = Qt::Key_K,
        KEY_L = Qt::Key_L,
        KEY_M = Qt::Key_M,
        KEY_N = Qt::Key_N,
        KEY_O = Qt::Key_O,
        KEY_P = Qt::Key_P,
        KEY_Q = Qt::Key_Q,
        KEY_R = Qt::Key_R,
        KEY_S = Qt::Key_S,
        KEY_T = Qt::Key_T,
        KEY_U = Qt::Key_U,
        KEY_V = Qt::Key_V,
        KEY_W = Qt::Key_W,
        KEY_X = Qt::Key_X,
        KEY_Y = Qt::Key_Y,
        KEY_Z = Qt::Key_Z,
        KEY_BRACKET_LEFT = Qt::Key_BracketLeft,
        KEY_BACKSLASH = Qt::Key_Backslash,
        KEY_BRACKET_RIGHT = Qt::Key_BracketRight,
        KEY_ASCII_CIRCUM = Qt::Key_AsciiCircum,
        KEY_UNDERSCORE = Qt::Key_Underscore,
        KEY_QUOTE_LEFT = Qt::Key_QuoteLeft,
        KEY_BRACE_LEFT = Qt::Key_BraceLeft,
        KEY_BAR = Qt::Key_Bar,
        KEY_BRACE_RIGHT = Qt::Key_BraceRight,
        KEY_ASCII_TILDE = Qt::Key_AsciiTilde,
    };

    enum class KeyModifier {
        NO_MODIFIER = 0x00000000,
        MODIFIER_KEY_META = Qt::MetaModifier,
        MODIFIER_SHIFT = Qt::ShiftModifier,
        MODIFIER_CTRL = Qt::ControlModifier,
        MODIFIER_ALT = Qt::AltModifier,
    };

#elif defined(REV_ENGINE_GLFW)
// TODO: GLFW

#endif
} // namespace rev

inline int operator|(const rev::KeyModifier& a, const rev::KeyModifier& b) {
    return (int) a | (int) b;
}

inline int operator&(const rev::KeyModifier& a, const rev::KeyModifier& b) {
    return (int) a & (int) b;
}

#endif /* REVKEY_H_ */
