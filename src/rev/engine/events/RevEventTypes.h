/*
 * RevEventTypes.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVEVENTTYPES_H_
#define REVEVENTTYPES_H_

#include <ostream>

namespace rev {

	enum class EventType {
		INIT,
		DESTROY,
		GAME_OBJECT_ADDED,
		GAME_OBJECT_REMOVED,
		ALL_GAME_OBJECTS_REMOVED,
		ON_COLLISION,
		OBJECT_PICKED,
		PICKING_RESETED,
		MOUSE_PRESSED,
		MOUSE_RELEASED,
		MOUSE_MOVED_EVENT, // "MOUSE_MOVED" is a macro in wincon.h
		MOUSE_WHEEL,
		KEY_PRESSED,
		KEY_RELEASED,
		ENGINE_EVENT,
		SCENE_LOADED,
		SCRIPTING
	};

} // namepsace rev

std::ostream& operator <<(std::ostream& out, const rev::EventType& e);

#endif /* REVEVENTTYPES_H_ */
