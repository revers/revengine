/*
 * RevKeyEvent.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVKEYEVENT_H_
#define REVKEYEVENT_H_

#include "RevEvent.h"
#include "RevKey.h"

namespace rev {

    class KeyEvent: public Event {
        const Key key;
        const int modifiers;

        KeyEvent(EventType eventType, Key key, int modifiers, int contextIndex) :
                Event(eventType, contextIndex), key(key), modifiers(modifiers) {
        }

    public:
        using Event::operator new;
        using Event::operator delete;

        static KeyEvent* createKeyPressedEvent(Key key, int modifiers, int contextIndex) {
            return new KeyEvent(EventType::KEY_PRESSED, key, modifiers, contextIndex);
        }

        static KeyEvent* createKeyReleasedEvent(Key key, int modifiers, int contextIndex) {
            return new KeyEvent(EventType::KEY_RELEASED, key, modifiers, contextIndex);
        }

        Key getKey() const {
            return key;
        }

        int getModifiers() const {
            return modifiers;
        }

        bool isModifierActive(int modifier) const {
            return modifiers & modifier;
        }

        bool isModifierActive(KeyModifier modifier) const {
            return modifiers & (int) modifier;
        }
    };

} /* namespace rev */
#endif /* REVKEYEVENT_H_ */
