/*
 * RevMouse.h
 *
 *  Created on: 31-12-2012
 *      Author: Revers
 */

#ifndef REVMOUSE_H_
#define REVMOUSE_H_

#include "RevMouseState.h"

namespace rev {

    class Mouse {
        Mouse() = delete;
        ~Mouse() = delete;

    public:
        static MouseState getState();
    };

} /* namespace rev */
#endif /* REVMOUSE_H_ */
