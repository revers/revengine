/*
 * RevPickingEvent.h
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#ifndef REVPICKINGEVENT_H_
#define REVPICKINGEVENT_H_

#include <rev/engine/picking/RevPixelInfo.h>
#include <rev/engine/RevGameObject.h>
#include "RevEvent.h"

namespace rev {

    class PickingEvent: public Event {
        GameObject* gameObject;
        const PixelInfo pixelInfo;

        PickingEvent(GameObject* gameObject, const PixelInfo& pixelInfo,
                int contextIndex) :
                Event(EventType::OBJECT_PICKED, contextIndex), gameObject(
                        gameObject),
                        pixelInfo(pixelInfo) {
        }

        PickingEvent(EventType eventType, int contextIndex) :
                Event(eventType, contextIndex), gameObject(nullptr) {
        }
    public:
        using Event::operator new;
        using Event::operator delete;

        static PickingEvent* createObjectPickedEvent(GameObject* gameObject,
                const PixelInfo& pixelInfo, int contextIndex) {
            return new PickingEvent(gameObject, pixelInfo, contextIndex);
        }

        static PickingEvent* createPickingResetedEvent(int contextIndex) {
            return new PickingEvent(EventType::PICKING_RESETED, contextIndex);
        }

        GameObject* getGameObject() const {
            return gameObject;
        }

        const PixelInfo& getPixelInfo() const {
            return pixelInfo;
        }
    };

} /* namespace rev */
#endif /* REVPICKINGEVENT_H_ */
