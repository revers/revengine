/*
 * RevScriptEvent.h
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTEVENT_H_
#define REVSCRIPTEVENT_H_

#include <rev/engine/components/RevScriptingComponent.h>
#include "RevEvent.h"

namespace rev {

	class ScriptEvent: public Event {
	public:
		enum class Type {
			COMPILED,
			INVALID,
			STATISTICS_CHANGED
		};

	private:
		ScriptingComponent* component;
		Type type;

	public:
		using Event::operator new;
		using Event::operator delete;

		ScriptEvent(ScriptingComponent* component, Type type, int contextIndex) :
				Event(EventType::SCRIPTING, contextIndex), component(component), type(type) {
		}

		ScriptingComponent* getComponent() const {
			return component;
		}
		Type getType() const {
			return type;
		}
		bool isTypeInvalid() const {
			return type == Type::INVALID;
		}
		bool isTypeCompiled() const {
			return type == Type::COMPILED;
		}
		bool isTypeStatisticsChanged() const {
			return type == Type::STATISTICS_CHANGED;
		}

	};
} /* namespace rev */

#endif /* REVSCRIPTEVENT_H_ */
