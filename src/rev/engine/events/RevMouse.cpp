/*
 * RevMouse.cpp
 *
 *  Created on: 31-12-2012
 *      Author: Revers
 */

#include <rev/engine/config/RevEngineConfig.h>
#include "RevMouse.h"

using namespace rev;

#ifdef REV_ENGINE_QT
#include <rev/engine/qt/QtUtil.h>

rev::MouseState rev::Mouse::getState() {
	return QtUtil::getMouseState();
}

#elif defined(REV_ENGINE_GLFW)
// TODO: GLFW

#endif
