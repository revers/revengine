/*
 * RevEvent.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVEVENT_H_
#define REVEVENT_H_

#include "RevEventTypes.h"
#include "RevEventManager.h"
#include <rev/engine/memory/RevMemoryPool.h>

namespace rev {

    class Event {
    private:
        int contextIndex;

    protected:
        const EventType eventType;
        bool consumed = false;

    public:
        Event(EventType eventType, int contextIndex) :
                eventType(eventType), contextIndex(contextIndex) {
        }

        void* operator new(size_t size) {
            return EventManager::ref().eventMemoryPool->alloc(size);
        }

        void operator delete(void* p) {
            EventManager::ref().eventMemoryPool->free(p);
        }

        virtual ~Event() {
        }

        EventType getType() const {
            return eventType;
        }

        bool isConsumed() const {
            return consumed;
        }

        void consume() {
            consumed = true;
        }

        int getContextIndex() const {
            return contextIndex;
        }
    };
} /* namespace rev */
#endif /* REVEVENT_H_ */
