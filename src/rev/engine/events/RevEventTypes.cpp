/*
 * RevEventTypes.cpp
 *
 *  Created on: 12-03-2013
 *      Author: Revers
 */

#include "RevEventTypes.h"

std::ostream& operator <<(std::ostream& out, const rev::EventType& e) {
	switch (e) {
	case rev::EventType::INIT:
		{
		out << "INIT";
		break;
	}
	case rev::EventType::DESTROY:
		{
		out << "DESTROY";
		break;
	}
	case rev::EventType::GAME_OBJECT_ADDED:
		{
		out << "GAME_OBJECT_ADDED";
		break;
	}
	case rev::EventType::GAME_OBJECT_REMOVED:
		{
		out << "GAME_OBJECT_REMOVED";
		break;
	}
	case rev::EventType::ALL_GAME_OBJECTS_REMOVED:
		{
		out << "ALL_GAME_OBJECTS_REMOVED";
		break;
	}
	case rev::EventType::ON_COLLISION:
		{
		out << "ON_COLLISION";
		break;
	}
	case rev::EventType::OBJECT_PICKED:
		{
		out << "OBJECT_PICKED";
		break;
	}
	case rev::EventType::PICKING_RESETED:
		{
		out << "PICKING_RESETED";
		break;
	}
	case rev::EventType::MOUSE_PRESSED:
		{
		out << "MOUSE_PRESSED";
		break;
	}
	case rev::EventType::MOUSE_RELEASED:
		{
		out << "MOUSE_RELEASED";
		break;
	}
	case rev::EventType::MOUSE_MOVED_EVENT:
		{
		out << "MOUSE_MOVED_EVENT";
		break;
	}
	case rev::EventType::MOUSE_WHEEL:
		{
		out << "MOUSE_WHEEL";
		break;
	}
	case rev::EventType::KEY_PRESSED:
		{
		out << "KEY_PRESSED";
		break;
	}
	case rev::EventType::KEY_RELEASED:
		{
		out << "KEY_RELEASED";
		break;
	}
	case rev::EventType::ENGINE_EVENT:
		{
		out << "ENGINE_EVENT";
		break;
	}
	default:
		{
		out << "UNKNOWN";
		break;
	}
	}
	return out;
}
