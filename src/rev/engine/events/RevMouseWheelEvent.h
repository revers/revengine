/*
 * RevMouseWheelEvent.h
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#ifndef REVMOUSEWHEELEVENT_H_
#define REVMOUSEWHEELEVENT_H_

#include "RevEvent.h"

namespace rev {

    class MouseWheelEvent: public Event {
        const int difference;

    public:
        using Event::operator new;
        using Event::operator delete;

        MouseWheelEvent(int difference, int contextIndex) :
                Event(EventType::MOUSE_WHEEL, contextIndex), difference(difference) {
        }

        int getDifference() const {
            return difference;
        }
    };

} // namespace rev

#endif /* REVMOUSEWHEELEVENT_H_ */
