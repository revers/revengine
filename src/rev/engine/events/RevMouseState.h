/*
 * RevMouseState.h
 *
 *  Created on: 15-03-2013
 *      Author: Revers
 */

#ifndef REVMOUSESTATE_H_
#define REVMOUSESTATE_H_

#include "RevMouseButton.h"

namespace rev {

	/********************************************
	 MouseState
	 ********************************************/
	class MouseState {
		int buttons;
		int x;
		int y;

	public:
		MouseState() :
				buttons(0), x(0), y(0) {
		}

		MouseState(int buttons, int x, int y) :
				buttons(buttons), x(x), y(y) {
		}

		int getButtons() const {
			return buttons;
		}

		bool isPressed(MouseButton b) const {
			return buttons & (int) b;
		}

		bool isPressed(int b) const {
			return buttons & b;
		}

		bool isLeftPressed() const {
			return buttons & (int) MouseButton::BUTTON_LEFT;
		}

		bool isRightPressed() const {
			return buttons & (int) MouseButton::BUTTON_RIGHT;
		}

		bool isMiddlePressed() const {
			return buttons & (int) MouseButton::BUTTON_MIDDLE;
		}

		int getX() const {
			return x;
		}

		int getY() const {
			return y;
		}
	};
}

#endif /* REVMOUSESTATE_H_ */
