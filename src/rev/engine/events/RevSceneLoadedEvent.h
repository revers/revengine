/*
 * RevSceneLoadedEvent.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVSCENELOADEDEVENT_H_
#define REVSCENELOADEDEVENT_H_

#include <functional>
#include "RevEvent.h"

namespace rev {

	class SceneLoadedEvent: public Event {
	public:
		using Event::operator new;
		using Event::operator delete;

		SceneLoadedEvent(int contextIndex) :
				Event(EventType::SCENE_LOADED, contextIndex) {
		}

	};

} /* namespace rev */

#endif /* REVSCENELOADEDEVENT_H_ */
