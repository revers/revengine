/*
 * RevKeyboard.h
 *
 *  Created on: 31-12-2012
 *      Author: Revers
 */

#ifndef REVKEYBOARD_H_
#define REVKEYBOARD_H_

#include "RevKey.h"

namespace rev {

    /********************************************
     KeyState
     ********************************************/
    class KeyState {
        friend class Keyboard;
        Key key;
        int modifiers;

        KeyState(Key key, int modifiers) :
                key(key), modifiers(modifiers) {
        }

    public:

        Key getKey() const {
            return key;
        }

        int getModifiers() const {
            return modifiers;
        }

        bool isModifierActive(int modifier) const {
            return modifiers & modifier;
        }

        bool isModifierActive(KeyModifier modifier) const {
            return modifiers & (int) modifier;
        }
    };

    /********************************************
     Keyboard
     ********************************************/
    class Keyboard {
        Keyboard();
        ~Keyboard();

    public:
        static KeyState getState();
    };

} /* namespace rev */
#endif /* REVKEYBOARD_H_ */
