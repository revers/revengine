/*
 * RevMouseButton.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVMOUSEBUTTON_H_
#define REVMOUSEBUTTON_H_

#include <rev/engine/config/RevEngineConfig.h>

#ifdef REV_ENGINE_QT
#include <qnamespace.h>
#elif defined(REV_ENGINE_GLFW)
// TODO: GLFW

#endif

namespace rev {

#ifdef REV_ENGINE_QT

    /********************************************
     MouseButton
     ********************************************/
    enum class MouseButton {
        NO_BUTTON = 0, // for mouse moved event
        BUTTON_LEFT = Qt::LeftButton,
        BUTTON_RIGHT = Qt::RightButton,
        BUTTON_MIDDLE = Qt::MiddleButton,
    };

#elif defined(REV_ENGINE_GLFW)
// TODO: GLFW
#endif
} // namespace rev

inline int operator|(const rev::MouseButton& a, const rev::MouseButton& b) {
    return (int) a | (int) b;
}

inline int operator&(const rev::MouseButton& a, const rev::MouseButton& b) {
    return (int) a & (int) b;
}
#endif /* REVMOUSEBUTTON_H_ */
