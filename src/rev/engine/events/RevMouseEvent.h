/*
 * RevMouseEvent.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVMOUSEEVENT_H_
#define REVMOUSEEVENT_H_

#include "RevEvent.h"
#include "RevMouseButton.h"

namespace rev {

    class MouseEvent: public Event {
        const int posX;
        const int posY;
        const MouseButton mouseButton;

        MouseEvent(EventType eventType, int posX,
                int posY, MouseButton mouseButton, int contextIndex) :
                Event(eventType, contextIndex), posX(posX), posY(posY),
                        mouseButton(mouseButton) {
        }

    public:
        using Event::operator new;
        using Event::operator delete;

        static MouseEvent* createMousePressedEvent(int posX,
                int posY, MouseButton mouseButton, int contextIndex) {
            return new MouseEvent(EventType::MOUSE_PRESSED, posX,
                    posY, mouseButton, contextIndex);
        }

        static MouseEvent* createMouseReleasedEvent(int posX,
                int posY, MouseButton mouseButton, int contextIndex) {
            return new MouseEvent(EventType::MOUSE_RELEASED, posX,
                    posY, mouseButton, contextIndex);
        }

        static MouseEvent* createMouseMovedEvent(int posX, int posY,
                int contextIndex) {
            return new MouseEvent(EventType::MOUSE_MOVED_EVENT, posX,
                    posY, MouseButton::NO_BUTTON, contextIndex);
        }

        virtual ~MouseEvent() {
        }

        int getX() const {
            return posX;
        }

        int getY() const {
            return posY;
        }

        MouseButton getButton() const {
            return mouseButton;
        }
    };

} /* namespace rev */
#endif /* REVMOUSEEVENT_H_ */
