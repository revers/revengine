/*
 * RevEventManager.cpp
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevDelete.hpp>
#include <rev/engine/memory/RevMemoryPool.h>
#include <rev/engine/RevEngine.h>

#include "RevEventManager.h"
#include "RevMouseEvent.h"
#include "RevKeyEvent.h"
#include "RevMouseWheelEvent.h"
#include "RevPickingEvent.h"
#include "RevGameObjectEvent.h"
#include "RevEngineEvent.h"
#include "RevScriptEvent.h"
#include "RevSceneLoadedEvent.h"
#include <rev/common/RevAssert.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include <rev/engine/components/RevIEventListenerComponent.h>

#if REV_ENGINE_MAX_CONTEXTS > 1
#include <rev/engine/glcontext/RevGLContextManager.h>
#endif

using namespace rev;

rev::EventManager* rev::EventManager::eventManager = nullptr;

#define POOL_SIZE 1000
#define MAX(a, b) ((a) < (b) ? (b) : (a))

EventManager::EventManager() {
	/*
	 * TODO: IMPORTANT! Every event should be compared below:
	 */
	int unitSize = sizeof(KeyEvent);
	unitSize = MAX(unitSize, sizeof(MouseEvent));
	unitSize = MAX(unitSize, sizeof(MouseWheelEvent));
	unitSize = MAX(unitSize, sizeof(PickingEvent));
	unitSize = MAX(unitSize, sizeof(GameObjectEvent));
	unitSize = MAX(unitSize, sizeof(EngineEvent));
	unitSize = MAX(unitSize, sizeof(ScriptEvent));
	unitSize = MAX(unitSize, sizeof(SceneLoadedEvent));
	eventMemoryPool = new MemoryPool(POOL_SIZE, unitSize);
}

EventManager::~EventManager() {
	removeAllEvents();
	DEL(eventMemoryPool);
}

void EventManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(!eventManager);
	eventManager = new EventManager();
	REV_TRACE_FUNCTION_OUT;
}

void EventManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(eventManager);
	delete eventManager;
	eventManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void EventManager::removeAllEvents() {
	REV_TRACE_FUNCTION;

	DEL(lastMousePressedEvent);
	DEL(lastMouseReleasedEvent);
	DEL(lastMouseMovedEvent);
	DEL(lastWheelEvent);
	DEL(lastKeyPressedEvent);
	DEL(lastKeyReleasedEvent);

	for (Event*& e : eventVectorA) {
		delete e;
	}
	eventVectorA.clear();

	for (Event*& e : eventVectorB) {
		delete e;
	}
	eventVectorB.clear();
}

void EventManager::update() {

	EventVector* eventVector;
	if (currentA) {
		eventVector = &eventVectorA;
		currentEventVector = &eventVectorB;
	} else {
		eventVector = &eventVectorB;
		currentEventVector = &eventVectorA;
	}

	currentA = !currentA;

	DEL(lastMousePressedEvent);
	DEL(lastMouseReleasedEvent);
	DEL(lastMouseMovedEvent);
	DEL(lastWheelEvent);
	DEL(lastKeyPressedEvent);
	DEL(lastKeyReleasedEvent);

	for (int i = 0; i < eventVector->size(); i++) {
		Event*& e = eventVector->operator [](i);
		switch (e->getType()) {
		case EventType::ENGINE_EVENT: {
			EngineEvent* engineEvent = static_cast<EngineEvent*>(e);
			std::function<bool()> callback = engineEvent->getCallback();
			if (!callback()) {
				REV_ERROR_MSG("EngineEvent::callback(): '" << engineEvent->getName()
						<< "': FAILED!");
				revAssert(false);
			}

			break;
		}
		case EventType::GAME_OBJECT_ADDED: {
			for (IEventListenerComponent*& c : components) {
				c->gameObjectAdded(*((GameObjectEvent*) e));
			}
			break;
		}
		case EventType::GAME_OBJECT_REMOVED: {
			for (IEventListenerComponent*& c : components) {
				c->gameObjectRemoved(*((GameObjectEvent*) e));
			}
			break;
		}
		case EventType::ALL_GAME_OBJECTS_REMOVED: {
			for (IEventListenerComponent*& c : components) {
				c->allGameObjectsRemoved(*((GameObjectEvent*) e));
			}
			break;
		}
		case EventType::SCRIPTING: {
			for (IEventListenerComponent*& c : components) {
				c->scriptingEvent(*((ScriptEvent*) e));
			}
			break;
		}
		case EventType::ON_COLLISION: {
			assertTODO(EventType::ON_COLLISION);
			break;
		}
		case EventType::OBJECT_PICKED: {
			for (IEventListenerComponent*& c : components) {
				c->objectPicked(*((PickingEvent*) e));
			}
			break;
		}
		case EventType::PICKING_RESETED: {
			for (IEventListenerComponent*& c : components) {
				c->pickingReseted();
			}
			break;
		}
		case EventType::SCENE_LOADED: {
			for (IEventListenerComponent*& c : components) {
				c->sceneLoaded();
			}
			break;
		}
		case EventType::MOUSE_PRESSED: {
			MouseEvent& evt = *((MouseEvent*) e);
			DEL(lastMousePressedEvent);
			lastMousePressedEvent = new MouseEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->mousePressed(evt);
			}
			break;
		}
		case EventType::MOUSE_RELEASED: {
			MouseEvent& evt = *((MouseEvent*) e);
			DEL(lastMouseReleasedEvent);
			lastMouseReleasedEvent = new MouseEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->mouseReleased(evt);
			}
			break;
		}
		case EventType::MOUSE_MOVED_EVENT: {
			MouseEvent& evt = *((MouseEvent*) e);
			DEL(lastMouseMovedEvent);
			lastMouseMovedEvent = new MouseEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->mouseMoved(evt);
			}
			break;
		}
		case EventType::MOUSE_WHEEL: {
			MouseWheelEvent& evt = *((MouseWheelEvent*) e);
			DEL(lastWheelEvent);
			lastWheelEvent = new MouseWheelEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->mouseWheelMoved(evt);
			}
			break;
		}
		case EventType::KEY_PRESSED: {
			KeyEvent& evt = *((KeyEvent*) e);
			DEL(lastKeyPressedEvent);
			lastKeyPressedEvent = new KeyEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->keyPressed(evt);
			}
			break;
		}
		case EventType::KEY_RELEASED: {
			KeyEvent& evt = *((KeyEvent*) e);
			DEL(lastKeyReleasedEvent);
			lastKeyReleasedEvent = new KeyEvent(evt);
			for (IEventListenerComponent*& c : components) {
				c->keyReleased(evt);
			}
			break;
		}
		default: {
			alwaysAssertMsg(false,
					"Unhandled event type: " << (int) (e->getType()));
			break;
		}

		}

		/*
		 * If nobody has cleared eventVector (for example,
		 * by EventManager::removeAllEvents()):
		 */
		if (!eventVector->empty()) {
			delete e;
		}
	}

	for (IEventListenerComponent*& c : components) {
		c->update();
	}

	eventVector->clear();
}

void EventManager::fireMousePressedEvent(int x, int y, rev::MouseButton button) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			MouseEvent::createMousePressedEvent(x, y, button, contextIndex));
}

void EventManager::fireMouseReleasedEvent(int x, int y, rev::MouseButton button) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			MouseEvent::createMouseReleasedEvent(x, y, button, contextIndex));
}

void EventManager::fireMouseMovedEvent(int x, int y) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			MouseEvent::createMouseMovedEvent(x, y, contextIndex));
}

void EventManager::fireKeyPressedEvent(rev::Key key, int modifiers) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			KeyEvent::createKeyPressedEvent(key, modifiers, contextIndex));
}

void EventManager::fireKeyReleasedEvent(rev::Key key, int modifiers) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			KeyEvent::createKeyReleasedEvent(key, modifiers, contextIndex));
}

void EventManager::fireMouseWheelEvent(int difference) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(new MouseWheelEvent(difference, contextIndex));
}

void EventManager::fireObjectPickedEvent(rev::GameObject* gameObject,
		const PixelInfo& pixelInfo) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			PickingEvent::createObjectPickedEvent(gameObject, pixelInfo,
					contextIndex));
}

void EventManager::firePickingResetedEvent() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			PickingEvent::createPickingResetedEvent(contextIndex));
}

void EventManager::fireGameObjectAddedEvent(GameObject* gameObject) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif
	currentEventVector->push_back(
			GameObjectEvent::createGameObjectAddedEvent(gameObject, contextIndex));
//	REV_INFO_MSG("Adding '" << gameObject->getName() << "' (" << gameObject->getId()
//			<< "); eventVector.size() = " << currentEventVector->size()
//			<< "; TOTAL GAME OBJECTS: " << Engine::ref().getGameObjects().size());
}

void EventManager::fireGameObjectRemovedEvent(GameObject* gameObject) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			GameObjectEvent::createGameObjectRemovedEvent(gameObject,
					contextIndex));
}

void EventManager::fireAllGameObjectsRemovedEvent() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			GameObjectEvent::createAllGameObjectsRemovedEvent(contextIndex));
}

void EventManager::fireEngineEvent(const char* name, std::function<bool()> callback) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(new EngineEvent(name, callback, contextIndex));
}

void EventManager::fireScriptInvalidEvent(ScriptingComponent* c) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(new ScriptEvent(c, ScriptEvent::Type::INVALID, contextIndex));
}

void EventManager::fireScriptCompiledEvent(ScriptingComponent* c) {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(new ScriptEvent(c, ScriptEvent::Type::COMPILED, contextIndex));
}

void EventManager::fireScriptStatisticsChangedEvent() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(
			new ScriptEvent(nullptr, ScriptEvent::Type::STATISTICS_CHANGED, contextIndex));
}

void EventManager::fireSceneLoadedEvent() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	currentEventVector->push_back(new SceneLoadedEvent(contextIndex));
}
