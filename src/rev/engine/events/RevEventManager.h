/*
 * RevEventManager.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVEVENTMANAGER_H_
#define REVEVENTMANAGER_H_

#include <vector>
#include <functional>

#include <rev/engine/picking/RevPixelInfo.h>
#include "RevMouseButton.h"
#include "RevKey.h"

namespace rev {
	class Event;
	class IEventListenerComponent;
	class ScriptingComponent;
	class MemoryPool;
	class GameObject;
	class MouseEvent;
	class MouseWheelEvent;
	class KeyEvent;
	class PickingEvent;
	class GameObjectEvent;
	class EngineEvent;
	class ScriptEvent;
	class SceneLoadedEvent;
}

namespace rev {

	class EventManager {
		friend class Event;
		typedef std::vector<Event*> EventVector;
		typedef std::vector<IEventListenerComponent*> EventListenerList;

		static EventManager* eventManager;
		MemoryPool* eventMemoryPool = nullptr;
		EventVector eventVectorA;
		EventVector eventVectorB;
		EventVector* currentEventVector = &eventVectorA;
		bool currentA = true;
		EventListenerList components;

		MouseEvent* lastMousePressedEvent = nullptr;
		MouseEvent* lastMouseReleasedEvent = nullptr;
		MouseEvent* lastMouseMovedEvent = nullptr;
		MouseWheelEvent* lastWheelEvent = nullptr;
		KeyEvent* lastKeyPressedEvent = nullptr;
		KeyEvent* lastKeyReleasedEvent = nullptr;

		EventManager();

		EventManager(const EventManager&);

	public:
		~EventManager();

		static void createSingleton();
		static void destroySingleton();

		static EventManager& ref() {
			return *eventManager;
		}

		static EventManager* getInstance() {
			return eventManager;
		}

		void update();

		void fireObjectPickedEvent(rev::GameObject* gameObject,
				const PixelInfo& pixelInfo);

		void firePickingResetedEvent();

		void fireMouseMovedEvent(int x, int y);
		void fireMousePressedEvent(int x, int y, rev::MouseButton button);
		void fireMouseReleasedEvent(int x, int y, rev::MouseButton button);

		void fireKeyPressedEvent(rev::Key key, int modifiers);
		void fireKeyReleasedEvent(rev::Key key, int modifiers);

		void fireMouseWheelEvent(int difference);

		void fireGameObjectAddedEvent(GameObject* gameObject);
		void fireGameObjectRemovedEvent(GameObject* gameObject);
		void fireAllGameObjectsRemovedEvent();

		void fireEngineEvent(const char* name, std::function<bool()> callback);
		void fireScriptInvalidEvent(ScriptingComponent* c);
		void fireScriptCompiledEvent(ScriptingComponent* c);
		void fireScriptStatisticsChangedEvent();
		void fireSceneLoadedEvent();

		void removeAllEvents();

		/**
		 * EventManager is NOT owner of added component.
		 */
		void addComponent(IEventListenerComponent* comp) {
			components.push_back(comp);
		}

		void removeComponent(IEventListenerComponent* comp) {
			components.erase(std::remove(components.begin(), components.end(), comp), 
                components.end());
		}

		/**
		 * These get*State() methods are giving valid results only in
		 * EventListenerComponent::update(), since
		 * update() is called after all events are captured
		 * and dispatched.
		 */
		MouseEvent* getLastMousePressEvent() {
			return lastMousePressedEvent;
		}

		MouseEvent* getLastMouseReleaseEvent() {
			return lastMouseReleasedEvent;
		}

		MouseEvent* getLastMouseMovedEvent() {
			return lastMouseMovedEvent;
		}

		MouseWheelEvent* getLastMouseWheelEvent() {
			return lastWheelEvent;
		}

		KeyEvent* getLastKeyPressedEvent() {
			return lastKeyPressedEvent;
		}

		KeyEvent* getLastKeyReleasedEvent() {
			return lastKeyReleasedEvent;
		}
	};

} /* namespace rev */
#endif /* REVEVENTMANAGER_H_ */
