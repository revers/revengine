/*
 * RevEngineEvent.h
 *
 *  Created on: 09-03-2013
 *      Author: Revers
 */

#ifndef REVENGINEEVENT_H_
#define REVENGINEEVENT_H_

#include <functional>
#include "RevEvent.h"

namespace rev {

	class EngineEvent: public Event {
		std::function<bool()> callback;
		const char* name;

	public:
		using Event::operator new;
		using Event::operator delete;

		EngineEvent(const char* name, std::function<bool()> callback, int contextIndex) :
				Event(EventType::ENGINE_EVENT, contextIndex), name(name),
						callback(callback) {
		}

		std::function<bool()> getCallback() {
			return callback;
		}

		const char* getName() {
			return name;
		}
	};

} /* namespace rev */

#endif /* REVENGINEEVENT_H_ */
