/*
 * RevSSAORenderer.cpp
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#include <glm/glm.hpp>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/drawables/RevVBOQuadVT.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>

#include "RevSSAORenderer.h"

using namespace glm;

#define SSAO_PASS_SHADER "shaders/SSAOPass.glsl"
#define SSAO_TEXTURE_SHADER "shaders/QuadNormalTexture.glsl"
#define TEXTURE_FILE "images/normalmap.png"

namespace rev {

SSAORenderer::~SSAORenderer() {
	if (ssaoFbo != 0) {
		glDeleteFramebuffers(1, &ssaoFbo);
	}

	if (ssaoTex != 0) {
		glDeleteTextures(1, &ssaoTex);
	}
	if (quad) {
		delete quad;
	}

}

bool SSAORenderer::init() {

	glGenFramebuffers(1, &ssaoFbo);
	if (!createSsaoTextureGLSLProgram()) {
		return false;
	}
	if (!createSsaoGLSLProgram()) {
		return false;
	}
	if (!loadTexture()) {
		return false;
	}

	quad = new VBOQuadVT(1.0f);

	inited = true;
	return true;
}

bool SSAORenderer::init(int windowWidth, int windowHeight) {
	REV_TRACE_MSG("w = " << windowWidth << "; h = " << windowHeight);
	if (inited) {
		glDeleteTextures(1, &ssaoTex);
	} else if (!init()) {
		return false;
	}

	if (!initLightFbo(windowWidth, windowHeight)) {
		return false;
	}

	return true;
}

bool SSAORenderer::initLightFbo(int windowWidth, int windowHeight) {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ssaoFbo);

	glGenTextures(1, &ssaoTex);
	//glGenTextures(1, &lightTrueDepthTex);

	glBindTexture(GL_TEXTURE_2D, ssaoTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, windowWidth, windowHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			ssaoTex, 0);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		REV_ERROR_MSG("Framebuffer is not complete!");
		return false;
	}

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glAssert;

	return glGetError() == GL_NO_ERROR;
}

bool SSAORenderer::createSsaoGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(SSAO_PASS_SHADER);
	if (!ssaoProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	if (!ssaoProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}
	ssaoProgram.use();
	mvpLocation = ssaoProgram.getUniformLocation("MVP");
	invViewProjLocation = ssaoProgram.getUniformLocation("InvViewProjection");
	depthTexLocation = ssaoProgram.getUniformLocation("DepthTexture");
	normalTexLocation = ssaoProgram.getUniformLocation("NormalTexture");

	attenuationLocation = ssaoProgram.getUniformLocation("Attenuation");
	samplingRadiusLocation = ssaoProgram.getUniformLocation("SamplingRadius");
	occluderBiasLocation = ssaoProgram.getUniformLocation("OccluderBias");
	texelSizeLocation = ssaoProgram.getUniformLocation("TexelSize");
	normalMapTexLocation = ssaoProgram.getUniformLocation("RandomNormalTexture");

	revAssert(mvpLocation != -1
			&& textureLocation != -1
			&& invViewProjLocation != -1
			&& depthTexLocation != -1
			&& normalTexLocation != -1
			&& attenuationLocation != -1
			&& samplingRadiusLocation != -1
			&& occluderBiasLocation != -1
			&& texelSizeLocation != -1
			&& normalMapTexLocation != -1);

	ssaoProgram.setUniform(depthTexLocation, 0);
	ssaoProgram.setUniform(normalTexLocation, 1);
	ssaoProgram.setUniform(normalMapTexLocation, 2);
	ssaoProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

bool SSAORenderer::createSsaoTextureGLSLProgram() {
	REV_TRACE_FUNCTION;

	std::string path = ResourcePath::get(SSAO_TEXTURE_SHADER);
	if (!ssaoTexProgram.compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}

	glBindAttribLocation(ssaoTexProgram.getHandle(), 0, "VertexPosition");
	glBindAttribLocation(ssaoTexProgram.getHandle(), 1, "VertexTexCoord");
	glBindAttribLocation(ssaoTexProgram.getHandle(), 0, "FragColor");
	glAssert;

	if (!ssaoTexProgram.link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		revAssert(false);
		return false;
	}
	ssaoTexProgram.use();
	textureLocation = ssaoTexProgram.getUniformLocation("Tex");
	revAssert(textureLocation != -1);
	ssaoTexProgram.unuse();

	return glGetError() == GL_NO_ERROR;
}

void SSAORenderer::renderQuadWithSSAOTexture() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	ssaoTexProgram.use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ssaoTex);
	glAssert;

	ssaoTexProgram.setUniform(textureLocation, 0);
	quad->render();

	ssaoTexProgram.unuse();
	glBindTexture(GL_TEXTURE_2D, 0);
}

bool SSAORenderer::loadTexture() {
	std::string path = ResourcePath::get(TEXTURE_FILE);
	bool succ = normalMapTexture.load(path.c_str());
	if (!succ) {
		REV_ERROR_MSG("Failed to load texture file: " << path);
		revAssert(succ);
	}
	return succ;
}

void SSAORenderer::render() {
	DeferredRenderer& deferredRenderer = LightingManager::ref().getDeferredRenderer();
	GLuint depthTex = deferredRenderer.getDepthTexture();
	GLuint normalTex = deferredRenderer.getNormalTexture();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTex);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normalMapTexture.handle());
	glActiveTexture(GL_TEXTURE0);

	ssaoProgram.use();

	int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	revAssert(contextIndex == 0);

	Renderer3D& renderer = Renderer3D::ref();
	float width = renderer.getWidth(contextIndex);
	float height = renderer.getHeight(contextIndex);

	ICamera* camera = CameraManager::ref().getCamera(contextIndex);

	mat4 viewProjection = camera->getProjectionMatrix() * camera->getViewMatrix();
	mat4 invViewProjection = mat4(glm::inverse(viewProjection));
	//mat4 invViewProjection = mat4(glm::inverse(camera->getProjectionMatrix()));
	ssaoProgram.setUniform(invViewProjLocation, invViewProjection);
	ssaoProgram.setUniform(mvpLocation, mat4(1.0f));

	ssaoProgram.setUniform(occluderBiasLocation, occluderBias);
	ssaoProgram.setUniform(samplingRadiusLocation, samplingRadius);
	ssaoProgram.setUniform(attenuationLocation, attenuation);
	ssaoProgram.setUniform(texelSizeLocation, vec2(1.0f / width, 1.0f / height));

	glAssert;
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFbo);
	glClear(GL_COLOR_BUFFER_BIT);

	quad->render();

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glAssert;
	ssaoProgram.unuse();
}

}
/* namespace rev */
