/*
 * RevLightingManager.cpp
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include "RevLightingManager.h"

using namespace rev;

rev::LightingManager* rev::LightingManager::lightingManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(LightingManager)

LightingManager::LightingManager() {
}

void LightingManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!lightingManager);
	lightingManager = new LightingManager();
	REV_TRACE_FUNCTION_OUT;
}

void LightingManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(lightingManager);

	delete lightingManager;
	lightingManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void LightingManager::update() {
}

void LightingManager::addComponent(ILightComponent* comp) {
	components.push_back(comp);
	DirectionalLightComponent* dirLight = dynamic_cast<DirectionalLightComponent*>(comp);
	if (dirLight) {
		directionalLightVect.push_back(dirLight);
		return;
	}

	SpotLightComponent* spotLight = dynamic_cast<SpotLightComponent*>(comp);
	if (spotLight) {
		spotLightVect.push_back(spotLight);
		return;
	}

	PointLightComponent* pointLight = dynamic_cast<PointLightComponent*>(comp);
	if (pointLight) {
		pointLightVect.push_back(pointLight);
		return;
	}

	DRPointLightComponent* drPointLight = dynamic_cast<DRPointLightComponent*>(comp);
	if (drPointLight) {
		drPointLightVect.push_back(drPointLight);
		return;
	}
}

void LightingManager::removeComponent(ILightComponent* comp) {
	components.erase(std::remove(components.begin(), components.end(), comp), components.end());
	directionalLightVect.erase(
			std::remove(directionalLightVect.begin(), directionalLightVect.end(), comp),
			directionalLightVect.end());
	pointLightVect.erase(std::remove(pointLightVect.begin(), pointLightVect.end(), comp),
			pointLightVect.end());
	spotLightVect.erase(std::remove(spotLightVect.begin(), spotLightVect.end(), comp),
			spotLightVect.end());
	drPointLightVect.erase(std::remove(drPointLightVect.begin(), drPointLightVect.end(), comp),
			drPointLightVect.end());
}

void LightingManager::removeAllComponents() {
	components.clear();
	directionalLightVect.clear();
	pointLightVect.clear();
	spotLightVect.clear();
	drPointLightVect.clear();
}
