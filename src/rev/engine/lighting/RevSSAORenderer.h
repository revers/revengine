/*
 * RevSSAORenderer.h
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#ifndef REVSSAORENDERER_H_
#define REVSSAORENDERER_H_

#include <GL/glew.h>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/gl/RevGLTexture2D.h>

namespace rev {

class VBOQuadVT;
class Simple3DComponent;

class SSAORenderer {
private:
	GLTexture2D normalMapTexture;
	GLSLProgram ssaoProgram;
	GLSLProgram ssaoTexProgram;
	VBOQuadVT* quad = nullptr;

	GLuint ssaoFbo = 0;
	GLuint ssaoTex = 0;
	bool inited = false;

	int textureLocation = -1;

	int mvpLocation = -1;
	int invViewProjLocation = -1;
	int depthTexLocation = -1;
	int normalTexLocation = -1;
	int normalMapTexLocation = -1;

	int attenuationLocation = -1;
	int samplingRadiusLocation = -1;
	int occluderBiasLocation = -1;
	int texelSizeLocation = -1;

//	/// <summary>
//	/// Attenuation amounts.
//	/// x = constant
//	/// y = linear
//	/// z = quadratic (not used)
//	/// </summary>
//	this.Attenuation = new Point(1.0, 0.0, 0.0);
	glm::vec2 attenuation = glm::vec2(1.0, 5.0);
	//
//
//	/// <summary>
//	/// Occluder bias to minimize self-occlusion.
//	/// <summary>
	float occluderBias = 0.05;
	//
//
//	/// <summary>
//	/// Specifies the size of the sampling radius.
//	/// <summary>
	float samplingRadius = 20.0;

	SSAORenderer(const SSAORenderer&) = delete;

public:
	SSAORenderer() {
	}
	~SSAORenderer();

	bool init(int windowWidth, int windowHeight);
	void render();

	void renderQuadWithSSAOTexture();

	GLuint getSSAOTexture() {
		return ssaoTex;
	}

	float getConstAttenuation() const {
		return attenuation.x;
	}

	void setConstAttenuation(float value) {
		attenuation.x = value;
	}

	float getLinearAttenuation() const {
		return attenuation.y;
	}

	void setLinearAttenuation(float value) {
		attenuation.y = value;
	}

	const glm::vec2& getAttenuation() const {
		return attenuation;
	}

	void setAttenuation(const glm::vec2& attenuation) {
		this->attenuation = attenuation;
	}

	float getOccluderBias() const {
		return occluderBias;
	}

	void setOccluderBias(float occluderBias) {
		this->occluderBias = occluderBias;
	}

	float getSamplingRadius() const {
		return samplingRadius;
	}

	void setSamplingRadius(float samplingRadius) {
		this->samplingRadius = samplingRadius;
	}

private:
	bool initLightFbo(int windowWidth, int windowHeight);
	bool createSsaoTextureGLSLProgram();
	bool createSsaoGLSLProgram();
	bool loadTexture();
	bool init();
};

} /* namespace rev */
#endif /* REVSSAORENDERER_H_ */
