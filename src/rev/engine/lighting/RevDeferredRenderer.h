/*
 * RevDeferredRenderer.h
 *
 *  Created on: 30 lip 2013
 *      Author: Revers
 */

#ifndef REVDEFERREDRENDERER_H_
#define REVDEFERREDRENDERER_H_

#include <GL/glew.h>
#include <rev/gl/RevGLSLProgram.h>

namespace rev {

class VBOQuadVT;
class Simple3DComponent;

class DeferredRenderer {
private:
	GLSLProgram geomProgram;
	GLSLProgram lightProgram;
	GLSLProgram depthTexProgram;
	GLSLProgram normalTexProgram;
	VBOQuadVT* quad = nullptr;
	Simple3DComponent* sphereComponent = nullptr;

	GLuint depthNormalFbo = 0;
	GLuint lightFbo = 0;
	GLuint normalTex = 0;
	GLuint depthTex = 0;
	GLuint lightTex = 0;
	bool inited = false;

	int mvpLocation = -1;
	int normalMatrixLocation = -1;

	int depthTexLocation = -1;
	int normalTexLocation = -1;

	int lightMvpLocaction = -1;
	int lightInvViewProjLocation = -1;
	int lightDepthTexLocation = -1;
	int lightNormalTexLocation = -1;
	int lightLightColorLocation = -1;
	int lightLightPositionLocation = -1;
	int lightLightAttenLocation = -1;
	int lightViewportLocation = -1;

	DeferredRenderer(const DeferredRenderer&) = delete;

public:
	DeferredRenderer() {
	}
	~DeferredRenderer();

	bool init(int windowWidth, int windowHeight);
	void render();

	void renderQuadWithDepthTexture();
	void renderQuadWithNormalTexture();
	void renderQuadWithLightTexture();
	void drawLightSpheres();

	GLuint getLightTexture() {
		return lightTex;
	}

	GLuint getNormalTexture() {
		return normalTex;
	}
	GLuint getDepthTexture() {
		return depthTex;
	}

private:
	bool initDepthNormalFbo(int windowWidth, int windowHeight);
	bool initLightFbo(int windowWidth, int windowHeight);
	void drawDepthNormalMap();
	void drawLightMap();
	bool createGeomGLSLProgram();
	bool createLightGLSLProgram();
	bool createDepthTexGLSLProgram();
	bool createNormalTexGLSLProgram();
	bool createTexGLSLProgram(GLSLProgram& prog, const char* filename);
	bool init();
};

} /* namespace rev */
#endif /* REVDEFERREDRENDERER_H_ */
