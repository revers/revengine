/*
 * RevLightingManager.h
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#ifndef REVLIGHTINGMANAGER_H_
#define REVLIGHTINGMANAGER_H_

#include <vector>
#include <rev/engine/components/light/RevILightComponent.h>
#include <rev/engine/components/light/RevDirectionalLightComponent.h>
#include <rev/engine/components/light/RevPointLightComponent.h>
#include <rev/engine/components/light/RevSpotLightComponent.h>
#include <rev/engine/components/light/RevDRPointLightComponent.h>
#include "RevDeferredRenderer.h"
#include "RevSSAORenderer.h"

namespace rev {

typedef std::vector<ILightComponent*> LightComponentVect;
typedef std::vector<DirectionalLightComponent*> DirectionalLightVect;
typedef std::vector<PointLightComponent*> PointLightVect;
typedef std::vector<SpotLightComponent*> SpotLightVect;
typedef std::vector<DRPointLightComponent*> DRPointLightVect;

class LightingManager: public IBindable {
	static LightingManager* lightingManager;

	LightingManager();
	LightingManager(const LightingManager&) = delete;

private:
	LightComponentVect components;
	DirectionalLightVect directionalLightVect;
	PointLightVect pointLightVect;
	SpotLightVect spotLightVect;
	DRPointLightVect drPointLightVect;

	DeferredRenderer deferredRenderer;
	SSAORenderer ssaoRenderer;

public:
	DECLARE_BINDABLE_SINGLETON(LightingManager)

	~LightingManager() {
	}

	static void createSingleton();
	static void destroySingleton();

	static LightingManager& ref() {
		return *lightingManager;
	}

	static LightingManager* getInstance() {
		return lightingManager;
	}

	bool initDeferredRenderer(int w, int h) {
		return deferredRenderer.init(w, h);
	}
	bool initSSAORenderer(int w, int h) {
		return ssaoRenderer.init(w, h);
	}
	void renderDeferred() {
		deferredRenderer.render();
	}
	void renderSSAO() {
		ssaoRenderer.render();
	}

	/**
	 * LightingManager is NOT owner of added component.
	 */
	void addComponent(ILightComponent* comp);

	void removeComponent(ILightComponent* comp);

	void removeAllComponents();

	LightComponentVect& getComponents() {
		return components;
	}
	DirectionalLightVect& getDirectionalLightVect() {
		return directionalLightVect;
	}
	PointLightVect& getPointLightVect() {
		return pointLightVect;
	}
	SpotLightVect& getSpotLightVect() {
		return spotLightVect;
	}
	DRPointLightVect& getDRPointLightVect() {
		return drPointLightVect;
	}
	DeferredRenderer& getDeferredRenderer() {
		return deferredRenderer;
	}
	SSAORenderer& getSSAORenderer() {
		return ssaoRenderer;
	}

	void update();

};

} /* namespace rev */
#endif /* REVLIGHTINGMANAGER_H_ */
