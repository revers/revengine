/*
 * RevEngine.cpp
 *
 *  Created on: 27-11-2012
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevDelete.hpp>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/xml/RevXMLConfigManager.h>
#include <rev/engine/xml/RevXMLElementBinder.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/console/RevConsoleManager.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/collision/RevCollisionManager.h>
#include <rev/engine/physics/RevPhysicsManager.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/shadow/RevShadowManager.h>

#include "RevEngine.h"

using namespace rev;
using namespace v8;
using namespace std;

rev::Engine* rev::Engine::engine = nullptr;

IMPLEMENT_BINDABLE_SINGLETON_FULL(Engine, getInstance(), Engine::initScripting)

Engine::Engine() {
}

Engine::~Engine() {
}

void Engine::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!engine);

	engine = new Engine();
	GLContextManager::createSingleton();
	EventManager::createSingleton();
	CameraManager::createSingleton();
	Renderer3D::createSingleton();
	XMLConfigManager::createSingleton();
	ScriptingManager::createSingleton();
	CollisionManager::createSingleton();
	PhysicsManager::createSingleton();
	ConsoleManager::createSingleton();
	LightingManager::createSingleton();
	ShadowManager::createSingleton();

	REV_TRACE_FUNCTION_OUT;
}

void Engine::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(engine);
	engine->removeAllGameObjects();

	ShadowManager::destroySingleton();
	LightingManager::destroySingleton();
	ConsoleManager::destroySingleton();
	CollisionManager::destroySingleton();
	PhysicsManager::destroySingleton();
	ScriptingManager::destroySingleton();
	XMLConfigManager::destroySingleton();
	Renderer3D::destroySingleton();
	CameraManager::destroySingleton();
	EventManager::destroySingleton();
	GLContextManager::destroySingleton();
	delete engine;
	engine = nullptr;

	REV_TRACE_FUNCTION_OUT;
}

void Engine::start() {
	REV_TRACE_FUNCTION;

	revAssert(Renderer3D::ref().isInited());

	if (!ScriptingManager::ref().isInited() && !ScriptingManager::ref().init()) {
		REV_ERROR_MSG("ScriptingManager::ref().init() FAILED!");
		revAssert(false);
	}

	lastTimeMs = 0.0;
	timer.start();
	running = true;
}

void Engine::stop() {
	REV_TRACE_FUNCTION;
	timer.stop();
	running = false;
}

void Engine::removeAllObjectsAndEvents() {
	REV_TRACE_FUNCTION;
	rev::EventManager::ref().removeAllEvents();
	removeAllGameObjects();
}

void Engine::removeAllGameObjects() {
	REV_TRACE_FUNCTION;

	ShadowManager::ref().removeAllComponents();
	LightingManager::ref().removeAllComponents();
	CollisionManager::ref().removeAllComponents();
	PhysicsManager::ref().removeAllComponents();
	Renderer3D::ref().removeAllComponents();
	ScriptingManager::ref().removeAllComponents();
	// There are components not bind to any GameObject
	// so removeAll cannot be applied:
	// rev::EventManager::ref().removeAllComponents();

	for (GameObject*& go : gameObjects) {
		if (go->getEventListenerComponent() != nullptr) {
			IEventListenerComponent* c = go->getEventListenerComponent();
			EventManager::ref().removeComponent(c);
		}
		delete go;
	}

	gameObjects.clear();
	rev::Renderer3D::ref().getPicker().reset();
	rev::EventManager::ref().fireAllGameObjectsRemovedEvent();
}

void Engine::update() {
	double currTimeMs = timer.getTimeInMilliSec();
	currentTimeMs = (float) (currTimeMs - lastTimeMs);
	lastTimeMs = currTimeMs;
	// std::cout << "time: " << elapsedTimeMs << std::endl;

	EventManager::ref().update();

	// remove not active game objects
	GameObjectVectIter i = gameObjects.begin();
	while (i != gameObjects.end()) {
		GameObject*& obj = *i;

		if (obj->isBeforeDeath()) {
			EventManager::ref().fireGameObjectRemovedEvent(obj);
			REV_TRACE_MSG("Before death");
			obj->setState(GameObjectState::DEATH);
			++i;
		} else if (obj->isDeath()) {
			REV_TRACE_MSG("dead");
			IEventListenerComponent* evt = obj->getEventListenerComponent();
			if (evt) {
				// send destroy signal.
				evt->gameObjectDestroy();
			}
			removeGameObjectComponents(obj);
			delete obj;
			i = gameObjects.erase(i); //items.erase(i++);
		} else {
			++i;
		}
	}

	ConsoleManager::ref().update();
	ScriptingManager::ref().update();
	PhysicsManager::ref().update();
	CollisionManager::ref().update();
	LightingManager::ref().update();
	ShadowManager::ref().update();
	Renderer3D::ref().update();

	// moved to QGLWidget:
	// Renderer3D::ref().render();
}

/**
 * Engine is owner of added game object.
 */
void Engine::addGameObject(GameObject* obj) {
	REV_TRACE_MSG("Adding object: " << obj->getName());

	gameObjects.push_back(obj);

	if (obj->getVisual3DComponent() != nullptr) {
		IVisual3DComponent* c = obj->getVisual3DComponent();
		Renderer3D::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getEventListenerComponent() != nullptr) {
		IEventListenerComponent* c = obj->getEventListenerComponent();
		EventManager::ref().addComponent(c);
		c->setParent(obj);
		// send init signal.
		c->gameObjectInit();
	}
	if (obj->getScriptingComponent() != nullptr) {
		ScriptingComponent* c = obj->getScriptingComponent();
		ScriptingManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getPhysicsComponent() != nullptr) {
		IPhysicsComponent* c = obj->getPhysicsComponent();
		PhysicsManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getCollisionComponent() != nullptr) {
		CollisionComponent* c = obj->getCollisionComponent();
		CollisionManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getLightComponent() != nullptr) {
		ILightComponent* c = obj->getLightComponent();
		LightingManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getShadowComponent() != nullptr) {
		ShadowSourceComponent* c = obj->getShadowComponent();
		ShadowManager::ref().addComponent(c);
		c->setParent(obj);
	}

	EventManager::ref().fireGameObjectAddedEvent(obj);
}

void Engine::removeGameObjectComponents(GameObject* obj) {
	REV_TRACE_FUNCTION;

	if (obj->getVisual3DComponent() != nullptr) {
		IVisual3DComponent* c = obj->getVisual3DComponent();
		Renderer3D::ref().removeComponent(c);
	}
	if (obj->getEventListenerComponent() != nullptr) {
		IEventListenerComponent* c = obj->getEventListenerComponent();
		EventManager::ref().removeComponent(c);
		// send destroy signal.
		c->gameObjectDestroy();
	}
	if (obj->getScriptingComponent() != nullptr) {
		ScriptingComponent* c = obj->getScriptingComponent();
		ScriptingManager::ref().removeComponent(c);
	}
	if (obj->getPhysicsComponent() != nullptr) {
		IPhysicsComponent* c = obj->getPhysicsComponent();
		PhysicsManager::ref().removeComponent(c);
	}
	if (obj->getCollisionComponent() != nullptr) {
		CollisionComponent* c = obj->getCollisionComponent();
		CollisionManager::ref().removeComponent(c);
	}
	if (obj->getLightComponent() != nullptr) {
		ILightComponent* c = obj->getLightComponent();
		LightingManager::ref().removeComponent(c);
	}
	if (obj->getShadowComponent() != nullptr) {
		ShadowSourceComponent* c = obj->getShadowComponent();
		ShadowManager::ref().removeComponent(c);
	}
}

void Engine::silentAddGameObject(GameObject* obj) {
	gameObjects.push_back(obj);

	if (obj->getVisual3DComponent() != nullptr) {
		IVisual3DComponent* c = obj->getVisual3DComponent();
		Renderer3D::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getEventListenerComponent() != nullptr) {
		IEventListenerComponent* c = obj->getEventListenerComponent();
		EventManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getScriptingComponent() != nullptr) {
		ScriptingComponent* c = obj->getScriptingComponent();
		ScriptingManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getPhysicsComponent() != nullptr) {
		IPhysicsComponent* c = obj->getPhysicsComponent();
		PhysicsManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getCollisionComponent() != nullptr) {
		CollisionComponent* c = obj->getCollisionComponent();
		CollisionManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getLightComponent() != nullptr) {
		ILightComponent* c = obj->getLightComponent();
		LightingManager::ref().addComponent(c);
		c->setParent(obj);
	}
	if (obj->getShadowComponent() != nullptr) {
		ShadowSourceComponent* c = obj->getShadowComponent();
		ShadowManager::ref().addComponent(c);
		c->setParent(obj);
	}
}

void Engine::silentRemoveGameObject(GameObject* obj) {
	if (obj->getVisual3DComponent() != nullptr) {
		IVisual3DComponent* c = obj->getVisual3DComponent();
		Renderer3D::ref().removeComponent(c);
	}
	if (obj->getEventListenerComponent() != nullptr) {
		IEventListenerComponent* c = obj->getEventListenerComponent();
		EventManager::ref().removeComponent(c);
	}
	if (obj->getScriptingComponent() != nullptr) {
		ScriptingComponent* c = obj->getScriptingComponent();
		ScriptingManager::ref().removeComponent(c);
	}
	if (obj->getPhysicsComponent() != nullptr) {
		IPhysicsComponent* c = obj->getPhysicsComponent();
		PhysicsManager::ref().removeComponent(c);
	}
	if (obj->getCollisionComponent() != nullptr) {
		CollisionComponent* c = obj->getCollisionComponent();
		CollisionManager::ref().removeComponent(c);
	}
	if (obj->getLightComponent() != nullptr) {
		ILightComponent* c = obj->getLightComponent();
		LightingManager::ref().removeComponent(c);
	}
	if (obj->getShadowComponent() != nullptr) {
		ShadowSourceComponent* c = obj->getShadowComponent();
		ShadowManager::ref().removeComponent(c);
	}

	auto iter = gameObjects.begin();
	auto end = gameObjects.end();
	for (; iter != end; ++iter) {
		if (*iter == obj) {
			gameObjects.erase(iter);
			break;
		}
	}
}

bool Engine::destroyGameObject(int id) {
	GameObject* go = getGameObject(id);
	if (!go) {
		return false;
	}

	go->destroy();
	return true;
}

GameObject* Engine::getGameObject(int id) {
	for (GameObject*& go : gameObjects) {
		if (go->getId() == id) {
			return go;
		}
	}

	return nullptr;
}

GameObject* Engine::getGameObject(const std::string& name) {
	for (GameObject*& go : gameObjects) {
		if (go->getName() == name) {
			return go;
		}
	}

	return nullptr;
}

GameObject* Engine::getGameObject(const char* name) {
	for (GameObject*& go : gameObjects) {
		if (go->getName() == name) {
			return go;
		}
	}

	return nullptr;
}

void Engine::bind(IBinder& binder) {
	binder.bindList(this, "gameObjects", &Engine::gameObjects);
}

void Engine::initScripting() {
	Handle<ObjectTemplate> proto = getFunctionTemplate()->PrototypeTemplate();

	proto->Set(String::New("get"), FunctionTemplate::New(jsGetGameObject)->GetFunction());
	proto->Set(String::New("getFrameTime"), FunctionTemplate::New(jsGetFrameTime)->GetFunction());
	proto->Set(String::New("remove"), FunctionTemplate::New(jsRemoveGameObject)->GetFunction());
}

v8::Handle<v8::Value> Engine::jsRemoveGameObject(const v8::Arguments& args) {
	if (args.Length() != 1) {
		std::ostringstream oss;
		oss << "Wrong number of argument: " << args.Length() << "; Expected: 0";
		return ThrowException(String::New(oss.str().c_str()));
	}

	Local<Object> argObject = Local<Object>::Cast(args[0]);
	Local<External> wrap = Local<External>::Cast(argObject->GetInternalField(0));
	GameObject* obj = static_cast<GameObject*>(wrap->Value());

	REV_WARN_MSG("Removing GameObject: '" << obj << "'");

	obj->destroy();

	return v8::Undefined();
}

v8::Handle<v8::Value> Engine::jsGetGameObject(const v8::Arguments& args) {
	if (args.Length() != 1) {
		std::ostringstream oss;
		oss << "Wrong number of argument: " << args.Length() << "; Expected: 0";
		return ThrowException(String::New(oss.str().c_str()));
	}

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	void* ptr = wrap->Value();
	Engine* engine = static_cast<Engine*>(ptr);
	Handle<Value> value = args[0];
	if (value->IsString()) {
		std::string name = ScriptType<std::string>::castFromJS(value);
		GameObject* go = engine->getGameObject(name);
		if (go) {
			return go->wrap();
		}
	}

	return v8::Null();
}

v8::Handle<v8::Value> Engine::jsGetFrameTime(const v8::Arguments& args) {
	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	Engine* engine = static_cast<Engine*>(wrap->Value());

	return ScriptType<float>::castToJS(engine->currentTimeMs);
}
