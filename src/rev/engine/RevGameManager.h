/*
 * RevGameManager.h
 *
 *  Created on: 01-12-2012
 *      Author: Revers
 */

#ifndef REVGAMEMANAGER_H_
#define REVGAMEMANAGER_H_

#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/components/RevIEventListenerComponent.h>

namespace rev {

	class GameManager: public IEventListenerComponent {
		static GameManager* gameManager;

		GameManager() {
		}

		GameManager(const GameManager&) = delete;
		~GameManager() {
		}
	public:
		DECLARE_BINDABLE_SINGLETON(GameManager)

		static void createSingleton();
		static void destroySingleton();

		static GameManager& ref() {
			return *gameManager;
		}

		static GameManager* getInstance() {
			return gameManager;
		}

		void keyPressed(const KeyEvent& e) override;
		bool init();
		
		void addSamplePhysicsObject();
		void addSampleParticleObject();

	private:
		void createScene();

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVGAMEMANAGER_H_ */
