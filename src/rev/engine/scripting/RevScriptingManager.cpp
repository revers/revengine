/*
 * RevScriptingManager.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/util/RevVectorUtil.h>
#include <rev/engine/binding/RevBindableFactory.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/events/RevEventManager.h>
#include "RevScriptingManager.h"

#define COMPONENT_ID "'" << (*c->getParent()) << " [" << c->getScriptFile() << "]'"
#define PRIVATE_SCRIPT_SPACE_BEGIN "delete self.init; delete self.destroy; delete self.update; (function() {\n"
#define PRIVATE_SCRIPT_SPACE_END "\n})();"

using namespace rev;
using namespace v8;
using namespace std;

rev::ScriptingManager* rev::ScriptingManager::scriptingManager = nullptr;
int rev::ScriptingManager::scriptId = 0;

IMPLEMENT_BINDABLE_SINGLETON(ScriptingManager)

ScriptingManager::ScriptingManager() {
	mainScriptComponent = new ScriptingComponent();
	GameObject* dummyObject = new GameObject("Main Script");
	mainScriptComponent->setParent(dummyObject);
}

ScriptingManager::~ScriptingManager() {
	if (mainScriptComponent) {
		delete mainScriptComponent->getParent();
		delete mainScriptComponent;
	}
	context.Dispose();
}

void ScriptingManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!scriptingManager);
	scriptingManager = new ScriptingManager();
	REV_TRACE_FUNCTION_OUT;
}

void ScriptingManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(scriptingManager);

	delete scriptingManager;
	scriptingManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void ScriptingManager::addComponent(ScriptingComponent* comp) {
	components.push_back(comp);
	comp->setActive(true);
	EventManager::ref().fireScriptStatisticsChangedEvent();
}

void ScriptingManager::removeComponent(ScriptingComponent* comp) {
	VectorUtil::removeValue(components, comp);
	clearTimeouts(comp);
	clearIntervals(comp);
	callDestroy(comp);
	EventManager::ref().fireScriptStatisticsChangedEvent();
}

void ScriptingManager::removeAllComponents() {
	mainScriptComponent->destroyFunc.Clear();
	mainScriptComponent->destroyFunc.Dispose();
	components.clear();
	removeAllIntervals();
	removeAllTimeouts();
	removeMainScript();
	EventManager::ref().fireScriptStatisticsChangedEvent();
}

void ScriptingManager::removeMainScript() {
	mainScriptComponent->clearScriptFile();
}

void ScriptingManager::handleError(ScriptingComponent* c) {
	REV_ERROR_MSG(COMPONENT_ID << " scripting error!");

	c->invalid = true;
	c->active = false;
	EventManager::ref().fireScriptInvalidEvent(c);
}

bool ScriptingManager::handleTryCatch(v8::TryCatch& tryCatch, ScriptingComponent* c) {
	if (tryCatch.HasCaught()) {
		ostringstream oss;
		reportException(&tryCatch, oss);
		handleError(c);

		return false;
	}
	return true;
}

void ScriptingManager::update() {
	if (pause) {
		updateStatistics();
		return;
	}
	scriptingTime += Engine::ref().getCurrentFrameTime();

	HandleScope handleScope;
	Context::Scope contextScope(context);

	activeCount = 0;
	compiledCount = 0;
	invalidCount = 0;
	updateMainScript();

	TryCatch tryCatch;
	for (ScriptingComponent*& c : components) {
		currentComponent = c;

		tryCatch.Reset();
		context->Global()->Set(String::New("self"), c->getParent()->wrap());

		if (c->active && !c->compiled && !c->isScriptEmpty()) {
			std::ostringstream oss;
			loadScript(c, oss);
		}
		if (c->compiled && !c->invalid && c->active) {
			c->updateFunc->Call(context->Global(), 0, nullptr);
			handleTryCatch(tryCatch, c);
		}

		if (c->isActive()) {
			activeCount++;
		}
		if (c->isCompiled()) {
			compiledCount++;
		}
		if (c->isInvalid()) {
			invalidCount++;
		}
	}

	updateTimeouts();
	updateIntervals();
}

void ScriptingManager::updateMainScript() {
	TryCatch tryCatch;
	context->Global()->Set(String::New("self"), v8::Null());

	currentComponent = mainScriptComponent;

	// TODO: main script should be handled only here, like other stuff
	if (mainScriptComponent->active && !mainScriptComponent->compiled
			&& !mainScriptComponent->isScriptEmpty()) {
		std::ostringstream oss;
		loadScript(mainScriptComponent, oss);
	}
	if (mainScriptComponent->compiled && !mainScriptComponent->invalid
			&& mainScriptComponent->active) {
		mainScriptComponent->updateFunc->Call(context->Global(), 0, nullptr);
		handleTryCatch(tryCatch, mainScriptComponent);
	}

	if (mainScriptComponent->isActive()) {
		activeCount++;
	}
	if (mainScriptComponent->isCompiled()) {
		compiledCount++;
	}
	if (mainScriptComponent->isInvalid()) {
		invalidCount++;
	}
}

void ScriptingManager::updateStatistics() {
	activeCount = 0;
	compiledCount = 0;
	invalidCount = 0;
	for (ScriptingComponent*& c : components) {
		if (c->isActive()) {
			activeCount++;
		}
		if (c->isCompiled()) {
			compiledCount++;
		}
		if (c->isInvalid()) {
			invalidCount++;
		}
	}
	if (mainScriptComponent->isActive()) {
		activeCount++;
	}
	if (mainScriptComponent->isCompiled()) {
		compiledCount++;
	}
	if (mainScriptComponent->isInvalid()) {
		invalidCount++;
	}
}

void ScriptingManager::updateTimeouts() {
	TryCatch tryCatch;

	float frameTime = Engine::ref().getCurrentFrameTime();
	for (int i = 0; i < timeoutVector.size(); i++) {
		ScriptingTimeoutStruct* sts = timeoutVector[i];

		ScriptingComponent*& c = sts->component;
		if (!c->compiled || c->invalid || !c->active) {
			sts->invokeTime += frameTime;
			continue;
		}

		if (scriptingTime >= sts->invokeTime) {
			currentComponent = sts->component;
			if (currentComponent == mainScriptComponent) {
				context->Global()->Set(String::New("self"), v8::Null());
			} else {
				context->Global()->Set(String::New("self"), currentComponent->getParent()->wrap());
			}

			tryCatch.Reset();
			sts->function->Call(context->Global(), 0, nullptr);
			handleTryCatch(tryCatch, sts->component);
			delete sts;

			if (VectorUtil::fastRemoveIndex(timeoutVector, i)) {
				i--;
			}
		}
	}
}

void ScriptingManager::updateIntervals() {
	TryCatch tryCatch;

	float frameTime = Engine::ref().getCurrentFrameTime();
	for (int i = 0; i < intervalVector.size(); i++) {
		ScriptingIntervalStruct* sis = intervalVector[i];

		ScriptingComponent*& c = sis->component;
		if (!c->compiled || c->invalid || !c->active) {
			sis->nextInvokeTime += frameTime;
			continue;
		}

		if (scriptingTime >= sis->nextInvokeTime) {
			currentComponent = sis->component;
			sis->nextInvokeTime += sis->interval;

			if (currentComponent == mainScriptComponent) {
				context->Global()->Set(String::New("self"), v8::Null());
			} else {
				context->Global()->Set(String::New("self"), currentComponent->getParent()->wrap());
			}

			tryCatch.Reset();
			sis->function->Call(context->Global(), 0, nullptr);
			handleTryCatch(tryCatch, sis->component);
		}
	}
}

void ScriptingManager::removeAllIntervals() {
	for (int i = 0; i < intervalVector.size(); i++) {
		ScriptingIntervalStruct*& sis = intervalVector[i];
		delete sis;
	}
	intervalVector.clear();
}

void ScriptingManager::removeAllTimeouts() {
	for (int i = 0; i < timeoutVector.size(); i++) {
		ScriptingTimeoutStruct*& sts = timeoutVector[i];
		delete sts;
	}
	timeoutVector.clear();
}

void ScriptingManager::clearTimeouts(ScriptingComponent* c) {
	for (int i = 0; i < timeoutVector.size(); i++) {
		ScriptingTimeoutStruct*& sts = timeoutVector[i];

		if (sts->component == c) {
			delete sts;
			if (VectorUtil::fastRemoveIndex(timeoutVector, i)) {
				i--;
			}
		}
	}
}

void ScriptingManager::clearIntervals(ScriptingComponent* c) {
	for (int i = 0; i < intervalVector.size(); i++) {
		ScriptingIntervalStruct*& sis = intervalVector[i];

		if (sis->component == c) {
			delete sis;
			if (VectorUtil::fastRemoveIndex(intervalVector, i)) {
				i--;
			}
		}
	}
}

void ScriptingManager::callDestroy(ScriptingComponent* c) {
	if (c->destroyFunc.IsEmpty()) {
		return;
	}

	REV_TRACE_MSG("Calling destroy of " << COMPONENT_ID);
	HandleScope handleScope;
	TryCatch tryCatch;
	c->destroyFunc->Call(context->Global(), 0, nullptr);
	handleTryCatch(tryCatch, c);
}

void ScriptingManager::bind(IBinder& binder) {
	binder.bindSimple(pause);
}

bool ScriptingManager::init() {
	REV_TRACE_FUNCTION;
	revAssert(!inited);

	rev::GLContextManager::ref().useMainContext();
	HandleScope handleScope;

	context = Context::New();

	// Enter the new context so all the following operations take place within it.
	Context::Scope contextScope(context);

	Handle<v8::Object> global = context->Global();
	global->Set(String::New("log"), FunctionTemplate::New(jsLog)->GetFunction());
	global->Set(String::New("setTimeout"), FunctionTemplate::New(jsSetTimeout)->GetFunction());
	global->Set(String::New("clearTimeout"), FunctionTemplate::New(jsClearTimeout)->GetFunction());
	global->Set(String::New("setInterval"), FunctionTemplate::New(jsSetInterval)->GetFunction());
	global->Set(String::New("clearInterval"),
			FunctionTemplate::New(jsClearInterval)->GetFunction());

	// Make persistent "global" object:
	Persistent<v8::Object> globalObj = Persistent<v8::Object>::New(v8::Object::New());
	global->Set(String::New("global"), globalObj);

	BindableStructMap& map = BindableFactory::ref().getBindableStructMap();
	for (auto& pair : map) {
		BindableStruct& bStruct = pair.second;
		GetFunctionTemplateFunc func = bStruct.templateFunc;

		// FunctionTemplate is lazy-initialized by GetFunctionTemplateFunc:
		v8::Persistent<v8::FunctionTemplate>& f = func();

		ScriptingInitFunc scriptingInitFunc = bStruct.scriptingInitFunc;
		if (scriptingInitFunc) {
			scriptingInitFunc();
		}

		if (bStruct.isSingleton) {
			string name = pair.first;
			REV_DEBUG_MSG("Registering singleton JS Object: '" << name << "'");

			IBindable* bindable = bStruct.factoryFunc();
			global->Set(String::New(name.c_str()), bindable->wrap());
		}
	}

	if (!loadLibScripts()) {
		return false;
	}

	inited = true;
	return true;
}

bool ScriptingManager::loadLibScripts() {
	ostringstream errOut;
	bool succ = loadScriptFromFile("scripts/lib/gl-matrix-min.js", errOut, false);
	if (!succ) {
		REV_ERROR_MSG(errOut.str());
		revAssert(succ);
		return false;
	}

	return true;
}

v8::Handle<v8::Value> ScriptingManager::jsSetTimeout(const v8::Arguments& args) {
	if (args.Length() != 2) {
		return v8::ThrowException(String::New("Wrong number of arguments. Expected two."));
	}
	Handle<Value> arg1 = args[0];
	if (!arg1->IsFunction()) {
		return v8::ThrowException(String::New("Expected function as first argument."));
	}
	Handle<Value> arg2 = args[1];
	if (!arg2->IsNumber()) {
		return v8::ThrowException(String::New("Expected number as second argument."));
	}

	ScriptingManager& mgr = ScriptingManager::ref();

	int id = mgr.timeIdCounter++;
	float ms = (float) arg2->NumberValue();
	float invokeTime = mgr.scriptingTime + ms;
	Handle<Function> func = Handle<Function>::Cast(arg1);
	Persistent<Function> function = Persistent<Function>::New(func);
	mgr.timeoutVector.push_back(new ScriptingTimeoutStruct { id, invokeTime,
			mgr.currentComponent, function });

	return v8::Uint32::New(id);
}

v8::Handle<v8::Value> ScriptingManager::jsClearTimeout(const v8::Arguments& args) {
	if (args.Length() != 1) {
		return v8::ThrowException(String::New("Wrong number of arguments. Expected one."));
	}
	Handle<Value> arg = args[0];
	if (!arg->IsNumber()) {
		return v8::ThrowException(String::New("Expected number as the argument."));
	}
	int id = arg->Int32Value();
	ScriptingManager& mgr = ScriptingManager::ref();

	for (int i = 0; i < mgr.timeoutVector.size(); i++) {
		ScriptingTimeoutStruct*& sts = mgr.timeoutVector[i];

		if (sts->id == id) {
			delete sts;
			VectorUtil::fastRemoveIndex(mgr.timeoutVector, i);
			break;
		}
	}

	return v8::Undefined();
}

v8::Handle<v8::Value> ScriptingManager::jsSetInterval(const v8::Arguments& args) {
	if (args.Length() != 2) {
		return v8::ThrowException(String::New("Wrong number of arguments. Expected two."));
	}
	Handle<Value> arg1 = args[0];
	if (!arg1->IsFunction()) {
		return v8::ThrowException(String::New("Expected function as first argument."));
	}
	Handle<Value> arg2 = args[1];
	if (!arg2->IsNumber()) {
		return v8::ThrowException(String::New("Expected number as second argument."));
	}

	ScriptingManager& mgr = ScriptingManager::ref();

	int id = mgr.timeIdCounter++;
	float ms = (float) arg2->NumberValue();
	float invokeTime = mgr.scriptingTime + ms;
	Handle<Function> func = Handle<Function>::Cast(arg1);
	Persistent<Function> function = Persistent<Function>::New(func);
	mgr.intervalVector.push_back(new ScriptingIntervalStruct { id, invokeTime, ms,
			mgr.currentComponent, function });

	return v8::Uint32::New(id);
}

v8::Handle<v8::Value> ScriptingManager::jsClearInterval(const v8::Arguments& args) {
	if (args.Length() != 1) {
		return v8::ThrowException(String::New("Wrong number of arguments. Expected one."));
	}
	Handle<Value> arg = args[0];
	if (!arg->IsNumber()) {
		return v8::ThrowException(String::New("Expected number as the argument."));
	}
	int id = arg->Int32Value();
	ScriptingManager& mgr = ScriptingManager::ref();

	for (int i = 0; i < mgr.intervalVector.size(); i++) {
		ScriptingIntervalStruct*& sis = mgr.intervalVector[i];

		if (sis->id == id) {
			delete sis;
			VectorUtil::fastRemoveIndex(mgr.intervalVector, i);
			break;
		}
	}

	return v8::Undefined();
}

v8::Handle<v8::Value> ScriptingManager::jsLog(const v8::Arguments& args) {
	if (args.Length() < 1) {
		return v8::Undefined();
	}
	HandleScope scope;
	Handle<Value> arg = args[0];
	String::Utf8Value value(arg);

	REV_INFO_MSG(*value);

	return v8::Undefined();
}

#define ERROR_MSG(msg) \
        ostringstream oss; \
        oss << msg; \
        errOut << oss.str(); \
        REV_ERROR_MSG(errOut);

void ScriptingManager::reportException(TryCatch* tryCatch, ostream& out) {
	HandleScope handleScope;
	String::Utf8Value exception(tryCatch->Exception());
	const char* exceptionString = toCString(exception);
	Handle<Message> message = tryCatch->Message();
	if (message.IsEmpty()) {
		// V8 didn't provide any extra information about this error; just
		// print the exception.
		REV_ERROR_MSG(exceptionString);
		out << exceptionString << endl;
	} else {
		/* Print (filename):(line number): (message). */
		String::Utf8Value filename(message->GetScriptResourceName());
		const char* filenameString = toCString(filename);
		int linenum = message->GetLineNumber();
		ostringstream oss;
		oss << filenameString << ":" << linenum << ": " << exceptionString << endl;

		/* Print line of source code. */
		String::Utf8Value sourceline(message->GetSourceLine());
		const char* sourcelineString = toCString(sourceline);
		oss << sourcelineString << endl;

		/* Print wavy underline (GetUnderline is deprecated). */
		int start = message->GetStartColumn();
		for (int i = 0; i < start; i++) {
			oss << ' ';
		}
		int end = message->GetEndColumn();
		for (int i = start; i < end; i++) {
			oss << '^';
		}
		oss << '\n';
		String::Utf8Value stackTrace(tryCatch->StackTrace());
		if (stackTrace.length() > 0) {
			const char* stackTraceString = toCString(stackTrace);
			oss << stackTraceString << endl;
		}

		string msg = oss.str();
		REV_ERROR_MSG(msg);
		out << msg << endl;
	}
}

bool ScriptingManager::executeString(Handle<String> source, ostream& out) {
	HandleScope handleScope;
	TryCatch tryCatch;
	ostringstream oss;
	string nameStr;
	oss << "script_" << (scriptId++);
	nameStr = oss.str();

	REV_DEBUG_MSG("Executing script '" << nameStr << "'...");

	Handle<String> name = String::New(nameStr.c_str(), nameStr.size());
	Handle<Script> script = Script::Compile(source, name);
	if (script.IsEmpty()) {
		// Print errors that happened during compilation.
		reportException(&tryCatch, out);
		return false;
	} else {
		Handle<Value> result = script->Run();
		if (result.IsEmpty()) {
			assert(tryCatch.HasCaught());
			// Print errors that happened during execution.
			reportException(&tryCatch, out);
			return false;
		} else {
			assert(!tryCatch.HasCaught());
			if (!result->IsUndefined()) {
				// If all went well and the result wasn't undefined then print
				// the returned value.
				String::Utf8Value str(result);
				const char* cstr = toCString(str);
				out << cstr << endl;
			}
			return true;
		}
	}
}

bool ScriptingManager::loadMainScriptFunctions(ScriptingComponent* c,
		std::ostream& errOut) {
	HandleScope handleScope;
	Context::Scope contextScope(context);
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// DESTROY FUNCTION:
	if (!c->destroyFunc.IsEmpty()) {
		// Already called in ScriptingManager::loadScript();
//		REV_TRACE_MSG("Calling destroy() function of " << COMPONENT_ID << "...");
//		TryCatch tryCatch;
//		Handle<Value> result = c->destroyFunc->Call(context->Global(), 0, nullptr);
//
//		if (!handleTryCatch(tryCatch, c)) {
//			return false;
//		}
		c->destroyFunc.Dispose();
	}

	Local<Object> globalObj = context->Global();

	Handle<Value> destroyVal = globalObj->Get(String::New("destroy"));
	if (!destroyVal->IsFunction()) {
		REV_WARN_MSG("There is no 'destroy' function in " << COMPONENT_ID << " script.");
	} else {
		c->destroyFunc = Persistent<Function>::New(Handle<Function>::Cast(destroyVal));
	}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// UPDATE FUNCTION:
	Handle<Value> updateVal = globalObj->Get(String::New("update"));

	if (!updateVal->IsFunction()) {
		REV_ERROR_MSG("Cannot find function 'update' in " << COMPONENT_ID << " script!");
		errOut << "Cannot find function 'update' in " << COMPONENT_ID << " script!" << endl;
		return false;
	}
	c->updateFunc.Dispose();
	c->updateFunc = Persistent<Function>::New(Handle<Function>::Cast(updateVal));

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// INIT FUNCTION:
	c->initFunc.Dispose();
	Handle<Value> initVal = globalObj->Get(String::New("init"));
	if (!initVal->IsFunction()) {
		REV_WARN_MSG("There is no 'init' function in " << COMPONENT_ID << " script.");
	} else {
		c->initFunc = Persistent<Function>::New(Handle<Function>::Cast(initVal));
		REV_TRACE_MSG("Calling 'init' of " << COMPONENT_ID << "...");
		TryCatch tryCatch;
		Handle<Value> result = c->initFunc->Call(context->Global(), 0, nullptr);

		if (!handleTryCatch(tryCatch, c)) {
			return false;
		}
		if (result == v8::False()) {
			REV_ERROR_MSG("'init' of " << COMPONENT_ID << " returned false!");
			errOut << "'init' of " << COMPONENT_ID << " returned false!" << endl;
			return false;
		}
	}

	return true;
}

bool ScriptingManager::loadBaseFunctions(ScriptingComponent* c, std::ostream& errOut) {
	HandleScope handleScope;
	Context::Scope contextScope(context);
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// DESTROY FUNCTION:
	if (!c->destroyFunc.IsEmpty()) {
		// Already called in ScriptingManager::loadScript();

//		REV_TRACE_MSG("Calling destroy() function of " << COMPONENT_ID << "...");
//		TryCatch tryCatch;
//		Handle<Value> result = c->destroyFunc->Call(context->Global(), 0, nullptr);
//
//		if (!handleTryCatch(tryCatch, c)) {
//			return false;
//		}
		c->destroyFunc.Dispose();
	}

	Handle<Value> selfValue = context->Global()->Get(String::New("self"));
	if (!selfValue->IsObject()) {
		REV_ERROR_MSG("There is no 'self' object in " << COMPONENT_ID << " script!!");
		errOut << "There is no 'self' object in " << COMPONENT_ID << " script!!" << endl;
		return false;
	}
	Handle<Object> selfObj = Handle<Object>::Cast(selfValue);

	Handle<Value> destroyVal = selfObj->Get(String::New("destroy"));
	if (!destroyVal->IsFunction()) {
		REV_WARN_MSG("There is no 'self.destroy' function in " << COMPONENT_ID << " script.");
	} else {
		c->destroyFunc = Persistent<Function>::New(Handle<Function>::Cast(destroyVal));
	}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// UPDATE FUNCTION:
	Handle<Value> updateVal = selfObj->Get(String::New("update"));

	if (!updateVal->IsFunction()) {
		REV_ERROR_MSG("Cannot find function 'self.update' in " << COMPONENT_ID << " script!");
		errOut << "Cannot find function 'self.update' in " << COMPONENT_ID << " script!" << endl;
		return false;
	}
	c->updateFunc.Dispose();
	c->updateFunc = Persistent<Function>::New(Handle<Function>::Cast(updateVal));

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// INIT FUNCTION:
	c->initFunc.Dispose();
	Handle<Value> initVal = selfObj->Get(String::New("init"));
	if (!initVal->IsFunction()) {
		REV_WARN_MSG("There is no 'self.init' function in " << COMPONENT_ID << " script.");
	} else {
		c->initFunc = Persistent<Function>::New(Handle<Function>::Cast(initVal));
		REV_TRACE_MSG("Calling 'self.init' of " << COMPONENT_ID << "...");
		TryCatch tryCatch;
		Handle<Value> result = c->initFunc->Call(context->Global(), 0, nullptr);

		if (!handleTryCatch(tryCatch, c)) {
			return false;
		}
		if (result == v8::False()) {
			REV_ERROR_MSG("'self.init' of " << COMPONENT_ID << " returned false!");
			errOut << "'self.init' of " << COMPONENT_ID << " returned false!" << endl;
			return false;
		}
	}

	return true;
}

bool ScriptingManager::loadScript(ScriptingComponent* c, std::ostream& errOut) {
	REV_DEBUG_MSG("Loading script '" << COMPONENT_ID << "'...");

	callDestroy(c);

	std::string path = ResourcePath::get(c->getScriptFile());
	if (!loadScriptFromFile(path.c_str(), errOut, !isMainScript(c))) {
		c->compiled = false;
		handleError(c);
		return false;
	}
	if (isMainScript(c)) {
		if (!loadMainScriptFunctions(c, errOut)) {
			c->compiled = true;
			handleError(c);
			return false;
		}
	} else {
		if (!loadBaseFunctions(c, errOut)) {
			c->compiled = true;
			handleError(c);
			return false;
		}
	}

	c->compiled = true;
	c->invalid = false;
	EventManager::ref().fireScriptCompiledEvent(c);
	return true;
}

bool ScriptingManager::loadScriptFromStream(std::istream& in, std::ostream& errOut,
		bool usePrivateSpace) {
	HandleScope handleScope;
	// Enter the new context so all the following operations take place within it.
	Context::Scope contextScope(context);

	string s;
	string source;

	if (usePrivateSpace) {
		source += PRIVATE_SCRIPT_SPACE_BEGIN;
	}
	while (in.good()) {
		getline(in, s);
		source += s;
		source += "\n";
	}
	if (usePrivateSpace) {
		source += PRIVATE_SCRIPT_SPACE_END;
	}

	if (!executeString(String::New(source.c_str(), source.size()), errOut)) {
		return false;
	}

	return true;
}

bool ScriptingManager::loadScriptFromFile(const char* filename, std::ostream& errOut,
		bool usePrivateSpace) {
	ifstream in(filename);
	if (!in) {
		ERROR_MSG("FILE NOT FOUND '" << filename << "'!!");
		return nullptr;
	}

	return loadScriptFromStream(in, errOut, usePrivateSpace);
}

