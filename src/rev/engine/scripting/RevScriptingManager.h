/*
 * RevScriptingManager.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTINGMANAGER_H_
#define REVSCRIPTINGMANAGER_H_

#include <vector>
#include <string>
#include <v8/v8.h>
#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/components/RevScriptingComponent.h>

namespace rev {

	//-----------------------------------------------------------------
	// ScriptingTimeoutStruct
	//=================================================================
	struct ScriptingTimeoutStruct {
		int id; // for clearTimeout
		float invokeTime;
		ScriptingComponent* component;
		v8::Persistent<v8::Function> function;

		~ScriptingTimeoutStruct() {
			function.Dispose();
			function.Clear();
		}
	};

	//-----------------------------------------------------------------
	// ScriptingIntervalStruct
	//=================================================================
	struct ScriptingIntervalStruct {
		int id; // for clearTimeout
		float nextInvokeTime;
		float interval;
		ScriptingComponent* component;
		v8::Persistent<v8::Function> function;

		~ScriptingIntervalStruct() {
			function.Dispose();
			function.Clear();
		}
	};

	typedef std::vector<ScriptingComponent*> ScriptingComponentVect;
	typedef std::vector<ScriptingTimeoutStruct*> TimeoutVector;
	typedef std::vector<ScriptingIntervalStruct*> IntervalVector;

	//-----------------------------------------------------------------
	// ScriptingManager
	//=================================================================
	class ScriptingManager: public IBindable {
		static ScriptingManager* scriptingManager;
		static int scriptId;

		ScriptingComponentVect components;
		ScriptingComponent* mainScriptComponent = nullptr;
		ScriptingComponent* currentComponent = nullptr;
		int timeIdCounter = 0;
		TimeoutVector timeoutVector;
		IntervalVector intervalVector;

		v8::Persistent<v8::Context> context;
		bool pause = false;
		bool inited = false;
		float scriptingTime = 0.0;

		int activeCount = 0;
		int compiledCount = 0;
		int invalidCount = 0;

		ScriptingManager();
		ScriptingManager(const ScriptingManager&) = delete;
		~ScriptingManager();

	public:
		DECLARE_BINDABLE_SINGLETON(ScriptingManager)

		static void createSingleton();
		static void destroySingleton();

		static ScriptingManager& ref() {
			return *scriptingManager;
		}

		static ScriptingManager* getInstance() {
			return scriptingManager;
		}

		void update();

		v8::Persistent<v8::Context>& getContext() {
			return context;
		}

		bool isInited() {
			return inited;
		}

		bool isPaused() {
			return pause;
		}

		void setPaused(bool paused) {
			this->pause = paused;
		}

		int getScriptCount() {
			// +1 for main script:
			return components.size() + 1;
		}
		int getActiveCount() {
			return activeCount;
		}
		int getCompiledCount() {
			return compiledCount;
		}
		int getInvalidCount() {
			return invalidCount;
		}
		int getTimeoutCount() {
			return timeoutVector.size();
		}
		int getIntervalCount() {
			return intervalVector.size();
		}

		bool isMainScript(ScriptingComponent* comp) {
			return comp == mainScriptComponent;
		}

		ScriptingComponent* getMainScriptingComponent() {
			return mainScriptComponent;
		}
		const std::string& getMainScriptFile() {
			return mainScriptComponent->getScriptFile();
		}
		void setMainScriptFile(const std::string& path) {
			mainScriptComponent->setScriptFile(path);
		}
		void reloadMainScript() {
			mainScriptComponent->compiled = false;
			mainScriptComponent->invalid = false;
			mainScriptComponent->active = true;
		}

		/**
		 * ScriptingManager is NOT owner of added component.
		 */
		void addComponent(ScriptingComponent* comp);
		void removeComponent(ScriptingComponent* comp);
		void removeAllComponents();

		ScriptingComponentVect& getComponents() {
			return components;
		}

		bool init();

	private:
		static v8::Handle<v8::Value> jsLog(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsSetTimeout(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsClearTimeout(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsSetInterval(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsClearInterval(const v8::Arguments& args);

		void removeAllIntervals();
		void removeAllTimeouts();
		void removeMainScript();
		void updateMainScript();
		void updateStatistics();
		void updateTimeouts();
		void updateIntervals();
		void clearTimeouts(ScriptingComponent* c);
		void clearIntervals(ScriptingComponent* c);
		void callDestroy(ScriptingComponent* c);
		bool loadScript(ScriptingComponent* c, std::ostream& errOut);
		bool loadScriptFromStream(std::istream& in, std::ostream& errOut, bool usePrivateSpace);
		bool loadScriptFromFile(const char* filename, std::ostream& errOut, bool usePrivateSpace);

		inline const char* toCString(const v8::String::Utf8Value& value) {
			return *value ? *value : "<string conversion failed>";
		}

		void reportException(v8::TryCatch* tryCatch, std::ostream& out);
		bool executeString(v8::Handle<v8::String> source, std::ostream& out);
		bool loadLibScripts();
		bool loadBaseFunctions(ScriptingComponent* c, std::ostream& errOut);
		bool loadMainScriptFunctions(ScriptingComponent* c, std::ostream& errOut);
		void handleError(ScriptingComponent* c);
		bool handleTryCatch(v8::TryCatch& tryCatch, ScriptingComponent* c);

	protected:
		void bind(IBinder& binder) override;
	};

}
/* namespace rev */
#endif /* REVSCRIPTINGMANAGER_H_ */
