/*
 * RevScriptType.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTTYPE_H_
#define REVSCRIPTTYPE_H_

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <v8/v8.h>
#include <rev/gl/RevColor.h>
#include <rev/common/RevAssert.h>

namespace rev {

	template<typename T>
	class ScriptType {
		ScriptType() = delete;
		ScriptType(const ScriptType&) = delete;
		~ScriptType() = delete;

	public:
		static T castFromJS(const v8::Handle<v8::Value>& value);
		static v8::Handle<v8::Value> castToJS(const T& value);
		static std::string toString(v8::Handle<v8::Value>& value);

	private:
		template<typename S, int LENGTH>
		static void populateArrayFromJS(S* array, const v8::Handle<v8::Value>& value);

		template<typename S, int LENGTH>
		static void populateArrayToJS(v8::Handle<v8::Array>& destArray, const S* srcArray);
	};

	template<>
	inline v8::Handle<v8::Value> ScriptType<std::string>::castToJS(
			const std::string& value) {
		return v8::String::New(value.c_str());
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<bool>::castToJS(const bool& value) {
		return v8::Boolean::New(value);
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<int>::castToJS(const int& value) {
		return v8::Int32::New(value);
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<uint32_t>::castToJS(
			const uint32_t& value) {
		return v8::Uint32::New(value);
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<int64_t>::castToJS(
			const int64_t& value) {
		return v8::Number::New(static_cast<double>(value));
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<float>::castToJS(const float& value) {
		return v8::Number::New(static_cast<double>(value));
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<double>::castToJS(const double& value) {
		return v8::Number::New(value);
	}

	template<typename T>
	template<typename S, int LENGTH>
	inline void ScriptType<T>::populateArrayToJS(v8::Handle<v8::Array>& destArray,
			const S* srcArray) {
		revAssert(destArray->Length() == LENGTH);
		for (int i = 0; i < LENGTH; i++) {
			destArray->Set(i, ScriptType<S>::castToJS(srcArray[i]));
		}
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::vec2>::castToJS(
			const glm::vec2& value) {
		v8::Handle<v8::Array> result = v8::Array::New(2);
		populateArrayToJS<float, 2>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::vec3>::castToJS(
			const glm::vec3& value) {
		v8::Handle<v8::Array> result = v8::Array::New(3);
		populateArrayToJS<float, 3>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::vec4>::castToJS(
			const glm::vec4& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<float, 4>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::mat2>::castToJS(
			const glm::mat2& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<float, 4>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::mat3>::castToJS(
			const glm::mat3& value) {
		v8::Handle<v8::Array> result = v8::Array::New(9);
		populateArrayToJS<float, 9>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::mat4>::castToJS(
			const glm::mat4& value) {
		v8::Handle<v8::Array> result = v8::Array::New(16);
		populateArrayToJS<float, 16>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::quat>::castToJS(
			const glm::quat& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<float, 4>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<rev::color3>::castToJS(
			const rev::color3& value) {
		v8::Handle<v8::Array> result = v8::Array::New(3);
		populateArrayToJS<float, 3>(result, rev::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<rev::color4>::castToJS(
			const rev::color4& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<float, 4>(result, rev::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dvec2>::castToJS(
			const glm::dvec2& value) {
		v8::Handle<v8::Array> result = v8::Array::New(2);
		populateArrayToJS<double, 2>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dvec3>::castToJS(
			const glm::dvec3& value) {
		v8::Handle<v8::Array> result = v8::Array::New(3);
		populateArrayToJS<double, 3>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dvec4>::castToJS(
			const glm::dvec4& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<double, 4>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dmat2>::castToJS(
			const glm::dmat2& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<double, 4>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dmat3>::castToJS(
			const glm::dmat3& value) {
		v8::Handle<v8::Array> result = v8::Array::New(9);
		populateArrayToJS<double, 9>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dmat4>::castToJS(
			const glm::dmat4& value) {
		v8::Handle<v8::Array> result = v8::Array::New(16);
		populateArrayToJS<double, 16>(result, glm::value_ptr(value));
		return result;
	}

	template<>
	inline v8::Handle<v8::Value> ScriptType<glm::dquat>::castToJS(
			const glm::dquat& value) {
		v8::Handle<v8::Array> result = v8::Array::New(4);
		populateArrayToJS<double, 4>(result, glm::value_ptr(value));
		return result;
	}

	//==================================================================================

	template<>
	inline std::string ScriptType<std::string>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		v8::String::Utf8Value s(value->ToString());
		return std::string(*s);
	}

	template<>
	inline bool ScriptType<bool>::castFromJS(const v8::Handle<v8::Value>& value) {
		return value->BooleanValue();
	}

	template<>
	inline int ScriptType<int>::castFromJS(const v8::Handle<v8::Value>& value) {
		return value->Int32Value();
	}

	template<>
	inline uint32_t ScriptType<uint32_t>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		return value->Uint32Value();
	}

	template<>
	inline int64_t ScriptType<int64_t>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		return value->IntegerValue();
	}

	template<>
	inline double ScriptType<double>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		return value->NumberValue();
	}

	template<>
	inline float ScriptType<float>::castFromJS(const v8::Handle<v8::Value>& value) {
		return static_cast<float>(value->NumberValue());
	}

	template<typename T>
	template<typename S, int LENGTH>
	inline void ScriptType<T>::populateArrayFromJS(S* array,
			const v8::Handle<v8::Value>& value) {
		revAssert(value->IsArray());
		v8::Handle<v8::Array> obj = v8::Handle<v8::Array>::Cast(value);
		revAssert(obj->Length() == LENGTH);
		for (int i = 0; i < LENGTH; i++) {
			v8::Handle<v8::Value> val = obj->Get(i);
			array[i] = ScriptType<S>::castFromJS(val);
		}
	}

	template<>
	inline glm::mat2 ScriptType<glm::mat2>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::mat2 result;
		populateArrayFromJS<float, 4>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::mat3 ScriptType<glm::mat3>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::mat3 result;
		populateArrayFromJS<float, 9>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::mat4 ScriptType<glm::mat4>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::mat4 result;
		populateArrayFromJS<float, 16>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::vec2 ScriptType<glm::vec2>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::vec2 result;
		populateArrayFromJS<float, 2>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::vec3 ScriptType<glm::vec3>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::vec3 result;
		populateArrayFromJS<float, 3>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::vec4 ScriptType<glm::vec4>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::vec4 result;
		populateArrayFromJS<float, 4>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::quat ScriptType<glm::quat>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::quat result;
		populateArrayFromJS<float, 4>(const_cast<float*>(glm::value_ptr(result)),
				value);
		return result;
	}

	template<>
	inline glm::dmat2 ScriptType<glm::dmat2>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dmat2 result;
		populateArrayFromJS<double, 4>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dmat3 ScriptType<glm::dmat3>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dmat3 result;
		populateArrayFromJS<double, 9>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dmat4 ScriptType<glm::dmat4>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dmat4 result;
		populateArrayFromJS<double, 16>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dvec2 ScriptType<glm::dvec2>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dvec2 result;
		populateArrayFromJS<double, 2>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dvec3 ScriptType<glm::dvec3>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dvec3 result;
		populateArrayFromJS<double, 3>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dvec4 ScriptType<glm::dvec4>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dvec4 result;
		populateArrayFromJS<double, 4>(glm::value_ptr(result), value);
		return result;
	}

	template<>
	inline glm::dquat ScriptType<glm::dquat>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		glm::dquat result;
		populateArrayFromJS<double, 4>(const_cast<double*>(glm::value_ptr(result)),
				value);
		return result;
	}

	template<>
	inline rev::color3 ScriptType<rev::color3>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		rev::color3 result;
		populateArrayFromJS<float, 3>(rev::value_ptr(result), value);
		return result;
	}

	template<>
	inline rev::color4 ScriptType<rev::color4>::castFromJS(
			const v8::Handle<v8::Value>& value) {
		rev::color4 result;
		populateArrayFromJS<float, 4>(rev::value_ptr(result), value);
		return result;
	}

}
/* namespace rev */
#endif /* REVSCRIPTTYPE_H_ */
