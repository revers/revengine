/*
 * RevScriptObjectBinder.cpp
 *
 *  Created on: 28-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevScriptObjectBinder.h"

using namespace rev;

ScriptObjectBinder::ScriptObjectBinder(const char* name, IBindable* bindable) :
		name(name) {
	if (bindable) {
		bindObject(bindable);
	}
}

ScriptObjectBinder::~ScriptObjectBinder() {
	unbindObject(baseBindable);
}

void ScriptObjectBinder::unbindWithoutClear() {
	if (baseBindable) {
		baseBindable = nullptr;
	}
}

void ScriptObjectBinder::unbindObject(IBindable* bindable) {
	if (bindable && bindable == baseBindable) {
		clearAll();
		baseBindable = nullptr;
	}
}

void ScriptObjectBinder::bindObject(IBindable* bindable) {
	if (!bindable) {
		REV_DEBUG_MSG("null bindable");
		return;
	}
	unbindObject(baseBindable);
	bindable->bind(*this);
	baseBindable = bindable;
}

void ScriptObjectBinder::clearAll() {
	for (auto& ptr : childProperties) {
		delete ptr;
	}

	childProperties.clear();
}

void ScriptObjectBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<IBindable*> wrapper, bool editable, bool visible, bool expanded) {
	childProperties.push_back(new BindablePropertyAccessor(name, std::move(wrapper)));
}

/**
 * BasicTypeListMemberWrapper
 */
void ScriptObjectBinder::bindList(IBindable* bindable, const char* name,
		BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	childProperties.push_back(new BasicTypeListPropertyAccessor(name, std::move(wrapper)));
}

/**
 * BindableListMemberWrapper
 */
void ScriptObjectBinder::bindList(IBindable* bindable, const char* name,
		BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	childProperties.push_back(new BindableListPropertyAccessor(name, std::move(wrapper)));
}

