/*
 * RevScriptObjectBinder.h
 *
 *  Created on: 28-02-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTOBJECTBINDER_H_
#define REVSCRIPTOBJECTBINDER_H_

#include <string>
#include <ostream>
#include <vector>
#include <rev/engine/binding/RevIBinder.h>
#include "RevScriptPropertyAccessor.h"

namespace rev {
	typedef std::vector<ScriptPropertyAccessor*> ScriptPropertyAccessorVector;

	class ScriptObjectBinder: public IBinder {
		IBindable* baseBindable = nullptr;
		ScriptPropertyAccessorVector childProperties;
		std::string name;

	public:
		ScriptObjectBinder() {
		}
		ScriptObjectBinder(const char* name, IBindable* bindable);

		virtual ~ScriptObjectBinder();

		const char* getName() {
			return name.c_str();
		}

		const char* getType() {
			return baseBindable->getBindableName();
		}

		IBindable* getBindable() {
			return baseBindable;
		}

		bool isBound() {
			return baseBindable != nullptr;
		}

		ScriptPropertyAccessorVector& getChildProperties() {
			return childProperties;
		}

		void unbindWithoutClear();
		void bindObject(IBindable* bindable) override;
		void unbindObject(IBindable* bindable) override;

	private:
		void clearAll();

		template<typename T>
		void addNewValue(IBindable* bindable, const char* name,
				MemberWrapper<T> wrapper, bool editable, bool visible, bool expanded) {
			childProperties.push_back(
					new ObjectPropertyAccessor<T>(name, std::move(wrapper),
							!editable));
		}

	private:
		// Inherited methods:

		/**
		 * IBindable.
		 */
		virtual void bind(IBindable* bindable, const char* name, MemberWrapper<IBindable*> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * BasicTypeListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name,
				BasicTypeListMemberWrapper wrapper, bool editable,
				bool visible, bool expanded) override;

		/**
		 * BindableListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name, BindableListMemberWrapper wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * filepath (std::string)
		 */
		virtual void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter,
				bool editable, bool visible) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					true);
		}

		/**
		 * int
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<int> wrapper, bool editable,
				bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * float
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<float> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * double
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<double> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * bool
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<bool> wrapper, bool editable,
				bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * std::string
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<std::string> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * quat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::quat> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dquat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dquat> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

	};

} /* namespace rev */
#endif /* REVSCRIPTOBJECTBINDER_H_ */
