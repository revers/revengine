/*
 * RevBindableListScriptObject.h
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#ifndef REVBINDABLELISTSCRIPTOBJECT_H_
#define REVBINDABLELISTSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <rev/engine/binding/RevBindableListBinder.h>

namespace rev {

	class BindableListScriptObject {
		static v8::Persistent<v8::FunctionTemplate> funcTemplate;
		BindableListBinder binder;

		BindableListScriptObject(const BindableListScriptObject&) = delete;

	public:
		BindableListScriptObject(BindableListBinder binder) :
				binder(std::move(binder)) {
		}
		~BindableListScriptObject() {
		}

	public:
		static v8::Handle<v8::Value> wrap(BindableListScriptObject* obj);
		static BindableListScriptObject* unwrap(v8::Handle<v8::Value>& val);
		v8::Handle<v8::Value> wrap() {
			return BindableListScriptObject::wrap(this);
		}

	private:
		static v8::Handle<v8::FunctionTemplate> makeTemplate();
		static void weakJSObjectCallback(v8::Persistent<v8::Value> object, void* parameter);
		static v8::Handle<v8::Value> constructorCall(const v8::Arguments& args);

		static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
				const v8::AccessorInfo& info);

		static v8::Handle<v8::Value> scriptGetSize(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptIterator(const v8::Arguments& args);
	};

} /* namespace rev */
#endif /* REVBINDABLELISTSCRIPTOBJECT_H_ */
