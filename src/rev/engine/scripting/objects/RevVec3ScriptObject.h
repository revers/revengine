/*
 * RevVec3ScriptObject.h
 *
 *  Created on: 25-02-2013
 *      Author: Revers
 */

#ifndef REVVEC3SCRIPTOBJECT_H_
#define REVVEC3SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class Vec3ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Vec3ScriptObject() = delete;
        Vec3ScriptObject(const Vec3ScriptObject&) = delete;
        ~Vec3ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::vec3* v);
        static glm::vec3* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getX(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setX(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getY(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setY(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getZ(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setZ(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVVEC3SCRIPTOBJECT_H_ */
