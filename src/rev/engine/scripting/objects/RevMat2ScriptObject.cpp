/*
 * RevMat2ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <glm/gtc/type_ptr.hpp>
#include <rev/common/RevErrorStream.h>
#include "RevMat2ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Mat2ScriptObject::funcTemplate;

glm::mat2* Mat2ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::mat2*>(ptr);
}

v8::Handle<v8::Value> Mat2ScriptObject::wrap(glm::mat2* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Mat2ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_mat2"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("m11"), getM11, setM11);
    instance->SetAccessor(v8::String::New("m12"), getM12, setM12);
    instance->SetAccessor(v8::String::New("m21"), getM21, setM21);
    instance->SetAccessor(v8::String::New("m22"), getM22, setM22);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Mat2ScriptObject::getM11(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    const glm::mat2& m = *val;
    return Number::New((double) (m[0][0]));
}

void Mat2ScriptObject::setM11(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    glm::mat2& m = *val;
    m[0][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat2ScriptObject::getM12(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    const glm::mat2& m = *val;
    return Number::New((double) (m[1][0]));
}

void Mat2ScriptObject::setM12(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    glm::mat2& m = *val;
    m[1][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat2ScriptObject::getM21(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    const glm::mat2& m = *val;
    return Number::New((double) (m[0][1]));
}

void Mat2ScriptObject::setM21(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    glm::mat2& m = *val;
    m[0][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat2ScriptObject::getM22(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    const glm::mat2& m = *val;
    return Number::New((double) (m[1][1]));
}

void Mat2ScriptObject::setM22(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    glm::mat2& m = *val;
    m[1][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat2ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    const float* v = glm::value_ptr(*val);
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Mat2ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat2* val = static_cast<glm::mat2*>(wrap->Value());
    float* v = glm::value_ptr(*val);
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
