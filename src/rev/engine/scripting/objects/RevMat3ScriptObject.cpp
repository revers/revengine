/*
 * RevMat3ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <glm/gtc/type_ptr.hpp>
#include <rev/common/RevErrorStream.h>
#include "RevMat3ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Mat3ScriptObject::funcTemplate;

glm::mat3* Mat3ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::mat3*>(ptr);
}

v8::Handle<v8::Value> Mat3ScriptObject::wrap(glm::mat3* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Mat3ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_mat3"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("m11"), getM11, setM11);
    instance->SetAccessor(v8::String::New("m12"), getM12, setM12);
    instance->SetAccessor(v8::String::New("m13"), getM13, setM13);
    instance->SetAccessor(v8::String::New("m21"), getM21, setM21);
    instance->SetAccessor(v8::String::New("m22"), getM22, setM22);
    instance->SetAccessor(v8::String::New("m23"), getM23, setM23);
    instance->SetAccessor(v8::String::New("m31"), getM31, setM31);
    instance->SetAccessor(v8::String::New("m32"), getM32, setM32);
    instance->SetAccessor(v8::String::New("m33"), getM33, setM33);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Mat3ScriptObject::getM11(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[0][0]));
}

void Mat3ScriptObject::setM11(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[0][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM12(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[1][0]));
}

void Mat3ScriptObject::setM12(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[1][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM13(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[2][0]));
}

void Mat3ScriptObject::setM13(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[2][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM21(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[0][1]));
}

void Mat3ScriptObject::setM21(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[0][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM22(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[1][1]));
}

void Mat3ScriptObject::setM22(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[1][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM23(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[2][1]));
}

void Mat3ScriptObject::setM23(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[2][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM31(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[0][2]));
}

void Mat3ScriptObject::setM31(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[0][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM32(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[1][2]));
}

void Mat3ScriptObject::setM32(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[1][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getM33(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const glm::mat3& m = *val;
    return Number::New((double) (m[2][2]));
}

void Mat3ScriptObject::setM33(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    glm::mat3& m = *val;
    m[2][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat3ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 9);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    const float* v = glm::value_ptr(*val);
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Mat3ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 9);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat3* val = static_cast<glm::mat3*>(wrap->Value());
    float* v = glm::value_ptr(*val);
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
