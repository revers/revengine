/*
 * RevVec2ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevVec2ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Vec2ScriptObject::funcTemplate;

glm::vec2* Vec2ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::vec2*>(ptr);
}

v8::Handle<v8::Value> Vec2ScriptObject::wrap(glm::vec2* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Vec2ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_vec2"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("x"), getX, setX);
    instance->SetAccessor(v8::String::New("y"), getY, setY);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Vec2ScriptObject::getX(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    return Number::New((double) (val->x));
}

void Vec2ScriptObject::setX(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    val->x = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec2ScriptObject::getY(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    return Number::New((double) (val->y));
}

void Vec2ScriptObject::setY(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    val->y = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec2ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 2);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    const glm::vec2& v = *val;
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Vec2ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 2);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec2* val = static_cast<glm::vec2*>(wrap->Value());
    glm::vec2& v = *val;
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
