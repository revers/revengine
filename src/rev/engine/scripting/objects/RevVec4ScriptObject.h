/*
 * RevVec4ScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVVEC4SCRIPTOBJECT_H_
#define REVVEC4SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class Vec4ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Vec4ScriptObject() = delete;
        Vec4ScriptObject(const Vec4ScriptObject&) = delete;
        ~Vec4ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::vec4* v);
        static glm::vec4* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getX(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setX(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getY(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setY(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getZ(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setZ(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getW(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setW(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVVEC4SCRIPTOBJECT_H_ */
