/*
 * RevColor3ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevColor3ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Color3ScriptObject::funcTemplate;

rev::color3* Color3ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<rev::color3*>(ptr);
}

v8::Handle<v8::Value> Color3ScriptObject::wrap(rev::color3* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Color3ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("rev_color3"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("r"), getR, setR);
    instance->SetAccessor(v8::String::New("g"), getG, setG);
    instance->SetAccessor(v8::String::New("b"), getB, setB);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Color3ScriptObject::getR(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    return Number::New((double) (val->r));
}

void Color3ScriptObject::setR(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    val->r = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Color3ScriptObject::getG(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    return Number::New((double) (val->g));
}

void Color3ScriptObject::setG(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    val->g = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Color3ScriptObject::getB(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    return Number::New((double) (val->b));
}

void Color3ScriptObject::setB(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    val->b = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Color3ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 3);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    const rev::color3& v = *val;
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Color3ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 3);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    rev::color3* val = static_cast<rev::color3*>(wrap->Value());
    rev::color3& v = *val;
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
