/*
 * RevMat3ScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVMAT3SCRIPTOBJECT_H_
#define REVMAT3SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class Mat3ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Mat3ScriptObject() = delete;
        Mat3ScriptObject(const Mat3ScriptObject&) = delete;
        ~Mat3ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::mat3* v);
        static glm::mat3* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getM11(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM11(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM12(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM12(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM13(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM13(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM21(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM21(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM22(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM22(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM23(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM23(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM31(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM31(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM32(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM32(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM33(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM33(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVMAT3SCRIPTOBJECT_H_ */
