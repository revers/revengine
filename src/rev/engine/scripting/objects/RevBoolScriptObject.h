/*
 * RevBoolScriptObject.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVBOOLSCRIPTOBJECT_H_
#define REVBOOLSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class BoolScriptObject {
        static v8::Persistent<v8::ObjectTemplate> objTemplate;

        BoolScriptObject() = delete;
        BoolScriptObject(const BoolScriptObject&) = delete;
        ~BoolScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(bool* v);
        static bool* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::ObjectTemplate> makeTemplate();
    };

} /* namespace rev */
#endif /* REVBOOLSCRIPTOBJECT_H_ */
