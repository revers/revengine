/*
 * RevBasicTypeListScriptObject.h
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#ifndef REVBASICTYPELISTSCRIPTOBJECT_H_
#define REVBASICTYPELISTSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <rev/engine/binding/RevBasicTypeListBinder.h>

namespace rev {

	class BasicTypeListScriptObject {
		static v8::Persistent<v8::FunctionTemplate> funcTemplate;
		BasicTypeListBinder binder;

		BasicTypeListScriptObject(const BasicTypeListScriptObject&) = delete;

	public:
		BasicTypeListScriptObject(BasicTypeListBinder binder) :
				binder(std::move(binder)) {
		}
		~BasicTypeListScriptObject() {
		}

	public:
		static v8::Handle<v8::Value> wrap(BasicTypeListScriptObject* obj);
		static BasicTypeListScriptObject* unwrap(v8::Handle<v8::Value>& val);
		v8::Handle<v8::Value> wrap() {
			return BasicTypeListScriptObject::wrap(this);
		}

	private:
		static v8::Handle<v8::FunctionTemplate> makeTemplate();
		static void weakJSObjectCallback(v8::Persistent<v8::Value> object, void* parameter);
		static v8::Handle<v8::Value> constructorCall(const v8::Arguments& args);

		static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
				const v8::AccessorInfo& info);
		static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
				v8::Local<v8::Value> value, const v8::AccessorInfo& info);

		static v8::Handle<v8::Value> scriptGetSize(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptIterator(const v8::Arguments& args);
	};

} /* namespace rev */
#endif /* REVBASICTYPELISTSCRIPTOBJECT_H_ */
