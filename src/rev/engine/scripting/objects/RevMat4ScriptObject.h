/*
 * RevMat4ScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVMAT4SCRIPTOBJECT_H_
#define REVMAT4SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class Mat4ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Mat4ScriptObject() = delete;
        Mat4ScriptObject(const Mat4ScriptObject&) = delete;
        ~Mat4ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::mat4* v);
        static glm::mat4* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getM11(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM11(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM12(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM12(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM13(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM13(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM14(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM14(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM21(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM21(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM22(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM22(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM23(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM23(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM24(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM24(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM31(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM31(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM32(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM32(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM33(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM33(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM34(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM34(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM41(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM41(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM42(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM42(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM43(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM43(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getM44(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setM44(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);

        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVMAT4SCRIPTOBJECT_H_ */
