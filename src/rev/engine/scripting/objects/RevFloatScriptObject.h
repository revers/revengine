/*
 * RevFloatScriptObject.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVFLOATSCRIPTOBJECT_H_
#define REVFLOATSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class FloatScriptObject {
        static v8::Persistent<v8::ObjectTemplate> objTemplate;

        FloatScriptObject() = delete;
        FloatScriptObject(const FloatScriptObject&) = delete;
        ~FloatScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(float* v);
        static float* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::ObjectTemplate> makeTemplate();
    };

} /* namespace rev */
#endif /* REVFLOATSCRIPTOBJECT_H_ */
