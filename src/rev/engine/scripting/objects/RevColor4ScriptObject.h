/*
 * RevColor4ScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVCOLOR4SCRIPTOBJECT_H_
#define REVCOLOR4SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <rev/gl/RevColor.h>

namespace rev {

    class Color4ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Color4ScriptObject() = delete;
        Color4ScriptObject(const Color4ScriptObject&) = delete;
        ~Color4ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(rev::color4* v);
        static rev::color4* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getR(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setR(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getG(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setG(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getB(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setB(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getA(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setA(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVCOLOR4SCRIPTOBJECT_H_ */
