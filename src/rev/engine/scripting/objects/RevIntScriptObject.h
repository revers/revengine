/*
 * RevIntScriptObject.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVINTSCRIPTOBJECT_H_
#define REVINTSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class IntScriptObject {
        static v8::Persistent<v8::ObjectTemplate> objTemplate;

        IntScriptObject() = delete;
        IntScriptObject(const IntScriptObject&) = delete;
        ~IntScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(int* v);
        static int* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::ObjectTemplate> makeTemplate();
    };

} /* namespace rev */
#endif /* REVINTSCRIPTOBJECT_H_ */
