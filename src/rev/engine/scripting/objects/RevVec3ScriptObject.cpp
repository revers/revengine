/*
 * RevVec3ScriptObject.cpp
 *
 *  Created on: 25-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevVec3ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Vec3ScriptObject::funcTemplate;

glm::vec3* Vec3ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::vec3*>(ptr);
}

v8::Handle<v8::Value> Vec3ScriptObject::wrap(glm::vec3* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Vec3ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_vec3"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("x"), getX, setX);
    instance->SetAccessor(v8::String::New("y"), getY, setY);
    instance->SetAccessor(v8::String::New("z"), getZ, setZ);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Vec3ScriptObject::getX(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    return Number::New((double) (val->x));
}

void Vec3ScriptObject::setX(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    val->x = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec3ScriptObject::getY(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    return Number::New((double) (val->y));
}

void Vec3ScriptObject::setY(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    val->y = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec3ScriptObject::getZ(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    return Number::New((double) (val->z));
}

void Vec3ScriptObject::setZ(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    val->z = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec3ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 3);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    const glm::vec3& v = *val;
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Vec3ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 3);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec3* val = static_cast<glm::vec3*>(wrap->Value());
    glm::vec3& v = *val;
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
