/*
 * RevDoubleScriptObject.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVDOUBLESCRIPTOBJECT_H_
#define REVDOUBLESCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class DoubleScriptObject {
        static v8::Persistent<v8::ObjectTemplate> objTemplate;

        DoubleScriptObject() = delete;
        DoubleScriptObject(const DoubleScriptObject&) = delete;
        ~DoubleScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(double* v);
        static double* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::ObjectTemplate> makeTemplate();
    };

} /* namespace rev */
#endif /* REVDOUBLESCRIPTOBJECT_H_ */
