/*
 * RevBindableListScriptObject.cpp
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/binding/RevBindableListBinder.h>
#include "RevBindableListScriptObject.h"
#include "RevBindableIterScriptObject.h"

using namespace rev;

#define CLASS_NAME "BindableListScriptObject"
#define OBJECT_NAME "BindableList"

v8::Persistent<v8::FunctionTemplate> BindableListScriptObject::funcTemplate;

BindableListScriptObject* BindableListScriptObject::unwrap(v8::Handle<v8::Value>& val) {
	using namespace v8;

	Handle<External> field = Handle<External>::Cast(
			Handle<Object>::Cast(val)->GetInternalField(0));
	void* ptr = field->Value();

	return static_cast<BindableListScriptObject*>(ptr);
}

v8::Handle<v8::Value> BindableListScriptObject::wrap(BindableListScriptObject* obj) {
	using namespace v8;

	HandleScope handleScope;
	if (funcTemplate.IsEmpty()) {
		funcTemplate = Persistent<FunctionTemplate>::New(makeTemplate());
	}
	v8::Handle<Value> external = v8::External::New(obj);
	v8::Handle<Value> result = funcTemplate->GetFunction()->NewInstance(1, &external);

	return handleScope.Close(result);
}

v8::Handle<v8::Value> BindableListScriptObject::constructorCall(const v8::Arguments& args) {
	using namespace v8;

	if (!args.IsConstructCall()) {
		const char* msg = "Cannot call constructor of class '" CLASS_NAME "' as function!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	HandleScope scope;
	Local<Value> external;
	if (args[0]->IsExternal()) {
		external = args[0];
		args.This()->SetInternalField(0, external);
		void* ptr = Local<External>::Cast(external)->Value();
		BindableListScriptObject* obj = static_cast<BindableListScriptObject*>(ptr);

		Persistent<Object> persistent = Persistent<Object>::New(args.This());
		persistent.MakeWeak(obj, weakJSObjectCallback);
	} else {
		const char* msg = "Calling constructor of class '" CLASS_NAME "' is forbidden!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	return args.This();
}

v8::Handle<v8::FunctionTemplate> BindableListScriptObject::makeTemplate() {
	using namespace v8;
	HandleScope handleScope;

	Handle<FunctionTemplate> templ = FunctionTemplate::New(constructorCall);
	templ->SetClassName(String::New(OBJECT_NAME));
	Handle<ObjectTemplate> instance = templ->InstanceTemplate();
	instance->SetInternalFieldCount(1);
	instance->SetIndexedPropertyHandler(getIndexedValue);

	Handle<ObjectTemplate> proto = templ->PrototypeTemplate();
	proto->Set(String::New("getSize"), FunctionTemplate::New(scriptGetSize)->GetFunction());
	proto->Set(String::New("iterator"), FunctionTemplate::New(scriptIterator)->GetFunction());

	return handleScope.Close(templ);
}

void BindableListScriptObject::weakJSObjectCallback(v8::Persistent<v8::Value> object,
		void* parameter) {
	BindableListScriptObject* obj = static_cast<BindableListScriptObject*>(parameter);

	REV_TRACE_MSG("Deleting BindableListScriptObject with name: '" << obj->binder.getName()
			<< "'...");
	delete obj;

	object.Dispose();
	object.Clear();
}

v8::Handle<v8::Value> BindableListScriptObject::getIndexedValue(uint32_t index,
		const v8::AccessorInfo& info) {
	using namespace v8;
	Local<Object> self = info.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));

	BindableListScriptObject* obj = static_cast<BindableListScriptObject*>(wrap->Value());
	return obj->binder.get(index);
}

v8::Handle<v8::Value> BindableListScriptObject::scriptGetSize(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BindableListScriptObject* obj = static_cast<BindableListScriptObject*>(wrap->Value());

	return ScriptType<int>::castToJS(obj->binder.getSize());
}

v8::Handle<v8::Value> BindableListScriptObject::scriptIterator(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BindableListScriptObject* obj = static_cast<BindableListScriptObject*>(wrap->Value());

	// This will be Garbage Collected by v8 engine.
	// See BindableIterScriptObject::weakJSObjectCallback().
	BindableIterScriptObject* iter = new BindableIterScriptObject(obj->binder.begin(),
			obj->binder.end());
	return iter->wrap();
}
