/*
 * RevBindableIterScriptObject.cpp
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/binding/RevIBindable.h>
#include "RevBindableIterScriptObject.h"
#include "../RevScriptType.h"

using namespace rev;

#define CLASS_NAME "BindableIterScriptObject"
#define OBJECT_NAME "BindableIterator"

v8::Persistent<v8::FunctionTemplate> BindableIterScriptObject::funcTemplate;

BindableIterScriptObject* BindableIterScriptObject::unwrap(v8::Handle<v8::Value>& val) {
	using namespace v8;

	Handle<External> field = Handle<External>::Cast(
			Handle<Object>::Cast(val)->GetInternalField(0));
	void* ptr = field->Value();
	return static_cast<BindableIterScriptObject*>(ptr);
}

v8::Handle<v8::Value> BindableIterScriptObject::wrap(BindableIterScriptObject* obj) {
	using namespace v8;

	HandleScope handleScope;
	if (funcTemplate.IsEmpty()) {
		funcTemplate = Persistent<FunctionTemplate>::New(makeTemplate());
	}
	v8::Handle<Value> external = v8::External::New(obj);
	v8::Handle<Value> result = funcTemplate->GetFunction()->NewInstance(1, &external);

	return handleScope.Close(result);
}

v8::Handle<v8::Value> BindableIterScriptObject::constructorCall(const v8::Arguments& args) {
	using namespace v8;

	if (!args.IsConstructCall()) {
		const char* msg = "Cannot call constructor of class '" CLASS_NAME "' as function!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	HandleScope scope;
	Local<Value> external;
	if (args[0]->IsExternal()) {
		external = args[0];
		args.This()->SetInternalField(0, external);
		void* ptr = Local<External>::Cast(external)->Value();
		BindableIterScriptObject* obj = static_cast<BindableIterScriptObject*>(ptr);

		Persistent<Object> persistent = Persistent<Object>::New(args.This());
		persistent.MakeWeak(obj, weakJSObjectCallback);
	} else {
		const char* msg = "Calling constructor of class '" CLASS_NAME "' is forbidden!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	return args.This();
}

v8::Handle<v8::FunctionTemplate> BindableIterScriptObject::makeTemplate() {
	using namespace v8;
	HandleScope handleScope;

	Handle<FunctionTemplate> templ = FunctionTemplate::New(constructorCall);
	templ->SetClassName(String::New(OBJECT_NAME));
	Handle<ObjectTemplate> instance = templ->InstanceTemplate();
	instance->SetInternalFieldCount(1);

	Handle<ObjectTemplate> proto = templ->PrototypeTemplate();
	proto->Set(String::New("hasNext"), FunctionTemplate::New(scriptHasNext)->GetFunction());
	proto->Set(String::New("next"), FunctionTemplate::New(scriptNext)->GetFunction());

	return handleScope.Close(templ);
}

void BindableIterScriptObject::weakJSObjectCallback(v8::Persistent<v8::Value> object,
		void* parameter) {
	BindableIterScriptObject* obj = static_cast<BindableIterScriptObject*>(parameter);
	REV_TRACE_MSG("Deleting BindableIterScriptObject");
	delete obj;

	object.Dispose();
	object.Clear();
}

v8::Handle<v8::Value> BindableIterScriptObject::scriptHasNext(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BindableIterScriptObject* obj = static_cast<BindableIterScriptObject*>(wrap->Value());

	if (obj->begin != obj->end) {
		return v8::True();
	}

	return v8::False();
}

v8::Handle<v8::Value> BindableIterScriptObject::scriptNext(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BindableIterScriptObject* obj = static_cast<BindableIterScriptObject*>(wrap->Value());

	if (obj->begin == obj->end) {
		revAssert(false);
		return v8::Undefined();
	}
	v8::Handle<v8::Value> value = obj->begin.getValue();
	obj->begin.operator ++();

	return value;
}
