/*
 * RevVec2ScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVVEC2SCRIPTOBJECT_H_
#define REVVEC2SCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class Vec2ScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        Vec2ScriptObject() = delete;
        Vec2ScriptObject(const Vec2ScriptObject&) = delete;
        ~Vec2ScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::vec2* v);
        static glm::vec2* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getX(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setX(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getY(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setY(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVVEC2SCRIPTOBJECT_H_ */
