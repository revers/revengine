/*
 * RevMat4ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <glm/gtc/type_ptr.hpp>
#include <rev/common/RevErrorStream.h>
#include "RevMat4ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Mat4ScriptObject::funcTemplate;

glm::mat4* Mat4ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::mat4*>(ptr);
}

v8::Handle<v8::Value> Mat4ScriptObject::wrap(glm::mat4* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Mat4ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_mat4"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("m11"), getM11, setM11);
    instance->SetAccessor(v8::String::New("m12"), getM12, setM12);
    instance->SetAccessor(v8::String::New("m13"), getM13, setM13);
    instance->SetAccessor(v8::String::New("m14"), getM14, setM14);

    instance->SetAccessor(v8::String::New("m21"), getM21, setM21);
    instance->SetAccessor(v8::String::New("m22"), getM22, setM22);
    instance->SetAccessor(v8::String::New("m23"), getM23, setM23);
    instance->SetAccessor(v8::String::New("m24"), getM24, setM24);

    instance->SetAccessor(v8::String::New("m31"), getM31, setM31);
    instance->SetAccessor(v8::String::New("m32"), getM32, setM32);
    instance->SetAccessor(v8::String::New("m33"), getM33, setM33);
    instance->SetAccessor(v8::String::New("m34"), getM34, setM34);

    instance->SetAccessor(v8::String::New("m41"), getM41, setM41);
    instance->SetAccessor(v8::String::New("m42"), getM42, setM42);
    instance->SetAccessor(v8::String::New("m43"), getM43, setM43);
    instance->SetAccessor(v8::String::New("m44"), getM44, setM44);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Mat4ScriptObject::getM11(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[0][0]));
}

void Mat4ScriptObject::setM11(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[0][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM12(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[1][0]));
}

void Mat4ScriptObject::setM12(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[1][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM13(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[2][0]));
}

void Mat4ScriptObject::setM13(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[2][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM14(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[3][0]));
}

void Mat4ScriptObject::setM14(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[3][0] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM21(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[0][1]));
}

void Mat4ScriptObject::setM21(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[0][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM22(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[1][1]));
}

void Mat4ScriptObject::setM22(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[1][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM23(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[2][1]));
}

void Mat4ScriptObject::setM23(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[2][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM24(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[3][1]));
}

void Mat4ScriptObject::setM24(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[3][1] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM31(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[0][2]));
}

void Mat4ScriptObject::setM31(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[0][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM32(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[1][2]));
}

void Mat4ScriptObject::setM32(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[1][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM33(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[2][2]));
}

void Mat4ScriptObject::setM33(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[2][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM34(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[3][2]));
}

void Mat4ScriptObject::setM34(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[3][2] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM41(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[0][3]));
}

void Mat4ScriptObject::setM41(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[0][3] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM42(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[1][3]));
}

void Mat4ScriptObject::setM42(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[1][3] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM43(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[2][3]));
}

void Mat4ScriptObject::setM43(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[2][3] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getM44(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const glm::mat4& m = *val;
    return Number::New((double) (m[3][3]));
}

void Mat4ScriptObject::setM44(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    glm::mat4& m = *val;
    m[3][3] = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Mat4ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 16);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    const float* v = glm::value_ptr(*val);
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Mat4ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 16);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::mat4* val = static_cast<glm::mat4*>(wrap->Value());
    float* v = glm::value_ptr(*val);
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
