/*
 * RevVec4ScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevVec4ScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> Vec4ScriptObject::funcTemplate;

glm::vec4* Vec4ScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::vec4*>(ptr);
}

v8::Handle<v8::Value> Vec4ScriptObject::wrap(glm::vec4* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> Vec4ScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_vec4"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("x"), getX, setX);
    instance->SetAccessor(v8::String::New("y"), getY, setY);
    instance->SetAccessor(v8::String::New("z"), getZ, setZ);
    instance->SetAccessor(v8::String::New("w"), getW, setW);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> Vec4ScriptObject::getX(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    return Number::New((double) (val->x));
}

void Vec4ScriptObject::setX(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    val->x = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec4ScriptObject::getY(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    return Number::New((double) (val->y));
}

void Vec4ScriptObject::setY(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    val->y = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec4ScriptObject::getZ(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    return Number::New((double) (val->z));
}

void Vec4ScriptObject::setZ(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    val->z = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec4ScriptObject::getW(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    return Number::New((double) (val->w));
}

void Vec4ScriptObject::setW(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    val->w = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> Vec4ScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    const glm::vec4& v = *val;
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> Vec4ScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::vec4* val = static_cast<glm::vec4*>(wrap->Value());
    glm::vec4& v = *val;
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
