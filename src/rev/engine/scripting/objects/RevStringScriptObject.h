/*
 * RevStringScriptObject.h
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#ifndef REVSTRINGSCRIPTOBJECT_H_
#define REVSTRINGSCRIPTOBJECT_H_

#include <string>
#include <v8/v8.h>
#include <glm/glm.hpp>

namespace rev {

    class StringScriptObject {
        static v8::Persistent<v8::ObjectTemplate> objTemplate;

        StringScriptObject() = delete;
        StringScriptObject(const StringScriptObject&) = delete;
        ~StringScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(std::string* v);
        static std::string* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::ObjectTemplate> makeTemplate();
    };

} /* namespace rev */
#endif /* REVSTRINGSCRIPTOBJECT_H_ */
