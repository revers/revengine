/*
 * RevQuatScriptObject.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevQuatScriptObject.h"

using namespace rev;

v8::Persistent<v8::FunctionTemplate> QuatScriptObject::funcTemplate;

glm::quat* QuatScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;

    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<glm::quat*>(ptr);
}

v8::Handle<v8::Value> QuatScriptObject::wrap(glm::quat* v) {
    v8::HandleScope handleScope;
    if (funcTemplate.IsEmpty()) {
        funcTemplate = v8::Persistent<v8::FunctionTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance =
            funcTemplate->InstanceTemplate()->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::FunctionTemplate> QuatScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::FunctionTemplate> templ = v8::FunctionTemplate::New();
    templ->SetClassName(v8::String::New("glm_quat"));
    v8::Handle<v8::ObjectTemplate> instance = templ->InstanceTemplate();
    instance->SetInternalFieldCount(1);
    instance->SetAccessor(v8::String::New("x"), getX, setX);
    instance->SetAccessor(v8::String::New("y"), getY, setY);
    instance->SetAccessor(v8::String::New("z"), getZ, setZ);
    instance->SetAccessor(v8::String::New("w"), getW, setW);
    instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);
    return handleScope.Close(templ);
}

v8::Handle<v8::Value> QuatScriptObject::getX(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    return Number::New((double) (val->x));
}

void QuatScriptObject::setX(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    val->x = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> QuatScriptObject::getY(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    return Number::New((double) (val->y));
}

void QuatScriptObject::setY(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    val->y = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> QuatScriptObject::getZ(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    return Number::New((double) (val->z));
}

void QuatScriptObject::setZ(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    val->z = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> QuatScriptObject::getW(
        v8::Local<v8::String> property, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    return Number::New((double) (val->w));
}

void QuatScriptObject::setW(v8::Local<v8::String> property,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    val->w = static_cast<float>(value->NumberValue());
}

v8::Handle<v8::Value> QuatScriptObject::getIndexedValue(uint32_t index,
        const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    const glm::quat& v = *val;
    return Number::New(static_cast<double>(v[index]));
}

v8::Handle<v8::Value> QuatScriptObject::setIndexedValue(uint32_t index,
        v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
    using namespace v8;
    revAssert(index < 4);
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    glm::quat* val = static_cast<glm::quat*>(wrap->Value());
    glm::quat& v = *val;
    v[index] = static_cast<float>(value->NumberValue());
    return v8::Undefined();
}
