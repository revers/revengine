/*
 * RevBasicTypeListScriptObject.cpp
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevBasicTypeListScriptObject.h"
#include "RevBasicTypeIterScriptObject.h"
#include "../RevScriptType.h"

using namespace rev;

#define CLASS_NAME "BasicTypeListScriptObject"
#define OBJECT_NAME "BasicTypeList"

v8::Persistent<v8::FunctionTemplate> BasicTypeListScriptObject::funcTemplate;

BasicTypeListScriptObject* BasicTypeListScriptObject::unwrap(v8::Handle<v8::Value>& val) {
	using namespace v8;

	Handle<External> field = Handle<External>::Cast(
			Handle<Object>::Cast(val)->GetInternalField(0));
	void* ptr = field->Value();

	return static_cast<BasicTypeListScriptObject*>(ptr);
}

v8::Handle<v8::Value> BasicTypeListScriptObject::wrap(BasicTypeListScriptObject* obj) {
	using namespace v8;

	HandleScope handleScope;
	if (funcTemplate.IsEmpty()) {
		funcTemplate = Persistent<FunctionTemplate>::New(makeTemplate());
	}
	v8::Handle<Value> external = v8::External::New(obj);
	v8::Handle<Value> result = funcTemplate->GetFunction()->NewInstance(1, &external);

	return handleScope.Close(result);
}

v8::Handle<v8::Value> BasicTypeListScriptObject::constructorCall(const v8::Arguments& args) {
	using namespace v8;

	if (!args.IsConstructCall()) {
		const char* msg = "Cannot call constructor of class '" CLASS_NAME "' as function!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	HandleScope scope;
	Local<Value> external;
	if (args[0]->IsExternal()) {
		external = args[0];
		args.This()->SetInternalField(0, external);
		void* ptr = Local<External>::Cast(external)->Value();
		BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(ptr);

		Persistent<Object> persistent = Persistent<Object>::New(args.This());
		persistent.MakeWeak(obj, weakJSObjectCallback);
	} else {
		const char* msg = "Calling constructor of class '" CLASS_NAME "' is forbidden!";
		REV_ERROR_MSG(msg);
		return ThrowException(String::New(msg));
	}

	return args.This();
}

v8::Handle<v8::FunctionTemplate> BasicTypeListScriptObject::makeTemplate() {
	using namespace v8;
	HandleScope handleScope;

	Handle<FunctionTemplate> templ = FunctionTemplate::New(constructorCall);
	templ->SetClassName(String::New(OBJECT_NAME));
	Handle<ObjectTemplate> instance = templ->InstanceTemplate();
	instance->SetInternalFieldCount(1);
	instance->SetIndexedPropertyHandler(getIndexedValue, setIndexedValue);

	Handle<ObjectTemplate> proto = templ->PrototypeTemplate();
	proto->Set(String::New("getSize"), FunctionTemplate::New(scriptGetSize)->GetFunction());
	proto->Set(String::New("iterator"), FunctionTemplate::New(scriptIterator)->GetFunction());

	return handleScope.Close(templ);
}

void BasicTypeListScriptObject::weakJSObjectCallback(v8::Persistent<v8::Value> object,
		void* parameter) {
	BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(parameter);

	REV_TRACE_MSG("Deleting BasicTypeListScriptObject with name: '" << obj->binder.getName()
			<< "' and type '" << obj->binder.getType() << "'...");
	delete obj;

	object.Dispose();
	object.Clear();
}

v8::Handle<v8::Value> BasicTypeListScriptObject::getIndexedValue(uint32_t index,
		const v8::AccessorInfo& info) {
	using namespace v8;
	Local<Object> self = info.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));

	BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(wrap->Value());
	return obj->binder.get(index);
}

v8::Handle<v8::Value> BasicTypeListScriptObject::setIndexedValue(uint32_t index,
		v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
	using namespace v8;

	Local<Object> self = info.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));

	BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(wrap->Value());
	obj->binder.set(index, value);

	return v8::Undefined();
}

v8::Handle<v8::Value> BasicTypeListScriptObject::scriptGetSize(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(wrap->Value());

	return ScriptType<int>::castToJS(obj->binder.getSize());
}

v8::Handle<v8::Value> BasicTypeListScriptObject::scriptIterator(const v8::Arguments& args) {
	using namespace v8;

	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	BasicTypeListScriptObject* obj = static_cast<BasicTypeListScriptObject*>(wrap->Value());

	// This will be Garbage Collected by v8 engine.
	// See BasicTypeIterScriptObject::weakJSObjectCallback().
	BasicTypeIterScriptObject* iter = new BasicTypeIterScriptObject(obj->binder.begin(),
			obj->binder.end());
	return iter->wrap();

}
