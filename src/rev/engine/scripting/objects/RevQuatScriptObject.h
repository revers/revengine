/*
 * RevQuatScriptObject.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#ifndef REVQUATSCRIPTOBJECT_H_
#define REVQUATSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <glm/gtc/quaternion.hpp>

namespace rev {

    class QuatScriptObject {
        static v8::Persistent<v8::FunctionTemplate> funcTemplate;

        QuatScriptObject() = delete;
        QuatScriptObject(const QuatScriptObject&) = delete;
        ~QuatScriptObject() = delete;

    public:
        static v8::Handle<v8::Value> wrap(glm::quat* v);
        static glm::quat* unwrap(v8::Handle<v8::Value>& val);

    private:
        static v8::Handle<v8::FunctionTemplate> makeTemplate();
        static v8::Handle<v8::Value> getX(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setX(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getY(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setY(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getZ(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setZ(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getW(v8::Local<v8::String> property,
                const v8::AccessorInfo& info);
        static void setW(v8::Local<v8::String> property, v8::Local<v8::Value> value,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> getIndexedValue(uint32_t index,
                const v8::AccessorInfo& info);
        static v8::Handle<v8::Value> setIndexedValue(uint32_t index,
                v8::Local<v8::Value> value, const v8::AccessorInfo& info);
    };

} /* namespace rev */
#endif /* REVQUATSCRIPTOBJECT_H_ */
