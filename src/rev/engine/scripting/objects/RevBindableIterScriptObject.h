/*
 * RevBindableIterScriptObject.h
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#ifndef REVBINDABLEITERSCRIPTOBJECT_H_
#define REVBINDABLEITERSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <rev/engine/binding/RevBindableListBinder.h>

namespace rev {

	class BindableIterScriptObject {
		static v8::Persistent<v8::FunctionTemplate> funcTemplate;
		BindableListBinderIterator begin;
		BindableListBinderIterator end;

		BindableIterScriptObject(const BindableIterScriptObject&) = delete;

	public:
		BindableIterScriptObject(BindableListBinderIterator begin,
				BindableListBinderIterator end) :
				begin(std::move(begin)), end(std::move(end)) {
		}
		~BindableIterScriptObject() {
		}

	public:
		static v8::Handle<v8::Value> wrap(BindableIterScriptObject* obj);
		static BindableIterScriptObject* unwrap(v8::Handle<v8::Value>& val);
		v8::Handle<v8::Value> wrap() {
			return BindableIterScriptObject::wrap(this);
		}

	private:
		static v8::Handle<v8::FunctionTemplate> makeTemplate();
		static void weakJSObjectCallback(v8::Persistent<v8::Value> object, void* parameter);
		static v8::Handle<v8::Value> constructorCall(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptHasNext(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptNext(const v8::Arguments& args);
	};

} /* namespace rev */
#endif /* REVBINDABLEITERSCRIPTOBJECT_H_ */
