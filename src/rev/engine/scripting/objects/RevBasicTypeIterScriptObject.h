/*
 * RevBasicTypeIterScriptObject.h
 *
 *  Created on: 16-03-2013
 *      Author: Revers
 */

#ifndef REVBASICTYPEITERSCRIPTOBJECT_H_
#define REVBASICTYPEITERSCRIPTOBJECT_H_

#include <v8/v8.h>
#include <rev/engine/binding/RevBasicTypeListBinder.h>

namespace rev {

	class BasicTypeIterScriptObject {
		static v8::Persistent<v8::FunctionTemplate> funcTemplate;
		BasicTypeListBinderIterator begin;
		BasicTypeListBinderIterator end;

		BasicTypeIterScriptObject(const BasicTypeIterScriptObject&) = delete;

	public:
		BasicTypeIterScriptObject(BasicTypeListBinderIterator begin,
				BasicTypeListBinderIterator end) :
				begin(std::move(begin)), end(std::move(end)) {
		}
		~BasicTypeIterScriptObject() {
		}

	public:
		static v8::Handle<v8::Value> wrap(BasicTypeIterScriptObject* obj);
		static BasicTypeIterScriptObject* unwrap(v8::Handle<v8::Value>& val);
		v8::Handle<v8::Value> wrap() {
			return BasicTypeIterScriptObject::wrap(this);
		}

	private:
		static v8::Handle<v8::FunctionTemplate> makeTemplate();
		static void weakJSObjectCallback(v8::Persistent<v8::Value> object, void* parameter);
		static v8::Handle<v8::Value> constructorCall(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptHasNext(const v8::Arguments& args);
		static v8::Handle<v8::Value> scriptNext(const v8::Arguments& args);
	};

} /* namespace rev */

#endif /* REVBASICTYPEITERSCRIPTOBJECT_H_ */
