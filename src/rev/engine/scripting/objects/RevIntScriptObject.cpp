/*
 * RevIntScriptObject.cpp
 *
 *  Created on: 27-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include "RevIntScriptObject.h"

using namespace rev;

v8::Persistent<v8::ObjectTemplate> IntScriptObject::objTemplate;

int* IntScriptObject::unwrap(v8::Handle<v8::Value>& val) {
    using namespace v8;
    Handle<External> field = Handle<External>::Cast(
            Handle<Object>::Cast(val)->GetInternalField(0));
    void* ptr = field->Value();

    return static_cast<int*>(ptr);
}

v8::Handle<v8::Value> IntScriptObject::wrap(int* v) {
    v8::HandleScope handleScope;
    if (objTemplate.IsEmpty()) {
        objTemplate = v8::Persistent<v8::ObjectTemplate>::New(makeTemplate());
    }
    v8::Local<v8::Object> vInstance = objTemplate->NewInstance();
    vInstance->SetInternalField(0, v8::External::New(v));
    return handleScope.Close(vInstance);
}

v8::Handle<v8::ObjectTemplate> IntScriptObject::makeTemplate() {
    v8::HandleScope handleScope;
    v8::Handle<v8::ObjectTemplate> templ = v8::ObjectTemplate::New();
    templ->SetInternalFieldCount(1);

    return handleScope.Close(templ);
}
