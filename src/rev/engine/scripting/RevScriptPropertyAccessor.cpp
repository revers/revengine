/*
 * RevScriptPropertyAccessor.cpp
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 */

#include <rev/engine/binding/RevIBindable.h>
#include "RevScriptPropertyAccessor.h"
#include "objects/RevBasicTypeListScriptObject.h"
#include "objects/RevBindableListScriptObject.h"

using namespace rev;

//------------------------------------------------------------------------
// ScriptPropertyAccessor
//========================================================================
void ScriptPropertyAccessor::setPropertyCallback(
		v8::Local<v8::String> property, v8::Local<v8::Value> value,
		const v8::AccessorInfo& info) {

	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(info.Data());
	ScriptPropertyAccessor* callBack = static_cast<ScriptPropertyAccessor*>(external->Value());
	callBack->setProperty(property, value, info);
}

v8::Handle<v8::Value> ScriptPropertyAccessor::getPropertyCallback(
		v8::Local<v8::String> property, const v8::AccessorInfo& info) {

	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(info.Data());
	ScriptPropertyAccessor* callBack = static_cast<ScriptPropertyAccessor*>(external->Value());
	return callBack->getProperty(property, info);
}

//------------------------------------------------------------------------
// BindablePropertyAccessor
//========================================================================
void BindablePropertyAccessor::setProperty(v8::Local<v8::String> property,
		v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* baseBindable = static_cast<IBindable*>(external->Value());

	revAlwaysAssertMsg(false, "Property '" << getPropertyName()
			<< "' (type IBindable*) of bindable '" << baseBindable->getBindableName()
			<< "' cannot be set!");
	//IBindable* bindable = *(wrapper->getMemberPointer(baseBindable));
	//*memberPtr = ScriptType<PropertyType>::castFromJS(value);
}

v8::Handle<v8::Value> BindablePropertyAccessor::getProperty(
		v8::Local<v8::String> property, const v8::AccessorInfo& info) {
	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* bindable = *(wrapper.getMemberPointer(external->Value()));
	if (bindable) {
		return bindable->wrap();
	}
	return v8::Null();
}

//------------------------------------------------------------------------
// BasicTypeListPropertyAccessor
//========================================================================
void BasicTypeListPropertyAccessor::setProperty(v8::Local<v8::String> property,
		v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* baseBindable = static_cast<IBindable*>(external->Value());

	revAlwaysAssertMsg(false, "Property '" << getPropertyName()
			<< "' (type IBindable*) of bindable '" << baseBindable->getBindableName()
			<< "' cannot be set!");
}

v8::Handle<v8::Value> BasicTypeListPropertyAccessor::getProperty(
		v8::Local<v8::String> property, const v8::AccessorInfo& info) {

	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* bindable = static_cast<IBindable*>(external->Value());

	// This will be Garbage Collected by v8 engine.
	// See BasicTypeListScriptObject::weakJSObjectCallback().
	BasicTypeListScriptObject* obj = new BasicTypeListScriptObject(wrapper.getBinder(bindable));
	return obj->wrap();
}

//------------------------------------------------------------------------
// BindableListPropertyAccessor
//========================================================================
void BindableListPropertyAccessor::setProperty(v8::Local<v8::String> property,
		v8::Local<v8::Value> value, const v8::AccessorInfo& info) {
	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* baseBindable = static_cast<IBindable*>(external->Value());

	revAlwaysAssertMsg(false, "Property '" << getPropertyName()
			<< "' (type IBindable*) of bindable '" << baseBindable->getBindableName()
			<< "' cannot be set!");
}

v8::Handle<v8::Value> BindableListPropertyAccessor::getProperty(
		v8::Local<v8::String> property, const v8::AccessorInfo& info) {

	v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
			info.Holder()->GetInternalField(0));
	IBindable* bindable = static_cast<IBindable*>(external->Value());

	// This will be Garbage Collected by v8 engine.
	// See BindableListScriptObject::weakJSObjectCallback().
	BindableListScriptObject* obj = new BindableListScriptObject(wrapper.getBinder(bindable));
	return obj->wrap();
}
