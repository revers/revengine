/*
 * RevScriptPropertyAccessor.h
 *
 *  Created on: 26-02-2013
 *      Author: Revers
 *
 * This file is based on http://code.google.com/p/cproxyv8/
 */

#ifndef REVSCRIPT_PROPERTY_ACESSOR_H_
#define REVSCRIPT_PROPERTY_ACESSOR_H_

#include <string>
#include <v8/v8.h>
#include <rev/engine/binding/RevMemberWrapper.h>
#include <rev/engine/binding/RevBasicTypeListMemberWrapper.h>
#include <rev/engine/binding/RevBindableListMemberWrapper.h>
#include "RevScriptType.h"

namespace rev {

	//------------------------------------------------------------------------
	// ScriptPropertyAccessor
	//========================================================================
	class ScriptPropertyAccessor {
	public:
		ScriptPropertyAccessor() = default;
		virtual ~ScriptPropertyAccessor() {
		}

		virtual void setProperty(v8::Local<v8::String> property,
				v8::Local<v8::Value> value, const v8::AccessorInfo& info) = 0;

		virtual v8::Handle<v8::Value> getProperty(v8::Local<v8::String> property,
				const v8::AccessorInfo& info) = 0;

		virtual const char* getPropertyName() = 0;

		virtual bool isReadOnly() = 0;

		/**
		 * V8 set property callback.
		 */
		static void setPropertyCallback(v8::Local<v8::String> property, v8::Local<v8::Value> value,
				const v8::AccessorInfo& info);

		/**
		 * V8 get property callback.
		 */
		static v8::Handle<v8::Value> getPropertyCallback(v8::Local<v8::String> property,
				const v8::AccessorInfo& info);
	};

	//------------------------------------------------------------------------
	// ObjectPropertyAccessor
	//========================================================================
	template<class T>
	class ObjectPropertyAccessor: public ScriptPropertyAccessor {
	public:
		typedef T PropertyType;

	private:
		MemberWrapper<PropertyType> wrapper;
		std::string name;
		bool readOnly;

	public:
		ObjectPropertyAccessor(const char* name, MemberWrapper<PropertyType> wrapper,
				bool readOnly) :
				name(name), wrapper(std::move(wrapper)), readOnly(readOnly) {
		}

		~ObjectPropertyAccessor() {
		}

		bool isReadOnly() override {
			return readOnly;
		}

		const char* getPropertyName() override {
			return name.c_str();
		}

		void setProperty(v8::Local<v8::String> property, v8::Local<v8::Value> value,
				const v8::AccessorInfo& info) override {

			v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
					info.Holder()->GetInternalField(0));
			PropertyType* memberPtr = wrapper.getMemberPointer(external->Value());

			*memberPtr = ScriptType<PropertyType>::castFromJS(value);
		}

		v8::Handle<v8::Value> getProperty(v8::Local<v8::String> property,
				const v8::AccessorInfo& info) override {

			v8::Local<v8::External> external = v8::Local<v8::External>::Cast(
					info.Holder()->GetInternalField(0));
			PropertyType* memberPtr = wrapper.getMemberPointer(external->Value());

			return ScriptType<PropertyType>::castToJS(*memberPtr);
		}
	};

	class IBindable;
	//------------------------------------------------------------------------
	// BindablePropertyAccessor
	//========================================================================
	class BindablePropertyAccessor: public ScriptPropertyAccessor {
	private:
		MemberWrapper<IBindable*> wrapper;
		std::string name;

	public:
		BindablePropertyAccessor(const char* name, MemberWrapper<IBindable*> wrapper) :
				name(name), wrapper(std::move(wrapper)) {
		}

		~BindablePropertyAccessor() {
		}

		bool isReadOnly() override {
			return true;
		}

		const char* getPropertyName() override {
			return name.c_str();
		}

		void setProperty(v8::Local<v8::String> property, v8::Local<v8::Value> value,
				const v8::AccessorInfo& info) override;

		v8::Handle<v8::Value> getProperty(v8::Local<v8::String> property,
				const v8::AccessorInfo& info) override;

	};

	//------------------------------------------------------------------------
	// BasicTypeListPropertyAccessor
	//========================================================================
	class BasicTypeListPropertyAccessor: public ScriptPropertyAccessor {
	private:
		BasicTypeListMemberWrapper wrapper;
		std::string name;

	public:
		BasicTypeListPropertyAccessor(const char* name, BasicTypeListMemberWrapper wrapper) :
				name(name), wrapper(std::move(wrapper)) {
		}

		~BasicTypeListPropertyAccessor() {
		}

		bool isReadOnly() override {
			return true;
		}

		const char* getPropertyName() override {
			return name.c_str();
		}

		void setProperty(v8::Local<v8::String> property, v8::Local<v8::Value> value,
				const v8::AccessorInfo& info) override;

		v8::Handle<v8::Value> getProperty(v8::Local<v8::String> property,
				const v8::AccessorInfo& info) override;
	};

	//------------------------------------------------------------------------
	// BindableListPropertyAccessor
	//========================================================================
	class BindableListPropertyAccessor: public ScriptPropertyAccessor {
	private:
		BindableListMemberWrapper wrapper;
		std::string name;

	public:
		BindableListPropertyAccessor(const char* name, BindableListMemberWrapper wrapper) :
				name(name), wrapper(std::move(wrapper)) {
		}

		~BindableListPropertyAccessor() {
		}

		bool isReadOnly() override {
			return true;
		}

		const char* getPropertyName() override {
			return name.c_str();
		}

		void setProperty(v8::Local<v8::String> property, v8::Local<v8::Value> value,
				const v8::AccessorInfo& info) override;

		v8::Handle<v8::Value> getProperty(v8::Local<v8::String> property,
				const v8::AccessorInfo& info) override;
	};

} /* namespace rev */
#endif /* REVSCRIPT_PROPERTY_ACESSOR_H_ */
