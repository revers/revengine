/*
 * File:   RevMemoryPool.cpp
 * Author: Revers
 *
 * Created on 29 grudzień 2012, 11:06
 *
 * Based on article by Jude Deng:
 * http://www.codeproject.com/Articles/27487/Why-to-use-memory-pool-and-how-to-implement-it
 */

#include <cstdio>
#include <cstdlib>
#include "RevMemoryPool.h"

#include <rev/common/RevAssert.h>

using namespace rev;

/**
 * Constructor. It allocates memory block from system and creates
 * a linked list to manage all memory unit.
 *
 * @param unitNum The number of units which are part of memory block.
 * @param unitSize The size of unit.
 */
MemoryPool::MemoryPool(unsigned long unitNum, unsigned long unitSize) :
blockSize(unitNum * (unitSize + sizeof (MemUnit))),
baseMemBlock(NULL),
unitSize(unitSize) {

    // Allocate a memory block:
    baseMemBlock = ::malloc(blockSize);
    alwaysAssert(baseMemBlock != NULL);

    freeMemBlockListHead = NULL;
    for (unsigned long i = 0; i < unitNum; i++) {

        MemUnit* currentUnit = (MemUnit*) ((char*) baseMemBlock
                + i * (unitSize + sizeof (MemUnit)));

        currentUnit->prev = freeMemBlockListHead;
        freeMemBlockListHead = currentUnit;
    }
}

/**
 * Destructor. Its task is to free memory block.
 */
MemoryPool::~MemoryPool() {
    ::free(baseMemBlock);
}

/**
 * Allocates a memory unit. If memory pool can't provide proper memory unit,
 * will call system function.
 *
 * @param size Memory unit size.
 * @param useMemPool Whether use memory pool.
 * @return Return a pointer to a memory unit.
 */
void* MemoryPool::alloc(unsigned long size, bool useMemPool) {
    if (size > unitSize || !useMemPool || freeMemBlockListHead == NULL) {
        revAssert(freeMemBlockListHead != NULL);
        revAssert(size <= unitSize);

        return ::malloc(size);
    }

    MemUnit* currentUnit = freeMemBlockListHead;
    freeMemBlockListHead = currentUnit->prev;

    return (void*) ((char*) currentUnit + sizeof (MemUnit));
}

/**
 * Free a memory unit. If the pointer of parameter point to a memory unit,
 * then insert it to "Free linked list". Otherwise, call system function "free".
 *
 * @param p It points to a memory unit and prepare to free it.
 */
void MemoryPool::free(void* p) {
    if (baseMemBlock < p && p < (void*) ((char*) baseMemBlock + blockSize)) {

        MemUnit* currentUnit = (MemUnit*) ((char*) p - sizeof (MemUnit));
        currentUnit->prev = freeMemBlockListHead;

        freeMemBlockListHead = currentUnit;
    } else {
        ::free(p);
    }
}
