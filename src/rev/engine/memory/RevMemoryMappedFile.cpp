/*
 * RevMemoryMappedFile.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 *
 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#include <rev/common/RevErrorStream.h>
#include "RevMemoryMappedFile.h"

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

using namespace rev;

#ifdef REV_ENGINE_WINDOWS
MemoryMappedFile::MemoryMappedFile(const char* name, int size) :
		name(name), size(size) {
	fileHeader = nullptr;
	mapFile = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, name);
	created = false;
	if (mapFile == nullptr) {
		created = true;
		mapFile = CreateFileMappingA(
				INVALID_HANDLE_VALUE, // use paging file
				nullptr, // default security
				PAGE_READWRITE, // read/write access
				0, // maximum object size (high-order DWORD)
				(unsigned int) size, // maximum object size (low-order DWORD)
				name);
	}
	if (mapFile) {
		fileHeader = MapViewOfFile(mapFile, FILE_MAP_ALL_ACCESS, 0, 0, (unsigned int) size);
	} else {
		REV_ERROR_MSG("Cannot create Memory Mapped File '" << name << "' with size: " << size);
		revAssert(false);
	}
}

MemoryMappedFile::~MemoryMappedFile() {
	if (fileHeader) {
		UnmapViewOfFile(fileHeader);
		if (mapFile) {
			CloseHandle(mapFile);
		}
	}
}

#else // Linux
// TODO: implement Linux version
#endif
