/*
 * RevMemoryMappedFile.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 *
 * Based on http://codesuppository.blogspot.com/2013/02/backdoor-simple-code-snippet-to-create.html
 * by John Ratcliff.
 */

#ifndef REVMEMORYMAPPEDFILE_H_
#define REVMEMORYMAPPEDFILE_H_

#include <string>
#include <rev/engine/config/RevEngineConfig.h>

#ifdef REV_ENGINE_WINDOWS
#include <windows.h>

namespace rev {

	class MemoryMappedFile {
		HANDLE mapFile;
		void* fileHeader;
		int size;
		std::string name;
		bool created = false;
		MemoryMappedFile(const MemoryMappedFile&) = delete;

	public:
		MemoryMappedFile(const char* name, int size);
		~MemoryMappedFile();

		/**
		 * @return file address, or nullptr in case of failure.
		 */
		void* getBaseAddress() {
			return fileHeader;
		}

		int getSize() const {
			return size;
		}

		const std::string& getName() const {
			return name;
		}

		/**
		 * Whether file was created or just opened existing file.
		 *
		 * @return true if was created, false if file was just opened.
		 */
		bool wasCreated() {
			return created;
		}
	};

} /* namespace rev */

#else // Linux
// TODO: implement Linux version
#endif

#endif /* REVMEMORYMAPPEDFILE_H_ */
