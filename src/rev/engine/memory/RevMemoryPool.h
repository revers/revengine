/*
 * File:   RevMemoryPool.h
 * Author: Revers
 *
 * Created on 29 grudzień 2012, 11:06
 *
 * Based on article by Jude Deng:
 * http://www.codeproject.com/Articles/27487/Why-to-use-memory-pool-and-how-to-implement-it
 */

#ifndef REVMEMORYPOOL_H
#define REVMEMORYPOOL_H

namespace rev {

    class MemoryPool {
    private:

        /**
         * The type of the node of LinkedList.
         */
        typedef struct _MemUnit {
            struct _MemUnit* prev;
        } MemUnit;

        /**
         * The address of memory pool.
         */
        void* baseMemBlock;

        /**
         * Head pointer to Free linkedlist.
         */
        MemUnit* freeMemBlockListHead;

        /**
         * Memory unit size.
         */
        unsigned long unitSize;

        /**
         * Memory pool size.
         */
        unsigned long blockSize;

        MemoryPool(const MemoryPool& orig);

    public:
        /**
         * Constructor. It allocates memory block from system and creates
         * a linked list to manage all memory unit.
         *
         * @param unitNum The number of units which are part of memory block.
         * @param unitSize The size of unit.
         */
        MemoryPool(unsigned long unitNum, unsigned long unitSize);

        /**
         * Destructor. Frees memory block.
         */
        ~MemoryPool();

        /**
         * Allocates a memory unit. If memory pool can't provide proper memory unit,
         * will call system function.
         *
         * @param size Memory unit size.
         * @param useMemPool Whether use memory pool.
         * @return Return a pointer to a memory unit.
         */
        void* alloc(unsigned long size, bool useMemPool = true);

        /**
         * Frees a memory unit. If the pointer of parameter point to a memory unit,
         * then insert it to "Free linked list". Otherwise, call system function "free".
         *
         * @param p It points to a memory unit and prepare to free it.
         */
        void free(void* p);
    };
}
#endif  /* REVMEMORYPOOL_H */

