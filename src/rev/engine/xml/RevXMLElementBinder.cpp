/*
 * RevXMLElementBinder.cpp
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#include <fstream>
#include <iostream>
#include <cstring>

#include "RevXMLElementBinder.h"

using namespace rev;
using namespace std;

#define XML_FILE_HEADER "<?xml version=\"1.0\"?>"

XMLElementBinder::XMLElementBinder(const char* name,
		IBindable** bindablePtr) :
		name(name), baseBindablePtr(bindablePtr) {
	if (*bindablePtr) {
		bindObject(*bindablePtr);
	}
}

XMLElementBinder::XMLElementBinder(const char* name, IBindable* bindable) :
		name(name) {
	if (bindable) {
		bindObject(bindable);
	}
}

void XMLElementBinder::unbindObject(IBindable* bindable) {
	if (bindable && bindable == baseBindable) {
		clearAll();
		baseBindable = nullptr;
	}
}

void XMLElementBinder::bindObject(IBindable* bindable) {
	if (!bindable) {
		REV_DEBUG_MSG("null bindable");
		return;
	}
	unbindObject(baseBindable);
	bindable->bind(*this);
	baseBindable = bindable;
}

void XMLElementBinder::clearAll() {
	for (auto& ptr : childElements) {
		delete ptr;
	}
	for (auto& ptr : childValues) {
		delete ptr;
	}
	for (auto& ptr : basicTypeLists) {
		delete ptr;
	}
	for (auto& ptr : bindableLists) {
		delete ptr;
	}

	childElements.clear();
	childValues.clear();
	basicTypeLists.clear();
	bindableLists.clear();
}

void XMLElementBinder::rebind(IBindable* b) {
	revAssert(baseBindablePtr != nullptr && b != nullptr);
	*baseBindablePtr = b;

	baseBindablePtr = nullptr;
}

void XMLElementBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<IBindable*> wrapper, bool editable, bool visible, bool expanded) {
	XMLElementBinder* elem = new XMLElementBinder(name,
			wrapper.getMemberPointer(bindable));
	childElements.push_back(elem);
}

/**
 * BasicTypeListBinder
 */
void XMLElementBinder::bindList(IBindable* bindable, const char* name,
		BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	basicTypeLists.push_back(wrapper.getBinder(bindable).release());
}

/**
 * BindableListMemberWrapper
 */
void XMLElementBinder::bindList(IBindable* bindable, const char* name,
		BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	bindableLists.push_back(wrapper.getBinder(bindable).release());
}

XMLElementBinder::~XMLElementBinder() {
	unbindObject(baseBindable);
	//clearAll();
}

XMLValueBinder* XMLElementBinder::getValueByName(const char* name) {
	for (XMLValueBinder*& ptr : childValues) {
		if (ptr->getName() == name) {
			return ptr;
		}
	}
	return nullptr;
}

XMLElementBinder* XMLElementBinder::getElementByName(const char* name) {
	for (XMLElementBinder*& ptr : childElements) {
		if (::strcmp(ptr->getName(), name) == 0) {
			return ptr;
		}
	}
	return nullptr;
}

IBindableListBinder* XMLElementBinder::getBindableListByName(
		const char* name) {
	for (IBindableListBinder*& ptr : bindableLists) {
		if (ptr->getName() == name) {
			return ptr;
		}
	}
	return nullptr;
}

IBasicTypeListBinder* XMLElementBinder::getBasicTypeListByName(
		const char* name) {
	for (IBasicTypeListBinder*& val : basicTypeLists) {
		if (val->getName() == name) {
			return val;
		}
	}

	return nullptr;
}

void XMLElementBinder::printToStream(std::ostream& out, bool withHeader) {
	if (withHeader) {
		out << XML_FILE_HEADER << endl;
	}

	printToStream(this, out, "");
}

void XMLElementBinder::printVarElement(std::ostream& out,
		XMLValueVector& childValues,
		string& subIndent) {
	out << ">" << endl;
	for (XMLValueBinder*& val : childValues) {
		out << subIndent << "<" REV_ELEMENT_VAR " name=\"" << val->getName()
				<< "\" type=\""
				<< val->getType() << "\">" << val->getValueString()
				<< "</" REV_ELEMENT_VAR ">" << endl;
	}
}

void XMLElementBinder::printVarListElement(std::ostream& out,
		BasicTypeListBinderVector& basicTypeLists,
		string& subIndent) {
	for (IBasicTypeListBinder*& val : basicTypeLists) {
		out << subIndent << "<" REV_ELEMENT_VAR_LIST " name=\""
				<< val->getName()
				<< "\" type=\"" << val->getType() << "\">";

		int i = 0;
		const int size = val->getSize();

		BasicTypeListBinderIterator iter = val->begin();
		BasicTypeListBinderIterator endIter = val->end();
		for (; iter != endIter; ++iter) {
			out << iter.toString();
			if (i < size - 1) {
				out << REV_ELEMENT_SEPARATOR;
			}
			i++;
		}
		out << "</" REV_ELEMENT_VAR_LIST ">" << endl;
	}
}

void XMLElementBinder::printBindableListElement(std::ostream& out,
		BindableListBinderVector& bindableLists, string& subIndent) {
	string subSubIndent = subIndent + REV_XML_SUB_INDENT;
	for (IBindableListBinder*& ptr : bindableLists) {
		out << subIndent << "<" REV_ELEMENT_LIST " name=\"" << ptr->getName()
				<< "\">" << endl;

		BindableListBinderIterator iter = ptr->begin();
		BindableListBinderIterator endIter = ptr->end();

		for (; iter != endIter; ++iter) {
			IBindable* b = iter.getPointer();
			XMLElementBinder elem(REV_LIST_ELEMENT_NAME, b);
			printToStream(&elem, out, subSubIndent);
		}

		out << subIndent << "</" REV_ELEMENT_LIST ">" << endl;
	}
}

void XMLElementBinder::printToStream(XMLElementBinder* element,
		std::ostream& out, const std::string& indent) {
	if (!element || !element->isBound()) {
		return;
	}
	out << indent << "<" REV_ELEMENT_OBJECT " ";
	if (strcmp(REV_LIST_ELEMENT_NAME, element->getName()) != 0) {
		out << "name=\"" << element->getName() << "\" ";
	}
	out << "type=\"" << element->getType() << "\"";

	string subIndent = indent + REV_XML_SUB_INDENT;

	XMLValueVector& childValues = element->getChildValues();
	bool hasChildren = false;
	if (!childValues.empty()) {
		hasChildren = true;
		printVarElement(out, childValues, subIndent);
	}

	BasicTypeListBinderVector& basicTypeLists = element->getBasicTypeLists();
	if (!basicTypeLists.empty()) {
		if (!hasChildren) {
			out << ">" << endl;
			hasChildren = true;
		}
		printVarListElement(out, basicTypeLists, subIndent);
	}

	XMLElementVector& childElements = element->getChildElements();
	if (!childElements.empty()) {
		if (!hasChildren) {
			out << ">" << endl;
			hasChildren = true;
		}
		// printBindableElement:
		for (XMLElementBinder*& val : childElements) {
			printToStream(val, out, subIndent);
		}
	}

	BindableListBinderVector& bindableLists = element->getBindableLists();
	if (!bindableLists.empty()) {
		if (!hasChildren) {
			out << ">" << endl;
			hasChildren = true;
		}
		printBindableListElement(out, bindableLists, subIndent);
	}

	if (!hasChildren) {
		out << "/>" << endl;
	} else {
		out << indent << "</" REV_ELEMENT_OBJECT ">" << endl;
	}
}
