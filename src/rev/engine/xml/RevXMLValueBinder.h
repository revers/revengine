/*
 * RevXMLValueBinder.h
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#ifndef REVXMLVALUEBINDER_H_
#define REVXMLVALUEBINDER_H_

#include <string>
#include <rev/engine/binding/RevTypeUtil.h>

namespace rev {

    //--------------------------------------------------------------------------
    // Private namespace begin
    //==========================================================================
    namespace prv {

        class XMLValueAbstractBinderBase {
        public:
            virtual void mapStringToValue(const std::string& s) = 0;
            virtual std::string mapValueToString() = 0;
            virtual void* getValuePtr() = 0;
            virtual const char* getType() = 0;

            virtual ~XMLValueAbstractBinderBase() {
            }
        };

        template<typename T>
        class XMLValueAbstractBinder: public XMLValueAbstractBinderBase {
        public:
            T* valuePtr;

            XMLValueAbstractBinder(T* valuePtr_) :
                    valuePtr(valuePtr_) {
            }

            void mapStringToValue(const std::string& s) override {
                *valuePtr = rev::TypeUtil<T>::stringTo(s);
            }

            std::string mapValueToString() override {
                return rev::TypeUtil<T>::toString(*valuePtr);
            }

            void* getValuePtr() override {
                return (void*) valuePtr;
            }

            const char* getType() override {
                return TypeUtil<T>::getName();
            }
        };
    }
    //--------------------------------------------------------------------------
    // Private namespace end
    //==========================================================================

    //--------------------------------------------------------------------------
    // XMLValueBinder
    //==========================================================================

    class XMLValueBinder {
        prv::XMLValueAbstractBinderBase* valuePtr;
        std::string name;
        public:
        template<typename T>
        XMLValueBinder(const char* name, T* valuePtr_) :
                name(name), valuePtr(
                        new prv::XMLValueAbstractBinder<T>(valuePtr_)) {
        }

        ~XMLValueBinder() {
            delete valuePtr;
        }

        void* getValuePtr() {
            return valuePtr->getValuePtr();
        }

        const char* getType() {
            return valuePtr->getType();
        }

        std::string getValueString() {
            return valuePtr->mapValueToString();
        }

        void mapStringToValue(const std::string& s) {
            valuePtr->mapStringToValue(s);
        }

        const std::string& getName() {
            return name;
        }
    };

} /* namespace rev */
#endif /* REVXMLVALUEBINDER_H_ */
