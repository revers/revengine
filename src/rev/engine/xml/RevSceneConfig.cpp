/*
 * RevSceneConfig.cpp
 *
 *  Created on: 07-02-2013
 *      Author: Revers
 */

#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/RevRenderer3D.h>
#include "RevSceneConfig.h"

using namespace rev;

IMPLEMENT_BINDABLE(SceneConfig)

SceneConfig::SceneConfig(const GameObjectVect& gameObjects) {
	for (GameObject* go : gameObjects) {
		if (go->isSaveable()) {
			this->gameObjects.push_back(go);
		}
	}
	mainScript = ScriptingManager::ref().getMainScriptFile();
	backgroundColor = Renderer3D::ref().getBackgroundColor();
}

void SceneConfig::bind(IBinder& binder) {
	binder.bindSimpleRO(configFormatVersion);
	binder.bindSimple(mainScript);
	binder.bindSimple(backgroundColor);
	binder.bindList(this, "gameObjects", &SceneConfig::gameObjects, true, true, true);
}
