/*
 * RevXMLConfigManager.h
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#ifndef REVXMLCONFIGMANAGER_H_
#define REVXMLCONFIGMANAGER_H_

#include <string>
#include <map>
//#include <rev/engine/binding/RevIBasicTypeFactory.h>
#include "RevXMLElementBinder.h"
#include "RevSceneConfig.h"

namespace pugi {
    class xml_node;
}

namespace rev {

    class XMLConfigManager {
        static XMLConfigManager* xmlConfigManager;

        // typedef std::map<std::string, IBasicTypeFactory*> FactoryMap;
        //  FactoryMap factoriesMap;

        XMLConfigManager();
        XMLConfigManager(const XMLConfigManager&) = delete;

    public:
        ~XMLConfigManager() {
        }

        static void createSingleton();
        static void destroySingleton();

        static XMLConfigManager& ref() {
            return *xmlConfigManager;
        }

        static XMLConfigManager* getInstance() {
            return xmlConfigManager;
        }

        /**
         * Saves scene configuration to a file.
         * IMPORTANT: If the given file exists it will be overwritten!
         *
         * @param path [in] path to configuration file
         * @param errOut [out] in case of failure, error message.
         *
         * @return true if succeed, false otherwise.
         */
        bool saveSceneConfiguration(const std::string& path, std::string& errOut);

        /**
         * Loads scene configuration from a file.
         *
         * @param path [in] path to configuration file
         * @param errOut [out] in case of failure, error message.
         *
         * @return true if succeed, false otherwise.
         */
        bool loadSceneConfiguration(const std::string& path, std::string& errOut);

    private:
        //   void addTypeFactory(IBasicTypeFactory* factory);
        IBindable* getBindable(const char* name, std::string& errOut);
        bool load(XMLElementBinder* element, pugi::xml_node* node, std::string& errOut);
        SceneConfig* loadFromStream(std::istream& in, std::string& errOut);
        SceneConfig* loadFromFile(const char* filename, std::string& errOut);

        bool loadVar(XMLElementBinder* element, pugi::xml_node& node, std::string& errOut);
        bool loadVarList(XMLElementBinder* element, pugi::xml_node& node, std::string& errOut);
        bool loadObjectList(XMLElementBinder* element, pugi::xml_node& node, std::string& errOut);
        bool loadObject(XMLElementBinder* element, pugi::xml_node& node, std::string& errOut);
    };

} /* namespace rev */
#endif /* REVXMLCONFIGMANAGER_H_ */
