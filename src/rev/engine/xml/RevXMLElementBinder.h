/*
 * RevXMLElementBinder.h
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#ifndef REVXMLELEMENTBINDER
#define REVXMLELEMENTBINDER

#include <cstring>
#include <string>
#include <ostream>
#include <vector>
#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/binding/RevIBinder.h>
#include "RevXMLValueBinder.h"

// indent in xml file:
#define REV_XML_SUB_INDENT "  "
#define REV_ELEMENT_OBJECT "object"
#define REV_ELEMENT_VAR "var"
#define REV_ELEMENT_VAR_LIST "var-list"
#define REV_ELEMENT_LIST "list"
#define REV_LIST_ELEMENT_NAME "__LIST_ELEMENT"

namespace rev {
	class XMLElementBinder;

	typedef std::vector<XMLValueBinder*> XMLValueVector;
	typedef std::vector<XMLElementBinder*> XMLElementVector;
	typedef std::vector<IBindableListBinder*> BindableListBinderVector;
	typedef std::vector<IBasicTypeListBinder*> BasicTypeListBinderVector;

	class XMLElementBinder: public IBinder {
		IBindable* baseBindable = nullptr;
		IBindable** baseBindablePtr = nullptr;
		XMLElementVector childElements;
		XMLValueVector childValues;
		BindableListBinderVector bindableLists;
		BasicTypeListBinderVector basicTypeLists;
		std::string name;

		XMLElementBinder(const char* name, IBindable** bindablePtr);

	public:
		XMLElementBinder(const char* name, IBindable* bindable);

		virtual ~XMLElementBinder();

		const char* getName() {
			return name.c_str();
		}

		const char* getType() {
			return baseBindable->getBindableName();
		}

		XMLValueBinder* getValueByName(const char* name);
		XMLElementBinder* getElementByName(const char* name);
		IBindableListBinder* getBindableListByName(const char* name);
		IBasicTypeListBinder* getBasicTypeListByName(const char* name);

		IBindable* getBindable() {
			return baseBindable;
		}

		bool isBound() {
			return baseBindable != nullptr;
		}

		XMLElementVector& getChildElements() {
			return childElements;
		}

		XMLValueVector& getChildValues() {
			return childValues;
		}

		BindableListBinderVector& getBindableLists() {
			return bindableLists;
		}

		BasicTypeListBinderVector& getBasicTypeLists() {
			return basicTypeLists;
		}

		void printToStream(std::ostream& out = std::cout, bool withHeader = true);
		void print(bool withHeader = true) {
			printToStream(std::cout, withHeader);
		}

		void bindObject(IBindable* bindable) override;
		void unbindObject(IBindable* bindable) override;

	private:
		void printToStream(XMLElementBinder* element, std::ostream& out,
				const std::string& indent);

		void clearAll();

		template<typename T>
		void addNewValue(IBindable* bindable, const char* name,
				MemberWrapper<T> wrapper, bool editable, bool visible, bool expanded) {
			T* val = wrapper.getMemberPointer(bindable);
			childValues.push_back(new XMLValueBinder(name, val));
		}

	private:
		friend class XMLConfigManager;
		/**
		 * For use in XMLConfigManager::loadObject().
		 */
		void rebind(IBindable* b);

		// Inherited methods:

		/**
		 * IBindable
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<IBindable*> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * BasicTypeListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name,
				BasicTypeListMemberWrapper wrapper, bool editable,
				bool visible, bool expanded) override;

		/**
		 * BindableListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name, BindableListMemberWrapper wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * filepath (std::string)
		 */
		virtual void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter,
				bool editable, bool visible) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					true);
		}

		/**
		 * int
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<int> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * float
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<float> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * double
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<double> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * bool
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<bool> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * std::string
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<std::string> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * vec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * mat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * quat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::quat> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dvec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat2> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dmat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * dquat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dquat> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color3> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

		/**
		 * color4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color4> wrapper,
				bool editable, bool visible, bool expanded) override {
			addNewValue(bindable, name, std::move(wrapper), editable, visible,
					expanded);
		}

	private:
		void printVarElement(std::ostream& out, XMLValueVector& childValues,
				std::string& subIndent);

		void printVarListElement(std::ostream& out,
				BasicTypeListBinderVector& basicTypeLists,
				std::string& subIndent);

		void printBindableListElement(std::ostream& out,
				BindableListBinderVector& bindableLists,
				std::string& subIndent);
	};

}
/* namespace rev */
#endif /* REVXMLELEMENTBINDER */
