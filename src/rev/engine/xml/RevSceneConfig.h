/*
 * RevSceneConfig.h
 *
 *  Created on: 07-02-2013
 *      Author: Revers
 */

#ifndef REVSCENECONFIG_H_
#define REVSCENECONFIG_H_

#include <vector>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

typedef std::vector<GameObject*> GameObjectVect;

class SceneConfig: public IBindable {
	double configFormatVersion = 0.9;
	rev::color3 backgroundColor;
	std::string mainScript;
	GameObjectVect gameObjects;

	SceneConfig() {
	}

public:
	DECLARE_BINDABLE(SceneConfig)

	SceneConfig(const GameObjectVect& gameObjects);

	virtual ~SceneConfig() {
	}

	const std::string& getMainScript() {
		return mainScript;
	}
	GameObjectVect& getGameObjectVect() {
		return gameObjects;
	}
	const rev::color3& getBackgroundColor() const {
		return backgroundColor;
	}
	double getConfigFormatVersion() const {
		return configFormatVersion;
	}

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVSCENECONFIG_H_ */
