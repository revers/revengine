/*
 * RevXMLConfigManager.cpp
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#include <fstream>
#include <sstream>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/binding/RevBindableFactory.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/xml/RevSceneConfig.h>
#include <rev/engine/xml/RevXMLElementBinder.h>
#include <rev/engine/xml/pugixml/pugixml.hpp>
#include <rev/engine/util/RevCharSetStringTokenizer.h>
#include <rev/engine/events/RevEventManager.h>

#include "RevXMLConfigManager.h"

using namespace rev;
using namespace std;

rev::XMLConfigManager* rev::XMLConfigManager::xmlConfigManager = nullptr;

void XMLConfigManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!xmlConfigManager);
	xmlConfigManager = new XMLConfigManager();
	REV_TRACE_FUNCTION_OUT;
}

void XMLConfigManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(xmlConfigManager);
	delete xmlConfigManager;
	xmlConfigManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

XMLConfigManager::XMLConfigManager() {
}

#define ERROR_MSG(msg) \
        ostringstream oss; \
        oss << msg; \
        errOut = oss.str(); \
        REV_ERROR_MSG(errOut);

inline bool strEquals(const char* a, const char* b) {
	return strcmp(a, b) == 0;
}

inline bool strNotEquals(const char* a, const char* b) {
	return strcmp(a, b) != 0;
}

bool isEmpty(const char* value) {
	if (value == nullptr || value[0] == (char) 0) {
		return true;
	}

	return StringUtil::removeWhitespaces(value) == "";
}

IBindable* XMLConfigManager::getBindable(const char* name,
		std::string& errOut) {
	IBindable* bindable = BindableFactory::ref().get(name);

	if (!bindable) {
		ERROR_MSG("Cannot find bindable object: '" << name << "'!");
		return nullptr;
	}

	return bindable;
}

string getAttribute(pugi::xml_node& node, const char* name,
		string& errOut) {
	pugi::xml_attribute attr = node.attribute(name);
	if (attr.empty()) {
		ERROR_MSG("Element: '" << node.name()
				<< "' has no '" << name << "' attribute!");
		return "";
	}
	const char* attrVal = attr.value();

	if (isEmpty(attrVal)) {
		ERROR_MSG("Attribute '" << name << "' of element '"
				<< node.name() << "' cannot be empty!");
		return "";
	}

	return attrVal;
}

bool XMLConfigManager::loadVar(XMLElementBinder* element, pugi::xml_node& node,
		std::string& errOut) {

	string attr = getAttribute(node, "name", errOut);
	if (attr == "") {
		return false;
	}

	XMLValueBinder* xmlVal = element->getValueByName(attr.c_str());
	if (!xmlVal) {
		ERROR_MSG("There is no '" << attr
				<< "' variable in '" << element->getType()
				<< "' bindable (or it is not bindend)!");
		return false;
	}

	const char* value = node.text().get();

	if (isEmpty(value) && strcmp("string", xmlVal->getType()) != 0) {
		ERROR_MSG("Element '" << node.name() << "' with name '"
				<< attr << "' cannot be empty!!");
		return false;
	}

	xmlVal->mapStringToValue(value);

	return true;
}

bool XMLConfigManager::loadVarList(XMLElementBinder* element,
		pugi::xml_node& node, std::string& errOut) {
	string attr = getAttribute(node, "name", errOut);
	if (attr == "") {
		return false;
	}

	IBasicTypeListBinder* typeList = element->getBasicTypeListByName(attr.c_str());

	if (!typeList) {
		ERROR_MSG("There is no list '" << attr << "' in '"
				<< element->getType()
				<< "' bindable (or it is not bindend)!");
		return false;
	}

	typeList->clear();

	const char* value = node.text().get();
	if (isEmpty(value)) {
		ERROR_MSG("Element '" << node.name() << "' with name '"
				<< attr << "' cannot be empty!!");
		return false;
	}

	CharSetStringTokenizer tokenizer(StringUtil::removeWhitespaces(value),
			REV_ELEMENT_SEPARATOR);
	string str;
	while ((str = tokenizer.next()) != "") {
		typeList->pushBackFromString(str);
	}

	return true;
}

bool XMLConfigManager::loadObjectList(XMLElementBinder* element,
		pugi::xml_node& node, std::string& errOut) {

	string nameAttr = getAttribute(node, "name", errOut);
	if (nameAttr == "") {
		return false;
	}

	IBindableListBinder* listBinder = element->getBindableListByName(
			nameAttr.c_str());

	if (!listBinder) {
		ERROR_MSG("There is no '" << nameAttr
				<< "' list in '" << element->getType()
				<< "' bindable (or it is not bindend)!");
		return false;
	}

	// clearing list:
	auto begin = listBinder->begin();
	auto end = listBinder->end();
	for (; begin != end; ++begin) {
		IBindable* b = begin.getPointer();
		if (b && !b->isSingleton()) {
			delete b;
		}
	}
	listBinder->clear();

	for (pugi::xml_node& listChild : node.children()) {
		const char* listChildName = listChild.name();
		if (strNotEquals(listChildName, REV_ELEMENT_OBJECT)) {
			ERROR_MSG("Element '" REV_ELEMENT_LIST "' can have only '"
					<< REV_ELEMENT_OBJECT "' sub-elements! (not '"
					<< listChildName << "')");
			return false;
		}

		string typeAttr = getAttribute(listChild, "type", errOut);
		if (typeAttr == "") {
			return false;
		}

		IBindable* bindable = getBindable(typeAttr.c_str(), errOut);
		if (!bindable) {
			return false;
		}

		XMLElementBinder elem(REV_LIST_ELEMENT_NAME, bindable);
		if (!load(&elem, &listChild, errOut)) {
			BindableFactory::ref().free(bindable);
			return false;
		}

		if (!elem.getBindable()->initAfterDeserialization()) {
			ERROR_MSG("init() of '"
					<< elem.getBindable()->getBindableName()
					<< "' bindable failed!");
			BindableFactory::ref().free(bindable);
			return false;
		}

		listBinder->pushBack(bindable);
	}

	return true;
}

bool XMLConfigManager::loadObject(XMLElementBinder* element,
		pugi::xml_node& node, std::string& errOut) {

	string nameAttr = getAttribute(node, "name", errOut);
	if (nameAttr == "") {
		return false;
	}

	XMLElementBinder* e = element->getElementByName(nameAttr.c_str());
	if (!e) {
		ERROR_MSG("There is no sub-element '" << nameAttr
				<< "' in '" << element->getType()
				<< "' bindable (or it is not bindend)!");
		return false;
	}

	IBindable* bindable = nullptr;
	bool needRebind = false;
	if (!e->isBound()) {
		string typeAttr = getAttribute(node, "type", errOut);
		if (typeAttr == "") {
			return false;
		}

		bindable = getBindable(typeAttr.c_str(), errOut);
		if (!bindable) {
			return false;
		}

		needRebind = true;
		e->bindObject(bindable);
	}

	if (!load(e, &node, errOut)) {
		BindableFactory::ref().free(bindable);
		return false;
	}

//    REV_INFO_MSG("init() of '"
//            << bindable->getBindableName() << "'...");
	if (!bindable->initAfterDeserialization()) {
		ERROR_MSG("init() of '"
				<< bindable->getBindableName() << "' bindable failed!");
		BindableFactory::ref().free(bindable);
		return false;
	}

	if (needRebind) {
		e->rebind(bindable);
	}

	return true;
}

bool XMLConfigManager::load(XMLElementBinder* element,
		pugi::xml_node* node, std::string& errOut) {
	if (strNotEquals(node->name(), REV_ELEMENT_OBJECT)) {
		ERROR_MSG("Wrong element '" << node->name()
				<< "'. Expected: '" REV_ELEMENT_OBJECT "'!");
		return false;
	}

	string type = getAttribute(*node, "type", errOut);

	if (type != element->getType()) {
		ERROR_MSG("Wrong type attribute: '" << type << "'. Expected: '"
				<< element->getType() << "'!");
		return false;
	}

	for (pugi::xml_node& child : node->children()) {
		const char* childName = child.name();

		if (strEquals(REV_ELEMENT_VAR, childName)) {
			// <var name="">
			if (!loadVar(element, child, errOut)) {
				return false;
			}
		} else if (strEquals(REV_ELEMENT_VAR_LIST, childName)) {
			// <var-list name="">
			if (!loadVarList(element, child, errOut)) {
				return false;
			}
		} else if (strEquals(REV_ELEMENT_LIST, childName)) {
			// object list
			// <list name="">
			if (!loadObjectList(element, child, errOut)) {
				return false;
			}
		} else if (strEquals(REV_ELEMENT_OBJECT, childName)) {
			// <object name="">
			if (!loadObject(element, child, errOut)) {
				return false;
			}
		} else {
			ERROR_MSG("Unknown element '" << childName << "'!");
			return false;
		}
	}

	return true;
}

SceneConfig* XMLConfigManager::loadFromFile(const char* filename,
		std::string& errOut) {
	ifstream in(filename);
	if (!in) {
		ERROR_MSG("FILE NOT FOUND '" << filename << "'!!");
		return nullptr;
	}

	return loadFromStream(in, errOut);
}

SceneConfig* XMLConfigManager::loadFromStream(std::istream& in,
		std::string& errOut) {
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load(in);
	if (!result) {
		ERROR_MSG("Parsing error: " << result.description());
		return nullptr;
	}
	pugi::xml_node root = doc.first_child();

	string rootType = getAttribute(root, "type", errOut);
	if (rootType != "SceneConfig") {
		ERROR_MSG("Wrong root element's type: '" << rootType
				<< "'! Expected: 'SceneConfig'");
		return nullptr;
	}

	IBindable* rootBindable = getBindable(rootType.c_str(), errOut);
	if (!rootBindable) {
		return nullptr;
	}

	SceneConfig* sceneConfig = dynamic_cast<SceneConfig*>(rootBindable);
	revAssert(sceneConfig != nullptr);
	XMLElementBinder element("sceneConfig", sceneConfig);

	if (!load(&element, &root, errOut)) {
		BindableFactory::ref().free(rootBindable);
		return nullptr;
	}

	return sceneConfig;
}

bool XMLConfigManager::saveSceneConfiguration(const std::string& path,
		std::string& errOut) {
	ofstream configFile(path);
	if (!configFile.is_open()) {
		ERROR_MSG("Cannot create file '" << path << "'!");
		return false;
	}

	REV_DEBUG_MSG("Saving file '" << path << "'...");

	SceneConfig sceneConfig(Engine::ref().getGameObjects());
	XMLElementBinder element("sceneConfig", &sceneConfig);
	element.printToStream(configFile, true);

	configFile.close();

	return true;
}

bool XMLConfigManager::loadSceneConfiguration(const std::string& path,
		std::string& errOut) {
	REV_DEBUG_MSG("Loading file '" << path << "'...");

	SceneConfig* config = loadFromFile(path.c_str(), errOut);
	if (!config) {
		return false;
	}

	EventManager::ref().fireEngineEvent("XMLConfigManager::loadSceneConfiguration",
			[config] () -> bool {
				Engine::ref().removeAllObjectsAndEvents();
				GameObjectVect& goList = config->getGameObjectVect();
				for (GameObject*& go : goList) {
					rev::Engine::ref().addGameObject(go);
				}
				ScriptingManager::ref().setMainScriptFile(config->getMainScript());
				Renderer3D::ref().setBackgroundColor(config->getBackgroundColor());

				if (!config->isSingleton()) {
					delete config;
				}
				EventManager::ref().fireSceneLoadedEvent();
				return true;
			});

	return true;
}
