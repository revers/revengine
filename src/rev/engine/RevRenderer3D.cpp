/*
 * RevRenderer3D.cpp
 *
 *  Created on: 27-11-2012
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevDelete.hpp>
#include <rev/gl/RevGLAssert.h>
#include <rev/gl/RevGLUtil.h>

#include "RevGameObject.h"
#include "RevRenderer3D.h"

#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevErrorStream.h>

#include <rev/engine/shadow/RevShadowManager.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/collision/RevCollisionManager.h>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/collision/RevCollisionSphere.h>
#include <rev/engine/collision/RevCollisionBox.h>
#include <rev/engine/collision/RevCollisionPlane.h>
#include <rev/engine/effects/RevNormalVisualizerEffect.h>
#include <rev/engine/drawables/RevVBOSphereVN.h>
#include <rev/engine/drawables/RevVBOFrustumVN.h>
#include <rev/engine/drawables/RevVBOCuboidVNT.h>
#include <rev/engine/drawables/RevVBOPlaneVNT.h>
#include <rev/engine/effects/RevDebugEffect.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

bool rev::Renderer3D::glewInited = false;
rev::Renderer3D* rev::Renderer3D::renderer = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(Renderer3D)

Renderer3D::Renderer3D() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		totalRenderedPolygons[i] = 0;
		totalRenderedObjects[i] = 0;
		widths[i] = 0;
		heights[i] = 0;
	}
}

Renderer3D::~Renderer3D() {
	DEL(normalVisualizerEffect);
	DEL(sphereComponent);
	DEL(frustumComponent);
	DEL(cuboidComponent);
	DEL(planeComponent);
}

void Renderer3D::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!renderer);
	renderer = new Renderer3D();
	REV_TRACE_FUNCTION_OUT;
}

void Renderer3D::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(renderer);
	delete renderer;
	renderer = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

inline std::string strWithInt(const char* str, int i) {
	std::ostringstream oss;
	oss << str << i;
	return oss.str();
}

void Renderer3D::bind(IBinder& binder) {
	binder.bindSimple(wireframeMode);
	binder.bindSimple(cullFaceMode);
	binder.bindSimple(debugNormals);
	binder.bindSimple(drawDebugElements);
	binder.bindSimple(frustumCullingEnabled);
	binder.bindSimple(drawObjects);
	binder.bindSimple(drawAllBoundingSpheres);
	binder.bindSimple(drawAllCollisionBoundingSpheres);
	binder.bindSimple(drawAllCollisionGeometry);
	binder.bindSimple(backgroundColor);

	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		binder.bindInfo(this, strWithInt("width_", i).c_str(), widths[i]);
		binder.bindInfo(this, strWithInt("height_", i).c_str(), heights[i]);
		binder.bindInfo(this, strWithInt("totalRenderedPolygons_", i).c_str(),
				totalRenderedPolygons[i]);
		binder.bindInfo(this, strWithInt("totalRenderedObjects_", i).c_str(),
				totalRenderedObjects[i]);
	}
}

bool Renderer3D::initContext(int ctx) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glAssert;

	/*
	 * http://stackoverflow.com/questions/13213227/gl-pointcoord-has-incorrect-uninitialized-value
	 * This is not available for OpenGL >= 3.0, but it is
	 * necessary as bug fix for gl_PointCoord always set to (0.0, 0.0) in other than
	 * main contexts.
	 */
	glEnable(GL_POINT_SPRITE);
	// this will generate error message, but it is ok.
	glAlwaysAssertNoExit;

	glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1.0);
	return init();
}

bool Renderer3D::init() {
	REV_TRACE_FUNCTION;
	//revAssert(!inited);
	if (inited) {
		return true;
	}

	if (!glewInited) {
		GLenum err = glewInit();
		if (GLEW_OK != err) {
			REV_ERROR_MSG("Error initializing GLEW: "
					<< glewGetErrorString(err));
			return false;
		}
		glAlwaysAssertNoExit;
		glewInited = true;

		normalVisualizerEffect = new rev::NormalVisualizerEffect();
		glAssert;

		REV_INFO_MSG(rev::GLUtil::getGLInfo(false));
	}

	if (!sphereComponent) {
		debugEffect = new rev::DebugEffect();
		debugEffect->setColor(color3(1.0f, 1.0f, 1.0f));
		IVBODrawable* drawable = new rev::VBOSphereVN(1.0, 12);
		sphereComponent = new rev::Simple3DComponent(debugEffect,
				false, drawable, true);
		sphereComponent->setWireframe(true);
	}

	if (!cuboidComponent) {
		IVBODrawable* drawable = new rev::VBOCuboidVNT(1.0f, 1.0f, 1.0f);
		cuboidComponent = new rev::Simple3DComponent(debugEffect, false, drawable, true);
		cuboidComponent->setWireframe(true);
	}
	if (!planeComponent) {
		IVBODrawable* drawable = new rev::VBOPlaneVNT(300, 300, 30, 30);
		planeComponent = new rev::Simple3DComponent(debugEffect, false, drawable, true);
		planeComponent->setWireframe(true);
	}

	if (!frustumComponent) {
		DebugEffect* effect = new rev::DebugEffect();
		effect->setColor(color3(1.0f, 0.8f, 0.2f));

		ICamera* camera = CameraManager::ref().getCamera(
				GLContextManager::MAIN_CONTEXT);

		IVBODrawable* drawable = new rev::VBOFrustumVN(
				camera->getViewFrustum());
		frustumComponent = new rev::Simple3DComponent(effect,
				true, drawable, true);
	}

	if (!ShadowManager::ref().isInited() && !ShadowManager::ref().init()) {
		REV_ERROR_MSG("ShadowManager::ref().init() FAILED!");
		return false;
	}

	inited = true;

	return true;
}

void Renderer3D::update() {
	for (IVisual3DComponent*& c : components) {
		c->update();
	}
}

void Renderer3D::resize(int width, int height) {
	REV_TRACE_MSG("width: " << width << "; height: " << height);

	if (width <= 0 || height <= 0) {
		return;
	}
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();

	widths[contextIndex] = width;
	heights[contextIndex] = height;

	glViewport(0, 0, width, height);

	CameraManager::ref().resize(width, height, contextIndex);
	bool pickerSucc = getPicker().init(width, height);
	revAssert(pickerSucc);

	if (contextIndex == GLContextManager::MAIN_CONTEXT) {
		bool lightSucc = LightingManager::ref().initDeferredRenderer(width, height);
		revAssert(lightSucc);

		bool ssaoSucc = LightingManager::ref().initSSAORenderer(width, height);
		revAssert(ssaoSucc);
	}
}

int Renderer3D::getGameObjectDrawId(GameObject* go) {
	int index = 0;
	for (IVisual3DComponent*& c : components) {
		if (c->getParent() == go) {
			return index;
		}
		index++;
	}
	return -1;
}

void Renderer3D::renderBoundingSphere(const rev::BoundingSphere* bs,
		const rev::ICamera& camera, rev::IVisual3DComponent& c) {

	debugEffect->setColor(color3(1.0f, 1.0f, 1.0f));
	mat4 m = c.getParent()->getModelMatrix() * glm::translate(bs->c)
			* glm::scale(vec3(bs->r));
	sphereComponent->applyEffect(m, camera.getViewMatrix(),
			camera.getProjectionMatrix(), camera);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	sphereComponent->render();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer3D::standardRender() {
	setDefaultClearColor();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	const rev::PixelInfo& pixelInfo = getPicker().getPixelInfo();

	ICamera* camera = CameraManager::ref().getCamera();
	unsigned int index = 0;

	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();

	totalRenderedPolygons[contextIndex] = 0;
	totalRenderedObjects[contextIndex] = 0;

	for (IVisual3DComponent*& c : components) {
		if (!c->isDebugElement() && exclusiveGameObject && c->getParent() != exclusiveGameObject) {
			index++;
			continue;
		}
		if (!c->getParent()->isVisible()
				|| (c->isDebugElement() && !drawDebugElements)
				|| (frustumCullingEnabled && c->culled)) {
			index++;
			continue;
		}

		totalRenderedPolygons[contextIndex] += c->getPrimitiveCount();
		++totalRenderedObjects[contextIndex];

		if (wireframeMode || c->isWireframe()) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		} else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		if (cullFaceMode || c->isCullFace()) {
			glEnable(GL_CULL_FACE);
		} else {
			glDisable(GL_CULL_FACE);
		}

		if (!pixelInfo.isEmpty() && pixelInfo.getDrawId() == index) {
			getPicker().renderPickedComponent(*c, *camera, drawDebugElements);
		} else {
			c->applyEffect(c->getParent()->getModelMatrix(),
					camera->getViewMatrix(), camera->getProjectionMatrix(),
					*camera);
			c->render();
		}

		if (drawDebugElements && drawAllBoundingSpheres) {
			renderBoundingSphere(c->getBoundingSphere(), *camera, *c);
		}

		index++;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if REV_ENGINE_MAX_CONTEXTS > 1
	drawCameraFrustum(camera, contextIndex);

	if (drawShadowSourceFrustum) {
		drawShadowFrustum(camera, contextIndex);
	}
#endif
}

void Renderer3D::drawCameraFrustum(ICamera* camera, int contextIndex) {
	// Draw camera frustum:
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		if (contextIndex != i && CameraManager::ref().isDrawCameraFrustum(i)) {
			DebugEffect* effect = (DebugEffect*) frustumComponent->getEffect();
			effect->setColor(color3(1.0f, 0.8f, 0.2f));
			VBOFrustumVN* frustum = (VBOFrustumVN*) frustumComponent->getDrawable();
			frustum->set(CameraManager::ref().getCamera(i)->getViewFrustum());

			frustumComponent->applyEffect(MathHelper::IDENTITY_MATRIX,
					camera->getViewMatrix(), camera->getProjectionMatrix(),
					*camera);
			frustumComponent->render();
		}
	}
}

void Renderer3D::drawShadowFrustum(ICamera* camera, int contextIndex) {
	if (contextIndex == GLContextManager::MAIN_CONTEXT) {
		return;
	}
	DebugEffect* effect = (DebugEffect*) frustumComponent->getEffect();
	effect->setColor(color3(1.0f, 0.05f, 0.09f));
	VBOFrustumVN* frustum = (VBOFrustumVN*) frustumComponent->getDrawable();

	frustum->set(ViewFrustum::fromProjectionMatrix(ShadowManager::ref().getViewProjMatrix()));

	frustumComponent->applyEffect(MathHelper::IDENTITY_MATRIX,
			camera->getViewMatrix(), camera->getProjectionMatrix(),
			*camera);
	frustumComponent->render();
}

void Renderer3D::pickingRender() {
	GLfloat bgColor[4];
	glGetFloatv(GL_COLOR_CLEAR_VALUE, bgColor);

	glClearColor(0.0, 0.0, 0.0, 0.0);
	ColorCodingPicker& picker = getPicker();
	picker.enable();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ICamera* camera = CameraManager::ref().getCamera();
	int componentCounter = 0;
	for (IVisual3DComponent*& c : components) {
		if (exclusiveGameObject && c->getParent() != exclusiveGameObject) {
			componentCounter++;
			continue;
		}
		if (!c->getParent()->isVisible() || !c->isPickable()
				|| (frustumCullingEnabled && c->culled)) {
			componentCounter++;
			continue;
		}

		picker.setIndices(c->getParent()->getId(), componentCounter);
		picker.applyPickingEffect(c->getParent()->getModelMatrix(),
				camera->getViewMatrix(), camera->getProjectionMatrix(),
				*camera);

		c->render();
		componentCounter++;
	}
	picker.disable();

	glClearColor(bgColor[0], bgColor[1], bgColor[2], bgColor[3]);
}
void Renderer3D::normalsRender() {
	ICamera* camera = CameraManager::ref().getCamera();
	for (IVisual3DComponent*& c : components) {
		if (exclusiveGameObject && c->getParent() != exclusiveGameObject) {
			continue;
		}
		if (!c->getParent()->isVisible()
				|| c->getRenderPrimitiveType() != RenderPrimitiveType::TRIANGLES
				|| c->isDebugElement()
				|| (frustumCullingEnabled && c->culled)) {
			continue;
		}

		normalVisualizerEffect->apply(c->getParent()->getModelMatrix(),
				camera->getViewMatrix(), camera->getProjectionMatrix(),
				*camera);

		c->render();
	}
}

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

void Renderer3D::frustumCullingTest() {
	ICamera* camera = CameraManager::ref().getCamera();
	for (IVisual3DComponent*& c : components) {
		if (!c->getParent()->isVisible()) {
			continue;
		}

		ViewFrustum& frustum = camera->getViewFrustum();
		BoundingSphere sphere(*c->getBoundingSphere());

		sphere.c += c->getParent()->getPosition();
		const glm::vec3& s = c->getParent()->getScaling();
		float scaleFactor = MAX(s.x, MAX(s.y, s.z));
		sphere.r *= scaleFactor;

		if (frustum.isVisible(sphere)) {
			c->culled = false;
		} else {
			c->culled = true;
		}
	}
}

void Renderer3D::drawCollisionSphere(CollisionSphere* sphere, CollisionComponent* comp) {
	ICamera* camera = CameraManager::ref().getCamera();

	float radius = sphere->getRadius();
	mat4 m = sphere->getTransform() * glm::scale(vec3(radius));

	sphereComponent->applyEffect(m, camera->getViewMatrix(),
			camera->getProjectionMatrix(), *camera);

	sphereComponent->render();
}

void Renderer3D::drawCollisionBox(CollisionBox* box, CollisionComponent* comp) {
	ICamera* camera = CameraManager::ref().getCamera();
	const glm::vec3& halfSize = box->getHalfSize();
	glm::mat4 scale = glm::scale(halfSize.x * 2, halfSize.y * 2, halfSize.z * 2);

	GameObject* parent = comp->getParent();
	mat4 m = box->getTransform() * scale;

	cuboidComponent->applyEffect(m, camera->getViewMatrix(),
			camera->getProjectionMatrix(), *camera);

	cuboidComponent->render();
}

void Renderer3D::drawCollisionPlane(CollisionPlane* plane, CollisionComponent* comp) {
	ICamera* camera = CameraManager::ref().getCamera();
	const glm::vec3& normal = plane->getNormal();

	vec3 distance(0, -plane->getDistance(), 0);
	mat4 rotation = MathHelper::getRotationFromAxisY(normal);
	rotation *= glm::translate(distance);

	GameObject* parent = comp->getParent();
	mat4 m = plane->getTransform() * rotation;

	planeComponent->applyEffect(m, camera->getViewMatrix(),
			camera->getProjectionMatrix(), *camera);

	planeComponent->render();
}

void Renderer3D::renderCollisionGeometry() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	debugEffect->setColor(color3(0, 1, 1));

	for (auto& c : CollisionManager::ref().getComponents()) {
		if (!drawAllCollisionGeometry && !c->isDrawGeometry()) {
			continue;
		}
		CollisionGeometry* geometry = c->getGeometry();
		if (!geometry) {
			continue;
		}
		switch (geometry->getType()) {
		case GeometryType::SPHERE: {
			drawCollisionSphere(static_cast<CollisionSphere*>(geometry), c);
			break;
		}
		case GeometryType::BOX: {
			drawCollisionBox(static_cast<CollisionBox*>(geometry), c);
			break;
		}
		case GeometryType::PLANE: {
			drawCollisionPlane(static_cast<CollisionPlane*>(geometry), c);
			break;
		}
		default: {
			revAssert(false);
			break;
		}
		}
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer3D::renderCollisionBoundingSpheres() {
	ICamera* camera = CameraManager::ref().getCamera();

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	debugEffect->setColor(color3(1.0f, 0.0f, 0.0f));

	for (auto& c : CollisionManager::ref().getComponents()) {
		if (!drawAllCollisionBoundingSpheres && !c->isDrawBoundingSphere()) {
			continue;
		}
		const BoundingSphere& bs = c->getBoundingSphere();
		mat4 m = glm::translate(bs.c + c->getParent()->getPosition())
				* glm::scale(vec3(bs.r));
		sphereComponent->applyEffect(m, camera->getViewMatrix(),
				camera->getProjectionMatrix(), *camera);

		sphereComponent->render();
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

bool Renderer3D::isProperForRender(IVisual3DComponent* c) {
	if (!c->isDebugElement()
			&& exclusiveGameObject
			&& c->getParent() != exclusiveGameObject) {
		return false;
	}
	if (!c->getParent()->isVisible()
			|| c->isDebugElement()
			|| (frustumCullingEnabled && c->culled)) {
		return false;
	}

	return true;
}

void Renderer3D::shadowRender() {
	if (!ShadowManager::ref().hasShadowSource()) {
		return;
	}

	ShadowManager::ref().renderDepthMap();

	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	glViewport(0, 0, widths[contextIndex], heights[contextIndex]);
}

void Renderer3D::deferredLightingRender() {
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	if (contextIndex != GLContextManager::MAIN_CONTEXT) {
		return;
	}

	LightingManager::ref().renderDeferred();
}

void Renderer3D::ssaoRender() {
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	if (contextIndex != GLContextManager::MAIN_CONTEXT) {
		return;
	}

	LightingManager::ref().renderSSAO();
}

bool Renderer3D::contextSensitiveRender() {
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	if (contextIndex == GLContextManager::SECOND_CONTEXT) {
		if (deferredLightingEnabled && drawDeferredDepthMap) {
			DeferredRenderer& deferredRenderer = LightingManager::ref().getDeferredRenderer();
			deferredRenderer.renderQuadWithDepthTexture();
			return true;
		} else if (shadowsEnabled && drawShadowDepthMap
				&& ShadowManager::ref().hasShadowSource()) {
			glViewport(0, 0, widths[contextIndex], heights[contextIndex]);
			ShadowManager::ref().renderQuadWithDepthTexture();
			return true;
		} else if (ssaoEnabled && drawSSAOMap) {
			SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
			renderer.renderQuadWithSSAOTexture();
			return true;
		}
	} else if (contextIndex == GLContextManager::THIRD_CONTEXT && deferredLightingEnabled) {
		if (drawDeferredNormalMap) {
			DeferredRenderer& deferredRenderer = LightingManager::ref().getDeferredRenderer();
			deferredRenderer.renderQuadWithNormalTexture();
			return true;
		} else if (drawDeferredLightMap) {
			DeferredRenderer& deferredRenderer = LightingManager::ref().getDeferredRenderer();
			deferredRenderer.renderQuadWithLightTexture();
			return true;
		}
	}

	return false;
}

void Renderer3D::render() {
	MutexGuard guard(&mutex);

	if (contextSensitiveRender()) {
		return;
	}

	if (frustumCullingEnabled) {
		frustumCullingTest();
	}
	if (shadowsEnabled) {
		shadowRender();
		glFlush();
		glFinish();
	}

	if (deferredLightingEnabled) {
		deferredLightingRender();
//		glFlush();
//		glFinish();
	}

	if (ssaoEnabled) {
		ssaoRender();
//		glFlush();
//		glFinish();
	}

	pickingRender();

	if (drawObjects) {
		standardRender();
	} else {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	if (drawDebugElements) {
		renderCollisionBoundingSpheres();
		renderCollisionGeometry();
		if (debugNormals) {
			normalsRender();
		}
	}
	if (drawDeferredLightSpheres) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		DeferredRenderer& deferredRenderer = LightingManager::ref().getDeferredRenderer();
		deferredRenderer.drawLightSpheres();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

