#ifndef REVVBOPLANEVT_H
#define REVVBOPLANEVT_H

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>

namespace rev {

	class VBOPlaneVNT: public IVBODrawable {
	private:
		enum {
			HANDLE_VERTEX,
			HANDLE_TEXTURE,
			HANDLE_ELEMENTS,
			HANDLE_SIZE
		};
		enum {
			ATTRIB_VERTEX,
			ATTRIB_NORMAL,
			ATTRIB_TEXTURE
		};

		unsigned int handle[HANDLE_SIZE];
		int faces;

		float xSize = 10;
		float zSize = 10;
		int xTessellation = 10;
		int zTessellation = 10;

		/**
		 * Plane normal.
		 */
		glm::vec3 normal = glm::vec3(0, 1, 0);

		/**
		 * Distance to the origin.
		 */
		float distance = 0.0;

		// info:
		int infoVertices = 0;
		int infoTriangles = 0;

		VBOPlaneVNT();

	public:
		DECLARE_BINDABLE(VBOPlaneVNT)

		VBOPlaneVNT(float xSize, float zSize, int xTessellation, int zTessellation);
		VBOPlaneVNT(float xSize, float zSize, int xTessellation, int zTessellation,
				const glm::vec3& normal, float distance);
		~VBOPlaneVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

	private:
		bool generate();
		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

}
#endif // REVVBOPLANEVT_H
