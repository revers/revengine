/*
 * RevVBOTriangularCageVN.cpp
 *
 *  Created on: 03-12-2012
 *      Author: Revers
 */

#include "RevVBOTriangularCageVN.h"
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOTriangularCageVN)

VBOTriangularCageVN::VBOTriangularCageVN() {
    glGenBuffers(2, handle);
    glAssert;
}

VBOTriangularCageVN::VBOTriangularCageVN(float length, int tesselation) {
    create(length, length, length, tesselation, tesselation, tesselation);
}

VBOTriangularCageVN::VBOTriangularCageVN(float xLength, float yLength,
        float zLength, int xTesselation, int yTesselation, int zTesselation) {
    create(xLength, yLength, zLength, xTesselation, yTesselation, zTesselation);
}

VBOTriangularCageVN::~VBOTriangularCageVN() {
    glDeleteBuffers(2, handle);
}

void VBOTriangularCageVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawArrays(GL_TRIANGLES, 0, vertices);
    glAssert;
    glBindVertexArray(0);
}

void VBOTriangularCageVN::create(float xLength, float yLength, float zLength,
        int xTesselation, int yTesselation, int zTesselation) {
    this->xLength = xLength;
    this->yLength = yLength;
    this->zLength = zLength;
    this->xTesselation = xTesselation;
    this->yTesselation = yTesselation;
    this->zTesselation = zTesselation;

    glGenBuffers(2, handle);

    bool succ = generate();
    revAssert(succ);
}

void VBOTriangularCageVN::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Vertex normal
    //-------------------------------------------------------------
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#define ADD_VEC(arr, idx, x, y, z) arr[idx++] = x; arr[idx++] = y; arr[idx++] = z;

bool VBOTriangularCageVN::generate() {
    float xBlockSize = xLength / xTesselation;
    float yBlockSize = yLength / yTesselation;
    float zBlockSize = zLength / zTesselation;
    float xMin = -(xLength / 2.0f);
    float xMax = xLength / 2.0f;
    float yMin = -(yLength / 2.0f);
    float yMax = yLength / 2.0f;
    float zMin = -(zLength / 2.0f);
    float zMax = zLength / 2.0f;

    vertices = (xTesselation * yTesselation * 2
            + xTesselation * zTesselation * 2
            + yTesselation * zTesselation * 2) * 2 * 3;
    infoTriangles = vertices / 3;
    infoVertices = vertices;

    arraySize = vertices * 3;

    float* v = new float[arraySize];
    float* n = new float[arraySize];
    int vIdx = 0;
    int nIdx = 0;

    // Front wall:
    for (int i = 0; i < yTesselation; i++) {
        for (int j = 0; j < xTesselation; j++) {
            ADD_VEC(v, vIdx, xMin + xBlockSize * j,
                    yMin + yBlockSize * i + yBlockSize, zMax);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i + yBlockSize, zMax);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i, zMax);

            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i, zMax);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMin + yBlockSize * i, zMax);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j,
                    yMin + yBlockSize * i + yBlockSize, zMax);

            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, -1.0f);
        }
    }

    // Back wall:
    for (int i = 0; i < yTesselation; i++) {
        for (int j = 0; j < xTesselation; j++) {
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i, zMin);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i + yBlockSize, zMin);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j,
                    yMin + yBlockSize * i + yBlockSize, zMin);

            ADD_VEC(v, vIdx, xMin + xBlockSize * j,
                    yMin + yBlockSize * i + yBlockSize, zMin);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMin + yBlockSize * i, zMin);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize,
                    yMin + yBlockSize * i, zMin);

            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
            ADD_VEC(n, nIdx, 0.0f, 0.0f, 1.0f);
        }
    }

    // Top wall:
    for (int i = 0; i < zTesselation; i++) {
        for (int j = 0; j < xTesselation; j++) {
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMax,
                    zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMax,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMax,
                    zMin + zBlockSize * i + zBlockSize);

            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMax,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMax, zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMax,
                    zMin + zBlockSize * i);

            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, -1.0f, 0.0f);
        }
    }

    // Bottom wall
    for (int i = 0; i < zTesselation; i++) {
        for (int j = 0; j < xTesselation; j++) {
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMin,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMin,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMin,
                    zMin + zBlockSize * i);

            ADD_VEC(v, vIdx, xMin + xBlockSize * j + xBlockSize, yMin,
                    zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMin, zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin + xBlockSize * j, yMin,
                    zMin + zBlockSize * i + zBlockSize);

            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
            ADD_VEC(n, nIdx, 0.0f, 1.0f, 0.0f);
        }
    }

    // Right wall:
    for (int i = 0; i < zTesselation; i++) {
        for (int j = 0; j < yTesselation; j++) {
            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i);

            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j, zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMax, yMin + yBlockSize * j,
                    zMin + zBlockSize * i + zBlockSize);

            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, -1.0f, 0.0f, 0.0f);
        }
    }

    // Left wall:
    for (int i = 0; i < zTesselation; i++) {
        for (int j = 0; j < yTesselation; j++) {
            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j,
                    zMin + zBlockSize * i + zBlockSize);

            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j,
                    zMin + zBlockSize * i + zBlockSize);
            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j, zMin + zBlockSize * i);
            ADD_VEC(v, vIdx, xMin, yMin + yBlockSize * j + yBlockSize,
                    zMin + zBlockSize * i);

            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
            ADD_VEC(n, nIdx, 1.0f, 0.0f, 0.0f);
        }
    }

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, vertices);
    // boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, vertices);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, arraySize * sizeof(float), v, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, arraySize * sizeof(float), n, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    delete[] v;
    delete[] n;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOTriangularCageVN::bind(IBinder& binder) {
    binder.bindSimple(xLength);
    binder.bindSimple(yLength);
    binder.bindSimple(zLength);
    binder.bindSimple(xTesselation);
    binder.bindSimple(yTesselation);
    binder.bindSimple(zTesselation);
    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOTriangularCageVN::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOTriangularCageVN::initAfterDeserialization() {
    return generate();
}
