/*
 * RevVBOFrustumVN.h
 *
 *  Created on: 29-01-2013
 *      Author: Revers
 */

#ifndef REVVBOFRUSTUMVN_H_
#define REVVBOFRUSTUMVN_H_

#include <glm/glm.hpp>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/collision/primitives/RevViewFrustum.h>

namespace rev {

	class VBOFrustumVN: public IVBODrawable {
		enum HandlesEnum {
			HANDLE_VERTEX,
			HANDLE_NORMAL,
			HANDLE_ELEMENT,
			HANDLE_SIZE
		};
		unsigned int handle[HANDLE_SIZE];

		int indices = 36;
		int infoVertices = 24;
		int infoTriangles = 12;

		glm::vec3 nearTopLeft = glm::vec3(1, 1, -1);
		glm::vec3 nearTopRight = glm::vec3(-1, 1, -1);
		glm::vec3 nearBottomLeft = glm::vec3(1, -1, -1);
		glm::vec3 nearBottomRight = glm::vec3(-1, -1, -1);
		glm::vec3 farTopLeft = glm::vec3(2, 2, 2);
		glm::vec3 farTopRight = glm::vec3(-2, 2, 2);
		glm::vec3 farBottomLeft = glm::vec3(2, -2, 2);
		glm::vec3 farBottomRight = glm::vec3(-2, -2, 2);

		VBOFrustumVN();

	public:
		DECLARE_BINDABLE(VBOFrustumVN)

		VBOFrustumVN(const glm::mat4& projectionMatrix);

		VBOFrustumVN(const ViewFrustum& frustum);

		VBOFrustumVN(const glm::vec3& nearTopLeft, const glm::vec3& nearTopRight,
				const glm::vec3& nearBottomLeft, const glm::vec3& nearBottomRight,
				const glm::vec3& farTopLeft, const glm::vec3& farTopRight,
				const glm::vec3& farBottomLeft, const glm::vec3& farBottomRight);

		virtual ~VBOFrustumVN();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

		void set(const ViewFrustum& frustum);

	private:
		bool generate();
		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

}
/* namespace rev */

#endif /* REVVBOFRUSTUMVN_H_ */
