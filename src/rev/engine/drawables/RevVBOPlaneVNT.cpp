#include <cstdio>
#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/transform.hpp>
#include "RevVBOPlaneVNT.h"
#include <rev/gl/RevGLAssert.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOPlaneVNT)

VBOPlaneVNT::VBOPlaneVNT() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOPlaneVNT::VBOPlaneVNT(float xSize, float zSize, int xTessellation,
		int zTessellation) :
		xSize(xSize), zSize(zSize), xTessellation(xTessellation), zTessellation(
				zTessellation) {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOPlaneVNT::VBOPlaneVNT(float xSize, float zSize, int xTessellation,
		int zTessellation, const glm::vec3& normal, float distance) :
		xSize(xSize), zSize(zSize), xTessellation(xTessellation), zTessellation(
				zTessellation), normal(normal), distance(distance) {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOPlaneVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glVertexAttrib3fv(ATTRIB_NORMAL, (float*) &normal); // Constant normal for all verts
	glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

VBOPlaneVNT::~VBOPlaneVNT() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOPlaneVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(ATTRIB_VERTEX); // Vertex position
	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glVertexAttribPointer((GLuint) ATTRIB_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(ATTRIB_TEXTURE); // Texture coords
	//-------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENTS]);
	//-------------------------------------------------------------
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOPlaneVNT::generate() {
	faces = xTessellation * zTessellation;
	int verts = (xTessellation + 1) * (zTessellation + 1);

	infoVertices = verts;
	infoTriangles = 2 * faces;

	float* v = new float[3 * verts];
	float* tex = new float[2 * verts];
	unsigned int* el = new unsigned int[6 * xTessellation * zTessellation];

	mat4 m = MathHelper::getRotationFromAxisY(normal);
	m *= glm::translate(vec3(0, -distance, 0));

	float x2 = xSize / 2.0f;
	float z2 = zSize / 2.0f;
	float iFactor = (float) zSize / zTessellation;
	float jFactor = (float) xSize / xTessellation;
	float texi = 1.0f / zTessellation;
	float texj = 1.0f / xTessellation;
	float x, z;
	int vidx = 0, tidx = 0;
	for (int i = 0; i <= zTessellation; i++) {
		z = iFactor * i - z2;
		for (int j = 0; j <= xTessellation; j++) {
			x = jFactor * j - x2;

			vec3 vertex = mul(m, vec3(x, 0, z));
			v[vidx] = vertex.x;
			v[vidx + 1] = vertex.y;
			v[vidx + 2] = vertex.z;

			vidx += 3;
			tex[tidx] = j * texj;
			tex[tidx + 1] = i * texi;
			tidx += 2;
		}
	}

	unsigned int rowStart, nextRowStart;
	int idx = 0;
	for (int i = 0; i < zTessellation; i++) {
		rowStart = i * (xTessellation + 1);
		nextRowStart = (i + 1) * (xTessellation + 1);
		for (int j = 0; j < xTessellation; j++) {
			el[idx] = rowStart + j;
			el[idx + 1] = nextRowStart + j;
			el[idx + 2] = nextRowStart + j + 1;
			el[idx + 3] = rowStart + j;
			el[idx + 4] = nextRowStart + j + 1;
			el[idx + 5] = rowStart + j + 1;
			idx += 6;
		}
	}

	boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, verts);
	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, verts);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	//-----------------------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER,
			3 * (xTessellation + 1) * (zTessellation + 1) * sizeof(float), v,
			GL_STATIC_DRAW);
	//-----------------------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glBufferData(GL_ARRAY_BUFFER,
			2 * (xTessellation + 1) * (zTessellation + 1) * sizeof(float),
			tex, GL_STATIC_DRAW);
	//-----------------------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENTS]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			6 * xTessellation * zTessellation * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	//-----------------------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] tex;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOPlaneVNT::bind(IBinder& binder) {
	binder.bindSimple(normal);
	binder.bindSimple(distance);
	binder.bindSimple(xSize);
	binder.bindSimple(zSize);
	binder.bindSimple(xTessellation);
	binder.bindSimple(zTessellation);
	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOPlaneVNT::bindableValueChanged(void* ptr) {
	if (ptr == &normal) {
		normal = glm::normalize(normal);
	}
	generate();
}

bool VBOPlaneVNT::initAfterDeserialization() {
	return generate();
}
