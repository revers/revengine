/*
 * RevVBOArrowVN.cpp
 *
 *  Created on: 19-12-2012
 *      Author: Revers
 */

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/transform.hpp>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>

#include "RevVBOArrowVN.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOArrowVN)

VBOArrowVN::VBOArrowVN() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOArrowVN::VBOArrowVN(float r, float h, int tessellation) :
		radius(r), height(h), tessellation(tessellation) {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOArrowVN::createCap(
		int tessellation,
		float height,
		float radius,
		const glm::vec3& normal,
		glm::vec3* v,
		glm::vec3* n,
		unsigned int* el,
		int& vIndex,
		int& nIndex,
		int& elIndex,
		const glm::mat4& rot) {
	// Create cap indices.
	for (int i = 0; i < tessellation - 2; i++) {
		if (normal.y > 0) {
			el[elIndex++] = vIndex;
			el[elIndex++] = vIndex + (i + 1) % tessellation;
			el[elIndex++] = vIndex + (i + 2) % tessellation;
		} else {
			el[elIndex++] = vIndex;
			el[elIndex++] = vIndex + (i + 2) % tessellation;
			el[elIndex++] = vIndex + (i + 1) % tessellation;
		}
	}

	// Create cap vertices.
	for (int i = 0; i < tessellation; i++) {
		vec3 position = getCircleVector(i, tessellation) * radius +
				normal * height;

		v[vIndex++] = mul(rot, position);
		n[nIndex++] = mul(rot, normal);
	}
}

glm::vec3 VBOArrowVN::getCircleVector(int i, int tessellation) {
	float angle = i * TWO_PI / tessellation;

	float dx = (float) cos(angle);
	float dz = (float) sin(angle);

	return vec3(dx, 0, dz);
}

VBOArrowVN::~VBOArrowVN() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOArrowVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

void VBOArrowVN::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	//-----------------------------------------------------------------
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

bool VBOArrowVN::generate() {
	mat4 rot;
	switch ((Orientation) orientation) {
	case Orientation::X: {
		rot = glm::rotate(-90.0f, 0.0f, 0.0f, 1.0f);
		break;
	}
	case Orientation::Y: {
		// do nothing - default state.
		break;
	}
	case Orientation::Z: {
		rot = glm::rotate(90.0f, 1.0f, 0.0f, 0.0f);
		break;
	}
	}

	int size = 3 * (tessellation + 1) + 3 * tessellation;
	float arrowHeight = height * 0.5;
	float cylinderHeight = height * 0.5;
	float cylinderRadius = radius * 0.6;

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];
	indices = 3 * tessellation + 3 * (tessellation - 2)
			+ tessellation * 6 + (tessellation - 2) * 3;

	infoVertices = size;
	infoTriangles = indices / 3;

	unsigned int* el = new unsigned int[indices];
	int vIndex = 0;
	int nIndex = 0;
	int elIndex = 0;

	float step = TWO_PI / tessellation;
	for (int i = 0; i < (tessellation + 1); i++) {
		float theta = step * i;
		float sinTheta = sin(theta);
		float cosTheta = cos(theta);

		v[vIndex++] = mul(rot, vec3(radius * cosTheta, 0, radius * sinTheta));
		v[vIndex++] = mul(rot, vec3(0, arrowHeight, 0));

		vec3 slopeTangent = v[vIndex - 1] - v[vIndex - 2];
		vec3 dTheta(-radius * sinTheta, 0, radius * cosTheta);
		vec3 norm = mul(rot, glm::normalize(glm::cross(slopeTangent, dTheta)));

		n[nIndex++] = norm;
		n[nIndex++] = norm;
	}

	for (int i = 0; i < tessellation; i++) {
		el[elIndex++] = i * 2;
		el[elIndex++] = i * 2 + 1;
		el[elIndex++] = (i + 1) * 2;
	}

	createCap(tessellation, 0, radius, MathHelper::NEGATIVE_Y, v, n, el,
			vIndex, nIndex, elIndex, rot);

	int coneSize = vIndex;
	//----------------------------------------------------------------

	// Create a ring of triangles around the outside of the cylinder.
	for (int i = 0; i < tessellation; i++) {
		vec3 normal = getCircleVector(i, tessellation);

		v[vIndex++] = mul(rot, normal * cylinderRadius);
		v[vIndex++] = mul(rot, normal * cylinderRadius + MathHelper::NEGATIVE_Y * cylinderHeight);
		normal = mul(rot, normal);
		n[nIndex++] = normal;
		n[nIndex++] = normal;

		el[elIndex++] = coneSize + i * 2;
		el[elIndex++] = coneSize + i * 2 + 1;
		el[elIndex++] = coneSize + (i * 2 + 2) % (tessellation * 2);

		el[elIndex++] = coneSize + i * 2 + 1;
		el[elIndex++] = coneSize + (i * 2 + 3) % (tessellation * 2);
		el[elIndex++] = coneSize + (i * 2 + 2) % (tessellation * 2);
	}

	// Create flat triangle fan caps to seal the top and bottom.
	createCap(tessellation, cylinderHeight, cylinderRadius,
			MathHelper::NEGATIVE_Y, v, n, el, vIndex, nIndex, elIndex, rot);
	//================================================================

	// boundingSphere = rev::Sphere::smallBall(v, size);
	boundingSphere = rev::Sphere::miniBall(v, size);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOArrowVN::bind(IBinder& binder) {
	binder.bindSimple(radius);
	binder.bindSimple(height);
	binder.bindSimple(tessellation);
	binder.bindSimple(orientation);
	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOArrowVN::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOArrowVN::initAfterDeserialization() {
	return generate();
}
