/*
 * RevVBOMultiCubicSurfaceVNT.h
 *
 *  Created on: 23-04-2013
 *      Author: Revers
 */

#ifndef REVVBOMULTICUBICSURFACEVNT_H_
#define REVVBOMULTICUBICSURFACEVNT_H_

#include <vector>
#include <glm/glm.hpp>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/parametric/RevISurfacePatch.h>
#include <rev/engine/parametric/RevMultiCubicSurface.h>

#include "RevIVBODrawable.h"

namespace rev {

	class VBOMultiCubicSurfaceVNT: public IVBODrawable, public ISurfacePatch {
	private:
		typedef ISurfacePatch::scalar_t scalar_t;
		typedef ISurfacePatch::vec3_t vec3_t;

		static const int HANDLE_SIZE = 4;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_TEXTURE = 2;
		static const int HANDLE_ELEMENT = 3;
		unsigned int handle[HANDLE_SIZE];
		int faces;

		rev::MultiCubicSurface surface;

		std::vector<glm::vec3>* pointsPtr = &surface.points;
		int* mPtr = &surface.m;
		int* nPtr = &surface.n;
		int* pointShiftPtr = &surface.pointShift;
		scalar_t* pointDistancePtr = &surface.pointDistance;

		int uTessellation = 10;
		int wTessellation = 10;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOMultiCubicSurfaceVNT();

	public:
		DECLARE_BINDABLE(VBOMultiCubicSurfaceVNT)

		VBOMultiCubicSurfaceVNT(int m, int n, int uTessellation = 10, int wTessellation = 10);

		/**
		 * IMPORTANT: VBOMultiCubicSurfaceVNT makes local copy of points.
		 */
		VBOMultiCubicSurfaceVNT(const glm::vec3* points, int m, int n,
				int uTessellation, int wTessellation);

		~VBOMultiCubicSurfaceVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

		int getUPointsCount() const override {
			return surface.m;
		}
		int getVPointsCount() const override {
			return surface.n;
		}
		glm::vec3& getPoint(int u, int v) override;

		void recalculateSurface() override {
			generate();
		}

	private:
		bool generate();

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOMULTICUBICSURFACEVNT_H_ */
