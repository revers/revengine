#ifndef REVVBOTEAPOTVNT_H
#define REVVBOTEAPOTVNT_H

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

    class VBOTeapotVNT: public IVBODrawable {
    private:
        unsigned int handle[4];
        unsigned int faces;
        int grid = 20;

        // info:
        int infoVertices = 0;
        int infoTriangles = 0;

        VBOTeapotVNT();

    public:
        DECLARE_BINDABLE(VBOTeapotVNT)

        VBOTeapotVNT(int grid);
        ~VBOTeapotVNT();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

    private:
        bool generate();

        void generatePatches(float * v, float * n, float *tc, unsigned int* el,
                int grid);
        void buildPatchReflect(int patchNum,
                float *B, float *dB,
                float *v, float *n, float *, unsigned int *el,
                int &index, int &elIndex, int &, int grid,
                bool reflectX, bool reflectY);
        void buildPatch(glm::vec3 patch[][4],
                float *B, float *dB,
                float *v, float *n, float *, unsigned int *el,
                int &index, int &elIndex, int &, int grid, glm::mat3 reflect,
                bool invertNormal);
        void getPatch(int patchNum, glm::vec3 patch[][4], bool reverseV);

        void computeBasisFunctions(float * B, float * dB, int grid);
        glm::vec3 evaluate(int gridU, int gridV, float *B, glm::vec3 patch[][4]);
        glm::vec3 evaluateNormal(int gridU, int gridV, float *B, float *dB,
                glm::vec3 patch[][4]);
        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

}
#endif // REVVBOTEAPOTVNT_H
