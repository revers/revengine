/*
 * RevVBOPointSpheresVC.h
 *
 *  Created on: 11-04-2013
 *      Author: Revers
 */

#ifndef REVVBOPOINTSPHERESVC_H_
#define REVVBOPOINTSPHERESVC_H_

#include <vector>
#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>

namespace rev {

	class VBOPointSpheresVC: public IVBODrawable {
	public:
		typedef std::vector<glm::vec3> PointsVector;

	private:
		enum {
			ATTRIBUTE_VERTEX = 0,
			ATTRIBUTE_COLOR
		};
		static const int HANDLE_SIZE = 1;
		unsigned int handle[HANDLE_SIZE];

		PointsVector pointsVector;
		rev::color3 color = rev::color3(1, 0, 0);

		int infoPoints = 0;

	public:
		DECLARE_BINDABLE(VBOPointSpheresVC)

		VBOPointSpheresVC();

		VBOPointSpheresVC(int pointsCount, const rev::color3& color = rev::color3(1, 0, 0));

		/**
		 * Argument points will be COPIED to VBOPointSpheresVC.
		 */
		VBOPointSpheresVC(glm::vec3* points, int pointsLength,
				const rev::color3& color = rev::color3(1, 0, 0));
		~VBOPointSpheresVC();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoPoints;
		}
		RenderPrimitiveType getRenderPrimitiveType() const override {
			return RenderPrimitiveType::POINTS;
		}

		/**
		 * Argument points will be COPIED to VBOPointSpheresVC.
		 */
		void setPoints(glm::vec3* points, int pointsLength);

		void setColor(const rev::color3& color) {
			this->color = color;
		}
		void reinit() {
			generate();
		}
		PointsVector& getPointsVector() {
			return pointsVector;
		}
		void setPoint(int index, const glm::vec3& p);
		int getPointsCount() {
			return pointsVector.size();
		}

	private:
		bool generate();
		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOPOINTSPHERESVC_H_ */
