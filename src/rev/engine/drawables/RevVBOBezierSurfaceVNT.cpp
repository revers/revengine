/*
 * RevVBOBezierSurfaceVNT.cpp
 *
 *  Created on: 21-04-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevDelete.hpp>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/parametric/RevBezierSurface.h>
#include <rev/common/RevAssert.h>

#include "RevVBOBezierSurfaceVNT.h"

using namespace rev;

IMPLEMENT_BINDABLE(VBOBezierSurfaceVNT)

VBOBezierSurfaceVNT::VBOBezierSurfaceVNT() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

glm::vec3& VBOBezierSurfaceVNT::getPoint(int u, int v) {
	revAssert(n * u + v < m * n);
	return points[n * u + v];
}

VBOBezierSurfaceVNT::VBOBezierSurfaceVNT(const glm::vec3* points, int m, int n) {
	create(points, m, n, 10, 10);
}

VBOBezierSurfaceVNT::VBOBezierSurfaceVNT(const glm::vec3* points, int m, int n,
		int uTessellation, int wTessellation) {
	create(points, m, n, uTessellation, wTessellation);
}

void VBOBezierSurfaceVNT::create(const glm::vec3* points, int m, int n,
		int uTessellation, int wTessellation) {
	this->m = m;
	this->n = n;
	this->uTessellation = uTessellation;
	this->wTessellation = wTessellation;

	for (int i = 0; i < m * n; i++) {
		this->points.push_back(points[i]);
	}

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOBezierSurfaceVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glAssert;
	glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

VBOBezierSurfaceVNT::~VBOBezierSurfaceVNT() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOBezierSurfaceVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glVertexAttribPointer((GLuint) HANDLE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_TEXTURE); // Texture coords
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

bool VBOBezierSurfaceVNT::generate() {
	if (this->m < 2 || this->n < 2) {
		addDefaultPoints();
	}

	using namespace glm;

	faces = uTessellation * wTessellation;

	int size = (uTessellation + 1) * (wTessellation + 1);

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];
	vec2* tex = new vec2[size];
	int elementSize = 6 * uTessellation * wTessellation;

	infoVertices = size;
	infoTriangles = elementSize / 3;

	unsigned int* el = new unsigned int[elementSize];

	rev::BezierSurfacef surface(points.data(), this->m, this->n);

	float iFactor = 1.0f / wTessellation;
	float jFactor = 1.0f / uTessellation;
	int index = 0;
	for (int i = 0; i <= wTessellation; i++) {
		float w = iFactor * i;
		for (int j = 0; j <= uTessellation; j++) {
			float u = jFactor * j;
			v[index] = surface.eval(u, w);
			n[index] = surface.normal(u, w);
			tex[index] = vec2((float) u, (float) w);
			index++;
		}
	}

	boundingSphere = rev::Sphere::smallBall(v, size);
	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, size);

	unsigned int rowStart, nextRowStart;
	int idx = 0;
	for (int i = 0; i < wTessellation; i++) {
		rowStart = i * (uTessellation + 1);
		nextRowStart = (i + 1) * (uTessellation + 1);
		for (int j = 0; j < uTessellation; j++) {
			el[idx] = rowStart + j;
			el[idx + 1] = nextRowStart + j;
			el[idx + 2] = nextRowStart + j + 1;
			el[idx + 3] = rowStart + j;
			el[idx + 4] = nextRowStart + j + 1;
			el[idx + 5] = rowStart + j + 1;
			idx += 6;
		}
	}

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glBufferData(GL_ARRAY_BUFFER, 2 * size * sizeof(float),
			tex, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementSize * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] tex;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOBezierSurfaceVNT::bind(IBinder& binder) {
	binder.bindSimple(uTessellation);
	binder.bindSimple(wTessellation);
	binder.bindSimple(m);
	binder.bindSimple(n);
	binder.bindList(this, "points", &VBOBezierSurfaceVNT::points, true, true, false);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOBezierSurfaceVNT::bindableValueChanged(void* ptr) {
	if (ptr == &m) {
		addDefaultPoints();
	} else if (ptr == &n) {
		addDefaultPoints();
	}

	generate();
	if (pointChangedCallback) {
		pointChangedCallback();
	}
}

bool VBOBezierSurfaceVNT::initAfterDeserialization() {
	return generate();
}

void VBOBezierSurfaceVNT::addDefaultPoints() {
	if (m < 2) {
		m = 2;
	}
	if (n < 2) {
		n = 2;
	}

	points.clear();
	points.resize(m * n, glm::vec3(0));

	float uStep = 1.0f / ((float) m - 1.0f);
	float wStep = 1.0f / ((float) n - 1.0f);
	float uBegin = -0.5f;
	float wBegin = -0.5f;

	for (int u = 0; u < m; u++) {
		float uValue = uBegin + uStep * u;
		for (int w = 0; w < n; w++) {
			float wValue = wBegin + wStep * w;
			getPoint(u, w) = glm::vec3(uValue, 0.0, wValue);
		}
	}
}
