/*
 * RevVBOAxesVC.h
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */

#ifndef REVVBOAXESVC_H_
#define REVVBOAXESVC_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>

namespace rev {

	class VBOAxesVC: public IVBODrawable {
		static const int HANDLE_SIZE = 2;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_COLOR = 1;
		unsigned int handle[HANDLE_SIZE];

		float length = 1;
		rev::color3 xColor = rev::color3(1, 0, 0);
		rev::color3 yColor = rev::color3(0, 1, 0);;
		rev::color3 zColor = rev::color3(0, 0, 1);

		int infoVertices = 6;
		int infoLines = 3;

		VBOAxesVC();

	public:
		DECLARE_BINDABLE(VBOAxesVC)

		VBOAxesVC(float length);
		VBOAxesVC(float length, const rev::color3& xColor,
				const rev::color3& yColor, const rev::color3& zColor);
		~VBOAxesVC();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoLines;
		}
		RenderPrimitiveType getRenderPrimitiveType() const override {
			return RenderPrimitiveType::LINES;
		}

	private:
		bool generate();

		void init(float length, const rev::color3& xColor,
				const rev::color3& yColor, const rev::color3& zColor);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOAXESVC_H_ */
