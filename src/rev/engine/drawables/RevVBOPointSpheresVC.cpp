/*
 * RevVBOPointSpheresVC.cpp
 *
 *  Created on: 11-04-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOPointSpheresVC.h"
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOPointSpheresVC)

VBOPointSpheresVC::VBOPointSpheresVC() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOPointSpheresVC::VBOPointSpheresVC(glm::vec3* points, int pointsLength,
		const rev::color3& color) {
	pointsVector.clear();
	for (int i = 0; i < pointsLength; i++) {
		pointsVector.push_back(points[i]);
	}
	infoPoints = pointsVector.size();
	this->color = color;

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOPointSpheresVC::VBOPointSpheresVC(int pointsCount, const rev::color3& color) {
	pointsVector.clear();
	pointsVector.resize(pointsCount, glm::vec3(0));
	infoPoints = pointsVector.size();
	this->color = color;

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOPointSpheresVC::setPoints(glm::vec3* points, int pointsLength) {
	pointsVector.clear();
	for (int i = 0; i < pointsLength; i++) {
		pointsVector.push_back(points[i]);
	}
	infoPoints = pointsVector.size();
	this->color = color;
	bool succ = generate();
	revAssert(succ);
}

VBOPointSpheresVC::~VBOPointSpheresVC() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOPointSpheresVC::render() {
	if (pointsVector.empty()) {
		return;
	}
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glVertexAttrib3fv(ATTRIBUTE_COLOR, rev::value_ptr(color)); // constant color (attribute 1)
	glDrawArrays(GL_POINTS, 0, pointsVector.size());
	glBindVertexArray(0);
}

void VBOPointSpheresVC::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[ATTRIBUTE_VERTEX]);
	glVertexAttribPointer((GLuint) ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	//-------------------------------------------------------------
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOPointSpheresVC::generate() {
	infoPoints = pointsVector.size();
	boundingSphere = rev::Sphere::smallBall(pointsVector.data(), pointsVector.size());
	//boundingSphere = rev::Sphere::miniBall(points, pointsLength);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[ATTRIBUTE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, pointsVector.size() * sizeof(glm::vec3), pointsVector.data(),
			GL_STATIC_DRAW);

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOPointSpheresVC::setPoint(int index, const glm::vec3& p) {
	revAssert(index >= 0 && index < pointsVector.size());
	pointsVector[index] = p;
}

void VBOPointSpheresVC::bind(IBinder& binder) {
	binder.bindSimple(color);
	binder.bindList(this, "pointsVector", &VBOPointSpheresVC::pointsVector, true, true, false);
	binder.bindInfo(this, "Points", infoPoints);
}

void VBOPointSpheresVC::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOPointSpheresVC::initAfterDeserialization() {
	return generate();
}
