#ifndef REVVBOTORUSVNT_H
#define REVVBOTORUSVNT_H

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>

namespace rev {

class ParticleInstancingComponent;

class VBOTorusVNT: public IVBODrawable {
	friend class ParticleInstancingComponent;

private:
	unsigned int handle[4];
	int faces;
	int rings = 20;
	int sides = 20;
	float outerRadius = 2;
	float innerRadius = 1;

	int infoVertices;
	int infoTriangles;

	VBOTorusVNT();

public:
	DECLARE_BINDABLE(VBOTorusVNT)

	VBOTorusVNT(float outerRadius, float innerRadius,
			int nsides, int nrings);
	~VBOTorusVNT();

	void render() override;
	void bindableValueChanged(void* ptr) override;

	int getPrimitiveCount() override {
		return infoTriangles;
	}

private:
	bool generate();

	void generateVerts(float* verts, float* norms, float* tex,
			unsigned int* el, float outerRadius, float innerRadius);

	void initVAO(int contextIndex);

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;
};

}
#endif // REVVBOTORUSVNT_H
