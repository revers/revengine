/*
 * RevVBOAxesVC.cpp
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOAxesVC.h"
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOAxesVC)

VBOAxesVC::VBOAxesVC() {
    glGenBuffers(HANDLE_SIZE, handle);
    glAssert;
}

VBOAxesVC::VBOAxesVC(float length, const rev::color3& xColor,
        const rev::color3& yColor, const rev::color3& zColor) {
    init(length, xColor, yColor, zColor);
}

VBOAxesVC::VBOAxesVC(float length) {
    init(length, rev::color3(1.0, 0.0, 0.0), rev::color3(0.0, 1.0, 0.0),
            rev::color3(0.0, 0.0, 1.0));
}

void VBOAxesVC::init(float length, const rev::color3& xColor,
        const rev::color3& yColor, const rev::color3& zColor) {
    this->length = length;
    this->xColor = xColor;
    this->yColor = yColor;
    this->zColor = zColor;

    glGenBuffers(HANDLE_SIZE, handle);

    bool succ = generate();
    revAssert(succ);
}

VBOAxesVC::~VBOAxesVC() {
    glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOAxesVC::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawArrays(GL_LINES, 0, 6);
    glBindVertexArray(0);
}

void VBOAxesVC::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(HANDLE_VERTEX);
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_COLOR]);
    glVertexAttribPointer((GLuint) HANDLE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(HANDLE_COLOR);
    //-------------------------------------------------------------
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOAxesVC::generate() {
    float* vert = new float[18]; // vertex array
    float* col = new float[18]; // color array

    float centerX = 0.0f;
    float centerY = 0.0f;
    float centerZ = 0.0f;

    col[0] = xColor[0];
    col[1] = xColor[1];
    col[2] = xColor[2];

    col[3] = xColor[0];
    col[4] = xColor[1];
    col[5] = xColor[2];

    col[6] = yColor[0];
    col[7] = yColor[1];
    col[8] = yColor[2];

    col[9] = yColor[0];
    col[10] = yColor[1];
    col[11] = yColor[2];

    col[12] = zColor[0];
    col[13] = zColor[1];
    col[14] = zColor[2];

    col[15] = zColor[0];
    col[16] = zColor[1];
    col[17] = zColor[2];

    vert[0] = centerX;
    vert[1] = centerY;
    vert[2] = centerZ;

    vert[3] = centerX + length;
    vert[4] = centerY;
    vert[5] = centerZ;

    vert[6] = centerX;
    vert[7] = centerY;
    vert[8] = centerZ;

    vert[9] = centerX;
    vert[10] = centerY + length;
    vert[11] = centerZ;

    vert[12] = centerX;
    vert[13] = centerY;
    vert[14] = centerZ;

    vert[15] = centerX;
    vert[16] = centerY;
    vert[17] = centerZ + length;

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) vert, 18 / 3);
    // boundingSphere = rev::Sphere::miniBall((glm::vec3*) vert, 18 / 3);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), vert, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_COLOR]);
    glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), col, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    delete[] vert;
    delete[] col;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOAxesVC::bind(IBinder& binder) {
    binder.bindSimple(length);
    binder.bindSimple(xColor);
    binder.bindSimple(yColor);
    binder.bindSimple(zColor);

    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Lines", infoLines);

}

void VBOAxesVC::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOAxesVC::initAfterDeserialization() {
    return generate();
}
