#include <cstdio>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include "RevVBOTeapotVNT.h"
#include "RevTeapotData.h"
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/common/RevAssert.h>

using namespace glm;
using namespace rev;

IMPLEMENT_BINDABLE(VBOTeapotVNT)

VBOTeapotVNT::VBOTeapotVNT() {
    glGenBuffers(4, handle);
    glAssert;
}

VBOTeapotVNT::VBOTeapotVNT(int grid) :
        grid(grid) {
    glGenBuffers(4, handle);

    bool succ = generate();
    revAssert(succ);
}

void VBOTeapotVNT::generatePatches(float * v, float * n, float * tc,
        unsigned int* el, int grid) {
    float * B = new float[4 * (grid + 1)]; // Pre-computed Bernstein basis functions
    float * dB = new float[4 * (grid + 1)]; // Pre-computed derivitives of basis functions

    int idx = 0, elIndex = 0, tcIndex = 0;

    // Pre-compute the basis functions  (Bernstein polynomials)
    // and their derivatives
    computeBasisFunctions(B, dB, grid);

    // Build each patch
    // The rim
    buildPatchReflect(0, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    // The body
    buildPatchReflect(1, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    buildPatchReflect(2, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    // The lid
    buildPatchReflect(3, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    buildPatchReflect(4, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    // The bottom
    buildPatchReflect(5, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, true,
            true);
    // The handle
    buildPatchReflect(6, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, false,
            true);
    buildPatchReflect(7, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, false,
            true);
    // The spout
    buildPatchReflect(8, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, false,
            true);
    buildPatchReflect(9, B, dB, v, n, tc, el, idx, elIndex, tcIndex, grid, false,
            true);

    delete[] B;
    delete[] dB;
}

void VBOTeapotVNT::buildPatchReflect(int patchNum,
        float *B, float *dB,
        float *v, float *n,
        float *tc, unsigned int *el,
        int &index, int &elIndex, int &tcIndex, int grid,
        bool reflectX, bool reflectY) {
    vec3 patch[4][4];
    vec3 patchRevV[4][4];
    getPatch(patchNum, patch, false);
    getPatch(patchNum, patchRevV, true);

    // Patch without modification
    buildPatch(patch, B, dB, v, n, tc, el,
            index, elIndex, tcIndex, grid, mat3(1.0f), true);

    // Patch reflected in x
    if (reflectX) {
        buildPatch(patchRevV, B, dB, v, n, tc, el,
                index, elIndex, tcIndex, grid, mat3(vec3(-1.0f, 0.0f, 0.0f),
                        vec3(0.0f, 1.0f, 0.0f),
                        vec3(0.0f, 0.0f, 1.0f)), false);
    }

    // Patch reflected in y
    if (reflectY) {
        buildPatch(patchRevV, B, dB, v, n, tc, el,
                index, elIndex, tcIndex, grid, mat3(vec3(1.0f, 0.0f, 0.0f),
                        vec3(0.0f, -1.0f, 0.0f),
                        vec3(0.0f, 0.0f, 1.0f)), false);
    }

    // Patch reflected in x and y
    if (reflectX && reflectY) {
        buildPatch(patch, B, dB, v, n, tc, el,
                index, elIndex, tcIndex, grid, mat3(vec3(-1.0f, 0.0f, 0.0f),
                        vec3(0.0f, -1.0f, 0.0f),
                        vec3(0.0f, 0.0f, 1.0f)), true);
    }
}

void VBOTeapotVNT::buildPatch(vec3 patch[][4],
        float *B, float *dB,
        float *v, float *n, float *tc,
        unsigned int *el,
        int &index, int &elIndex, int &tcIndex, int grid, mat3 reflect,
        bool invertNormal) {
    int startIndex = index / 3;
    float tcFactor = 1.0f / grid;

    for (int i = 0; i <= grid; i++) {
        for (int j = 0; j <= grid; j++) {
            vec3 pt = reflect * evaluate(i, j, B, patch);
            vec3 norm = reflect * evaluateNormal(i, j, B, dB, patch);
            if (invertNormal)
                norm = -norm;

            v[index] = pt.x;
            v[index + 1] = pt.y;
            v[index + 2] = pt.z;

            n[index] = norm.x;
            n[index + 1] = norm.y;
            n[index + 2] = norm.z;

            tc[tcIndex] = i * tcFactor;
            tc[tcIndex + 1] = j * tcFactor;

            index += 3;
            tcIndex += 2;
        }
    }

    for (int i = 0; i < grid; i++) {
        int iStart = i * (grid + 1) + startIndex;
        int nextiStart = (i + 1) * (grid + 1) + startIndex;
        for (int j = 0; j < grid; j++) {
            el[elIndex] = iStart + j;
            el[elIndex + 1] = nextiStart + j + 1;
            el[elIndex + 2] = nextiStart + j;

            el[elIndex + 3] = iStart + j;
            el[elIndex + 4] = iStart + j + 1;
            el[elIndex + 5] = nextiStart + j + 1;

            elIndex += 6;
        }
    }
}

void VBOTeapotVNT::getPatch(int patchNum, vec3 patch[][4], bool reverseV) {
    for (int u = 0; u < 4; u++) { // Loop in u direction
        for (int v = 0; v < 4; v++) { // Loop in v direction
            if (reverseV) {
                patch[u][v] =
                        vec3(
                                Teapot::cpdata[Teapot::patchdata[patchNum][u * 4
                                        + (3 - v)]][0],
                                Teapot::cpdata[Teapot::patchdata[patchNum][u * 4
                                        + (3 - v)]][1],
                                Teapot::cpdata[Teapot::patchdata[patchNum][u * 4
                                        + (3 - v)]][2]
                                );
            } else {
                patch[u][v] = vec3(
                        Teapot::cpdata[Teapot::patchdata[patchNum][u * 4 + v]][0],
                        Teapot::cpdata[Teapot::patchdata[patchNum][u * 4 + v]][1],
                        Teapot::cpdata[Teapot::patchdata[patchNum][u * 4 + v]][2]
                        );
            }
        }
    }
}

void VBOTeapotVNT::computeBasisFunctions(float * B, float * dB, int grid) {
    float inc = 1.0f / grid;
    for (int i = 0; i <= grid; i++) {
        float t = i * inc;
        float tSqr = t * t;
        float oneMinusT = (1.0f - t);
        float oneMinusT2 = oneMinusT * oneMinusT;

        B[i * 4 + 0] = oneMinusT * oneMinusT2;
        B[i * 4 + 1] = 3.0f * oneMinusT2 * t;
        B[i * 4 + 2] = 3.0f * oneMinusT * tSqr;
        B[i * 4 + 3] = t * tSqr;

        dB[i * 4 + 0] = -3.0f * oneMinusT2;
        dB[i * 4 + 1] = -6.0f * t * oneMinusT + 3.0f * oneMinusT2;
        dB[i * 4 + 2] = -3.0f * tSqr + 6.0f * t * oneMinusT;
        dB[i * 4 + 3] = 3.0f * tSqr;
    }
}

vec3 VBOTeapotVNT::evaluate(int gridU, int gridV, float *B, vec3 patch[][4]) {
    vec3 p(0.0f, 0.0f, 0.0f);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            p += patch[i][j] * B[gridU * 4 + i] * B[gridV * 4 + j];
        }
    }
    return p;
}

vec3 VBOTeapotVNT::evaluateNormal(int gridU, int gridV, float *B, float *dB,
        vec3 patch[][4]) {
    vec3 du(0.0f, 0.0f, 0.0f);
    vec3 dv(0.0f, 0.0f, 0.0f);

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            du += patch[i][j] * dB[gridU * 4 + i] * B[gridV * 4 + j];
            dv += patch[i][j] * B[gridU * 4 + i] * dB[gridV * 4 + j];
        }
    }
    return glm::normalize(glm::cross(du, dv));
}

void VBOTeapotVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif
    glAssert;
    glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
            ((GLubyte *) NULL + (0)));
    glBindVertexArray(0);
}

VBOTeapotVNT::~VBOTeapotVNT() {
    glDeleteBuffers(4, handle);
}

void VBOTeapotVNT::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Vertex normal
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glVertexAttribPointer((GLuint) 2, 2, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(2); // texture coords
    //-------------------------------------------------------------
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    //-------------------------------------------------------------

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOTeapotVNT::generate() {
    int verts = 32 * (grid + 1) * (grid + 1);
    faces = grid * grid * 32;

    infoVertices = verts;
    infoTriangles = faces * 2;

    float * v = new float[verts * 3];
    float * n = new float[verts * 3];
    float * tc = new float[verts * 2];
    unsigned int * el = new unsigned int[faces * 6];

    generatePatches(v, n, tc, el, grid);

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, verts);
    //boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, verts);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, (3 * verts) * sizeof(float), v, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, (3 * verts) * sizeof(float), n, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, (2 * verts) * sizeof(float), tc, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * faces * sizeof(unsigned int), el,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete[] v;
    delete[] n;
    delete[] el;
    delete[] tc;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOTeapotVNT::bind(IBinder& binder) {
    binder.bindSimple(grid);
    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOTeapotVNT::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOTeapotVNT::initAfterDeserialization() {
    return generate();
}
