/*
 * RevVBOSphereVN.h
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#ifndef REVVBOSPHEREVN_H_
#define REVVBOSPHEREVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

    class VBOSphereVN: public IVBODrawable {
        static const int HANDLE_SIZE = 3;
        static const int HANDLE_VERTEX = 0;
        static const int HANDLE_NORMAL = 1;
        static const int HANDLE_ELEMENT = 2;
        unsigned int handle[HANDLE_SIZE];
        int indices = 0;
        glm::vec3 center;
        float radius = 1;
        int tessellation = 10;

        int infoVertices = 0;
        int infoTriangles = 0;

        VBOSphereVN();

    public:
        DECLARE_BINDABLE(VBOSphereVN)

        VBOSphereVN(float radius, int tessellation);
        VBOSphereVN(const glm::vec3& center, float radius, int tessellation);
        virtual ~VBOSphereVN();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

        void setRadius(float radius) {
        	this->radius = radius;
        	generate();
        }

    private:
        bool generate();
        void create(const glm::vec3& center, float radius, int tessellation);
        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

} /* namespace rev */
#endif /* REVVBOSPHEREVN_H_ */
