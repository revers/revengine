/*
 * RevVBOFrustumVN.cpp
 *
 *  Created on: 29-01-2013
 *      Author: Revers
 */

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/collision/primitives/RevViewFrustum.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOFrustumVN.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOFrustumVN)

VBOFrustumVN::VBOFrustumVN() {
    glGenBuffers(HANDLE_SIZE, handle);
    glAssert;
}

VBOFrustumVN::VBOFrustumVN(const glm::vec3& nearTopLeft,
        const glm::vec3& nearTopRight, const glm::vec3& nearBottomLeft,
        const glm::vec3& nearBottomRight, const glm::vec3& farTopLeft,
        const glm::vec3& farTopRight, const glm::vec3& farBottomLeft,
        const glm::vec3& farBottomRight) :
        nearTopLeft(nearTopLeft), nearTopRight(nearTopRight), nearBottomLeft(
                nearBottomLeft), nearBottomRight(nearBottomRight), farTopLeft(
                farTopLeft), farTopRight(farTopRight), farBottomLeft(
                farBottomLeft), farBottomRight(farBottomRight) {
    glGenBuffers(HANDLE_SIZE, handle);

    bool succ = generate();
    revAssert(succ);
}

VBOFrustumVN::VBOFrustumVN(const glm::mat4& projectionMatrix) {
    glGenBuffers(HANDLE_SIZE, handle);

    ViewFrustum frustum = ViewFrustum::fromProjectionMatrix(
            projectionMatrix);

    set(frustum);
}

VBOFrustumVN::VBOFrustumVN(const ViewFrustum& frustum) {
    glGenBuffers(HANDLE_SIZE, handle);
    set(frustum);
}

void VBOFrustumVN::set(const ViewFrustum& frustum) {
    nearTopLeft = Plane::intersection(frustum.nearPlane, frustum.leftPlane,
            frustum.topPlane);
    nearTopRight = Plane::intersection(frustum.nearPlane, frustum.rightPlane,
            frustum.topPlane);
    nearBottomLeft = Plane::intersection(frustum.nearPlane, frustum.leftPlane,
            frustum.bottomPlane);
    nearBottomRight = Plane::intersection(frustum.nearPlane, frustum.rightPlane,
            frustum.bottomPlane);

    farTopLeft = Plane::intersection(frustum.farPlane, frustum.leftPlane,
            frustum.topPlane);
    farTopRight = Plane::intersection(frustum.farPlane, frustum.rightPlane,
            frustum.topPlane);
    farBottomLeft = Plane::intersection(frustum.farPlane, frustum.leftPlane,
            frustum.bottomPlane);
    farBottomRight = Plane::intersection(frustum.farPlane, frustum.rightPlane,
            frustum.bottomPlane);

    bool succ = generate();
    revAssert(succ);
}

VBOFrustumVN::~VBOFrustumVN() {
    glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOFrustumVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT,
            ((GLubyte *) NULL + (0)));
    glAssert;
    glBindVertexArray(0);
}

void VBOFrustumVN::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-----------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
    //-----------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
    glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
    //-----------------------------------------------------------------
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
    //-----------------------------------------------------------------

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOFrustumVN::generate() {

    const vec3& ntl = nearTopLeft; // 0
    const vec3& ntr = nearTopRight; // 1
    const vec3& nbl = nearBottomLeft; // 2
    const vec3& nbr = nearBottomRight; // 3
    const vec3& ftl = farTopLeft; // 4
    const vec3& ftr = farTopRight; // 5
    const vec3& fbl = farBottomLeft; // 6
    const vec3& fbr = farBottomRight; // 7

    vec3 v[24] {
            /* 0    1    2    3  */
            // 0,   1,   2,   3  // NEAR
            ntl, ntr, nbl, nbr,

            /* 4    5    6    7  */
            // 4,   5,   6,   7  // FAR
            ftl, ftr, fbl, fbr,

            /* 8    9    10   11  */
            // 0,   2,   4,   6  // LEFT
            ntl, nbl, ftl, fbl,

            /* 12   13   14   15  */
            // 1,   3,   5,   7  // RIGHT
            ntr, nbr, ftr, fbr,

            /* 16   17   18   19  */
            // 0,   1,   4,   5  // TOP
            ntl, ntr, ftl, ftr,

            /* 20   21   22   23  */
            // 2,   3,   6,   7  // BOTTOM
            nbl, nbr, fbl, fbr
    };
    vec3* n = new vec3[infoVertices];
    unsigned int* el = new unsigned int[indices];
    int nIndex = 0;
    int elIndex = 0;

    // near face:
    vec3 a = nearBottomRight - nearTopRight;
    vec3 b = nearTopLeft - nearTopRight;
    vec3 normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // near 1
    el[elIndex++] = 3;
    el[elIndex++] = 0;
    el[elIndex++] = 1;
    // near 2
    el[elIndex++] = 3;
    el[elIndex++] = 2;
    el[elIndex++] = 0;
    //-----------------------------------------------
    // far face:
    a = farTopLeft - farTopRight;
    b = farBottomRight - farTopRight;
    normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // far 1
    el[elIndex++] = 4;
    el[elIndex++] = 7;
    el[elIndex++] = 5;
    // far 2
    el[elIndex++] = 4;
    el[elIndex++] = 6;
    el[elIndex++] = 7;
    //-----------------------------------------------
    // left face:
    a = nearBottomLeft - nearTopLeft;
    b = farTopLeft - nearTopLeft;
    normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // left 1
    el[elIndex++] = 8; // 0
    el[elIndex++] = 9; // 2
    el[elIndex++] = 10; // 4
    // left 2
    el[elIndex++] = 9; // 2
    el[elIndex++] = 11; // 6
    el[elIndex++] = 10; // 4
    //-----------------------------------------------
    // right face:
    a = farTopRight - nearTopRight;
    b = nearBottomRight - nearTopRight;
    normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // right 1
    el[elIndex++] = 14; // 5
    el[elIndex++] = 13; // 3
    el[elIndex++] = 12; // 1
    // right 2
    el[elIndex++] = 14; // 5
    el[elIndex++] = 15; // 7
    el[elIndex++] = 13; // 3

    //-----------------------------------------------
    // top face:
    a = farTopLeft - nearTopLeft;
    b = nearTopRight - nearTopLeft;
    normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // top 1
    el[elIndex++] = 16; // 0
    el[elIndex++] = 18; // 4
    el[elIndex++] = 19; // 5
    // top 2
    el[elIndex++] = 19; // 5
    el[elIndex++] = 17; // 1
    el[elIndex++] = 16; // 0

    //-----------------------------------------------
    // bottom face:
    a = nearBottomRight - nearBottomLeft;
    b = farBottomLeft - nearBottomLeft;
    normal = glm::normalize(glm::cross(a, b));
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;
    n[nIndex++] = normal;

    // bottom 1
    el[elIndex++] = 22; // 6
    el[elIndex++] = 20; // 2
    el[elIndex++] = 23; // 7
    // bottom 2
    el[elIndex++] = 20; // 2
    el[elIndex++] = 21; // 3
    el[elIndex++] = 23; // 7

    //================================================================

    // boundingSphere = rev::Sphere::smallBall(v, size);
    boundingSphere = rev::Sphere::miniBall(v, infoVertices);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, 3 * infoVertices * sizeof(float), v,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, 3 * infoVertices * sizeof(float), n,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int),
            el, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete[] n;
    delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOFrustumVN::bind(IBinder& binder) {
    binder.bindSimple(nearTopLeft);
    binder.bindSimple(nearTopRight);
    binder.bindSimple(nearBottomLeft);
    binder.bindSimple(nearBottomRight);
    binder.bindSimple(farTopLeft);
    binder.bindSimple(farTopRight);
    binder.bindSimple(farBottomLeft);
    binder.bindSimple(farBottomRight);

    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOFrustumVN::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOFrustumVN::initAfterDeserialization() {
    return generate();
}
