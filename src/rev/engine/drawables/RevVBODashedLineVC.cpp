/*
 * RevVBODashedLineVC.cpp
 *
 *  Created on: 15-04-2013
 *      Author: Revers
 */

#include <vector>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/verbose_operator.hpp>
#include <glm/gtx/transform.hpp>

#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBODashedLineVC.h"
#include <rev/common/RevAssert.h>

using namespace std;
using namespace rev;

IMPLEMENT_BINDABLE(VBODashedLineVC)

VBODashedLineVC::VBODashedLineVC() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBODashedLineVC::VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, const rev::color3& color) :
		p1(p1), p2(p2), color(color) {
	init();
}

VBODashedLineVC::VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2) :
		p1(p1), p2(p2) {
}

VBODashedLineVC::VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, float step,
		const rev::color3& color) :
		p1(p1), p2(p2), color(color), step(step) {
	init();
}

VBODashedLineVC::VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, float step) :
		p1(p1), p2(p2), step(step) {
}

void VBODashedLineVC::init() {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBODashedLineVC::~VBODashedLineVC() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBODashedLineVC::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glVertexAttrib3fv(HANDLE_COLOR, rev::value_ptr(color)); // constant color (attribute 1)

	glDrawArrays(GL_LINES, 0, infoVertices);
	glBindVertexArray(0);
}

void VBODashedLineVC::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(HANDLE_VERTEX);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBODashedLineVC::generate() {
	using namespace glm;

	std::vector<glm::vec3> verts;

	vec3 dir = p2 - p1;
	float length = glm::length(dir);
	dir /= length;
	float distance = 0.0f;

	vec3 p = p1;
	while (distance < length) {
		verts.push_back(p);
		distance += step;
		if (distance > length) {
			p = p2;
		} else {
			p += dir * step;
		}
		verts.push_back(p);
		distance += step;
		if (distance > length) {
			p = p2;
		} else {
			p += dir * step;
		}
	}

	infoVertices = verts.size();
	infoLines = infoVertices / 2;

	boundingSphere = rev::Sphere::smallBall(verts.data(), infoVertices);
	// boundingSphere = rev::Sphere::miniBall((vec3*) vert, tessellation);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, infoVertices * 3 * sizeof(GLfloat), (float*) verts.data(),
			GL_STATIC_DRAW);

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBODashedLineVC::bind(IBinder& binder) {
	binder.bindSimple(p1);
	binder.bindSimple(p2);
	binder.bindSimple(color);
	binder.bindSimple(step);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Lines", infoLines);
}

void VBODashedLineVC::bindableValueChanged(void* ptr) {
	generate();
}

bool VBODashedLineVC::initAfterDeserialization() {
	return generate();
}
