/*
 * RevVBOBicubicSurfaceVNT.h
 *
 *  Created on: 10-12-2012
 *      Author: Revers
 */

#ifndef REVVBOBICUBICSURFACEVNT_H_
#define REVVBOBICUBICSURFACEVNT_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/parametric/RevISurfacePatch.h>
#include <glm/glm.hpp>

namespace rev {

	class VBOBicubicSurfaceVNT: public IVBODrawable, public ISurfacePatch {
	public:
		enum class BasisMatrix {
			BEZIER, BSPLINE
		};

	private:
		static const int HANDLE_SIZE = 4;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_TEXTURE = 2;
		static const int HANDLE_ELEMENT = 3;
		unsigned int handle[HANDLE_SIZE];
		int faces;
		int basisMatrix = (int) BasisMatrix::BSPLINE;
		glm::vec3 p00 = glm::vec3(-0.5, 0.0, -0.5);
		glm::vec3 p01 = glm::vec3(-0.5, 0.0, -0.16666666);
		glm::vec3 p02 = glm::vec3(-0.5, 0.0, 0.16666669);
		glm::vec3 p03 = glm::vec3(-0.5, 0.0, 0.5);
		glm::vec3 p10 = glm::vec3(-0.16666666, 0.0, -0.5);
		glm::vec3 p11 = glm::vec3(-0.16666666, 0.0, -0.16666666);
		glm::vec3 p12 = glm::vec3(-0.16666666, 0.0, 0.16666669);
		glm::vec3 p13 = glm::vec3(-0.16666666, 0.0, 0.5);
		glm::vec3 p20 = glm::vec3(0.16666669, 0.0, -0.5);
		glm::vec3 p21 = glm::vec3(0.16666669, 0.0, -0.16666666);
		glm::vec3 p22 = glm::vec3(0.16666669, 0.0, 0.16666669);
		glm::vec3 p23 = glm::vec3(0.16666669, 0.0, 0.5);
		glm::vec3 p30 = glm::vec3(0.5, 0.0, -0.5);
		glm::vec3 p31 = glm::vec3(0.5, 0.0, -0.16666666);
		glm::vec3 p32 = glm::vec3(0.5, 0.0, 0.16666669);
		glm::vec3 p33 = glm::vec3(0.5, 0.0, 0.5);
		int uTessellation = 10;
		int wTessellation = 10;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOBicubicSurfaceVNT();

	public:
		DECLARE_BINDABLE(VBOBicubicSurfaceVNT)

		VBOBicubicSurfaceVNT(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p03,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p13,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				const glm::vec3& p23,
				const glm::vec3& p30,
				const glm::vec3& p31,
				const glm::vec3& p32,
				const glm::vec3& p33,
				int uTessellation,
				int wTessellation);

		VBOBicubicSurfaceVNT(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p03,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p13,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				const glm::vec3& p23,
				const glm::vec3& p30,
				const glm::vec3& p31,
				const glm::vec3& p32,
				const glm::vec3& p33,
				int tesselation);

		~VBOBicubicSurfaceVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}
		int getUPointsCount() const override {
			return 4;
		}
		int getVPointsCount() const override {
			return 4;
		}
		glm::vec3& getPoint(int u, int v) override {
			glm::vec3* points = &p00;
			return points[4 * v + u];
		}
		void recalculateSurface() override {
			generate();
		}

	private:
		bool generate();

		void create(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p03,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p13,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				const glm::vec3& p23,
				const glm::vec3& p30,
				const glm::vec3& p31,
				const glm::vec3& p32,
				const glm::vec3& p33,
				int uTessellation,
				int wTessellation);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOBICUBICSURFACEVNT_H_ */
