/*
 * RevVBOTrefoilKnotVN.h
 *
 *  Created on: 28 cze 2013
 *      Author: Revers
 */

#ifndef REVVBOTREFOILKNOTVN_H_
#define REVVBOTREFOILKNOTVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

	class VBOTrefoilKnotVN: public IVBODrawable {
		static const int HANDLE_SIZE = 3;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_ELEMENT = 2;
		unsigned int handle[HANDLE_SIZE];

		int slices = 128;
		int stacks = 32;

		int indices = 0;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOTrefoilKnotVN();

	public:
		DECLARE_BINDABLE(VBOTrefoilKnotVN)

		VBOTrefoilKnotVN(int slices, int stacks);

		virtual ~VBOTrefoilKnotVN();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

	private:
		bool generate();
		void initVAO(int contextIndex);
		glm::vec3 evaluateTrefoil(float s, float t);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

}
/* namespace rev */
#endif /* REVVBOTREFOILKNOTVN_H_ */
