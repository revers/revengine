/*
 * RevVBOCuboidVNT.h
 *
 *  Created on: 04-12-2012
 *      Author: Revers
 */

#ifndef REVVBOCUBOIDVNT_H_
#define REVVBOCUBOIDVNT_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>

namespace rev {

    class VBOCuboidVNT: public IVBODrawable {
    private:
        unsigned int handle[4];

        float xLength = 1;
        float yLength = 1;
        float zLength = 1;

        int infoVertices = 24;
        int infoTriangles = 12;

        VBOCuboidVNT();

    public:
        DECLARE_BINDABLE(VBOCuboidVNT)

        VBOCuboidVNT(float length);
        VBOCuboidVNT(float xLength, float yLength, float zLength);

        ~VBOCuboidVNT();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

        void setLengths(float xLength, float yLength, float zLength) {
        	this->xLength = xLength;
        	this->yLength = yLength;
        	this->zLength = zLength;
        	generate();
        }

    private:
        bool generate();
        void create(float xLength, float yLength, float zLength);
        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

}
#endif /* REVVBOCUBOIDVNT_H_ */
