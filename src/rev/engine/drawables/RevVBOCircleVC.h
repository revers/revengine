/*
 * RevVBOCircleVC.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVVBOCIRCLEV_H_
#define REVVBOCIRCLEV_H_

#include <rev/gl/RevColor.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

	class VBOCircleVC: public IVBODrawable {
		static const int HANDLE_SIZE = 2;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_COLOR = 1;
		unsigned int handle[HANDLE_SIZE];

		rev::color3 color = rev::color3(1, 0, 0);
		glm::vec3 center;
		float radius = 1.0f;
		glm::vec3 normal = glm::vec3(0, 0, 1);

		int tessellation = 40;
		int infoVertices = 0;
		int infoLines = 0;

		VBOCircleVC();

	public:
		DECLARE_BINDABLE(VBOCircleVC)

		VBOCircleVC(const glm::vec3& center, float radius, const glm::vec3& normal,
				int tessellation = 20);
		VBOCircleVC(const glm::vec3& center, float radius, const glm::vec3& normal,
						rev::color3 color, int tessellation = 40);
		~VBOCircleVC();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoLines;
		}
		RenderPrimitiveType getRenderPrimitiveType() const override {
			return RenderPrimitiveType::LINES;
		}
		const glm::vec3& getCenter() const {
			return center;
		}
		void setCenter(const glm::vec3& center) {
			this->center = center;
		}
		const rev::color3& getColor() const {
			return color;
		}
		void setColor(const rev::color3& color) {
			this->color = color;
		}
		const glm::vec3& getNormal() const {
			return normal;
		}
		void setNormal(const glm::vec3& normal) {
			this->normal = normal;
		}
		float getRadius() const {
			return radius;
		}
		void setRadius(float radius) {
			this->radius = radius;
		}
		void reinit() {
			generate();
		}

	private:
		bool generate();

		void init();
		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOCIRCLEV_H_ */
