/*
 * RevVBOBezierSurfaceVNT.h
 *
 *  Created on: 21-04-2013
 *      Author: Revers
 */

#ifndef REVVBOBEZIERSURFACEVNT_H_
#define REVVBOBEZIERSURFACEVNT_H_

#include <vector>
#include <glm/glm.hpp>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/parametric/RevISurfacePatch.h>

#include "RevIVBODrawable.h"

namespace rev {

	class VBOBezierSurfaceVNT: public IVBODrawable, public ISurfacePatch {
	private:
		static const int HANDLE_SIZE = 4;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_TEXTURE = 2;
		static const int HANDLE_ELEMENT = 3;
		unsigned int handle[HANDLE_SIZE];
		int faces;

		std::vector<glm::vec3> points;
		int m = 0;
		int n = 0;
		int uTessellation = 10;
		int wTessellation = 10;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOBezierSurfaceVNT();

	public:
		DECLARE_BINDABLE(VBOBezierSurfaceVNT)

		/**
		 * IMPORTANT: VBOBezierSurfaceVNT makes local copy of points.
		 */
		VBOBezierSurfaceVNT(const glm::vec3* points, int m, int n);

		/**
		 * IMPORTANT: VBOBezierSurfaceVNT makes local copy of points.
		 */
		VBOBezierSurfaceVNT(const glm::vec3* points, int m, int n,
				int uTessellation, int wTessellation);

		~VBOBezierSurfaceVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

		int getUPointsCount() const override {
			return m;
		}
		int getVPointsCount() const override {
			return n;
		}
		glm::vec3& getPoint(int u, int v) override;

		void recalculateSurface() override {
			generate();
		}

	private:
		bool generate();
		void addDefaultPoints();

		void create(const glm::vec3* points, int m, int n,
				int uTessellation, int wTessellation);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOBEZIERSURFACEVNT_H_ */
