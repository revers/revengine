/*
 * RevVBOLineVC.cpp
 *
 *  Created on: 15-04-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/verbose_operator.hpp>
#include <glm/gtx/transform.hpp>

#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOLineVC.h"
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOLineVC)

VBOLineVC::VBOLineVC() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOLineVC::VBOLineVC(const glm::vec3& p1, const glm::vec3& p2, const rev::color3& color) :
		p1(p1), p2(p2), color(color) {
	init();
}

VBOLineVC::VBOLineVC(const glm::vec3& p1, const glm::vec3& p2) :
		p1(p1), p2(p2) {
}

void VBOLineVC::init() {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOLineVC::~VBOLineVC() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOLineVC::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glVertexAttrib3fv(HANDLE_COLOR, rev::value_ptr(color)); // constant color (attribute 1)

	glDrawArrays(GL_LINES, 0, infoVertices);
	glBindVertexArray(0);
}

void VBOLineVC::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(HANDLE_VERTEX);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOLineVC::generate() {
	using namespace glm;

	boundingSphere = rev::Sphere::smallBall(&p1, infoVertices);
	// boundingSphere = rev::Sphere::miniBall((vec3*) vert, tessellation);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, infoVertices * 3 * sizeof(GLfloat), (float*) &p1,
			GL_STATIC_DRAW);

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOLineVC::bind(IBinder& binder) {
	binder.bindSimple(p1);
	binder.bindSimple(p2);
	binder.bindSimple(color);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Lines", infoLines);
}

void VBOLineVC::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOLineVC::initAfterDeserialization() {
	return generate();
}
