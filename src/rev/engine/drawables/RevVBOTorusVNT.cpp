#include <cstdio>
#include <cmath>

#include <GL/glew.h>
#include <GL/gl.h>

#include "RevVBOTorusVNT.h"
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOTorusVNT)

VBOTorusVNT::VBOTorusVNT() {
    glGenBuffers(4, handle);
    glAssert;
}

VBOTorusVNT::VBOTorusVNT(float outerRadius, float innerRadius,
        int nsides, int nrings) :
        outerRadius(outerRadius), innerRadius(innerRadius),
                rings(nrings), sides(nsides) {

    glGenBuffers(4, handle);

    bool succ = generate();
    revAssert(succ);
}

void VBOTorusVNT::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Vertex normal
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glVertexAttribPointer((GLuint) 2, 2, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(2); // Texture coords
    //-------------------------------------------------------------
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    //-------------------------------------------------------------

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOTorusVNT::generate() {
    faces = sides * rings;
    int nVerts = sides * (rings + 1); // One extra ring to duplicate first ring

    infoTriangles = 2 * faces;
    infoVertices = nVerts;

    // Verts
    float * v = new float[3 * nVerts];
    // Normals
    float * n = new float[3 * nVerts];
    // Tex coords
    float * tex = new float[2 * nVerts];
    // Elements
    unsigned int * el = new unsigned int[6 * faces];

    // Generate the vertex data
    generateVerts(v, n, tex, el, outerRadius, innerRadius);

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, nVerts);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), v, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), n, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, (2 * nVerts) * sizeof(float), tex,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * faces * sizeof(unsigned int), el,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete[] v;
    delete[] n;
    delete[] el;
    delete[] tex;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOTorusVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
            ((GLubyte *) NULL + (0)));
    glBindVertexArray(0);
}

void VBOTorusVNT::generateVerts(float* verts, float* norms, float* tex,
        unsigned int* el, float outerRadius, float innerRadius) {
    float ringFactor = (float) (TWO_PI / rings);
    float sideFactor = (float) (TWO_PI / sides);
    int idx = 0, tidx = 0;
    for (int ring = 0; ring <= rings; ring++) {
        float u = ring * ringFactor;
        float cu = cos(u);
        float su = sin(u);
        for (int side = 0; side < sides; side++) {
            float v = side * sideFactor;
            float cv = cos(v);
            float sv = sin(v);
            float r = (outerRadius + innerRadius * cv);
            verts[idx] = r * cu;
            verts[idx + 1] = r * su;
            verts[idx + 2] = innerRadius * sv;
            norms[idx] = cv * cu * r;
            norms[idx + 1] = cv * su * r;
            norms[idx + 2] = sv * r;
            tex[tidx] = (float) (u / TWO_PI);
            tex[tidx + 1] = (float) (v / TWO_PI);
            tidx += 2;
            // Normalize
            float len = sqrt(norms[idx] * norms[idx] +
                    norms[idx + 1] * norms[idx + 1] +
                    norms[idx + 2] * norms[idx + 2]);
            norms[idx] /= len;
            norms[idx + 1] /= len;
            norms[idx + 2] /= len;
            idx += 3;
        }
    }

    idx = 0;
    for (int ring = 0; ring < rings; ring++) {
        int ringStart = ring * sides;
        int nextRingStart = (ring + 1) * sides;
        for (int side = 0; side < sides; side++) {
            int nextSide = (side + 1) % sides;
            // The quad
            el[idx] = (ringStart + side);
            el[idx + 1] = (nextRingStart + side);
            el[idx + 2] = (nextRingStart + nextSide);
            el[idx + 3] = ringStart + side;
            el[idx + 4] = nextRingStart + nextSide;
            el[idx + 5] = (ringStart + nextSide);
            idx += 6;
        }
    }
}

VBOTorusVNT::~VBOTorusVNT() {
    glDeleteBuffers(4, handle);
}

void VBOTorusVNT::bind(IBinder& binder) {
    binder.bindSimple(outerRadius);
    binder.bindSimple(innerRadius);
    binder.bindSimple(sides);
    binder.bindSimple(rings);
    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOTorusVNT::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOTorusVNT::initAfterDeserialization() {
    return generate();
}
