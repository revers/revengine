/*
 * RevVBOCylinderVN.cpp
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOCylinderVN.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOCylinderVN)

VBOCylinderVN::VBOCylinderVN() {
    glGenBuffers(HANDLE_SIZE, handle);
    glAssert;
}

VBOCylinderVN::VBOCylinderVN(float height, float radius, int tessellation) :
        height(height), radius(radius), tessellation(tessellation) {
    glGenBuffers(HANDLE_SIZE, handle);

    bool succ = generate();
    revAssert(succ);
}

void VBOCylinderVN::createCap(int tessellation,
        float height,
        float radius,
        const glm::vec3& normal,
        glm::vec3* v,
        glm::vec3* n,
        unsigned int* el,
        int& vIndex,
        int& nIndex,
        int& elIndex) {
    // Create cap indices.
    for (int i = 0; i < tessellation - 2; i++) {
        if (normal.y > 0) {
            el[elIndex++] = vIndex;
            el[elIndex++] = vIndex + (i + 1) % tessellation;
            el[elIndex++] = vIndex + (i + 2) % tessellation;
        } else {
            el[elIndex++] = vIndex;
            el[elIndex++] = vIndex + (i + 2) % tessellation;
            el[elIndex++] = vIndex + (i + 1) % tessellation;
        }
    }

    // Create cap vertices.
    for (int i = 0; i < tessellation; i++) {
        vec3 position = getCircleVector(i, tessellation) * radius +
                normal * height;

        v[vIndex++] = position;
        n[nIndex++] = normal;
    }
}

glm::vec3 VBOCylinderVN::getCircleVector(int i, int tessellation) {
    float angle = i * TWO_PI / tessellation;

    float dx = (float) cos(angle);
    float dz = (float) sin(angle);

    return vec3(dx, 0, dz);
}

VBOCylinderVN::~VBOCylinderVN() {
    glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOCylinderVN::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-----------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
    //-----------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
    glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
    //-----------------------------------------------------------------
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
    //-----------------------------------------------------------------

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOCylinderVN::generate() {

    int size = 4 * tessellation;

    vec3* v = new vec3[size];
    vec3* n = new vec3[size];
    indices = tessellation * 6 + (tessellation - 2) * 6;

    infoVertices = size;
    infoTriangles = indices / 3;

    unsigned int* el = new unsigned int[indices];
    int vIndex = 0;
    int nIndex = 0;
    int elIndex = 0;

    float h = height / 2;

    // Create a ring of triangles around the outside of the cylinder.
    for (int i = 0; i < tessellation; i++) {
        vec3 normal = getCircleVector(i, tessellation);

        v[vIndex++] = normal * radius + MathHelper::POSITIVE_Y * h;
        v[vIndex++] = normal * radius + MathHelper::NEGATIVE_Y * h;
        n[nIndex++] = normal;
        n[nIndex++] = normal;

        el[elIndex++] = i * 2;
        el[elIndex++] = i * 2 + 1;
        el[elIndex++] = (i * 2 + 2) % (tessellation * 2);

        el[elIndex++] = i * 2 + 1;
        el[elIndex++] = (i * 2 + 3) % (tessellation * 2);
        el[elIndex++] = (i * 2 + 2) % (tessellation * 2);
    }

    // Create flat triangle fan caps to seal the top and bottom.
    createCap(tessellation, h, radius, MathHelper::POSITIVE_Y, v, n, el, vIndex,
            nIndex, elIndex);
    createCap(tessellation, h, radius, MathHelper::NEGATIVE_Y, v, n, el, vIndex,
            nIndex, elIndex);

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, size);
    // boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, size);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
            GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int),
            el, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete[] v;
    delete[] n;
    delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOCylinderVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT,
            ((GLubyte *) NULL + (0)));
    glAssert;
    glBindVertexArray(0);
}

void VBOCylinderVN::bind(IBinder& binder) {
    binder.bindSimple(height);
    binder.bindSimple(radius);
    binder.bindSimple(tessellation);

    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOCylinderVN::bindableValueChanged(void* ptr) {
    generate();
}

bool VBOCylinderVN::initAfterDeserialization() {
    return generate();
}

