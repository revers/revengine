/*
 * RevVBOConeVN.h
 *
 *  Created on: 19-12-2012
 *      Author: Revers
 */

#ifndef REVVBOCONEVN_H_
#define REVVBOCONEVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

    class VBOConeVN: public IVBODrawable {
        static const int HANDLE_SIZE = 3;
        static const int HANDLE_VERTEX = 0;
        static const int HANDLE_NORMAL = 1;
        static const int HANDLE_ELEMENT = 2;
        unsigned int handle[HANDLE_SIZE];
        int indices = 0;

        float radius = 1;
        float height = 1;
        int tessellation = 10;

        int infoVertices = 0;
        int infoTriangles = 0;

        VBOConeVN();

    public:
        DECLARE_BINDABLE(VBOConeVN)

        VBOConeVN(float r, float height, int tessellation);

        virtual ~VBOConeVN();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

    private:
        bool generate();

        void createCap(int tessellation,
                float height,
                float radius,
                const glm::vec3& normal,
                glm::vec3* v,
                glm::vec3* n,
                unsigned int* el,
                int& vIndex,
                int& nIndex,
                int& elIndex);

        glm::vec3 getCircleVector(int i, int tessellation);

        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

}
/* namespace rev */
#endif /* REVVBOCONEVN_H_ */
