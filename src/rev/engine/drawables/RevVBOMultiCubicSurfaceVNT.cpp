/*
 * RevVBOMultiCubicSurfaceVNT.cpp
 *
 *  Created on: 23-04-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevDelete.hpp>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/parametric/RevBezierSurface.h>
#include <rev/common/RevAssert.h>

#include "RevVBOMultiCubicSurfaceVNT.h"

using namespace rev;

IMPLEMENT_BINDABLE(VBOMultiCubicSurfaceVNT)

VBOMultiCubicSurfaceVNT::VBOMultiCubicSurfaceVNT() :
		uTessellation(20), wTessellation(20) {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOMultiCubicSurfaceVNT::VBOMultiCubicSurfaceVNT(int m, int n, int uTessellation, int wTessellation) :
		surface(m, n), uTessellation(uTessellation), wTessellation(wTessellation) {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

glm::vec3& VBOMultiCubicSurfaceVNT::getPoint(int u, int v) {
	revAssert(surface.n * u + v < surface.m * surface.n);
	return surface.points[surface.n * u + v];
}

void VBOMultiCubicSurfaceVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glAssert;
	glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

VBOMultiCubicSurfaceVNT::~VBOMultiCubicSurfaceVNT() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOMultiCubicSurfaceVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glVertexAttribPointer((GLuint) HANDLE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_TEXTURE); // Texture coords
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

bool VBOMultiCubicSurfaceVNT::generate() {
	if (surface.m < 4 || surface.n < 4) {
		if (*mPtr < 4) {
			*mPtr = 4;
		}
		if (*nPtr < 4) {
			*nPtr = 4;
		}
		surface.recreate();
	}
	using namespace glm;

	faces = uTessellation * wTessellation;

	int size = (uTessellation + 1) * (wTessellation + 1);

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];
	vec2* tex = new vec2[size];
	int elementSize = 6 * uTessellation * wTessellation;

	infoVertices = size;
	infoTriangles = elementSize / 3;

	unsigned int* el = new unsigned int[elementSize];

	typedef rev::MultiCubicSurface::vec3_pair vec3_pair;

	float iFactor = 1.0f / wTessellation;
	float jFactor = 1.0f / uTessellation;
	int index = 0;
	for (int i = 0; i <= wTessellation; i++) {
		float w = iFactor * i;
		for (int j = 0; j <= uTessellation; j++) {
			float u = jFactor * j;

			vec3_pair p = surface.evalAndNormal(u, w);
			v[index] = p.first;
			n[index] = p.second;
			tex[index] = vec2((float) u, (float) w);
			index++;
		}
	}

	boundingSphere = rev::Sphere::smallBall(v, size);
	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, size);

	unsigned int rowStart, nextRowStart;
	int idx = 0;
	for (int i = 0; i < wTessellation; i++) {
		rowStart = i * (uTessellation + 1);
		nextRowStart = (i + 1) * (uTessellation + 1);
		for (int j = 0; j < uTessellation; j++) {
			el[idx] = rowStart + j;
			el[idx + 1] = nextRowStart + j;
			el[idx + 2] = nextRowStart + j + 1;
			el[idx + 3] = rowStart + j;
			el[idx + 4] = nextRowStart + j + 1;
			el[idx + 5] = rowStart + j + 1;
			idx += 6;
		}
	}

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glBufferData(GL_ARRAY_BUFFER, 2 * size * sizeof(float),
			tex, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementSize * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] tex;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOMultiCubicSurfaceVNT::bind(IBinder& binder) {
	binder.bindSimple(uTessellation);
	binder.bindSimple(wTessellation);
	binder.bind(this, "m", MEMBER_PTR_WRAPPER(mPtr), true, true, true);
	binder.bind(this, "n", MEMBER_PTR_WRAPPER(nPtr), true, true, true);
	binder.bind(this, "pointShift", MEMBER_PTR_WRAPPER(pointShiftPtr), true, true, true);
	binder.bind(this, "pointDistance", MEMBER_PTR_WRAPPER(pointDistancePtr), true, true, true);
	binder.bindList(this, "points", &VBOMultiCubicSurfaceVNT::pointsPtr, true, false, false);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOMultiCubicSurfaceVNT::bindableValueChanged(void* ptr) {
	if (ptr == mPtr) {
		if (*mPtr < 4) {
			*mPtr = 4;
		}
		surface.recreate();
		generate();
		if (pointChangedCallback) {
			pointChangedCallback();
		}
	} else if (ptr == nPtr) {
		if (*nPtr < 4) {
			*nPtr = 4;
		}
		surface.recreate();
		generate();
		if (pointChangedCallback) {
			pointChangedCallback();
		}
	} else if (ptr == pointDistancePtr) {
		surface.recreate();
		generate();
	} else {
		generate();
	}
}

bool VBOMultiCubicSurfaceVNT::initAfterDeserialization() {
	return generate();
}
