/*
 * RevVBOTorusKnotVN.h
 *
 *  Created on: 3 sie 2013
 *      Author: Revers
 *
 * Torus Knot by Nathaniel Meyer:
 * http://devmaster.net/posts/3095/shader-effects-screen-space-ambient-occlusion
 */

#ifndef REVVBOTORUSKNOTVN_H_
#define REVVBOTORUSKNOTVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

class VBOTorusKnotVN: public IVBODrawable {
	static const int HANDLE_SIZE = 3;
	static const int HANDLE_VERTEX = 0;
	static const int HANDLE_NORMAL = 1;
	static const int HANDLE_ELEMENT = 2;
	unsigned int handle[HANDLE_SIZE];

	/**
	 * P times around its axis of rotational symmetry.
	 */
	int p = 2;

	/**
	 * Q times around a circle in the interior of the torus.
	 */
	int q = 3;

	/**
	 * Number of horizontal segments to create.
	 */
	int numSegments = 64;

	/**
	 * Number of vertical rings to create.
	 */
	int numRings = 64;

	/**
	 * Radius of the tube.
	 */
	float radius = 1.0; // 0.1;

	/**
	 * Distance from the center of the torus to the center of the tube.
	 */
	float distance = 5.0; // 0.5

	int indices = 0;

	int infoVertices = 0;
	int infoTriangles = 0;

	VBOTorusKnotVN();

public:
	DECLARE_BINDABLE(VBOTorusKnotVN)

	VBOTorusKnotVN(int p, int q, int numSegments = 64, int numRings = 64,
			float radius = 1.0, float distance = 5.0);

	virtual ~VBOTorusKnotVN();

	void render() override;
	void bindableValueChanged(void* ptr) override;

	int getPrimitiveCount() override {
		return infoTriangles;
	}

private:
	bool generate();
	void initVAO(int contextIndex);
	void createNormals(glm::vec3* vertices, glm::vec3* normals,
			unsigned int* indexArray, int indices);

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;
};

}
/* namespace rev */

#endif /* REVVBOTORUSKNOTVN_H_ */
