/*
 * RevVBOBiquadraticSurfaceVNT.h
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#ifndef REVVBOBILINEARSURFACEVNT_H_
#define REVVBOBILINEARSURFACEVNT_H_

#include <glm/glm.hpp>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/parametric/RevISurfacePatch.h>

#include "RevIVBODrawable.h"

namespace rev {

	class VBOBilinearSurfaceVNT: public IVBODrawable, public ISurfacePatch {
	private:
		static const int HANDLE_SIZE = 4;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_TEXTURE = 2;
		static const int HANDLE_ELEMENT = 3;
		unsigned int handle[HANDLE_SIZE];
		int faces;

		glm::vec3 p00 = glm::vec3(-0.5, 0.0, -0.5);
		glm::vec3 p01 = glm::vec3(-0.5, 0.0, 0.5);
		glm::vec3 p10 = glm::vec3(0.5, 0.0, -0.5);
		glm::vec3 p11 = glm::vec3(0.5, 0.0, 0.5);
		int uTessellation = 10;
		int wTessellation = 10;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOBilinearSurfaceVNT();

	public:
		DECLARE_BINDABLE(VBOBilinearSurfaceVNT)

		VBOBilinearSurfaceVNT(
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p10,
				const glm::vec3& p11,
				int uTessellation,
				int wTessellation);

		VBOBilinearSurfaceVNT(
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p10,
				const glm::vec3& p11,
				int tesselation);

		~VBOBilinearSurfaceVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

		int getUPointsCount() const override {
			return 2;
		}
		int getVPointsCount() const override {
			return 2;
		}
		glm::vec3& getPoint(int u, int v) override {
			glm::vec3* points = &p00;
			return points[2 * v + u];
		}
		void recalculateSurface() override {
			generate();
		}

	private:
		bool generate();

		void create(
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p10,
				const glm::vec3& p11,
				int uTessellation,
				int wTessellation);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOBILINEARSURFACEVNT_H_ */
