/*
 * RevRenderPrimitiveType.h
 *
 *  Created on: 21-03-2013
 *      Author: Revers
 */

#ifndef REVRENDERPRIMITIVETYPE_H_
#define REVRENDERPRIMITIVETYPE_H_

namespace rev {

	enum class RenderPrimitiveType {
		TRIANGLES, LINES, TRIANGLE_STRIP, POINTS
	};
}

#endif /* REVRENDERPRIMITIVETYPE_H_ */
