#ifndef REVVBOCUBEVNT_H
#define REVVBOCUBEVNT_H

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>

namespace rev {

    class VBOCubeVNT: public IVBODrawable {
    private:
        unsigned int handle[4];
        float side = 1;

        int infoVertices = 24;
        int infoTriangles = 12;

        VBOCubeVNT();

    public:
        DECLARE_BINDABLE(VBOCubeVNT)

        VBOCubeVNT(float side);
        ~VBOCubeVNT();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

    private:
        bool generate();

        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

}
#endif // REVVBOCUBEVNT_H
