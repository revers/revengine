/*
 * RevVBOBilinearSurfaceVNT.cpp
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/parametric/RevBilinearSurface.h>
#include <rev/common/RevAssert.h>

#include "RevVBOBilinearSurfaceVNT.h"

using namespace rev;

IMPLEMENT_BINDABLE(VBOBilinearSurfaceVNT)

VBOBilinearSurfaceVNT::VBOBilinearSurfaceVNT() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOBilinearSurfaceVNT::VBOBilinearSurfaceVNT(
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p10,
		const glm::vec3& p11,
		int uTessellation,
		int wTessellation) {
	create(p00, p01, p10, p11, uTessellation, wTessellation);
}

VBOBilinearSurfaceVNT::VBOBilinearSurfaceVNT(
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p10,
		const glm::vec3& p11,
		int tesselation) {
	create(p00, p01, p10, p11, tesselation, tesselation);
}

void VBOBilinearSurfaceVNT::create(
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p10,
		const glm::vec3& p11,
		int uTessellation,
		int wTessellation) {
	this->p00 = p00;
	this->p01 = p01;
	this->p10 = p10;
	this->p11 = p11;
	this->uTessellation = uTessellation;
	this->wTessellation = wTessellation;

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOBilinearSurfaceVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glAssert;
	glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

VBOBilinearSurfaceVNT::~VBOBilinearSurfaceVNT() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOBilinearSurfaceVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glVertexAttribPointer((GLuint) HANDLE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_TEXTURE); // Texture coords
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

bool VBOBilinearSurfaceVNT::generate() {
	using namespace glm;

	faces = uTessellation * wTessellation;

	int size = (uTessellation + 1) * (wTessellation + 1);

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];
	vec2* tex = new vec2[size];
	int elementSize = 6 * uTessellation * wTessellation;

	infoVertices = size;
	infoTriangles = elementSize / 3;

	unsigned int* el = new unsigned int[elementSize];

	typedef rev::BilinearSurfaced::vec3_t vec3_t;
	rev::BilinearSurfaced surface(
			(vec3_t(p00)), (vec3_t(p01)),
			(vec3_t(p10)), (vec3_t(p11)));

	double iFactor = 1.0f / wTessellation;
	double jFactor = 1.0f / uTessellation;
	int index = 0;
	for (int i = 0; i <= wTessellation; i++) {
		double w = iFactor * i;
		for (int j = 0; j <= uTessellation; j++) {
			double u = jFactor * j;
			v[index] = vec3(surface.eval(u, w));
			n[index] = vec3(surface.normal(u, w));
			tex[index] = vec2((float) u, (float) w);
			index++;
		}
	}

	boundingSphere = rev::Sphere::smallBall(v, size);
	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, size);
//	for (int i = 0; i < size; i++) {
//		v[i] -= boundingSphere.center;
//	}
//	boundingSphere.center = vec3(0.0f);

	unsigned int rowStart, nextRowStart;
	int idx = 0;
	for (int i = 0; i < wTessellation; i++) {
		rowStart = i * (uTessellation + 1);
		nextRowStart = (i + 1) * (uTessellation + 1);
		for (int j = 0; j < uTessellation; j++) {
			el[idx] = rowStart + j;
			el[idx + 1] = nextRowStart + j;
			el[idx + 2] = nextRowStart + j + 1;
			el[idx + 3] = rowStart + j;
			el[idx + 4] = nextRowStart + j + 1;
			el[idx + 5] = rowStart + j + 1;
			idx += 6;
		}
	}

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glBufferData(GL_ARRAY_BUFFER, 2 * size * sizeof(float),
			tex, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementSize * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] tex;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOBilinearSurfaceVNT::bind(IBinder& binder) {
	binder.bindSimple(uTessellation);
	binder.bindSimple(wTessellation);
	binder.bindSimple(p00);
	binder.bindSimple(p01);
	binder.bindSimple(p10);
	binder.bindSimple(p11);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOBilinearSurfaceVNT::bindableValueChanged(void* ptr) {
	generate();
	if (pointChangedCallback) {
		pointChangedCallback();
	}
}

bool VBOBilinearSurfaceVNT::initAfterDeserialization() {
	return generate();
}
