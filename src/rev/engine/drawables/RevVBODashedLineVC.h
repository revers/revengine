/*
 * RevVBODashedLineVC.h
 *
 *  Created on: 15-04-2013
 *      Author: Revers
 */

#ifndef REVVBODASHEDLINEVC_H_
#define REVVBODASHEDLINEVC_H_

#include <rev/gl/RevColor.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

	class VBODashedLineVC: public IVBODrawable {
		static const int HANDLE_SIZE = 2;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_COLOR = 1;
		unsigned int handle[HANDLE_SIZE];

		rev::color3 color = rev::color3(1, 1, 1);
		glm::vec3 p1 = glm::vec3(10, 10, 10);
		glm::vec3 p2 = glm::vec3(-10, -10, -10);
		float step = 0.2f;

		int infoVertices = 2;
		int infoLines = 1;

		VBODashedLineVC();

	public:
		DECLARE_BINDABLE(VBODashedLineVC)

		VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2);
		VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, float step);
		VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, const rev::color3& color);
		VBODashedLineVC(const glm::vec3& p1, const glm::vec3& p2, float step, const rev::color3& color);
		~VBODashedLineVC();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoLines;
		}
		RenderPrimitiveType getRenderPrimitiveType() const override {
			return RenderPrimitiveType::LINES;
		}
		const rev::color3& getColor() const {
			return color;
		}
		void setColor(const rev::color3& color) {
			this->color = color;
		}
		const glm::vec3& getP1() const {
			return p1;
		}
		void setP1(const glm::vec3& p1) {
			this->p1 = p1;
		}
		const glm::vec3& getP2() const {
			return p2;
		}
		void setP2(const glm::vec3& p2) {
			this->p2 = p2;
		}
		void reinit() {
			generate();
		}

	private:
		bool generate();

		void init();
		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBODASHEDLINEVC_H_ */
