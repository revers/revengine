/*
 * RevVBOTorusKnotVN.cpp
 *
 *  Created on: 3 sie 2013
 *      Author: Revers
 *
 * Torus Knot by Nathaniel Meyer:
 * http://devmaster.net/posts/3095/shader-effects-screen-space-ambient-occlusion
 */

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOTorusKnotVN.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOTorusKnotVN)

VBOTorusKnotVN::VBOTorusKnotVN() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOTorusKnotVN::VBOTorusKnotVN(int p, int q, int numSegments /* = 64 */,
		int numRings /* = 64 */, float radius /* = 1.0 */, float distance /* = 5.0 */) :
		p(p), q(q), numSegments(numSegments), numRings(numRings),
				radius(radius), distance(distance) {

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOTorusKnotVN::~VBOTorusKnotVN() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOTorusKnotVN::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void VBOTorusKnotVN::createNormals(vec3* vertices, vec3* normals,
		unsigned int* indexArray, int indices) {

	// Sum normals
	for (int i = 0; i < indices; i += 3) {
		const unsigned int& i1 = indexArray[i];
		const unsigned int& i2 = indexArray[i + 1];
		const unsigned int& i3 = indexArray[i + 2];

		const vec3& p1 = vertices[i1];
		const vec3& p2 = vertices[i2];
		const vec3& p3 = vertices[i3];

		vec3 v1 = p1 - p2;
		vec3 v2 = p2 - p3;
		vec3 normal = glm::normalize(glm::cross(v1, v2));

		normals[i1] = normal;
		normals[i2] = normal;
		normals[i3] = normal;
	}
}

bool VBOTorusKnotVN::generate() {
	indices = ((numSegments - 1) * 6) * (numRings - 1);

	infoVertices = numSegments * numRings;
	infoTriangles = indices / 3;

	vec3* vertexArray = new vec3[infoVertices];
	vec3* normalArray = new vec3[infoVertices];
	unsigned int* indexArray = new unsigned int[indices];

	// Range between 0 and 2PI
	float u;
	float v;

	int point = 0;
	int index = 0;

	for (int i = 0; i < numRings; ++i) {
		v = (i / (numRings - 1.0f)) * (2.0f * PI);

		// One ring (circle) in the torus
		for (int j = 0; j < numSegments; ++j) {
			u = (j / (numSegments - 1.0f)) * (2.0f * PI);

			float w = distance + radius * (cos(q * u) + cos(q * v));
			float x = w * cos(p * u);
			float y = w * sin(p * u);
			float z = radius * (sin(q * v) + sin(q * u));

			vertexArray[point] = vec3(x, y, z);
			// SetUV(point, new Point(i / (numRings - 1.0f), j / (numSegments - 1.0f)));

			if ((i < (numRings - 1)) && (j < (numSegments - 1))) {
				indexArray[index] = point;
				indexArray[index + 1] = point + 1;
				indexArray[index + 2] = point + 1 + numSegments;

				indexArray[index + 3] = point + 1 + numSegments;
				indexArray[index + 4] = point + numSegments;
				indexArray[index + 5] = point;

				index += 6;
			}

			++point;
		}
	}

	// Set Normals
	createNormals(vertexArray, normalArray, indexArray, indices);

	// Average normals along the seam
	int index2;
	for (int i = 0; i < numRings; ++i) {
		index = i * numSegments;
		index2 = index + (numSegments - 1);

		normalArray[index] = (normalArray[index] + normalArray[index2]) * 0.5f;
		normalArray[index2] = normalArray[index];
	}

	// Average normals along the seam
	int endIndex = (numRings - 1) * numSegments;
	for (int i = 0; i < numSegments; ++i) {
		index = i;
		normalArray[index] = (normalArray[index] + normalArray[index + endIndex]) * 0.5f;
		normalArray[index + endIndex] = normalArray[index];
	}

	// boundingSphere = rev::Sphere::smallBall(v, infoVertices);
	boundingSphere = rev::Sphere::miniBall(vertexArray, infoVertices);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * infoVertices * sizeof(float), vertexArray,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * infoVertices * sizeof(float), normalArray,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int), indexArray, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] vertexArray;
	delete[] normalArray;
	delete[] indexArray;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOTorusKnotVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT, ((GLubyte *) NULL + (0)));

	glAssert;
	glBindVertexArray(0);
}

void VBOTorusKnotVN::bind(IBinder& binder) {
	binder.bindSimple(p);
	binder.bindSimple(q);
	binder.bindSimple(numSegments);
	binder.bindSimple(numRings);
	binder.bindSimple(radius);
	binder.bindSimple(distance);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOTorusKnotVN::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOTorusKnotVN::initAfterDeserialization() {
	return generate();
}

