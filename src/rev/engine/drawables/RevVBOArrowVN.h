/*
 * RevVBOArrowVN.h
 *
 *  Created on: 19-12-2012
 *      Author: Revers
 */

#ifndef REVVBOARROWVN_H_
#define REVVBOARROWVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <glm/glm.hpp>

namespace rev {

class VBOArrowVN: public IVBODrawable {
public:
	enum class Orientation {
		X, Y, Z
	};

private:
	static const int HANDLE_SIZE = 3;
	static const int HANDLE_VERTEX = 0;
	static const int HANDLE_NORMAL = 1;
	static const int HANDLE_ELEMENT = 2;
	unsigned int handle[HANDLE_SIZE];
	int indices = 0;

	float radius = 1;
	float height = 1;
	int tessellation = 10;

	int infoVertices = 0;
	int infoTriangles = 0;

	int orientation = int(Orientation::Y);

	VBOArrowVN();

public:
	DECLARE_BINDABLE(VBOArrowVN)

	VBOArrowVN(float r, float height, int tessellation);

	virtual ~VBOArrowVN();

	void render() override;
	void bindableValueChanged(void* ptr) override;

	int getPrimitiveCount() override {
		return infoTriangles;
	}

	void setOrientation(Orientation orientation) {
		this->orientation = int(orientation);
		generate();
	}

	Orientation getOrientation() {
		return (Orientation) orientation;
	}

private:
	bool generate();

	void createCap(int tessellation,
			float height,
			float radius,
			const glm::vec3& normal,
			glm::vec3* v,
			glm::vec3* n,
			unsigned int* el,
			int& vIndex,
			int& nIndex,
			int& elIndex,
			const glm::mat4& rot);

	glm::vec3 getCircleVector(int i, int tessellation);

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;

private:
	void initVAO(int contextIndex);
};

}
/* namespace rev */
#endif /* REVVBOARROWVN_H_ */
