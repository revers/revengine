/*
 * RevVBOTriangularCageVN.h
 *
 *  Created on: 03-12-2012
 *      Author: Revers
 */

#ifndef REVVBOCAGEVN_H_
#define REVVBOCAGEVN_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>

namespace rev {

	class VBOTriangularCageVN: public IVBODrawable {
		unsigned int handle[2];
		int vertices = 0;
		int infoVertices = 0;
		int infoTriangles = 0;
		int arraySize = 0;

		float xLength = 1;
		float yLength = 1;
		float zLength = 1;
		int xTesselation = 10;
		int yTesselation = 10;
		int zTesselation = 10;

		VBOTriangularCageVN();

	public:
		DECLARE_BINDABLE(VBOTriangularCageVN)

		VBOTriangularCageVN(float xLength, float yLength, float zLength,
				int xTesselation, int yTesselation, int zTesselation);

		VBOTriangularCageVN(float length, int tesselation);

		~VBOTriangularCageVN();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}

	private:
		bool generate();

		void create(float xLength, float yLength, float zLength,
				int xTesselation, int yTesselation, int zTesselation);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOCAGEVN_H_ */
