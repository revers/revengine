/*
 * RevVBOTrefoilKnotVN.cpp
 *
 *  Created on: 28 cze 2013
 *      Author: Revers
 */

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOTrefoilKnotVN.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(VBOTrefoilKnotVN)

VBOTrefoilKnotVN::VBOTrefoilKnotVN() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOTrefoilKnotVN::VBOTrefoilKnotVN(int slices, int stacks) :
		slices(slices), stacks(stacks) {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOTrefoilKnotVN::~VBOTrefoilKnotVN() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOTrefoilKnotVN::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

glm::vec3 VBOTrefoilKnotVN::evaluateTrefoil(float s, float t) {
	const float a = 0.5f;
	const float b = 0.3f;
	const float c = 0.5f;
	const float d = 0.1f;
	const float u = (1 - s) * 2 * TWO_PI;
	const float v = t * TWO_PI;
	const float r = a + b * cos(1.5f * u);
	const float x = r * cos(u);
	const float y = r * sin(u);
	const float z = c * sin(1.5f * u);

	vec3 dv;
	dv.x = -1.5f * b * sin(1.5f * u) * cos(u) - (a + b * cos(1.5f * u)) * sin(u);
	dv.y = -1.5f * b * sin(1.5f * u) * sin(u) + (a + b * cos(1.5f * u)) * cos(u);
	dv.z = 1.5f * c * cos(1.5f * u);

	vec3 q = glm::normalize(dv);
	vec3 qvn = glm::normalize(vec3(q.y, -q.x, 0));
	vec3 ww = glm::cross(q, qvn);

	vec3 range;
	range.x = x + d * (qvn.x * cos(v) + ww.x * sin(v));
	range.y = y + d * (qvn.y * cos(v) + ww.y * sin(v));
	range.z = z + d * ww.z * sin(v);
	return range;
}

/**
 * Alghorithm by Philip Rideout (http://prideout.net/blog/?p=22)
 */
bool VBOTrefoilKnotVN::generate() {
	int size = slices * stacks;
	indices = size * 6;

	infoVertices = size;
	infoTriangles = indices / 3;

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];

	float ds = 1.0f / slices;
	float dt = 1.0f / stacks;

	// The upper bounds in these loops are tweaked to reduce the
	// chance of precision error causing an incorrect # of iterations.
	int index = 0;
	for (float s = 0; s < 1 - ds / 2; s += ds) {
		for (float t = 0; t < 1 - dt / 2; t += dt) {
			const float E = 0.01f;
			vec3 pVec = evaluateTrefoil(s, t);
			vec3 uVec = evaluateTrefoil(s + E, t) - pVec;
			vec3 vVec = evaluateTrefoil(s, t + E) - pVec;
			vec3 nVec = glm::normalize(glm::cross(uVec, vVec));

			v[index] = pVec;
			n[index] = nVec;
			index++;
		}
	}

	revAssert(index == size);

	unsigned int* el = new unsigned int[indices];
	int stacksCount = 0;
	index = 0;
	for (int i = 0; i < slices; i++) {
		for (int j = 0; j < stacks; j++) {
			el[index++] = stacksCount + j;
			el[index++] = stacksCount + (j + 1) % stacks;
			el[index++] = (stacksCount + j + stacks) % size;

			el[index++] = (stacksCount + j + stacks) % size;
			el[index++] = (stacksCount + (j + 1) % stacks) % size;
			el[index++] = (stacksCount + (j + 1) % stacks + stacks) % size;
		}
		stacksCount += stacks;
	}

	revAssert(stacksCount == size);
	revAssert(index == indices);

	// boundingSphere = rev::Sphere::smallBall(v, size);
	boundingSphere = rev::Sphere::miniBall(v, size);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int), el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOTrefoilKnotVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT, ((GLubyte *) NULL + (0)));

	glAssert;
	glBindVertexArray(0);
}

void VBOTrefoilKnotVN::bind(IBinder& binder) {
	binder.bindSimple(slices);
	binder.bindSimple(stacks);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOTrefoilKnotVN::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOTrefoilKnotVN::initAfterDeserialization() {
	return generate();
}
