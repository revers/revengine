/*
 * RevVBOCircleVC.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/verbose_operator.hpp>
#include <glm/gtx/transform.hpp>

#include <rev/gl/RevGLAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevVBOCircleVC.h"
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOCircleVC)

VBOCircleVC::VBOCircleVC() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOCircleVC::VBOCircleVC(const glm::vec3& center, float radius, const glm::vec3& normal,
		int tessellation) :
		center(center), radius(radius), normal(normal), tessellation(tessellation) {
	init();
}

VBOCircleVC::VBOCircleVC(const glm::vec3& center, float radius, const glm::vec3& normal,
		rev::color3 color, int tessellation) :
		center(center), radius(radius), normal(normal), color(color), tessellation(tessellation) {
	init();
}

void VBOCircleVC::init() {
	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

VBOCircleVC::~VBOCircleVC() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOCircleVC::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glVertexAttrib3fv(HANDLE_COLOR, rev::value_ptr(color)); // constant color (attribute 1)

	glDrawArrays(GL_LINES, 0, infoVertices);
	glBindVertexArray(0);
}

void VBOCircleVC::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(HANDLE_VERTEX);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOCircleVC::generate() {
	using namespace glm;

	infoLines = tessellation;
	infoVertices = tessellation * 2;
	vec3* vert = new vec3[infoVertices];

	vec3 r = glm::normalize(MathHelper::anyOrthogonal(normal)) * radius;
	vec3 lastV = r + center;
	float step = 360.0f / tessellation;
	for (int i = 0; i < tessellation; i++) {
		float angle = step * (i + 1);
		mat4 rot = glm::rotate(angle, normal);
		vert[i * 2] = lastV;
		lastV = mul(rot, r) + center;
		vert[i * 2 + 1] = lastV;
	}

	boundingSphere = rev::Sphere::smallBall((vec3*) vert, tessellation);
	// boundingSphere = rev::Sphere::miniBall((vec3*) vert, tessellation);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, infoVertices * 3 * sizeof(GLfloat), (float*) vert,
			GL_STATIC_DRAW);

	delete[] vert;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOCircleVC::bind(IBinder& binder) {
	binder.bindSimple(tessellation);
	binder.bindSimple(center);
	binder.bindSimple(radius);
	binder.bindSimple(normal);
	binder.bindSimple(color);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Lines", infoLines);

}

void VBOCircleVC::bindableValueChanged(void* ptr) {
	generate();
}

bool VBOCircleVC::initAfterDeserialization() {
	return generate();
}
