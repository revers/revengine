/*
 * RevVBOBiquadraticSurfaceVNT.h
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#ifndef REVVBOBIQUADRATICSURFACEVNT_H_
#define REVVBOBIQUADRATICSURFACEVNT_H_

#include <glm/glm.hpp>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/parametric/RevISurfacePatch.h>

namespace rev {

	class VBOBiquadraticSurfaceVNT: public IVBODrawable, public ISurfacePatch {
	public:
		enum class BasisMatrix {
			BEZIER, LAGRANGE
		};

	private:
		static const int HANDLE_SIZE = 4;
		static const int HANDLE_VERTEX = 0;
		static const int HANDLE_NORMAL = 1;
		static const int HANDLE_TEXTURE = 2;
		static const int HANDLE_ELEMENT = 3;
		unsigned int handle[HANDLE_SIZE];
		int faces;
		int basisMatrix = 0;
		glm::vec3 p00 = glm::vec3(-0.5, 0.0, -0.5);
		glm::vec3 p01 = glm::vec3(-0.5, 0.0, 0.0);
		glm::vec3 p02 = glm::vec3(-0.5, 0.0, 0.5);
		glm::vec3 p10 = glm::vec3(0.0, 0.0, -0.5);
		glm::vec3 p11 = glm::vec3(0.0, 0.0, 0.0);
		glm::vec3 p12 = glm::vec3(0.0, 0.0, 0.5);
		glm::vec3 p20 = glm::vec3(0.5, 0.0, -0.5);
		glm::vec3 p21 = glm::vec3(0.5, 0.0, 0.0);
		glm::vec3 p22 = glm::vec3(0.5, 0.0, 0.5);
		int uTessellation = 10;
		int wTessellation = 10;

		int infoVertices = 0;
		int infoTriangles = 0;

		VBOBiquadraticSurfaceVNT();

	public:

		DECLARE_BINDABLE(VBOBiquadraticSurfaceVNT)

		VBOBiquadraticSurfaceVNT(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				int uTessellation,
				int wTessellation);

		VBOBiquadraticSurfaceVNT(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				int tesselation);

		~VBOBiquadraticSurfaceVNT();

		void render() override;
		void bindableValueChanged(void* ptr) override;

		int getPrimitiveCount() override {
			return infoTriangles;
		}
		int getUPointsCount() const override {
			return 3;
		}
		int getVPointsCount() const override {
			return 3;
		}
		glm::vec3& getPoint(int u, int v) override {
			glm::vec3* points = &p00;
			return points[3 * v + u];
		}
		void recalculateSurface() override {
			generate();
		}

	private:
		bool generate();

		void create(
				BasisMatrix basisMatrix,
				const glm::vec3& p00,
				const glm::vec3& p01,
				const glm::vec3& p02,
				const glm::vec3& p10,
				const glm::vec3& p11,
				const glm::vec3& p12,
				const glm::vec3& p20,
				const glm::vec3& p21,
				const glm::vec3& p22,
				int uTessellation,
				int wTessellation);

		void initVAO(int contextIndex);

	protected:
		void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
	};

} /* namespace rev */
#endif /* REVVBOBIQUADRATICSURFACEVNT_H_ */
