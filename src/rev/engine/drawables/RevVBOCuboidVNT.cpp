/*
 * RevVBOCuboidVNT.cpp
 *
 *  Created on: 04-12-2012
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "RevVBOCuboidVNT.h"
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOCuboidVNT)

VBOCuboidVNT::VBOCuboidVNT() {
    glGenBuffers(4, handle);
    glAssert;
}

VBOCuboidVNT::VBOCuboidVNT(float length) {
    create(length, length, length);
}

VBOCuboidVNT::VBOCuboidVNT(float xLength, float yLength, float zLength) {
    create(xLength, yLength, zLength);
}

void VBOCuboidVNT::create(float xLength, float yLength, float zLength) {
    this->xLength = xLength;
    this->yLength = yLength;
    this->zLength = zLength;

    glGenBuffers(4, handle);

    bool succ = generate();
    revAssert(succ);
}

void VBOCuboidVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, ((GLubyte *) NULL + (0)));
    glBindVertexArray(0);
}

VBOCuboidVNT::~VBOCuboidVNT() {
    glDeleteBuffers(4, handle);
}

void VBOCuboidVNT::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Vertex normal
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glVertexAttribPointer((GLuint) 2, 2, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(2); // texture coords
    //-------------------------------------------------------------
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    //-------------------------------------------------------------
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOCuboidVNT::generate() {
    float xLength2 = xLength / 2.0f;
    float yLength2 = yLength / 2.0f;
    float zLength2 = zLength / 2.0f;

    float v[24 * 3] = {
            // Front
            -xLength2, -yLength2, zLength2,
            xLength2, -yLength2, zLength2,
            xLength2, yLength2, zLength2,
            -xLength2, yLength2, zLength2,
            // Right
            xLength2, -yLength2, zLength2,
            xLength2, -yLength2, -zLength2,
            xLength2, yLength2, -zLength2,
            xLength2, yLength2, zLength2,
            // Back
            -xLength2, -yLength2, -zLength2,
            -xLength2, yLength2, -zLength2,
            xLength2, yLength2, -zLength2,
            xLength2, -yLength2, -zLength2,
            // Left
            -xLength2, -yLength2, zLength2,
            -xLength2, yLength2, zLength2,
            -xLength2, yLength2, -zLength2,
            -xLength2, -yLength2, -zLength2,
            // Bottom
            -xLength2, -yLength2, zLength2,
            -xLength2, -yLength2, -zLength2,
            xLength2, -yLength2, -zLength2,
            xLength2, -yLength2, zLength2,
            // Top
            -xLength2, yLength2, zLength2,
            xLength2, yLength2, zLength2,
            xLength2, yLength2, -zLength2,
            -xLength2, yLength2, -zLength2
    };

    float n[24 * 3] = {
            // Front
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            // Right
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            // Back
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            // Left
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            // Bottom
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            // Top
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f
    };

    float tex[24 * 2] = {
            // Front
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            // Right
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            // Back
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            // Left
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            // Bottom
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            // Top
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };

    GLuint el[] = {
            0, 1, 2, 0, 2, 3,
            4, 5, 6, 4, 6, 7,
            8, 9, 10, 8, 10, 11,
            12, 13, 14, 12, 14, 15,
            16, 17, 18, 16, 18, 19,
            20, 21, 22, 20, 22, 23
    };

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, 24);
    // boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, 24);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), v, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), n, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, 24 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), el, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

bool VBOCuboidVNT::initAfterDeserialization() {
    return generate();
}

void VBOCuboidVNT::bind(IBinder& binder) {
    binder.bindSimple(xLength);
    binder.bindSimple(yLength);
    binder.bindSimple(zLength);

    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOCuboidVNT::bindableValueChanged(void* ptr) {
    generate();
}
