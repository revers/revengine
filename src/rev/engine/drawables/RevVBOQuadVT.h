/* 
 * File:   RevVBOQuadVT.h
 * Author: Revers
 *
 * Created on 3 czerwiec 2012, 13:03
 */

#ifndef REVVBOQUADVT_H
#define	REVVBOQUADVT_H

#include <rev/engine/drawables/RevIVBODrawable.h>

namespace rev {

    class VBOQuadVT: public IVBODrawable {
        unsigned int handle[2];

        float xMin = -1;
        float xMax = 1;
        float yMin = -1;
        float yMax = 1;

        int infoVertices = 6;
        int infoTriangles = 2;

        VBOQuadVT();

    public:
        DECLARE_BINDABLE(VBOQuadVT)

        VBOQuadVT(float length);
        VBOQuadVT(float xMin, float xMax, float yMin, float yMax);

        ~VBOQuadVT();

        void render() override;
        void bindableValueChanged(void* ptr) override;

        int getPrimitiveCount() override {
            return infoTriangles;
        }

    private:
        bool generate();
        void create(float xMin, float xMax,
                float yMin, float yMax);
        void initVAO(int contextIndex);

    protected:
        void bind(IBinder& binder) override;
		bool initAfterDeserialization() override;
    };

}
#endif	/* REVVBOQUADVT_H */

