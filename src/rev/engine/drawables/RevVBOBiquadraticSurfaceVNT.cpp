/*
 * RevVBOBiquadraticSurfaceVNT.cpp
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/parametric/RevBiquadraticSurface.h>
#include <rev/common/RevAssert.h>

#include "RevVBOBiquadraticSurfaceVNT.h"

using namespace rev;

IMPLEMENT_BINDABLE(VBOBiquadraticSurfaceVNT)

VBOBiquadraticSurfaceVNT::VBOBiquadraticSurfaceVNT() {
	glGenBuffers(HANDLE_SIZE, handle);
	glAssert;
}

VBOBiquadraticSurfaceVNT::VBOBiquadraticSurfaceVNT(
		BasisMatrix basisMatrix,
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p02,
		const glm::vec3& p10,
		const glm::vec3& p11,
		const glm::vec3& p12,
		const glm::vec3& p20,
		const glm::vec3& p21,
		const glm::vec3& p22,
		int uTessellation,
		int wTessellation) {
	create(basisMatrix, p00, p01, p02, p10, p11, p12, p20,
			p21, p22, uTessellation, wTessellation);
}

VBOBiquadraticSurfaceVNT::VBOBiquadraticSurfaceVNT(
		BasisMatrix basisMatrix,
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p02,
		const glm::vec3& p10,
		const glm::vec3& p11,
		const glm::vec3& p12,
		const glm::vec3& p20,
		const glm::vec3& p21,
		const glm::vec3& p22,
		int tesselation) {
	create(basisMatrix, p00, p01, p02, p10, p11, p12, p20,
			p21, p22, tesselation, tesselation);
}

void VBOBiquadraticSurfaceVNT::create(
		BasisMatrix basisMatrix,
		const glm::vec3& p00,
		const glm::vec3& p01,
		const glm::vec3& p02,
		const glm::vec3& p10,
		const glm::vec3& p11,
		const glm::vec3& p12,
		const glm::vec3& p20,
		const glm::vec3& p21,
		const glm::vec3& p22,
		int uTessellation,
		int wTessellation) {
	this->basisMatrix = (int) basisMatrix;
	this->p00 = p00;
	this->p01 = p01;
	this->p02 = p02;
	this->p10 = p10;
	this->p11 = p11;
	this->p12 = p12;
	this->p20 = p20;
	this->p21 = p21;
	this->p22 = p22;
	this->uTessellation = uTessellation;
	this->wTessellation = wTessellation;

	glGenBuffers(HANDLE_SIZE, handle);

	bool succ = generate();
	revAssert(succ);
}

void VBOBiquadraticSurfaceVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glDrawElements(GL_TRIANGLES, 6 * faces, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	glAssert;
	glBindVertexArray(0);
}

VBOBiquadraticSurfaceVNT::~VBOBiquadraticSurfaceVNT() {
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOBiquadraticSurfaceVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glVertexAttribPointer((GLuint) HANDLE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_TEXTURE); // Texture coords
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOBiquadraticSurfaceVNT::generate() {
	using namespace glm;

	faces = uTessellation * wTessellation;

	int size = (uTessellation + 1) * (wTessellation + 1);

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];
	vec2* tex = new vec2[size];
	int elementSize = 6 * uTessellation * wTessellation;

	infoVertices = size;
	infoTriangles = elementSize / 3;

	unsigned int* el = new unsigned int[elementSize];

	typedef rev::BiquadraticSurfaced::scalar_t scalar_t;
	typedef rev::BiquadraticSurfaced::vec3_t vec3_t;

	const scalar_t (*basis)[3] = nullptr;
	if (basisMatrix == (int) BasisMatrix::LAGRANGE) {
		basis = rev::BiquadraticSurfaced::LAGRANGE_BASIS;
	} else {
		basis = rev::BiquadraticSurfaced::BEZIER_BASIS;
	}

	rev::BiquadraticSurfaced surface(basis,
			(vec3_t(p00)), (vec3_t(p01)), (vec3_t(p02)),
			(vec3_t(p10)), (vec3_t(p11)), (vec3_t(p12)),
			(vec3_t(p20)), (vec3_t(p21)), (vec3_t(p22)));

	double iFactor = 1.0f / wTessellation;
	double jFactor = 1.0f / uTessellation;
	int index = 0;
	for (int i = 0; i <= wTessellation; i++) {
		double w = iFactor * i;
		for (int j = 0; j <= uTessellation; j++) {
			double u = jFactor * j;
			v[index] = vec3(surface.eval(u, w));
			n[index] = vec3(surface.normal(u, w));
			tex[index] = vec2((float) u, (float) w);
			index++;
		}
	}

	unsigned int rowStart, nextRowStart;
	int idx = 0;
	for (int i = 0; i < wTessellation; i++) {
		rowStart = i * (uTessellation + 1);
		nextRowStart = (i + 1) * (uTessellation + 1);
		for (int j = 0; j < uTessellation; j++) {
			el[idx] = rowStart + j;
			el[idx + 1] = nextRowStart + j;
			el[idx + 2] = nextRowStart + j + 1;
			el[idx + 3] = rowStart + j;
			el[idx + 4] = nextRowStart + j + 1;
			el[idx + 5] = rowStart + j + 1;
			idx += 6;
		}
	}

	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, size);
	boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, size);
//	for (int i = 0; i < size; i++) {
//		v[i] -= boundingSphere.center;
//	}
//	boundingSphere.center = vec3(0.0f);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
			GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_TEXTURE]);
	glBufferData(GL_ARRAY_BUFFER, 2 * size * sizeof(float),
			tex, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementSize * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] v;
	delete[] n;
	delete[] tex;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOBiquadraticSurfaceVNT::bind(IBinder& binder) {
	binder.bindSimple(uTessellation);
	binder.bindSimple(wTessellation);
	binder.bindSimple(p00);
	binder.bindSimple(p01);
	binder.bindSimple(p02);
	binder.bindSimple(p10);
	binder.bindSimple(p11);
	binder.bindSimple(p12);
	binder.bindSimple(p20);
	binder.bindSimple(p21);
	binder.bindSimple(p22);

	binder.bind(this, "BasisMatrixType", MEMBER_WRAPPER(basisMatrix), false, true, true);

	binder.bindInfo(this, "Vertices", infoVertices);
	binder.bindInfo(this, "Triangles", infoTriangles);
}

void VBOBiquadraticSurfaceVNT::bindableValueChanged(void* ptr) {
	generate();
	if (pointChangedCallback) {
		pointChangedCallback();
	}
}

bool VBOBiquadraticSurfaceVNT::initAfterDeserialization() {
	return generate();
}
