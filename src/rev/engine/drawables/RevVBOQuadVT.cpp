#include <GL/glew.h>
#include <GL/gl.h>
#include "RevVBOQuadVT.h"
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/common/RevAssert.h>

using namespace rev;

IMPLEMENT_BINDABLE(VBOQuadVT)

VBOQuadVT::VBOQuadVT() {
    glGenBuffers(2, handle);
    glAssert;
}

VBOQuadVT::VBOQuadVT(float length) {
    create(-length, length, -length, length);
}

VBOQuadVT::VBOQuadVT(float xMin, float xMax, float yMin, float yMax) {
    create(xMin, xMax, yMin, yMax);
}

void VBOQuadVT::create(
        float xMin,
        float xMax,
        float yMin,
        float yMax) {
    this->xMin = xMin;
    this->xMax = xMax;
    this->yMin = yMin;
    this->yMax = yMax;

    glGenBuffers(2, handle);

    bool succ = generate();
    revAssert(succ);

}

VBOQuadVT::~VBOQuadVT() {
    glDeleteBuffers(2, handle);
}

void VBOQuadVT::bindableValueChanged(void* ptr) {
    generate();
}

void VBOQuadVT::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    vaoInited[contextIndex] = true;
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 2, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Texture coordinates
    //-------------------------------------------------------------
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOQuadVT::generate() {
    GLfloat verts[] = {
            xMin, yMin, 0.0f,
            xMax, yMin, 0.0f,
            xMax, yMax, 0.0f,
            xMin, yMin, 0.0f,
            xMax, yMax, 0.0f,
            xMin, yMax, 0.0f
    };
    GLfloat tc[] = {
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };

    boundingSphere = rev::Sphere::smallBall((glm::vec3*) verts, 6);
    // boundingSphere = rev::Sphere::miniBall((glm::vec3*) verts, 6);

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof(float), verts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof(float), tc, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

#if REV_ENGINE_MAX_CONTEXTS > 1
    resetVAOHAndles();
#else
    initVAO(0);
#endif

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOQuadVT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
    int index = GLContextManager::ref().getCurrentContextIndex();
    if (!vaoInited[index]) {
        // lazy initialization
        initVAO(index);
    }
    glBindVertexArray(vaoHandle[index]);
#else
    glBindVertexArray(vaoHandle[0]);
#endif

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void VBOQuadVT::bind(IBinder& binder) {
    binder.bindSimple(xMin);
    binder.bindSimple(xMax);
    binder.bindSimple(yMin);
    binder.bindSimple(yMax);
    binder.bindInfo(this, "Vertices", infoVertices);
    binder.bindInfo(this, "Triangles", infoTriangles);
}

bool VBOQuadVT::initAfterDeserialization() {
    return generate();
}
