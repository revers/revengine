/*
 * RevIVBODrawable.cpp
 *
 *  Created on: 02-02-2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevIVBODrawable.h"

using namespace rev;

IVBODrawable::~IVBODrawable() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	GLContextManager::ref().scheduleVAOsToDestroy(vaoHandle);
#else
	glDeleteVertexArrays(1, &vaoHandle[0]);
#endif
}

