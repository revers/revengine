#ifndef REVIVBODRAWABLE_H
#define REVIVBODRAWABLE_H

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/binding/RevIBindable.h>
#include "RevRenderPrimitiveType.h"

namespace rev {

class IVBODrawable: public IBindable {
protected:
	rev::BoundingSphere boundingSphere;

	unsigned int vaoHandle[REV_ENGINE_MAX_CONTEXTS];
	bool vaoInited[REV_ENGINE_MAX_CONTEXTS];

	IVBODrawable() {
		clearVAOHandles();
	}

public:
	virtual ~IVBODrawable();

	virtual void render() = 0;

	const rev::BoundingSphere* getBoundingSphere() const {
		return &boundingSphere;
	}

	virtual int getPrimitiveCount() = 0;

	/**
	 * IMPORTANT: override this if your drawable uses
	 * different render primitives.
	 */
	virtual RenderPrimitiveType getRenderPrimitiveType() const {
		return RenderPrimitiveType::TRIANGLES;
	}

	unsigned int getVAOHandle(int contextIndex = 0) {
		return vaoHandle[contextIndex];
	}

protected:
	void clearVAOHandles();
	void resetVAOHAndles();
};

inline void IVBODrawable::clearVAOHandles() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		vaoHandle[i] = 0;
		vaoInited[i] = false;
	}
}

inline void IVBODrawable::resetVAOHAndles() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		vaoInited[i] = false;
	}
}

}
#endif // REVIVBODRAWABLE_H
