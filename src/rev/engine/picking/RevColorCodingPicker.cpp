/*
 * RevColorCodingPicker.cpp
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#include <stdio.h>
#include <GL/glew.h>
#include <rev/common/RevDelete.hpp>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/camera/RevICamera.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/drawables/RevVBOSphereVN.h>
#include <rev/engine/drawables/RevVBOAxesVC.h>
#include <rev/engine/drawables/RevVBOArrowVN.h>
#include <rev/engine/effects/RevIEffect.h>
#include <rev/engine/effects/RevStencilOutlineEffect.h>
#include <rev/engine/effects/RevFlatColorEffect.h>
#include <rev/engine/effects/RevDebugEffect.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/events/RevEventManager.h>
#include "RevColorCodingPicker.h"

#include <rev/engine/glcontext/RevGLContextManager.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE_SINGLETON_FULL(ColorCodingPicker, &Renderer3D::ref().getPicker(), nullptr)
;

ColorCodingPicker::ColorCodingPicker() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		fbos[i] = 0;
		pickingTextures[i] = 0;
		depthTextures[i] = 0;
		widths[i] = 0;
		heights[i] = 0;
	}
}

ColorCodingPicker::~ColorCodingPicker() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		deleteFrameBuffer(i);
	}
	DEL(stencilOutlineEffect);
	DEL(colorCodingEffect);
	DEL(axesComponent);
	DEL(sphereComponent);
	DEL(arrowComponent);
}

void ColorCodingPicker::deleteFrameBuffer(int contextIndex) {
	if (fbos[contextIndex] != 0) {
		glDeleteFramebuffers(1, &fbos[contextIndex]);
		fbos[contextIndex] = 0;
	}

	if (pickingTextures[contextIndex] != 0) {
		glDeleteTextures(1, &pickingTextures[contextIndex]);
		pickingTextures[contextIndex] = 0;
	}

	if (depthTextures[contextIndex] != 0) {
		glDeleteTextures(1, &depthTextures[contextIndex]);
		depthTextures[contextIndex] = 0;
	}
}

bool ColorCodingPicker::init(int windowWidth, int windowHeight) {
	//REV_TRACE_FUNCTION;
	int contextIndex = rev::GLContextManager::ref().getCurrentContextIndex();

	REV_TRACE_MSG(rev::a2s(R(contextIndex), R(windowWidth), R(windowHeight)));

	if (windowWidth == 0 || windowHeight == 0
			|| (windowWidth == widths[contextIndex]
					&& windowHeight == heights[contextIndex])) {
		return true;
	}
	widths[contextIndex] = windowWidth;
	heights[contextIndex] = windowHeight;

	if (!colorCodingEffect) {
		colorCodingEffect = new rev::ColorCodingEffect();
	}

	if (!stencilOutlineEffect) {
		stencilOutlineEffect = new rev::StencilOutlineEffect();
	}

	if (!sphereComponent) {
		IEffect* effect = new rev::DebugEffect();
		IVBODrawable* drawable = new rev::VBOSphereVN(1.0, 20);
		sphereComponent = new rev::Simple3DComponent(effect,
				true, drawable, true);
		sphereComponent->setWireframe(true);
	}

	if (!axesComponent) {
		IEffect* effect = new rev::FlatColorEffect();
		IVBODrawable* drawable = new rev::VBOAxesVC(25.0f,
				color3(0, 1, 1), color3(1, 0, 1), color3(1, 1, 0));
		axesComponent = new rev::Simple3DComponent(effect,
				true, drawable, true);
	}

	if (!arrowComponent) {
		IEffect* effect = new rev::DebugEffect();
		IVBODrawable* drawable = new rev::VBOArrowVN(0.125 * 3, 1.25 * 3, 40);
		arrowComponent = new rev::Simple3DComponent(effect,
				true, drawable, true);
	}

	deleteFrameBuffer(contextIndex);

	// Create the FBO
	glGenFramebuffers(1, &fbos[contextIndex]);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbos[contextIndex]);

	// Create the texture object for the primitive information buffer
	glGenTextures(1, &pickingTextures[contextIndex]);
	glBindTexture(GL_TEXTURE_2D, pickingTextures[contextIndex]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, windowWidth, windowHeight, 0,
			GL_RGBA, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_2D,
			pickingTextures[contextIndex], 0);
	glAssert;

	// Create the texture object for the depth buffer
	glGenTextures(1, &depthTextures[contextIndex]);
	glBindTexture(GL_TEXTURE_2D, depthTextures[contextIndex]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, windowWidth, windowHeight, 0,
			GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
			depthTextures[contextIndex], 0);
	glAssert;

	// Verify that the FBO is correct
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE) {
		REV_ERROR_MSG("FB error, status: " << status);
		return false;
	}

	// Restore the default framebuffer
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glAssert;

	return (glGetError() == GL_NO_ERROR);
}

void ColorCodingPicker::enable() {
	int contextIndex = rev::GLContextManager::ref().getCurrentContextIndex();

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbos[contextIndex]);
}

void ColorCodingPicker::disable() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

PixelInfo ColorCodingPicker::readPixel(int x, int y) const {
	int contextIndex = rev::GLContextManager::ref().getCurrentContextIndex();

	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbos[contextIndex]);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	PixelInfo pixel;
	glReadPixels(x, y, 1, 1, GL_RGBA, GL_FLOAT, &pixel);

	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	return pixel;
}

/**
 * From http://en.wikibooks.org/wiki/OpenGL_Programming/Object_selection
 */
void ColorCodingPicker::showUnprojectedCoords(int x, int y) {
	char color[4];
	GLfloat depth;
	//GLuint index;

	glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, color);
	glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
	//glReadPixels(x, height - y - 1, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &index);

	int contextIndex = rev::GLContextManager::ref().getCurrentContextIndex();

	glm::vec4 viewport = glm::vec4(0, 0, widths[contextIndex],
			heights[contextIndex]);
	glm::vec3 wincoord = glm::vec3(x, y, depth);
	ICamera* camera = CameraManager::ref().getCamera();
	glm::vec3 objcoord = glm::unProject(
			wincoord, camera->getViewMatrix(),
			camera->getProjectionMatrix(),
			viewport);

	typedef unsigned int uint;

	uint r = 0xff & color[0];
	uint g = 0xff & color[1];
	uint b = 0xff & color[2];
	uint a = 0xff & color[3];

	if (depth == 1.0f) {
		REV_TRACE_MSG("Clicked on pixel (" << rev::StringUtil::format(
				"(%d, %d), Color(%d, %d, %d, %d), depth: %f", x, y,
				r, g, b, a, depth)
				<< "\nClicked on Background!");
	} else {
		REV_TRACE_MSG("Clicked on pixel (" << rev::StringUtil::format(
				"(%d, %d), Color(%d, %d, %d, %d), depth: %f"
				"\nCoordinates in object space: %f, %f, %f",
				x, y, r, g, b, a, depth, objcoord.x, objcoord.y, objcoord.z));
	}
}

void ColorCodingPicker::renderBoundingSphere(const rev::BoundingSphere* bs,
		const rev::ICamera& camera, rev::IVisual3DComponent& c) {
	mat4 m = c.getParent()->getModelMatrix() * glm::translate(bs->c)
			* glm::scale(vec3(bs->r));
	sphereComponent->applyEffect(m, camera.getViewMatrix(),
			camera.getProjectionMatrix(), camera);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	sphereComponent->render();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ColorCodingPicker::renderAxes(const rev::ICamera& camera,
		rev::IVisual3DComponent& c) {
	mat4 m = glm::translate(c.getParent()->getPosition())
			* glm::mat4_cast(c.getParent()->getOrientation());

	axesComponent->applyEffect(m, camera.getViewMatrix(),
			camera.getProjectionMatrix(), camera);
	axesComponent->render();
}

//void ColorCodingPicker::renderLightDirection(
//		const rev::BoundingSphere* bs,
//		const rev::ICamera& camera,
//		rev::IVisual3DComponent& c) {
//
//	vec3 lightDir = Renderer3D::ref().getDirectionalLight().getDirection();
//	lightDir = glm::normalize(lightDir);
//
//	float angle = glm::degrees(
//			acos(std::min(1.0f, glm::dot(MathHelper::POSITIVE_Y, lightDir))));
//
//	glm::vec3 axis = glm::cross(MathHelper::POSITIVE_Y, lightDir);
//	glm::mat4 rotation = glm::rotate(angle, axis);
//
//	GameObject& p = *c.getParent();
//	mat4 m = glm::translate(p.getPosition() + bs->c
//			+ (bs->r + 2.0f) * p.getScaling() * lightDir) * rotation;
//
//	arrowComponent->applyEffect(m, camera.getViewMatrix(),
//			camera.getProjectionMatrix(), camera);
//	arrowComponent->render();
//}

void ColorCodingPicker::renderPickedComponent(
		rev::IVisual3DComponent& c,
		const rev::ICamera& camera, bool drawDebugElements) {

	const rev::BoundingSphere* bs = c.getBoundingSphere();

	if (drawOutline && c.isCanDrawOutline()) {
		glEnable(GL_STENCIL_TEST);
		// Set the stencil buffer to write a 1 in every time
		// a pixel is written to the screen
		glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

		// draw stencil pattern
		glClear(GL_STENCIL_BUFFER_BIT);

		c.applyEffect(c.getParent()->getModelMatrix(),
				camera.getViewMatrix(), camera.getProjectionMatrix(),
				camera);
		c.render();

		// Set the stencil buffer to only allow writing
		// to the screen when the value of the
		// stencil buffer is not 1
		glStencilFunc(GL_NOTEQUAL, 1, 0xFF);

		mat4 t1 = glm::translate(-bs->c);
		mat4 t2 = glm::translate(bs->c);
		mat4 s = glm::scale(vec3(1.05));
		stencilOutlineEffect->setScaleMatrix(t2 * s * t1);

		stencilOutlineEffect->apply(c.getParent()->getModelMatrix(),
				camera.getViewMatrix(), camera.getProjectionMatrix(),
				camera);

		c.render();

		glDisable(GL_STENCIL_TEST);
	} else {
		c.applyEffect(c.getParent()->getModelMatrix(),
				camera.getViewMatrix(), camera.getProjectionMatrix(),
				camera);
		c.render();
	}

	glDisable(GL_CULL_FACE);

	if (drawDebugElements && drawAxes) {
		renderAxes(camera, c);
	}

	if (drawDebugElements && drawBoundingSphere) {
		renderBoundingSphere(bs, camera, c);
	}

//	if (drawDebugElements && drawLightDirection) {
//		renderLightDirection(bs, camera, c);
//	}
}

void ColorCodingPicker::click(int x, int y, bool invertY/* = true*/) {
	REV_TRACE_MSG("x = " << x << "; y = " << y);

	int contextIndex = rev::GLContextManager::ref().getCurrentContextIndex();

	if (invertY) {
		lastPixelInfo = readPixel(x, heights[contextIndex] - y);
		// TODO: remove me
		showUnprojectedCoords(x, heights[contextIndex] - y);
	} else {
		lastPixelInfo = readPixel(x, y);
		// TODO: remove me
		showUnprojectedCoords(x, y);
	}

	if (!lastPixelInfo.isEmpty()) {
		GameObject* go = Engine::ref().getGameObject(
				(int) lastPixelInfo.getObjectId());

		alwaysAssertMsg(go != nullptr, "Cannot find GameObject with id "
				<< ((int) lastPixelInfo.getObjectId()));

		EventManager::ref().fireObjectPickedEvent(go, lastPixelInfo);
	} else {
		lastPixelInfo.reset();
		EventManager::ref().firePickingResetedEvent();
	}
}

void ColorCodingPicker::pickGameObject(GameObject* obj) {
	int drawId = rev::Renderer3D::ref().getGameObjectDrawId(obj);

	REV_TRACE_MSG(
			"trying to pick game object: " << obj->getId() << "; drawId = " << drawId);

	lastPixelInfo = PixelInfo(obj->getId(), drawId);
	EventManager::ref().fireObjectPickedEvent(obj, lastPixelInfo);
}

GameObject* ColorCodingPicker::getPickedGameObject() {
	int id = (int) lastPixelInfo.getObjectId();
	return rev::Engine::ref().getGameObject(id);
}

void ColorCodingPicker::reset() {
	lastPixelInfo.reset();
	EventManager::ref().firePickingResetedEvent();
}

void ColorCodingPicker::bind(IBinder& binder) {
	binder.bindSimple(drawAxes);
	binder.bindSimple(drawOutline);
	//binder.bindSimple(drawLightDirection);
	binder.bindSimple(drawBoundingSphere);
}
