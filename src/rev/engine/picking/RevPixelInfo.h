/*
 * RevPixelInfo.h
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#ifndef REVPIXELINFO_H_
#define REVPIXELINFO_H_

namespace rev {
    class PixelInfo {
    private:
        float objectID;
        float drawID;
        float primID;
        float alphaID;

    public:
        PixelInfo(int objectId, int drawId) {
            objectID = (float) objectId;
            drawID = (float) drawId;
            primID = 0.0;
            alphaID = 1;
        }

        PixelInfo() {
            reset();
        }

        void reset() {
            objectID = 0.0;
            drawID = 0.0;
            primID = 0.0;
            alphaID = 0.0;
        }

        bool isEmpty() const {
            return (alphaID == 0);
        }

        float getDrawId() const {
            return drawID;
        }

        float getObjectId() const {
            return objectID;
        }

        float getPrimId() const {
            return primID;
        }
    };
} // namespace rev
#endif /* REVPIXELINFO_H_ */
