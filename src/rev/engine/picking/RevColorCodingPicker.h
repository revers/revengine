/*
 * RevColorCodingPicker.h
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 *
 *  Based on http://ogldev.atspace.co.uk/www/tutorial29/tutorial29.html
 */

#ifndef REVPICKINGTEXTURE_H_
#define REVPICKINGTEXTURE_H_

#include <GL/glew.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include <rev/engine/effects/RevColorCodingEffect.h>
#include <rev/engine/binding/RevIBindable.h>
#include "RevPixelInfo.h"

namespace rev {
    class StencilOutlineEffect;
    class IVisual3DComponent;
    class ICamera;
    class GameObject;
}

namespace rev {

    class ColorCodingPicker: public IBindable {
    private:
        GLuint fbos[REV_ENGINE_MAX_CONTEXTS];
        GLuint pickingTextures[REV_ENGINE_MAX_CONTEXTS];
        GLuint depthTextures[REV_ENGINE_MAX_CONTEXTS];
        int widths[REV_ENGINE_MAX_CONTEXTS];
        int heights[REV_ENGINE_MAX_CONTEXTS];
        rev::StencilOutlineEffect* stencilOutlineEffect = nullptr;
        rev::ColorCodingEffect* colorCodingEffect = nullptr;
        rev::Simple3DComponent* axesComponent = nullptr;
        rev::Simple3DComponent* sphereComponent = nullptr;
        rev::Simple3DComponent* arrowComponent = nullptr;
        PixelInfo lastPixelInfo;

        bool drawAxes = true;
        bool drawOutline = true;
        //bool drawLightDirection = false;
        bool drawBoundingSphere = false;

    public:
        DECLARE_BINDABLE_SINGLETON(ColorCodingPicker)

        ColorCodingPicker();

        ~ColorCodingPicker();

        bool init(int windowWidth, int windowHeight);

        void enable();
        void disable();

        void click(int x, int y, bool invertY = true);

        void reset();

        const PixelInfo& getPixelInfo() const {
            return lastPixelInfo;
        }

        PixelInfo readPixel(int x, int y) const;

        unsigned int getDrawIndex() const {
            return colorCodingEffect->getDrawIndex();
        }

        void setDrawIndex(unsigned int drawIndex) {
            colorCodingEffect->setDrawIndex(drawIndex);
        }

        unsigned int getObjectIndex() const {
            return colorCodingEffect->getObjectIndex();
        }

        void setObjectIndex(unsigned int objectIndex) {
            colorCodingEffect->setObjectIndex(objectIndex);
        }

        void setIndices(unsigned int objectIndex, unsigned int drawIndex) {
            colorCodingEffect->setObjectIndex(objectIndex);
            colorCodingEffect->setDrawIndex(drawIndex);
        }

        void applyPickingEffect(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) {

            colorCodingEffect->apply(modelMatrix, viewMatrix,
                    projectionMatrix, camera);
        }

        void renderPickedComponent(
                rev::IVisual3DComponent& c,
                const rev::ICamera& camera,
                bool drawDebugElements);

        bool isDrawAxes() const {
            return drawAxes;
        }

        void setDrawAxes(bool drawAxes) {
            this->drawAxes = drawAxes;
        }

        bool isDrawBoundingSphere() const {
            return drawBoundingSphere;
        }

        void setDrawBoundingSphere(bool drawBoundingSphere) {
            this->drawBoundingSphere = drawBoundingSphere;
        }

        bool isDrawOutline() const {
            return drawOutline;
        }

        void setDrawOutline(bool drawOutline) {
            this->drawOutline = drawOutline;
        }

//        bool isDrawLightDirection() const {
//            return drawLightDirection;
//        }
//
//        void setDrawLightDirection(bool drawLightDirection) {
//            this->drawLightDirection = drawLightDirection;
//        }

        void pickGameObject(GameObject* obj);
		GameObject* getPickedGameObject();

    private:
        void deleteFrameBuffer(int contextIndex);

        void showUnprojectedCoords(int x, int y);
        void renderBoundingSphere(const rev::BoundingSphere* bs,
                const rev::ICamera& camera, rev::IVisual3DComponent& c);
        void renderAxes(const rev::ICamera& camera, rev::IVisual3DComponent& c);
//        void renderLightDirection(const rev::BoundingSphere* bs,
//                const rev::ICamera& camera, rev::IVisual3DComponent& c);

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVPICKINGTEXTURE_H_ */
