/*
 * RevFreeCamera.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVFREECAMERA_H_
#define REVFREECAMERA_H_

#include <glm/glm.hpp>
#include <rev/engine/math/RevMathHelper.h>
#include "RevICamera.h"

namespace rev {

	class FreeCamera: public ICamera {
		// FIXME: remove this:
		friend class CameraWidget;

		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 up;
		glm::mat4 viewMatrix;
		glm::mat4 rotation;
		float yaw = 0;
		float pitch = 0;
		int lastX = 0;
		int lastY = 0;

	public:
		DECLARE_BINDABLE(FreeCamera)

		FreeCamera() :
				position(0.0f), direction(MathHelper::POSITIVE_Z) {
		}

		FreeCamera(const glm::vec3& position) :
				position(position), direction(MathHelper::POSITIVE_Z) {
		}

		FreeCamera(const glm::vec3& position, const glm::vec3& direction) :
				position(position), direction(direction) {
		}

		virtual ~FreeCamera() {
		}

		CameraType getType() const override {
			return CameraType::FREE;
		}
		const glm::vec3& getPosition() const override {
			return position;
		}
		const glm::vec3& getDirection() const override {
			return direction;
		}
		const glm::vec3& getUp() const override {
			return up;
		}
		const glm::mat4& getViewMatrix() const override {
			return viewMatrix;
		}

		void increaseDistance(float value);

		void setPosition(const glm::vec3& position) {
			this->position = position;
			calcViewMatrix();
		}

		/**
		 * @param direction - normalized direction vector.
		 */
		void setDirection(const glm::vec3& direction);

		void set(const glm::vec3& position, const glm::vec3& direction) {
			this->position = position;
			setDirection(direction);
		}

		void move(float translationX, float translationY,
				float translationZ) {
			move(glm::vec3(translationX, translationY, translationZ));
		}
		void move(const glm::vec3& translation);
		void click(int x, int y);
		void drag(int x, int y);

	private:
		void calcViewMatrix();

	protected:
		void bind(IBinder& binder) override;
		void bindableValueChanged(void* ptr) override;
		void setScreenSizeImpl(float width, float height) override;

	};

} /* namespace rev */
#endif /* REVFREECAMERA_H_ */
