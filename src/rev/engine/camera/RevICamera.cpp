/*
 * RevICamera.cpp
 *
 *  Created on: 28-01-2013
 *      Author: Revers
 */

#include "RevICamera.h"

using namespace rev;
using namespace glm;

void ICamera::recalculateViewFrustum() {
	frustum = ViewFrustum::fromProjectionMatrix(
			getProjectionMatrix() * getViewMatrix());
}

glm::vec3 ICamera::getNearPlanePoint(int x, int y) {
	/*
	 * IMPORTANT: y coordinate is inverted here since
	 * in my current window system y grows from top to bottom,
	 * in contrast to OpenGL's y that grows from bottom to top.
	 *
	 * This might need to be changed in other window systems.
	 * (from "screenHeight - y" to "y").
	 *
	 * z = -1.0 -> Near Plane
	 * z = 1.0 -> Far Plane
	 */
	glm::vec3 winCoord = glm::vec3(x, screenHeight - y, -1.0f);

	mat4 inverse = glm::inverse(getProjectionMatrix() * getViewMatrix());

	vec4 tmp = vec4(winCoord, 1.0f);
	tmp.x /= float(screenWidth);
	tmp.y /= float(screenHeight);
	tmp = tmp * 2.0f - 1.0f;

	vec4 obj = inverse * tmp;
	obj /= obj.w;

	return vec3(obj);
}

Ray ICamera::getRay(int x, int y) {
	return Ray::fromTwoPoints(getPosition(), getNearPlanePoint(x, y));
}
