/*
 * RevSphericalCamera.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVSPHERECAMERA_H_
#define REVSPHERECAMERA_H_

#include "RevICamera.h"

namespace rev {

	class SphericalCamera: public ICamera {
	private:
		const glm::vec4 UP;
		const glm::vec4 FORWARD;
		const glm::vec4 SIDE;
		const glm::vec3 FORWARD_3D;

		glm::mat4 rotMatrix;
		glm::mat4 lastRotMatrix;

		glm::vec3 stVec;
		glm::vec3 enVec;

		glm::vec3 position;
		glm::vec3 target;
		glm::vec3 forward;
		glm::vec3 side;
		glm::vec3 up;
		glm::vec3 direction;
		float cameraDistance;

		float adjustWidth;
		float adjustHeight;

		glm::mat4 viewMatrix;

	public:
		DECLARE_BINDABLE(SphericalCamera)

		SphericalCamera();

		SphericalCamera(const glm::vec3& position_,
				const glm::vec3& target_,
				float windowWidth,
				float windowHeight);

		void setPositionAndTarget(const glm::vec3& position,
				const glm::vec3& target);
		void setPosition(const glm::vec3& position);
		void setTarget(const glm::vec3& target);
		void setCameraDistance(float cameraDistance);

		glm::vec3 getTarget() const {
			return target;
		}

		float getCameraDistance() const {
			return cameraDistance;
		}

		CameraType getType() const override {
			return CameraType::SPHERICAL;
		}
		const glm::vec3& getPosition() const override {
			return position;
		}
		const glm::vec3& getDirection() const override {
			return direction;
		}
		const glm::vec3& getUp() const override {
			return up;
		}
		const glm::mat4& getViewMatrix() const override {
			return viewMatrix;
		}

		const glm::mat4& getRotationMatrix() const {
			return rotMatrix;
		}

		void increaseDistance(float val) {
			float temp = cameraDistance + val;
			setCameraDistance(temp > 0 ? temp : 0.1f);
		}

		void click(int x, int y);
		void drag(int x, int y);

		void setProjectionMatrix(const glm::mat4& m) {
			projectionMatrix = m;
		}

	private:
		void calcMatricesFromPosAndTarget();
		void mapToSphere(const glm::vec2* newPt, glm::vec3* newVec) const;
		void bindableValueChanged(void* ptr) override;

	protected:
		void bind(IBinder& binder) override;
		void setScreenSizeImpl(float newWidth, float newHeight) override;

	};

}
#endif /* REVSPHERECAMERA_H_ */
