/*
 * RevCameraTransitionAnimator.h
 *
 *  Created on: 08-04-2013
 *      Author: Revers
 */

#ifndef REVCAMERATRANSITIONANIMATOR_H_
#define REVCAMERATRANSITIONANIMATOR_H_

#include "RevICamera.h"

namespace rev {

	class CameraTransitionAnimator: public ICamera {
		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 up;

		glm::vec3 oldPosition;
		glm::vec3 oldDirection;
		glm::vec3 oldUp;

		glm::mat4 viewMatrix;
		float elapsedTime = 0.0f;
		float duration = 0.0f;
		int contextIndex = 0;
		ICamera* endCamera = nullptr;

	public:
		DECLARE_BINDABLE(CameraTransitionAnimator)

		CameraTransitionAnimator();
		virtual ~CameraTransitionAnimator();

		CameraType getType() const override {
			return CameraType::TRANSITIONAL;
		}
		const glm::vec3& getPosition() const override {
			return position;
		}
		const glm::vec3& getDirection() const override {
			return direction;
		}
		const glm::vec3& getUp() const override {
			return up;
		}
		const glm::mat4& getViewMatrix() const override {
			return viewMatrix;
		}

		void start(ICamera* beginCamera, ICamera* endCamera, float duration, int contextIndex);
		void update(float frameTimeMs);

	protected:
		void bind(IBinder& binder) override {
		}
		void setScreenSizeImpl(float width, float height) override {
		}
	};

} /* namespace rev */
#endif /* REVCAMERATRANSITIONANIMATOR_H_ */
