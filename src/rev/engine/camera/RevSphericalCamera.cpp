/*
 * RevSphericalCamera.cpp
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <rev/common/RevAssert.h>
#include <rev/engine/math/RevMathHelper.h>
#include "RevSphericalCamera.h"

#define FUNC_SQRT sqrtf

using namespace rev;

IMPLEMENT_BINDABLE(SphericalCamera)

SphericalCamera::SphericalCamera() :
		UP(0.0f, 1.0f, 0.0f, 1.0f),
				FORWARD(0.0f, 0.0f, 1.0f, 1.0f),
				FORWARD_3D(0.0f, 0.0f, 1.0f),
				SIDE(1.0f, 0.0f, 0.0f, 1.0f),
				rotMatrix(1.0f),
				lastRotMatrix(0.0f),
				position(10),
				target(0),
				viewMatrix(1.0f) {

	forward = position - target;
	cameraDistance = glm::length(forward);
	forward /= cameraDistance;
	direction = -forward;

	calcMatricesFromPosAndTarget();
}

SphericalCamera::SphericalCamera(const glm::vec3& position_,
		const glm::vec3& target_,
		float windowWidth,
		float windowHeight) :
		UP(0.0f, 1.0f, 0.0f, 1.0f),
				FORWARD(0.0f, 0.0f, 1.0f, 1.0f),
				FORWARD_3D(0.0f, 0.0f, 1.0f),
				SIDE(1.0f, 0.0f, 0.0f, 1.0f),
				rotMatrix(1.0f),
				lastRotMatrix(0.0f),
				position(position_),
				target(target_),
				viewMatrix(1.0f) {
	setScreenSize(windowWidth, windowHeight);

	forward = position - target;
	cameraDistance = glm::length(forward);
	forward /= cameraDistance;
	direction = -forward;

	calcMatricesFromPosAndTarget();
}

void SphericalCamera::click(int x, int y) {
	glm::vec2 pt((float) x, (float) y);

	lastRotMatrix = rotMatrix;

	mapToSphere(&pt, &stVec);
}

void SphericalCamera::drag(int x, int y) {
	glm::vec2 pt((float) x, (float) y);
	mapToSphere(&pt, &enVec);

	float angle = glm::degrees(acos(std::min(1.0f, glm::dot(stVec, enVec))));

	glm::vec3 axis = glm::cross(enVec, stVec);
	glm::mat4 rotation = glm::rotate(angle, axis);

	rotMatrix = lastRotMatrix * rotation;

	forward = glm::vec3(rotMatrix * FORWARD);
	direction = -forward;
	side = glm::vec3(rotMatrix * SIDE);
	up = glm::vec3(rotMatrix * UP);

	position = target + forward * cameraDistance;

	viewMatrix[0][0] = side[0];
	viewMatrix[1][0] = side[1];
	viewMatrix[2][0] = side[2];
	viewMatrix[3][0] = 0;

	viewMatrix[0][1] = up[0];
	viewMatrix[1][1] = up[1];
	viewMatrix[2][1] = up[2];
	viewMatrix[3][1] = 0;

	viewMatrix[0][2] = forward[0];
	viewMatrix[1][2] = forward[1];
	viewMatrix[2][2] = forward[2];
	viewMatrix[3][2] = 0;

	viewMatrix[3][0] = -glm::dot(side, position);
	viewMatrix[3][1] = -glm::dot(up, position);;
	viewMatrix[3][2] = -glm::dot(forward, position);;
	viewMatrix[3][3] = 1;
}

void SphericalCamera::calcMatricesFromPosAndTarget() {
	float angle = glm::degrees(acos(std::min(1.0f, glm::dot(FORWARD_3D, forward))));
	glm::vec3 axis(0, 1, 0);

	if (angle != 0.0f && angle != 180.0f) {
		axis = glm::cross(FORWARD_3D, forward);
	}
	rotMatrix = glm::rotate(angle, axis);

	side = glm::vec3(rotMatrix * SIDE);
	up = glm::vec3(rotMatrix * UP);

	viewMatrix[0][0] = side[0];
	viewMatrix[1][0] = side[1];
	viewMatrix[2][0] = side[2];
	viewMatrix[3][0] = 0;

	viewMatrix[0][1] = up[0];
	viewMatrix[1][1] = up[1];
	viewMatrix[2][1] = up[2];
	viewMatrix[3][1] = 0;

	viewMatrix[0][2] = forward[0];
	viewMatrix[1][2] = forward[1];
	viewMatrix[2][2] = forward[2];
	viewMatrix[3][2] = 0;

	viewMatrix[3][0] = 0;
	viewMatrix[3][1] = 0;
	viewMatrix[3][2] = 0;
	viewMatrix[3][3] = 1;

	viewMatrix *= glm::translate(-position);
}

void SphericalCamera::mapToSphere(const glm::vec2* newPt, glm::vec3* newVec) const {
	glm::vec2 tempPt;
	GLfloat length;

	//Copy paramter into temp point
	tempPt = *newPt;

	//Adjust point coords and scale down to range of [-1 ... 1]
	tempPt.x = (tempPt.x * this->adjustWidth) - 1.0f;
	tempPt.y = 1.0f - (tempPt.y * this->adjustHeight);

	//Compute the square of the length of the vector to the point from the center
	length = (tempPt.x * tempPt.x) + (tempPt.y * tempPt.y);

	//If the point is mapped outside of the sphere... (length > radius squared)
	if (length > 1.0f) {
		GLfloat norm;

		//Compute a normalizing factor (radius / sqrt(length))
		norm = 1.0f / FUNC_SQRT(length);

		//Return the "normalized" vector, a point on the sphere
		newVec->x = tempPt.x * norm;
		newVec->y = tempPt.y * norm;
		newVec->z = 0.0f;
	} else //Else it's on the inside
	{
		//Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
		newVec->x = tempPt.x;
		newVec->y = tempPt.y;
		newVec->z = FUNC_SQRT(1.0f - length);
	}
}

void SphericalCamera::setPositionAndTarget(const glm::vec3& position,
		const glm::vec3& target) {
	this->position = position;
	this->target = target;

	forward = position - target;
	cameraDistance = glm::length(forward);
	forward /= cameraDistance;
	direction = -forward;

	calcMatricesFromPosAndTarget();
}

void SphericalCamera::setPosition(const glm::vec3& position) {
	this->position = position;

	forward = position - target;
	cameraDistance = glm::length(forward);
	forward /= cameraDistance;
	direction = -forward;

	calcMatricesFromPosAndTarget();
}

void SphericalCamera::setTarget(const glm::vec3& target) {
	this->target = target;

	forward = position - target;
	cameraDistance = glm::length(forward);
	forward /= cameraDistance;
	direction = -forward;

	calcMatricesFromPosAndTarget();
}

void SphericalCamera::setCameraDistance(float cameraDistance) {
	this->cameraDistance = cameraDistance;

	viewMatrix[3][0] = 0;
	viewMatrix[3][1] = 0;
	viewMatrix[3][2] = 0;
	viewMatrix[3][3] = 1;

	position = target + forward * cameraDistance;

	viewMatrix *= glm::translate(-position);
}

void SphericalCamera::setScreenSizeImpl(float newWidth, float newHeight) {
	revAssert((newWidth > 1.0f) && (newHeight > 1.0f));

	//Set adjustment factor for width/height
	this->adjustWidth = 1.0f / ((newWidth - 1.0f) * 0.5f);
	this->adjustHeight = 1.0f / ((newHeight - 1.0f) * 0.5f);
}

void SphericalCamera::bindableValueChanged(void* ptr) {
	if (ptr == (void*) &cameraDistance) {
		setCameraDistance(cameraDistance);
	} else if (ptr == &position) {
		setPosition(position);
	} else if (ptr == &target) {
		setTarget(target);
	}
}

void SphericalCamera::bind(IBinder& binder) {
	binder.bindSimple(cameraDistance);
	binder.bind(this, "target", MEMBER_WRAPPER(target), true, true, true);
	binder.bind(this, "position", MEMBER_WRAPPER(position), true, true, true);
	binder.bindInfo(this, NVP(direction));
}
