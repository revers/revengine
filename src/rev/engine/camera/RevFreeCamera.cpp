/*
 * RevFreeCamera.cpp
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/math/RevMathHelper.h>
#include "RevFreeCamera.h"

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(FreeCamera)

void FreeCamera::move(const glm::vec3& translation) {
	position += vec3(rotation * vec4(translation, 0));
	calcViewMatrix();
}

void FreeCamera::click(int x, int y) {
	lastX = x;
	lastY = y;
}

void FreeCamera::drag(int x, int y) {
	int diffX = x - lastX;
	int diffY = lastY - y;
	lastX = x;
	lastY = y;

	const float factor = 0.006;
	yaw -= diffX * factor;
	pitch -= diffY * factor;
	rotation = glm::yawPitchRoll(yaw, pitch, 0.0f);
	calcViewMatrix();
}

void FreeCamera::setScreenSizeImpl(float width, float height) {
	calcViewMatrix();
}

#define SQR(x) ((x)*(x))

void FreeCamera::setDirection(const glm::vec3& direction) {
	yaw = atan2(direction.x, direction.z);
	pitch = atan2(-direction.y, sqrt(SQR(direction.x) + SQR(direction.z)));
	rotation = glm::yawPitchRoll(yaw, pitch, 0.0f);
	calcViewMatrix();
}

void FreeCamera::calcViewMatrix() {
	direction = vec3(rotation * vec4(MathHelper::POSITIVE_Z, 0));
	up = vec3(rotation * vec4(MathHelper::POSITIVE_Y, 0));
	viewMatrix = glm::lookAt(position, position + direction, up);
}

void FreeCamera::increaseDistance(float value) {
	position -= direction * value;
	calcViewMatrix();
}

void FreeCamera::bindableValueChanged(void* ptr) {
	if (ptr == &yaw) {
		rotation = glm::yawPitchRoll(yaw, pitch, 0.0f);
		calcViewMatrix();
	} else if (ptr == &pitch) {
		rotation = glm::yawPitchRoll(yaw, pitch, 0.0f);
		calcViewMatrix();
	} else if (ptr == &position) {
		setPosition(position);
	} else if (ptr == &direction) {
		setDirection(direction);
	}
}

void FreeCamera::bind(IBinder& binder) {
	binder.bindSimple(yaw);
	binder.bindSimple(pitch);
	binder.bind(this, "position", MEMBER_WRAPPER(position), true, true, true);
	binder.bind(this, "direction", MEMBER_WRAPPER(direction), true, true, true);
}
