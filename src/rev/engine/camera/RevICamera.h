/*
 * RevICamera.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVICAMERA_H_
#define REVICAMERA_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <rev/engine/collision/primitives/RevViewFrustum.h>
#include <rev/engine/collision/primitives/RevRay.h>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

	enum class CameraType {
		FREE,
		SPHERICAL,
		TRANSITIONAL
	};

	class ICamera: public IBindable {
	protected:
		ViewFrustum frustum;

		float cameraAngle = 60.0f;
		float cameraNear = 0.3f;
		float cameraFar = 10000.0f;
		float screenWidth = -1;
		float screenHeight = -1;
		glm::mat4 projectionMatrix;

	public:
		virtual ~ICamera() {
		}

		virtual const glm::vec3& getPosition() const = 0;
		virtual const glm::vec3& getDirection() const = 0;
		virtual const glm::vec3& getUp() const = 0;
		virtual const glm::mat4& getViewMatrix() const = 0;
		virtual CameraType getType() const = 0;

		virtual const glm::mat4& getProjectionMatrix() const {
			return projectionMatrix;
		}

		virtual void setScreenSize(float width, float height) final {
			screenWidth = width;
			screenHeight = height;
			projectionMatrix = createDefalutProjectionMatrix(width, height);
			setScreenSizeImpl(width, height);
		}

		float getCameraNear() const {
			return cameraNear;
		}
		float getCameraFar() const {
			return cameraFar;
		}
		float getCameraAngle() const {
			return cameraAngle;
		}
		float getScreenWidth() const {
			return screenWidth;
		}
		float getScreenHeight() const {
			return screenHeight;
		}

		/**
		 * x and y are screen coordinates.
		 * y starts in the left top corner of the screen
		 * and has a positive direction towards down.
		 */
		Ray getRay(int x, int y);

		/**
		 * Returns position on the near plane in the world coordinates.
		 *
		 * x and y are screen coordinates.
		 * y starts in the left top corner of the screen
		 * and has a positive direction towards down.
		 */
		glm::vec3 getNearPlanePoint(int x, int y);

		/**
		 * Call this only once per frame!
		 */
		void recalculateViewFrustum();

		ViewFrustum& getViewFrustum() {
			return frustum;
		}

	protected:
		glm::mat4 createDefalutProjectionMatrix(float width, float height) {
			return glm::perspective(cameraAngle, width / height, cameraNear, cameraFar);
		}

		virtual void setScreenSizeImpl(float width, float height) = 0;
	};

}

#endif /* REVICAMERA_H_ */
