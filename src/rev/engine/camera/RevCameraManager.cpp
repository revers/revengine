/*
 * RevCameraManager.cpp
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#include <iostream>
#include <glm/glm.hpp>
#include <rev/engine/RevEngine.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/events/RevEventManager.h>
#include "RevCameraManager.h"
#include "RevFreeCamera.h"

#if REV_ENGINE_MAX_CONTEXTS > 1
#include <rev/engine/glcontext/RevGLContextManager.h>
#endif

#define CAMERA_ANIMATION_TIME 500

using namespace rev;
using namespace std;
using namespace glm;

rev::CameraManager* rev::CameraManager::cameraManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(CameraManager)

CameraManager::CameraManager() {
	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		cameras[i] = &freeCameras[i];
		drawCameraFrustum[i] = false;
	}
}

CameraManager::~CameraManager() {
}

void CameraManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!cameraManager);
	cameraManager = new CameraManager();

	EventManager::ref().addComponent(cameraManager);
	REV_TRACE_FUNCTION_OUT;
}

void CameraManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(cameraManager);

	EventManager::ref().removeComponent(cameraManager);
	delete cameraManager;
	cameraManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void CameraManager::resize(int width, int height, int contextIndex) {
	float w = width;
	float h = height;
	freeCameras[contextIndex].setScreenSize(w, h);
	sphericalCameras[contextIndex].setScreenSize(w, h);
	transtionAnimators[contextIndex].setScreenSize(w, h);
}

void CameraManager::useFreeCamera(int contextIndex) {
	REV_TRACE_FUNCTION;
	ICamera* camera = cameras[contextIndex];
	if (camera->getType() == CameraType::FREE) {
		return;
	}
	const vec3& position = camera->getPosition();
	const vec3& direction = camera->getDirection();

	FreeCamera& freeCamera = freeCameras[contextIndex];
	freeCamera.set(position, direction);
	CameraTransitionAnimator& animator = transtionAnimators[contextIndex];
	cameras[contextIndex] = &animator;

	animator.start(camera, &freeCamera, CAMERA_ANIMATION_TIME, contextIndex);
}

void CameraManager::useSphericalCamera(int contextIndex) {
	ICamera* camera = cameras[contextIndex];
	if (camera->getType() == CameraType::SPHERICAL) {
		return;
	}

	SphericalCamera& sphericalCamera = sphericalCameras[contextIndex];
	CameraTransitionAnimator& animator = transtionAnimators[contextIndex];
	cameras[contextIndex] = &animator;

	animator.start(camera, &sphericalCamera, CAMERA_ANIMATION_TIME, contextIndex);
}

void CameraManager::useSphericalCamera(int contextIndex, glm::vec3 target) {
	ICamera* camera = cameras[contextIndex];
	SphericalCamera& sphericalCamera = sphericalCameras[contextIndex];
	CameraTransitionAnimator& animator = transtionAnimators[contextIndex];
	cameras[contextIndex] = &animator;

	animator.start(camera, &sphericalCamera, CAMERA_ANIMATION_TIME, contextIndex);
	sphericalCamera.setPositionAndTarget(camera->getPosition(), target);
}

void CameraManager::useSphericalCamera(int contextIndex, glm::vec3 position,
		glm::vec3 target) {
	ICamera* camera = cameras[contextIndex];
	SphericalCamera& sphericalCamera = sphericalCameras[contextIndex];
	CameraTransitionAnimator& animator = transtionAnimators[contextIndex];
	cameras[contextIndex] = &animator;

	animator.start(camera, &sphericalCamera, CAMERA_ANIMATION_TIME, contextIndex);
	sphericalCamera.setPositionAndTarget(position, target);
}

ICamera* CameraManager::getCamera() {
	return cameras[GLContextManager::ref().getCurrentContextIndex()];
}

void CameraManager::keyPressed(const KeyEvent& e) {
	if (e.getKey() == Key::KEY_UP) {
		keyPressedContext = e.getContextIndex();
		upPressed = true;
	} else if (e.getKey() == Key::KEY_DOWN) {
		keyPressedContext = e.getContextIndex();
		downPressed = true;
	} else if (e.getKey() == Key::KEY_RIGHT) {
		keyPressedContext = e.getContextIndex();
		rightPressed = true;
	} else if (e.getKey() == Key::KEY_LEFT) {
		keyPressedContext = e.getContextIndex();
		leftPressed = true;
	}
}

void CameraManager::keyReleased(const KeyEvent& e) {
	if (e.getKey() == Key::KEY_UP) {
		upPressed = false;
	} else if (e.getKey() == Key::KEY_DOWN) {
		downPressed = false;
	} else if (e.getKey() == Key::KEY_RIGHT) {
		rightPressed = false;
	} else if (e.getKey() == Key::KEY_LEFT) {
		leftPressed = false;
	}
}

void CameraManager::mousePressed(const MouseEvent& e) {
	ICamera* camera = cameras[e.getContextIndex()];
	if (camera->getType() == CameraType::TRANSITIONAL) {
		return;
	}

	if (e.getButton() == MouseButton::BUTTON_LEFT) {
		mousePress = true;

		if (camera->getType() == CameraType::FREE) {
			FreeCamera* freeCamera = (FreeCamera*) camera;
			freeCamera->click(e.getX(), e.getY());
		} else if (camera->getType() == CameraType::SPHERICAL) {
			SphericalCamera* sphericalCamera = (SphericalCamera*) camera;
			sphericalCamera->click(e.getX(), e.getY());
		}
	} else if (e.getButton() == MouseButton::BUTTON_MIDDLE) {
		rev::Renderer3D::ref().getPicker().click(e.getX(), e.getY());
	}
}

void CameraManager::mouseReleased(const MouseEvent& e) {
	ICamera* camera = cameras[e.getContextIndex()];
	if (camera->getType() == CameraType::TRANSITIONAL) {
		return;
	}
	if (e.getButton() == MouseButton::BUTTON_LEFT) {
		mousePress = false;
	}
}

void CameraManager::mouseMoved(const MouseEvent& e) {
	ICamera* camera = cameras[e.getContextIndex()];
	if (camera->getType() == CameraType::TRANSITIONAL) {
		return;
	}
	if (!mousePress) {
		return;
	}

	if (camera->getType() == CameraType::FREE) {
		FreeCamera* freeCamera = (FreeCamera*) camera;
		freeCamera->drag(e.getX(), e.getY());
	} else if (camera->getType() == CameraType::SPHERICAL) {
		SphericalCamera* sphericalCamera = (SphericalCamera*) camera;
		sphericalCamera->drag(e.getX(), e.getY());
	}
}

void CameraManager::mouseWheelMoved(const MouseWheelEvent& e) {
	ICamera* camera = cameras[e.getContextIndex()];
	if (camera->getType() == CameraType::TRANSITIONAL) {
		return;
	}
	wheelContext = e.getContextIndex();
	wheelDistance = 0;
	const float step = 10.0;
	if (e.getDifference() < 0) {
		wheelDistance = step;
	} else {
		wheelDistance = -step;
	}
	//REV_TRACE_MSG(R(wheelDistance));
}

void CameraManager::update() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	const int contextIndex = GLContextManager::ref().getCurrentContextIndex();
#else
	const int contextIndex = 0;
#endif

	cameras[contextIndex]->recalculateViewFrustum();
	float frameTime = rev::Engine::ref().getCurrentFrameTime();
	translateCamera(frameTime);

	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		transtionAnimators[i].update(frameTime);
	}
}

void CameraManager::translateCamera(float elapsedMs) {
	if (cameras[keyPressedContext]->getType() == CameraType::FREE) {
		vec3 translation(0);

		float step = 1.0;
		if (upPressed) {
			translation.z += step;
		}
		if (downPressed) {
			translation.z -= step;
		}
		if (rightPressed) {
			translation.x -= step;
		}
		if (leftPressed) {
			translation.x += step;
		}
		translation *= elapsedMs * 0.03;

		FreeCamera* freeCamera = (FreeCamera*) cameras[keyPressedContext];
		freeCamera->move(translation);
	}

	if (wheelDistance != 0.0f) {
		float distance = wheelDistance * elapsedMs * 0.3;

	//	REV_TRACE_MSG(R(distance));
//		float distance = wheelDistance * elapsedMs * 0.3;
//
//		if (distance > 10.0f) {
//			wheelDistance = 10.0;
//		} else if (distance < -10.0f) {
//			wheelDistance = -10.0f;
//		}

		if (distance > 10.0f) {
			// Bug fix with huge distance step when selecting object.
			wheelDistance = 0.0;
		} else if (cameras[wheelContext]->getType() == CameraType::FREE) {
			FreeCamera* freeCamera = (FreeCamera*) cameras[wheelContext];
			freeCamera->increaseDistance(distance);
		} else if (cameras[wheelContext]->getType() == CameraType::SPHERICAL) {
			SphericalCamera* sphericalCamera = (SphericalCamera*) cameras[wheelContext];
			sphericalCamera->increaseDistance(distance);

		}
		wheelDistance = 0;
	}
}
