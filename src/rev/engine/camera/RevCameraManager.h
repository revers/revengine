/*
 * RevCameraManager.h
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#ifndef REVCAMERAMANAGER_H_
#define REVCAMERAMANAGER_H_

#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevIEventListenerComponent.h>
#include "RevICamera.h"
#include "RevFreeCamera.h"
#include "RevSphericalCamera.h"
#include "RevCameraTransitionAnimator.h"

namespace rev {

	class CameraManager: public IEventListenerComponent {
		friend class CameraTransitionAnimator;
		static CameraManager* cameraManager;

		FreeCamera freeCameras[REV_ENGINE_MAX_CONTEXTS];
		SphericalCamera sphericalCameras[REV_ENGINE_MAX_CONTEXTS];
		CameraTransitionAnimator transtionAnimators[REV_ENGINE_MAX_CONTEXTS];

		ICamera* cameras[REV_ENGINE_MAX_CONTEXTS];
		bool drawCameraFrustum[REV_ENGINE_MAX_CONTEXTS];

		bool mousePress = false;
		int lastMouseX = 0;
		int lastMouseY = 0;

		bool upPressed = false;
		bool downPressed = false;
		bool leftPressed = false;
		bool rightPressed = false;
		int keyPressedContext = 0;

		float wheelDistance = 0;
		int wheelContext = 0;

		CameraManager();

	public:
		DECLARE_BINDABLE_SINGLETON(CameraManager)

		virtual ~CameraManager();

		static void createSingleton();
		static void destroySingleton();

		static CameraManager& ref() {
			return *cameraManager;
		}

		static CameraManager* getInstance() {
			return cameraManager;
		}

		ICamera* getCamera();
		ICamera* getCamera(int contextIndex) {
			return cameras[contextIndex];
		}
		FreeCamera* getFreeCamera(int contextIndex) {
			return &freeCameras[contextIndex];
		}
		SphericalCamera* getSphericalCamera(int contextIndex) {
			return &sphericalCameras[contextIndex];
		}
		bool isDrawCameraFrustum(int contextIndex) {
			return drawCameraFrustum[contextIndex];
		}
		void setDrawCameraFrustum(int contextIndex, bool draw) {
			drawCameraFrustum[contextIndex] = draw;
		}

		void useFreeCamera(int contextIndex);
		void useSphericalCamera(int contextIndex);
		void useSphericalCamera(int contextIndex, glm::vec3 target);
		void useSphericalCamera(int contextIndex, glm::vec3 position, glm::vec3 target);
		bool isFreeCameraUsed(int contextIndex) {
			return cameras[contextIndex] == &freeCameras[contextIndex];
		}
		bool isSphericalCameraUsed(int contextIndex) {
			return cameras[contextIndex] == &sphericalCameras[contextIndex];
		}
		void resize(int width, int height, int contextIndex);
		void keyPressed(const KeyEvent& e) override;
		void keyReleased(const KeyEvent& e) override;
		void mousePressed(const MouseEvent& e) override;
		void mouseReleased(const MouseEvent& e) override;
		void mouseMoved(const MouseEvent& e) override;
		void mouseWheelMoved(const MouseWheelEvent& e) override;
		void update() override;

	private:
		void translateCamera(float elapsedMs);

	};

}
/* namespace rev */
#endif /* REVCAMERAMANAGER_H_ */
