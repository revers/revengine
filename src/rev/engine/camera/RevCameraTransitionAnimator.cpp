/*
 * RevCameraTransitionAnimator.cpp
 *
 *  Created on: 08-04-2013
 *      Author: Revers
 */

#include "RevCameraTransitionAnimator.h"
#include "RevCameraManager.h"

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(CameraTransitionAnimator)

CameraTransitionAnimator::CameraTransitionAnimator() {
}

CameraTransitionAnimator::~CameraTransitionAnimator() {
}

void CameraTransitionAnimator::start(ICamera* beginCamera, ICamera* endCamera,
		float duration, int contextIndex) {
	this->endCamera = endCamera;
	this->duration = duration;
	this->elapsedTime = 0.0f;
	this->contextIndex = contextIndex;
	this->oldPosition = beginCamera->getPosition();
	this->oldDirection = beginCamera->getDirection();
	this->oldUp = beginCamera->getUp();

	update(0);
}

void CameraTransitionAnimator::update(float frameTimeMs) {
	if (!endCamera) {
		return;
	}
	float percent = elapsedTime / duration;

	position = glm::mix(oldPosition, endCamera->getPosition(), percent);
	direction = glm::mix(oldDirection, endCamera->getDirection(), percent);
	up = glm::mix(oldUp, endCamera->getUp(), percent);

	viewMatrix = glm::lookAt(position, position + direction, up);

	elapsedTime += frameTimeMs;
	if (elapsedTime > duration) {
		CameraManager::ref().cameras[contextIndex] = endCamera;
		endCamera = nullptr;
	}
}
