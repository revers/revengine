/*
 * RevGameFactory.h
 *
 *  Created on: 01-12-2012
 *      Author: Revers
 */

#ifndef REVGAMEFACTORY_H_
#define REVGAMEFACTORY_H_

#include <rev/engine/drawables/RevIVBODrawable.h>

#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/effects/RevPhongShadingEffect.h>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

enum class PredefinedGLSLProgram {
	PHONG_SHADING, SIZE_OF_ENUM
};

enum class PredefinedDrawable {
	CUBE, TORUS, SIZE_OF_ENUM
};

class GameFactory: public IBindable {
	static GameFactory* gameFactory;
	rev::IVBODrawable* predefinedDrawables[(int) PredefinedDrawable::SIZE_OF_ENUM];
	rev::GLSLProgram* predefinedGLSLPrograms[(int) PredefinedGLSLProgram::SIZE_OF_ENUM];

	GameFactory() {
	}

	GameFactory(const GameFactory&) = delete;
	~GameFactory();

public:
	DECLARE_BINDABLE_SINGLETON(GameFactory)

	static void createSingleton();
	static void destroySingleton();

	static GameFactory& ref() {
		return *gameFactory;
	}

	static GameFactory* getInstance() {
		return gameFactory;
	}

	rev::PhongShadingEffect* createPhongShadingEffect() {
		return new PhongShadingEffect(
				getPredefinedGLSLProgram(
						PredefinedGLSLProgram::PHONG_SHADING));
	}

	/**
	 * TODO: dodac parametry do tych funkcji:
	 */
	rev::GameObject* createSimpleTorusGameObject();
	rev::GameObject* createSimpleCubeGameObject();

	rev::GameObject* createSimpleSphereGameObject(
			float radius = 1.0f,
			int tessellation = 30);

	rev::GameObject* createSimpleCylinderGameObject(
			float height = 2.0f,
			float radius = 1.0f,
			int tessellation = 30);

	rev::GameObject* createSimpleFrustumGameObject();
	rev::GameObject* createSimpleCageGameObject();
	rev::GameObject* createSimpleCuboidGameObject();
	rev::GameObject* createSimplePlaneGameObject();
	rev::GameObject* createSimplePlane2GameObject();
	rev::GameObject* createSimpleTeapotGameObject();
	rev::GameObject* createSimpleConeGameObject();
	rev::GameObject* createSimpleArrowGameObject();

	rev::GameObject* createSimpleAxesGameObject(
			float length = 20.0f);

	rev::GameObject* createSimpleBilinearSurfaceGameObject(
			int xTesselation = 20,
			int yTesselation = 20,
			float scaleFactor = 1.0);

	rev::GameObject* createSimpleBilinearEffectGameObject();

	rev::GameObject* createSimpleBiquadraticSurfaceGameObject(
			int xTesselation = 20,
			int yTesselation = 20,
			float scaleFactor = 1.0);

	rev::GameObject* createSimpleBicubicSurfaceGameObject(
			int xTesselation = 20,
			int yTesselation = 20,
			float scaleFactor = 1.0);

	IVBODrawable* getPredefinedDrawable(PredefinedDrawable predefinedDrawable) {
		return predefinedDrawables[(int) predefinedDrawable];
	}

	GLSLProgram* getPredefinedGLSLProgram(
			PredefinedGLSLProgram predefinedProgram) {
		return predefinedGLSLPrograms[(int) predefinedProgram];
	}

	rev::GameObject* createSamplePhysicsObject();
	rev::GameObject* createSampleParticleObject();
	rev::GameObject* createSampleParticleInstancingObject();

	bool init();

private:
	GLSLProgram* createPhongShadingProgram();
	bool createPredefinedGLSLPrograms();
	bool createPredefinedDrawables();
	void deletePredefinedGLSLPrograms();
	void deletePredefinedDrawables();

};

} /* namespace rev */
#endif /* REVGAMEFACTORY_H_ */
