/*
 * RevCollisionComponent.cpp
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#include <rev/engine/RevGameObject.h>
#include "RevCollisionComponent.h"

using namespace rev;

IMPLEMENT_BINDABLE(CollisionComponent)

void CollisionComponent::bind(IBinder& binder) {
	binder.bind(this, "bsCenter", MEMBER_PTR_WRAPPER(ptrBsCenter), true, true, false);
	binder.bind(this, "bsRadius", MEMBER_PTR_WRAPPER(ptrBsRadius), true, true, true);
	binder.bindSimple(drawBoundingSphere);
	binder.bindSimple(drawGeometry);
	binder.bindSimpleObjExpanded(geometry);
}

BoundingSphere CollisionComponent::getWorldBoundingSphere() const {
	return BoundingSphere(boundingSphere.c + getParent()->getPosition(),
			boundingSphere.r);
}
