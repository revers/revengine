/*
 * RevShadowSourceComponent.cpp
 *
 *  Created on: 27 lip 2013
 *      Author: Revers
 */

#include <glm/gtc/matrix_transform.hpp>
#include "RevShadowSourceComponent.h"

namespace rev {

IMPLEMENT_BINDABLE(ShadowSourceComponent)

void ShadowSourceComponent::bind(IBinder& binder) {
	binder.bindSimple(viewAngle);
	binder.bindSimple(viewNear);
	binder.bindSimple(viewFar);
}

void ShadowSourceComponent::bindableValueChanged(void* ptr) {
	calcProjectionMatrix();
}

void ShadowSourceComponent::calcProjectionMatrix() {
	projectionMatrix = glm::perspective(viewAngle, 1.0f, viewNear, viewFar);
}

} /* namespace rev */
