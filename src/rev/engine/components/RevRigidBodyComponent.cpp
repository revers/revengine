/*
 * RevRigidBodyComponent.cpp
 *
 *  Created on: 21-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#include <memory.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevGameObject.h>
#include "RevRigidBodyComponent.h"

#include <rev/engine/util/RevGLMOstream.h>

using namespace rev;
using namespace glm;

IMPLEMENT_BINDABLE(RigidBodyComponent)

/**
 * Adds the given vector to this, scaled by the given amount.
 * This is used to update the orientation quaternion by a rotation
 * and time.
 */
inline void addScaledVector(rquat& qu, const rvec3& vector, real scale) {
	rquat q(real(0), vector.x * scale, vector.y * scale, vector.z * scale);
	q = q * qu;
	qu.w += q.w * real(0.5);
	qu.x += q.x * real(0.5);
	qu.y += q.y * real(0.5);
	qu.z += q.z * real(0.5);
}

/**
 * Integrates the rigid body forward in time by the given amount.
 * This function uses a Newton-Euler integration method, which is a
 * linear approximation to the correct integral. For this reason it
 * may be inaccurate in some cases.
 */
void RigidBodyComponent::update(real duration) {
	if (!isAwake) {
		return;
	}

	// Normalize the orientation, and update the matrices with the new
	// position and orientation
	calculateDerivedData();

	// Calculate linear acceleration from force inputs.
	lastFrameAcceleration = constantAcceleration;
	//addScaledVector(lastFrameAcceleration, forceAccum, inverseMass);
	lastFrameAcceleration += forceAccum * inverseMass;

	// Calculate angular acceleration from torque inputs.
	rvec3 angularAcceleration = inverseInertiaTensorWorld * torqueAccum;
	//inverseInertiaTensorWorld.transform(torqueAccum);

	// Adjust velocities
	// Update linear velocity from both acceleration and impulse.
	velocity += lastFrameAcceleration * duration;

	// Update angular velocity from both acceleration and impulse.
	angularVelocity += angularAcceleration * duration;

	// Impose drag.
	velocity *= real_pow(linearDamping, duration);
	angularVelocity *= real_pow(angularDamping, duration);

	// Adjust positions
	// Update linear position.
	position += velocity * duration;

	// Update angular position.
	addScaledVector(orientation, angularVelocity, duration);

	updateParent();

	// Clear accumulators.
	clearAccumulators();

	// Update the kinetic energy store, and possibly put the body to
	// sleep.
	if (canSleep) {
		real currentMotion = dot(velocity, velocity)
				+ dot(angularVelocity, angularVelocity);

		real bias = real_pow(0.5, duration);
		motion = bias * motion + (real(1.0) - bias) * currentMotion;

		if (motion < sleepEpsilon) {
			setAwake(false);
		} else if (motion > real(10.0) * sleepEpsilon) {
			motion = real(10.0) * sleepEpsilon;
		}
	}
}

void RigidBodyComponent::updateParent() {
	GameObject* parent = getParent();
	parent->setPosition(float(position.x), float(position.y), float(position.z));

	parent->setOrientation(float(orientation.w), float(orientation.x),
			float(orientation.y), float(orientation.z));
}

/**
 * Internal function to do an intertia tensor transform by a quaternion.
 * Note that the implementation of this function was created by an
 * automated code-generator and optimizer.
 */
static inline void _transformInertiaTensor(rmat3& iitWorld, const rquat& q,
		const rmat3& iitBody, const rmat4& rotmat) {
	real t4 = rotmat[0][0] * iitBody[0][0]
			+ rotmat[1][0] * iitBody[0][1]
			+ rotmat[2][0] * iitBody[0][2];

	real t9 = rotmat[0][0] * iitBody[1][0]
			+ rotmat[1][0] * iitBody[1][1]
			+ rotmat[2][0] * iitBody[1][2];

	real t14 = rotmat[0][0] * iitBody[2][0]
			+ rotmat[1][0] * iitBody[2][1]
			+ rotmat[2][0] * iitBody[2][2];

	real t28 = rotmat[0][1] * iitBody[0][0]
			+ rotmat[1][1] * iitBody[0][1]
			+ rotmat[2][1] * iitBody[0][2];

	real t33 = rotmat[0][1] * iitBody[1][0]
			+ rotmat[1][1] * iitBody[1][1]
			+ rotmat[2][1] * iitBody[1][2];

	real t38 = rotmat[0][1] * iitBody[2][0]
			+ rotmat[1][1] * iitBody[2][1]
			+ rotmat[2][1] * iitBody[2][2];

	real t52 = rotmat[0][2] * iitBody[0][0]
			+ rotmat[1][2] * iitBody[0][1]
			+ rotmat[2][2] * iitBody[0][2];

	real t57 = rotmat[0][2] * iitBody[1][0]
			+ rotmat[1][2] * iitBody[1][1]
			+ rotmat[2][2] * iitBody[1][2];

	real t62 = rotmat[0][2] * iitBody[2][0]
			+ rotmat[1][2] * iitBody[2][1]
			+ rotmat[2][2] * iitBody[2][2];

	iitWorld[0][0] = t4 * rotmat[0][0] + t9 * rotmat[1][0] + t14 * rotmat[2][0];
	iitWorld[1][0] = t4 * rotmat[0][1] + t9 * rotmat[1][1] + t14 * rotmat[2][1];
	iitWorld[2][0] = t4 * rotmat[0][2] + t9 * rotmat[1][2] + t14 * rotmat[2][2];

	iitWorld[0][1] = t28 * rotmat[0][0] + t33 * rotmat[1][0] + t38 * rotmat[2][0];
	iitWorld[1][1] = t28 * rotmat[0][1] + t33 * rotmat[1][1] + t38 * rotmat[2][1];
	iitWorld[2][1] = t28 * rotmat[0][2] + t33 * rotmat[1][2] + t38 * rotmat[2][2];

	iitWorld[0][2] = t52 * rotmat[0][0] + t57 * rotmat[1][0] + t62 * rotmat[2][0];
	iitWorld[1][2] = t52 * rotmat[0][1] + t57 * rotmat[1][1] + t62 * rotmat[2][1];
	iitWorld[2][2] = t52 * rotmat[0][2] + t57 * rotmat[1][2] + t62 * rotmat[2][2];
}

/**
 * Inline function that creates a transform matrix from a
 * position and orientation.
 */
static inline void _calculateTransformMatrix(rmat4& transformMatrix,
		const rvec3& position, const rquat& orientation) {
	transformMatrix[0][0] = real(1) - real(2) * orientation.y * orientation.y
			- real(2) * orientation.z * orientation.z;
	transformMatrix[1][0] = real(2) * orientation.x * orientation.y
			- real(2) * orientation.w * orientation.z;
	transformMatrix[2][0] = real(2) * orientation.x * orientation.z
			+ real(2) * orientation.w * orientation.y;
	transformMatrix[3][0] = position.x;

	transformMatrix[0][1] = real(2) * orientation.x * orientation.y
			+ real(2) * orientation.w * orientation.z;
	transformMatrix[1][1] = real(1) - real(2) * orientation.x * orientation.x
			- real(2) * orientation.z * orientation.z;
	transformMatrix[2][1] = real(2) * orientation.y * orientation.z
			- real(2) * orientation.w * orientation.x;
	transformMatrix[3][1] = position.y;

	transformMatrix[0][2] = real(2) * orientation.x * orientation.z
			- real(2) * orientation.w * orientation.y;
	transformMatrix[1][2] = real(2) * orientation.y * orientation.z
			+ real(2) * orientation.w * orientation.x;
	transformMatrix[2][2] = real(1) - real(2) * orientation.x * orientation.x
			- real(2) * orientation.y * orientation.y;
	transformMatrix[3][2] = position.z;
}

void RigidBodyComponent::calculateDerivedData() {
	GameObject* parent = getParent();
	//orientation.normalise();
	const quat& q = parent->getOrientation();
	orientation.w = real(q.w);
	orientation.x = real(q.x);
	orientation.y = real(q.y);
	orientation.z = real(q.z);
	orientation = glm::normalize(orientation);

	const vec3& v = parent->getPosition();
	position.x = real(v.x);
	position.y = real(v.y);
	position.z = real(v.z);

	// Calculate the transform matrix for the body.
	_calculateTransformMatrix(transformMatrix, position, orientation);

	// Calculate the inertiaTensor in world space.
	_transformInertiaTensor(inverseInertiaTensorWorld, orientation,
			inverseInertiaTensor, transformMatrix);
}

void RigidBodyComponent::setMass(const real mass) {
	assert(mass != real(0.0));
	this->inverseMass = real(1.0) / mass;
}

real RigidBodyComponent::getMass() const {
	if (inverseMass == real(0.0)) {
		return REAL_MAX;
	} else {
		return real(1.0) / inverseMass;
	}
}

void RigidBodyComponent::setInertiaTensor(const rmat3& inertiaTensor) {
	//inverseInertiaTensor.setInverse(inertiaTensor);
	inverseInertiaTensor = glm::inverse(inertiaTensor);
}

rmat3 RigidBodyComponent::getInertiaTensor() const {
	return glm::inverse(inverseInertiaTensor);
}

rmat3 RigidBodyComponent::getInertiaTensorWorld() const {
	return glm::inverse(inverseInertiaTensorWorld);
}

void RigidBodyComponent::setInverseInertiaTensor(const rmat3& inverseInertiaTensor) {
	this->inverseInertiaTensor = inverseInertiaTensor;
}

rvec3 RigidBodyComponent::getPointInLocalSpace(const rvec3& point) const {
	//return transformMatrix.transformInverse(point);
	return MathHelper::transformInverse(transformMatrix, point);
}

rvec3 RigidBodyComponent::getPointInWorldSpace(const rvec3& point) const {
	//return transformMatrix.transform(point);
	return mul(transformMatrix, point);
}

rvec3 RigidBodyComponent::getDirectionInLocalSpace(const rvec3& direction) const {
	return MathHelper::transformInverseDirection(transformMatrix, direction);
}

rvec3 RigidBodyComponent::getDirectionInWorldSpace(const rvec3& direction) const {
	return MathHelper::transformDirection(transformMatrix, direction);
}

void RigidBodyComponent::setAwake(const bool awake) {
	if (awake) {
		isAwake = true;

		// Add a bit of motion to avoid it falling asleep immediately.
		motion = sleepEpsilon * real(2.0);
	} else {
		isAwake = false;
		velocity = rvec3(0);
		angularVelocity = rvec3(0);
	}
}

void RigidBodyComponent::setCanSleep(const bool canSleep) {
	RigidBodyComponent::canSleep = canSleep;

	if (!canSleep && !isAwake) {
		setAwake();
	}
}

void RigidBodyComponent::addForceAtBodyPoint(const rvec3& force,
		const rvec3& point) {
	// Convert to coordinates relative to center of mass.
	rvec3 pt = getPointInWorldSpace(point);
	addForceAtPoint(force, pt);

}

void RigidBodyComponent::addForceAtPoint(const rvec3& force,
		const rvec3& point) {
	// Convert to coordinates relative to center of mass.
	rvec3 pt = point;
	pt -= position;

	forceAccum += force;
	torqueAccum += cross(pt, force);

	isAwake = true;
}

void RigidBodyComponent::setPosition(const rvec3& p) {
	//REV_TRACE_MSG(R(p));
	this->position = position;
	getParent()->setPosition(float(p.x), float(p.y), float(p.z));
}

void RigidBodyComponent::setPosition(const real x, const real y, const real z) {
	//REV_TRACE_MSG(a2s(R(x), R(y), R(z)));
	position.x = x;
	position.y = y;
	position.z = z;
	getParent()->setPosition(float(x), float(y), float(z));
}

void RigidBodyComponent::setOrientation(const rquat& o) {
	this->orientation = glm::normalize(o);
	getParent()->setOrientation(float(o.w), float(o.x), float(o.y), float(o.z));
}

void RigidBodyComponent::bindableValueChanged(void* ptr) {
	if (ptr == &velocity || ptr == &angularVelocity) {
		isAwake = true;
	}
}

void RigidBodyComponent::bind(IBinder& binder) {
	binder.bindInfo(this, NVP(motion));
	binder.bindSimple(isAwake);
	binder.bindSimple(canSleep);
	binder.bindSimple(sleepEpsilon);
	binder.bindSimple(inverseMass);
	binder.bindSimpleRO(inverseInertiaTensor);
	binder.bindSimple(linearDamping);
	binder.bindSimple(angularDamping);
	binder.bindSimple(velocity);
	binder.bindSimple(angularVelocity);
	binder.bindSimple(constantAcceleration);
	binder.bindSimple(friction);
	binder.bindSimple(restitution);
	binder.bindInfo(this, NVP(position));
	binder.bindInfo(this, NVP(orientation));
	binder.bindInfo(this, NVP(forceAccum));
	binder.bindInfo(this, NVP(torqueAccum));
}

