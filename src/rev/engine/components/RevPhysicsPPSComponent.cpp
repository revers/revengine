/*
 * RevPhysicsPPSComponent.cpp
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#include <rev/common/RevDelete.hpp>
#include <rev/engine/physics/RevIntegratorSet.h>
#include <rev/engine/physics/RevLinePPS.h>
#include "RevPhysicsPPSComponent.h"

using namespace rev;

IMPLEMENT_BINDABLE(PhysicsPPSComponent)

PhysicsPPSComponent::PhysicsPPSComponent() {
	particleSet = new LinePPS();
	particleSet->setIntegrator(IntegratorSet::getVerlet());
}

PhysicsPPSComponent::~PhysicsPPSComponent() {
	DEL(particleSet);
}

void PhysicsPPSComponent::update(real timestep) {
	particleSet->step(timestep);
}
