/*
 * RevCollisionComponent.h
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#ifndef REVCOLLISIONCOMPONENT_H_
#define REVCOLLISIONCOMPONENT_H_

#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/collision/RevCollisionGeometry.h>
#include "RevIComponent.h"

namespace rev {

	class CollisionComponent: public IComponent {
		/**
		 * Bounding sphere for coarse collision detection.
		 */
		BoundingSphere boundingSphere;
		/** binding */
		glm::vec3* ptrBsCenter = &boundingSphere.c;
		float* ptrBsRadius = &boundingSphere.r;

		/**
		 * Geometry for fine collision detection.
		 */
		CollisionGeometry* geometry = nullptr;

		bool drawBoundingSphere = false;
		bool drawGeometry = false;

	public:
		DECLARE_BINDABLE(CollisionComponent)

		CollisionComponent() :
				IComponent(ComponentType::COLLISION) {
		}

		~CollisionComponent() {
			if (geometry) {
				delete geometry;
			}
		}

		const BoundingSphere& getBoundingSphere() const {
			return boundingSphere;
		}

		BoundingSphere getWorldBoundingSphere() const;

		void setBoundingSphere(const BoundingSphere& boundingSphere) {
			this->boundingSphere = boundingSphere;
		}

		bool isDrawBoundingSphere() const {
			return drawBoundingSphere;
		}

		void setDrawBoundingSphere(bool drawBoundingSphere) {
			this->drawBoundingSphere = drawBoundingSphere;
		}

		bool isDrawGeometry() const {
			return drawGeometry;
		}

		void setDrawGeometry(bool drawGeometry) {
			this->drawGeometry = drawGeometry;
		}

		CollisionGeometry* getGeometry() const {
			return geometry;
		}

		/**
		 * CollisionComponent takes ownership of the "geometry" object.
		 */
		void setGeometry(CollisionGeometry* geometry) {
			this->geometry = geometry;
		}

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCOLLISIONCOMPONENT_H_ */
