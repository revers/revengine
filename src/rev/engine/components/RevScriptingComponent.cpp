/*
 * RevScriptingComponent.cpp
 *
 *  Created on: 24-03-2013
 *      Author: Revers
 */

#include "RevScriptingComponent.h"

using namespace rev;

IMPLEMENT_BINDABLE(ScriptingComponent)

void ScriptingComponent::bind(IBinder& binder) {
	binder.bindFilepath(this, "Script file", MEMBER_WRAPPER(scriptFile), "JavaScript files (*.js)");
	binder.bindSimple(active);
	binder.bindInfo(this, NVP(compiled));
	binder.bindInfo(this, NVP(invalid));
}

void ScriptingComponent::bindableValueChanged(void* ptr) {
	if (ptr == (void*) &scriptFile) {
		scriptFileChanged();
	}
}
