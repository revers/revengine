/*
 * RevIComponent.h
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#ifndef REVICOMPONENT_H_
#define REVICOMPONENT_H_

#include "RevComponentTypes.h"

#include <rev/engine/binding/RevIBindable.h>

namespace rev {
    class GameObject;

    class IComponent: public IBindable {
    protected:
        GameObject* parentObject = nullptr;
        const ComponentType componentType;

    public:
        IComponent(ComponentType componentType) :
                componentType(componentType) {
        }

        ComponentType getType() const {
            return componentType;
        }

        GameObject* getParent() const {
            return parentObject;
        }

        void setParent(GameObject* parentObject) {
            this->parentObject = parentObject;
        }
    };
} /* namespace rev */

#endif /* REVICOMPONENT_H_ */
