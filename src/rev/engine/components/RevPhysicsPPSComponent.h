/*
 * RevPhysicsPPSComponent.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVPHYSICSPPSCOMPONENT_H_
#define REVPHYSICSPPSCOMPONENT_H_

#include <rev/engine/physics/RevPointParticleSet.h>
#include "RevIPhysicsComponent.h"

namespace rev {

	class PhysicsPPSComponent: public IPhysicsComponent {
	private:
		PointParticleSet* particleSet = nullptr;

	public:
		DECLARE_BINDABLE(PhysicsPPSComponent)

		PhysicsPPSComponent();
		~PhysicsPPSComponent();

		PhysicsComponentType getType() override {
			return PhysicsComponentType::PARTICLE_SET;
		}

		void update(real timestep) override;

		PointParticleSet* getPointParticleSet() {
			return particleSet;
		}

		int getParticlesCount() {
			return particleSet->getPointParticlesCount();
		}
		glm::vec3 getParticlePosition(int index) {
			return glm::vec3(particleSet->getParticle(index).getState().x);
		}
		float getParticleRadius(int index) {
			return particleSet->getParticle(index).getRadius();
		}
		PointParticle& getParticle(int index) {
			return particleSet->getParticle(index);
		}
	};

} /* namespace rev */
#endif /* REVPHYSICSPPSCOMPONENT_H_ */
