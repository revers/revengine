/*
 * Simple3DComponent.h
 *
 *  Created on: 29-11-2012
 *      Author: Revers
 */

#ifndef SIMPLE3DCOMPONENT_H_
#define SIMPLE3DCOMPONENT_H_

#include "RevIVisual3DComponent.h"
#include <rev/engine/effects/RevIEffect.h>
#include <rev/engine/effects/RevIEffect.h>
#include <rev/engine/drawables/RevIVBODrawable.h>

namespace rev {

	class Simple3DComponent: public IVisual3DComponent {
	private:
		IEffect* effect = nullptr;
		IVBODrawable* drawable = nullptr;
		bool effectOwner = true;
		bool drawableOwner = true;

		Simple3DComponent() {
		}

	public:
		DECLARE_BINDABLE(Simple3DComponent)

		Simple3DComponent(IEffect* effect, bool effectOwner,
				IVBODrawable* drawable, bool drawableOwner);
		virtual ~Simple3DComponent();

		IEffect* getEffect() {
			return effect;
		}

		void setEffect(IEffect* effect, bool effectOwner = true);

		IVBODrawable* getDrawable() {
			return drawable;
		}

		void setDrawable(IVBODrawable* drawable, bool drawableOwner = true);

		RenderPrimitiveType getRenderPrimitiveType() const override {
			return drawable->getRenderPrimitiveType();
		}

		int getPrimitiveCount() override {
			return drawable->getPrimitiveCount();
		}

		void update() override {
		}

		void applyEffect(
				const glm::mat4& modelMatrix,
				const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix,
				const ICamera& camera) override {
			effect->apply(modelMatrix, viewMatrix, projectionMatrix, camera);
		}

		const rev::BoundingSphere* getBoundingSphere() override {
			return drawable->getBoundingSphere();
		}

		void render() override {
			drawable->render();
		}

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* SIMPLE3DCOMPONENT_H_ */
