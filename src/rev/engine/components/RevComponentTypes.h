/*
 * RevComponentTypes.h
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#ifndef REVCOMPONENTTYPES_H_
#define REVCOMPONENTTYPES_H_

namespace rev {
    enum class ComponentType {
        VISUAL_3D,
        COLLISION,
        PHYSICS,
        EVENT_LISTENER,
        SCRIPTING,
        LIGHT,
        SHADOW
    };
} /* namespace rev */

#endif /* REVCOMPONENTTYPES_H_ */
