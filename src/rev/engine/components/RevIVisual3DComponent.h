/*
 * RevIVisual3DComponent.h
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#ifndef REVIVISUAL3DCOMPONENT_H_
#define REVIVISUAL3DCOMPONENT_H_

#include <rev/engine/collision/primitives/RevBoundingSphere.h>
#include <rev/engine/camera/RevICamera.h>
#include <rev/engine/drawables/RevRenderPrimitiveType.h>
#include "RevIComponent.h"

namespace rev {

class IVisual3DComponent: public IComponent {
	friend class Renderer3D;
	/**
	 * Helper variable for Renderer3D:
	 */
	bool culled = false;

protected:
	bool wireFrame = false;
	bool cullFace = false;
	bool debugElement = false;
	bool pickable = true;
	bool canDrawOutline = true;

	IVisual3DComponent() :
			IComponent(ComponentType::VISUAL_3D) {
	}

public:
	virtual void update() {
	}

	virtual void applyEffect(
			const glm::mat4& modelMatrix,
			const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix,
			const ICamera& camera) = 0;

	virtual void render() = 0;

	virtual const rev::BoundingSphere* getBoundingSphere() = 0;

	virtual int getPrimitiveCount() = 0;

	virtual RenderPrimitiveType getRenderPrimitiveType() const = 0;

	bool isWireframe() const {
		return wireFrame;
	}

	void setWireframe(bool wireframe) {
		this->wireFrame = wireframe;
	}

	bool isCullFace() const {
		return cullFace;
	}

	void setCullFace(bool cullFace) {
		this->cullFace = cullFace;
	}

	bool isDebugElement() const {
		return debugElement;
	}

	void setDebugElement(bool debugElement) {
		this->debugElement = debugElement;
		this->pickable = !debugElement;
	}

	bool isPickable() const {
		return pickable;
	}

	void setPickable(bool pickable) {
		this->pickable = pickable;
	}

	bool isCanDrawOutline() const {
		return canDrawOutline;
	}

	void setCanDrawOutline(bool canDrawOutline) {
		this->canDrawOutline = canDrawOutline;
	}

protected:
	void bind(IBinder& binder) override {
		binder.bindSimple(wireFrame);
		binder.bindSimple(cullFace);
		binder.bindSimple(debugElement);
		binder.bindSimple(pickable);
	}
};
} /* namespace rev */

#endif /* REVIVISUAL3DCOMPONENT_H_ */
