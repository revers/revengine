/*
 * RevScriptingComponent.h
 *
 *  Created on: 24-03-2013
 *      Author: Revers
 */

#ifndef REVSCRIPTINGCOMPONENT_H_
#define REVSCRIPTINGCOMPONENT_H_

#include <string>
#include <v8/v8.h>
#include "RevIComponent.h"

namespace rev {

	class ScriptingComponent: public IComponent {
		friend class ScriptingManager;

		std::string scriptFile;
		bool active = false;
		bool compiled = false;
		bool invalid = true;

		v8::Persistent<v8::Function> updateFunc;
		v8::Persistent<v8::Function> initFunc;
		v8::Persistent<v8::Function> destroyFunc;

	public:
		DECLARE_BINDABLE(ScriptingComponent)

		ScriptingComponent(const char* scriptFile) :
				IComponent(ComponentType::SCRIPTING), scriptFile(scriptFile) {
		}

		ScriptingComponent() :
				IComponent(ComponentType::SCRIPTING) {
		}

		~ScriptingComponent() {
			updateFunc.Dispose();
			initFunc.Dispose();
			destroyFunc.Dispose();
		}

		void bind(IBinder& binder);

		bool isActive() const {
			return active;
		}

		void setActive(bool active) {
			this->active = active;
			if (active && invalid) {
				compile();
			}
		}

		void compile() {
			compiled = false;
			invalid = false;
		}

		bool isCompiled() const {
			return compiled;
		}

		bool isInvalid() const {
			return invalid;
		}

		bool isScriptEmpty() {
			return scriptFile.empty();
		}

		const std::string& getScriptFile() const {
			return scriptFile;
		}

		void setScriptFile(const std::string& scriptFile) {
			this->scriptFile = scriptFile;
			scriptFileChanged();
		}

		void clearScriptFile() {
			scriptFile = "";
			active = false;
			compiled = false;
			invalid = true;
		}

	private:
		void bindableValueChanged(void* ptr) override;
		void scriptFileChanged() {
			active = true;
			compile();
		}
	};

} /* namespace rev */

#endif /* REVSCRIPTINGCOMPONENT_H_ */
