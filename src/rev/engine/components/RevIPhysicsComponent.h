/*
 * RevIPhysicsComponent.h
 *
 *  Created on: 27-04-2013
 *      Author: Revers
 */

#ifndef REVIPHYSICSCOMPONENT_H_
#define REVIPHYSICSCOMPONENT_H_

#include <rev/engine/math/RevReal.h>
#include "RevIComponent.h"

namespace rev {

	enum class PhysicsComponentType {
		RIGID_BODY, MASS_PARTICLE, PARTICLE_SET
	};

	class IPhysicsComponent: public IComponent {
	public:

		IPhysicsComponent() :
				IComponent(ComponentType::PHYSICS) {
		}

		virtual void update(real timestep) = 0;
		virtual PhysicsComponentType getType() = 0;

	};
} /* namespace rev */


#endif /* REVIPHYSICSCOMPONENT_H_ */
