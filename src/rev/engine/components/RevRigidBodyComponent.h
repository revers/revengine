/*
 * RevRigidBodyComponent.h
 *
 *  Created on: 21-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVRIGIDBODYCOMPONENT_H_
#define REVRIGIDBODYCOMPONENT_H_

#include "RevIPhysicsComponent.h"

namespace rev {

	class RigidBodyContact;

	class RigidBodyComponent: public IPhysicsComponent {
		friend class RigidBodyContact;

		real sleepEpsilon = real(0.3);

		/**
		 * Holds the inverse of the mass of the rigid body. It
		 * is more useful to hold the inverse mass because
		 * integration is simpler, and because in real time
		 * simulation it is more useful to have bodies with
		 * infinite mass (immovable) than zero mass
		 * (completely unstable in numerical simulation).
		 */
		real inverseMass;

		/**
		 * Holds the inverse of the body's inertia tensor. The
		 * inertia tensor provided must not be degenerate
		 * (that would mean the body had zero inertia for
		 * spinning along one axis). As long as the tensor is
		 * finite, it will be invertible. The inverse tensor
		 * is used for similar reasons to the use of inverse
		 * mass.
		 *
		 * The inertia tensor, unlike the other variables that
		 * define a rigid body, is given in body space.
		 *
		 * @see inverseMass
		 */
		rmat3 inverseInertiaTensor;

		/**
		 * Holds the amount of damping applied to linear
		 * motion.  Damping is required to remove energy added
		 * through numerical instability in the integrator.
		 */
		real linearDamping = real(0.95);

		/**
		 * Holds the amount of damping applied to angular
		 * motion.  Damping is required to remove energy added
		 * through numerical instability in the integrator.
		 */
		real angularDamping = real(0.8);

		/**
		 * Holds the linear position of the rigid body in
		 * world space.
		 *
		 * At the beginning of each frame of physics simulation
		 * this value is set to be a copy of rigidBody->getPerant()->getPosition().
		 *
		 * At the end of each frame of physics simulation the parent's position
		 * will be update according to this variable
		 * (rigidBody->getPerant()->setPosition(rigidBody->position)).
		 */
		rvec3 position;

		/**
		 * Holds the angular orientation of the rigid body in
		 * world space.
		 *
		 * At the beginning of each frame of physics simulation
		 * this value is set to be a copy of rigidBody->getPerant()->getOrientation().
		 *
		 * At the end of each frame of physics simulation the parent's orientation
		 * will be update according to this variable
		 * (rigidBody->getPerant()->setOrientation(rigidBody->orientation)).
		 */
		rquat orientation;

		/**
		 * Holds the linear velocity of the rigid body in
		 * world space.
		 */
		rvec3 velocity;

		/**
		 * Holds the angular velocity, or rotation, or the
		 * rigid body in world space.
		 */
		rvec3 angularVelocity;

		/**
		 * @name Derived Data
		 *
		 * These data members hold information that is derived from
		 * the other data in the class.
		 */

		/**
		 * Holds the inverse inertia tensor of the body in world
		 * space. The inverse inertia tensor member is specified in
		 * the body's local space.
		 *
		 * @see inverseInertiaTensor
		 */
		rmat3 inverseInertiaTensorWorld;

		/**
		 * Holds the amount of motion of the body. This is a recency
		 * weighted mean that can be used to put a body to sleap.
		 */
		real motion = real(0.0);

		/**
		 * A body can be put to sleep to avoid it being updated
		 * by the integration functions or affected by collisions
		 * with the world.
		 */
		bool isAwake = true;

		/**
		 * Some bodies may never be allowed to fall asleep.
		 * User controlled bodies, for example, should be
		 * always awake.
		 */
		bool canSleep = true;

		/**
		 * Holds a transform matrix for converting body space into
		 * world space and vice versa. This can be achieved by calling
		 * the getPointIn*Space functions.
		 *
		 * @see getPointInLocalSpace
		 * @see getPointInWorldSpace
		 * @see getTransform
		 */
		rmat4 transformMatrix;

		/**
		 * Holds the accumulated force to be applied at the next
		 * integration step.
		 */
		rvec3 forceAccum;

		/**
		 * Holds the accumulated torque to be applied at the next
		 * integration step.
		 */
		rvec3 torqueAccum;

		/**
		 * Holds the acceleration of the rigid body.  This value
		 * can be used to set acceleration due to gravity (its primary
		 * use), or any other constant acceleration.
		 */
		rvec3 constantAcceleration = rvec3(0, -5, 0);

		/**
		 * Holds the linear acceleration of the rigid body, for the
		 * previous frame.
		 */
		rvec3 lastFrameAcceleration;

		/**
		 * Holds the lateral friction coefficient at the contact.
		 */
		real friction = real(0.3);

		/**
		 * Holds the normal restitution coefficient at the contact.
		 */
		real restitution = real(0.3);

	public:
		DECLARE_BINDABLE(RigidBodyComponent)

		RigidBodyComponent() {

		}
		~RigidBodyComponent() {
		}

		PhysicsComponentType getType() override {
			return PhysicsComponentType::RIGID_BODY;
		}

		void update(real timestep) override;

		/**
		 * Calculates internal data from state data. This should be called
		 * after the body's state is altered directly (it is called
		 * automatically during integration). If you change the body's state
		 * and then intend to integrate before querying any data (such as
		 * the transform matrix), then you can ommit this step.
		 */
		void calculateDerivedData();

		/**
		 * Sets the mass of the rigid body.
		 *
		 * @param mass The new mass of the body. This may not be zero.
		 * Small masses can produce unstable rigid bodies under
		 * simulation.
		 *
		 * @warning This invalidates internal data for the rigid body.
		 * Either an integration function, or the calculateInternals
		 * function should be called before trying to get any settings
		 * from the rigid body.
		 */
		void setMass(const real mass);

		/**
		 * Gets the mass of the rigid body.
		 *
		 * @return The current mass of the rigid body.
		 */
		real getMass() const;

		/**
		 * Sets the inverse mass of the rigid body.
		 *
		 * @param inverseMass The new inverse mass of the body. This
		 * may be zero, for a body with infinite mass
		 * (i.e. unmovable).
		 *
		 * @warning This invalidates internal data for the rigid body.
		 * Either an integration function, or the calculateInternals
		 * function should be called before trying to get any settings
		 * from the rigid body.
		 */
		void setInverseMass(const real inverseMass) {
			this->inverseMass = inverseMass;
		}

		/**
		 * Gets the inverse mass of the rigid body.
		 *
		 * @return The current inverse mass of the rigid body.
		 */
		real getInverseMass() const {
			return inverseMass;
		}

		/**
		 * Returns true if the mass of the body is not-infinite.
		 */
		bool hasFiniteMass() const {
			return inverseMass >= real(0.0);
		}

		/**
		 * Sets the intertia tensor for the rigid body.
		 *
		 * @param inertiaTensor The inertia tensor for the rigid
		 * body. This must be a full rank matrix and must be
		 * invertible.
		 *
		 * @warning This invalidates internal data for the rigid body.
		 * Either an integration function, or the calculateInternals
		 * function should be called before trying to get any settings
		 * from the rigid body.
		 */
		void setInertiaTensor(const rmat3& inertiaTensor);

		/**
		 * Gets a copy of the current inertia tensor of the rigid body.
		 *
		 * @return A new matrix containing the current intertia
		 * tensor. The inertia tensor is expressed in the rigid body's
		 * local space.
		 */
		rmat3 getInertiaTensor() const;

		/**
		 * Gets a copy of the current inertia tensor of the rigid body.
		 *
		 * @return A new matrix containing the current intertia
		 * tensor. The inertia tensor is expressed in world space.
		 */
		rmat3 getInertiaTensorWorld() const;

		/**
		 * Sets the inverse intertia tensor for the rigid body.
		 *
		 * @param inverseInertiaTensor The inverse inertia tensor for
		 * the rigid body. This must be a full rank matrix and must be
		 * invertible.
		 *
		 * @warning This invalidates internal data for the rigid body.
		 * Either an integration function, or the calculateInternals
		 * function should be called before trying to get any settings
		 * from the rigid body.
		 */
		void setInverseInertiaTensor(const rmat3& inverseInertiaTensor);

		/**
		 * Gets a copy of the current inverse inertia tensor of the
		 * rigid body.
		 *
		 * @return A new matrix containing the current inverse
		 * intertia tensor. The inertia tensor is expressed in the
		 * rigid body's local space.
		 */
		const rmat3& getInverseInertiaTensor() const {
			return inverseInertiaTensor;
		}

		/**
		 * Gets a copy of the current inverse inertia tensor of the
		 * rigid body.
		 *
		 * @return A new matrix containing the current inverse
		 * intertia tensor. The inertia tensor is expressed in world
		 * space.
		 */
		const rmat3& getInverseInertiaTensorWorld() const {
			return inverseInertiaTensorWorld;
		}

		void setDamping(const real linearDamping, const real angularDamping) {
			this->linearDamping = linearDamping;
			this->angularDamping = angularDamping;
		}

		void setLinearDamping(const real linearDamping) {
			this->linearDamping = linearDamping;
		}

		real getLinearDamping() const {
			return linearDamping;
		}

		void setAngularDamping(const real angularDamping) {
			this->angularDamping = angularDamping;
		}

		real getAngularDamping() const {
			return angularDamping;
		}

		void setPosition(const rvec3& p);
		void setPosition(const real x, const real y, const real z);

		const rvec3& getPosition() const {
			return position;
		}

		/**
		 * Sets the orientation of the rigid body.
		 *
		 * @param orientation The new orientation of the rigid body.
		 *
		 * @note The given orientation does not need to be normalized.
		 */
		void setOrientation(const rquat& o);

		const rquat& getOrientation() const {
			return orientation;
		}

		/**
		 * Gets a transformation representing the rigid body's
		 * position and orientation.
		 *
		 * @note Transforming a vector by this matrix turns it from
		 * the body's local space to world space.
		 *
		 * @return The transform matrix for the rigid body.
		 */
		const rmat4& getTransform() const {
			return transformMatrix;
		}

		/**
		 * Converts the given point from world space into the body's
		 * local space.
		 *
		 * @param point The point to covert, given in world space.
		 *
		 * @return The converted point, in local space.
		 */
		rvec3 getPointInLocalSpace(const rvec3& point) const;

		/**
		 * Converts the given point from world space into the body's
		 * local space.
		 *
		 * @param point The point to covert, given in local space.
		 *
		 * @return The converted point, in world space.
		 */
		rvec3 getPointInWorldSpace(const rvec3& point) const;

		/**
		 * Converts the given direction from world space into the
		 * body's local space.
		 *
		 * @note When a direction is converted between frames of
		 * reference, there is no translation required.
		 *
		 * @param direction The direction to covert, given in world
		 * space.
		 *
		 * @return The converted direction, in local space.
		 */
		rvec3 getDirectionInLocalSpace(const rvec3& direction) const;

		/**
		 * Converts the given direction from world space into the
		 * body's local space.
		 *
		 * @note When a direction is converted between frames of
		 * reference, there is no translation required.
		 *
		 * @param direction The direction to covert, given in local
		 * space.
		 *
		 * @return The converted direction, in world space.
		 */
		rvec3 getDirectionInWorldSpace(const rvec3& direction) const;

		/**
		 * Sets the velocity of the rigid body.
		 *
		 * @param velocity The new velocity of the rigid body. The
		 * velocity is given in world space.
		 */
		void setVelocity(const rvec3& velocity) {
			this->velocity = velocity;
			isAwake = true;
		}

		/**
		 * Sets the velocity of the rigid body by component. The
		 * velocity is given in world space.
		 */
		void setVelocity(const real x, const real y, const real z) {
			velocity.x = x;
			velocity.y = y;
			velocity.z = z;
			isAwake = true;
		}

		/**
		 * Gets the velocity of the rigid body.
		 *
		 * @return The velocity of the rigid body. The velocity is
		 * given in world local space.
		 */
		const rvec3& getVelocity() const {
			return velocity;
		}

		/**
		 * Applies the given change in velocity.
		 */
		void addVelocity(const rvec3& deltaVelocity) {
			velocity += deltaVelocity;
			isAwake = true;
		}

		/**
		 * Sets the angular velocity of the rigid body.
		 *
		 * @param angularVelocity The new angular velocity of the rigid body. The
		 * angular velocity is given in world space.
		 */
		void setAngularVelocity(const rvec3& angularVelocity) {
			this->angularVelocity = angularVelocity;
			isAwake = true;
		}

		/**
		 * Sets the angular velocity of the rigid body by component. The
		 * angular velocity is given in world space.
		 */
		void setAngularVelocity(const real x, const real y, const real z) {
			angularVelocity.x = x;
			angularVelocity.y = y;
			angularVelocity.z = z;
			isAwake = true;
		}

		/**
		 * Gets the angular velocity of the rigid body.
		 *
		 * @return The angular velocity of the rigid body. The angular velocity is
		 * given in world local space.
		 */
		const rvec3& getAngularVelocity() const {
			return angularVelocity;
		}

		/**
		 * Applies the given change in the angular velocity.
		 */
		void addAngularVelocity(const rvec3& deltaAngularVelocity) {
			angularVelocity += deltaAngularVelocity;
		}

		/**
		 * Returns true if the body is awake and responding to
		 * integration.
		 *
		 * @return The awake state of the body.
		 */
		bool getAwake() const {
			return isAwake;
		}

		/**
		 * Sets the awake state of the body. If the body is set to be
		 * not awake, then its velocities are also cancelled, since
		 * a moving body that is not awake can cause problems in the
		 * simulation.
		 *
		 * @param awake The new awake state of the body.
		 */
		void setAwake(const bool awake = true);

		/**
		 * Returns true if the body is allowed to go to sleep at
		 * any time.
		 */
		bool getCanSleep() const {
			return canSleep;
		}

		/**
		 * Sets whether the body is ever allowed to go to sleep. Bodies
		 * under the player's control, or for which the set of
		 * transient forces applied each frame are not predictable,
		 * should be kept awake.
		 *
		 * @param canSleep Whether the body can now be put to sleep.
		 */
		void setCanSleep(const bool canSleep = true);

		/**
		 * Gets the current accumulated value for linear
		 * acceleration. The acceleration accumulators are set during
		 * the integration step. They can be read to determine the
		 * rigid body's acceleration over the last integration
		 * step. The linear acceleration is given in world space.
		 *
		 * @return The rigid body's linear acceleration.
		 */
		const rvec3& getLastFrameAcceleration() const {
			return lastFrameAcceleration;
		}

		/**
		 * Clears the forces and torques in the accumulators. This will
		 * be called automatically after each intergration step.
		 */
		void clearAccumulators() {
			forceAccum = rvec3(0);
			torqueAccum = rvec3(0);
		}

		/**
		 * Adds the given force to center of mass of the rigid body.
		 * The force is expressed in world-coordinates.
		 *
		 * @param force The force to apply.
		 */
		void addForce(const rvec3& force) {
			forceAccum += force;
			isAwake = true;
		}

		/**
		 * Adds the given force to the given point on the rigid body.
		 * Both the force and the
		 * application point are given in world space. Because the
		 * force is not applied at the center of mass, it may be split
		 * into both a force and torque.
		 *
		 * @param force The force to apply.
		 *
		 * @param point The location at which to apply the force, in
		 * world-coordinates.
		 */
		void addForceAtPoint(const rvec3& force, const rvec3& point);

		/**
		 * Adds the given force to the given point on the rigid body.
		 * The direction of the force is given in world coordinates,
		 * but the application point is given in body space. This is
		 * useful for spring forces, or other forces fixed to the
		 * body.
		 *
		 * @param force The force to apply.
		 *
		 * @param point The location at which to apply the force, in
		 * body-coordinates.
		 */
		void addForceAtBodyPoint(const rvec3& force, const rvec3& point);

		/**
		 * Adds the given torque to the rigid body.
		 * The force is expressed in world-coordinates.
		 */
		void addTorque(const rvec3& torque) {
			torqueAccum += torque;
			isAwake = true;
		}

		void setConstantAcceleration(const rvec3& acceleration) {
			this->constantAcceleration = acceleration;
		}

		void setConstantAcceleration(const real x, const real y, const real z) {
			constantAcceleration.x = x;
			constantAcceleration.y = y;
			constantAcceleration.z = z;
		}

		const rvec3& getConstantAcceleration() const {
			return constantAcceleration;
		}

	private:
		void updateParent();

	protected:
		void bind(IBinder& binder) override;
		void bindableValueChanged(void* ptr);
	};

} /* namespace rev */
#endif /* REVRIGIDBODYCOMPONENT_H_ */
