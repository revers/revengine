/*
 * RevShadowSourceComponent.h
 *
 *  Created on: 27 lip 2013
 *      Author: Revers
 */

#ifndef REVSHADOWSOURCECOMPONENT_H_
#define REVSHADOWSOURCECOMPONENT_H_

#include "RevIComponent.h"

namespace rev {

class ShadowSourceComponent: public IComponent {

	float viewAngle = 60.0f;
	float viewNear = 0.3f;
	float viewFar = 10000.0f;
	glm::mat4 projectionMatrix;

public:
	DECLARE_BINDABLE(ShadowSourceComponent)

	ShadowSourceComponent() :
			IComponent(ComponentType::SHADOW) {
		calcProjectionMatrix();
	}

	~ShadowSourceComponent() {
	}

	const glm::mat4& getProjectionMatrix() const {
		return projectionMatrix;
	}

protected:
	void bind(IBinder& binder) override;
	void bindableValueChanged(void* ptr) override;
	void calcProjectionMatrix();
};

} /* namespace rev */
#endif /* REVSHADOWSOURCECOMPONENT_H_ */
