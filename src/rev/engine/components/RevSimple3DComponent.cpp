/*
 * RevSimple3DComponent.cpp
 *
 *  Created on: 23-01-2013
 *      Author: Revers
 */

#include "RevSimple3DComponent.h"

using namespace rev;

IMPLEMENT_BINDABLE(Simple3DComponent)

Simple3DComponent::Simple3DComponent(IEffect* effect, bool effectOwner,
		IVBODrawable* drawable, bool drawableOwner) :
		effect(effect), effectOwner(effectOwner), drawable(
				drawable), drawableOwner(drawableOwner) {
}

Simple3DComponent::~Simple3DComponent() {
	if (effectOwner && effect) {
		delete effect;
		effect = nullptr;
	}
	if (drawableOwner && drawable) {
		delete drawable;
		drawable = nullptr;
	}
}

void Simple3DComponent::bind(IBinder& binder) {
	IVisual3DComponent::bind(binder);
	binder.bindSimpleObjExpanded(effect);
	binder.bindSimpleObjExpanded(drawable);
	binder.bindSimpleInvisible(effectOwner);
	binder.bindSimpleInvisible(drawableOwner);
}

void Simple3DComponent::setEffect(IEffect* effect, bool effectOwner) {
	if (this->effect && this->effectOwner) {
		delete this->effect;
	}
	this->effectOwner = effectOwner;
	this->effect = effect;
}

void Simple3DComponent::setDrawable(IVBODrawable* drawable, bool drawableOwner) {
	if (this->drawable && this->drawableOwner) {
		delete this->drawable;
	}
	this->drawableOwner = drawableOwner;
	this->drawable = drawable;
}
