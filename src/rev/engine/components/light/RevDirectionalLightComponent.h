/*
 * RevDirectionalLightComponent.h
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#ifndef REVDIRECTIONALLIGHTCOMPONENT_H_
#define REVDIRECTIONALLIGHTCOMPONENT_H_

#include "RevILightComponent.h"

namespace rev {

class DirectionalLightComponent: public ILightComponent {
	glm::vec3 direction = glm::vec3(0.0f, -1.0f, 0.0f);

public:
	DECLARE_BINDABLE(DirectionalLightComponent)

	DirectionalLightComponent() {
	}

	const glm::vec3& getDirection() const {
		return direction;
	}

	void setDirection(const glm::vec3& direction) {
		this->direction = direction;
	}

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVDIRECTIONALLIGHTCOMPONENT_H_ */
