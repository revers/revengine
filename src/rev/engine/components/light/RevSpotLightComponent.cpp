/*
 * RevSpotLightComponent.cpp
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#include "RevSpotLightComponent.h"

namespace rev {

IMPLEMENT_BINDABLE(SpotLightComponent)

void SpotLightComponent::bind(IBinder& binder) {
	PointLightComponent::bind(binder);
	binder.bindSimple(direction);
	binder.bindSimple(cutoff);
}

} /* namespace rev */
