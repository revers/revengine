/*
 * RevPointLightComponent.cpp
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#include "RevPointLightComponent.h"

namespace rev {

IMPLEMENT_BINDABLE(PointLightComponent)

void PointLightComponent::bind(IBinder& binder) {
	ILightComponent::bind(binder);
	binder.bindSimple(constantAttenuation);
	binder.bindSimple(linearAttenuation);
	binder.bindSimple(expAttenuation);
}

} /* namespace rev */
