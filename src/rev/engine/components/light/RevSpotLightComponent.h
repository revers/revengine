/*
 * RevSpotLightComponent.h
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#ifndef REVSPOTLIGHTCOMPONENT_H_
#define REVSPOTLIGHTCOMPONENT_H_

#include "RevPointLightComponent.h"

namespace rev {

class SpotLightComponent: public PointLightComponent {
	glm::vec3 direction = glm::vec3(0.0f, -1.0f, 0.0f);
	float cutoff = 1.0f;

public:
	DECLARE_BINDABLE(SpotLightComponent)

	SpotLightComponent() {
	}

	const glm::vec3& getDirection() const {
		return direction;
	}

	void setDirection(const glm::vec3& direction) {
		this->direction = direction;
	}

	float getCutoff() const {
		return cutoff;
	}

	void setCutoff(float cutoff) {
		this->cutoff = cutoff;
	}

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVSPOTLIGHTCOMPONENT_H_ */
