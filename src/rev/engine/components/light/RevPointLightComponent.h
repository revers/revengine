/*
 * RevPointLightComponent.h
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#ifndef REVPOINTLIGHTCOMPONENT_H_
#define REVPOINTLIGHTCOMPONENT_H_

#include "RevILightComponent.h"

namespace rev {

class PointLightComponent: public ILightComponent {
	float constantAttenuation = 1.0f;
	float linearAttenuation = 0.1f;
	float expAttenuation = 0.0f;

public:
	DECLARE_BINDABLE(PointLightComponent)

	PointLightComponent() {
	}

	float getConstantAttenuation() const {
		return constantAttenuation;
	}

	void setConstantAttenuation(float constantAttenuation) {
		this->constantAttenuation = constantAttenuation;
	}

	float getExpAttenuation() const {
		return expAttenuation;
	}

	void setExpAttenuation(float expAttenuation) {
		this->expAttenuation = expAttenuation;
	}

	float getLinearAttenuation() const {
		return linearAttenuation;
	}

	void setLinearAttenuation(float linearAttenuation) {
		this->linearAttenuation = linearAttenuation;
	}

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVPOINTLIGHTCOMPONENT_H_ */
