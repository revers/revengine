/*
 * RevDirectionalLightComponent.cpp
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#include "RevDirectionalLightComponent.h"

namespace rev {

IMPLEMENT_BINDABLE(DirectionalLightComponent)

void DirectionalLightComponent::bind(IBinder& binder) {
	ILightComponent::bind(binder);
	binder.bindSimple(direction);
}

} /* namespace rev */
