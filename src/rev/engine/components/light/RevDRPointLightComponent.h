/*
 * RevDRDRPointLightComponent.h
 *
 *  Created on: 2 sie 2013
 *      Author: Revers
 */

#ifndef REVDRPOINTLIGHTCOMPONENT_H_
#define REVDRPOINTLIGHTCOMPONENT_H_

#include "RevILightComponent.h"

namespace rev {

class DRPointLightComponent: public ILightComponent {
	float attenuation = 1.0f;

public:
	DECLARE_BINDABLE(DRPointLightComponent)

	DRPointLightComponent() {
	}

	float getAttenuation() const {
		return attenuation;
	}

	void setAttenuation(float attenuation) {
		this->attenuation = attenuation;
	}

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVDRPOINTLIGHTCOMPONENT_H_ */
