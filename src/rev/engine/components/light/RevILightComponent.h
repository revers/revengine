/*
 * RevIILightComponent.h
 *
 *  Created on: 19 lip 2013
 *      Author: Revers
 */

#ifndef REVILIGHTCOMPONENT_H_
#define REVILIGHTCOMPONENT_H_

#include <rev/gl/RevColor.h>
#include <rev/engine/components/RevIComponent.h>

namespace rev {

class ILightComponent: public IComponent {
	bool enabled = true;
	rev::color3 color = rev::color3(1.0f, 1.0f, 1.0f);
	float ambientIntensity = 0.2f;
	float diffuseIntensity = 0.8f;

public:
	ILightComponent() :
			IComponent(ComponentType::LIGHT) {
	}

	const rev::color3& getColor() const {
		return color;
	}

	void setColor(const rev::color3& color) {
		this->color = color;
	}

	float getAmbientIntensity() const {
		return ambientIntensity;
	}

	void setAmbientIntensity(float ambientIntensity) {
		this->ambientIntensity = ambientIntensity;
	}

	float getDiffuseIntensity() const {
		return diffuseIntensity;
	}

	void setDiffuseIntensity(float diffuseIntensity) {
		this->diffuseIntensity = diffuseIntensity;
	}

	bool isEnabled() const {
		return enabled;
	}

	void setEnabled(bool enabled) {
		this->enabled = enabled;
	}

protected:
	void bind(IBinder& binder) override {
		binder.bindSimple(enabled);
		binder.bindSimple(color);
		binder.bindSimple(ambientIntensity);
		binder.bindSimple(diffuseIntensity);
	}
};

} /* namespace rev */

#endif /* REVILIGHTCOMPONENT_H_ */
