/*
 * RevDRDRPointLightComponent.cpp
 *
 *  Created on: 2 sie 2013
 *      Author: Revers
 */

#include "RevDRPointLightComponent.h"

namespace rev {

IMPLEMENT_BINDABLE(DRPointLightComponent)

void DRPointLightComponent::bind(IBinder& binder) {
	ILightComponent::bind(binder);
	binder.bindSimple(attenuation);
}

} /* namespace rev */
