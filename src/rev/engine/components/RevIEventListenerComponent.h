/*
 * RevIEventListenerComponent.h
 *
 *  Created on: 30-12-2012
 *      Author: Revers
 */

#ifndef REVEVENTLISTENERCOMPONENT_H_
#define REVEVENTLISTENERCOMPONENT_H_

#include "RevIComponent.h"
#include <rev/engine/events/RevMouseEvent.h>
#include <rev/engine/events/RevMouseWheelEvent.h>
#include <rev/engine/events/RevKeyEvent.h>
#include <rev/engine/events/RevPickingEvent.h>
#include <rev/engine/events/RevGameObjectEvent.h>
#include <rev/engine/events/RevScriptEvent.h>

namespace rev {

    class IEventListenerComponent: public IComponent {
    public:
        IEventListenerComponent() :
                IComponent(ComponentType::EVENT_LISTENER) {
        }

        virtual void mousePressed(const MouseEvent& e) {
        }

        virtual void mouseReleased(const MouseEvent& e) {
        }

        virtual void mouseMoved(const MouseEvent& e) {
        }

        virtual void keyPressed(const KeyEvent& e) {
        }

        virtual void keyReleased(const KeyEvent& e) {
        }

        virtual void mouseWheelMoved(const MouseWheelEvent& e) {
        }

        virtual void objectPicked(const PickingEvent& e) {
        }

        virtual void update() {
        }

        virtual void gameObjectInit() {
        }

        virtual void gameObjectDestroy() {
        }

        virtual void allGameObjectsRemoved(const GameObjectEvent& e) {
        }

        virtual void gameObjectAdded(const GameObjectEvent& e) {
        }

        virtual void gameObjectRemoved(const GameObjectEvent& e) {
        }

        virtual void scriptingEvent(const ScriptEvent& e) {
        }

        virtual void pickingReseted() {
        }

        virtual void sceneLoaded() {
        }

    protected:
        virtual void bind(IBinder& binder) override {
        }
    };

} /* namespace rev */
#endif /* REVEVENTLISTENERCOMPONENT_H_ */
