/*
 * RevParticleFountainTFTFComponent.h
 *
 *  Created on: 17 lip 2013
 *      Author: Revers
 */

#ifndef REVPARTICLEFOUNTAINTFCOMPONENT_H_
#define REVPARTICLEFOUNTAINTFCOMPONENT_H_

#include <random>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/gl/RevGLTexture2D.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevIVisual3DComponent.h>

namespace rev {

class ParticleFountainTFComponent: public IVisual3DComponent {
	enum {
		ATTRIBUTE_POSITION,
		ATTRIBUTE_VELOCITY,
		ATTRIBUTE_START_TIME,
		ATTRIBUTE_INITIAL_VELOCITY,
	};

	GLuint posBuf[2] = { 0, 0 };
	GLuint velBuf[2] = { 0, 0 };
	GLuint startTime[2] = { 0, 0 };
	GLuint initVel = 0;

	GLuint feedback[2] = { 0, 0 };

	GLuint drawBuf = 0;
	GLuint query = 0;

	GLuint renderSub = 0;
	GLuint updateSub = 0;

	const int SEED = 12345;
	std::mt19937 engine = std::mt19937(SEED);
	std::uniform_real_distribution<float> distribution = std::uniform_real_distribution<float>(0.0,
			1.0);

	GLSLProgram* program = nullptr;
	GLTexture2D texture;

	glm::vec3 acceleration = glm::vec3(0.0f, -0.4f, 0.0f);
	bool paused = false;
	bool alternativeBlend = false;
	float time = 0.0f;
	float deltaT = 0.0001f;
	float particleLifetime = 3.5f;
	float particleSize = 10.0f;
	bool depthTest = true;

	BoundingSphere boundingSphere;
	int particleCount = 8000;

	unsigned int vaoHandle[REV_ENGINE_MAX_CONTEXTS * 2];
	bool vaoInited[REV_ENGINE_MAX_CONTEXTS];

	ParticleFountainTFComponent();

public:
	DECLARE_BINDABLE(ParticleFountainTFComponent)

	ParticleFountainTFComponent(int particleCount);

	~ParticleFountainTFComponent();

	void update() override;

	void applyEffect(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix, const ICamera& camera) override;

	void render() override;
	void bindableValueChanged(void* ptr) override;

	const rev::BoundingSphere* getBoundingSphere() override {
		return &boundingSphere;
	}

	int getPrimitiveCount() override {
		return particleCount;
	}

	RenderPrimitiveType getRenderPrimitiveType() const override {
		return RenderPrimitiveType::POINTS;
	}

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;

private:
	void init();
	bool generate();
	void initVAO(int contextIndex);
	void loadTexture();

	void clearVAOHandles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoHandle[i * 2] = 0;
			vaoHandle[i * 2 + 1] = 0;
			vaoInited[i] = false;
		}
	}

	static rev::GLSLProgram* createGLSLProgram();

	void resetVAOHAndles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoInited[i] = false;
		}
	}

	float randFloat() {
		return distribution(engine);
	}
};

} /* namespace rev */
#endif /* REVPARTICLEFOUNTAINTFCOMPONENT_H_ */
