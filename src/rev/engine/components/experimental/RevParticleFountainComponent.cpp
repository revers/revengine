/*
 * RevParticleFountainComponent.cpp
 *
 *  Created on: 14 lip 2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevParticleFountainComponent.h"

using namespace glm;

#define SHADER_FILE "shaders/ParticleFountain.glsl"
#define TEXTURE_FILE "images/bluewater.png"
//#define TEXTURE_FILE "images/particle.png"

namespace rev {

IMPLEMENT_BINDABLE(ParticleFountainComponent)

void ParticleFountainComponent::init() {
	IVisual3DComponent::canDrawOutline = false;
	clearVAOHandles();

	glGenBuffers(HANDLES_SIZE, handles);
	glAssert;

	boundingSphere.r = 10.0f;
}

ParticleFountainComponent::ParticleFountainComponent() {
	init();
}

ParticleFountainComponent::ParticleFountainComponent(int particleCount) :
		particleCount(particleCount) {
	init();
	initAfterDeserialization();
}

ParticleFountainComponent::~ParticleFountainComponent() {
	glDeleteBuffers(HANDLES_SIZE, handles);

#if REV_ENGINE_MAX_CONTEXTS > 1
	GLContextManager::ref().scheduleVAOsToDestroy(vaoHandle);
#else
	glDeleteVertexArrays(1, &vaoHandle[0]);
#endif

	if (program) {
		delete program;
	}
}

bool ParticleFountainComponent::initAfterDeserialization() {
	program = createGLSLProgram();
	loadTexture();
	return generate();
}

void ParticleFountainComponent::loadTexture() {
	std::string path = ResourcePath::get(TEXTURE_FILE);
	bool succ = texture.load(path.c_str());
	if (!succ) {
		REV_ERROR_MSG("Failed to load texture file: " << path);
		revAssert(succ);
	}

}

void ParticleFountainComponent::update() {
	if (!paused) {
		time += Engine::ref().getCurrentFrameTime() * 0.001;
	}
}

void ParticleFountainComponent::applyEffect(const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const ICamera& camera) {
	program->use();
	mat4 mvp = projectionMatrix * camera.getViewMatrix() * modelMatrix;

	program->setUniform("ParticleTex", 0);
	program->setUniform("Gravity", gravity);
	program->setUniform("Period", period);
	program->setUniform("Time", time);
	program->setUniform("ParticleSize", particleSize);
	program->setUniform("ParticleLifetime", particleLifetime);
	program->setUniform("MVP", mvp);

	texture.bind();
}

void ParticleFountainComponent::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glEnable(GL_BLEND);
	if (!depthTest) {
		glDisable(GL_DEPTH_TEST);
	}

	if (alternativeBlend) {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	} else {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	glDrawArrays(GL_POINTS, 0, particleCount);
	glBindVertexArray(0);

	if (!depthTest) {
		glEnable(GL_DEPTH_TEST);
	}
	glDisable(GL_BLEND);
}

void ParticleFountainComponent::bindableValueChanged(void* ptr) {
	if (&particleCount == ptr) {
		generate();
	}
}

bool ParticleFountainComponent::generate() {
	// Allocate space for all buffers
	int size = particleCount * 3 * sizeof(float);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_INITIAL_VELOCITY]);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_START_TIME]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(float), NULL, GL_STATIC_DRAW);

	// Fill the first velocity buffer with random velocities
	vec3 v(0.0f);
	float velocity, theta, phi;
	GLfloat *data = new GLfloat[particleCount * 3];
	for (uint i = 0; i < particleCount; i++) {

		theta = glm::mix(0.0f, (float) PI / 6.0f, randFloat());
		phi = glm::mix(0.0f, (float) TWO_PI, randFloat());

		v.x = sinf(theta) * cosf(phi);
		v.y = cosf(theta);
		v.z = sinf(theta) * sinf(phi);

		velocity = glm::mix(1.25f, 1.5f, randFloat());
		v = glm::normalize(v) * velocity * 5.0f;

		data[3 * i] = v.x;
		data[3 * i + 1] = v.y;
		data[3 * i + 2] = v.z;
	}
	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_INITIAL_VELOCITY]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);

	// Fill the start time buffer
	delete[] data;
	data = new GLfloat[particleCount];
	float time = 0.0f;
	float rate = 0.00075f;
	for (uint i = 0; i < particleCount; i++) {
		data[i] = time;
		time += rate;
	}
	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_START_TIME]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * sizeof(float), data);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	delete[] data;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void ParticleFountainComponent::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}
	vaoInited[contextIndex] = true;
	glBindVertexArray(vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_INITIAL_VELOCITY]);
	glVertexAttribPointer(ATTRIBUTE_INITIAL_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_INITIAL_VELOCITY);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handles[ATTRIBUTE_START_TIME]);
	glVertexAttribPointer(ATTRIBUTE_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_START_TIME);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glAssert;
}

rev::GLSLProgram* ParticleFountainComponent::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG(
				"Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), ATTRIBUTE_INITIAL_VELOCITY, "VertexInitVel");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_START_TIME, "StartTime");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " SHADER_FILE " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ParticleFountainComponent::bind(IBinder& binder) {
	IVisual3DComponent::bind(binder);
	binder.bindSimple(paused);
	binder.bindSimple(period);
	binder.bindSimple(particleCount);
	binder.bindSimple(gravity);
	binder.bindSimple(particleLifetime);
	binder.bindSimple(alternativeBlend);
	binder.bindSimple(particleSize);
	binder.bindSimple(depthTest);
}

}
/* namespace rev */
