/*
 * RevParticleFountainComponent.h
 *
 *  Created on: 14 lip 2013
 *      Author: Revers
 */

#ifndef REVPARTICLEFOUNTAINCOMPONENT_H_
#define REVPARTICLEFOUNTAINCOMPONENT_H_

#include <random>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/gl/RevGLTexture2D.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevIVisual3DComponent.h>

namespace rev {

class ParticleFountainComponent: public IVisual3DComponent {
	enum {
		ATTRIBUTE_INITIAL_VELOCITY,
		ATTRIBUTE_START_TIME,
		HANDLES_SIZE
	};
	unsigned int handles[HANDLES_SIZE];

	const int SEED = 12345;
	std::mt19937 engine = std::mt19937(SEED);
	std::uniform_real_distribution<float> distribution = std::uniform_real_distribution<float>(0.0,
			1.0);

	GLSLProgram* program = nullptr;
	GLTexture2D texture;

	bool paused = false;
	bool alternativeBlend = false;
	float period = 5.0;
	glm::vec3 gravity = glm::vec3(0.0, -0.5, 0.0);
	float time = 0.0;
	float particleLifetime = 3.5f;
	float particleSize = 10.0f;
	bool depthTest = true;

	BoundingSphere boundingSphere;
	int particleCount = 8000;

	unsigned int vaoHandle[REV_ENGINE_MAX_CONTEXTS];
	bool vaoInited[REV_ENGINE_MAX_CONTEXTS];

	ParticleFountainComponent();

public:
	DECLARE_BINDABLE(ParticleFountainComponent)

	ParticleFountainComponent(int particleCount);

	~ParticleFountainComponent();

	void update() override;

	void applyEffect(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix, const ICamera& camera) override;

	void render() override;
	void bindableValueChanged(void* ptr) override;

	const rev::BoundingSphere* getBoundingSphere() override {
		return &boundingSphere;
	}

	int getPrimitiveCount() override {
		return particleCount;
	}

	RenderPrimitiveType getRenderPrimitiveType() const override {
		return RenderPrimitiveType::POINTS;
	}

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;

private:
	void init();
	bool generate();
	void initVAO(int contextIndex);
	void loadTexture();

	void clearVAOHandles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoHandle[i] = 0;
			vaoInited[i] = false;
		}
	}

	static rev::GLSLProgram* createGLSLProgram();

	void resetVAOHAndles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoInited[i] = false;
		}
	}

	float randFloat() {
		return distribution(engine);
	}
};

} /* namespace rev */
#endif /* REVPARTICLEFOUNTAINCOMPONENT_H_ */
