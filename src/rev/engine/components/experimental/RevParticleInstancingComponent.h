/*
 * RevParticleInstancingComponent.h
 *
 *  Created on: 15 lip 2013
 *      Author: Revers
 */

#ifndef REVPARTICLEINSTANCINGCOMPONENT_H_
#define REVPARTICLEINSTANCINGCOMPONENT_H_

#include <random>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/engine/config/RevEngineConfig.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/drawables/RevVBOTorusVNT.h>

namespace rev {

class ParticleInstancingComponent: public IVisual3DComponent {
	enum {
		ATTRIBUTE_VERTEX,
		ATTRIBUTE_NORMAL,
		ATTRIBUTE_TEXTURE,
		ATTRIBUTE_INITIAL_VELOCITY,
		ATTRIBUTE_START_TIME,
	};
	unsigned int handleInitVel = 0;
	unsigned int handleStartTime = 0;

	const int SEED = 12345;
	std::mt19937 engine = std::mt19937(SEED);
	std::uniform_real_distribution<float> distribution = std::uniform_real_distribution<float>(0.0,
			1.0);

	GLSLProgram* program = nullptr;
	VBOTorusVNT* torus = nullptr;

	bool paused = false;
	float period = 5.0;
	glm::vec3 gravity = glm::vec3(0.0, -0.5, 0.0);
	float time = 0.0;
	float particleLifetime = 3.5f;

	BoundingSphere boundingSphere;
	int particleCount = 500;

	bool vaoInited[REV_ENGINE_MAX_CONTEXTS];

	ParticleInstancingComponent();

public:
	DECLARE_BINDABLE(ParticleInstancingComponent)

	ParticleInstancingComponent(int particleCount);

	~ParticleInstancingComponent();

	void update() override;

	void applyEffect(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix, const ICamera& camera) override;

	void render() override;
	void bindableValueChanged(void* ptr) override;

	const rev::BoundingSphere* getBoundingSphere() override {
		return &boundingSphere;
	}

	int getPrimitiveCount() override {
		return particleCount * torus->getPrimitiveCount();
	}

	RenderPrimitiveType getRenderPrimitiveType() const override {
		return RenderPrimitiveType::POINTS;
	}

protected:
	void bind(IBinder& binder) override;
	bool initAfterDeserialization() override;

private:
	void init();
	bool generate();
	void initVAO(int contextIndex);

	void clearVAOHandles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoInited[i] = false;
		}
	}

	static rev::GLSLProgram* createGLSLProgram();

	void resetVAOHAndles() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaoInited[i] = false;
		}
	}

	float randFloat() {
		return distribution(engine);
	}
};

} /* namespace rev */
#endif /* REVPARTICLEINSTANCINGCOMPONENT_H_ */
