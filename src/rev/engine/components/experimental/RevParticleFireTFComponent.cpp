/*
 * RevParticleFireTFComponent.cpp
 *
 *  Created on: 18 lip 2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevParticleFireTFComponent.h"

using namespace glm;

#define SHADER_FILE "shaders/ParticleFireTF.glsl"
#define TEXTURE_FILE "images/fire.png"

namespace rev {

IMPLEMENT_BINDABLE(ParticleFireTFComponent)

void ParticleFireTFComponent::init() {
	IVisual3DComponent::canDrawOutline = false;
	clearVAOHandles();

	// Generate the buffers
	glGenBuffers(2, posBuf);    // position buffers
	glGenBuffers(2, velBuf);    // velocity buffers
	glGenBuffers(2, startTime); // Start time buffers
	glGenBuffers(1, &initVel);  // Initial velocity buffer (never changes, only need one)
	glGenQueries(1, &query);
	glAssert;

	boundingSphere.r = 10.0f;
}

ParticleFireTFComponent::ParticleFireTFComponent() {
	init();
}

ParticleFireTFComponent::ParticleFireTFComponent(int particleCount) :
		particleCount(particleCount) {
	init();
	initAfterDeserialization();
}

ParticleFireTFComponent::~ParticleFireTFComponent() {
	glDeleteBuffers(2, posBuf);    // position buffers
	glDeleteBuffers(2, velBuf);    // velocity buffers
	glDeleteBuffers(2, startTime); // Start time buffers
	glDeleteBuffers(1, &initVel);  // Initial velocity buffer (never changes, only need one)

	glDeleteQueries(1, &query);

#if REV_ENGINE_MAX_CONTEXTS > 1
	GLContextManager::ref().scheduleVAOsToDestroy(vaoHandle, 2);
#else
	glDeleteVertexArrays(2, &vaoHandle[0]);
#endif

	glDeleteTransformFeedbacks(2, feedback);

	if (program) {
		delete program;
	}
}

bool ParticleFireTFComponent::initAfterDeserialization() {
	program = createGLSLProgram();

	program->use();
	renderSub = program->getSubroutineIndex("render", GL_VERTEX_SHADER);
	updateSub = program->getSubroutineIndex("update", GL_VERTEX_SHADER);

	loadTexture();
	return generate();
}

void ParticleFireTFComponent::loadTexture() {
	std::string path = ResourcePath::get(TEXTURE_FILE);
	bool succ = texture.load(path.c_str());
	if (!succ) {
		REV_ERROR_MSG("Failed to load texture file: " << path);
		revAssert(succ);
	}
}

void ParticleFireTFComponent::update() {
}

void ParticleFireTFComponent::applyEffect(const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const ICamera& camera) {
	program->use();
	mat4 mvp = projectionMatrix * camera.getViewMatrix() * modelMatrix;

	program->setUniform("ParticleTex", 0);
	program->setUniform("Time", time);
	program->setUniform("ParticleSize", particleSize);
	program->setUniform("ParticleLifetime", particleLifetime);
	program->setUniform("MVP", mvp);
	program->setUniform("Accel", acceleration);
	program->setUniform("H", deltaT);

	texture.bind();
}

void ParticleFireTFComponent::render() {
	int index = 0;
#if REV_ENGINE_MAX_CONTEXTS > 1
	index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
#endif

	if (!paused) {
		deltaT = Engine::ref().getCurrentFrameTime() * 0.001;
		time += deltaT;
	}
	program->use();

	if (!paused) {
		//----------------------------------------------------------
		// Update pass
		program->setSubroutine(updateSub, GL_VERTEX_SHADER);

		glEnable(GL_RASTERIZER_DISCARD);

		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, feedback[drawBuf]);
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
		glBeginTransformFeedback(GL_POINTS);

		glBindVertexArray(vaoHandle[index * 2 + drawBuf]);
		glDrawArrays(GL_POINTS, 0, particleCount);
		glEndTransformFeedback();

		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
		glDisable(GL_RASTERIZER_DISCARD);

		// GLuint ptsWritten;
		// glGetQueryObjectuiv(query, GL_QUERY_RESULT, &ptsWritten);
		// REV_DEBUG_MSG("Written: " << ptsWritten);

		// Render pass0
		program->setSubroutine(renderSub, GL_VERTEX_SHADER);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
	}

	// Swap buffers
	drawBuf = 1 - drawBuf;
	glBindVertexArray(vaoHandle[index * 2 + drawBuf]);

	glAssert;
	//==========================================================
	glEnable(GL_BLEND);
	if (!depthTest) {
		glDisable(GL_DEPTH_TEST);
	}

	if (alternativeBlend) {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	} else {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	glDrawArrays(GL_POINTS, 0, particleCount);

	if (!depthTest) {
		glEnable(GL_DEPTH_TEST);
	}
	glDisable(GL_BLEND);

	glBindVertexArray(0);

	//texture.unbind();
	//program->unuse();
}

void ParticleFireTFComponent::bindableValueChanged(void* ptr) {
	if (&particleCount == ptr) {
		generate();
	}
}

bool ParticleFireTFComponent::generate() {
	// Allocate space for all buffers
	int size = particleCount * 3 * sizeof(float);

	glBindVertexArray(0);
	// Allocate space for all buffers
	glBindBuffer(GL_ARRAY_BUFFER, posBuf[0]);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, posBuf[1]);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, velBuf[0]);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, velBuf[1]);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, initVel);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, startTime[0]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(float), NULL, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, startTime[1]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(float), NULL, GL_DYNAMIC_COPY);

	// Fill the first position buffer with zeroes
	GLfloat *data = new GLfloat[particleCount * 3];
	for (int i = 0; i < particleCount * 3; i += 3) {
		data[i] = glm::mix(-2.0f, 2.0f, randFloat());
		data[i + 1] = 0.0f;
		data[i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, posBuf[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);

	// Fill the first velocity buffer with random velocities
	for (uint i = 0; i < particleCount; i++) {
		data[3 * i] = 0.0f;
		data[3 * i + 1] = glm::mix(0.1f, 0.5f, randFloat());
		data[3 * i + 2] = 0.0f;
	}
	glBindBuffer(GL_ARRAY_BUFFER, velBuf[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
	glBindBuffer(GL_ARRAY_BUFFER, initVel);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);

	// Fill the first start time buffer
	delete[] data;
	data = new GLfloat[particleCount];
	float time = 0.0f;
	float rate = 0.001f;
	for (int i = 0; i < particleCount; i++) {
		data[i] = time;
		time += rate;
	}
	glBindBuffer(GL_ARRAY_BUFFER, startTime[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * sizeof(float), data);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	delete[] data;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void ParticleFireTFComponent::initVAO(int contextIndex) {
	vaoInited[contextIndex] = true;

	contextIndex *= 2;
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(2, &vaoHandle[contextIndex]);
	}
	glBindVertexArray(vaoHandle[contextIndex]);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, posBuf[0]);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, velBuf[0]);
	glVertexAttribPointer(ATTRIBUTE_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_VELOCITY);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, startTime[0]);
	glVertexAttribPointer(ATTRIBUTE_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_START_TIME);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, initVel);
	glVertexAttribPointer(ATTRIBUTE_INITIAL_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_INITIAL_VELOCITY);
	//-----------------------------------------------------------------

	glBindVertexArray(vaoHandle[contextIndex + 1]);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, posBuf[1]);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, velBuf[1]);
	glVertexAttribPointer(ATTRIBUTE_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_VELOCITY);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, startTime[1]);
	glVertexAttribPointer(ATTRIBUTE_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_START_TIME);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, initVel);
	glVertexAttribPointer(ATTRIBUTE_INITIAL_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_INITIAL_VELOCITY);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Setup the feedback objects
	glGenTransformFeedbacks(2, feedback);

	// Transform feedback 0
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, feedback[0]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_POSITION, posBuf[0]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_VELOCITY, velBuf[0]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_START_TIME, startTime[0]);

	// Transform feedback 1
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, feedback[1]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_POSITION, posBuf[1]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_VELOCITY, velBuf[1]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, ATTRIBUTE_START_TIME, startTime[1]);

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	glAssert;
}

rev::GLSLProgram* ParticleFireTFComponent::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG(
				"Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), ATTRIBUTE_POSITION, "VertexPosition");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_VELOCITY, "VertexVelocity");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_START_TIME, "VertexStartTime");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_INITIAL_VELOCITY, "VertexInitialVelocity");

	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	//////////////////////////////////////////////////////
	// Setup the transform feedback (must be done before linking the program)
	GLuint progHandle = program->getHandle();
	const char* outputNames[] = { "Position", "Velocity", "StartTime" };
	glTransformFeedbackVaryings(progHandle, 3, outputNames, GL_SEPARATE_ATTRIBS);
	///////////////////////////////////////////////////////
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " SHADER_FILE " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ParticleFireTFComponent::bind(IBinder& binder) {
	IVisual3DComponent::bind(binder);
	binder.bindSimple(paused);
	binder.bindSimple(particleCount);
	binder.bindSimple(particleLifetime);
	binder.bindSimple(alternativeBlend);
	binder.bindSimple(particleSize);
	binder.bindSimple(depthTest);
	binder.bindSimple(acceleration);
}

} /* namespace rev */
