/*
 * RevParticleInstancingComponent.cpp
 *
 *  Created on: 15 lip 2013
 *      Author: Revers
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevParticleInstancingComponent.h"

using namespace glm;

#define SHADER_FILE "shaders/ParticleInstancing.glsl"

namespace rev {

IMPLEMENT_BINDABLE(ParticleInstancingComponent)

void ParticleInstancingComponent::init() {
	IVisual3DComponent::canDrawOutline = false;
	clearVAOHandles();

	glGenBuffers(1, &handleInitVel);
	glGenBuffers(1, &handleStartTime);

	boundingSphere.r = 10.0f;
}

ParticleInstancingComponent::ParticleInstancingComponent() {
	init();
}

ParticleInstancingComponent::ParticleInstancingComponent(int particleCount) :
		particleCount(particleCount) {
	init();
	initAfterDeserialization();
}

ParticleInstancingComponent::~ParticleInstancingComponent() {
	glDeleteBuffers(1, &handleInitVel);
	glDeleteBuffers(1, &handleStartTime);

	if (torus) {
		delete torus;
	}
	if (program) {
		delete program;
	}
}

void ParticleInstancingComponent::update() {
	if (!paused) {
		time += Engine::ref().getCurrentFrameTime() * 0.001;
	}
}

void ParticleInstancingComponent::applyEffect(const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const ICamera& camera) {
	program->use();

	program->setUniform("LightPosition", vec4(0.0f, 0.0f, 0.0f, 1.0f));
	program->setUniform("Material.Kd", 0.9f, 0.5f, 0.2f);
	program->setUniform("Material.Ks", 0.95f, 0.95f, 0.95f);
	program->setUniform("Material.Ka", 0.1f, 0.1f, 0.1f);
	program->setUniform("Material.Shininess", 100.0f);

	program->setUniform("Gravity", gravity);
	program->setUniform("Period", period);
	program->setUniform("Time", time);
	program->setUniform("ParticleLifetime", particleLifetime);

	mat4 mv = viewMatrix * modelMatrix;
	program->setUniform("ModelViewMatrix", mv);
	program->setUniform("NormalMatrix", mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	program->setUniform("ProjectionMatrix", projectionMatrix);
}

void ParticleInstancingComponent::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(torus->vaoHandle[index]);
#else
	glBindVertexArray(torus->vaoHandle[0]);
#endif

	glAssert;
	glDrawElementsInstanced(GL_TRIANGLES, 6 * torus->faces, GL_UNSIGNED_INT, 0, particleCount);
	glDrawArrays(GL_POINTS, 0, particleCount);
	glAssert;
	glBindVertexArray(0);
}

bool ParticleInstancingComponent::initAfterDeserialization() {
	if (!torus) {
		torus = new VBOTorusVNT();
		torus->outerRadius = 2.0f / 6.0f;
		torus->innerRadius = 1.0f / 6.0f;
		torus->rings = 20;
		torus->sides = 20;
	}
	program = createGLSLProgram();
	return generate();
}

void ParticleInstancingComponent::bindableValueChanged(void* ptr) {
	if (&particleCount == ptr) {
		generate();
	}
}

bool ParticleInstancingComponent::generate() {
	REV_DEBUG_MSG("generate 1");
	if (!torus->generate()) {
		revAssert(false);
		return false;
	}
	REV_DEBUG_MSG("generate 2");

	// Allocate space for all buffers
	int size = particleCount * 3 * sizeof(float);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handleInitVel);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, handleStartTime);
	glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(float), NULL, GL_STATIC_DRAW);

	// Fill the first velocity buffer with random velocities
	vec3 v(0.0f);
	float velocity, theta, phi;
	GLfloat *data = new GLfloat[particleCount * 3];
	for (uint i = 0; i < particleCount; i++) {

		theta = glm::mix(0.0f, (float) PI / 6.0f, randFloat());
		phi = glm::mix(0.0f, (float) TWO_PI, randFloat());

		v.x = sinf(theta) * cosf(phi);
		v.y = cosf(theta);
		v.z = sinf(theta) * sinf(phi);

		velocity = glm::mix(1.25f, 1.5f, randFloat());
		v = glm::normalize(v) * velocity * 5.0f;

		data[3 * i] = v.x;
		data[3 * i + 1] = v.y;
		data[3 * i + 2] = v.z;
	}
	glBindBuffer(GL_ARRAY_BUFFER, handleInitVel);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);

	// Fill the start time buffer
	delete[] data;
	data = new GLfloat[particleCount];
	float time = 0.0f;
	float rate = 0.00075f;
	for (uint i = 0; i < particleCount; i++) {
		data[i] = time;
		time += rate;
	}
	glBindBuffer(GL_ARRAY_BUFFER, handleStartTime);
	glBufferSubData(GL_ARRAY_BUFFER, 0, particleCount * sizeof(float), data);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	delete[] data;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void ParticleInstancingComponent::initVAO(int contextIndex) {
	torus->initVAO(contextIndex);
	vaoInited[contextIndex] = true;
	glBindVertexArray(torus->vaoHandle[contextIndex]);

	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handleInitVel);
	glVertexAttribPointer(ATTRIBUTE_INITIAL_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_INITIAL_VELOCITY);
	glVertexAttribDivisor(ATTRIBUTE_INITIAL_VELOCITY, 1);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handleStartTime);
	glVertexAttribPointer(ATTRIBUTE_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_START_TIME);
	glVertexAttribDivisor(ATTRIBUTE_START_TIME, 1);
	//-----------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glAssert;
}

rev::GLSLProgram* ParticleInstancingComponent::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG(
				"Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), ATTRIBUTE_VERTEX, "VertexPosition");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_NORMAL, "VertexNormal");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_TEXTURE, "VertexTexCoord");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_INITIAL_VELOCITY, "VertexInitialVelocity");
	glBindAttribLocation(program->getHandle(), ATTRIBUTE_START_TIME, "StartTime");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " SHADER_FILE " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ParticleInstancingComponent::bind(IBinder& binder) {
	IVisual3DComponent::bind(binder);
	binder.bindSimple(paused);
	binder.bindSimple(period);
	binder.bindSimple(particleCount);
	binder.bindSimple(gravity);
	binder.bindSimple(particleLifetime);
	binder.bindSimpleObjExpanded(torus);
}

}
/* namespace rev */
