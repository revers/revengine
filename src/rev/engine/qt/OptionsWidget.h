/*
 * OptionsWidget.h
 *
 *  Created on: 29 lip 2013
 *      Author: Revers
 */

#ifndef OPTIONSWIDGET_H_
#define OPTIONSWIDGET_H_

#include <QWidget>
#include "ui_OptionsWidget.h"

namespace rev {

class GameEditorWindow;

class OptionsWidget: public QWidget {
Q_OBJECT

	GameEditorWindow* mainWindow;
	Ui::OptionsWidget widget;

public:
	OptionsWidget(GameEditorWindow* mainWindow);
	virtual ~OptionsWidget();

	void refreshEngineControls();

private slots:
	void showNormalsSelected(bool checked);
	void wireframeModeSelected(bool checked);
	void drawDebugElemsSelected(bool checked);
	void drawLocAxesSelected(bool checked);
	void drawBSSelected(bool checked);
	void drawObjectsSelected(bool checked);
	void drawAllBSSelected(bool checked);
	void drawAllCollisionBSsSelected(bool checked);
	void drawAllCollisionGeometrySelected(bool checked);
	void drawOutlineSelected(bool checked);
	void drawCameraFrustumSelected(bool checked);
	void drawShadowFrustumSelected(bool checked);
	void drawShadowDepthSelected(bool checked);
	void shadowsEnabledSelected(bool checked);
	void cullingEnabledSelected(bool checked);
	void deferredEnabledSelected(bool checked);
	void drawDeferredNormalsSelected(bool checked);
	void drawDeferredDepthSelected(bool checked);
	void drawDeferredSpheresSelected(bool checked);
	void drawDeferredLightTexSelected(bool checked);
	void ssaoEnabledSelected(bool checked);
	void drawSsaoTextureSelected(bool checked);

private:
	void setupActions();
};

} /* namespace rev */
#endif /* OPTIONSWIDGET_H_ */
