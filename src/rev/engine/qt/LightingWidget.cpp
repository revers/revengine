/*
 * LightingWidget.cpp
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/lighting/RevSSAORenderer.h>
#include "GameEditorWindow.h"
#include "LightingWidget.h"

namespace rev {

static const float biasValueMin = 0.0;
static const float biasValueMax = 0.2;

static const float radiusMin = 0.0;
static const float radiusMax = 40.0;

static const float constAttenMin = 0.0;
static const float constAttenMax = 2.0;

static const float linearAttenMin = 0.0;
static const float linearAttenMax = 10.0;

LightingWidget::LightingWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	widget.setupUi(this);

	setupActions();

	widget.biasSlider->setValue(getBiasValue());
	widget.constAttenSlider->setValue(getConstAtten());
	widget.linearAttenSlider->setValue(getLinearAtten());
	widget.radiusSlider->setValue(getRadius());
}

LightingWidget::~LightingWidget() {
}

void LightingWidget::setupActions() {
	connect(widget.biasSlider, SIGNAL(valueChanged(int)),
			this, SLOT(biasValueChanged(int)));
	connect(widget.radiusSlider, SIGNAL(valueChanged(int)),
			this, SLOT(radiusValueChanged(int)));
	connect(widget.constAttenSlider, SIGNAL(valueChanged(int)),
			this, SLOT(constAttenValueChanged(int)));
	connect(widget.linearAttenSlider, SIGNAL(valueChanged(int)),
			this, SLOT(linearAttenValueChanged(int)));
}

void LightingWidget::biasValueChanged(int value) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	setBiasValue(value);
	widget.biasL->setText(QString::number(renderer.getOccluderBias()));
}

void LightingWidget::radiusValueChanged(int value) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	setRadius(value);
	widget.radiusL->setText(QString::number(renderer.getSamplingRadius()));
}

void LightingWidget::constAttenValueChanged(int value) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	setConstAtten(value);
	widget.constAttenL->setText(QString::number(renderer.getConstAttenuation()));
}

void LightingWidget::linearAttenValueChanged(int value) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	setLinearAtten(value);
	widget.linearAttenL->setText(QString::number(renderer.getLinearAttenuation()));
}

int LightingWidget::getBiasValue() const {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float val = renderer.getOccluderBias() - biasValueMin;
	float range = biasValueMax - biasValueMin;
	return int((val / range) * 100.0f);
}

void LightingWidget::setBiasValue(int biasValue) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float range = biasValueMax - biasValueMin;
	float val = biasValueMin + float(biasValue) * 0.01f * range;
	renderer.setOccluderBias(val);
}

int LightingWidget::getConstAtten() const {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float val = renderer.getConstAttenuation() - constAttenMin;
	float range = constAttenMax - constAttenMin;
	return int((val / range) * 100.0f);
}

void LightingWidget::setConstAtten(int constAtten) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float range = constAttenMax - constAttenMin;
	float val = constAttenMin + float(constAtten) * 0.01f * range;
	renderer.setConstAttenuation(val);
}

int LightingWidget::getLinearAtten() const {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float val = renderer.getLinearAttenuation() - linearAttenMin;
	float range = linearAttenMax - linearAttenMin;
	return int((val / range) * 100.0f);

}

void LightingWidget::setLinearAtten(int linearAtten) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float range = linearAttenMax - linearAttenMin;
	float val = linearAttenMin + float(linearAtten) * 0.01f * range;
	renderer.setLinearAttenuation(val);
}

int LightingWidget::getRadius() const {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float val = renderer.getSamplingRadius() - radiusMin;
	float range = radiusMax - radiusMin;
	return int((val / range) * 100.0f);
}

void LightingWidget::setRadius(int radius) {
	SSAORenderer& renderer = LightingManager::ref().getSSAORenderer();
	float range = radiusMax - radiusMin;
	float val = radiusMin + float(radius) * 0.01f * range;
	renderer.setSamplingRadius(val);
}

} /* namespace rev */
