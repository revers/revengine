/*
 * GameWidgetsController.h
 *
 *  Created on: 12-01-2013
 *      Author: Revers
 */

#ifndef GAMEWIDGETSCONTROLLER_H_
#define GAMEWIDGETSCONTROLLER_H_

#include <qobject.h>
#include <QModelIndex>
#include <rev/engine/components/RevIEventListenerComponent.h>

class QEvent;

namespace Ui {
	class GameEditorWindowUi;
}

namespace rev {
	class GameObject;
	class GameGLWidget;
	class GameEditorWindow;
	class GameObjectTableModel;
	class GameObjectTableDelegate;
}

namespace rev {

	class GameWidgetsController: public QObject, public IEventListenerComponent {
	Q_OBJECT

		Ui::GameEditorWindowUi* widget = nullptr;
		rev::GameGLWidget* glWidget = nullptr;
		rev::GameEditorWindow* editorWindow = nullptr;
		rev::GameObjectTableModel* objectListModel = nullptr;
		rev::GameObjectTableDelegate* objectListDelegate = nullptr;
		bool leftMousePressed = false;

	public:
		DECLARE_WEAK_BINDABLE(GameWidgetsController)

		GameWidgetsController(rev::GameEditorWindow* editorWindow);
		virtual ~GameWidgetsController();

		void objectPicked(const rev::PickingEvent& e) override;
		void update() override;
		void mousePressed(const MouseEvent& e) override;
		void mouseReleased(const MouseEvent& e) override;
		void mouseMoved(const MouseEvent& e) override;
		void keyPressed(const KeyEvent& e) override;
		void keyReleased(const KeyEvent& e) override;
		void gameObjectAdded(const rev::GameObjectEvent& e) override;
		void gameObjectRemoved(const rev::GameObjectEvent& e) override;
		void allGameObjectsRemoved(const GameObjectEvent& e) override;
		void pickingReseted() override;
		void scriptingEvent(const ScriptEvent& e) override;
		void clearObjectList();
		void sceneLoaded();

		GameObjectTableModel* getObjectListModel() {
			return objectListModel;
		}

	private:
		void bind(IBinder& binder) override {
		}

		void selectGameObject(GameObject& obj);

	private slots:
		void objectTableDoubleClicked(const QModelIndex& index);
		void scriptErrorMsgSlot(const QString& objName);

	signals:
		void scriptErrorMsgSignal(const QString& objName);
	};

} /* namespace rev */
#endif /* GAMEWIDGETSCONTROLLER_H_ */
