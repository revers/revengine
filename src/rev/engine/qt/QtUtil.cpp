/*
 * QtUtil.cpp
 *
 *  Created on: 15-03-2013
 *      Author: Revers
 */

#include <QApplication>
#include <QCursor>
#include <QPoint>

#include "QtUtil.h"
#include "GameEditorWindow.h"

rev::MouseState rev::QtUtil::getMouseState() {
	int buttons = (int) QApplication::mouseButtons();
	GameEditorWindow* gew = GameEditorWindow::getInstance();

	QPoint p = gew->getFocusedGlWidget()->mapFromGlobal(QCursor::pos());
	return MouseState(buttons, p.x(), p.y());
}
