/*
 * GameGLWidget.h
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#ifndef GAMEGLWIDGET_H_
#define GAMEGLWIDGET_H_

#include <GL/glew.h>
#include <QGLWidget>

#include <rev/engine/glcontext/RevGLContextManager.h>

class QMouseEvent;
class QWheelEvent;
class QKeyEvent;
class QFocusEvent;

namespace Ui {
    class GameEditorWindowUi;
}

namespace rev {
	class GameEditorWindow;

    class GameGLWidget: public QGLWidget {
    Q_OBJECT

        int width, height;
        bool mousePressed = false;
        Ui::GameEditorWindowUi* widget;
        int contextIndex = -1;
        GameEditorWindow* gameEditorWindow;

    public:
        GameGLWidget(Ui::GameEditorWindowUi* widget,
                const QGLFormat& format, int contextIndex,
                QWidget* parent = 0);

        GameGLWidget(Ui::GameEditorWindowUi* widget,
                QGLWidget* shareWidget, int contextIndex,
                QWidget* parent = 0);

        virtual ~GameGLWidget();

    protected:
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;
        void wheelEvent(QWheelEvent* event) override;
        void keyPressEvent(QKeyEvent* event) override;
        void keyReleaseEvent(QKeyEvent* event) override;
		void focusInEvent(QFocusEvent* e) override;

        void initializeGL() override;
        void paintGL() override;
        void resizeGL(int w, int h) override;

    private:
        void setupActions();

    };

} /* namespace rev */
#endif /* GAMEGLWIDGET_H_ */
