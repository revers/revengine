/*
 * ScriptingWidget.h
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#ifndef SCRIPTINGWIDGET_H_
#define SCRIPTINGWIDGET_H_

#include <functional>
#include <QWidget>
#include "ui_ScriptingWidget.h"

class QMenu;
class QAction;
class QItemSelection;

namespace rev {

	class ScriptingTableModel;
	class ScriptingComponent;
	class GameEditorWindow;

	class ScriptingWidget: public QWidget {
	Q_OBJECT

		GameEditorWindow* mainWindow;
		ScriptingTableModel* scriptingModel;
		Ui::ScriptingWidget uiWidget;
		int selectedCount = -1;

		QMenu* tableMenu = nullptr;
		QAction* activateMenuAction = nullptr;
		QAction* pauseMenuAction = nullptr;
		QAction* recompileMenuAction = nullptr;
		QAction* pickMenuAction = nullptr;
		QAction* editMenuAction = nullptr;
		QAction* openMenuAction = nullptr;

	public:
		ScriptingWidget(GameEditorWindow* mainWindow);
		virtual ~ScriptingWidget();

		void refreshEngineControls();
		void refreshTable();
		void refreshStatistics();

		void refresh() {
			refreshTable();
			refreshEngineControls();
			refreshStatistics();
		}

		ScriptingTableModel* getModel() {
			return scriptingModel;
		}

		void init();
		void addMainScript();
		void addScriptingComponent(ScriptingComponent* component);
		void removeScriptingComponent(ScriptingComponent* component);
		void removeAllScriptingComponents();
		void addAllScriptingComponents();

	private slots:
		void updateStatisticsTimerAction();
		void tableDoubleClicked(const QModelIndex& index);
		void displayTableMenu(const QPoint& pos);
		void activateSelectedAction();
		void pauseSelectedAction();
		void recompileSelectedAction();
		void pickSelectedAction();
		void editSelectedAction();
		void openSelectedAction();
		void pauseScriptingSelected(bool checked);
		void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected);

	private:

		void initTableMenu();

		void setupActions();
		void updateSelectedCount();
		void applyForSelectedScripts(std::function<void(ScriptingComponent*)> callback,
				bool refresh = true);
	};

} /* namespace rev */
#endif /* SCRIPTINGWIDGET_H_ */
