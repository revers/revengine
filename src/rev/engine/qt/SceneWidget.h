/*
 * SceneWidget.h
 *
 *  Created on: 16 lip 2013
 *      Author: Revers
 */

#ifndef SCENEWIDGET_H_
#define SCENEWIDGET_H_

#include <QWidget>
#include <rev/gl/RevColor.h>
#include "ui_SceneWidget.h"

class QtTreePropertyBrowser;

namespace rev {

class GameEditorWindow;

class SceneWidget: public QWidget {
Q_OBJECT

	GameEditorWindow* mainWindow;
	Ui::SceneWidget uiWidget;
	QColor backgroundColor;

public:
	SceneWidget(GameEditorWindow* mainWindow);
	virtual ~SceneWidget();

	const QColor& getBackgroundColor() const {
		return backgroundColor;
	}

	void setBackgroundColor(const QColor& backgroundColor) {
		this->backgroundColor = backgroundColor;
		setButtonColor(backgroundColor);
	}

	void setBackgroundColor(const rev::color3& c) {
		this->backgroundColor = QColor(int(255.0f * c.r), int(255.0f * c.g), int(255.0f * c.b));
		setButtonColor(backgroundColor);
	}

	void setSceneName(const char* name);

private slots:
	void backgroundColorAction();

private:
	void setupActions();
	void setButtonColor(const QColor& color);
};

} /* namespace rev */

#endif /* SCENEWIDGET_H_ */
