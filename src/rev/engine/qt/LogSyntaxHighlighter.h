/*
 * LogSyntaxHighlighter.h
 *
 *  Created on: 25-01-2013
 *      Author: Revers
 */

#ifndef LOGSYNTAXHIGHLIGHTER_H_
#define LOGSYNTAXHIGHLIGHTER_H_

#include <QSyntaxHighlighter>

namespace rev {

    class LogSyntaxHighlighter: public QSyntaxHighlighter {
        Q_OBJECT

    public:
        LogSyntaxHighlighter(QTextDocument* document);

        ~LogSyntaxHighlighter();

        void highlightBlock(const QString& text);
    };

} /* namespace rev */
#endif /* LOGSYNTAXHIGHLIGHTER_H_ */
