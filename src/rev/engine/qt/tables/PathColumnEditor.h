/*
 * PathColumnEditor.h
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#ifndef PATHCOLUMNEDITOR_H_
#define PATHCOLUMNEDITOR_H_

#include <QWidget>
#include <QModelIndex>

class QPushButton;
class QLineEdit;

namespace rev {

	class ScriptingComponent;

	class PathColumnEditor: public QWidget {
	Q_OBJECT

		QPushButton* browseButton;
		QLineEdit* pathLineEdit;
		QModelIndex index;
		ScriptingComponent* component = nullptr;

	public:
		PathColumnEditor(const QModelIndex& index, QWidget* parent = 0);
		~PathColumnEditor() {
		}

	private slots:
		void browseButtonPressed();
	};

} /* namespace rev */
#endif /* PATHCOLUMNEDITOR_H_ */
