/*
 * ScriptingTableModel.cpp
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include "ScriptingTableModel.h"

using namespace rev;

void ScriptingTableModel::addScriptingComponent(ScriptingComponent* obj) {
	int index = dataVector.size();
	beginInsertRows(QModelIndex(), index, index);
	dataVector.push_back(obj);
	endInsertRows();
}

void ScriptingTableModel::removeScriptingComponent(ScriptingComponent* obj) {
	REV_TRACE_FUNCTION;

	for (int i = 0; i < dataVector.size(); i++) {
		ScriptingComponent*& go = dataVector[i];
		if (go == obj) {
			beginRemoveRows(QModelIndex(), i, i);
			dataVector.erase(i);
			endRemoveRows();
			break;
		}
	}
}

void ScriptingTableModel::removeAllScriptingComponents() {
	if (dataVector.empty()) {
		return;
	}
	beginRemoveRows(QModelIndex(), 0, dataVector.size() - 1);
	dataVector.clear();
	endRemoveRows();
}

int ScriptingTableModel::getScriptingComponentIndex(const ScriptingComponent* obj) {
	for (int i = 0; i < dataVector.size(); i++) {
		ScriptingComponent*& go = dataVector[i];
		if (go == obj) {
			return i;
		}
	}

	return -1;
}

int ScriptingTableModel::rowCount(const QModelIndex& parent) const {
	return dataVector.size();
}

int ScriptingTableModel::columnCount(const QModelIndex& parent) const {
	return SCRIPTING_COLUMN_COUNT;
}

QVariant ScriptingTableModel::data(const QModelIndex& index, int role) const {
	if (role == Qt::DisplayRole) {
		ScriptingComponent* c = dataVector[index.row()];
		GameObject* go = c->getParent();

		if (index.column() == SCRIPTING_COLUMN_ID) {
			return go->getId();
		} else if (index.column() == SCRIPTING_COLUMN_NAME) {
			const std::string& name = go->getName();
			return QString::fromUtf8(name.data(), name.size());
		} else if (index.column() == SCRIPTING_COLUMN_ACTIVE) {
			QString value;
			if (c->isActive()) {
				value = "active";
			} else {
				value = "not active";
			}
			return value;
		} else if (index.column() == SCRIPTING_COLUMN_STATUS) {
			QString value;
			if (c->isInvalid()) {
				value = "invalid";
			} else if (c->isCompiled()) {
				value = "compiled";
			} else {
				value = "not compiled";
			}
			return value;
		} else if (index.column() == SCRIPTING_COLUMN_PATH) {
			const std::string& path = c->getScriptFile();
			return QString::fromUtf8(path.data(), path.size());
		}
	} else if (role == Qt::UserRole) {
		return qVariantFromValue((void*) (dataVector[index.row()]));
	} else if (role == Qt::ToolTipRole) {
		ScriptingComponent* c = dataVector[index.row()];
		if (index.column() == SCRIPTING_COLUMN_ACTIVE) {
			QString value;
			if (c->isActive()) {
				value = "Active";
			} else {
				value = "Not active";
			}
			return value;
		} else if (index.column() == SCRIPTING_COLUMN_STATUS) {
			QString value;
			if (c->isInvalid()) {
				value = "Invalid";
			} else if (c->isCompiled()) {
				value = "Compiled";
			} else {
				value = "Not compiled";
			}
			return value;
		}
	} else if (role == Qt::TextAlignmentRole) {
		if (index.column() == SCRIPTING_COLUMN_ID) {
			return Qt::AlignCenter;
		}
		return Qt::AlignLeft;
	} else if (role == Qt::EditRole) {
		if (index.column() == SCRIPTING_COLUMN_PATH) {
			ScriptingComponent* c = dataVector[index.row()];
			const std::string& path = c->getScriptFile();
			return QString::fromUtf8(path.data(), path.size());
		}
	}
	return QVariant::Invalid;
}

QVariant ScriptingTableModel::headerData(int section, Qt::Orientation orientation,
		int role) const {
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section == SCRIPTING_COLUMN_ID) {
				return QString("ID");
			} else if (section == SCRIPTING_COLUMN_NAME) {
				return QString("Name");
			} else if (section == SCRIPTING_COLUMN_ACTIVE) {
				return QString("Active");
			} else if (section == SCRIPTING_COLUMN_STATUS) {
				return QString("Status");
			} else if (section == SCRIPTING_COLUMN_PATH) {
				return QString("Path");
			}
		}
	}

	return QVariant::Invalid;
}

Qt::ItemFlags ScriptingTableModel::flags(const QModelIndex& index) const {
	if (!index.isValid()) {
		return Qt::ItemIsEnabled;
	}
	if (index.column() == SCRIPTING_COLUMN_PATH) {
		return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
	}
	return QAbstractTableModel::flags(index);
}

// http://pepper.troll.no/s60prereleases/doc/itemviews-addressbook.html
bool ScriptingTableModel::setData(const QModelIndex& index, const QVariant& value,
		int role) {
//	if (index.isValid() && role == Qt::EditRole) {
//		int row = index.row();
//
//		//QPair<QString, QString> p = listOfPairs.value(row);
//		ScriptingComponent* c = dataVector[index.row()];
//
//		if (index.column() == SCRIPTING_COLUMN_ACTIVE) {
//			REV_TRACE_MSG((value.toBool() ? "true" : "false"));
//			c->setActive(value.toBool());
//			return true;
//		} else {
//			return false;
//		}
//
//		//listOfPairs.replace(row, p);
//		emit dataChanged(index, index);
//
//		return true;
//	}

	return false;
}

bool ScriptingTableModel::insertRows(int position, int rows, const QModelIndex& index) {
	return true;
}

bool ScriptingTableModel::removeRows(int position, int rows, const QModelIndex& index) {
	return true;
}

ScriptingComponent* ScriptingTableModel::getScriptingComponent(int index) {
	if (index < 0 || index >= dataVector.size()) {
		return nullptr;
	}

	return dataVector[index];
}

void ScriptingTableModel::emitDataChanged(int row, int col) {
	QModelIndex i = index(row, col);
	emit dataChanged(i, i);
}
