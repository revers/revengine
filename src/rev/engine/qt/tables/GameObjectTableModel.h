/*
 * GameObjectTableModel.h
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#ifndef GAMEOBJECTTABLEMODEL_H_
#define GAMEOBJECTTABLEMODEL_H_

#include <QAbstractTableModel>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/util/ThreadSafeVector.h>

namespace rev {

	class GameObjectTableModel: public QAbstractTableModel {
	Q_OBJECT

	public:
		typedef ThreadSafeVector<rev::GameObject*> GameObjectVector;

	private:
		GameObjectVector dataVector;

	public:
		GameObjectTableModel(QObject* parent = 0) :
				QAbstractTableModel(parent) {
		}

		virtual ~GameObjectTableModel() {
		}

		int rowCount(const QModelIndex& parent) const;
		int columnCount(const QModelIndex& parent) const;
		QVariant data(const QModelIndex& index, int role) const;
		QVariant headerData(int section, Qt::Orientation orientation,
				int role) const;

		void addGameObject(GameObject* obj);
		void removeGameObject(GameObject* obj);
		void removeAllGameObjects();

		/**
		 * Returns GameObject index on the list, or -1 if there's
		 * no such GameObject on the list.
		 */
		int getGameObjectIndex(const GameObject* obj);

		GameObject* getGameObject(int index);

		GameObjectVector& getDataVector() {
			return dataVector;
		}
	};

} /* namespace rev */
#endif /* GAMEOBJECTTABLEMODEL_H_ */
