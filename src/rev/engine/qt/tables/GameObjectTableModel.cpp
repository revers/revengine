/*
 * GameObjectTableModel.cpp
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#include <QSize>
#include <rev/common/RevErrorStream.h>
#include "GameObjectTableModel.h"

using namespace rev;

#define COLUMN_COUNT 2
#define COLUMN_ID 0
#define COLUMN_NAME 1

void GameObjectTableModel::addGameObject(GameObject* obj) {
	int index = dataVector.size();
	beginInsertRows(QModelIndex(), index, index);
	dataVector.push_back(obj);
	endInsertRows();
}

void GameObjectTableModel::removeGameObject(GameObject* obj) {
	REV_TRACE_FUNCTION;

	for (int i = 0; i < dataVector.size(); i++) {
		GameObject*& go = dataVector[i];
		if (go == obj) {
			beginRemoveRows(QModelIndex(), i, i);
			dataVector.erase(i);
			endRemoveRows();
			break;
		}
	}
}

void GameObjectTableModel::removeAllGameObjects() {
	if (dataVector.empty()) {
		return;
	}
	beginRemoveRows(QModelIndex(), 0, dataVector.size() - 1);
	dataVector.clear();
	endRemoveRows();
}

int GameObjectTableModel::getGameObjectIndex(const GameObject* obj) {
	for (int i = 0; i < dataVector.size(); i++) {
		GameObject*& go = dataVector[i];
		if (go == obj) {
			return i;
		}
	}

	return -1;
}

int GameObjectTableModel::rowCount(const QModelIndex& parent) const {
	return dataVector.size();
}

int GameObjectTableModel::columnCount(const QModelIndex& parent) const {
	return COLUMN_COUNT;
}

QVariant GameObjectTableModel::data(const QModelIndex& index, int role) const {
	if (role == Qt::DisplayRole) {
		if (index.column() == COLUMN_ID) {
			return dataVector[index.row()]->getId();
		} else {
			const std::string& name = dataVector[index.row()]->getName();
			return QString::fromUtf8(name.data(), name.size());
		}
	} else if (role == Qt::TextAlignmentRole) {
		if (index.column() == COLUMN_ID) {
			return Qt::AlignCenter;
		}
		return Qt::AlignLeft;
	}
	return QVariant::Invalid;
}

QVariant GameObjectTableModel::headerData(int section, Qt::Orientation orientation,
		int role) const {
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section == COLUMN_ID) {
				return QString("ID");
			} else {
				return QString("Name");
			}
		}
	}

	return QVariant::Invalid;
}

GameObject* GameObjectTableModel::getGameObject(int index) {
	if (index < 0 || index >= dataVector.size()) {
		return nullptr;
	}

	return dataVector[index];
}
