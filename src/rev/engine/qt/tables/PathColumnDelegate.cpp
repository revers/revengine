/*
 * PathColumnDelegate.cpp
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#include <QPainter>
#include <QPushButton>
#include <QStylePainter>
#include <QTableView>

#include <QtGui/QApplication>
#include <QtGui/QMouseEvent>
#include "PathColumnDelegate.h"
#include "PathColumnEditor.h"

using namespace rev;

PathColumnDelegate::PathColumnDelegate(QObject* parent) :
		QStyledItemDelegate(parent) {
}

PathColumnDelegate::~PathColumnDelegate() {
}

void PathColumnDelegate::paint(QPainter* painter,
		const QStyleOptionViewItem& option,
		const QModelIndex& index) const {
	QStyledItemDelegate::paint(painter, option, index);
}

QWidget* PathColumnDelegate::createEditor(QWidget* parent,
		const QStyleOptionViewItem& option,
		const QModelIndex& index) const {
	return new PathColumnEditor(index, parent);
}

void PathColumnDelegate::setEditorData(QWidget* editor,
		const QModelIndex& index) const {
}

void PathColumnDelegate::setModelData(QWidget* editor, QAbstractItemModel* model,
		const QModelIndex& index) const {
}

void PathColumnDelegate::updateEditorGeometry(QWidget* editor,
		const QStyleOptionViewItem& option, const QModelIndex& /* index */) const {
	editor->setGeometry(option.rect);
}
