/*
 * ActiveColumnDelegate.h
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#ifndef ACTIVECOLUMNDELEGATE_H_
#define ACTIVECOLUMNDELEGATE_H_

#include <QIcon>
#include <QStyledItemDelegate>

class QPainter;
class QStyleOptionViewItem;
class QModelIndex;

namespace rev {

	class ActiveColumnDelegate: public QStyledItemDelegate {
	Q_OBJECT

		QIcon playPauseIcon;

	public:
		ActiveColumnDelegate(QObject* parent = 0);

		void paint(QPainter* painter, const QStyleOptionViewItem& option,
				const QModelIndex& i) const override;

		bool editorEvent(QEvent* event, QAbstractItemModel* model,
				const QStyleOptionViewItem& option, const QModelIndex& index) override;
	};

} /* namespace rev */
#endif /* ACTIVECOLUMNDELEGATE_H_ */
