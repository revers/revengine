/*
 * StatusColumnDelegate.h
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#ifndef STATUSCOLUMNDELEGATE_H_
#define STATUSCOLUMNDELEGATE_H_

#include <QIcon>
#include <QStyledItemDelegate>

class QPainter;
class QStyleOptionViewItem;
class QModelIndex;

namespace rev {

	class StatusColumnDelegate: public QStyledItemDelegate {
	Q_OBJECT

		QIcon redCircleIcon;
		QIcon greenCircleIcon;
		QIcon blueCircleIcon;

	public:
		StatusColumnDelegate(QObject* parent = 0);

		void paint(QPainter* painter, const QStyleOptionViewItem& option,
				const QModelIndex& i) const override;

	};

} /* namespace rev */
#endif /* STATUSCOLUMNDELEGATE_H_ */
