/*
 * GameObjectTableDelegate.cpp
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#include <QApplication>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include "GameObjectTableDelegate.h"

using namespace std;
using namespace rev;

void GameObjectTableDelegate::paint(QPainter* painter, const QStyleOptionViewItem& opt,
		const QModelIndex& index) const {
	QStyleOptionViewItemV4 option = opt;

	QString text =
			index.model()->data(index.model()->index(index.row(), index.column())).toString();

//	option.text = "";
//	QStyle* style = option.widget ? option.widget->style() : QApplication::style();
//	style->drawControl(QStyle::CE_ItemViewItem, &option, painter, option.widget);

	QRect rect = option.rect;
	QPalette::ColorGroup cg =
			option.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
	if (cg == QPalette::Normal && !(option.state & QStyle::State_Active)) {
		cg = QPalette::Inactive;
	}

	bool selected = option.showDecorationSelected && (option.state & QStyle::State_Selected);

	painter->save();
	painter->setPen(Qt::NoPen);

	if (selected) {
		option.font.setBold(true);
		painter->setBrush(QColor(200, 200, 230));
	} else {
		option.font.setBold(false);
		painter->setBrush(Qt::white);
	}

	painter->drawRect(rect);
	painter->restore();

	if (index.row() == pickedIndex) {
		painter->setPen(pickedColor);
		option.font.setBold(true);
	} else if (selected) {
		//painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
		painter->setPen(Qt::white);
		option.font.setBold(false);
	} else {
		//painter->setPen(option.palette.color(cg, QPalette::Text));
		painter->setPen(Qt::black);
		option.font.setBold(false);
	}

	painter->setFont(option.font);
	rect.setLeft(rect.x() + 5);
	painter->drawText(rect, Qt::AlignJustify | Qt::AlignVCenter | Qt::TextWordWrap, text);
}
