/*
 * PathColumnEditor.cpp
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#include <QDir>
#include <QFile>
#include <QLineEdit>
#include <QSettings>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QModelIndex>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include "PathColumnEditor.h"
#include "ScriptingTableModel.h"
#include "../GameEditorWindow.h"

#define DEFAULT_SCRIPT_DIR_KEY "dsdk"

using namespace rev;

PathColumnEditor::PathColumnEditor(const QModelIndex& index, QWidget* parent) :
		QWidget(parent), index(index) {

	void* ptr = index.data(Qt::UserRole).value<void*>();
	component = static_cast<ScriptingComponent*>(ptr);

	setAutoFillBackground(true);

	browseButton = new QPushButton("...", this);
	pathLineEdit = new QLineEdit(component->getScriptFile().c_str());
	pathLineEdit->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred));
	pathLineEdit->setReadOnly(true);
	browseButton->setMaximumWidth(24);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->addWidget(pathLineEdit);
	layout->addWidget(browseButton);
	layout->setStretchFactor(pathLineEdit, 0x7FFFFFFF);
	layout->setStretchFactor(browseButton, 1);
	layout->setMargin(0);
	layout->setSpacing(0);

	setLayout(layout);

	connect(browseButton, SIGNAL(clicked()), this, SLOT(browseButtonPressed()));
}

void PathColumnEditor::browseButtonPressed() {

	QString text = pathLineEdit->text();
	QSettings mySettings;
	std::string dir;
	if (!text.isEmpty()) {
		dir = ResourcePath::get(text.toUtf8().data(), false);
	}

	QString filename = QFileDialog::getOpenFileName(
			this,
			tr("Open Script"),
			!text.isEmpty() ? dir.c_str() : mySettings.value(DEFAULT_SCRIPT_DIR_KEY).toString(),
			tr("JavaScript files (*.js)"));
	if (filename.isNull()) {
		return;
	}

	QDir currentDir;
	mySettings.setValue(DEFAULT_SCRIPT_DIR_KEY, currentDir.absoluteFilePath(filename));

	GameEditorWindow* gameEditor = GameEditorWindow::getInstance();
	gameEditor->setSceneSaved(false);

	std::string path = filename.toUtf8().data();
	std::string relativePath = ResourcePath::makeRelative(path);

	pathLineEdit->setText(relativePath.c_str());
	component->setScriptFile(relativePath);

	const ScriptingTableModel* m = static_cast<const ScriptingTableModel*>(index.model());
	ScriptingTableModel* model = const_cast<ScriptingTableModel*>(m);

	model->emitDataChanged(index.row(), 0);
	model->emitDataChanged(index.row(), 1);
}
