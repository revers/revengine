/*
 * StatusColumnDelegate.cpp
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#include <QPainter>
#include <QTableView>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <rev/engine/components/RevScriptingComponent.h>
#include "StatusColumnDelegate.h"

using namespace std;
using namespace rev;

StatusColumnDelegate::StatusColumnDelegate(QObject* parent) :
		QStyledItemDelegate(parent) {
	redCircleIcon.addFile(QString::fromUtf8(":/images/circle_red.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	greenCircleIcon.addFile(QString::fromUtf8(":/images/circle_green.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	blueCircleIcon.addFile(QString::fromUtf8(":/images/circle_blue.png"), QSize(), QIcon::Normal,
			QIcon::Off);
}

void StatusColumnDelegate::paint(QPainter* painter, const QStyleOptionViewItem& opt,
		const QModelIndex& index) const {
	QStyleOptionViewItemV4 option = opt;

	void* ptr = index.data(Qt::UserRole).value<void*>();
	ScriptingComponent* c = static_cast<ScriptingComponent*>(ptr);

	bool enabled = option.state & QStyle::State_Enabled;
	bool active = option.state & QStyle::State_Active;
	QPalette::ColorGroup cg =
			!enabled ? QPalette::Disabled : active ? QPalette::Normal : QPalette::Inactive;

	painter->save();
	painter->setPen(Qt::NoPen);

	bool selected = option.showDecorationSelected && (option.state & QStyle::State_Selected);
	if (selected) {
		painter->setBrush(option.palette.color(cg, QPalette::Highlight));
	} else {
		painter->setBrush(Qt::white);
	}

	QRect r = option.rect;
	painter->drawRect(r);
	painter->restore();

	const QIcon* icon;
	if (c->isInvalid()) {
		icon = &redCircleIcon;
	} else if (c->isCompiled()) {
		icon = &greenCircleIcon;
	} else {
		icon = &blueCircleIcon;
	}

	QPixmap pixmap = icon->pixmap(QSize(16, 16),
			enabled ? QIcon::Normal : QIcon::Disabled,
			QIcon::On);

	int x = r.left() + (r.width() / 2) - 8;
	int y = r.top();

	painter->drawPixmap(x, y, pixmap);
}
