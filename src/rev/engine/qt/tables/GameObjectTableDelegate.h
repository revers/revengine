/*
 * GameObjectTableDelegate.h
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#ifndef GAMEOBJECTTABLEDELEGATE_H_
#define GAMEOBJECTTABLEDELEGATE_H_

#include <QColor>
#include <QStyledItemDelegate>

class QPainter;
class QStyleOptionViewItem;
class QModelIndex;

namespace rev {

	class GameObjectTableDelegate: public QStyledItemDelegate {
		Q_OBJECT

		QColor pickedColor = QColor(207, 78, 80);
		int pickedIndex = -1;

	public:
		GameObjectTableDelegate(QObject* parent = 0): QStyledItemDelegate(parent) {
		}

		void paint(QPainter* painter, const QStyleOptionViewItem& option,
				const QModelIndex& i) const override;

		int getPickedIndex() const {
			return pickedIndex;
		}
		void setPickedIndex(int pickedIndex) {
			this->pickedIndex = pickedIndex;
		}
	};

} /* namespace rev */
#endif /* GAMEOBJECTTABLEDELEGATE_H_ */
