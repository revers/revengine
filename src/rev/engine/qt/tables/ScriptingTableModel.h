/*
 * ScriptingTableModel.h
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#ifndef SCRIPTINGTABLEMODEL_H_
#define SCRIPTINGTABLEMODEL_H_

#include <QAbstractTableModel>
#include <rev/engine/components/RevScriptingComponent.h>
#include <rev/engine/util/ThreadSafeVector.h>

#define SCRIPTING_COLUMN_COUNT 5
#define SCRIPTING_COLUMN_ACTIVE 0
#define SCRIPTING_COLUMN_STATUS 1
#define SCRIPTING_COLUMN_ID 2
#define SCRIPTING_COLUMN_NAME 3
#define SCRIPTING_COLUMN_PATH 4

namespace rev {

	class ScriptingTableModel: public QAbstractTableModel {
	Q_OBJECT

	public:
		typedef ThreadSafeVector<rev::ScriptingComponent*> ScriptingComponentVector;

	private:
		ScriptingComponentVector dataVector;

	public:
		ScriptingTableModel(QObject* parent = 0) :
				QAbstractTableModel(parent) {
		}

		virtual ~ScriptingTableModel() {
		}

		int rowCount(const QModelIndex& parent) const;
		int columnCount(const QModelIndex& parent) const;
		QVariant data(const QModelIndex& index, int role) const;
		QVariant headerData(int section, Qt::Orientation orientation,
				int role) const;

		Qt::ItemFlags flags(const QModelIndex& index) const;
		bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
		bool insertRows(int position, int rows, const QModelIndex& index = QModelIndex());
		bool removeRows(int position, int rows, const QModelIndex& index = QModelIndex());

		void addScriptingComponent(ScriptingComponent* obj);
		void removeScriptingComponent(ScriptingComponent* obj);
		void removeAllScriptingComponents();

		void emitDataChanged(int row, int col);

		/**
		 * Returns ScriptingComponent index on the list, or -1 if there's
		 * no such ScriptingComponent on the list.
		 */
		int getScriptingComponentIndex(const ScriptingComponent* obj);

		ScriptingComponent* getScriptingComponent(int index);

		ScriptingComponentVector& getDataVector() {
			return dataVector;
		}
	};

} /* namespace rev */
#endif /* SCRIPTINGTABLEMODEL_H_ */
