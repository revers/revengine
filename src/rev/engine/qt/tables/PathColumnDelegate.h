/*
 * PathColumnDelegate.h
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#ifndef PATHCOLUMNDELEGATE_H_
#define PATHCOLUMNDELEGATE_H_

#include <QStyledItemDelegate>

class QPainter;
class QStyleOptionViewItem;
class QModelIndex;

namespace rev {

	class PathColumnDelegate: public QStyledItemDelegate {
	Q_OBJECT

	public:
		PathColumnDelegate(QObject* parent = 0);
		~PathColumnDelegate();

		void paint(QPainter* painter, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override;

		QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override;

		void setEditorData(QWidget* editor, const QModelIndex& index) const override;

		void setModelData(QWidget* editor, QAbstractItemModel* model,
				const QModelIndex& index) const override;

		void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override;
	};

} /* namespace rev */
#endif /* PATHCOLUMNDELEGATE_H_ */
