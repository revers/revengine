/*
 * ActiveColumnDelegate.cpp
 *
 *  Created on: 03-04-2013
 *      Author: Revers
 */

#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QStylePainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <rev/engine/components/RevScriptingComponent.h>
#include "ActiveColumnDelegate.h"
#include "ScriptingTableModel.h"

using namespace rev;

ActiveColumnDelegate::ActiveColumnDelegate(QObject* parent) {
	playPauseIcon.addFile(QString::fromUtf8(":/images/play.png"), QSize(), QIcon::Normal,
			QIcon::On);
	playPauseIcon.addFile(QString::fromUtf8(":/images/pause.png"), QSize(), QIcon::Normal,
			QIcon::Off);
}

void ActiveColumnDelegate::paint(QPainter* painter,
		const QStyleOptionViewItem& option,
		const QModelIndex& index) const {
	void* ptr = index.data(Qt::UserRole).value<void*>();
	ScriptingComponent* c = static_cast<ScriptingComponent*>(ptr);

	bool enabled = option.state & QStyle::State_Enabled;
	bool active = option.state & QStyle::State_Active;
	QPalette::ColorGroup cg =
			!enabled ? QPalette::Disabled : active ? QPalette::Normal : QPalette::Inactive;

	painter->save();
	painter->setPen(Qt::NoPen);

	bool selected = option.showDecorationSelected && (option.state & QStyle::State_Selected);
	if (selected) {
		painter->setBrush(option.palette.color(cg, QPalette::Highlight));
	} else {
		painter->setBrush(Qt::white);
	}

	QRect r = option.rect;
	painter->drawRect(r);
	painter->restore();

	QPixmap pixmap = playPauseIcon.pixmap(QSize(16, 16),
			enabled ? QIcon::Normal : QIcon::Disabled,
			c->isActive() ? QIcon::On : QIcon::Off);

	int x = r.left() + (r.width() / 2) - 8;
	int y = r.top();

	painter->drawPixmap(x, y, pixmap);
}

bool ActiveColumnDelegate::editorEvent(QEvent* event, QAbstractItemModel* model,
		const QStyleOptionViewItem& option, const QModelIndex& index) {
	if ((event->type() == QEvent::MouseButtonPress) ||
			(event->type() == QEvent::MouseButtonDblClick)) {
		QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
		if (mouse_event->button() != Qt::LeftButton ||
				!option.rect.contains(mouse_event->pos())) {
			return true;
		}
		if (event->type() == QEvent::MouseButtonDblClick) {
			return true;
		}
	} else if (event->type() == QEvent::KeyPress) {
		if (static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space &&
				static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select) {
			return false;
		}
	} else {
		return false;
	}

	void* ptr = index.data(Qt::UserRole).value<void*>();
	ScriptingComponent* c = static_cast<ScriptingComponent*>(ptr);
	bool active = c->isActive();
	c->setActive(!active);

	ScriptingTableModel* m = static_cast<ScriptingTableModel*>(model);
	m->emitDataChanged(index.row(), 0);
	m->emitDataChanged(index.row(), 1);

	return false; //model->setData(index, !checked, Qt::EditRole);
}
