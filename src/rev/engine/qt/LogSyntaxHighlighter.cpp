/*
 * LogSyntaxHighlighter.cpp
 *
 *  Created on: 25-01-2013
 *      Author: Revers
 */

#include "LogSyntaxHighlighter.h"

using namespace rev;

LogSyntaxHighlighter::LogSyntaxHighlighter(QTextDocument* document) :
        QSyntaxHighlighter(document) {
}

LogSyntaxHighlighter::~LogSyntaxHighlighter() {
}

void LogSyntaxHighlighter::highlightBlock(const QString& text) {
    if (text.startsWith("[TRACE]")) {
        setFormat(0, text.length(), Qt::blue);
    } else if (text.startsWith("[ERROR]")) {
        setFormat(0, text.length(), Qt::red);
    } else if (text.startsWith("[WARN]")) {
        setFormat(0, text.length(), QColor(244, 164, 96));
    } else if (text.startsWith("[INFO]")) {
        setFormat(0, text.length(), Qt::darkGreen);
    } else if (text.startsWith("[DEBUG]")) {
        setFormat(0, text.length(), QColor(204, 0, 204));
    }
}
