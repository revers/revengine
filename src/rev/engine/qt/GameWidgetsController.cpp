/*
 * GameWidgetsController.cpp
 *
 *  Created on: 12-01-2013
 *      Author: Revers
 */

#include <iostream>

#include <QEvent>
#include <QMessageBox>
#include <QMouseEvent>
#include <QItemSelectionModel>
#include <QMutexLocker>

#include "GameGLWidget.h"
#include <rev/engine/RevEngine.h>
#include <rev/engine/RevGameFactory.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include "tables/GameObjectTableDelegate.h"
#include "tables/GameObjectTableModel.h"

#include "GameWidgetsController.h"
#include "GameEditorWindow.h"
#include "ui_GameEditorWindow.h"

#include "QtPropertiesBinder.h"
#include "ScriptingWidget.h"
#include "SurfacesWidget.h"
#include "PhysicsWidget.h"
#include "SceneWidget.h"

// TODO: remove me:
#include <rev/engine/RevGameManager.h>

using namespace rev;
using namespace std;

GameWidgetsController::GameWidgetsController(rev::GameEditorWindow* editorWindow) :
		editorWindow(editorWindow) {
	this->glWidget = editorWindow->getGameGLWidget();
	this->widget = editorWindow->getGameEditorUi();

	objectListDelegate = new GameObjectTableDelegate(this);
	widget->objectListTable->setItemDelegate(objectListDelegate);
	objectListModel = new GameObjectTableModel(widget->objectListTable);
	widget->objectListTable->setModel(objectListModel);

	connect(widget->objectListTable, SIGNAL(doubleClicked(const QModelIndex&)),
			this, SLOT(objectTableDoubleClicked(const QModelIndex&)));
	connect(this, SIGNAL(scriptErrorMsgSignal(const QString&)), this,
	SLOT(scriptErrorMsgSlot(const QString&)), Qt::QueuedConnection);
}

GameWidgetsController::~GameWidgetsController() {
}

void GameWidgetsController::update() {
}

void GameWidgetsController::mousePressed(const MouseEvent& e) {
	if (e.getButton() == MouseButton::BUTTON_RIGHT) {
		leftMousePressed = true;
		if (editorWindow->getCurrentTool()) {
			editorWindow->getCurrentTool()->mousePressed(e.getX(), e.getY(), e.getContextIndex());
		}
	}
}

void GameWidgetsController::mouseReleased(const MouseEvent& e) {
	if (e.getButton() == MouseButton::BUTTON_RIGHT) {
		leftMousePressed = false;
		if (editorWindow->getCurrentTool()) {
			editorWindow->getCurrentTool()->mouseReleased(e.getX(), e.getY(), e.getContextIndex());
		}
	}
}

void GameWidgetsController::mouseMoved(const MouseEvent& e) {
	if (leftMousePressed) {
		if (editorWindow->getCurrentTool()) {
			editorWindow->getCurrentTool()->mouseDrag(e.getX(), e.getY(), e.getContextIndex());
		}
	}
	if (editorWindow->getCurrentTool()) {
		editorWindow->getCurrentTool()->mouseMoved(e.getX(), e.getY(), e.getContextIndex());
	}
}

void GameWidgetsController::keyPressed(const KeyEvent& e) {
	if (editorWindow->getCurrentTool()) {
		editorWindow->getCurrentTool()->keyPressed(e);
	}
}

void GameWidgetsController::keyReleased(const KeyEvent& e) {
	if (editorWindow->getCurrentTool()) {
		editorWindow->getCurrentTool()->keyReleased(e);
	}
}

void GameWidgetsController::objectPicked(const rev::PickingEvent& e) {
	GameObject& go = *e.getGameObject();
	REV_TRACE_MSG("Picked object: " << go.getName()
			<< " (" << go.getId() << ")");
	selectGameObject(go);
	editorWindow->setObjectPicked(true);

	if (editorWindow->isNewPanelVisible()) {
		editorWindow->repickNewOrEditedObject();
	} else {
		editorWindow->cameraUnlock();
		if (rev::Renderer3D::ref().isExclusiveMode()) {
			rev::Renderer3D::ref().setExclusiveGameObject(&go);
		}
		editorWindow->getSurfacesWidget()->objectPicked(&go);
		editorWindow->getPhysicsWidget()->objectPicked(&go);
	}
}

void GameWidgetsController::pickingReseted() {
	REV_TRACE_FUNCTION;
	editorWindow->setObjectPicked(false);
	objectListDelegate->setPickedIndex(-1);

	if (editorWindow->isNewPanelVisible()) {
		editorWindow->repickNewOrEditedObject();
	} else {
		editorWindow->cameraUnlock();
		rev::Renderer3D::ref().turnOffExclusiveMode();
		editorWindow->clearPropertiesPanel();
		editorWindow->getSurfacesWidget()->pickingReseted();
		editorWindow->getPhysicsWidget()->pickingReseted();
	}
}

void GameWidgetsController::gameObjectAdded(const rev::GameObjectEvent& e) {
	GameObject& obj = *e.getGameObject();
	REV_DEBUG_MSG("GameObject ADDED: " << obj.getName()
			<< " (" << obj.getId() << ")");

	objectListModel->addGameObject(&obj);
	if (obj.getScriptingComponent()) {
		ScriptingWidget* scriptingWidget = editorWindow->getScriptingWidget();
		scriptingWidget->addScriptingComponent(obj.getScriptingComponent());
	}
}

void GameWidgetsController::gameObjectRemoved(const rev::GameObjectEvent& e) {
	GameObject& obj = *e.getGameObject();
	REV_DEBUG_MSG("GameObject REMOVED: " << obj.getName()
			<< " (" << obj.getId() << ")");

	rev::Renderer3D::ref().getPicker().reset();
	objectListModel->removeGameObject(&obj);
	if (obj.getScriptingComponent()) {
		ScriptingWidget* scriptingWidget = editorWindow->getScriptingWidget();
		scriptingWidget->removeScriptingComponent(obj.getScriptingComponent());
	}
}

void GameWidgetsController::clearObjectList() {
	objectListModel->removeAllGameObjects();
	widget->objectListTable->setModel(nullptr);
	widget->objectListTable->setModel(objectListModel);

	ScriptingWidget* scriptingWidget = editorWindow->getScriptingWidget();
	scriptingWidget->removeAllScriptingComponents();
}

void GameWidgetsController::allGameObjectsRemoved(const rev::GameObjectEvent& e) {
	REV_DEBUG_MSG("Removed all game objects.");
	clearObjectList();
}

void GameWidgetsController::selectGameObject(rev::GameObject& obj) {
	int index = objectListModel->getGameObjectIndex(&obj);
	if (index < 0) {
		REV_ERROR_MSG("There is no GameObject '" << obj.getName()
				<< " (" << obj.getId() << ") in objectListModel!!");
		objectListDelegate->setPickedIndex(-1);
		widget->objectListTable->viewport()->repaint();
		return;
	}

	widget->objectListTable->selectRow(index);
	objectListDelegate->setPickedIndex(index);
	//widget->objectListTable->scrollTo(widget->objectListTable->model()->index(index, 0));
	editorWindow->scrollObjectTableToSelectedItem();

	editorWindow->getPropertiesBinder()->clearAll();
	editorWindow->getPropertiesBinder()->bindObject(&obj);
	editorWindow->setPropertiesPanelVisible(true);

	widget->objectListTable->viewport()->repaint();
}

void GameWidgetsController::objectTableDoubleClicked(const QModelIndex& index) {
	int i = index.row();

	GameObject* obj = objectListModel->getGameObject(i);
	rev::Renderer3D::ref().getPicker().pickGameObject(obj);
}

void GameWidgetsController::scriptingEvent(const ScriptEvent& e) {
	if (e.isTypeInvalid()) {
		ScriptingComponent* c = e.getComponent();
		std::string name = c->getParent()->toString();
		REV_TRACE_MSG("Object: " << name << " has a script error. Details are in the log.");
		editorWindow->refreshEngineControls();
		ScriptingManager::ref().setPaused(true);
		emit scriptErrorMsgSignal(name.c_str());
	}

	ScriptingWidget* scriptingWidget = editorWindow->getScriptingWidget();
	scriptingWidget->refresh();
}

void GameWidgetsController::scriptErrorMsgSlot(const QString& objName) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.setWindowTitle("Error");
	msgBox.setText(
			QString("Object: %1 has a script error! See 'Output' tab for details.").arg(objName));
	msgBox.setInformativeText("Do you want to PAUSE Scripting Engine?");
	msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgBox.setDefaultButton(QMessageBox::No);
	int ret = msgBox.exec();
	if (ret == QMessageBox::Yes) {
		ScriptingManager::ref().setPaused(true);
	} else {
		ScriptingManager::ref().setPaused(false);
	}
}

void GameWidgetsController::sceneLoaded() {
	editorWindow->getSceneWidget()->setBackgroundColor(Renderer3D::ref().getBackgroundColor());
	// TODO: remove me:
	EventManager::ref().fireEngineEvent("addSampleObject", [] () -> bool {
		//GameManager::ref().addSamplePhysicsObject();
		//GameManager::ref().addSampleParticleObject();
//			GameObject* go = GameFactory::ref().createSampleParticleInstancingObject();
//			Engine::ref().addGameObject(go);

			return true;
		});
}
