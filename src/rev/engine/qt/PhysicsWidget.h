/*
 * PhysicsWidget.h
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#ifndef PHYSICSWIDGET_H_
#define PHYSICSWIDGET_H_

#include <QWidget>
#include "ui_PhysicsWidget.h"

class QtTreePropertyBrowser;

namespace rev {

	class GameEditorWindow;
	class GameObject;
	class QtPropertiesBinder;
	class SaveStateBinder;
	class RigidBodyComponent;

	class PhysicsWidget: public QWidget {
	Q_OBJECT

		GameEditorWindow* mainWindow;
		Ui::PhysicsWidget uiWidget;
		GameObject* pickedObject = nullptr;
		QtTreePropertyBrowser* propertyBrowser = nullptr;
		QtPropertiesBinder* propertiesBinder = nullptr;
		SaveStateBinder* collisionSavedState = nullptr;
		bool canChangeCollision = true;
		bool saveSceneState = true;
		bool hasRigidBodyComponent = false;
		bool hasCollisionComponent = false;

		RigidBodyComponent* rigidBody = nullptr;

	public:
		PhysicsWidget(GameEditorWindow* mainWindow);
		virtual ~PhysicsWidget();

		void objectPicked(GameObject* go);
		void pickingReseted();
		void refreshPickedObject();

		void init();
		void refresh();

	private slots:
		void bsFromDrawableAction();
		void editAction();
		void applyAction();
		void cancelAction();
		void geometryChanged(int index);
		void massValueChanged(double value);
		void calcInertiaAction();

	private:
		void setupActions();
		void setWidgetsEnabled(bool enabled);
		void setSurfacesWidgetsEnabled(bool enabled);
		void setCollsionWidgetsEnabled(bool enabled);
		void setPhysicsWidgetsEnabled(bool enabled);
		void setCollisionCB();
	};

} /* namespace rev */
#endif /* PHYSICSWIDGET_H_ */
