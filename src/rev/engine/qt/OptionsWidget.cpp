/*
 * OptionsWidget.cpp
 *
 *  Created on: 29 lip 2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "GameEditorWindow.h"
#include "OptionsWidget.h"

namespace rev {

OptionsWidget::OptionsWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	widget.setupUi(this);

	setupActions();
}

OptionsWidget::~OptionsWidget() {
}

void OptionsWidget::setupActions() {
	connect(widget.showNormalsCB, SIGNAL(toggled(bool)),
			this, SLOT(showNormalsSelected(bool)));

	connect(widget.wireFrameModeCB, SIGNAL(toggled(bool)),
			this, SLOT(wireframeModeSelected(bool)));

	connect(widget.drawDebugElemsCB, SIGNAL(toggled(bool)),
			this, SLOT(drawDebugElemsSelected(bool)));

	connect(widget.drawObjectsCB, SIGNAL(toggled(bool)),
			this, SLOT(drawObjectsSelected(bool)));

	connect(widget.drawBSCB, SIGNAL(toggled(bool)),
			this, SLOT(drawBSSelected(bool)));

	connect(widget.drawAllCollisionBSsCB, SIGNAL(toggled(bool)),
			this, SLOT(drawAllCollisionBSsSelected(bool)));

	connect(widget.drawAllCollisionGeometryCB, SIGNAL(toggled(bool)),
			this, SLOT(drawAllCollisionGeometrySelected(bool)));

	connect(widget.drawLocAxesCB, SIGNAL(toggled(bool)),
			this, SLOT(drawLocAxesSelected(bool)));

	connect(widget.drawOutlineCB, SIGNAL(toggled(bool)),
			this, SLOT(drawOutlineSelected(bool)));

	connect(widget.drawAllBSCB, SIGNAL(toggled(bool)),
			this, SLOT(drawAllBSSelected(bool)));

	connect(widget.drawShadowDepthCB, SIGNAL(toggled(bool)),
			this, SLOT(drawShadowDepthSelected(bool)));

	connect(widget.drawShadowFrustumCB, SIGNAL(toggled(bool)),
			this, SLOT(drawShadowFrustumSelected(bool)));

	connect(widget.shadowsEnabledCB, SIGNAL(toggled(bool)),
			this, SLOT(shadowsEnabledSelected(bool)));

	connect(widget.cullingEnabledCB, SIGNAL(toggled(bool)),
			this, SLOT(cullingEnabledSelected(bool)));

	connect(widget.drawCameraFrustumCB, SIGNAL(toggled(bool)),
			this, SLOT(drawCameraFrustumSelected(bool)));

	connect(widget.deferredEnabledCB, SIGNAL(toggled(bool)),
			this, SLOT(deferredEnabledSelected(bool)));

	connect(widget.drawDeferredDepthCB, SIGNAL(toggled(bool)),
			this, SLOT(drawDeferredDepthSelected(bool)));

	connect(widget.drawDeferredNormalsCB, SIGNAL(toggled(bool)),
			this, SLOT(drawDeferredNormalsSelected(bool)));

	connect(widget.drawDeferredSpheresCB, SIGNAL(toggled(bool)),
			this, SLOT(drawDeferredSpheresSelected(bool)));

	connect(widget.drawDeferredLightTexCB, SIGNAL(toggled(bool)),
			this, SLOT(drawDeferredLightTexSelected(bool)));

	connect(widget.ssaoEnabledCB, SIGNAL(toggled(bool)),
			this, SLOT(ssaoEnabledSelected(bool)));

	connect(widget.drawSsaoTextureCB, SIGNAL(toggled(bool)),
			this, SLOT(drawSsaoTextureSelected(bool)));
}

void OptionsWidget::refreshEngineControls() {
	widget.showNormalsCB->setChecked(
			rev::Renderer3D::ref().isDebugNormals());
	widget.wireFrameModeCB->setChecked(
			rev::Renderer3D::ref().isWireframeMode());
	widget.drawDebugElemsCB->setChecked(
			rev::Renderer3D::ref().isDrawDebugElements());
	widget.drawAllCollisionBSsCB->setChecked(
			rev::Renderer3D::ref().isDrawAllCollisionBoundingSpheres());
	widget.drawBSCB->setChecked(
			rev::Renderer3D::ref().getPicker().isDrawBoundingSphere());
	widget.drawLocAxesCB->setChecked(
			rev::Renderer3D::ref().getPicker().isDrawAxes());
	widget.drawOutlineCB->setChecked(
			rev::Renderer3D::ref().getPicker().isDrawOutline());
	widget.drawAllBSCB->setChecked(
			rev::Renderer3D::ref().isDrawAllBoundingSpheres());
	widget.drawAllCollisionGeometryCB->setChecked(
			rev::Renderer3D::ref().isDrawAllCollisionGeometry());
	widget.drawObjectsCB->setChecked(rev::Renderer3D::ref().isDrawObjects());
	widget.drawCameraFrustumCB->setChecked(
			rev::CameraManager::ref().isDrawCameraFrustum(0));
	widget.drawShadowFrustumCB->setChecked(
			rev::Renderer3D::ref().isDrawShadowSourceFrustum());
	widget.drawShadowDepthCB->setChecked(
			rev::Renderer3D::ref().isDrawShadowDepthMap());
	widget.shadowsEnabledCB->setChecked(
			rev::Renderer3D::ref().isShadowsEnabled());
	widget.cullingEnabledCB->setChecked(
			rev::Renderer3D::ref().isFrustumCullingEnabled());
	widget.deferredEnabledCB->setChecked(
			rev::Renderer3D::ref().isDeferredLightingEnabled());
	widget.drawDeferredDepthCB->setChecked(
			rev::Renderer3D::ref().isDrawDeferredDepthMap());
	widget.drawDeferredNormalsCB->setChecked(
			rev::Renderer3D::ref().isDrawDeferredNormalMap());
	widget.ssaoEnabledCB->setChecked(
			rev::Renderer3D::ref().isSSAOEnabled());
	widget.drawSsaoTextureCB->setChecked(
			rev::Renderer3D::ref().isDrawSSAOMap());
}

void OptionsWidget::showNormalsSelected(bool checked) {
	rev::Renderer3D::ref().setDebugNormals(checked);
}

void OptionsWidget::wireframeModeSelected(bool checked) {
	rev::Renderer3D::ref().setWireframeMode(checked);
}

void OptionsWidget::drawDebugElemsSelected(bool checked) {
	rev::Renderer3D::ref().setDrawDebugElements(checked);
}

void OptionsWidget::drawLocAxesSelected(bool checked) {
	rev::Renderer3D::ref().getPicker().setDrawAxes(checked);
}

void OptionsWidget::drawBSSelected(bool checked) {
	rev::Renderer3D::ref().getPicker().setDrawBoundingSphere(checked);
}

void OptionsWidget::drawOutlineSelected(bool checked) {
	rev::Renderer3D::ref().getPicker().setDrawOutline(checked);
}

void OptionsWidget::drawAllBSSelected(bool checked) {
	rev::Renderer3D::ref().setDrawAllBoundingSpheres(checked);
}

void OptionsWidget::drawObjectsSelected(bool checked) {
	rev::Renderer3D::ref().setDrawObjects(checked);
}

void OptionsWidget::drawAllCollisionBSsSelected(bool checked) {
	rev::Renderer3D::ref().setDrawAllCollisionBoundingSpheres(checked);
}

void OptionsWidget::drawAllCollisionGeometrySelected(bool checked) {
	rev::Renderer3D::ref().setDrawAllCollisionGeometry(checked);
}

void OptionsWidget::drawShadowFrustumSelected(bool checked) {
	rev::Renderer3D::ref().setDrawShadowSourceFrustum(checked);
}

void OptionsWidget::drawShadowDepthSelected(bool checked) {
	rev::Renderer3D::ref().setDrawShadowDepthMap(checked);
}

void OptionsWidget::shadowsEnabledSelected(bool checked) {
	rev::Renderer3D::ref().setShadowsEnabled(checked);
}

void OptionsWidget::cullingEnabledSelected(bool checked) {
	rev::Renderer3D::ref().setFrustumCullingEnabled(checked);
}

void OptionsWidget::deferredEnabledSelected(bool checked) {
	rev::Renderer3D::ref().setDeferredLightingEnabled(checked);
}

void OptionsWidget::drawDeferredNormalsSelected(bool checked) {
	rev::Renderer3D::ref().setDrawDeferredNormalMap(checked);
}

void OptionsWidget::drawDeferredDepthSelected(bool checked) {
	rev::Renderer3D::ref().setDrawDeferredDepthMap(checked);
}

void OptionsWidget::drawCameraFrustumSelected(bool checked) {
	REV_DEBUG_MSG(R(checked));
	rev::CameraManager::ref().setDrawCameraFrustum(GLContextManager::MAIN_CONTEXT,
			checked);
}

void OptionsWidget::drawDeferredSpheresSelected(bool checked) {
	rev::Renderer3D::ref().setDrawDeferredLightSpheres(checked);
}

void OptionsWidget::drawDeferredLightTexSelected(bool checked) {
	rev::Renderer3D::ref().setDrawDeferredLightMap(checked);
}

void OptionsWidget::ssaoEnabledSelected(bool checked) {
	rev::Renderer3D::ref().setSSAOEnabled(checked);
}

void OptionsWidget::drawSsaoTextureSelected(bool checked) {
	rev::Renderer3D::ref().setDrawSSAOMap(checked);
}

} /* namespace rev */
