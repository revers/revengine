/*
 * PhysicsWidget.cpp
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#include <QMessageBox>
#include <QtTreePropertyBrowser>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/binding/RevSaveStateBinder.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/components/RevCollisionComponent.h>
#include <rev/engine/components/RevRigidBodyComponent.h>
#include <rev/engine/collision/RevCollisionManager.h>
#include <rev/engine/collision/RevCollisionSphere.h>
#include <rev/engine/collision/RevCollisionBox.h>
#include <rev/engine/collision/RevCollisionPlane.h>
#include <rev/engine/physics/RevPhysicsManager.h>
#include <rev/engine/physics/RevInertiaTensorUtil.h>
#include "GameEditorWindow.h"
#include "PhysicsWidget.h"
#include "QtPropertiesBinder.h"

#define GEOMETRY_NONE "None"
#define GEOMETRY_SPHERE "Sphere"
#define GEOMETRY_BOX "Box"
#define GEOMETRY_PLANE "Plane"

#define INDEX_NONE 0
#define INDEX_SPHERE 1
#define INDEX_BOX 2
#define INDEX_PLANE 3

using namespace rev;

PhysicsWidget::PhysicsWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);

	propertyBrowser = new QtTreePropertyBrowser(this);
	uiWidget.propertiesLayout->addWidget(propertyBrowser);
	propertiesBinder = new QtPropertiesBinder(propertyBrowser, this);

	uiWidget.geometryCB->addItem(GEOMETRY_NONE);
	uiWidget.geometryCB->addItem(GEOMETRY_SPHERE);
	uiWidget.geometryCB->addItem(GEOMETRY_BOX);
	uiWidget.geometryCB->addItem(GEOMETRY_PLANE);

	uiWidget.collisionL->setText("-");
	uiWidget.rigidBodyL->setText("-");

	setWidgetsEnabled(false);

	setupActions();
}

PhysicsWidget::~PhysicsWidget() {
}

void PhysicsWidget::setupActions() {
	connect(uiWidget.bsFromDrawableB, SIGNAL(clicked()),
			this, SLOT(bsFromDrawableAction()));

	connect(uiWidget.editB, SIGNAL(clicked()), this, SLOT(editAction()));

	connect(uiWidget.applyB, SIGNAL(clicked()), this, SLOT(applyAction()));

	connect(uiWidget.cancelB, SIGNAL(clicked()), this, SLOT(cancelAction()));

	connect(uiWidget.inertiaB, SIGNAL(clicked()), this, SLOT(calcInertiaAction()));

	connect(uiWidget.geometryCB, SIGNAL(currentIndexChanged(int)),
			this, SLOT(geometryChanged(int)));

	connect(uiWidget.massSB, SIGNAL(valueChanged(double)), this, SLOT(massValueChanged(double)));
}

void PhysicsWidget::init() {
	propertiesBinder->bindObject(CollisionManager::getInstance());
	propertiesBinder->bindObject(PhysicsManager::getInstance());
}

void PhysicsWidget::refresh() {
	propertiesBinder->refreshAll();
}

void PhysicsWidget::setWidgetsEnabled(bool enabled) {
	setCollsionWidgetsEnabled(enabled);
	setPhysicsWidgetsEnabled(enabled);

	uiWidget.rigidBodyL->setEnabled(enabled);
	uiWidget.rigidBodyTxtL->setEnabled(enabled);
	uiWidget.collisionL->setEnabled(enabled);
	uiWidget.collisionTxtL->setEnabled(enabled);

	uiWidget.editB->setEnabled(enabled);
	if (!enabled) {
		setSurfacesWidgetsEnabled(false);
	}
}

void PhysicsWidget::setCollsionWidgetsEnabled(bool enabled) {
	enabled = enabled && hasCollisionComponent;
	uiWidget.bsFromDrawableB->setEnabled(enabled);
}

void PhysicsWidget::setPhysicsWidgetsEnabled(bool enabled) {
	enabled = enabled && hasRigidBodyComponent;
	uiWidget.massSB->setEnabled(enabled);
	uiWidget.inertiaB->setEnabled(enabled);
}

void PhysicsWidget::setSurfacesWidgetsEnabled(bool enabled) {
	uiWidget.applyB->setEnabled(enabled);
	uiWidget.cancelB->setEnabled(enabled);
	uiWidget.geometryCB->setEnabled(enabled);
}

void PhysicsWidget::calcInertiaAction() {
	REV_TRACE_FUNCTION;
	if (!hasRigidBodyComponent) {
		return;
	}
	if (!hasCollisionComponent) {
		QMessageBox::warning(this, "Warning",
				"Selected object has no Collision component.\n"
						"Inertia Tensor not calculated!");
		return;
	}
	CollisionGeometry* geometry = pickedObject->getCollisionComponent()->getGeometry();
	if (!geometry) {
		QMessageBox::warning(this, "Warning", "You must first choose collision geometry.");
		return;
	}

	switch (geometry->getType()) {
	case GeometryType::SPHERE: {
		CollisionSphere& sphere = static_cast<CollisionSphere&>(*geometry);
		rmat3 m = InertiaTensorUtil::getSphereTensor(real(sphere.getRadius()),
				rigidBody->getMass());
		rigidBody->setInertiaTensor(m);
		break;
	}
	case GeometryType::BOX: {
		CollisionBox& box = static_cast<CollisionBox&>(*geometry);
		rvec3 halfSizes(box.getHalfSize());

		rmat3 m = InertiaTensorUtil::getBoxTensor(halfSizes, rigidBody->getMass());
		rigidBody->setInertiaTensor(m);
		break;
	}
	case GeometryType::PLANE: {
		rmat3 m(1.0);
		rigidBody->setInertiaTensor(m);
		QMessageBox::warning(this, "Warning",
				"Plane will have set inertia tensor to identity matrix.");
		break;
	}
	default: {
		revAssert(false);
		break;
	}
	}

	QMessageBox::information(this, "Info", "Inertia Tensor calculated successfully.");
	mainWindow->setSceneSaved(false);
}

void PhysicsWidget::massValueChanged(double value) {
	REV_TRACE_MSG(R(value));

	if (!hasRigidBodyComponent) {
		return;
	}

	rigidBody->setMass(value);
	mainWindow->setSceneSaved(false);
}

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

void PhysicsWidget::bsFromDrawableAction() {
	if (!pickedObject) {
		return;
	}

	IVisual3DComponent* component = pickedObject->getVisual3DComponent();
	revAssert(component);
	if (!component) {
		REV_WARN_MSG("Object " << pickedObject << " has no IVisual3DComponent.");
		QMessageBox::warning(this, "Warning",
				"Selected object has no IVisual3DComponent component.\n"
						"BoundingSphere not calculated!");
		return;
	}

	BoundingSphere sphere(*component->getBoundingSphere());
	const glm::vec3& s = pickedObject->getScaling();
	float scaleFactor = MAX(s.x, MAX(s.y, s.z));
	sphere.r *= scaleFactor;

	CollisionComponent* cc = pickedObject->getCollisionComponent();
	cc->setBoundingSphere(sphere);

	QMessageBox::information(this, "Info", "BoundingSphere calculated successfully.");
	mainWindow->setSceneSaved(false);
}

void PhysicsWidget::objectPicked(GameObject* go) {
	saveSceneState = mainWindow->isSceneSaved();
	if (pickedObject) {
		pickingReseted();
	}
	REV_DEBUG_MSG(R(go));

	rigidBody = dynamic_cast<RigidBodyComponent*>(go->getPhysicsComponent());
	hasRigidBodyComponent = rigidBody != nullptr;
	hasCollisionComponent = go->getCollisionComponent() != nullptr;

	if (hasCollisionComponent) {
		uiWidget.collisionL->setText("Yes");
	} else {
		uiWidget.collisionL->setText("No");
	}

	if (hasRigidBodyComponent) {
		uiWidget.rigidBodyL->setText("Yes");
		uiWidget.massSB->setValue((double) rigidBody->getMass());
	} else {
		uiWidget.rigidBodyL->setText("No");
	}

	if (hasRigidBodyComponent || hasCollisionComponent) {
		setWidgetsEnabled(true);
		pickedObject = go;
		setCollisionCB();
	} else {
		setWidgetsEnabled(false);
		pickedObject = nullptr;
	}
}

void PhysicsWidget::pickingReseted() {
	setWidgetsEnabled(false);
	pickedObject = nullptr;
	rigidBody = nullptr;
	hasRigidBodyComponent = false;
	hasCollisionComponent = false;
	uiWidget.collisionL->setText("-");
	uiWidget.rigidBodyL->setText("-");
	cancelAction();
}

void PhysicsWidget::refreshPickedObject() {
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (go) {
		objectPicked(go);
	} else {
		pickingReseted();
	}
}

void PhysicsWidget::editAction() {
	REV_TRACE_FUNCTION;
	setSurfacesWidgetsEnabled(true);
	uiWidget.editB->setEnabled(false);

	saveSceneState = mainWindow->isSceneSaved();
	mainWindow->setSceneSaved(false);

	revAssert(pickedObject);
	if (collisionSavedState) {
		delete collisionSavedState;
	}
	collisionSavedState = new SaveStateBinder(pickedObject->getCollisionComponent());
}

void PhysicsWidget::applyAction() {
	REV_TRACE_FUNCTION;
	setSurfacesWidgetsEnabled(false);
	if (pickedObject) {
		uiWidget.editB->setEnabled(true);

		revAssert(collisionSavedState);

		delete collisionSavedState;
		collisionSavedState = nullptr;
		mainWindow->bindPropertiesPanel(pickedObject);
	}

}

void PhysicsWidget::cancelAction() {
	REV_TRACE_FUNCTION;
	setSurfacesWidgetsEnabled(false);
	mainWindow->setSceneSaved(saveSceneState);
	if (!pickedObject) {
		return;
	}
	uiWidget.editB->setEnabled(true);

	revAssert(collisionSavedState);

	collisionSavedState->resore(pickedObject->getCollisionComponent());
	delete collisionSavedState;
	collisionSavedState = nullptr;
	mainWindow->bindPropertiesPanel(pickedObject);
	setCollisionCB();
}

void PhysicsWidget::setCollisionCB() {
	CollisionGeometry* geometry = pickedObject->getCollisionComponent()->getGeometry();
	if (!geometry) {
		canChangeCollision = false;
		uiWidget.geometryCB->setCurrentIndex(INDEX_NONE);
		canChangeCollision = true;
		return;
	}

	canChangeCollision = false;
	switch (geometry->getType()) {
	case GeometryType::SPHERE: {
		uiWidget.geometryCB->setCurrentIndex(INDEX_SPHERE);
		break;
	}
	case GeometryType::BOX: {
		uiWidget.geometryCB->setCurrentIndex(INDEX_BOX);
		break;
	}
	case GeometryType::PLANE: {
		uiWidget.geometryCB->setCurrentIndex(INDEX_PLANE);
		break;
	}
	default: {
		revAssert(false);
		break;
	}
	}
	canChangeCollision = true;
}

void PhysicsWidget::geometryChanged(int index) {
	if (!canChangeCollision) {
		return;
	}
	REV_TRACE_MSG(R(index));

	CollisionComponent* collisionComp = pickedObject->getCollisionComponent();
	CollisionGeometry* geometry = collisionComp->getGeometry();
	if (geometry) {
		delete geometry;
	}

	switch (index) {
	case INDEX_NONE: {
		collisionComp->setGeometry(nullptr);
		break;
	}
	case INDEX_SPHERE: {
		collisionComp->setGeometry(new CollisionSphere());
		break;
	}
	case INDEX_BOX: {
		collisionComp->setGeometry(new CollisionBox());
		break;
	}
	case INDEX_PLANE: {
		collisionComp->setGeometry(new CollisionPlane());
		break;
	}
	default: {
		revAssert(false);
		break;
	}
	}

	mainWindow->bindPropertiesPanel(pickedObject);
}
