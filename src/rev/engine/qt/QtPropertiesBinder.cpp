/*
 * QtPropertiesBinder.cpp
 *
 *  Created on: 19-01-2013
 *      Author: Revers
 */

#include <iostream>
#include <sstream>

#include <QtTreePropertyBrowser>
#include <QtGroupPropertyManager>
#include <QtIntPropertyManager>
#include <QtBoolPropertyManager>
#include <QtDoublePropertyManager>
#include <QtColorPropertyManager>
#include <QtStringPropertyManager>

#include "QtConfig.h"

#include "properties/FilePathManager.h"
#include "properties/Vec2PropertyManager.h"
#include "properties/Vec3PropertyManager.h"
#include "properties/Vec4PropertyManager.h"
#include "properties/Mat2PropertyManager.h"
#include "properties/Mat3PropertyManager.h"
#include "properties/Mat4PropertyManager.h"
#include "properties/QuatPropertyManager.h"

#include "properties/DVec2PropertyManager.h"
#include "properties/DVec3PropertyManager.h"
#include "properties/DVec4PropertyManager.h"
#include "properties/DMat2PropertyManager.h"
#include "properties/DMat3PropertyManager.h"
#include "properties/DMat4PropertyManager.h"
#include "properties/DQuatPropertyManager.h"

#include <QtGroupPropertyManager>
#include <QtDoubleSpinBoxFactory>
#include <QtCheckBoxFactory>
#include <QtSpinBoxFactory>
#include <QtEnumEditorFactory>
#include <QtColorEditorFactory>
#include <QtLineEditFactory>
#include "properties/FileEditFactory.h"

#include <rev/engine/xml/RevXMLElementBinder.h>
#include <rev/common/RevErrorStream.h>

#include "QtPropertiesBinder.h"

#include <rev/common/RevAssert.h>

using namespace rev;
using namespace std;

QtPropertiesBinder::QtPropertiesBinder(QtTreePropertyBrowser* propertyBrowser, QObject* parent) :
		QObject(parent), propertyBrowser(propertyBrowser) {

	groupManager = new QtGroupPropertyManager(this);
	intManager = new QtIntPropertyManager(this);
	boolManager = new QtBoolPropertyManager(this);
	doubleManager = new QtDoublePropertyManager(this);
	colorManager = new QtColorPropertyManager(this);
	vec2Manager = new Vec2PropertyManager(this);
	vec3Manager = new Vec3PropertyManager(this);
	vec4Manager = new Vec4PropertyManager(this);
	mat2Manager = new Mat2PropertyManager(this);
	mat3Manager = new Mat3PropertyManager(this);
	mat4Manager = new Mat4PropertyManager(this);
	quatManager = new QuatPropertyManager(this);

	dvec2Manager = new DVec2PropertyManager(this);
	dvec3Manager = new DVec3PropertyManager(this);
	dvec4Manager = new DVec4PropertyManager(this);
	dmat2Manager = new DMat2PropertyManager(this);
	dmat3Manager = new DMat3PropertyManager(this);
	dmat4Manager = new DMat4PropertyManager(this);
	dquatManager = new DQuatPropertyManager(this);

	stringManager = new QtStringPropertyManager(this);
	filepathManager = new FilePathManager(this);

	connect(intManager, SIGNAL(valueChanged(QtProperty*, int)), this,
			SLOT(valueChanged(QtProperty*, int)));
	connect(boolManager, SIGNAL(valueChanged(QtProperty*, bool)), this,
			SLOT(valueChanged(QtProperty*, bool)));
	connect(doubleManager, SIGNAL(valueChanged(QtProperty*, double)), this,
			SLOT(valueChanged(QtProperty*, double)));
	connect(colorManager, SIGNAL(valueChanged(QtProperty*, QColor)), this,
			SLOT(valueChanged(QtProperty*, QColor)));
	connect(vec2Manager, SIGNAL(valueChanged(QtProperty*, glm::vec2)), this,
			SLOT(valueChanged(QtProperty*, glm::vec2)));
	connect(vec3Manager, SIGNAL(valueChanged(QtProperty*, glm::vec3)), this,
			SLOT(valueChanged(QtProperty*, glm::vec3)));
	connect(vec4Manager, SIGNAL(valueChanged(QtProperty*, glm::vec4)), this,
			SLOT(valueChanged(QtProperty*, glm::vec4)));
	connect(mat2Manager, SIGNAL(valueChanged(QtProperty*, glm::mat2)), this,
			SLOT(valueChanged(QtProperty*, glm::mat2)));
	connect(mat3Manager, SIGNAL(valueChanged(QtProperty*, glm::mat3)), this,
			SLOT(valueChanged(QtProperty*, glm::mat3)));
	connect(mat4Manager, SIGNAL(valueChanged(QtProperty*, glm::mat4)), this,
			SLOT(valueChanged(QtProperty*, glm::mat4)));
	connect(quatManager, SIGNAL(valueChanged(QtProperty*, glm::quat)), this,
			SLOT(valueChanged(QtProperty*, glm::quat)));

	connect(dvec2Manager, SIGNAL(valueChanged(QtProperty*, glm::dvec2)), this,
			SLOT(valueChanged(QtProperty*, glm::dvec2)));
	connect(dvec3Manager, SIGNAL(valueChanged(QtProperty*, glm::dvec3)), this,
			SLOT(valueChanged(QtProperty*, glm::dvec3)));
	connect(dvec4Manager, SIGNAL(valueChanged(QtProperty*, glm::dvec4)), this,
			SLOT(valueChanged(QtProperty*, glm::dvec4)));
	connect(dmat2Manager, SIGNAL(valueChanged(QtProperty*, glm::dmat2)), this,
			SLOT(valueChanged(QtProperty*, glm::dmat2)));
	connect(dmat3Manager, SIGNAL(valueChanged(QtProperty*, glm::dmat3)), this,
			SLOT(valueChanged(QtProperty*, glm::dmat3)));
	connect(dmat4Manager, SIGNAL(valueChanged(QtProperty*, glm::dmat4)), this,
			SLOT(valueChanged(QtProperty*, glm::dmat4)));
	connect(dquatManager, SIGNAL(valueChanged(QtProperty*, glm::dquat)), this,
			SLOT(valueChanged(QtProperty*, glm::dquat)));

	connect(stringManager, SIGNAL(valueChanged(QtProperty*, QString)), this,
			SLOT(valueChanged(QtProperty*, QString)));
	connect(filepathManager, SIGNAL(valueChanged(QtProperty*, QString)), this,
			SLOT(filepathValueChanged(QtProperty*, QString)));

	QtDoubleSpinBoxFactory* doubleFactory = new QtDoubleSpinBoxFactory(this);
	QtCheckBoxFactory* boolFactory = new QtCheckBoxFactory(this);
	QtSpinBoxFactory* intFactory = new QtSpinBoxFactory(this);
	QtEnumEditorFactory* enumFactory = new QtEnumEditorFactory(this);
	QtColorEditorFactory* colorFactory = new QtColorEditorFactory(this);
	QtLineEditFactory* lineEditFactory = new QtLineEditFactory(this);
	FileEditFactory* filepathFactory = new FileEditFactory(this);

	propertyBrowser->setFactoryForManager(intManager, intFactory);
	propertyBrowser->setFactoryForManager(boolManager, boolFactory);
	propertyBrowser->setFactoryForManager(doubleManager, doubleFactory);
	propertyBrowser->setFactoryForManager(colorManager, colorFactory);
	propertyBrowser->setFactoryForManager(
			colorManager->subIntPropertyManager(), intFactory);
	propertyBrowser->setFactoryForManager(stringManager, lineEditFactory);

	propertyBrowser->setFactoryForManager(
			vec2Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			vec3Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			vec4Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			mat2Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			mat3Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			mat4Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			quatManager->subDoublePropertyManager(), doubleFactory);

	propertyBrowser->setFactoryForManager(
			dvec2Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dvec3Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dvec4Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dmat2Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dmat3Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dmat4Manager->subDoublePropertyManager(), doubleFactory);
	propertyBrowser->setFactoryForManager(
			dquatManager->subDoublePropertyManager(), doubleFactory);

	propertyBrowser->setFactoryForManager(filepathManager, filepathFactory);
}

QtPropertiesBinder::~QtPropertiesBinder() {
}

void QtPropertiesBinder::bindObject(IBindable* bindable) {
	REV_TRACE_FUNCTION;

	if (!bindable) {
		REV_DEBUG_MSG("null bindable");
		return;
	}

	//clearAll();
	canChangeValue = false;
	bindable->bind(*this);
	canChangeValue = true;
}

void QtPropertiesBinder::unbindObject(IBindable* bindable) {
	REV_DEBUG_WARN_MSG("Removing all bindables not just one");
	clearAll();
}

/**
 * int
 */
void QtPropertiesBinder::valueChanged(QtProperty* property, int value) {
	if (!canChangeValue) {
		return;
	}

	if (intPointersMap.contains(property)) {
		int* ptr = intPointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);
		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * bool
 */
void QtPropertiesBinder::valueChanged(QtProperty* property, bool value) {
	if (!canChangeValue) {
		return;
	}

	if (boolPointersMap.contains(property)) {
		bool* ptr = boolPointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);
		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * float & double
 */
void QtPropertiesBinder::valueChanged(QtProperty* property, double value) {
	if (!canChangeValue) {
		return;
	}

	if (floatPointersMap.contains(property)) {
		float* ptr = floatPointersMap[property];
		*ptr = float(value);
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	} else if (doublePointersMap.contains(property)) {
		double* ptr = doublePointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * color
 */
void QtPropertiesBinder::valueChanged(QtProperty* property, const QColor& value) {
	if (!canChangeValue) {
		return;
	}

	if (color3PointersMap.contains(property)) {
		rev::color3* ptr = color3PointersMap[property];
		*ptr = toGLColor3(value);
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}

	if (color4PointersMap.contains(property)) {
		rev::color4* ptr = color4PointersMap[property];
		*ptr = toGLColor4(value);
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * vec2
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::vec2& value) {
	if (!canChangeValue) {
		return;
	}

	if (vec2PointersMap.contains(property)) {
		glm::vec2* ptr = vec2PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * vec3
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::vec3& value) {
	if (!canChangeValue) {
		return;
	}

	if (vec3PointersMap.contains(property)) {
		glm::vec3* ptr = vec3PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * vec4
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::vec4& value) {
	if (!canChangeValue) {
		return;
	}

	if (vec4PointersMap.contains(property)) {
		glm::vec4* ptr = vec4PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * mat2
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::mat2& value) {
	if (!canChangeValue) {
		return;
	}

	if (mat2PointersMap.contains(property)) {
		glm::mat2* ptr = mat2PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * mat3
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::mat3& value) {
	if (!canChangeValue) {
		return;
	}

	if (mat3PointersMap.contains(property)) {
		glm::mat3* ptr = mat3PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * mat4
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::mat4& value) {
	if (!canChangeValue) {
		return;
	}

	if (mat4PointersMap.contains(property)) {
		glm::mat4* ptr = mat4PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * quat
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::quat& value) {
	if (!canChangeValue) {
		return;
	}

	if (quatPointersMap.contains(property)) {
		glm::quat* ptr = quatPointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dvec2
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dvec2& value) {
	if (!canChangeValue) {
		return;
	}

	if (dvec2PointersMap.contains(property)) {
		glm::dvec2* ptr = dvec2PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dvec3
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dvec3& value) {
	if (!canChangeValue) {
		return;
	}

	if (dvec3PointersMap.contains(property)) {
		glm::dvec3* ptr = dvec3PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dvec4
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dvec4& value) {
	if (!canChangeValue) {
		return;
	}

	if (dvec4PointersMap.contains(property)) {
		glm::dvec4* ptr = dvec4PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dmat2
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dmat2& value) {
	if (!canChangeValue) {
		return;
	}

	if (dmat2PointersMap.contains(property)) {
		glm::dmat2* ptr = dmat2PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dmat3
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dmat3& value) {
	if (!canChangeValue) {
		return;
	}

	if (dmat3PointersMap.contains(property)) {
		glm::dmat3* ptr = dmat3PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dmat4
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dmat4& value) {
	if (!canChangeValue) {
		return;
	}

	if (dmat4PointersMap.contains(property)) {
		glm::dmat4* ptr = dmat4PointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * dquat
 */
void QtPropertiesBinder::valueChanged(QtProperty* property,
		const glm::dquat& value) {
	if (!canChangeValue) {
		return;
	}

	if (dquatPointersMap.contains(property)) {
		glm::dquat* ptr = dquatPointersMap[property];
		*ptr = value;
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

/**
 * string
 */
void QtPropertiesBinder::valueChanged(QtProperty* property, const QString& value) {
	if (!canChangeValue) {
		return;
	}

	if (stringPointersMap.contains(property)) {
		std::string* ptr = stringPointersMap[property];
		*ptr = value.toStdString();
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

void QtPropertiesBinder::filepathValueChanged(QtProperty* property, const QString& value) {
	if (!canChangeValue) {
		return;
	}

	if (filepathPointersMap.contains(property)) {
		std::string* ptr = filepathPointersMap[property];
		*ptr = value.toStdString();
		IBindable* bindable = propertyBindableMap[property];
		bindable->bindableValueChanged(ptr);

		if (changeListener) {
			changeListener(ptr);
		}
	}
}

void QtPropertiesBinder::clearAll() {
	intPointersMap.clear();
	boolPointersMap.clear();
	floatPointersMap.clear();
	doublePointersMap.clear();
	color3PointersMap.clear();
	color4PointersMap.clear();

	vec2PointersMap.clear();
	vec3PointersMap.clear();
	vec4PointersMap.clear();
	mat2PointersMap.clear();
	mat3PointersMap.clear();
	mat4PointersMap.clear();
	quatPointersMap.clear();

	dvec2PointersMap.clear();
	dvec3PointersMap.clear();
	dvec4PointersMap.clear();
	dmat2PointersMap.clear();
	dmat3PointersMap.clear();
	dmat4PointersMap.clear();
	dquatPointersMap.clear();

	stringPointersMap.clear();
	filepathPointersMap.clear();
	groupsMap.clear();
	propertyBindableMap.clear();

	propertyBrowser->clear();
	//baseBindable = nullptr;
}

void QtPropertiesBinder::setExpanend(QtProperty* property, bool expanded) {
	QList<QtBrowserItem*> items = propertyBrowser->items(property);

	assertMsg(items.size() > 0, "Cannot find property: "
			<< property->propertyName().toStdString());

	if (items.size() > 0) {
		QtBrowserItem* browseItem = items[0];
		propertyBrowser->setExpanded(browseItem, expanded);
	}
}

QtProperty* QtPropertiesBinder::getGroupProperty(IBindable* bindable) {
	BinderGroupMapIter it = groupsMap.find(bindable);
	if (it != groupsMap.end()) {
		return it.value();
	}

	QtProperty* group = groupManager->addProperty(bindable->getBindableName());
	propertyBrowser->addProperty(group);
	groupsMap[bindable] = group;

	return group;
}

//=====================================================================

/**
 * IBindable.
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<IBindable*> wrapper, bool editable, bool visible, bool expanded) {
	IBindable* value = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, value, editable, visible, expanded);
}

/**
 * BasicTypeListMemberWrapper
 */
void QtPropertiesBinder::bindList(IBindable* bindable, const char* name,
		BasicTypeListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	BasicTypeListBinderIterator iter = wrapper.begin(bindable);
	BasicTypeListBinderIterator endIter = wrapper.end(bindable);

	if (wrapper.getTypeInfo() == typeid(int)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			int* ptr = static_cast<int*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(float)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			float* ptr = static_cast<float*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(double)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			double* ptr = static_cast<double*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(bool)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			bool* ptr = static_cast<bool*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(std::string)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			std::string* ptr = static_cast<std::string*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::vec2)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::vec2* ptr = static_cast<glm::vec2*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::vec3)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::vec3* ptr = static_cast<glm::vec3*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::vec4)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::vec4* ptr = static_cast<glm::vec4*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::mat2)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::mat2* ptr = static_cast<glm::mat2*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::mat3)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::mat3* ptr = static_cast<glm::mat3*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::mat4)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::mat4* ptr = static_cast<glm::mat4*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::quat)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::quat* ptr = static_cast<glm::quat*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(rev::color3)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			rev::color3* ptr = static_cast<rev::color3*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(rev::color4)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			rev::color4* ptr = static_cast<rev::color4*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	}
	else if (wrapper.getTypeInfo() == typeid(glm::dvec2)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dvec2* ptr = static_cast<glm::dvec2*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dvec3)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dvec3* ptr = static_cast<glm::dvec3*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dvec4)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dvec4* ptr = static_cast<glm::dvec4*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dmat2)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dmat2* ptr = static_cast<glm::dmat2*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dmat3)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dmat3* ptr = static_cast<glm::dmat3*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dmat4)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dmat4* ptr = static_cast<glm::dmat4*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	} else if (wrapper.getTypeInfo() == typeid(glm::dquat)) {
		int index = 0;
		for (; iter != endIter; ++iter) {
			glm::dquat* ptr = static_cast<glm::dquat*>(iter.getPointer());
			QString nameStr = QString("%1[%2]").arg(name).arg(index++);
			bindVal(bindable, nameStr.toUtf8().data(), *ptr, editable, visible, expanded);
		}
	}
}

/**
 * BindableListMemberWrapper
 */
void QtPropertiesBinder::bindList(IBindable* bindable, const char* name,
		BindableListMemberWrapper wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	BindableListBinderIterator iter = wrapper.begin(bindable);
	BindableListBinderIterator endIter = wrapper.end(bindable);

	int index = 0;
	for (; iter != endIter; ++iter) {
		QString nameStr = QString("%1[%2]").arg(name).arg(index++);
		bindVal(bindable, name, iter.getPointer(), editable, visible, expanded);
	}
}

/**
 * int
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<int> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	int& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * float
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<float> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	float& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * double
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<double> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	double& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * bool
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<bool> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	bool& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * std::string
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<std::string> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	std::string& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * filepath (std::string)
 */
void QtPropertiesBinder::bindFilepath(IBindable* bindable, const char* name,
		MemberWrapper<std::string> wrapper, const char* fileFilter,
		bool editable, bool visible) {
	if (!visible) {
		return;
	}
	std::string& str = *(wrapper.getMemberPointer(bindable));
	QtProperty* property = filepathManager->addProperty(name);

	property->setEnabled(editable);
	filepathManager->setValue(property, str.c_str());

	if (fileFilter) {
		filepathManager->setFilter(property, fileFilter);
	}
	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	filepathPointersMap[property] = &str;
	propertyBindableMap[property] = bindable;
}

/**
 * vec2
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::vec2> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::vec2& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * vec3
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::vec3> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::vec3& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * vec4
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::vec4> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::vec4& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * mat2
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::mat2> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::mat2& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * mat3
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::mat3> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::mat3& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);

}

/**
 * mat4
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::mat4> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::mat4& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * quat
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::quat> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::quat& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dvec2
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dvec2> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dvec2& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dvec3
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dvec3> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dvec3& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dvec4
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dvec4> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dvec4& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dmat2
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dmat2> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dmat2& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dmat3
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dmat3> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dmat3& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);

}

/**
 * dmat4
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dmat4> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dmat4& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * dquat
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<glm::dquat> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	glm::dquat& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * color3
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<rev::color3> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	rev::color3& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);
}

/**
 * color4
 */
void QtPropertiesBinder::bind(IBindable* bindable, const char* name,
		MemberWrapper<rev::color4> wrapper, bool editable, bool visible, bool expanded) {
	if (!visible) {
		return;
	}
	rev::color4& val = *(wrapper.getMemberPointer(bindable));
	bindVal(bindable, name, val, editable, visible, expanded);

}

void QtPropertiesBinder::refreshAll() {
	lockValueChangeListeners();

	auto intBegin = intPointersMap.begin();
	auto intEnd = intPointersMap.end();
	for (; intBegin != intEnd; ++intBegin) {
		intManager->setValue(intBegin.key(), *intBegin.value());
	}
	auto boolBegin = boolPointersMap.begin();
	auto boolEnd = boolPointersMap.end();
	for (; boolBegin != boolEnd; ++boolBegin) {
		boolManager->setValue(boolBegin.key(), *boolBegin.value());
	}
	auto floatBegin = floatPointersMap.begin();
	auto floatEnd = floatPointersMap.end();
	for (; floatBegin != floatEnd; ++floatBegin) {
		doubleManager->setValue(floatBegin.key(), (double) (*floatBegin.value()));
		doubleManager->setSingleStep(floatBegin.key(), SINGLE_STEP);
	}
	auto doubleBegin = doublePointersMap.begin();
	auto doubleEnd = doublePointersMap.end();
	for (; doubleBegin != doubleEnd; ++doubleBegin) {
		doubleManager->setValue(doubleBegin.key(), *doubleBegin.value());
		doubleManager->setSingleStep(doubleBegin.key(), SINGLE_STEP);
	}
	auto color3Begin = color3PointersMap.begin();
	auto color3End = color3PointersMap.end();
	for (; color3Begin != color3End; ++color3Begin) {
		colorManager->setValue(color3Begin.key(), toQColor(*color3Begin.value()));
	}
	auto color4Begin = color4PointersMap.begin();
	auto color4End = color4PointersMap.end();
	for (; color4Begin != color4End; ++color4Begin) {
		colorManager->setValue(color4Begin.key(), toQColor(*color4Begin.value()));
	}
	auto vec2Begin = vec2PointersMap.begin();
	auto vec2End = vec2PointersMap.end();
	for (; vec2Begin != vec2End; ++vec2Begin) {
		vec2Manager->setValue(vec2Begin.key(), *vec2Begin.value());
	}
	auto vec3Begin = vec3PointersMap.begin();
	auto vec3End = vec3PointersMap.end();
	for (; vec3Begin != vec3End; ++vec3Begin) {
		vec3Manager->setValue(vec3Begin.key(), *vec3Begin.value());
	}
	auto vec4Begin = vec4PointersMap.begin();
	auto vec4End = vec4PointersMap.end();
	for (; vec4Begin != vec4End; ++vec4Begin) {
		vec4Manager->setValue(vec4Begin.key(), *vec4Begin.value());
	}
	auto mat2Begin = mat2PointersMap.begin();
	auto mat2End = mat2PointersMap.end();
	for (; mat2Begin != mat2End; ++mat2Begin) {
		mat2Manager->setValue(mat2Begin.key(), *mat2Begin.value());
	}
	auto mat3Begin = mat3PointersMap.begin();
	auto mat3End = mat3PointersMap.end();
	for (; mat3Begin != mat3End; ++mat3Begin) {
		mat3Manager->setValue(mat3Begin.key(), *mat3Begin.value());
	}
	auto mat4Begin = mat4PointersMap.begin();
	auto mat4End = mat4PointersMap.end();
	for (; mat4Begin != mat4End; ++mat4Begin) {
		mat4Manager->setValue(mat4Begin.key(), *mat4Begin.value());
	}
	auto quatBegin = quatPointersMap.begin();
	auto quatEnd = quatPointersMap.end();
	for (; quatBegin != quatEnd; ++quatBegin) {
		quatManager->setValue(quatBegin.key(), *quatBegin.value());
	}

	auto dvec2Begin = dvec2PointersMap.begin();
	auto dvec2End = dvec2PointersMap.end();
	for (; dvec2Begin != dvec2End; ++dvec2Begin) {
		dvec2Manager->setValue(dvec2Begin.key(), *dvec2Begin.value());
	}
	auto dvec3Begin = dvec3PointersMap.begin();
	auto dvec3End = dvec3PointersMap.end();
	for (; dvec3Begin != dvec3End; ++dvec3Begin) {
		dvec3Manager->setValue(dvec3Begin.key(), *dvec3Begin.value());
	}
	auto dvec4Begin = dvec4PointersMap.begin();
	auto dvec4End = dvec4PointersMap.end();
	for (; dvec4Begin != dvec4End; ++dvec4Begin) {
		dvec4Manager->setValue(dvec4Begin.key(), *dvec4Begin.value());
	}
	auto dmat2Begin = dmat2PointersMap.begin();
	auto dmat2End = dmat2PointersMap.end();
	for (; dmat2Begin != dmat2End; ++dmat2Begin) {
		dmat2Manager->setValue(dmat2Begin.key(), *dmat2Begin.value());
	}
	auto dmat3Begin = dmat3PointersMap.begin();
	auto dmat3End = dmat3PointersMap.end();
	for (; dmat3Begin != dmat3End; ++dmat3Begin) {
		dmat3Manager->setValue(dmat3Begin.key(), *dmat3Begin.value());
	}
	auto dmat4Begin = dmat4PointersMap.begin();
	auto dmat4End = dmat4PointersMap.end();
	for (; dmat4Begin != dmat4End; ++dmat4Begin) {
		dmat4Manager->setValue(dmat4Begin.key(), *dmat4Begin.value());
	}
	auto dquatBegin = dquatPointersMap.begin();
	auto dquatEnd = dquatPointersMap.end();
	for (; dquatBegin != dquatEnd; ++dquatBegin) {
		dquatManager->setValue(dquatBegin.key(), *dquatBegin.value());
	}

	auto stringBegin = stringPointersMap.begin();
	auto stringEnd = stringPointersMap.end();
	for (; stringBegin != stringEnd; ++stringBegin) {
		stringManager->setValue(stringBegin.key(), (*stringBegin.value()).c_str());
	}
	auto filepathBegin = filepathPointersMap.begin();
	auto filepathEnd = filepathPointersMap.end();
	for (; filepathBegin != filepathEnd; ++filepathBegin) {
		filepathManager->setValue(filepathBegin.key(), (*filepathBegin.value()).c_str());
	}

	unlockValueChangeListeners();
}

/**
 * int
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		int& val) {

	QtProperty* property = intManager->addProperty(name);
	property->setEnabled(false);
	intManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	intPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;
}

/**
 * float
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		float& val) {

	QtProperty* property = doubleManager->addProperty(name);
	property->setEnabled(false);
	doubleManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	floatPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;
}

/**
 * double
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		double& val) {

	QtProperty* property = doubleManager->addProperty(name);
	property->setEnabled(false);
	doubleManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	doublePointersMap[property] = &val;
	propertyBindableMap[property] = bindable;
}

/**
 * bool
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		bool& val) {

	QtProperty* property = boolManager->addProperty(name);
	property->setEnabled(false);
	boolManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	boolPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;
}

/**
 * std::string
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		std::string& str) {

	QtProperty* property = stringManager->addProperty(name);
	property->setEnabled(false);
	stringManager->setValue(property, str.c_str());

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	stringPointersMap[property] = &str;
	propertyBindableMap[property] = bindable;
}

/**
 * vec2
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::vec2& v) {

	QtProperty* property = vec2Manager->addProperty(name);
	property->setEnabled(false);
	vec2Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec2PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * vec3
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::vec3& v) {

	QtProperty* property = vec3Manager->addProperty(name);
	property->setEnabled(false);
	vec3Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec3PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * vec4
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::vec4& v) {

	QtProperty* property = vec4Manager->addProperty(name);
	property->setEnabled(false);
	vec4Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec4PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * mat2
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::mat2& v) {

	QtProperty* property = mat2Manager->addProperty(name);
	property->setEnabled(false);
	mat2Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat2PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * mat3
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::mat3& v) {

	QtProperty* property = mat3Manager->addProperty(name);
	property->setEnabled(false);
	mat3Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat3PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * mat4
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::mat4& v) {

	QtProperty* property = mat4Manager->addProperty(name);
	property->setEnabled(false);
	mat4Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat4PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * quat
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::quat& q) {

	QtProperty* property = quatManager->addProperty(name);
	property->setEnabled(false);
	quatManager->setValue(property, q);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	quatPointersMap[property] = &q;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dvec2
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dvec2& v) {
	QtProperty* property = dvec2Manager->addProperty(name);
	property->setEnabled(false);
	dvec2Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec2PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dvec3
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dvec3& v) {
	QtProperty* property = dvec3Manager->addProperty(name);
	property->setEnabled(false);
	dvec3Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec3PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dvec4
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dvec4& v) {
	QtProperty* property = dvec4Manager->addProperty(name);
	property->setEnabled(false);
	dvec4Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec4PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dmat2
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dmat2& v) {
	QtProperty* property = dmat2Manager->addProperty(name);
	property->setEnabled(false);
	dmat2Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat2PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dmat3
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dmat3& v) {
	QtProperty* property = dmat3Manager->addProperty(name);
	property->setEnabled(false);
	dmat3Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat3PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dmat4
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dmat4& v) {
	QtProperty* property = dmat4Manager->addProperty(name);
	property->setEnabled(false);
	dmat4Manager->setValue(property, v);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat4PointersMap[property] = &v;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * dquat
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		glm::dquat& q) {
	QtProperty* property = dquatManager->addProperty(name);
	property->setEnabled(false);
	dquatManager->setValue(property, q);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dquatPointersMap[property] = &q;
	propertyBindableMap[property] = bindable;

	setExpanend(property, false);
}

/**
 * color3
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		rev::color3& color) {

	QtProperty* property = colorManager->addProperty(name);
	property->setEnabled(false);
	QColor qColor = toQColor(color);
	colorManager->setValue(property, qColor);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	color3PointersMap[property] = &color;
	propertyBindableMap[property] = bindable;
}

/**
 * color4
 */
void QtPropertiesBinder::bindInfo(IBindable* bindable, const char* name,
		rev::color4& color) {

	QtProperty* property = colorManager->addProperty(name);
	property->setEnabled(false);

	QColor qColor = toQColor(color);
	colorManager->setValue(property, qColor);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	color4PointersMap[property] = &color;
	propertyBindableMap[property] = bindable;
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name, IBindable* value,
		bool editable, bool visible, bool expanded) {
	if (!value || !visible) {
		return;
	}
	value->bind(*this);
	QtProperty* group = getGroupProperty(value);
	group->setEnabled(editable);

	setExpanend(group, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name, int& val,
		bool editable, bool visible, bool expanded) {
	QtProperty* property = intManager->addProperty(name);

	property->setEnabled(editable);
	intManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	intPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name, float& val,
		bool editable, bool visible, bool expanded) {
	QtProperty* property = doubleManager->addProperty(name);

	property->setEnabled(editable);
	doubleManager->setValue(property, val);
	doubleManager->setSingleStep(property, SINGLE_STEP);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	floatPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name, double& val,
		bool editable, bool visible, bool expanded) {
	QtProperty* property = doubleManager->addProperty(name);

	property->setEnabled(editable);
	doubleManager->setValue(property, val);
	doubleManager->setSingleStep(property, SINGLE_STEP);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	doublePointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name, bool& val,
		bool editable, bool visible, bool expanded) {
	QtProperty* property = boolManager->addProperty(name);

	property->setEnabled(editable);
	boolManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	boolPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		std::string& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = stringManager->addProperty(name);

	property->setEnabled(editable);
	stringManager->setValue(property, val.c_str());

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	stringPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::vec2& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = vec2Manager->addProperty(name);

	property->setEnabled(editable);
	vec2Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec2PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::vec3& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = vec3Manager->addProperty(name);

	property->setEnabled(editable);
	vec3Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec3PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::vec4& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = vec4Manager->addProperty(name);

	property->setEnabled(editable);
	vec4Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	vec4PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::mat2& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = mat2Manager->addProperty(name);

	property->setEnabled(editable);
	mat2Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat2PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::mat3& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = mat3Manager->addProperty(name);

	property->setEnabled(editable);
	mat3Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat3PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::mat4& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = mat4Manager->addProperty(name);

	property->setEnabled(editable);
	mat4Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	mat4PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);

}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::quat& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = quatManager->addProperty(name);

	property->setEnabled(editable);
	quatManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	quatPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dvec2& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dvec2Manager->addProperty(name);

	property->setEnabled(editable);
	dvec2Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec2PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dvec3& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dvec3Manager->addProperty(name);

	property->setEnabled(editable);
	dvec3Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec3PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dvec4& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dvec4Manager->addProperty(name);

	property->setEnabled(editable);
	dvec4Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dvec4PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dmat2& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dmat2Manager->addProperty(name);

	property->setEnabled(editable);
	dmat2Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat2PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dmat3& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dmat3Manager->addProperty(name);

	property->setEnabled(editable);
	dmat3Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat3PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dmat4& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dmat4Manager->addProperty(name);

	property->setEnabled(editable);
	dmat4Manager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dmat4PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		glm::dquat& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = dquatManager->addProperty(name);

	property->setEnabled(editable);
	dquatManager->setValue(property, val);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	dquatPointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		rev::color3& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = colorManager->addProperty(name);

	property->setEnabled(editable);
	QColor qColor = toQColor(val);
	colorManager->setValue(property, qColor);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	color3PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}

void QtPropertiesBinder::bindVal(IBindable* bindable, const char* name,
		rev::color4& val, bool editable, bool visible, bool expanded) {
	QtProperty* property = colorManager->addProperty(name);

	property->setEnabled(editable);

	QColor qColor = toQColor(val);
	colorManager->setValue(property, qColor);

	QtProperty* group = getGroupProperty(bindable);
	group->addSubProperty(property);

	color4PointersMap[property] = &val;
	propertyBindableMap[property] = bindable;

	setExpanend(property, expanded);
}
