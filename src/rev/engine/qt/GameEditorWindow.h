/*
 * GameEditorWindow.h
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#ifndef GAMEEDITORWINDOW_H_
#define GAMEEDITORWINDOW_H_

#include <QMainWindow>
#include <QGLFormat>
#include <rev/common/RevFPSCounter.hpp>
#include <rev/engine/tools/RevToolRegistry.h>
#include "ui_GameEditorWindow.h"

#define REV_NEW_OBJECT_DISTANCE 10.0f

class QtTreePropertyBrowser;
class QLabel;
class QSplitter;
class QPoint;
class QMenu;
class QAction;
class QCloseEvent;

namespace rev {
class GameObject;
class GameGLWidget;
class CameraWidget;
class PhysicsWidget;
class NewObjectWidget;
class SceneWidget;
class OptionsWidget;
class ScriptingWidget;
class GameWidgetsController;
class SurfacesWidget;
class LightingWidget;
class QtPropertiesBinder;
class GameObjectClipboard;
}

namespace rev {

class GameEditorWindow: public QMainWindow {
Q_OBJECT

	static GameEditorWindow* instance;

private:
	Ui::GameEditorWindowUi widget;
	rev::FPSCounter fpsCounter;
	rev::ToolRegistry toolRegistry;
	rev::NewObjectWidget* newObjectWidget = nullptr;
	rev::ScriptingWidget* scriptingWidget = nullptr;
	rev::SurfacesWidget* surfacesWidget = nullptr;
	rev::GameGLWidget* gameWidget = nullptr;
	rev::CameraWidget* cameraWidget = nullptr;
	rev::PhysicsWidget* physicsWidget = nullptr;
	rev::SceneWidget* sceneWidget = nullptr;
	rev::OptionsWidget* optionsWidget = nullptr;
	rev::LightingWidget* lightingWidget = nullptr;
	rev::GameWidgetsController* widgetsController = nullptr;
	rev::GameObjectClipboard* clipboard = nullptr;
	QtTreePropertyBrowser* propertyBrowser = nullptr;
	QtPropertiesBinder* propertiesBinder = nullptr;
	QLabel* statsLabel = nullptr;
	QSplitter* mainSplitter = nullptr;
	QSplitter* secondSplitter = nullptr;

	bool sidePanelVisEventActive = true;
	bool bottomPanelVisEventActive = true;
	rev::GameGLWidget* secondGameWidget = nullptr;
	rev::GameGLWidget* thirdGameWidget = nullptr;
	QWidget* focusedGLWidget = nullptr;

	QMenu* objectListMenu = nullptr;
	QAction* addObjectMenuAction = nullptr;
	QAction* removeObjectMenuAction = nullptr;
	QAction* editObjectMenuAction = nullptr;
	QAction* copyObjectMenuAction = nullptr;
	QAction* cutObjectMenuAction = nullptr;
	QAction* pasteObjectMenuAction = nullptr;

	ITool* moveTool = nullptr;
	ITool* rotateTool = nullptr;
	ITool* resizeTool = nullptr;
	ITool* lightTool = nullptr;
	bool canChangeTool = true;

	std::string scenePath;
	std::string sceneName;
	bool sceneSaved = true;
	float refreshTime = 0;
	bool cameraLockActionEnabled = true;
	bool exitEngine = false;

public:
	GameEditorWindow(const QGLFormat& format);
	virtual ~GameEditorWindow();

	rev::GameGLWidget* getGameGLWidget() {
		return gameWidget;
	}
	Ui::GameEditorWindowUi* getGameEditorUi() {
		return &widget;
	}
	static rev::GameEditorWindow* getInstance() {
		return instance;
	}
	static rev::GameEditorWindow& ref() {
		return *instance;
	}
	void initEngineRelatedStuff();

	QtPropertiesBinder* getPropertiesBinder() {
		return propertiesBinder;
	}
	GameWidgetsController* getGameWidgetsController() {
		return widgetsController;
	}
	rev::ScriptingWidget* getScriptingWidget() {
		return scriptingWidget;
	}
	rev::SurfacesWidget* getSurfacesWidget() {
		return surfacesWidget;
	}
	rev::PhysicsWidget* getPhysicsWidget() {
		return physicsWidget;
	}
	rev::SceneWidget* getSceneWidget() {
		return sceneWidget;
	}
	rev::OptionsWidget* getOptionsWidget() {
		return optionsWidget;
	}
	rev::LightingWidget* getLightingWidget() {
		return lightingWidget;
	}
	rev::ToolRegistry* getToolRegistry() {
		return &toolRegistry;
	}
	rev::ITool* getCurrentTool() {
		return toolRegistry.getCurrentTool();
	}
	void setCurrentTool(rev::ITool* tool) {
		return toolRegistry.setCurrentTool(tool);
	}

	void setPropertiesPanelVisible(bool visible);
	void setSecondGameWidgetVisible(bool visible);

	void setCursorOpenHand();
	void setCursorClosedHand();
	void setCursorArrow();

	QWidget* getFocusedGlWidget() {
		return focusedGLWidget;
	}
	void setFocusedGlWidget(QWidget* focusedGlWidget) {
		focusedGLWidget = focusedGlWidget;
	}

	/**
	 * If "editMode" is set to false, it means it is "newMode".
	 */
	void setNewPanelVisible(bool visible, bool editMode);
	bool isNewPanelVisible();
	void repickNewOrEditedObject();
	void setObjectPicked(bool picked);
	void refreshEngineControls();
	void clearPropertiesPanel();
	void bindPropertiesPanel(GameObject* go);
	void scrollObjectTableToSelectedItem();
	void silentRemoveCameraLock();
	void cameraUnlock();
	void refreshPanels();

	bool isSceneSaved() {
		return sceneSaved;
	}
	void setSceneSaved(bool sceneSaved);

protected:
	void closeEvent(QCloseEvent* event) override;

private:
	void scheduleExitEngine();
	bool saveScene();
	bool saveSceneAs();
	bool saveUnsavedScene();
	void initObjectListMenu();
	void initClipboard();
	void setupActions();
	void initPropertiesBrowser();
	void showPolygonInfo();
	void removeObject(GameObject* go);
	void editObject(GameObject* go);
	int getSelectedRowIndex();
	GameObject* getSelectedGameObject();
	void loadDefaultScene();
	void setSceneNameAndPath(const QString& filename);
	void toolChanged(ITool* tool);
	void deselectAllTools();
	void initTools();
	void setToolsEnabled(bool enabled);

public slots:
	void copyGameObject();
	void pasteGameObject();
	void cutGameObject();
	void clearClipboard();

private slots:
	void refreshTimerAction();
	void displayTableMenu(const QPoint& pos);

	void exitAction();
	void addObjectAction();
	void removePickedObjectMenuAction();
	void editPickedObjectMenuAction();
	void removeObjectTableAction();
	void editObjectTableAction();

	void copyObjectTableAction();
	void cutObjectTableAction();
	void pasteObjectTableAction();

	void aboutAction();
	void openSceneAction();
	void saveSceneAction();
	void saveSceneAsAction();
	void newSceneAction();
	void updateEngine();
	void objectListToggled(bool toggled);
	void showSideBarToggled(bool toggled);
	void showBottomBarToggled(bool toggled);
	void showToolbarToggled(bool toggled);
	void deselectButtonPressed();
	void editComponentsButtonPressed();
	void removeObjectButtonPressed();
	void cameraLockButtonToggled(bool toggled);
	void exclusiveModeButtonToggled(bool toggled);
	void sidePanelVisibilityChanged(bool visible);
	void bottomPanelVisibilityChanged(bool visible);
	void singleViewportActionToggled(bool toggled);
	void doubleVertViewportActionToggled(bool toggled);
	void doubleHorViewportActionToggled(bool toggled);
	void tripleViewportActionToggled(bool toggled);

	void moveModeActionToggled(bool toggled);
	void rotateModeActionToggled(bool toggled);
	void resizeModeActionToggled(bool toggled);
	void lightModeActionToggled(bool toggled);

signals:
	void logOutput(const QString& line);
};

}
#endif /* GAMEEDITORWINDOW_H_ */
