/*
 * GameGLWidget.cpp
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#include "GameGLWidget.h"

#include <iostream>

#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QFocusEvent>

#include <rev/common/RevAssert.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/RevEngine.h>

#include "GameGLWidget.h"
#include "GameEditorWindow.h"
#include "ui_GameEditorWindow.h"

using namespace rev;
using namespace glm;
using namespace std;

GameGLWidget::GameGLWidget(Ui::GameEditorWindowUi* widget,
		const QGLFormat& format, int contextIndex, QWidget* parent) :
		QGLWidget(format, parent), widget(widget),
				contextIndex(contextIndex) {
	this->gameEditorWindow = (rev::GameEditorWindow*) parent;
	// This tells the widget to accept keyboard
	// focus when the widget is clicked.
	this->setFocusPolicy(Qt::ClickFocus);

	//showCameraCoords();

	setupActions();
}

GameGLWidget::GameGLWidget(Ui::GameEditorWindowUi* widget,
		QGLWidget* shareWidget, int contextIndex, QWidget* parent) :
		QGLWidget(parent, shareWidget), widget(widget),
				contextIndex(contextIndex) {
	this->gameEditorWindow = (rev::GameEditorWindow*) parent;
	// This tells the widget to accept keyboard
	// focus when the widget is clicked.
	this->setFocusPolicy(Qt::ClickFocus);

	//showCameraCoords();

	setupActions();
}

GameGLWidget::~GameGLWidget() {
	if (GLContextManager::getInstance()) {
		GLContextManager::ref().unregisterContextIndex(contextIndex);
	}
}

void GameGLWidget::setupActions() {
	setMouseTracking(true);
}

void GameGLWidget::initializeGL() {
	GLContextManager::ref().registerCurrentContext(contextIndex);

	if (!Renderer3D::ref().initContext(contextIndex)) {
		REV_ERROR_MSG("Renderer3D::initContext() FAILED!");
		exit(-1);
	}
	glAlwaysAssert;
}

void GameGLWidget::paintGL() {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	Renderer3D::ref().render();
}

void GameGLWidget::resizeGL(int w, int h) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	rev::Renderer3D::ref().resize(w, h);
}

void GameGLWidget::mousePressEvent(QMouseEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireMousePressedEvent(event->x(), event->y(),
			(rev::MouseButton) event->button());

	if (event->button() == Qt::LeftButton) {
		mousePressed = true;
		//  showCameraCoords();
	}

	repaint();
}

void GameGLWidget::mouseReleaseEvent(QMouseEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireMouseReleasedEvent(event->x(), event->y(),
			(rev::MouseButton) event->button());
	if (event->button() == Qt::LeftButton) {
		mousePressed = false;
	}
}

void GameGLWidget::mouseMoveEvent(QMouseEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireMouseMovedEvent(event->x(), event->y());

	if (!mousePressed) {
		return;
	}
	//  showCameraCoords();
	repaint();

}

void GameGLWidget::wheelEvent(QWheelEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireMouseWheelEvent(event->delta());

	//showCameraCoords();

	repaint();
}

void GameGLWidget::keyReleaseEvent(QKeyEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireKeyReleasedEvent((rev::Key) event->key(),
			(int) event->modifiers());
}

void GameGLWidget::keyPressEvent(QKeyEvent* event) {
	GLContextManager::ref().setCurrentContextIndex(contextIndex);
	EventManager::ref().fireKeyPressedEvent((rev::Key) event->key(),
			(int) event->modifiers());

//    if (event->key() == Qt::Key_Space) {
//        bool checked = widget->patch3DWireFrameModeCB->isChecked();
//        widget->patch3DWireFrameModeCB->setChecked(!checked);
//    }
}

void GameGLWidget::focusInEvent(QFocusEvent* e) {
	// REV_TRACE_MSG("Focused widget = " << this);
	gameEditorWindow->setFocusedGlWidget(this);
}
