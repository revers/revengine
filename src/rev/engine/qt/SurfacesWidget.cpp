/*
 * SurfacesWidget.cpp
 *
 *  Created on: 10-04-2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include <rev/engine/drawables/RevVBOPointSpheresVC.h>
#include <rev/engine/effects/RevControlPointsEffect.h>
#include <rev/engine/parametric/RevISurfacePatch.h>
#include <rev/engine/RevRenderer3D.h>
#include "GameEditorWindow.h"
#include "SurfacesWidget.h"

using namespace rev;

SurfacesWidget::SurfacesWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);
	uiWidget.editButton->setEnabled(false);
	enableWidgets(false);

	setupActions();
	uiWidget.sensitivitySlider->setValue(20);
	pickingReseted();
}

SurfacesWidget::~SurfacesWidget() {
	if (origPoints) {
		delete[] origPoints;
	}
	if (controlPointsGO) {
		delete controlPointsGO;
	}
// FIXME: another problem with destroying VBODrawable in Qt object's destructor:
//	if (component) {
//		delete component;
//	}
}

void SurfacesWidget::init() {
	drawable = new VBOPointSpheresVC();
	effect = new ControlPointsEffect();
	effect->setPointRadius(ControlPointPicker::CONTROL_POINT_RADIUS);
	effect->setHighlightColor(rev::color3(1, 1, 0));

	component = new Simple3DComponent(effect, true, drawable, true);
	component->setDebugElement(true);
	controlPointsGO = new GameObject();
	component->setParent(controlPointsGO);
}

void SurfacesWidget::setupActions() {
	connect(uiWidget.editButton, SIGNAL(clicked(bool)), this, SLOT(editButtonAction()));
	connect(uiWidget.applyButton, SIGNAL(clicked(bool)), this, SLOT(applyAction()));
	connect(uiWidget.cancelButton, SIGNAL(clicked(bool)), this, SLOT(cancelAction()));
	connect(uiWidget.sensitivitySlider, SIGNAL(valueChanged(int)), this,
			SLOT(sensitivityValueChanged(int)));
	connect(uiWidget.distanceSlider, SIGNAL(valueChanged(int)), this,
			SLOT(distanceValueChanged(int)));
}

void SurfacesWidget::objectPicked(GameObject* go) {
	IVisual3DComponent* visComp = go->getVisual3DComponent();
	if (!visComp) {
		pickingReseted();
		return;
	}
	Simple3DComponent* simpleComp = dynamic_cast<Simple3DComponent*>(visComp);
	if (!simpleComp) {
		pickingReseted();
		return;
	}
	IVBODrawable* drawable = simpleComp->getDrawable();
	ISurfacePatch* patch = dynamic_cast<ISurfacePatch*>(drawable);
	if (!patch) {
		pickingReseted();
		return;
	}
	pickedGameObject = go;
	surfacePatch = patch;
	uiWidget.editButton->setEnabled(true);
}

void SurfacesWidget::pickingReseted() {
	pickedGameObject = nullptr;
//	uiWidget.editButton->setEnabled(false);
//	enableWidgets(false);
//	resetValues();
//	if (origPoints) {
//		delete[] origPoints;
//		origPoints = nullptr;
//	}
//	Renderer3D::ref().removeComponent(component);
//	picker.setSurfacePatch(nullptr, nullptr);
	cancel();
}

void SurfacesWidget::refreshPickedObject() {
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (go) {
		objectPicked(go);
	} else {
		pickingReseted();
	}
}

void SurfacesWidget::resetValues() {
	canChangeSliderValue = false;
	uiWidget.distanceSlider->setValue(50);
	uiWidget.distanceLabel->setText(QString::number(uiWidget.distanceSlider->value() - 50));
	canChangeSliderValue = true;
}

void SurfacesWidget::enableWidgets(bool enable) {
	uiWidget.applyButton->setEnabled(enable);
	uiWidget.cancelButton->setEnabled(enable);
	uiWidget.sensitivitySlider->setEnabled(enable);
	uiWidget.distanceSlider->setEnabled(enable);
	uiWidget.sensitivityLabel->setEnabled(enable);
	uiWidget.distanceLabel->setEnabled(enable);
}

void SurfacesWidget::refreshPoints() {
	int uCount = surfacePatch->getUPointsCount();
	int vCount = surfacePatch->getVPointsCount();

	VBOPointSpheresVC::PointsVector& modifiedPoints = drawable->getPointsVector();
	for (int u = 0; u < uCount; u++) {
		for (int v = 0; v < vCount; v++) {
			int index = u * vCount + v;
			revAssert(index < uCount * vCount);
			modifiedPoints[index] = surfacePatch->getPoint(u, v);
		}
	}

	drawable->reinit();
}

void SurfacesWidget::sensitivityValueChanged(int value) {
	uiWidget.sensitivityLabel->setText(QString::number(uiWidget.sensitivitySlider->value()));
}

void SurfacesWidget::distanceValueChanged(int value) {
	if (!canChangeSliderValue || !surfacePatch) {
		return;
	}
	uiWidget.distanceLabel->setText(QString::number(uiWidget.distanceSlider->value() - 50));

	float percent;
	const float multiplier = 10.0f;

	if (value <= 50) {
		value = 50 - value;
		float val = float(value) / 100.0f;
		percent = 1.0f / (1.0f + val * multiplier);
	} else {
		value -= 50;
		float val = float(value) / 100.0f;
		percent = 1.0f + val * multiplier;
	}

	int uCount = surfacePatch->getUPointsCount();
	int vCount = surfacePatch->getVPointsCount();

	VBOPointSpheresVC::PointsVector& modifiedPoints = drawable->getPointsVector();
	for (int u = 0; u < uCount; u++) {
		for (int v = 0; v < vCount; v++) {
			int index = u * vCount + v;
			revAssert(index < uCount * vCount);
			modifiedPoints[index] = origPoints[index] * percent;
			surfacePatch->setPoint(u, v, modifiedPoints[index]);
		}
	}
	surfacePatch->recalculateSurface();
	drawable->reinit();
}

void SurfacesWidget::editButtonAction() {
	lastTool = mainWindow->getCurrentTool();
	mainWindow->setCurrentTool(this);
	editingMode = true;
	component->setParent(pickedGameObject);
	picker.setSurfacePatch(surfacePatch, pickedGameObject);
	enableWidgets(true);
	uiWidget.editButton->setEnabled(false);

	surfacePatch->setPointChangedCallback([this] () -> void {
		refreshPoints();
	});

	int uCount = surfacePatch->getUPointsCount();
	int vCount = surfacePatch->getVPointsCount();

	if (origPoints) {
		delete[] origPoints;
	}
	origPoints = new glm::vec3[uCount * vCount];

	for (int u = 0; u < uCount; u++) {
		for (int v = 0; v < vCount; v++) {
			int index = u * vCount + v;
			revAssert(index < uCount * vCount);
			origPoints[index] = surfacePatch->getPoint(u, v);
		}
	}

	int dataSize = uCount * vCount;
	drawable->setPoints(origPoints, dataSize);
	Renderer3D::ref().addComponent(component);
}

void SurfacesWidget::applyAction() {
	mainWindow->setCurrentTool(lastTool);
	editingMode = false;
	component->setParent(controlPointsGO);
	picker.setSurfacePatch(nullptr, nullptr);
	enableWidgets(false);
	uiWidget.editButton->setEnabled(true);
	mainWindow->setSceneSaved(false);
	resetValues();

	surfacePatch->setPointChangedCallback(nullptr);
	if (origPoints) {
		Renderer3D::ref().removeComponent(component);
		delete[] origPoints;
		origPoints = nullptr;
	}
}

void SurfacesWidget::cancel() {
	editingMode = false;
	picker.setSurfacePatch(nullptr, nullptr);

	if (origPoints) {
		int uCount = surfacePatch->getUPointsCount();
		int vCount = surfacePatch->getVPointsCount();
		for (int u = 0; u < uCount; u++) {
			for (int v = 0; v < vCount; v++) {
				int index = u * vCount + v;
				revAssert(index < uCount * vCount);
				surfacePatch->setPoint(u, v, origPoints[index]);
			}
		}
		surfacePatch->recalculateSurface();
		surfacePatch->setPointChangedCallback(nullptr);

		Renderer3D::ref().removeComponent(component);
		delete[] origPoints;
		origPoints = nullptr;
	}

	enableWidgets(false);
	if (pickedGameObject) {
		uiWidget.editButton->setEnabled(true);
	}
	resetValues();
}

void SurfacesWidget::cancelAction() {
	mainWindow->setCurrentTool(lastTool);
	component->setParent(controlPointsGO);

	cancel();
}

void SurfacesWidget::mousePressed(int x, int y, int contextIndex) {
	if (!isEditingMode()) {
		return;
	}

	lastMouseX = x;
	lastMouseY = y;
	uv = picker.getControlPoint(x, y, contextIndex);
	if (uv.isValid()) {
		mainWindow->setCursorClosedHand();
		effect->setHighlightPosition(surfacePatch->getPoint(uv.u, uv.v));
	} else {
		mainWindow->setCursorArrow();
		effect->setInvalidHighlightPosition();
	}
}

void SurfacesWidget::mouseReleased(int x, int y, int contextIndex) {
	effect->setInvalidHighlightPosition();
	mainWindow->setCursorOpenHand();
	dragPhase = false;
}

void SurfacesWidget::mouseDrag(int x, int y, int contextIndex) {
	dragPhase = true;
	if (!isEditingMode()) {
		return;
	}
	int yDiff = lastMouseY - y;

	lastMouseX = x;
	lastMouseY = y;

	if (!uv.isValid()) {
		return;
	}

	float percent = (float) uiWidget.sensitivitySlider->value() / 100.0f;
	float sensitivity = percent * 0.5f;

	int vCount = surfacePatch->getVPointsCount();
	VBOPointSpheresVC::PointsVector& modifiedPoints = drawable->getPointsVector();
	glm::vec3& v = modifiedPoints[uv.u * vCount + uv.v];
	v.y += (float) yDiff * sensitivity;
	surfacePatch->setPoint(uv.u, uv.v, v);
	surfacePatch->recalculateSurface();

	effect->setHighlightPosition(v);
	drawable->reinit();
}

void SurfacesWidget::mouseMoved(int x, int y, int contextIndex) {
	if (!isEditingMode() || dragPhase) {
		return;
	}
	int yDiff = lastMouseY - y;

	lastMouseX = x;
	lastMouseY = y;

	uv = picker.getControlPoint(x, y, contextIndex);
	if (uv.isValid()) {
		mainWindow->setCursorOpenHand();
	} else {
		mainWindow->setCursorArrow();
	}
}
