/*
 * GameEditorWindow.cpp
 *
 *  Created on: 10-01-2013
 *      Author: Revers
 */

#include <iostream>

#include "GameGLWidget.h"

#include <QDir>
#include <QFile>
#include <QCloseEvent>
#include <QFileDialog>
#include <QEvent>
#include <QTimer>
#include <QString>
#include <QMessageBox>
#include <QSettings>
#include <QGLFormat>
#include <QApplication>
#include <QLabel>
#include <QHBoxLayout>
#include <QSplitter>
#include <QtTreePropertyBrowser>

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevDelete.hpp>
#include <rev/engine/RevEngine.h>
#include <rev/common/RevResourcePath.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/xml/RevXMLConfigManager.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/util/RevGameObjectClipboard.h>

#include <rev/engine/tools/RevMoveTool.h>
#include <rev/engine/tools/RevResizeTool.h>
#include <rev/engine/tools/RevRotateTool.h>
#include <rev/engine/tools/RevLightTool.h>

#include "GameWidgetsController.h"
#include "tables/GameObjectTableModel.h"
#include "GameEditorWindow.h"

#include "QtPropertiesBinder.h"
#include "LogSyntaxHighlighter.h"
#include "NewObjectWidget.h"
#include "ScriptingWidget.h"
#include "CameraWidget.h"
#include "SurfacesWidget.h"
#include "PhysicsWidget.h"
#include "SceneWidget.h"
#include "OptionsWidget.h"
#include "LightingWidget.h"

using namespace std;
using namespace rev;

#ifdef NDEBUG
#define WINDOW_TITLE "GameEditor"
#elif defined(REV_DEBUG_FULL)
#define WINDOW_TITLE "GameEditor (DEBUG_FULL)"
#else
#define WINDOW_TITLE "GameEditor (DEBUG)"
#endif

#define DEFAULT_SCENE "scenes/default.scene.xml"
#define UNTITLED_SCENE_NAME "Untitled"
#define ENGINE_GUI_REFRESH_TIME 100

static const QString DEFAULT_DIR_KEY("default_dir");

rev::GameEditorWindow* rev::GameEditorWindow::instance = nullptr;

static QWidget* getTabWidget(QTabWidget* t, const QString& tabName) {
	for (int i = 0; i < t->count(); i++) {
		if (t->tabText(i) == tabName) {
			return t->widget(i);
		}
	}
	return nullptr;
}

GameEditorWindow::GameEditorWindow(const QGLFormat& format) :
		fpsCounter(1.0f / 60.0f) {
	instance = this;

	setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
	setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
	setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

	widget.setupUi(this);

	lightingWidget = new rev::LightingWidget(this);
	QWidget* lightingTab = getTabWidget(widget.bottomInnerPanel, "Lighting");
	revAssert(lightingTab);
	lightingTab->layout()->addWidget(lightingWidget);

	optionsWidget = new rev::OptionsWidget(this);
	QWidget* optionsTab = getTabWidget(widget.bottomInnerPanel, "Options");
	revAssert(optionsTab);
	optionsTab->layout()->addWidget(optionsWidget);

	sceneWidget = new rev::SceneWidget(this);
	QWidget* sceneTab = getTabWidget(widget.bottomInnerPanel, "Scene");
	revAssert(sceneTab);
	sceneTab->layout()->addWidget(sceneWidget);

	scriptingWidget = new rev::ScriptingWidget(this);
	QWidget* scriptingTab = getTabWidget(widget.bottomInnerPanel, "Scripting");
	revAssert(scriptingTab);
	scriptingTab->layout()->addWidget(scriptingWidget);

	cameraWidget = new rev::CameraWidget(this);
	QWidget* cameraTab = getTabWidget(widget.bottomInnerPanel, "Camera");
	revAssert(cameraTab);
	cameraTab->layout()->addWidget(cameraWidget);

	surfacesWidget = new rev::SurfacesWidget(this);
	QWidget* editTab = getTabWidget(widget.bottomInnerPanel, "Surfaces");
	revAssert(editTab);
	editTab->layout()->addWidget(surfacesWidget);

	physicsWidget = new rev::PhysicsWidget(this);
	QWidget* physicsTab = getTabWidget(widget.bottomInnerPanel, "Collision && Physics");
	revAssert(physicsTab);
	physicsTab->layout()->addWidget(physicsWidget);

	newObjectWidget = new rev::NewObjectWidget(this);
	gameWidget = new rev::GameGLWidget(&widget, format,
			GLContextManager::MAIN_CONTEXT, this);
	setFocusedGlWidget(gameWidget);

	mainSplitter = new QSplitter(Qt::Horizontal, this);
	secondSplitter = new QSplitter(Qt::Vertical, mainSplitter);

	mainSplitter->addWidget(gameWidget);
#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget = new rev::GameGLWidget(&widget, gameWidget,
			GLContextManager::SECOND_CONTEXT, this);
	secondSplitter->addWidget(secondGameWidget);
	mainSplitter->addWidget(secondSplitter);

	QList<int> sizes;
	sizes.push_back(200);
	sizes.push_back(200);
	mainSplitter->setSizes(sizes);
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	thirdGameWidget = new rev::GameGLWidget(&widget, gameWidget,
			GLContextManager::THIRD_CONTEXT, this);
	secondSplitter->addWidget(thirdGameWidget);
#endif

	widget.centralVertLayout->addWidget(mainSplitter);
	gameWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	propertyBrowser = new QtTreePropertyBrowser(this);
	widget.propertiesPanel->layout()->addWidget(propertyBrowser);
	setPropertiesPanelVisible(false);

	widget.splitter->insertWidget(1, newObjectWidget);
	widget.splitter->setStretchFactor(0, 1);
	widget.splitter->setStretchFactor(1, 0);
	widget.splitter->setStretchFactor(2, 2);

	widgetsController = new GameWidgetsController(this);
	propertiesBinder = new QtPropertiesBinder(propertyBrowser, this);
	propertiesBinder->setPropertyChangeListener([this] (void*) {
		setSceneSaved(false);
	});

	refreshEngineControls();

	new LogSyntaxHighlighter(widget.logTextEdit->document());

	rev::errout.addErrorStreamCallback([this] (const char* line) {
		QString str(line);

		emit logOutput(str.replace("<", "&lt;")
				.replace(">", "&gt;"));
		//.replace("  ", "&nbsp;&nbsp;"));
		});

#ifdef NDEBUG
	rev::errout.setUseStdout(false); // only log stream
#endif

	statsLabel = new QLabel();
	statusBar()->addPermanentWidget(statsLabel);

	QActionGroup* viewportGroup = new QActionGroup(this);
	viewportGroup->addAction(widget.singleViewportAction);
	viewportGroup->addAction(widget.doubleVertViewportAction);
	viewportGroup->addAction(widget.doubleHorViewportAction);
	viewportGroup->addAction(widget.tripleViewportAction);

	QActionGroup* cursorModeGroup = new QActionGroup(this);
	cursorModeGroup->addAction(widget.moveModeAction);
	cursorModeGroup->addAction(widget.rotateModeAction);
	cursorModeGroup->addAction(widget.resizeModeAction);
	cursorModeGroup->addAction(widget.lightModeAction);

	initObjectListMenu();
	initClipboard();

	setNewPanelVisible(false, false);
	setObjectPicked(false);
	setSceneSaved(true);

	setupActions();
	singleViewportActionToggled(true);

	QTimer* engineTimer = new QTimer(this);
	QObject::connect(engineTimer, SIGNAL(timeout()), this, SLOT(updateEngine()));
	engineTimer->start(0);

	resize(1200, 810);
}

GameEditorWindow::~GameEditorWindow() {
	DEL(widgetsController);
	DEL(moveTool);
	DEL(rotateTool);
	DEL(resizeTool);
	DEL(lightTool);
	DEL(clipboard);
}

void GameEditorWindow::initTools() {
	moveTool = new MoveTool();
	rotateTool = new RotateTool();
	resizeTool = new ResizeTool();
	lightTool = new LightTool();
	toolRegistry.addToolSwitchedCallback([this] (ITool* tool) -> void {
		toolChanged(tool);
	});

	ToolChangedCallback callback = [this] (ITool* tool) -> void {
		setSceneSaved(false);
	};

	moveTool->addToolChangedCallback(callback);
	rotateTool->addToolChangedCallback(callback);
	resizeTool->addToolChangedCallback(callback);
	lightTool->addToolChangedCallback(callback);

	toolRegistry.setCurrentTool(moveTool);

	// FIXME: is light tool necessery?
	widget.lightModeAction->setVisible(false);
}

void GameEditorWindow::initClipboard() {
	clipboard = new GameObjectClipboard();
}

void GameEditorWindow::setupActions() {

	connect(widget.actionExit, SIGNAL(triggered(bool)), this, SLOT(exitAction()));

	connect(widget.actionAbout, SIGNAL(triggered(bool)),
			this, SLOT(aboutAction()));

	connect(widget.newSceneAction, SIGNAL(triggered(bool)),
			this, SLOT(newSceneAction()));

	connect(widget.saveSceneAction, SIGNAL(triggered(bool)),
			this, SLOT(saveSceneAction()));

	connect(widget.saveSceneAsAction, SIGNAL(triggered(bool)),
			this, SLOT(saveSceneAsAction()));

	connect(widget.openSceneAction, SIGNAL(triggered(bool)),
			this, SLOT(openSceneAction()));

	connect(widget.showSidePanelAction, SIGNAL(triggered(bool)),
			this, SLOT(showSideBarToggled(bool)));

	connect(widget.showBottomPanelAction, SIGNAL(triggered(bool)),
			this, SLOT(showBottomBarToggled(bool)));

	connect(widget.showToolbarAction, SIGNAL(triggered(bool)),
			this, SLOT(showToolbarToggled(bool)));

	connect(widget.objectListButton, SIGNAL(toggled(bool)), this,
	SLOT(objectListToggled(bool)));

	connect(widget.cameraLockPB, SIGNAL(toggled(bool)), this,
	SLOT(cameraLockButtonToggled(bool)));

	connect(widget.exclusiveModePB, SIGNAL(toggled(bool)), this,
	SLOT(exclusiveModeButtonToggled(bool)));

	connect(widget.deselectPB, SIGNAL(clicked()), this,
	SLOT(deselectButtonPressed()));

	connect(widget.removePB, SIGNAL(clicked()), this,
	SLOT(removeObjectButtonPressed()));

	connect(widget.editComponentsPB, SIGNAL(clicked()), this,
	SLOT(editComponentsButtonPressed()));

	connect(this, SIGNAL(logOutput(const QString&)),
			widget.logTextEdit, SLOT(append(const QString&)));

	connect(widget.sidePanel, SIGNAL(visibilityChanged(bool)),
			this, SLOT(sidePanelVisibilityChanged(bool)));

	connect(widget.bottomPanel, SIGNAL(visibilityChanged(bool)),
			this, SLOT(bottomPanelVisibilityChanged(bool)));

	connect(widget.singleViewportAction, SIGNAL(toggled(bool)),
			this, SLOT(singleViewportActionToggled(bool)));

	connect(widget.doubleVertViewportAction, SIGNAL(toggled(bool)),
			this, SLOT(doubleVertViewportActionToggled(bool)));

	connect(widget.doubleHorViewportAction, SIGNAL(toggled(bool)),
			this, SLOT(doubleHorViewportActionToggled(bool)));

	connect(widget.tripleViewportAction, SIGNAL(toggled(bool)),
			this, SLOT(tripleViewportActionToggled(bool)));

	connect(widget.moveModeAction, SIGNAL(toggled(bool)),
			this, SLOT(moveModeActionToggled(bool)));

	connect(widget.rotateModeAction, SIGNAL(toggled(bool)),
			this, SLOT(rotateModeActionToggled(bool)));

	connect(widget.resizeModeAction, SIGNAL(toggled(bool)),
			this, SLOT(resizeModeActionToggled(bool)));

	connect(widget.lightModeAction, SIGNAL(toggled(bool)),
			this, SLOT(lightModeActionToggled(bool)));

	connect(widget.objectListTable, SIGNAL(customContextMenuRequested(QPoint)),
			this, SLOT(displayTableMenu(QPoint)));

	connect(widget.removePickedObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(removePickedObjectMenuAction()));

	connect(widget.editPickedObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(editPickedObjectMenuAction()));

	connect(widget.addObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(addObjectAction()));

	connect(widget.copyPickedObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(copyGameObject()));
	connect(widget.cutPickedObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(cutGameObject()));
	connect(widget.pasteObjectAction, SIGNAL(triggered(bool)),
			this, SLOT(pasteGameObject()));
}

void GameEditorWindow::exitAction() {
	if (isNewPanelVisible()) {
		QMessageBox msgBox;
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setWindowTitle("Unsaved editing");
		msgBox.setText("You are in the middle of editing an object. Do you really want to exit?");
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		int ret = msgBox.exec();

		if (ret == QMessageBox::Yes) {
			newObjectWidget->cancelAction();
			scheduleExitEngine();
		}
	} else if (saveUnsavedScene()) {
		scheduleExitEngine();
	}
}

void GameEditorWindow::repickNewOrEditedObject() {
	newObjectWidget->repickNewOrEditedObject();
	setNewPanelVisible(true, newObjectWidget->isEditMode());
}

bool GameEditorWindow::isNewPanelVisible() {
	return newObjectWidget->isVisible();
}

void GameEditorWindow::setNewPanelVisible(bool visible, bool editMode) {
	newObjectWidget->setVisible(visible);
	if (visible) {
		widget.objectListButton->setChecked(false);
		setPropertiesPanelVisible(true);
		addObjectMenuAction->setEnabled(false);
		removeObjectMenuAction->setEnabled(false);
		editObjectMenuAction->setEnabled(false);

		widget.removePickedObjectAction->setEnabled(false);
		widget.editPickedObjectAction->setEnabled(false);
		widget.addObjectAction->setEnabled(false);

		widget.removePB->setEnabled(false);
		widget.editComponentsPB->setEnabled(false);
		widget.deselectPB->setEnabled(false);

		widget.openSceneAction->setEnabled(false);
		widget.saveSceneAction->setEnabled(false);
		widget.saveSceneAsAction->setEnabled(false);
		widget.newSceneAction->setEnabled(false);
	} else {
		widget.objectListButton->setChecked(true);
		addObjectMenuAction->setEnabled(true);
		removeObjectMenuAction->setEnabled(true);
		editObjectMenuAction->setEnabled(true);

		widget.openSceneAction->setEnabled(true);
		widget.saveSceneAction->setEnabled(!sceneSaved);
		widget.saveSceneAsAction->setEnabled(true);
		widget.newSceneAction->setEnabled(true);

		widget.addObjectAction->setEnabled(true);
		if (editMode) {
			widget.removePickedObjectAction->setEnabled(false);
			widget.editPickedObjectAction->setEnabled(false);
		} else {
			widget.removePickedObjectAction->setEnabled(true);
			widget.editPickedObjectAction->setEnabled(true);
		}

		widget.removePB->setEnabled(true);
		widget.editComponentsPB->setEnabled(true);
		widget.deselectPB->setEnabled(true);
	}

	refreshPanels();
}

void GameEditorWindow::refreshPanels() {
	REV_TRACE_FUNCTION;
	physicsWidget->refreshPickedObject();
	surfacesWidget->refreshPickedObject();
}

void GameEditorWindow::initObjectListMenu() {
	widget.objectListTable->setContextMenuPolicy(Qt::CustomContextMenu);

	objectListMenu = new QMenu(this);
	addObjectMenuAction = objectListMenu->addAction("Add object...");
	QIcon addObjectIcon;
	addObjectIcon.addFile(QString::fromUtf8(":/images/addObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	addObjectMenuAction->setIcon(addObjectIcon);

	editObjectMenuAction = objectListMenu->addAction("Edit components");
	QIcon editObjectIcon;
	editObjectIcon.addFile(QString::fromUtf8(":/images/editObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	editObjectMenuAction->setIcon(editObjectIcon);

	removeObjectMenuAction = objectListMenu->addAction("Remove object");
	QIcon deleteObjectIcon;
	deleteObjectIcon.addFile(QString::fromUtf8(":/images/deleteObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	removeObjectMenuAction->setIcon(deleteObjectIcon);

	objectListMenu->addSeparator();

	copyObjectMenuAction = objectListMenu->addAction("Copy object");
	QIcon copyObjectIcon;
	copyObjectIcon.addFile(QString::fromUtf8(":/images/copyObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	copyObjectMenuAction->setIcon(copyObjectIcon);

	cutObjectMenuAction = objectListMenu->addAction("Cut object");
	QIcon cutObjectIcon;
	cutObjectIcon.addFile(QString::fromUtf8(":/images/cutObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	cutObjectMenuAction->setIcon(cutObjectIcon);

	pasteObjectMenuAction = objectListMenu->addAction("Paste object");
	QIcon pasteObjectIcon;
	pasteObjectIcon.addFile(QString::fromUtf8(":/images/pasteObject.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	pasteObjectMenuAction->setIcon(pasteObjectIcon);
}

void GameEditorWindow::scheduleExitEngine() {
	exitEngine = true;
}

void GameEditorWindow::updateEngine() {
	fpsCounter.frameBegin();
	if (Engine::ref().isRunning()) {
		Engine::ref().update();
		gameWidget->repaint();
#if REV_ENGINE_MAX_CONTEXTS >= 2
		if (secondGameWidget->isVisible()) {
			secondGameWidget->repaint();
		}
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
		if (thirdGameWidget->isVisible()) {
			thirdGameWidget->repaint();
		}
#endif
	}

	if (fpsCounter.frameEnd()) {
		QString title;
		float fps = fpsCounter.getFramesPerSec();
		if (isSceneSaved()) {
			setWindowTitle(
					title.sprintf(WINDOW_TITLE " | %s | %.2f", sceneName.c_str(), fps));
		} else {
			setWindowTitle(
					title.sprintf(WINDOW_TITLE " | %s* | %.2f", sceneName.c_str(), fps));
		}
		showPolygonInfo();
	}

	refreshTime += Engine::ref().getCurrentFrameTime();
	if (refreshTime >= ENGINE_GUI_REFRESH_TIME) {
		refreshTimerAction();
		refreshTime = 0.0f;
	}

	if (exitEngine) {
		qApp->quit();
	}
}

void GameEditorWindow::refreshTimerAction() {
	cameraWidget->refresh();
	propertiesBinder->refreshAll();
	physicsWidget->refresh();
}

void GameEditorWindow::displayTableMenu(const QPoint& pos) {
	QAction* a = objectListMenu->exec(widget.objectListTable->viewport()->mapToGlobal(pos));
	if (a == addObjectMenuAction) {
		addObjectAction();
	} else if (a == removeObjectMenuAction) {
		removeObjectTableAction();
	} else if (a == editObjectMenuAction) {
		editObjectTableAction();
	} else if (a == copyObjectMenuAction) {
		copyObjectTableAction();
	} else if (a == cutObjectMenuAction) {
		cutObjectTableAction();
	} else if (a == pasteObjectMenuAction) {
		pasteObjectTableAction();
	}
}

void GameEditorWindow::showPolygonInfo() {
	int polyCount = rev::Renderer3D::ref().getTotalPolygons();
	int renderedPolyCount = rev::Renderer3D::ref().getTotalRenderedPolygons(0);
	int objectCount = rev::Renderer3D::ref().getTotalObjects();
	int renderedObjectCount = rev::Renderer3D::ref().getTotalRenderedObjects(0);

	QString info =
			QString(
					"Total objects: %1 Total polygons: %2 | [Context #0] Objects %3 Polygons: %4")
					.arg(objectCount).arg(polyCount)
					.arg(renderedObjectCount).arg(renderedPolyCount);

#if REV_ENGINE_MAX_CONTEXTS >= 2
	if (secondGameWidget->isVisible()) {
		int renderedPolyCount1 =
				rev::Renderer3D::ref().getTotalRenderedPolygons(1);
		int renderedObjectCount1 =
				rev::Renderer3D::ref().getTotalRenderedObjects(
						1);

		info += QString(" | [Context #1] Objects %1 Polygons: %2")
				.arg(renderedObjectCount1).arg(renderedPolyCount1);

	}
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	if (thirdGameWidget->isVisible()) {

		int renderedPolyCount2 =
				rev::Renderer3D::ref().getTotalRenderedPolygons(2);
		int renderedObjectCount2 =
				rev::Renderer3D::ref().getTotalRenderedObjects(
						2);

		info += QString(" | [Context #2] Objects %1 Polygons: %2")
				.arg(renderedObjectCount2).arg(renderedPolyCount2);
	}
#endif

	statsLabel->setText(info);
}

void GameEditorWindow::aboutAction() {
	QMessageBox::information(this, "About...", "Author: Kamil Kolaczynski");
}

void GameEditorWindow::refreshEngineControls() {
	optionsWidget->refreshEngineControls();
	scriptingWidget->refreshEngineControls();
}

void GameEditorWindow::openSceneAction() {
	REV_TRACE_FUNCTION;

	if (!saveUnsavedScene()) {
		return;
	}

	QSettings mySettings;
	QString filename = QFileDialog::getOpenFileName(
			this,
			tr("Open Scene"),
			mySettings.value(DEFAULT_DIR_KEY).toString(),
			tr("Scene files (*.scene.xml)"));
	if (filename.isNull()) {
		return;
	}

	widgetsController->clearObjectList();
	clearPropertiesPanel();

	QDir currentDir;
	mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

	XMLConfigManager& confManager = XMLConfigManager::ref();
	std::string errOut;

	if (!confManager.loadSceneConfiguration(filename.toUtf8().data(), errOut)) {
		QString err(errOut.c_str());
		QMessageBox::critical(this, "ERROR", err);
		return;
	}
	clearClipboard();
	refreshEngineControls();
	setSceneSaved(true);
	setSceneNameAndPath(filename);
}

void GameEditorWindow::saveSceneAsAction() {
	REV_TRACE_FUNCTION;
	saveSceneAs();
}

bool GameEditorWindow::saveSceneAs() {
	QSettings mySettings;
	QString filename = QFileDialog::getSaveFileName(
			this,
			tr("Save Scene"),
			mySettings.value(DEFAULT_DIR_KEY).toString(),
			tr("Scene files (*.scene.xml)"));
	if (filename.isNull()) {
		return false;
	}

	QDir currentDir;
	mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

	XMLConfigManager& confManager = XMLConfigManager::ref();
	std::string errOut;

	if (!confManager.saveSceneConfiguration(filename.toUtf8().data(), errOut)) {
		QString err(errOut.c_str());
		QMessageBox::critical(this, "ERROR", err);
		return false;
	}

	setSceneNameAndPath(filename);
	setSceneSaved(true);
	return true;
}

void GameEditorWindow::saveSceneAction() {
	REV_TRACE_FUNCTION;
	saveScene();
}

bool GameEditorWindow::saveScene() {
	if (scenePath == "") {
		return saveSceneAs();
	}

	XMLConfigManager& confManager = XMLConfigManager::ref();
	std::string errOut;

	if (!confManager.saveSceneConfiguration(scenePath, errOut)) {
		QString err(errOut.c_str());
		QMessageBox::critical(this, "ERROR", err);
		return false;
	}

	setSceneSaved(true);
	return true;
}

void GameEditorWindow::closeEvent(QCloseEvent* event) {
	if (isNewPanelVisible()) {
		QMessageBox msgBox;
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.setWindowTitle("Unsaved editing");
		msgBox.setText("You are in the middle of editing an object. Do you really want to exit?");
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::No);
		int ret = msgBox.exec();

		if (ret == QMessageBox::No) {
			event->ignore();
		} else {
			newObjectWidget->cancelAction();
			event->ignore();
			scheduleExitEngine();
		}
	} else if (!saveUnsavedScene()) {
		event->ignore();
	}
}

bool GameEditorWindow::saveUnsavedScene() {
	if (isSceneSaved()) {
		return true;
	}

	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setWindowTitle("Unsaved scene");
	msgBox.setText(
			QString("Scene '%1' is not saved. Do you want to save it now?").arg(sceneName.c_str()));
	msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Yes);
	int ret = msgBox.exec();

	if (ret == QMessageBox::Yes) {
		if (isNewPanelVisible()) {
			newObjectWidget->okAction();
		}
		bool succ = saveScene();
		if (succ) {
			QMessageBox::information(this, "Save Successful",
					QString("Scene '%1' saved successfully.").arg(scenePath.c_str()));
		}
		return succ;
	} else if (ret == QMessageBox::No) {
		if (isNewPanelVisible()) {
			newObjectWidget->cancelAction();
		}
		return true;
	}

	return false;
}

void GameEditorWindow::setSceneNameAndPath(const QString& filename) {
	int index = filename.lastIndexOf('/');
	if (index < 0) {
		index = filename.lastIndexOf('\\');
	}
	if (index >= 0) {
		index++;
		sceneName = filename.mid(index, filename.length() - index).toUtf8().data();
	} else {
		sceneName = filename.toUtf8().data();
	}
	scenePath = filename.toUtf8().data();
}

void GameEditorWindow::loadDefaultScene() {
	REV_TRACE_FUNCTION;
	std::string file = ResourcePath::get(DEFAULT_SCENE);
	QString filename(file.c_str());

	XMLConfigManager& confManager = XMLConfigManager::ref();
	std::string errOut;

	if (!confManager.loadSceneConfiguration(filename.toUtf8().data(), errOut)) {
		QString err(errOut.c_str());
		QMessageBox::critical(this, "ERROR", err);
		return;
	}
	setSceneSaved(true);
	setSceneNameAndPath(filename);
	clearClipboard();
}

void GameEditorWindow::initEngineRelatedStuff() {
	rev::EventManager::ref().addComponent(widgetsController);
	newObjectWidget->init();
	scriptingWidget->init();
	surfacesWidget->init();
	physicsWidget->init();
	initTools();
	loadDefaultScene();
}

void GameEditorWindow::objectListToggled(bool toggled) {
	widget.objectListTable->setVisible(toggled);
}

void GameEditorWindow::showSideBarToggled(bool toggled) {
	sidePanelVisEventActive = false;
	widget.sidePanel->setVisible(toggled);
	sidePanelVisEventActive = true;
}

void GameEditorWindow::showBottomBarToggled(bool toggled) {
	bottomPanelVisEventActive = false;
	widget.bottomPanel->setVisible(toggled);
	bottomPanelVisEventActive = true;
}

void GameEditorWindow::showToolbarToggled(bool toggled) {
	widget.toolBar->setVisible(toggled);
}

void GameEditorWindow::setPropertiesPanelVisible(bool visible) {
	widget.propertiesPanel->setVisible(visible);
}

void GameEditorWindow::deselectButtonPressed() {
	rev::Renderer3D::ref().getPicker().reset();
}

void GameEditorWindow::editComponentsButtonPressed() {
	editPickedObjectMenuAction();
}

void GameEditorWindow::removeObjectButtonPressed() {
	removePickedObjectMenuAction();
}

void GameEditorWindow::silentRemoveCameraLock() {
	cameraLockActionEnabled = false;
	widget.cameraLockPB->setChecked(false);
	cameraLockActionEnabled = true;
}

void GameEditorWindow::cameraLockButtonToggled(bool toggled) {
	if (!cameraLockActionEnabled) {
		return;
	}

	if (toggled) {
		GameObject* go = getSelectedGameObject();
		revAssert(go);
		CameraManager::ref().useSphericalCamera(0, go->getPosition());
	} else {
		CameraManager::ref().useFreeCamera(0);
	}
}

void GameEditorWindow::cameraUnlock() {
	cameraLockButtonToggled(false);
}

void GameEditorWindow::exclusiveModeButtonToggled(bool toggled) {
	REV_TRACE_FUNCTION;
	if (!toggled) {
		rev::Renderer3D::ref().turnOffExclusiveMode();
	} else {
		GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
		revAssert(go);
		rev::Renderer3D::ref().setExclusiveGameObject(go);
	}
}

void GameEditorWindow::sidePanelVisibilityChanged(bool visible) {
	if (!sidePanelVisEventActive) {
		return;
	}

	widget.showSidePanelAction->setChecked(visible);
}

void GameEditorWindow::bottomPanelVisibilityChanged(bool visible) {
	if (!bottomPanelVisEventActive) {
		return;
	}

	widget.showBottomPanelAction->setChecked(visible);
}

void GameEditorWindow::setSecondGameWidgetVisible(bool visible) {
#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget->setVisible(visible);
#endif
}

void GameEditorWindow::singleViewportActionToggled(bool toggled) {
	REV_DEBUG_MSG(R(toggled));
	if (!toggled) {
		return;
	}
#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget->setVisible(false);
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	thirdGameWidget->setVisible(false);
#endif
}

void GameEditorWindow::doubleVertViewportActionToggled(bool toggled) {
	REV_DEBUG_MSG(R(toggled));
	if (!toggled) {
		return;
	}

#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget->setVisible(true);
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	thirdGameWidget->setVisible(false);
#endif

	mainSplitter->setOrientation(Qt::Horizontal);
}

void GameEditorWindow::doubleHorViewportActionToggled(bool toggled) {
	REV_DEBUG_MSG(R(toggled));
	if (!toggled) {
		return;
	}

#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget->setVisible(true);
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	thirdGameWidget->setVisible(false);
#endif

	mainSplitter->setOrientation(Qt::Vertical);
}

void GameEditorWindow::tripleViewportActionToggled(bool toggled) {
	REV_DEBUG_MSG(R(toggled));
	if (!toggled) {
		return;
	}

#if REV_ENGINE_MAX_CONTEXTS >= 2
	secondGameWidget->setVisible(true);
#endif
#if REV_ENGINE_MAX_CONTEXTS >= 3
	thirdGameWidget->setVisible(true);
#endif

	mainSplitter->setOrientation(Qt::Horizontal);
}

void GameEditorWindow::moveModeActionToggled(bool toggled) {
	if (!canChangeTool) {
		return;
	}
	REV_TRACE_MSG("toggled = " << (toggled ? "true" : "false"));

	if (toggled) {
		toolRegistry.setCurrentTool(moveTool);
	}
}

void GameEditorWindow::rotateModeActionToggled(bool toggled) {
	if (!canChangeTool) {
		return;
	}
	REV_TRACE_MSG("toggled = " << (toggled ? "true" : "false"));

	if (toggled) {
		toolRegistry.setCurrentTool(rotateTool);
	}
}

void GameEditorWindow::resizeModeActionToggled(bool toggled) {
	if (!canChangeTool) {
		return;
	}
	REV_TRACE_MSG("toggled = " << (toggled ? "true" : "false"));

	if (toggled) {
		toolRegistry.setCurrentTool(resizeTool);
	}
}

void GameEditorWindow::lightModeActionToggled(bool toggled) {
	if (!canChangeTool) {
		return;
	}
	REV_TRACE_MSG("toggled = " << (toggled ? "true" : "false"));

	if (toggled) {
		toolRegistry.setCurrentTool(lightTool);
	}
}

void GameEditorWindow::setObjectPicked(bool picked) {
	if (!picked) {
		widget.objectListTable->reset();
		setPropertiesPanelVisible(false);
		statusBar()->showMessage("");
		widget.exclusiveModePB->setChecked(false);
		widget.copyPickedObjectAction->setEnabled(false);
		widget.cutPickedObjectAction->setEnabled(false);
		if (clipboard->isEmpty()) {
			widget.pasteObjectAction->setEnabled(false);
		} else {
			widget.pasteObjectAction->setEnabled(true);
		}
	} else {
		GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
		revAssert(go);
		statusBar()->showMessage(QString("Picked object: %1 (%2)")
				.arg(go->getName().c_str()).arg(go->getId()));

		widget.copyPickedObjectAction->setEnabled(true);
		widget.cutPickedObjectAction->setEnabled(true);
		if (clipboard->isEmpty()) {
			widget.pasteObjectAction->setEnabled(false);
		} else {
			widget.pasteObjectAction->setEnabled(true);
		}
	}
	widget.removePickedObjectAction->setEnabled(picked);
	widget.editPickedObjectAction->setEnabled(picked);
}

void GameEditorWindow::removeObject(GameObject* go) {
	REV_TRACE_FUNCTION;

	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setWindowTitle("Warning");
	msgBox.setText(QString("Do you really want to remove '%1 (%2)' object?")
			.arg(go->getName().c_str()).arg(go->getId()));
	msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgBox.setDefaultButton(QMessageBox::Yes);
	int ret = msgBox.exec();

	if (ret == QMessageBox::Yes) {
		//propertiesBinder->unbindObject(go);
		propertiesBinder->clearAll();
		go->destroy();
		setSceneSaved(false);
	}
}

void GameEditorWindow::editObject(GameObject* go) {
	REV_TRACE_FUNCTION;
	newObjectWidget->activateEditObjectMode(go);
	setNewPanelVisible(true, true);
}

void GameEditorWindow::addObjectAction() {
	REV_TRACE_FUNCTION;
	newObjectWidget->activateNewObjectMode();
	setNewPanelVisible(true, false);
}

void GameEditorWindow::removePickedObjectMenuAction() {
	REV_TRACE_FUNCTION;

	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (!go) {
		REV_DEBUG_MSG("There's no game object picked!");
		return;
	}

	removeObject(go);
}

void GameEditorWindow::editPickedObjectMenuAction() {
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (!go) {
		REV_WARN_MSG("There's no game object picked!");
		return;
	}
	editObject(go);
}

int GameEditorWindow::getSelectedRowIndex() {
	QModelIndexList selectedList = widget.objectListTable->selectionModel()->selectedRows();
	if (selectedList.count() == 0) {
		return -1;
	}
	return selectedList.at(0).row();
}

GameObject* GameEditorWindow::getSelectedGameObject() {
	int index = getSelectedRowIndex();
	REV_TRACE_MSG("Selected row index = " << index);

	if (index < 0) {
		return nullptr;
	}
	GameObjectTableModel* model =
			static_cast<GameObjectTableModel*>(widget.objectListTable->model());
	GameObject* go = model->getGameObject(index);
	if (!go) {
		REV_ERROR_MSG("There is no game object with table index: " << index);
		return nullptr;
	}
	return go;
}

void GameEditorWindow::removeObjectTableAction() {
	GameObject* go = getSelectedGameObject();
	if (go) {
		removeObject(go);
	}
}

void GameEditorWindow::editObjectTableAction() {
	GameObject* go = getSelectedGameObject();
	if (go) {
		editObject(go);
	}
}

void GameEditorWindow::newSceneAction() {
	REV_TRACE_FUNCTION;

	if (!saveUnsavedScene()) {
		return;
	}

	clearPropertiesPanel();
	widgetsController->clearObjectList();

	setSceneSaved(true);
	scenePath = "";
	sceneName = UNTITLED_SCENE_NAME;

	EventManager::ref().fireEngineEvent("GameEditorWindow::newSceneAction",
			[] () -> bool {
				Engine::ref().removeAllObjectsAndEvents();
				return true;
			});
	clearClipboard();
}

void GameEditorWindow::clearPropertiesPanel() {
	propertiesBinder->clearAll();
	cameraUnlock();
}

void GameEditorWindow::bindPropertiesPanel(GameObject* go) {
	propertiesBinder->clearAll();
	propertiesBinder->bindObject(go);
}

void GameEditorWindow::scrollObjectTableToSelectedItem() {
	widget.objectListTable->scrollTo(
			widget.objectListTable->model()->index(getSelectedRowIndex(), 0));
}

void GameEditorWindow::copyObjectTableAction() {
	REV_TRACE_FUNCTION;
}

void GameEditorWindow::cutObjectTableAction() {
	REV_TRACE_FUNCTION;
}

void GameEditorWindow::pasteObjectTableAction() {
	REV_TRACE_FUNCTION;
}

void GameEditorWindow::setSceneSaved(bool sceneSaved) {
	// REV_WARN_MSG("sceneSaved = " << (sceneSaved ? "true" : "false"));
	this->sceneSaved = sceneSaved;
	widget.saveSceneAction->setEnabled(!sceneSaved);
}

void GameEditorWindow::setToolsEnabled(bool enabled) {
	widget.moveModeAction->setEnabled(enabled);
	widget.rotateModeAction->setEnabled(enabled);
	widget.resizeModeAction->setEnabled(enabled);
	widget.lightModeAction->setEnabled(enabled);
}

void GameEditorWindow::deselectAllTools() {
	canChangeTool = false;
	widget.moveModeAction->setChecked(false);
	widget.rotateModeAction->setChecked(false);
	widget.resizeModeAction->setChecked(false);
	widget.lightModeAction->setChecked(false);
	canChangeTool = true;
}

void GameEditorWindow::toolChanged(ITool* tool) {
	deselectAllTools();
	canChangeTool = false;
	if (!tool) {
		REV_DEBUG_MSG("Tool changed to: nullptr");
		return;
	}
	REV_DEBUG_MSG("Tool changed to: " << tool->getToolName());

	if (tool == moveTool) {
		widget.moveModeAction->setChecked(true);
		setToolsEnabled(true);
	} else if (tool == rotateTool) {
		widget.rotateModeAction->setChecked(true);
		setToolsEnabled(true);
	} else if (tool == resizeTool) {
		widget.resizeModeAction->setChecked(true);
		setToolsEnabled(true);
	} else if (tool == lightTool) {
		widget.lightModeAction->setChecked(true);
		setToolsEnabled(true);
	} else {
		setToolsEnabled(false);
	}

	REV_DEBUG_MSG("Tool changed to: " << tool->getToolName());
	canChangeTool = true;
}

void GameEditorWindow::clearClipboard() {
	clipboard->clear();
	widget.pasteObjectAction->setEnabled(false);
}

void GameEditorWindow::copyGameObject() {
	REV_TRACE_FUNCTION;
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (!go) {
		return;
	}
	clipboard->copy(go);
	widget.pasteObjectAction->setEnabled(true);
}

void GameEditorWindow::cutGameObject() {
	REV_TRACE_FUNCTION;
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (!go) {
		return;
	}
	clipboard->cut(go);
	widget.pasteObjectAction->setEnabled(true);
	rev::Renderer3D::ref().getPicker().reset();
}

void GameEditorWindow::pasteGameObject() {
	REV_TRACE_FUNCTION;

	GameObject* go = clipboard->paste();
	if (!go) {
		return;
	}
	rev::Renderer3D::ref().getPicker().pickGameObject(go);
	ICamera* camera = CameraManager::ref().getCamera(0);
	go->setPosition(camera->getPosition() + REV_NEW_OBJECT_DISTANCE * camera->getDirection());
}

void GameEditorWindow::setCursorOpenHand() {
	setCursor(Qt::OpenHandCursor);
}

void GameEditorWindow::setCursorClosedHand() {
	setCursor(Qt::ClosedHandCursor);
}

void GameEditorWindow::setCursorArrow() {
	setCursor(Qt::ArrowCursor);
}
