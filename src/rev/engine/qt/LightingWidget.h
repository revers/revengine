/*
 * LightingWidget.h
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#ifndef LIGHTINGWIDGET_H_
#define LIGHTINGWIDGET_H_

#include <QWidget>
#include "ui_LightingWidget.h"

namespace rev {

class GameEditorWindow;

class LightingWidget: public QWidget {
Q_OBJECT

	GameEditorWindow* mainWindow;
	Ui::LightingWidget widget;

public:
	LightingWidget(GameEditorWindow* mainWindow);
	virtual ~LightingWidget();

private slots:
	void biasValueChanged(int value);
	void radiusValueChanged(int value);
	void constAttenValueChanged(int value);
	void linearAttenValueChanged(int value);

private:
	void setupActions();
	int getBiasValue() const;
	void setBiasValue(int biasValue);

	int getConstAtten() const;
	void setConstAtten(int constAtten);

	int getLinearAtten() const;
	void setLinearAtten(int linearAtten);

	int getRadius() const;
	void setRadius(int radius);

};

} /* namespace rev */
#endif /* LIGHTINGWIDGET_H_ */
