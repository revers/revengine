/*
 * CameraWidget.cpp
 *
 *  Created on: 07-04-2013
 *      Author: Revers
 */

#include <QHBoxLayout>
#include <QButtonGroup>
#include <QtTreePropertyBrowser>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/camera/RevICamera.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/camera/RevCameraManager.h>
#include "QtPropertiesBinder.h"
#include "GameEditorWindow.h"
#include "CameraWidget.h"

using namespace rev;

CameraWidget::CameraWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);

	freeCameraTree = new QtTreePropertyBrowser(this);
	uiWidget.freeCameraWidget->layout()->addWidget(freeCameraTree);
	freeCameraBinder = new QtPropertiesBinder(freeCameraTree, this);

	sphericalCameraTree = new QtTreePropertyBrowser(this);
	uiWidget.sphericalCameraWidget->layout()->addWidget(sphericalCameraTree);
	sphericalCameraBinder = new QtPropertiesBinder(sphericalCameraTree, this);

	for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
		uiWidget.contextCB->addItem(QString("Context #%1").arg(i));
	}

	QHBoxLayout* boxLayout = (QHBoxLayout*) layout();
	boxLayout->setStretchFactor(uiWidget.leftLayout, 1);
	boxLayout->setStretchFactor(uiWidget.freeCameraLayout, 1000);
	boxLayout->setStretchFactor(uiWidget.sphericalCameraLayout, 1000);

	QButtonGroup* group = new QButtonGroup(this);
	group->addButton(uiWidget.freeCameraRB);
	group->addButton(uiWidget.sphericalCameraRB);

	contextCBChanged(0);
	setWidgetsEnabled();
	setupActions();
}

CameraWidget::~CameraWidget() {
}

void CameraWidget::setupActions() {
	connect(uiWidget.contextCB, SIGNAL(currentIndexChanged(int)),
			this, SLOT(contextCBChanged(int)));

	connect(uiWidget.freeCameraRB, SIGNAL(toggled(bool)),
			this, SLOT(freeCameraRBChecked(bool)));

	connect(uiWidget.sphericalCameraRB, SIGNAL(toggled(bool)),
			this, SLOT(sphericalCameraRBChecked(bool)));

	connect(uiWidget.sameAsContext0PB, SIGNAL(clicked()), this,
	SLOT(sameAsContext0Pressed()));

	connect(uiWidget.sameAsContext1PB, SIGNAL(clicked()), this,
	SLOT(sameAsContext1Pressed()));

	connect(uiWidget.sameAsContext2PB, SIGNAL(clicked()), this,
	SLOT(sameAsContext2Pressed()));
}

void CameraWidget::refresh() {
	freeCameraBinder->refreshAll();
	sphericalCameraBinder->refreshAll();
	updateCameraButtons();
}

void CameraWidget::updateCameraButtons() {
	if (CameraManager::ref().isFreeCameraUsed(getCameraContextIndex())
			&& !isFreeCameraUsed()) {
		return uiWidget.freeCameraRB->setChecked(true);
	} else if (CameraManager::ref().isSphericalCameraUsed(getCameraContextIndex())
			&& !isSphericalCameraUsed()) {
		return uiWidget.sphericalCameraRB->setChecked(true);
	}
}

void CameraWidget::contextCBChanged(int index) {
	freeCameraBinder->clearAll();
	sphericalCameraBinder->clearAll();

	FreeCamera* freeCamera = CameraManager::ref().getFreeCamera(index);
	SphericalCamera* sphericalCamera = CameraManager::ref().getSphericalCamera(index);

	freeCameraBinder->bindObject(freeCamera);
	sphericalCameraBinder->bindObject(sphericalCamera);

	switch (index) {
	case 0: {
		uiWidget.sameAsContext0PB->setEnabled(false);
		uiWidget.sameAsContext1PB->setEnabled(true);
		uiWidget.sameAsContext2PB->setEnabled(true);
		break;
	}
	case 1: {
		uiWidget.sameAsContext0PB->setEnabled(true);
		uiWidget.sameAsContext1PB->setEnabled(false);
		uiWidget.sameAsContext2PB->setEnabled(true);
		break;
	}
	case 2: {
		uiWidget.sameAsContext0PB->setEnabled(true);
		uiWidget.sameAsContext1PB->setEnabled(true);
		uiWidget.sameAsContext2PB->setEnabled(false);
		break;
	}

	}

	updateCameraButtons();
}

bool CameraWidget::isFreeCameraUsed() {
	return uiWidget.freeCameraRB->isChecked();
}

bool CameraWidget::isSphericalCameraUsed() {
	return uiWidget.sphericalCameraRB->isChecked();
}

int CameraWidget::getCameraContextIndex() {
	return uiWidget.contextCB->currentIndex();
}

void CameraWidget::changeCamera() {
	EventManager::ref().fireEngineEvent("CameraWidget::changeCamera()", [this] () -> bool {
		if (isFreeCameraUsed()) {
			CameraManager::ref().useFreeCamera(getCameraContextIndex());
		} else if (isSphericalCameraUsed()) {
			CameraManager::ref().useSphericalCamera(getCameraContextIndex());
		}
		return true;
	});

	if (getCameraContextIndex() == 0 && !isSphericalCameraUsed()) {
		mainWindow->silentRemoveCameraLock();
	}
}

void CameraWidget::freeCameraRBChecked(bool checked) {
	setWidgetsEnabled();
	changeCamera();
}

void CameraWidget::sphericalCameraRBChecked(bool checked) {
	setWidgetsEnabled();
	changeCamera();
}

void CameraWidget::setWidgetsEnabled() {
	if (uiWidget.freeCameraRB->isChecked()) {
		freeCameraTree->setEnabled(true);
		sphericalCameraTree->setEnabled(false);
	} else {
		freeCameraTree->setEnabled(false);
		sphericalCameraTree->setEnabled(true);
	}
}

void CameraWidget::setSameCamera(int srcIndex, int dstIndex) {
	CameraManager& mgr = CameraManager::ref();
	if (mgr.isFreeCameraUsed(srcIndex)) {
		mgr.useFreeCamera(dstIndex);

		FreeCamera& src = *mgr.getFreeCamera(srcIndex);
		FreeCamera& dst = *mgr.getFreeCamera(dstIndex);
		dst.set(src.getPosition(), src.getDirection());

	} else if (mgr.isSphericalCameraUsed(srcIndex)) {
		mgr.useSphericalCamera(dstIndex);

		SphericalCamera& src = *mgr.getSphericalCamera(srcIndex);
		SphericalCamera& dst = *mgr.getSphericalCamera(dstIndex);
		dst.setPositionAndTarget(src.getPosition(), src.getTarget());
	}
}

void CameraWidget::sameAsContext0Pressed() {
	int cameraIndex = uiWidget.contextCB->currentIndex();
	setSameCamera(0, cameraIndex);
}

void CameraWidget::sameAsContext1Pressed() {
	int cameraIndex = uiWidget.contextCB->currentIndex();
	setSameCamera(1, cameraIndex);
}

void CameraWidget::sameAsContext2Pressed() {
	int cameraIndex = uiWidget.contextCB->currentIndex();
	setSameCamera(2, cameraIndex);
}
