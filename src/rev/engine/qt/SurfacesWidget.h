/*
 * SurfacesWidget.h
 *
 *  Created on: 10-04-2013
 *      Author: Revers
 */

#ifndef EDITWIDGET_H_
#define EDITWIDGET_H_

#include <QWidget>
#include <QTimer>
#include <glm/glm.hpp>
#include <rev/engine/parametric/RevISurfacePatch.h>
#include <rev/engine/parametric/RevControlPointPicker.h>
#include <rev/engine/tools/RevITool.h>
#include "ui_SurfacesWidget.h"

namespace rev {

	class GameEditorWindow;
	class GameObject;
	class Simple3DComponent;
	class VBOPointSpheresVC;
	class ControlPointsEffect;

	class SurfacesWidget: public QWidget, public ITool {
	Q_OBJECT

		GameEditorWindow* mainWindow;
		Ui::SurfacesWidget uiWidget;
		ISurfacePatch* surfacePatch = nullptr;
		Simple3DComponent* component = nullptr;
		VBOPointSpheresVC* drawable = nullptr;
		ControlPointsEffect* effect = nullptr;
		GameObject* pickedGameObject = nullptr;
		GameObject* controlPointsGO = nullptr;
		ITool* lastTool = nullptr;

		ControlPointPicker picker;
		ControlPointPicker::UV uv;

		glm::vec3* origPoints = nullptr;
		bool canChangeSliderValue = true;
		bool editingMode = false;
		bool dragPhase = false;
		int lastMouseX = 0;
		int lastMouseY = 0;

	public:
		SurfacesWidget(GameEditorWindow* mainWindow);
		virtual ~SurfacesWidget();

		void objectPicked(GameObject* go);
		void pickingReseted();
		void init();
		void refreshPickedObject();
		void mousePressed(int x, int y, int contextIndex) override;
		void mouseMoved(int x, int y, int contextIndex);
		void mouseDrag(int x, int y, int contextIndex) override;
		void mouseReleased(int x, int y, int contextIndex) override;
		const char* getToolName() override {
			return "SurfacePatchTool";
		}

	public slots:
		void refreshPoints();

	private slots:
		void editButtonAction();
		void applyAction();
		void cancelAction();
		void sensitivityValueChanged(int value);
		void distanceValueChanged(int value);

	private:
		bool isEditingMode() {
			return editingMode;
		}
		void setupActions();
		void resetValues();
		void enableWidgets(bool enable);
		void cancel();
	};

} /* namespace rev */
#endif /* EDITWIDGET_H_ */
