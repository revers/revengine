/*
 * DVec2PropertyManager.cpp
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#include <rev/engine/qt/QtConfig.h>
#include "DVec2PropertyManager.h"

using namespace rev;

void DVec2PropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    changeListenerActive = false;
    if (QtProperty* prop = xToProperty.value(property, 0)) {
        glm::dvec2 p = values[prop].val;
        p.x = value;
        setValue(prop, p);
    } else if (QtProperty* prop = yToProperty.value(property, 0)) {
        glm::dvec2 p = values[prop].val;
        p.y = value;
        setValue(prop, p);
    }
    changeListenerActive = true;
}

void DVec2PropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = xToProperty.value(property, 0)) {
        propertyToX[pointProp] = 0;
        xToProperty.remove(property);
    } else if (QtProperty* pointProp = yToProperty.value(property, 0)) {
        propertyToY[pointProp] = 0;
        yToProperty.remove(property);
    }
}

DVec2PropertyManager::DVec2PropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

DVec2PropertyManager::~DVec2PropertyManager() {
    clear();
}

QtDoublePropertyManager* DVec2PropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::dvec2 DVec2PropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::dvec2(0);
    }

    return it.value().val;
}

int DVec2PropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString DVec2PropertyManager::valueText(const QtProperty* property) const {
    const DVec2PropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::dvec2 v = it.value().val;
    const int dec = it.value().decimals;
    return QString(tr("(%1, %2)")
            .arg(QString::number(v.x, 'f', dec))
            .arg(QString::number(v.y, 'f', dec)));
}

void DVec2PropertyManager::setValue(QtProperty* property,
        const glm::dvec2& val) {
    const DVec2PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToX[property],
            val.x);
    doublePropertyManager->setValue(propertyToY[property],
            val.y);

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void DVec2PropertyManager::setDecimals(QtProperty* property, int prec) {

    const DVec2PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    DVec2PropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToX[property],
            prec);
    doublePropertyManager->setDecimals(propertyToY[property],
            prec);


    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void DVec2PropertyManager::initializeProperty(QtProperty* property) {
    values[property] = DVec2PropertyManager::Data();

    QtProperty* xProp = doublePropertyManager->addProperty();
    xProp->setPropertyName(tr("X"));
    doublePropertyManager->setDecimals(xProp, decimals(property));
    doublePropertyManager->setValue(xProp, 0);
    doublePropertyManager->setSingleStep(xProp, SINGLE_STEP);
    propertyToX[property] = xProp;
    xToProperty[xProp] = property;
    property->addSubProperty(xProp);

    QtProperty* yProp = doublePropertyManager->addProperty();
    yProp->setPropertyName(tr("Y"));
    doublePropertyManager->setDecimals(yProp, decimals(property));
    doublePropertyManager->setValue(yProp, 0);
    doublePropertyManager->setSingleStep(yProp, SINGLE_STEP);
    propertyToY[property] = yProp;
    yToProperty[yProp] = property;
    property->addSubProperty(yProp);
}

void DVec2PropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* xProp = propertyToX[property];
    if (xProp) {
        xToProperty.remove(xProp);
        delete xProp;
    }
    propertyToX.remove(property);

    QtProperty* yProp = propertyToY[property];
    if (yProp) {
        yToProperty.remove(yProp);
        delete yProp;
    }
    propertyToY.remove(property);

    values.remove(property);
}
