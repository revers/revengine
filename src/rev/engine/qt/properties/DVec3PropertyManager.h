/*
 * DVec3PropertyManager.h
 *
 *  Created on: 13-01-2013
 *      Author: Revers
 */

#ifndef DVEC3PROPERTYMANAGER_H_
#define DVEC3PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class DVec3PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2) {
            }

            glm::dvec3 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToX;
        QMap<const QtProperty*, QtProperty*> propertyToY;
        QMap<const QtProperty*, QtProperty*> propertyToZ;

        QMap<const QtProperty*, QtProperty*> xToProperty;
        QMap<const QtProperty*, QtProperty*> yToProperty;
        QMap<const QtProperty*, QtProperty*> zToProperty;

    public:
        DVec3PropertyManager(QObject* parent = 0);
        ~DVec3PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::dvec3 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::dvec3& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::dvec3& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev

#endif /* DVEC3PROPERTYMANAGER_H_ */
