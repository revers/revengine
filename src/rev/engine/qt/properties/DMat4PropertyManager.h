/*
 * DMat4PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef DMAT4PROPERTYMANAGER_H_
#define DMAT4PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class DMat4PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2), val(0.0) {
            }

            glm::dmat4 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToM11;
        QMap<const QtProperty*, QtProperty*> propertyToM12;
		QMap<const QtProperty*, QtProperty*> propertyToM13;
		QMap<const QtProperty*, QtProperty*> propertyToM14;
		
        QMap<const QtProperty*, QtProperty*> propertyToM21;
        QMap<const QtProperty*, QtProperty*> propertyToM22;
		QMap<const QtProperty*, QtProperty*> propertyToM23;
		QMap<const QtProperty*, QtProperty*> propertyToM24;
		
		QMap<const QtProperty*, QtProperty*> propertyToM31;
        QMap<const QtProperty*, QtProperty*> propertyToM32;
		QMap<const QtProperty*, QtProperty*> propertyToM33;
		QMap<const QtProperty*, QtProperty*> propertyToM34;
		
		QMap<const QtProperty*, QtProperty*> propertyToM41;
        QMap<const QtProperty*, QtProperty*> propertyToM42;
		QMap<const QtProperty*, QtProperty*> propertyToM43;
		QMap<const QtProperty*, QtProperty*> propertyToM44;

        QMap<const QtProperty*, QtProperty*> m11ToProperty;
        QMap<const QtProperty*, QtProperty*> m12ToProperty;
		QMap<const QtProperty*, QtProperty*> m13ToProperty;
		QMap<const QtProperty*, QtProperty*> m14ToProperty;
		
        QMap<const QtProperty*, QtProperty*> m21ToProperty;
        QMap<const QtProperty*, QtProperty*> m22ToProperty;
		QMap<const QtProperty*, QtProperty*> m23ToProperty;
		QMap<const QtProperty*, QtProperty*> m24ToProperty;
		
		QMap<const QtProperty*, QtProperty*> m31ToProperty;
        QMap<const QtProperty*, QtProperty*> m32ToProperty;
		QMap<const QtProperty*, QtProperty*> m33ToProperty;
		QMap<const QtProperty*, QtProperty*> m34ToProperty;
		
		QMap<const QtProperty*, QtProperty*> m41ToProperty;
        QMap<const QtProperty*, QtProperty*> m42ToProperty;
		QMap<const QtProperty*, QtProperty*> m43ToProperty;
		QMap<const QtProperty*, QtProperty*> m44ToProperty;

    public:
        DMat4PropertyManager(QObject* parent = 0);
        ~DMat4PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::dmat4 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::dmat4& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::dmat4& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* DMAT4PROPERTYMANAGER_H_ */
