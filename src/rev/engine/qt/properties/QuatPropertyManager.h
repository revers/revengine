/*
 * QuatPropertyManager.h
 *
 *  Created on: 21-01-2013
 *      Author: Revers
 */

#ifndef QUATPROPERTYMANAGER_H_
#define QUATPROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace rev {
    class QuatPropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2), val(0.0, 0.0, 0.0, 0.0) {
            }

            glm::quat val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToYaw;
        QMap<const QtProperty*, QtProperty*> propertyToPitch;
        QMap<const QtProperty*, QtProperty*> propertyToRoll;

        QMap<const QtProperty*, QtProperty*> yawToProperty;
        QMap<const QtProperty*, QtProperty*> pitchToProperty;
        QMap<const QtProperty*, QtProperty*> rollToProperty;

    public:
        QuatPropertyManager(QObject* parent = 0);
        ~QuatPropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::quat value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::quat& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::quat& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* QUATPROPERTYMANAGER_H_ */
