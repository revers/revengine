/*
 * DMat2PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef DMAT2PROPERTYMANAGER_H_
#define DMAT2PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class DMat2PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2), val(0.0) {
            }

            glm::dmat2 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToM11;
        QMap<const QtProperty*, QtProperty*> propertyToM12;
        QMap<const QtProperty*, QtProperty*> propertyToM21;
        QMap<const QtProperty*, QtProperty*> propertyToM22;

        QMap<const QtProperty*, QtProperty*> m11ToProperty;
        QMap<const QtProperty*, QtProperty*> m12ToProperty;
        QMap<const QtProperty*, QtProperty*> m21ToProperty;
        QMap<const QtProperty*, QtProperty*> m22ToProperty;

    public:
        DMat2PropertyManager(QObject* parent = 0);
        ~DMat2PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::dmat2 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::dmat2& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::dmat2& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* DMAT2PROPERTYMANAGER_H_ */
