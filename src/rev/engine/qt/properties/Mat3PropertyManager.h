/*
 * Mat3PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef MAT3PROPERTYMANAGER_H_
#define MAT3PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class Mat3PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2), val(0.0) {
            }

            glm::mat3 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToM11;
        QMap<const QtProperty*, QtProperty*> propertyToM12;
		QMap<const QtProperty*, QtProperty*> propertyToM13;
        QMap<const QtProperty*, QtProperty*> propertyToM21;
        QMap<const QtProperty*, QtProperty*> propertyToM22;
		QMap<const QtProperty*, QtProperty*> propertyToM23;
		QMap<const QtProperty*, QtProperty*> propertyToM31;
        QMap<const QtProperty*, QtProperty*> propertyToM32;
		QMap<const QtProperty*, QtProperty*> propertyToM33;

        QMap<const QtProperty*, QtProperty*> m11ToProperty;
        QMap<const QtProperty*, QtProperty*> m12ToProperty;
		QMap<const QtProperty*, QtProperty*> m13ToProperty;
        QMap<const QtProperty*, QtProperty*> m21ToProperty;
        QMap<const QtProperty*, QtProperty*> m22ToProperty;
		QMap<const QtProperty*, QtProperty*> m23ToProperty;
		QMap<const QtProperty*, QtProperty*> m31ToProperty;
        QMap<const QtProperty*, QtProperty*> m32ToProperty;
		QMap<const QtProperty*, QtProperty*> m33ToProperty;

    public:
        Mat3PropertyManager(QObject* parent = 0);
        ~Mat3PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::mat3 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::mat3& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::mat3& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* MAT3PROPERTYMANAGER_H_ */
