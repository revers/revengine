/*
 * Mat4PropertyManager.cpp
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#include <rev/engine/qt/QtConfig.h>
#include "Mat4PropertyManager.h"

using namespace rev;

void Mat4PropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    changeListenerActive = false;
    if (QtProperty* prop = m11ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[0][0] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m12ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[1][0] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m13ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[2][0] = value;
        setValue(prop, m);	
	} else if (QtProperty* prop = m14ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[3][0] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m21ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[0][1] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m22ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[1][1] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m23ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[2][1] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m24ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[3][1] = value;
        setValue(prop, m);	
	} else if (QtProperty* prop = m31ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[0][2] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m32ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[1][2] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m33ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[2][2] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m34ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[3][2] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m41ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[0][3] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m42ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[1][3] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m43ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[2][3] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m44ToProperty.value(property, 0)) {
        glm::mat4 m = values[prop].val;
        m[3][3] = value;
        setValue(prop, m);
	}
	
    changeListenerActive = true;
}

void Mat4PropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = m11ToProperty.value(property, 0)) {
        propertyToM11[pointProp] = 0;
        m11ToProperty.remove(property);
    } else if (QtProperty* pointProp = m12ToProperty.value(property, 0)) {
        propertyToM12[pointProp] = 0;
        m12ToProperty.remove(property);
	} else if (QtProperty* pointProp = m13ToProperty.value(property, 0)) {
        propertyToM13[pointProp] = 0;
        m13ToProperty.remove(property);
	} else if (QtProperty* pointProp = m14ToProperty.value(property, 0)) {
        propertyToM14[pointProp] = 0;
        m14ToProperty.remove(property);		
    } else if (QtProperty* pointProp = m21ToProperty.value(property, 0)) {
        propertyToM21[pointProp] = 0;
        m21ToProperty.remove(property);
    } else if (QtProperty* pointProp = m22ToProperty.value(property, 0)) {
        propertyToM22[pointProp] = 0;
        m22ToProperty.remove(property);
    } else if (QtProperty* pointProp = m23ToProperty.value(property, 0)) {
        propertyToM23[pointProp] = 0;
        m23ToProperty.remove(property);
	} else if (QtProperty* pointProp = m24ToProperty.value(property, 0)) {
        propertyToM24[pointProp] = 0;
        m24ToProperty.remove(property);
    } else if (QtProperty* pointProp = m31ToProperty.value(property, 0)) {
        propertyToM31[pointProp] = 0;
        m31ToProperty.remove(property);
    } else if (QtProperty* pointProp = m32ToProperty.value(property, 0)) {
        propertyToM32[pointProp] = 0;
        m32ToProperty.remove(property);
    } else if (QtProperty* pointProp = m33ToProperty.value(property, 0)) {
        propertyToM33[pointProp] = 0;
        m33ToProperty.remove(property);
    } else if (QtProperty* pointProp = m34ToProperty.value(property, 0)) {
        propertyToM34[pointProp] = 0;
        m34ToProperty.remove(property);
    } else if (QtProperty* pointProp = m41ToProperty.value(property, 0)) {
        propertyToM41[pointProp] = 0;
        m41ToProperty.remove(property);
    } else if (QtProperty* pointProp = m42ToProperty.value(property, 0)) {
        propertyToM42[pointProp] = 0;
        m42ToProperty.remove(property);
    } else if (QtProperty* pointProp = m43ToProperty.value(property, 0)) {
        propertyToM43[pointProp] = 0;
        m43ToProperty.remove(property);
    } else if (QtProperty* pointProp = m44ToProperty.value(property, 0)) {
        propertyToM44[pointProp] = 0;
        m44ToProperty.remove(property);
    }
}

Mat4PropertyManager::Mat4PropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

Mat4PropertyManager::~Mat4PropertyManager() {
    clear();
}

QtDoublePropertyManager* Mat4PropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::mat4 Mat4PropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::mat4(0);
    }

    return it.value().val;
}

int Mat4PropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString Mat4PropertyManager::valueText(const QtProperty* property) const {
    const Mat4PropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::mat4 m = it.value().val;
    const int dec = it.value().decimals;
    return QString(tr("(%1, %2, %3, %4 | %5, %6, %7, %8 | %9, %10, %11, %12 | %13, %14, %15, %16)")
            .arg(QString::number(m[0][0], 'f', dec))
            .arg(QString::number(m[1][0], 'f', dec))
			.arg(QString::number(m[2][0], 'f', dec))
			.arg(QString::number(m[3][0], 'f', dec))
            .arg(QString::number(m[0][1], 'f', dec))
            .arg(QString::number(m[1][1], 'f', dec))
			.arg(QString::number(m[2][1], 'f', dec))
			.arg(QString::number(m[3][1], 'f', dec))
			.arg(QString::number(m[0][2], 'f', dec))
            .arg(QString::number(m[1][2], 'f', dec))
			.arg(QString::number(m[2][2], 'f', dec))
			.arg(QString::number(m[3][2], 'f', dec))
			.arg(QString::number(m[0][3], 'f', dec))
            .arg(QString::number(m[1][3], 'f', dec))
			.arg(QString::number(m[2][3], 'f', dec))
			.arg(QString::number(m[3][3], 'f', dec)));
}

void Mat4PropertyManager::setValue(QtProperty* property,
        const glm::mat4& val) {
    const Mat4PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToM11[property],
            val[0][0]);
    doublePropertyManager->setValue(propertyToM12[property],
            val[1][0]);
	doublePropertyManager->setValue(propertyToM13[property],
            val[2][0]);
	doublePropertyManager->setValue(propertyToM14[property],
            val[3][0]);
    doublePropertyManager->setValue(propertyToM21[property],
            val[0][1]);
    doublePropertyManager->setValue(propertyToM22[property],
            val[1][1]);
	doublePropertyManager->setValue(propertyToM23[property],
            val[2][1]);	
	doublePropertyManager->setValue(propertyToM24[property],
            val[3][1]);	
	doublePropertyManager->setValue(propertyToM31[property],
            val[0][2]);
    doublePropertyManager->setValue(propertyToM32[property],
            val[1][2]);
	doublePropertyManager->setValue(propertyToM33[property],
            val[2][2]);		
	doublePropertyManager->setValue(propertyToM34[property],
            val[3][2]);	
	doublePropertyManager->setValue(propertyToM41[property],
            val[0][3]);
    doublePropertyManager->setValue(propertyToM42[property],
            val[1][3]);
	doublePropertyManager->setValue(propertyToM43[property],
            val[2][3]);		
	doublePropertyManager->setValue(propertyToM44[property],
            val[3][3]);	

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void Mat4PropertyManager::setDecimals(QtProperty* property, int prec) {

    const Mat4PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    Mat4PropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToM11[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM12[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM13[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM14[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM21[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM22[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM23[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM24[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM31[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM32[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM33[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM34[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM41[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM42[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM43[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM44[property],
            prec);

    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void Mat4PropertyManager::initializeProperty(QtProperty* property) {
    values[property] = Mat4PropertyManager::Data();

    QtProperty* m11Prop = doublePropertyManager->addProperty();
    m11Prop->setPropertyName(tr("m11"));
    doublePropertyManager->setDecimals(m11Prop, decimals(property));
    doublePropertyManager->setValue(m11Prop, 0);
    doublePropertyManager->setSingleStep(m11Prop, SINGLE_STEP);
    propertyToM11[property] = m11Prop;
    m11ToProperty[m11Prop] = property;
    property->addSubProperty(m11Prop);

    QtProperty* m12Prop = doublePropertyManager->addProperty();
    m12Prop->setPropertyName(tr("m12"));
    doublePropertyManager->setDecimals(m12Prop, decimals(property));
    doublePropertyManager->setValue(m12Prop, 0);
    doublePropertyManager->setSingleStep(m12Prop, SINGLE_STEP);
    propertyToM12[property] = m12Prop;
    m12ToProperty[m12Prop] = property;
    property->addSubProperty(m12Prop);
	
	QtProperty* m13Prop = doublePropertyManager->addProperty();
    m13Prop->setPropertyName(tr("m13"));
    doublePropertyManager->setDecimals(m13Prop, decimals(property));
    doublePropertyManager->setValue(m13Prop, 0);
    doublePropertyManager->setSingleStep(m13Prop, SINGLE_STEP);
    propertyToM13[property] = m13Prop;
    m13ToProperty[m13Prop] = property;
    property->addSubProperty(m13Prop);
	
	QtProperty* m14Prop = doublePropertyManager->addProperty();
    m14Prop->setPropertyName(tr("m14"));
    doublePropertyManager->setDecimals(m14Prop, decimals(property));
    doublePropertyManager->setValue(m14Prop, 0);
    doublePropertyManager->setSingleStep(m14Prop, SINGLE_STEP);
    propertyToM14[property] = m14Prop;
    m14ToProperty[m14Prop] = property;
    property->addSubProperty(m14Prop);	

    QtProperty* m21Prop = doublePropertyManager->addProperty();
    m21Prop->setPropertyName(tr("m21"));
    doublePropertyManager->setDecimals(m21Prop, decimals(property));
    doublePropertyManager->setValue(m21Prop, 0);
    doublePropertyManager->setSingleStep(m21Prop, SINGLE_STEP);
    propertyToM21[property] = m21Prop;
    m21ToProperty[m21Prop] = property;
    property->addSubProperty(m21Prop);

    QtProperty* m22Prop = doublePropertyManager->addProperty();
    m22Prop->setPropertyName(tr("m22"));
    doublePropertyManager->setDecimals(m22Prop, decimals(property));
    doublePropertyManager->setValue(m22Prop, 0);
    doublePropertyManager->setSingleStep(m22Prop, SINGLE_STEP);
    propertyToM22[property] = m22Prop;
    m22ToProperty[m22Prop] = property;
    property->addSubProperty(m22Prop);
	
	QtProperty* m23Prop = doublePropertyManager->addProperty();
    m23Prop->setPropertyName(tr("m23"));
    doublePropertyManager->setDecimals(m23Prop, decimals(property));
    doublePropertyManager->setValue(m23Prop, 0);
    doublePropertyManager->setSingleStep(m23Prop, SINGLE_STEP);
    propertyToM23[property] = m23Prop;
    m23ToProperty[m23Prop] = property;
    property->addSubProperty(m23Prop);
	
	QtProperty* m24Prop = doublePropertyManager->addProperty();
    m24Prop->setPropertyName(tr("m24"));
    doublePropertyManager->setDecimals(m24Prop, decimals(property));
    doublePropertyManager->setValue(m24Prop, 0);
    doublePropertyManager->setSingleStep(m24Prop, SINGLE_STEP);
    propertyToM24[property] = m24Prop;
    m24ToProperty[m24Prop] = property;
    property->addSubProperty(m24Prop);
	
	QtProperty* m31Prop = doublePropertyManager->addProperty();
    m31Prop->setPropertyName(tr("m31"));
    doublePropertyManager->setDecimals(m31Prop, decimals(property));
    doublePropertyManager->setValue(m31Prop, 0);
    doublePropertyManager->setSingleStep(m31Prop, SINGLE_STEP);
    propertyToM31[property] = m31Prop;
    m31ToProperty[m31Prop] = property;
    property->addSubProperty(m31Prop);

    QtProperty* m32Prop = doublePropertyManager->addProperty();
    m32Prop->setPropertyName(tr("m32"));
    doublePropertyManager->setDecimals(m32Prop, decimals(property));
    doublePropertyManager->setValue(m32Prop, 0);
    doublePropertyManager->setSingleStep(m32Prop, SINGLE_STEP);
    propertyToM32[property] = m32Prop;
    m32ToProperty[m32Prop] = property;
    property->addSubProperty(m32Prop);
	
	QtProperty* m33Prop = doublePropertyManager->addProperty();
    m33Prop->setPropertyName(tr("m33"));
    doublePropertyManager->setDecimals(m33Prop, decimals(property));
    doublePropertyManager->setValue(m33Prop, 0);
    doublePropertyManager->setSingleStep(m33Prop, SINGLE_STEP);
    propertyToM33[property] = m33Prop;
    m33ToProperty[m33Prop] = property;
    property->addSubProperty(m33Prop);
	
	QtProperty* m34Prop = doublePropertyManager->addProperty();
    m34Prop->setPropertyName(tr("m34"));
    doublePropertyManager->setDecimals(m34Prop, decimals(property));
    doublePropertyManager->setValue(m34Prop, 0);
    doublePropertyManager->setSingleStep(m34Prop, SINGLE_STEP);
    propertyToM34[property] = m34Prop;
    m34ToProperty[m34Prop] = property;
    property->addSubProperty(m34Prop);
	
	QtProperty* m41Prop = doublePropertyManager->addProperty();
    m41Prop->setPropertyName(tr("m41"));
    doublePropertyManager->setDecimals(m41Prop, decimals(property));
    doublePropertyManager->setValue(m41Prop, 0);
    doublePropertyManager->setSingleStep(m41Prop, SINGLE_STEP);
    propertyToM41[property] = m41Prop;
    m41ToProperty[m41Prop] = property;
    property->addSubProperty(m41Prop);

    QtProperty* m42Prop = doublePropertyManager->addProperty();
    m42Prop->setPropertyName(tr("m42"));
    doublePropertyManager->setDecimals(m42Prop, decimals(property));
    doublePropertyManager->setValue(m42Prop, 0);
    doublePropertyManager->setSingleStep(m42Prop, SINGLE_STEP);
    propertyToM42[property] = m42Prop;
    m42ToProperty[m42Prop] = property;
    property->addSubProperty(m42Prop);
	
	QtProperty* m43Prop = doublePropertyManager->addProperty();
    m43Prop->setPropertyName(tr("m43"));
    doublePropertyManager->setDecimals(m43Prop, decimals(property));
    doublePropertyManager->setValue(m43Prop, 0);
    doublePropertyManager->setSingleStep(m43Prop, SINGLE_STEP);
    propertyToM43[property] = m43Prop;
    m43ToProperty[m43Prop] = property;
    property->addSubProperty(m43Prop);
	
	QtProperty* m44Prop = doublePropertyManager->addProperty();
    m44Prop->setPropertyName(tr("m44"));
    doublePropertyManager->setDecimals(m44Prop, decimals(property));
    doublePropertyManager->setValue(m44Prop, 0);
    doublePropertyManager->setSingleStep(m44Prop, SINGLE_STEP);
    propertyToM44[property] = m44Prop;
    m44ToProperty[m44Prop] = property;
    property->addSubProperty(m44Prop);
}

void Mat4PropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* m11Prop = propertyToM11[property];
    if (m11Prop) {
        m11ToProperty.remove(m11Prop);
        delete m11Prop;
    }
    propertyToM11.remove(property);

    QtProperty* m12Prop = propertyToM12[property];
    if (m12Prop) {
        m12ToProperty.remove(m12Prop);
        delete m12Prop;
    }
    propertyToM12.remove(property);
	
	QtProperty* m13Prop = propertyToM13[property];
    if (m13Prop) {
        m13ToProperty.remove(m13Prop);
        delete m13Prop;
    }
    propertyToM13.remove(property);
	
	QtProperty* m14Prop = propertyToM14[property];
    if (m14Prop) {
        m14ToProperty.remove(m14Prop);
        delete m14Prop;
    }
    propertyToM14.remove(property);

    QtProperty* m21Prop = propertyToM21[property];
    if (m21Prop) {
        m21ToProperty.remove(m21Prop);
        delete m21Prop;
    }
    propertyToM21.remove(property);

    QtProperty* m22Prop = propertyToM22[property];
    if (m22Prop) {
        m22ToProperty.remove(m22Prop);
        delete m22Prop;
    }
    propertyToM22.remove(property);
	
	QtProperty* m23Prop = propertyToM23[property];
    if (m23Prop) {
        m23ToProperty.remove(m23Prop);
        delete m23Prop;
    }
    propertyToM23.remove(property);
	
	QtProperty* m24Prop = propertyToM24[property];
    if (m24Prop) {
        m24ToProperty.remove(m24Prop);
        delete m24Prop;
    }
    propertyToM24.remove(property);
	
	QtProperty* m31Prop = propertyToM31[property];
    if (m31Prop) {
        m31ToProperty.remove(m31Prop);
        delete m31Prop;
    }
    propertyToM31.remove(property);

    QtProperty* m32Prop = propertyToM32[property];
    if (m32Prop) {
        m32ToProperty.remove(m32Prop);
        delete m32Prop;
    }
    propertyToM32.remove(property);
	
	QtProperty* m33Prop = propertyToM33[property];
    if (m33Prop) {
        m33ToProperty.remove(m33Prop);
        delete m33Prop;
    }
    propertyToM33.remove(property);
	
	QtProperty* m34Prop = propertyToM34[property];
    if (m34Prop) {
        m34ToProperty.remove(m34Prop);
        delete m34Prop;
    }
    propertyToM34.remove(property);
	
	QtProperty* m41Prop = propertyToM41[property];
    if (m41Prop) {
        m41ToProperty.remove(m41Prop);
        delete m41Prop;
    }
    propertyToM41.remove(property);

    QtProperty* m42Prop = propertyToM42[property];
    if (m42Prop) {
        m42ToProperty.remove(m42Prop);
        delete m42Prop;
    }
    propertyToM42.remove(property);
	
	QtProperty* m43Prop = propertyToM43[property];
    if (m43Prop) {
        m43ToProperty.remove(m43Prop);
        delete m43Prop;
    }
    propertyToM43.remove(property);
	
	QtProperty* m44Prop = propertyToM44[property];
    if (m44Prop) {
        m44ToProperty.remove(m44Prop);
        delete m44Prop;
    }
    propertyToM44.remove(property);

    values.remove(property);
}
