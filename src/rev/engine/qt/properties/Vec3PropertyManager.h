/*
 * Vec3PropertyManager.h
 *
 *  Created on: 13-01-2013
 *      Author: Revers
 */

#ifndef VEC3PROPERTYMANAGER_H_
#define VEC3PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class Vec3PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2) {
            }

            glm::vec3 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToX;
        QMap<const QtProperty*, QtProperty*> propertyToY;
        QMap<const QtProperty*, QtProperty*> propertyToZ;

        QMap<const QtProperty*, QtProperty*> xToProperty;
        QMap<const QtProperty*, QtProperty*> yToProperty;
        QMap<const QtProperty*, QtProperty*> zToProperty;

    public:
        Vec3PropertyManager(QObject* parent = 0);
        ~Vec3PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::vec3 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::vec3& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::vec3& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev

#endif /* VEC3PROPERTYMANAGER_H_ */
