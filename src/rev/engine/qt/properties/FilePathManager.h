/*
 * FilePathManager.h
 *
 *  Created on: 13-01-2013
 *      Author: Revers
 */

#ifndef FILEPATHMANAGER_H_
#define FILEPATHMANAGER_H_

#include <QtAbstractPropertyManager>
#include <QMap>

namespace rev {

	class FilePathManager: public QtAbstractPropertyManager {
	Q_OBJECT

		struct Data {
			QString value;
			QString filter;
		};
		QMap<const QtProperty*, Data> theValues;

	public:
		FilePathManager(QObject* parent = 0) :
				QtAbstractPropertyManager(parent) {
		}

		QString value(const QtProperty* property) const;
		QString filter(const QtProperty* property) const;

	public slots:
		void setValue(QtProperty*, const QString&);
		void setFilter(QtProperty*, const QString&);

	signals:
		void valueChanged(QtProperty*, const QString&);
		void filterChanged(QtProperty*, const QString&);

	protected:
		QString valueText(const QtProperty* property) const {
			return value(property);
		}
		void initializeProperty(QtProperty* property) {
			theValues[property] = Data();
		}
		void uninitializeProperty(QtProperty* property) {
			theValues.remove(property);
		}
	};

} /* namespace rev */
#endif /* FILEPATHMANAGER_H_ */
