/*
 * FileEditFactory.h
 *
 *  Created on: 24-03-2013
 *      Author: Revers
 */

#ifndef FILEEDITFACTORY_H_
#define FILEEDITFACTORY_H_

#include <qtpropertybrowser.h>
#include "FilePathManager.h"

namespace rev {
	class FileEdit;

	class FileEditFactory: public QtAbstractEditorFactory<FilePathManager> {
	Q_OBJECT
		public:
		FileEditFactory(QObject* parent = 0) :
				QtAbstractEditorFactory<FilePathManager>(parent) {
		}
		virtual ~FileEditFactory();

	protected:
		virtual void connectPropertyManager(FilePathManager* manager);
		virtual QWidget* createEditor(FilePathManager* manager, QtProperty* property,
				QWidget* parent);
		virtual void disconnectPropertyManager(FilePathManager* manager);

	private slots:
		void slotPropertyChanged(QtProperty* property, const QString& value);
		void slotFilterChanged(QtProperty* property, const QString& filter);
		void slotSetValue(const QString& value);
		void slotEditorDestroyed(QObject* object);

	private:
		QMap<QtProperty*, QList<FileEdit*>> theCreatedEditors;
		QMap<FileEdit*, QtProperty*> theEditorToProperty;
	};

} /* namespace rev */
#endif /* FILEEDITFACTORY_H_ */
