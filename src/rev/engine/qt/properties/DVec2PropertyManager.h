/*
 * DVec2PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef DVEC2PROPERTYMANAGER_H_
#define DVEC2PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class DVec2PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2) {
            }

            glm::dvec2 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToX;
        QMap<const QtProperty*, QtProperty*> propertyToY;

        QMap<const QtProperty*, QtProperty*> xToProperty;
        QMap<const QtProperty*, QtProperty*> yToProperty;

    public:
        DVec2PropertyManager(QObject* parent = 0);
        ~DVec2PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::dvec2 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::dvec2& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::dvec2& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* DVEC2PROPERTYMANAGER_H_ */
