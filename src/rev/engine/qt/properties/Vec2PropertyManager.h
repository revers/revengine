/*
 * Vec2PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef VEC2PROPERTYMANAGER_H_
#define VEC2PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class Vec2PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2) {
            }

            glm::vec2 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToX;
        QMap<const QtProperty*, QtProperty*> propertyToY;

        QMap<const QtProperty*, QtProperty*> xToProperty;
        QMap<const QtProperty*, QtProperty*> yToProperty;

    public:
        Vec2PropertyManager(QObject* parent = 0);
        ~Vec2PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::vec2 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::vec2& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::vec2& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* VEC2PROPERTYMANAGER_H_ */
