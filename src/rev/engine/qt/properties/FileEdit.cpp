/*
 * FileEdit.cpp
 *
 *  Created on: 24-03-2013
 *      Author: Revers
 */

#include <QHBoxLayout>
#include <QToolButton>
#include <QFileDialog>
#include <QFocusEvent>
#include <QSettings>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>

#include "FileEdit.h"

#define DEFAULT_FILE_EDIT_DIR_KEY "fedk"

using namespace rev;

FileEdit::FileEdit(QWidget* parent) :
		QWidget(parent) {
	QHBoxLayout* layout = new QHBoxLayout(this);
	layout->setMargin(0);
	layout->setSpacing(0);
	theLineEdit = new QLineEdit(this);
	theLineEdit->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred));
	theLineEdit->setReadOnly(true);
	QToolButton* button = new QToolButton(this);
	button->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred));
	button->setText(QLatin1String("..."));
	layout->addWidget(theLineEdit);
	layout->addWidget(button);
	theLineEdit->setFocusProxy(this);
	setFocusPolicy(Qt::StrongFocus);
	setAttribute(Qt::WA_InputMethodEnabled);

	connect(theLineEdit, SIGNAL(textEdited(const QString&)),
			this, SIGNAL(filePathChanged(const QString&)));
	connect(button, SIGNAL(clicked()), this, SLOT(buttonClicked()));
}

void FileEdit::buttonClicked() {
	QSettings mySettings;

	QString text = theLineEdit->text();
	std::string dir;
	if (!text.isEmpty()) {
		dir = ResourcePath::get(text.toUtf8().data(), false);
	}

	QString filePath = QFileDialog::getOpenFileName(this, tr("Choose a file"),
			!text.isEmpty() ? dir.c_str() : mySettings.value(DEFAULT_FILE_EDIT_DIR_KEY).toString(),
			theFilter);
	if (filePath.isNull()) {
		return;
	}
	QDir currentDir;
	mySettings.setValue(DEFAULT_FILE_EDIT_DIR_KEY, currentDir.absoluteFilePath(filePath));

	std::string path = filePath.toUtf8().data();
	std::string relativePath = ResourcePath::makeRelative(path);

	theLineEdit->setText(relativePath.c_str());
	emit filePathChanged(relativePath.c_str());
}

void FileEdit::focusInEvent(QFocusEvent* e) {
	theLineEdit->event(e);
	if (e->reason() == Qt::TabFocusReason || e->reason() == Qt::BacktabFocusReason) {
		theLineEdit->selectAll();
	}
	QWidget::focusInEvent(e);
}

void FileEdit::focusOutEvent(QFocusEvent* e) {
	theLineEdit->event(e);
	QWidget::focusOutEvent(e);
}

void FileEdit::keyPressEvent(QKeyEvent* e) {
	theLineEdit->event(e);
}

void FileEdit::keyReleaseEvent(QKeyEvent* e) {
	theLineEdit->event(e);
}
