/*
 * DMat3PropertyManager.cpp
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#include <rev/engine/qt/QtConfig.h>
#include "DMat3PropertyManager.h"

using namespace rev;

void DMat3PropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    changeListenerActive = false;
    if (QtProperty* prop = m11ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[0][0] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m12ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[1][0] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m13ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[2][0] = value;
        setValue(prop, m);	
    } else if (QtProperty* prop = m21ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[0][1] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m22ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[1][1] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m23ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[2][1] = value;
        setValue(prop, m);
	} else if (QtProperty* prop = m31ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[0][2] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m32ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[1][2] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m33ToProperty.value(property, 0)) {
        glm::dmat3 m = values[prop].val;
        m[2][2] = value;
        setValue(prop, m);
	}
    changeListenerActive = true;
}

void DMat3PropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = m11ToProperty.value(property, 0)) {
        propertyToM11[pointProp] = 0;
        m11ToProperty.remove(property);
    } else if (QtProperty* pointProp = m12ToProperty.value(property, 0)) {
        propertyToM12[pointProp] = 0;
        m12ToProperty.remove(property);
	} else if (QtProperty* pointProp = m13ToProperty.value(property, 0)) {
        propertyToM13[pointProp] = 0;
        m13ToProperty.remove(property);	
    } else if (QtProperty* pointProp = m21ToProperty.value(property, 0)) {
        propertyToM21[pointProp] = 0;
        m21ToProperty.remove(property);
    } else if (QtProperty* pointProp = m22ToProperty.value(property, 0)) {
        propertyToM22[pointProp] = 0;
        m22ToProperty.remove(property);
    } else if (QtProperty* pointProp = m23ToProperty.value(property, 0)) {
        propertyToM23[pointProp] = 0;
        m23ToProperty.remove(property);
    } else if (QtProperty* pointProp = m31ToProperty.value(property, 0)) {
        propertyToM31[pointProp] = 0;
        m31ToProperty.remove(property);
    } else if (QtProperty* pointProp = m32ToProperty.value(property, 0)) {
        propertyToM32[pointProp] = 0;
        m32ToProperty.remove(property);
    } else if (QtProperty* pointProp = m33ToProperty.value(property, 0)) {
        propertyToM33[pointProp] = 0;
        m33ToProperty.remove(property);
    }
}

DMat3PropertyManager::DMat3PropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

DMat3PropertyManager::~DMat3PropertyManager() {
    clear();
}

QtDoublePropertyManager* DMat3PropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::dmat3 DMat3PropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::dmat3(0);
    }

    return it.value().val;
}

int DMat3PropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString DMat3PropertyManager::valueText(const QtProperty* property) const {
    const DMat3PropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::dmat3 m = it.value().val;
    const int dec = it.value().decimals;
    return QString(tr("(%1, %2, %3 | %4, %5, %6 | %7, %8, %9)")
            .arg(QString::number(m[0][0], 'f', dec))
            .arg(QString::number(m[1][0], 'f', dec))
			.arg(QString::number(m[2][0], 'f', dec))
            .arg(QString::number(m[0][1], 'f', dec))
            .arg(QString::number(m[1][1], 'f', dec))
			.arg(QString::number(m[2][1], 'f', dec))
			.arg(QString::number(m[0][2], 'f', dec))
            .arg(QString::number(m[1][2], 'f', dec))
			.arg(QString::number(m[2][2], 'f', dec)));
}

void DMat3PropertyManager::setValue(QtProperty* property,
        const glm::dmat3& val) {
    const DMat3PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToM11[property],
            val[0][0]);
    doublePropertyManager->setValue(propertyToM12[property],
            val[1][0]);
	doublePropertyManager->setValue(propertyToM13[property],
            val[2][0]);
    doublePropertyManager->setValue(propertyToM21[property],
            val[0][1]);
    doublePropertyManager->setValue(propertyToM22[property],
            val[1][1]);
	doublePropertyManager->setValue(propertyToM23[property],
            val[2][1]);	
	doublePropertyManager->setValue(propertyToM31[property],
            val[0][2]);
    doublePropertyManager->setValue(propertyToM32[property],
            val[1][2]);
	doublePropertyManager->setValue(propertyToM33[property],
            val[2][2]);		

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void DMat3PropertyManager::setDecimals(QtProperty* property, int prec) {

    const DMat3PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    DMat3PropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToM11[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM12[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM13[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM21[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM22[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM23[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM31[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM32[property],
            prec);
	doublePropertyManager->setDecimals(propertyToM33[property],
            prec);

    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void DMat3PropertyManager::initializeProperty(QtProperty* property) {
    values[property] = DMat3PropertyManager::Data();

    QtProperty* m11Prop = doublePropertyManager->addProperty();
    m11Prop->setPropertyName(tr("m11"));
    doublePropertyManager->setDecimals(m11Prop, decimals(property));
    doublePropertyManager->setValue(m11Prop, 0);
    doublePropertyManager->setSingleStep(m11Prop, SINGLE_STEP);
    propertyToM11[property] = m11Prop;
    m11ToProperty[m11Prop] = property;
    property->addSubProperty(m11Prop);

    QtProperty* m12Prop = doublePropertyManager->addProperty();
    m12Prop->setPropertyName(tr("m12"));
    doublePropertyManager->setDecimals(m12Prop, decimals(property));
    doublePropertyManager->setValue(m12Prop, 0);
    doublePropertyManager->setSingleStep(m12Prop, SINGLE_STEP);
    propertyToM12[property] = m12Prop;
    m12ToProperty[m12Prop] = property;
    property->addSubProperty(m12Prop);
	
	QtProperty* m13Prop = doublePropertyManager->addProperty();
    m13Prop->setPropertyName(tr("m13"));
    doublePropertyManager->setDecimals(m13Prop, decimals(property));
    doublePropertyManager->setValue(m13Prop, 0);
    doublePropertyManager->setSingleStep(m13Prop, SINGLE_STEP);
    propertyToM13[property] = m13Prop;
    m13ToProperty[m13Prop] = property;
    property->addSubProperty(m13Prop);

    QtProperty* m21Prop = doublePropertyManager->addProperty();
    m21Prop->setPropertyName(tr("m21"));
    doublePropertyManager->setDecimals(m21Prop, decimals(property));
    doublePropertyManager->setValue(m21Prop, 0);
    doublePropertyManager->setSingleStep(m21Prop, SINGLE_STEP);
    propertyToM21[property] = m21Prop;
    m21ToProperty[m21Prop] = property;
    property->addSubProperty(m21Prop);

    QtProperty* m22Prop = doublePropertyManager->addProperty();
    m22Prop->setPropertyName(tr("m22"));
    doublePropertyManager->setDecimals(m22Prop, decimals(property));
    doublePropertyManager->setValue(m22Prop, 0);
    doublePropertyManager->setSingleStep(m22Prop, SINGLE_STEP);
    propertyToM22[property] = m22Prop;
    m22ToProperty[m22Prop] = property;
    property->addSubProperty(m22Prop);
	
	QtProperty* m23Prop = doublePropertyManager->addProperty();
    m23Prop->setPropertyName(tr("m23"));
    doublePropertyManager->setDecimals(m23Prop, decimals(property));
    doublePropertyManager->setValue(m23Prop, 0);
    doublePropertyManager->setSingleStep(m23Prop, SINGLE_STEP);
    propertyToM23[property] = m23Prop;
    m23ToProperty[m23Prop] = property;
    property->addSubProperty(m23Prop);
	
	QtProperty* m31Prop = doublePropertyManager->addProperty();
    m31Prop->setPropertyName(tr("m31"));
    doublePropertyManager->setDecimals(m31Prop, decimals(property));
    doublePropertyManager->setValue(m31Prop, 0);
    doublePropertyManager->setSingleStep(m31Prop, SINGLE_STEP);
    propertyToM31[property] = m31Prop;
    m31ToProperty[m31Prop] = property;
    property->addSubProperty(m31Prop);

    QtProperty* m32Prop = doublePropertyManager->addProperty();
    m32Prop->setPropertyName(tr("m32"));
    doublePropertyManager->setDecimals(m32Prop, decimals(property));
    doublePropertyManager->setValue(m32Prop, 0);
    doublePropertyManager->setSingleStep(m32Prop, SINGLE_STEP);
    propertyToM32[property] = m32Prop;
    m32ToProperty[m32Prop] = property;
    property->addSubProperty(m32Prop);
	
	QtProperty* m33Prop = doublePropertyManager->addProperty();
    m33Prop->setPropertyName(tr("m33"));
    doublePropertyManager->setDecimals(m33Prop, decimals(property));
    doublePropertyManager->setValue(m33Prop, 0);
    doublePropertyManager->setSingleStep(m33Prop, SINGLE_STEP);
    propertyToM33[property] = m33Prop;
    m33ToProperty[m33Prop] = property;
    property->addSubProperty(m33Prop);
}

void DMat3PropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* m11Prop = propertyToM11[property];
    if (m11Prop) {
        m11ToProperty.remove(m11Prop);
        delete m11Prop;
    }
    propertyToM11.remove(property);

    QtProperty* m12Prop = propertyToM12[property];
    if (m12Prop) {
        m12ToProperty.remove(m12Prop);
        delete m12Prop;
    }
    propertyToM12.remove(property);
	
	QtProperty* m13Prop = propertyToM13[property];
    if (m13Prop) {
        m13ToProperty.remove(m13Prop);
        delete m13Prop;
    }
    propertyToM13.remove(property);

    QtProperty* m21Prop = propertyToM21[property];
    if (m21Prop) {
        m21ToProperty.remove(m21Prop);
        delete m21Prop;
    }
    propertyToM21.remove(property);

    QtProperty* m22Prop = propertyToM22[property];
    if (m22Prop) {
        m22ToProperty.remove(m22Prop);
        delete m22Prop;
    }
    propertyToM22.remove(property);
	
	QtProperty* m23Prop = propertyToM23[property];
    if (m23Prop) {
        m23ToProperty.remove(m23Prop);
        delete m23Prop;
    }
    propertyToM23.remove(property);
	
	QtProperty* m31Prop = propertyToM31[property];
    if (m31Prop) {
        m31ToProperty.remove(m31Prop);
        delete m31Prop;
    }
    propertyToM31.remove(property);

    QtProperty* m32Prop = propertyToM32[property];
    if (m32Prop) {
        m32ToProperty.remove(m32Prop);
        delete m32Prop;
    }
    propertyToM32.remove(property);
	
	QtProperty* m33Prop = propertyToM33[property];
    if (m33Prop) {
        m33ToProperty.remove(m33Prop);
        delete m33Prop;
    }
    propertyToM33.remove(property);

    values.remove(property);
}
