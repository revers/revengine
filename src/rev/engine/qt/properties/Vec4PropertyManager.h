/*
 * Vec4PropertyManager.h
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#ifndef VEC4PROPERTYMANAGER_H_
#define VEC4PROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>

namespace rev {
    class Vec4PropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2) {
            }

            glm::vec4 val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToX;
        QMap<const QtProperty*, QtProperty*> propertyToY;
        QMap<const QtProperty*, QtProperty*> propertyToZ;
        QMap<const QtProperty*, QtProperty*> propertyToW;

        QMap<const QtProperty*, QtProperty*> xToProperty;
        QMap<const QtProperty*, QtProperty*> yToProperty;
        QMap<const QtProperty*, QtProperty*> zToProperty;
        QMap<const QtProperty*, QtProperty*> wToProperty;

    public:
        Vec4PropertyManager(QObject* parent = 0);
        ~Vec4PropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::vec4 value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::vec4& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::vec4& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* VEC4PROPERTYMANAGER_H_ */
