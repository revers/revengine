/*
 * Vec4PropertyManager.cpp
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#include <rev/engine/qt/QtConfig.h>
#include "Vec4PropertyManager.h"

using namespace rev;

void Vec4PropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    changeListenerActive = false;
    if (QtProperty* prop = xToProperty.value(property, 0)) {
        glm::vec4 p = values[prop].val;
        p.x = value;
        setValue(prop, p);
    } else if (QtProperty* prop = yToProperty.value(property, 0)) {
        glm::vec4 p = values[prop].val;
        p.y = value;
        setValue(prop, p);
    } else if (QtProperty* prop = zToProperty.value(property, 0)) {
        glm::vec4 p = values[prop].val;
        p.z = value;
        setValue(prop, p);
    } else if (QtProperty* prop = wToProperty.value(property, 0)) {
        glm::vec4 p = values[prop].val;
        p.w = value;
        setValue(prop, p);
    }
    changeListenerActive = true;
}

void Vec4PropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = xToProperty.value(property, 0)) {
        propertyToX[pointProp] = 0;
        xToProperty.remove(property);
    } else if (QtProperty* pointProp = yToProperty.value(property, 0)) {
        propertyToY[pointProp] = 0;
        yToProperty.remove(property);
    } else if (QtProperty* pointProp = zToProperty.value(property, 0)) {
        propertyToZ[pointProp] = 0;
        zToProperty.remove(property);
    } else if (QtProperty* pointProp = wToProperty.value(property, 0)) {
        propertyToW[pointProp] = 0;
        wToProperty.remove(property);
    }
}

Vec4PropertyManager::Vec4PropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

Vec4PropertyManager::~Vec4PropertyManager() {
    clear();
}

QtDoublePropertyManager* Vec4PropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::vec4 Vec4PropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::vec4(0);
    }

    return it.value().val;
}

int Vec4PropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString Vec4PropertyManager::valueText(const QtProperty* property) const {
    const Vec4PropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::vec4 v = it.value().val;
    const int dec = it.value().decimals;
    return QString(tr("(%1, %2, %3, %4)")
            .arg(QString::number(v.x, 'f', dec))
            .arg(QString::number(v.y, 'f', dec))
            .arg(QString::number(v.z, 'f', dec))
            .arg(QString::number(v.w, 'f', dec)));
}

void Vec4PropertyManager::setValue(QtProperty* property,
        const glm::vec4& val) {
    const Vec4PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToX[property],
            val.x);
    doublePropertyManager->setValue(propertyToY[property],
            val.y);
    doublePropertyManager->setValue(propertyToZ[property],
            val.z);
    doublePropertyManager->setValue(propertyToW[property],
            val.w);

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void Vec4PropertyManager::setDecimals(QtProperty* property, int prec) {

    const Vec4PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    Vec4PropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToX[property],
            prec);
    doublePropertyManager->setDecimals(propertyToY[property],
            prec);
    doublePropertyManager->setDecimals(propertyToZ[property],
            prec);
    doublePropertyManager->setDecimals(propertyToW[property],
            prec);

    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void Vec4PropertyManager::initializeProperty(QtProperty* property) {
    values[property] = Vec4PropertyManager::Data();

    QtProperty* xProp = doublePropertyManager->addProperty();
    xProp->setPropertyName(tr("X"));
    doublePropertyManager->setDecimals(xProp, decimals(property));
    doublePropertyManager->setValue(xProp, 0);
    doublePropertyManager->setSingleStep(xProp, SINGLE_STEP);
    propertyToX[property] = xProp;
    xToProperty[xProp] = property;
    property->addSubProperty(xProp);

    QtProperty* yProp = doublePropertyManager->addProperty();
    yProp->setPropertyName(tr("Y"));
    doublePropertyManager->setDecimals(yProp, decimals(property));
    doublePropertyManager->setValue(yProp, 0);
    doublePropertyManager->setSingleStep(yProp, SINGLE_STEP);
    propertyToY[property] = yProp;
    yToProperty[yProp] = property;
    property->addSubProperty(yProp);

    QtProperty* zProp = doublePropertyManager->addProperty();
    zProp->setPropertyName(tr("Z"));
    doublePropertyManager->setDecimals(zProp, decimals(property));
    doublePropertyManager->setValue(zProp, 0);
    doublePropertyManager->setSingleStep(zProp, SINGLE_STEP);
    propertyToZ[property] = zProp;
    zToProperty[zProp] = property;
    property->addSubProperty(zProp);

    QtProperty* wProp = doublePropertyManager->addProperty();
    wProp->setPropertyName(tr("W"));
    doublePropertyManager->setDecimals(wProp, decimals(property));
    doublePropertyManager->setValue(wProp, 0);
    doublePropertyManager->setSingleStep(wProp, SINGLE_STEP);
    propertyToW[property] = wProp;
    wToProperty[wProp] = property;
    property->addSubProperty(wProp);
}

void Vec4PropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* xProp = propertyToX[property];
    if (xProp) {
        xToProperty.remove(xProp);
        delete xProp;
    }
    propertyToX.remove(property);

    QtProperty* yProp = propertyToY[property];
    if (yProp) {
        yToProperty.remove(yProp);
        delete yProp;
    }
    propertyToY.remove(property);

    QtProperty* zProp = propertyToZ[property];
    if (zProp) {
        zToProperty.remove(zProp);
        delete zProp;
    }
    propertyToZ.remove(property);

    QtProperty* wProp = propertyToW[property];
    if (wProp) {
        wToProperty.remove(wProp);
        delete wProp;
    }
    propertyToW.remove(property);

    values.remove(property);
}
