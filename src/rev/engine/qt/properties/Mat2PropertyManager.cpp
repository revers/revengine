/*
 * Mat2PropertyManager.cpp
 *
 *  Created on: 22-01-2013
 *      Author: Revers
 */

#include <rev/engine/qt/QtConfig.h>
#include "Mat2PropertyManager.h"

using namespace rev;

void Mat2PropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    changeListenerActive = false;
    if (QtProperty* prop = m11ToProperty.value(property, 0)) {
        glm::mat2 m = values[prop].val;
        m[0][0] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m12ToProperty.value(property, 0)) {
        glm::mat2 m = values[prop].val;
        m[1][0] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m21ToProperty.value(property, 0)) {
        glm::mat2 m = values[prop].val;
        m[0][1] = value;
        setValue(prop, m);
    } else if (QtProperty* prop = m22ToProperty.value(property, 0)) {
        glm::mat2 m = values[prop].val;
        m[1][1] = value;
        setValue(prop, m);
    }
    changeListenerActive = true;
}

void Mat2PropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = m11ToProperty.value(property, 0)) {
        propertyToM11[pointProp] = 0;
        m11ToProperty.remove(property);
    } else if (QtProperty* pointProp = m12ToProperty.value(property, 0)) {
        propertyToM12[pointProp] = 0;
        m12ToProperty.remove(property);
    } else if (QtProperty* pointProp = m21ToProperty.value(property, 0)) {
        propertyToM21[pointProp] = 0;
        m21ToProperty.remove(property);
    } else if (QtProperty* pointProp = m22ToProperty.value(property, 0)) {
        propertyToM22[pointProp] = 0;
        m22ToProperty.remove(property);
    }
}

Mat2PropertyManager::Mat2PropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

Mat2PropertyManager::~Mat2PropertyManager() {
    clear();
}

QtDoublePropertyManager* Mat2PropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::mat2 Mat2PropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::mat2(0);
    }

    return it.value().val;
}

int Mat2PropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString Mat2PropertyManager::valueText(const QtProperty* property) const {
    const Mat2PropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::mat2 m = it.value().val;
    const int dec = it.value().decimals;
    return QString(tr("(%1, %2 | %3, %4)")
            .arg(QString::number(m[0][0], 'f', dec))
            .arg(QString::number(m[1][0], 'f', dec))
            .arg(QString::number(m[0][1], 'f', dec))
            .arg(QString::number(m[1][1], 'f', dec)));
}

void Mat2PropertyManager::setValue(QtProperty* property,
        const glm::mat2& val) {
    const Mat2PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToM11[property],
            val[0][0]);
    doublePropertyManager->setValue(propertyToM12[property],
            val[1][0]);
    doublePropertyManager->setValue(propertyToM21[property],
            val[0][1]);
    doublePropertyManager->setValue(propertyToM22[property],
            val[1][1]);

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void Mat2PropertyManager::setDecimals(QtProperty* property, int prec) {

    const Mat2PropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    Mat2PropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToM11[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM12[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM21[property],
            prec);
    doublePropertyManager->setDecimals(propertyToM22[property],
            prec);

    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void Mat2PropertyManager::initializeProperty(QtProperty* property) {
    values[property] = Mat2PropertyManager::Data();

    QtProperty* m11Prop = doublePropertyManager->addProperty();
    m11Prop->setPropertyName(tr("m11"));
    doublePropertyManager->setDecimals(m11Prop, decimals(property));
    doublePropertyManager->setValue(m11Prop, 0);
    doublePropertyManager->setSingleStep(m11Prop, SINGLE_STEP);
    propertyToM11[property] = m11Prop;
    m11ToProperty[m11Prop] = property;
    property->addSubProperty(m11Prop);

    QtProperty* m12Prop = doublePropertyManager->addProperty();
    m12Prop->setPropertyName(tr("m12"));
    doublePropertyManager->setDecimals(m12Prop, decimals(property));
    doublePropertyManager->setValue(m12Prop, 0);
    doublePropertyManager->setSingleStep(m12Prop, SINGLE_STEP);
    propertyToM12[property] = m12Prop;
    m12ToProperty[m12Prop] = property;
    property->addSubProperty(m12Prop);

    QtProperty* m21Prop = doublePropertyManager->addProperty();
    m21Prop->setPropertyName(tr("m21"));
    doublePropertyManager->setDecimals(m21Prop, decimals(property));
    doublePropertyManager->setValue(m21Prop, 0);
    doublePropertyManager->setSingleStep(m21Prop, SINGLE_STEP);
    propertyToM21[property] = m21Prop;
    m21ToProperty[m21Prop] = property;
    property->addSubProperty(m21Prop);

    QtProperty* m22Prop = doublePropertyManager->addProperty();
    m22Prop->setPropertyName(tr("m22"));
    doublePropertyManager->setDecimals(m22Prop, decimals(property));
    doublePropertyManager->setValue(m22Prop, 0);
    doublePropertyManager->setSingleStep(m22Prop, SINGLE_STEP);
    propertyToM22[property] = m22Prop;
    m22ToProperty[m22Prop] = property;
    property->addSubProperty(m22Prop);
}

void Mat2PropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* m11Prop = propertyToM11[property];
    if (m11Prop) {
        m11ToProperty.remove(m11Prop);
        delete m11Prop;
    }
    propertyToM11.remove(property);

    QtProperty* m12Prop = propertyToM12[property];
    if (m12Prop) {
        m12ToProperty.remove(m12Prop);
        delete m12Prop;
    }
    propertyToM12.remove(property);

    QtProperty* m21Prop = propertyToM21[property];
    if (m21Prop) {
        m21ToProperty.remove(m21Prop);
        delete m21Prop;
    }
    propertyToM21.remove(property);

    QtProperty* m22Prop = propertyToM22[property];
    if (m22Prop) {
        m22ToProperty.remove(m22Prop);
        delete m22Prop;
    }
    propertyToM22.remove(property);

    values.remove(property);
}
