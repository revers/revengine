/*
 * DQuatPropertyManager.cpp
 *
 *  Created on: 21-01-2013
 *      Author: Revers
 */

#include <glm/gtx/quaternion.hpp>
#include <rev/engine/qt/QtConfig.h>
#include <rev/common/RevAssert.h>

#include "DQuatPropertyManager.h"

using namespace rev;
using namespace std;

void DQuatPropertyManager::slotDoubleChanged(QtProperty* property,
        double value) {
    if (!changeListenerActive) {
        return;
    }

    QtProperty* prop = yawToProperty.value(property, 0);
    prop = prop ? prop : pitchToProperty.value(property, 0);
    prop = prop ? prop : rollToProperty.value(property, 0);
    if (!prop) {
        revAssert(prop);
        return;
    }

    double yaw = glm::radians((double) doublePropertyManager->value(
            propertyToYaw[prop]));
    double pitch = glm::radians((double) doublePropertyManager->value(
            propertyToPitch[prop]));
    double roll = glm::radians((double) doublePropertyManager->value(
            propertyToRoll[prop]));
    glm::dquat q(glm::dvec3(pitch, yaw, roll));

    changeListenerActive = false;
    setValue(prop, q);
    changeListenerActive = true;
}

void DQuatPropertyManager::slotPropertyDestroyed(QtProperty* property) {
    if (QtProperty* pointProp = yawToProperty.value(property, 0)) {
        propertyToYaw[pointProp] = 0;
        yawToProperty.remove(property);
    } else if (QtProperty* pointProp = pitchToProperty.value(property, 0)) {
        propertyToPitch[pointProp] = 0;
        pitchToProperty.remove(property);
    } else if (QtProperty* pointProp = rollToProperty.value(property, 0)) {
        propertyToRoll[pointProp] = 0;
        rollToProperty.remove(property);
    }
}

DQuatPropertyManager::DQuatPropertyManager(QObject* parent) :
        QtAbstractPropertyManager(parent) {

    doublePropertyManager = new QtDoublePropertyManager(this);
    connect(doublePropertyManager,
            SIGNAL(valueChanged(QtProperty* , double)),
            this, SLOT(slotDoubleChanged(QtProperty*, double)));
    connect(doublePropertyManager,
            SIGNAL(propertyDestroyed(QtProperty*)),
            this, SLOT(slotPropertyDestroyed(QtProperty*)));
}

DQuatPropertyManager::~DQuatPropertyManager() {
    clear();
}

QtDoublePropertyManager* DQuatPropertyManager::subDoublePropertyManager() const {
    return doublePropertyManager;
}

glm::dquat DQuatPropertyManager::value(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return glm::dquat(0, 0, 0, 1);
    }

    return it.value().val;
}

int DQuatPropertyManager::decimals(const QtProperty* property) const {
    typedef typename PropertyValueMap::const_iterator PropertyValueMapIter;

    const PropertyValueMapIter it = values.constFind(property);
    if (it == values.constEnd()) {
        return 0;
    }

    return it.value().decimals;
}

QString DQuatPropertyManager::valueText(const QtProperty* property) const {
    const DQuatPropertyManager::PropertyValueMap::const_iterator it =
            values.constFind(property);

    if (it == values.constEnd()) {
        return QString();
    }
    const glm::dquat q = it.value().val;
    const int dec = it.value().decimals;

    double yaw = (double) glm::yaw(q);
    double pitch = (double) glm::pitch(q);
    double roll = (double) glm::roll(q);

    return QString(tr("(%1, %2, %3)")
            .arg(QString::number(yaw, 'f', dec))
            .arg(QString::number(pitch, 'f', dec))
            .arg(QString::number(roll, 'f', dec)));
}

void DQuatPropertyManager::setValue(QtProperty* property,
        const glm::dquat& val) {
    const DQuatPropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    if (it.value().val == val) {
        return;
    }

    it.value().val = val;
    doublePropertyManager->setValue(propertyToYaw[property],
            (double) glm::yaw(val));
    doublePropertyManager->setValue(propertyToPitch[property],
            (double) glm::pitch(val));
    doublePropertyManager->setValue(propertyToRoll[property],
            (double) glm::roll(val));

    emit propertyChanged(property);
    emit valueChanged(property, val);
}

void DQuatPropertyManager::setDecimals(QtProperty* property, int prec) {

    const DQuatPropertyManager::PropertyValueMap::iterator it =
            values.find(property);

    if (it == values.end()) {
        return;
    }

    DQuatPropertyManager::Data data = it.value();

    if (prec > 13) {
        prec = 13;
    } else if (prec < 0) {
        prec = 0;
    }

    if (data.decimals == prec) {
        return;
    }

    data.decimals = prec;
    doublePropertyManager->setDecimals(propertyToYaw[property],
            prec);
    doublePropertyManager->setDecimals(propertyToPitch[property],
            prec);
    doublePropertyManager->setDecimals(propertyToRoll[property],
            prec);

    it.value() = data;

    emit decimalsChanged(property, data.decimals);
}

void DQuatPropertyManager::initializeProperty(QtProperty* property) {
    values[property] = DQuatPropertyManager::Data();

    QtProperty* yawProp = doublePropertyManager->addProperty();
    yawProp->setPropertyName(tr("Yaw"));
    doublePropertyManager->setDecimals(yawProp, decimals(property));
    doublePropertyManager->setValue(yawProp, 0);
    doublePropertyManager->setSingleStep(yawProp, SINGLE_ANGLE_STEP);
    propertyToYaw[property] = yawProp;
    yawToProperty[yawProp] = property;
    property->addSubProperty(yawProp);

    QtProperty* pitchProp = doublePropertyManager->addProperty();
    pitchProp->setPropertyName(tr("Pitch"));
    doublePropertyManager->setDecimals(pitchProp, decimals(property));
    doublePropertyManager->setValue(pitchProp, 0);
    doublePropertyManager->setSingleStep(pitchProp, SINGLE_ANGLE_STEP);
    propertyToPitch[property] = pitchProp;
    pitchToProperty[pitchProp] = property;
    property->addSubProperty(pitchProp);

    QtProperty* rollProp = doublePropertyManager->addProperty();
    rollProp->setPropertyName(tr("Roll"));
    doublePropertyManager->setDecimals(rollProp, decimals(property));
    doublePropertyManager->setValue(rollProp, 0);
    doublePropertyManager->setSingleStep(rollProp, SINGLE_ANGLE_STEP);
    propertyToRoll[property] = rollProp;
    rollToProperty[rollProp] = property;
    property->addSubProperty(rollProp);
}

void DQuatPropertyManager::uninitializeProperty(QtProperty* property) {
    QtProperty* xProp = propertyToYaw[property];
    if (xProp) {
        yawToProperty.remove(xProp);
        delete xProp;
    }
    propertyToYaw.remove(property);

    QtProperty* yProp = propertyToPitch[property];
    if (yProp) {
        pitchToProperty.remove(yProp);
        delete yProp;
    }
    propertyToPitch.remove(property);

    QtProperty* zProp = propertyToRoll[property];
    if (zProp) {
        rollToProperty.remove(zProp);
        delete zProp;
    }
    propertyToRoll.remove(property);

    values.remove(property);
}

