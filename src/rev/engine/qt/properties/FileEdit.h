/*
 * FileEdit.h
 *
 *  Created on: 24-03-2013
 *      Author: Revers
 */

#ifndef FILEEDIT_H_
#define FILEEDIT_H_

#include <QLineEdit>

namespace rev {

	class FileEdit: public QWidget {
	Q_OBJECT

		QLineEdit* theLineEdit;
		QString theFilter;

	public:
		FileEdit(QWidget* parent = 0);

		void setFilePath(const QString& filePath) {
			if (theLineEdit->text() != filePath) {
				theLineEdit->setText(filePath);
			}
		}

		QString filePath() const {
			return theLineEdit->text();
		}

		void setFilter(const QString& filter) {
			theFilter = filter;
		}

		QString filter() const {
			return theFilter;
		}

	signals:
		void filePathChanged(const QString& filePath);

	protected:
		void focusInEvent(QFocusEvent* e);
		void focusOutEvent(QFocusEvent* e);
		void keyPressEvent(QKeyEvent* e);
		void keyReleaseEvent(QKeyEvent* e);

	private slots:
		void buttonClicked();
	};

} /* namespace rev */
#endif /* FILEEDIT_H_ */
