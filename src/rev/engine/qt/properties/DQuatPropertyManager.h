/*
 * DQuatPropertyManager.h
 *
 *  Created on: 21-01-2013
 *      Author: Revers
 */

#ifndef DQUATPROPERTYMANAGER_H_
#define DQUATPROPERTYMANAGER_H_

#include <QMap>
#include <qtpropertymanager.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace rev {
    class DQuatPropertyManager: public QtAbstractPropertyManager {
    Q_OBJECT

        bool changeListenerActive = true;

        struct Data {
            Data() :
                    decimals(2), val(0.0, 0.0, 0.0, 0.0) {
            }

            glm::dquat val;
            int decimals;
        };

        typedef QMap<const QtProperty*, Data> PropertyValueMap;
        PropertyValueMap values;

        QtDoublePropertyManager* doublePropertyManager;

        QMap<const QtProperty*, QtProperty*> propertyToYaw;
        QMap<const QtProperty*, QtProperty*> propertyToPitch;
        QMap<const QtProperty*, QtProperty*> propertyToRoll;

        QMap<const QtProperty*, QtProperty*> yawToProperty;
        QMap<const QtProperty*, QtProperty*> pitchToProperty;
        QMap<const QtProperty*, QtProperty*> rollToProperty;

    public:
        DQuatPropertyManager(QObject* parent = 0);
        ~DQuatPropertyManager();

        QtDoublePropertyManager* subDoublePropertyManager() const;

        glm::dquat value(const QtProperty* property) const;
        int decimals(const QtProperty* property) const;

    public slots:
        void setValue(QtProperty* property, const glm::dquat& val);
        void setDecimals(QtProperty* property, int prec);

    private slots:
        void slotDoubleChanged(QtProperty* property, double value);
        void slotPropertyDestroyed(QtProperty* property);

    signals:
        void valueChanged(QtProperty* property, const glm::dquat& val);
        void decimalsChanged(QtProperty* property, int prec);

    protected:
        QString valueText(const QtProperty* property) const;
        virtual void initializeProperty(QtProperty* property);
        virtual void uninitializeProperty(QtProperty* property);
    };
} // namespace rev
#endif /* DQUATPROPERTYMANAGER_H_ */
