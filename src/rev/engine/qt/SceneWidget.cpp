/*
 * SceneWidget.cpp
 *
 *  Created on: 16 lip 2013
 *      Author: Revers
 */

#include <QColorDialog>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include "GameEditorWindow.h"
#include "SceneWidget.h"

namespace rev {

SceneWidget::SceneWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);

	setupActions();
}

SceneWidget::~SceneWidget() {
}

void SceneWidget::setupActions() {
	connect(uiWidget.backgroundPB, SIGNAL(clicked()), this, SLOT(backgroundColorAction()));
}

void SceneWidget::backgroundColorAction() {
	QColor chosenColor = QColorDialog::getColor(backgroundColor);
	if (chosenColor.isValid()) {
		setButtonColor(chosenColor);
		backgroundColor = chosenColor;
		color3 color(float(chosenColor.red()) / 255.0, float(chosenColor.green()) / 255.0,
				float(chosenColor.blue()) / 255.0);

		Renderer3D::ref().setBackgroundColor(color);
		mainWindow->setSceneSaved(false);
	}
}

void SceneWidget::setSceneName(const char* name) {
	uiWidget.nameL->setText(name);
}

void SceneWidget::setButtonColor(const QColor& color) {
	const int THRESHOLD = 105;
	int BackgroundDelta = (color.red() * 0.299) + (color.green() * 0.587) + (color.blue() * 0.114);
	QColor textColor = QColor((255 - BackgroundDelta < THRESHOLD) ? Qt::black : Qt::white);

	const QString COLOR_STYLE("QPushButton { background-color : %1; color : %2; }");
	uiWidget.backgroundPB->setStyleSheet(COLOR_STYLE.arg(color.name()).arg(textColor.name()));
}

} /* namespace rev */
