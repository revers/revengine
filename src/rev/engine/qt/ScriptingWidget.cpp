/*
 * ScriptingWidget.cpp
 *
 *  Created on: 28-03-2013
 *      Author: Revers
 */

#include <cstdlib>
#include <sstream>
#include <QDir>
#include <QFile>
#include <QMenu>
#include <QTimer>
#include <QMessageBox>
#include <QAction>
#include <QSettings>
#include <QFileDialog>
#include <rev/common/RevConfigReader.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include "tables/ScriptingTableModel.h"
#include "tables/StatusColumnDelegate.h"
#include "tables/ActiveColumnDelegate.h"
#include "tables/PathColumnDelegate.h"
#include "GameEditorWindow.h"
#include "ScriptingWidget.h"

#define DEFAULT_SCRIPT_DIR_KEY "dsdk"
#define STATISTICS_UPDATE_TIME 500

using namespace rev;
using namespace std;

ScriptingWidget::ScriptingWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);
	scriptingModel = new ScriptingTableModel(this);
	uiWidget.scriptingTable->setModel(scriptingModel);

	StatusColumnDelegate* statusDelegate = new StatusColumnDelegate(uiWidget.scriptingTable);
	uiWidget.scriptingTable->setItemDelegateForColumn(SCRIPTING_COLUMN_STATUS, statusDelegate);

	ActiveColumnDelegate* playButtonDelegate = new ActiveColumnDelegate(uiWidget.scriptingTable);
	uiWidget.scriptingTable->setItemDelegateForColumn(SCRIPTING_COLUMN_ACTIVE, playButtonDelegate);

	PathColumnDelegate* pathDelegate = new PathColumnDelegate(uiWidget.scriptingTable);
	uiWidget.scriptingTable->setItemDelegateForColumn(SCRIPTING_COLUMN_PATH, pathDelegate);

	uiWidget.scriptingTable->setColumnWidth(SCRIPTING_COLUMN_ACTIVE, 50);
	uiWidget.scriptingTable->setColumnWidth(SCRIPTING_COLUMN_STATUS, 50);
	uiWidget.scriptingTable->setColumnWidth(SCRIPTING_COLUMN_ID, 50);
	uiWidget.scriptingTable->setColumnWidth(SCRIPTING_COLUMN_NAME, 150);

	initTableMenu();
	updateSelectedCount();
	setupActions();

	QTimer* timer = new QTimer(this);
	QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateStatisticsTimerAction()));

	timer->start(STATISTICS_UPDATE_TIME);
}

ScriptingWidget::~ScriptingWidget() {
}

void ScriptingWidget::updateStatisticsTimerAction() {
	EventManager::ref().fireScriptStatisticsChangedEvent();
}

void ScriptingWidget::initTableMenu() {
	uiWidget.scriptingTable->setContextMenuPolicy(Qt::CustomContextMenu);
	tableMenu = new QMenu(this);

	activateMenuAction = tableMenu->addAction("Activate");
	QIcon activateIcon;
	activateIcon.addFile(QString::fromUtf8(":/images/play.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	activateMenuAction->setIcon(activateIcon);

	pauseMenuAction = tableMenu->addAction("Pause");
	QIcon pauseIcon;
	pauseIcon.addFile(QString::fromUtf8(":/images/pause.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	pauseMenuAction->setIcon(pauseIcon);

	recompileMenuAction = tableMenu->addAction("Recompile");
	QIcon recompileIcon;
	recompileIcon.addFile(QString::fromUtf8(":/images/refresh.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	recompileMenuAction->setIcon(recompileIcon);

	editMenuAction = tableMenu->addAction("Open in editor...");
	QIcon editIcon;
	editIcon.addFile(QString::fromUtf8(":/images/edit-green.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	editMenuAction->setIcon(editIcon);

	openMenuAction = tableMenu->addAction("Open/Load JS file...");
	QIcon openIcon;
	openIcon.addFile(QString::fromUtf8(":/images/open-green.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	openMenuAction->setIcon(openIcon);

	pickMenuAction = tableMenu->addAction("Pick");
	QIcon pickIcon;
	pickIcon.addFile(QString::fromUtf8(":/images/hand-green.png"), QSize(), QIcon::Normal,
			QIcon::Off);
	pickMenuAction->setIcon(pickIcon);
}

void ScriptingWidget::setupActions() {
	connect(uiWidget.pauseScriptingB, SIGNAL(toggled(bool)),
			this, SLOT(pauseScriptingSelected(bool)));

	connect(uiWidget.scriptingTable->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
			this, SLOT(selectionChanged(const QItemSelection&, const QItemSelection&)));

	connect(uiWidget.scriptingTable, SIGNAL(doubleClicked(const QModelIndex&)),
			this, SLOT(tableDoubleClicked(const QModelIndex&)));

	connect(uiWidget.activateToolB, SIGNAL(clicked()), this, SLOT(activateSelectedAction()));
	connect(uiWidget.pauseToolB, SIGNAL(clicked()), this, SLOT(pauseSelectedAction()));
	connect(uiWidget.recompileToolB, SIGNAL(clicked()), this, SLOT(recompileSelectedAction()));
	connect(uiWidget.pickToolB, SIGNAL(clicked()), this, SLOT(pickSelectedAction()));
	connect(uiWidget.openToolB, SIGNAL(clicked()), this, SLOT(openSelectedAction()));
	connect(uiWidget.editToolB, SIGNAL(clicked()), this, SLOT(editSelectedAction()));
	connect(uiWidget.scriptingTable, SIGNAL(customContextMenuRequested(QPoint)),
			this, SLOT(displayTableMenu(QPoint)));
}

void ScriptingWidget::displayTableMenu(const QPoint& pos) {
	QAction* a = tableMenu->exec(uiWidget.scriptingTable->viewport()->mapToGlobal(pos));
	if (a == activateMenuAction) {
		activateSelectedAction();
	} else if (a == pauseMenuAction) {
		pauseSelectedAction();
	} else if (a == recompileMenuAction) {
		recompileSelectedAction();
	} else if (a == pickMenuAction) {
		pickSelectedAction();
	} else if (a == openMenuAction) {
		openSelectedAction();
	} else if (a == editMenuAction) {
		editSelectedAction();
	}
}

void ScriptingWidget::refreshEngineControls() {
	uiWidget.pauseScriptingB->setChecked(rev::ScriptingManager::ref().isPaused());
}

void ScriptingWidget::refreshTable() {
	uiWidget.scriptingTable->viewport()->repaint();
}

void ScriptingWidget::refreshStatistics() {
	ScriptingManager& mgr = ScriptingManager::ref();
	uiWidget.totalL->setText(QString::number(mgr.getScriptCount()));
	uiWidget.activeL->setText(QString::number(mgr.getActiveCount()));
	uiWidget.compiledL->setText(QString::number(mgr.getCompiledCount()));
	uiWidget.invalidL->setText(QString::number(mgr.getInvalidCount()));
	uiWidget.intervalsL->setText(QString::number(mgr.getIntervalCount()));
	uiWidget.timeoutsL->setText(QString::number(mgr.getTimeoutCount()));
}

void ScriptingWidget::pauseScriptingSelected(bool checked) {
	rev::ScriptingManager::ref().setPaused(checked);
}

void ScriptingWidget::tableDoubleClicked(const QModelIndex& index) {
	editSelectedAction();
}

void ScriptingWidget::activateSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts([] (ScriptingComponent* comp) {
		comp->setActive(true);
	});
}

void ScriptingWidget::pauseSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts([] (ScriptingComponent* comp) {
		comp->setActive(false);
	});
}

void ScriptingWidget::recompileSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts([] (ScriptingComponent* comp) {
		comp->compile();
	});
}

void ScriptingWidget::pickSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts([] (ScriptingComponent* comp) {
		if (ScriptingManager::ref().isMainScript(comp)) {
			return;
		}
		Renderer3D::ref().getPicker().pickGameObject(comp->getParent());
	});
}

void ScriptingWidget::editSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts([] (ScriptingComponent* comp) {
		std::string cmd = ConfigReader::getInstance().getString("open.script.command", "");
		if (cmd == "") {
			QMessageBox::critical(GameEditorWindow::getInstance(), "ERROR",
					"\"open.script.command\" property is not set!!");
			return;
		}
		std::string path = ResourcePath::get(comp->getScriptFile());
		QString qCmd(cmd.c_str());
		system(qCmd.arg(path.c_str()).toUtf8().data());
	});
}

void ScriptingWidget::openSelectedAction() {
	REV_TRACE_FUNCTION;
	applyForSelectedScripts(
			[] (ScriptingComponent* comp) {
				QSettings mySettings;
				std::string dir;
				if (!comp->isScriptEmpty()) {
					dir = ResourcePath::get(comp->getScriptFile(), false);
				}

				QString filename = QFileDialog::getOpenFileName(
						GameEditorWindow::getInstance(),
						"Open Script",
						!comp->isScriptEmpty() ? dir.c_str() : mySettings.value(DEFAULT_SCRIPT_DIR_KEY).toString(),
						"JavaScript files (*.js)");
				if (filename.isNull()) {
					return;
				}

				QDir currentDir;
				mySettings.setValue(DEFAULT_SCRIPT_DIR_KEY, currentDir.absoluteFilePath(filename));

				GameEditorWindow* gameEditor = GameEditorWindow::getInstance();
				gameEditor->setSceneSaved(false);

				std::string path = filename.toUtf8().data();
				std::string relativePath = ResourcePath::makeRelative(path);

				comp->setScriptFile(relativePath);
			});
}

void ScriptingWidget::addMainScript() {
	scriptingModel->addScriptingComponent(ScriptingManager::ref().getMainScriptingComponent());
}

void ScriptingWidget::addScriptingComponent(ScriptingComponent* component) {
	scriptingModel->addScriptingComponent(component);
}

void ScriptingWidget::removeScriptingComponent(ScriptingComponent* component) {
	scriptingModel->removeScriptingComponent(component);
}

void ScriptingWidget::removeAllScriptingComponents() {
	scriptingModel->removeAllScriptingComponents();
	addMainScript();
	selectedCount = 0;
}

void ScriptingWidget::addAllScriptingComponents() {
	ScriptingComponentVect& components = ScriptingManager::ref().getComponents();
	for (auto& c : components) {
		scriptingModel->addScriptingComponent(c);
	}
}

void ScriptingWidget::init() {
	addMainScript();
}

void ScriptingWidget::selectionChanged(const QItemSelection& selected,
		const QItemSelection& deselected) {
	updateSelectedCount();
}

void ScriptingWidget::updateSelectedCount() {
	QModelIndexList selectedList = uiWidget.scriptingTable->selectionModel()->selectedRows();
	selectedCount = selectedList.count();
	uiWidget.selectedL->setText(QString::number(selectedCount));
	if (selectedCount == 1) {
		uiWidget.editToolB->setEnabled(true);
		uiWidget.openToolB->setEnabled(true);
		uiWidget.pickToolB->setEnabled(true);
		editMenuAction->setEnabled(true);
		openMenuAction->setEnabled(true);
		pickMenuAction->setEnabled(true);
	} else {
		uiWidget.editToolB->setEnabled(false);
		uiWidget.openToolB->setEnabled(false);
		uiWidget.pickToolB->setEnabled(false);
		editMenuAction->setEnabled(false);
		openMenuAction->setEnabled(false);
		pickMenuAction->setEnabled(false);
	}

	if (selectedCount > 0) {
		uiWidget.recompileToolB->setEnabled(true);
		uiWidget.activateToolB->setEnabled(true);
		uiWidget.pauseToolB->setEnabled(true);
		recompileMenuAction->setEnabled(true);
		activateMenuAction->setEnabled(true);
		pauseMenuAction->setEnabled(true);
	} else {
		uiWidget.recompileToolB->setEnabled(false);
		uiWidget.activateToolB->setEnabled(false);
		uiWidget.pauseToolB->setEnabled(false);
		recompileMenuAction->setEnabled(false);
		activateMenuAction->setEnabled(false);
		pauseMenuAction->setEnabled(false);
	}
}

void ScriptingWidget::applyForSelectedScripts(std::function<void(ScriptingComponent*)> callback,
		bool refresh) {
	REV_TRACE_FUNCTION;

	QModelIndexList selectedList = uiWidget.scriptingTable->selectionModel()->selectedRows();
	int count = selectedList.count();
	for (int i = 0; i < count; i++) {
		QModelIndex index = selectedList.at(i);
		void* ptr = index.data(Qt::UserRole).value<void*>();
		ScriptingComponent* component = static_cast<ScriptingComponent*>(ptr);
		callback(component);
	}

	if (refresh) {
		refreshTable();
	}
}
