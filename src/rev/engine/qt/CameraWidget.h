/*
 * CameraWidget.h
 *
 *  Created on: 07-04-2013
 *      Author: Revers
 */

#ifndef CAMERAWIDGET_H_
#define CAMERAWIDGET_H_

#include <QWidget>
#include <QTimer>
#include "ui_CameraWidget.h"

class QtTreePropertyBrowser;

namespace rev {

class GameEditorWindow;
class ICamera;
class QtPropertiesBinder;

class CameraWidget: public QWidget {
Q_OBJECT

	GameEditorWindow* mainWindow;
	Ui::CameraWidget uiWidget;
	QtTreePropertyBrowser* freeCameraTree = nullptr;
	QtTreePropertyBrowser* sphericalCameraTree = nullptr;
	rev::QtPropertiesBinder* freeCameraBinder = nullptr;
	rev::QtPropertiesBinder* sphericalCameraBinder = nullptr;

public:
	CameraWidget(GameEditorWindow* mainWindow);
	virtual ~CameraWidget();

	void init();
	void refresh();
	void updateCameraButtons();
	int getCameraContextIndex();

private slots:
	void contextCBChanged(int index);
	void freeCameraRBChecked(bool checked);
	void sphericalCameraRBChecked(bool checked);
	void sameAsContext0Pressed();
	void sameAsContext1Pressed();
	void sameAsContext2Pressed();

private:
	void setupActions();
	void setWidgetsEnabled();
	void changeCamera();
	bool isFreeCameraUsed();
	bool isSphericalCameraUsed();
	void setSameCamera(int srcIndex, int dstIndex);
};

} /* namespace rev */
#endif /* CAMERAWIDGET_H_ */
