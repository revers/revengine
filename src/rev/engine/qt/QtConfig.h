/*
 * QtConfig.h
 *
 *  Created on: 23 lip 2013
 *      Author: Revers
 */

#ifndef QTCONFIG_H_
#define QTCONFIG_H_

/**
 * Single step for spin boxes.
 */
#define SINGLE_STEP 0.05

/**
 * Single step for angles (in degrees).
 */
#define SINGLE_ANGLE_STEP 1.0


#endif /* QTCONFIG_H_ */
