/*
 * QtPropertiesBinder.h
 *
 *  Created on: 19-01-2013
 *      Author: Revers
 */

#ifndef QTPROPERTIESBINDER_H_
#define QTPROPERTIESBINDER_H_

#include <functional>
#include <QObject>
#include <QMap>
#include <QtProperty>

#include <glm/glm.hpp>
#include <rev/engine/binding/RevIBindable.h>

class QtTreePropertyBrowser;
class QtGroupPropertyManager;
class QtIntPropertyManager;
class QtBoolPropertyManager;
class QtDoublePropertyManager;
class QtColorPropertyManager;
class QtStringPropertyManager;

namespace rev {
	class Vec2PropertyManager;
	class Vec3PropertyManager;
	class Vec4PropertyManager;
	class Mat2PropertyManager;
	class Mat3PropertyManager;
	class Mat4PropertyManager;
	class QuatPropertyManager;

	class DVec2PropertyManager;
	class DVec3PropertyManager;
	class DVec4PropertyManager;
	class DMat2PropertyManager;
	class DMat3PropertyManager;
	class DMat4PropertyManager;
	class DQuatPropertyManager;

	class FilePathManager;
}

namespace rev {

	/**
	 *  function void changeListener(void* property);
	 */
	typedef std::function<void(void*)> PropertyChangeListener;

	class QtPropertiesBinder: public QObject, public IBinder {
	Q_OBJECT

		typedef QMap<IBindable*, QtProperty*> BinderGroupMap;
		typedef BinderGroupMap::iterator BinderGroupMapIter;

		QtTreePropertyBrowser* propertyBrowser = nullptr;
		QtGroupPropertyManager* groupManager = nullptr;
		QtIntPropertyManager* intManager = nullptr;
		QtBoolPropertyManager* boolManager = nullptr;
		QtDoublePropertyManager* doubleManager = nullptr;
		QtColorPropertyManager* colorManager = nullptr;
		Vec2PropertyManager* vec2Manager = nullptr;
		Vec3PropertyManager* vec3Manager = nullptr;
		Vec4PropertyManager* vec4Manager = nullptr;
		Mat2PropertyManager* mat2Manager = nullptr;
		Mat3PropertyManager* mat3Manager = nullptr;
		Mat4PropertyManager* mat4Manager = nullptr;
		QuatPropertyManager* quatManager = nullptr;

		DVec2PropertyManager* dvec2Manager = nullptr;
		DVec3PropertyManager* dvec3Manager = nullptr;
		DVec4PropertyManager* dvec4Manager = nullptr;
		DMat2PropertyManager* dmat2Manager = nullptr;
		DMat3PropertyManager* dmat3Manager = nullptr;
		DMat4PropertyManager* dmat4Manager = nullptr;
		DQuatPropertyManager* dquatManager = nullptr;

		QtStringPropertyManager* stringManager = nullptr;
		FilePathManager* filepathManager = nullptr;

		QMap<QtProperty*, int*> intPointersMap;
		QMap<QtProperty*, bool*> boolPointersMap;
		QMap<QtProperty*, float*> floatPointersMap;
		QMap<QtProperty*, double*> doublePointersMap;
		QMap<QtProperty*, rev::color3*> color3PointersMap;
		QMap<QtProperty*, rev::color4*> color4PointersMap;
		QMap<QtProperty*, glm::vec2*> vec2PointersMap;
		QMap<QtProperty*, glm::vec3*> vec3PointersMap;
		QMap<QtProperty*, glm::vec4*> vec4PointersMap;
		QMap<QtProperty*, glm::mat2*> mat2PointersMap;
		QMap<QtProperty*, glm::mat3*> mat3PointersMap;
		QMap<QtProperty*, glm::mat4*> mat4PointersMap;
		QMap<QtProperty*, glm::quat*> quatPointersMap;

		QMap<QtProperty*, glm::dvec2*> dvec2PointersMap;
		QMap<QtProperty*, glm::dvec3*> dvec3PointersMap;
		QMap<QtProperty*, glm::dvec4*> dvec4PointersMap;
		QMap<QtProperty*, glm::dmat2*> dmat2PointersMap;
		QMap<QtProperty*, glm::dmat3*> dmat3PointersMap;
		QMap<QtProperty*, glm::dmat4*> dmat4PointersMap;
		QMap<QtProperty*, glm::dquat*> dquatPointersMap;
		QMap<QtProperty*, std::string*> stringPointersMap;
		QMap<QtProperty*, std::string*> filepathPointersMap;
		QMap<QtProperty*, IBindable*> propertyBindableMap;

		BinderGroupMap groupsMap;
		bool canChangeValue = true;

		PropertyChangeListener changeListener;

	public:

		QtPropertiesBinder(QtTreePropertyBrowser* propertyBrowser, QObject* parent = 0);

		virtual ~QtPropertiesBinder();

		void bindObject(IBindable* bindable) override;

		void unbindObject(IBindable* bindable) override;

		void clearAll();

		void setPropertyChangeListener(PropertyChangeListener changeListener) {
			this->changeListener = changeListener;
		}

		/**
		 * IBindable
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<IBindable*> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * BasicTypeListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name, BasicTypeListMemberWrapper wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * BindableListMemberWrapper
		 */
		void bindList(IBindable* bindable, const char* name, BindableListMemberWrapper wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * int
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<int> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * float
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<float> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * double
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<double> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * bool
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<bool> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * std::string
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<std::string> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * filepath (std::string)
		 */
		virtual void bindFilepath(IBindable* bindable, const char* name,
				MemberWrapper<std::string> wrapper, const char* fileFilter,
				bool editable, bool visible) override;

		/**
		 * vec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec2> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * vec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec3> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * vec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::vec4> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * mat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat2> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * mat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat3> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * mat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::mat4> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * quat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::quat> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dvec2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec2> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dvec3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec3> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dvec4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dvec4> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dmat2
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat2> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dmat3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat3> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dmat4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dmat4> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * dquat
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<glm::dquat> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * color3
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color3> wrapper,
				bool editable, bool visible, bool expanded) override;

		/**
		 * color4
		 */
		void bind(IBindable* bindable, const char* name, MemberWrapper<rev::color4> wrapper,
				bool editable, bool visible, bool expanded) override;

		void refreshAll();

		/**
		 * int
		 */
		void bindInfo(IBindable* bindable, const char* name,
				int& value) override;

		/**
		 * float
		 */
		void bindInfo(IBindable* bindable, const char* name,
				float& value) override;

		/**
		 * double
		 */
		void bindInfo(IBindable* bindable, const char* name,
				double& value) override;

		/**
		 * bool
		 */
		void bindInfo(IBindable* bindable, const char* name,
				bool& value) override;

		/**
		 * std::string
		 */
		void bindInfo(IBindable* bindable, const char* name,
				std::string& value) override;

		/**
		 * vec2
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::vec2& value) override;

		/**
		 * vec3
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::vec3& value) override;

		/**
		 * vec4
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::vec4& value) override;

		/**
		 * mat2
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::mat2& value) override;

		/**
		 * mat3
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::mat3& value) override;

		/**
		 * mat4
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::mat4& value) override;

		/**
		 * quat
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::quat& value) override;

		/**
		 * dvec2
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dvec2& value) override;

		/**
		 * dvec3
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dvec3& value) override;

		/**
		 * dvec4
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dvec4& value) override;

		/**
		 * dmat2
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dmat2& value) override;

		/**
		 * dmat3
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dmat3& value) override;

		/**
		 * dmat4
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dmat4& value) override;

		/**
		 * dquat
		 */
		void bindInfo(IBindable* bindable, const char* name,
				glm::dquat& value) override;

		/**
		 * color3
		 */
		void bindInfo(IBindable* bindable, const char* name,
				rev::color3& value) override;

		/**
		 * color4
		 */
		void bindInfo(IBindable* bindable, const char* name,
				rev::color4& value) override;

	private:
		QtProperty* getGroupProperty(IBindable* bindable);
		void setExpanend(QtProperty* property, bool expanded);

		QColor toQColor(const rev::color3& color) {
			return QColor(color.r * 255.0f, color.g * 255.0f,
					color.b * 255.0f, 255.0f);
		}

		QColor toQColor(const rev::color4& color) {
			return QColor(color.r * 255.0f, color.g * 255.0f,
					color.b * 255.0f, color.a * 255.0f);
		}

		rev::color3 toGLColor3(const QColor& color) {
			return rev::color3(float(color.red()) / 255.0f,
					float(color.green()) / 255.0f,
					float(color.blue()) / 255.0f);
		}

		rev::color4 toGLColor4(const QColor& color) {
			return rev::color4(float(color.red()) / 255.0f,
					float(color.green()) / 255.0f,
					float(color.blue()) / 255.0f,
					float(color.alpha()) / 255.0f);
		}

		void lockValueChangeListeners() {
			canChangeValue = false;
		}

		void unlockValueChangeListeners() {
			canChangeValue = true;
		}

	private slots:
		void valueChanged(QtProperty* property, int value);
		void valueChanged(QtProperty* property, bool value);
		void valueChanged(QtProperty* property, double value);
		void valueChanged(QtProperty* property, const QColor& value);
		void valueChanged(QtProperty* property, const glm::vec2& value);
		void valueChanged(QtProperty* property, const glm::vec3& value);
		void valueChanged(QtProperty* property, const glm::vec4& value);
		void valueChanged(QtProperty* property, const glm::mat2& value);
		void valueChanged(QtProperty* property, const glm::mat3& value);
		void valueChanged(QtProperty* property, const glm::mat4& value);
		void valueChanged(QtProperty* property, const glm::quat& value);

		void valueChanged(QtProperty* property, const glm::dvec2& value);
		void valueChanged(QtProperty* property, const glm::dvec3& value);
		void valueChanged(QtProperty* property, const glm::dvec4& value);
		void valueChanged(QtProperty* property, const glm::dmat2& value);
		void valueChanged(QtProperty* property, const glm::dmat3& value);
		void valueChanged(QtProperty* property, const glm::dmat4& value);
		void valueChanged(QtProperty* property, const glm::dquat& value);

		void valueChanged(QtProperty* property, const QString& value);
		void filepathValueChanged(QtProperty* property, const QString& value);

	private:
		void bindVal(IBindable* bindable, const char* name, IBindable* value, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, int& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, float& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, double& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, bool& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, std::string& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::vec2& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::vec3& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::vec4& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::mat2& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::mat3& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::mat4& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::quat& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dvec2& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dvec3& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dvec4& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dmat2& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dmat3& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dmat4& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, glm::dquat& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, rev::color3& val, bool editable,
				bool visible, bool expanded);
		void bindVal(IBindable* bindable, const char* name, rev::color4& val, bool editable,
				bool visible, bool expanded);
	};

}
/* namespace rev */
#endif /* QTPROPERTIESBINDER_H_ */
