/*
 * NewObjectWidget.cpp
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#include <algorithm>
#include <sstream>
#include <rev/common/RevStringUtil.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/binding/RevBindableFactory.h>
#include <rev/engine/binding/RevSaveStateBinder.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include <rev/engine/components/RevCollisionComponent.h>
#include <rev/engine/components/RevShadowSourceComponent.h>
#include <rev/engine/components/RevRigidBodyComponent.h>
#include <rev/engine/components/light/RevILightComponent.h>
#include <rev/engine/effects/RevIEffect.h>
#include <rev/engine/drawables/RevIVBODrawable.h>
#include <rev/engine/events/RevEventManager.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/scripting/RevScriptingManager.h>
#include <rev/engine/collision/RevCollisionManager.h>
#include <rev/engine/physics/RevPhysicsManager.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/shadow/RevShadowManager.h>

#include "tables/GameObjectTableDelegate.h"
#include "tables/GameObjectTableModel.h"

#include "GameWidgetsController.h"
#include "GameEditorWindow.h"
#include "ScriptingWidget.h"
#include "NewObjectWidget.h"

#define EDIT_TITLE "Edit object"
#define NEW_TITLE "Add new object"
#define NONE_TXT "None"
#define INDEX_NONE 0
#define NEW_OBJECT_PREFIX "New object "

using namespace rev;

NewObjectWidget::NewObjectWidget(GameEditorWindow* mainWindow) :
		QWidget(mainWindow), mainWindow(mainWindow) {
	uiWidget.setupUi(this);

	scrollToSelected = new QTimer(this);
	scrollToSelected->setSingleShot(true);

	uiWidget.visualTypeCB->addItem(NONE_TXT);

	uiWidget.lightTypeCB->addItem(NONE_TXT);

	uiWidget.shadowTypeCB->addItem(NONE_TXT);
	uiWidget.shadowTypeCB->addItem(ShadowSourceComponent::staticGetClassName());

	uiWidget.physicsTypeCB->addItem(NONE_TXT);
	uiWidget.physicsTypeCB->addItem(RigidBodyComponent::staticGetClassName());

	uiWidget.scriptTypeCB->addItem(NONE_TXT);
	uiWidget.scriptTypeCB->addItem(ScriptingComponent::staticGetClassName());

	uiWidget.collisionTypeCB->addItem(NONE_TXT);
	uiWidget.collisionTypeCB->addItem(CollisionComponent::staticGetClassName());

	setupActions();
}

void NewObjectWidget::init() {
	REV_TRACE_FUNCTION;

	BindableStructMap& bindMap = rev::BindableFactory::ref().getBindableStructMap();
	for (auto& pair : bindMap) {
		const std::string& name = pair.first;
		const BindableStruct& bindStruct = pair.second;
		IBindable* bindable = bindStruct.factoryFunc();

		if (dynamic_cast<IEffect*>(bindable) != nullptr) {
			effectVector.push_back( { name, bindStruct.factoryFunc });
		} else if (dynamic_cast<IVBODrawable*>(bindable) != nullptr) {
			drawableVector.push_back( { name, bindStruct.factoryFunc });
		} else if (dynamic_cast<IVisual3DComponent*>(bindable) != nullptr) {
			visual3DVector.push_back( { name, bindStruct.factoryFunc });
		} else if (dynamic_cast<ILightComponent*>(bindable) != nullptr) {
			lightVector.push_back( { name, bindStruct.factoryFunc });
		}

		if (!bindable->isSingleton()) {
			delete bindable;
		}
	}

	auto comparator = [] (const NameFactoryStruct& a, const NameFactoryStruct& b) -> bool {
		return a.name < b.name;
	};

	std::sort(effectVector.begin(), effectVector.end(), comparator);
	for (auto& effect : effectVector) {
		uiWidget.effectCB->addItem(effect.name.c_str());
	}

	std::sort(drawableVector.begin(), drawableVector.end(), comparator);
	for (auto& drawable : drawableVector) {
		uiWidget.drawableCB->addItem(drawable.name.c_str());
	}

	std::sort(visual3DVector.begin(), visual3DVector.end(), comparator);
	for (auto& comp : visual3DVector) {
		uiWidget.visualTypeCB->addItem(comp.name.c_str());
	}

	std::sort(lightVector.begin(), lightVector.end(), comparator);
	for (auto& comp : lightVector) {
		uiWidget.lightTypeCB->addItem(comp.name.c_str());
	}
}

NewObjectWidget::~NewObjectWidget() {
	if (saveStateBinder) {
		delete saveStateBinder;
	}
}

void NewObjectWidget::setupActions() {
	connect(uiWidget.okButton, SIGNAL(clicked()), this, SLOT(okActionPerformed()));
	connect(uiWidget.cancelButton, SIGNAL(clicked()), this, SLOT(cancelActionPerformed()));

	connect(uiWidget.visualTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(visualTypeCBChanged(int)));
	connect(uiWidget.effectCB, SIGNAL(currentIndexChanged(int)), this, SLOT(effectCBChanged(int)));
	connect(uiWidget.drawableCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(drawableCBChanged(int)));
	connect(uiWidget.physicsTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(physicsTypeCBChanged(int)));
	connect(uiWidget.scriptTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(scriptTypeCBChanged(int)));
	connect(uiWidget.collisionTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(collisionTypeCBChanged(int)));
	connect(uiWidget.lightTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(lightTypeCBChanged(int)));
	connect(uiWidget.shadowTypeCB, SIGNAL(currentIndexChanged(int)), this,
	SLOT(shadowTypeCBChanged(int)));
	connect(scrollToSelected, SIGNAL(timeout()), this, SLOT(scrollToSelectedSlot()));
}

void NewObjectWidget::visualTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.visualTypeCB->currentText();
	IVisual3DComponent* component = nullptr;
	if (name == Simple3DComponent::staticGetClassName()) {
		component = createDefaultSimple3DComp();
	} else if (index != INDEX_NONE) {
		BindableFactoryFunc factoryFunc = getFactoryFunc(visual3DVector, name.toUtf8().data());
		revAssert(factoryFunc);
		component = (IVisual3DComponent*) factoryFunc();
		bool succ = component->initAfterDeserialization();
		revAssert(succ);
	}

	struct Visual3DStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		IVisual3DComponent* component;
	};

	Visual3DStruct* vs = new Visual3DStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changeVisual3D", [this,vs] () -> bool {

		vs->mainWindow->clearPropertiesPanel();

		if (vs->gameObject->getVisual3DComponent()) {
			rev::Renderer3D::ref().removeComponent(vs->gameObject->getVisual3DComponent());
			delete vs->gameObject->getVisual3DComponent();
		}
		vs->gameObject->setVisual3DComponent(vs->component);
		if (vs->component) {
			vs->component->setParent(vs->gameObject);
			rev::Renderer3D::ref().addComponent(vs->component);
		}

		vs->mainWindow->bindPropertiesPanel(vs->gameObject);
		vs->mainWindow->refreshPanels();
		loadVisual3DComponent();
		delete vs;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::effectCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.effectCB->currentText();
	BindableFactoryFunc factoryFunc = getFactoryFunc(effectVector, name.toUtf8().data());
	revAssert(factoryFunc);
	IVisual3DComponent* component = gameObject->getVisual3DComponent();
	Simple3DComponent* simpleComp = dynamic_cast<Simple3DComponent*>(component);

	struct EffectStruct {
		BindableFactoryFunc factoryFunc;
		Simple3DComponent* simpleComp;
		GameEditorWindow* mainWindow;
	};

	EffectStruct* es = new EffectStruct { factoryFunc, simpleComp, mainWindow };

	EventManager::ref().fireEngineEvent("NewObjectWidget,changeEffect", [es] () -> bool {
		es->mainWindow->clearPropertiesPanel();

		IEffect* effect = (IEffect*) es->factoryFunc();
		bool succ = effect->initAfterDeserialization();
		revAssert(succ);
		es->simpleComp->setEffect(effect);

		es->mainWindow->bindPropertiesPanel(es->simpleComp->getParent());
		es->mainWindow->refreshPanels();

		delete es;
		return succ;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::drawableCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.drawableCB->currentText();
	BindableFactoryFunc factoryFunc = getFactoryFunc(drawableVector, name.toUtf8().data());
	revAssert(factoryFunc);
	IVisual3DComponent* component = gameObject->getVisual3DComponent();
	Simple3DComponent* simpleComp = dynamic_cast<Simple3DComponent*>(component);

	struct DrawableStruct {
		BindableFactoryFunc factoryFunc;
		Simple3DComponent* simpleComp;
		GameEditorWindow* mainWindow;
	};

	DrawableStruct* ds = new DrawableStruct { factoryFunc, simpleComp, mainWindow };

	EventManager::ref().fireEngineEvent("NewObjectWidget,changeDrawable", [ds] () -> bool {

		ds->mainWindow->clearPropertiesPanel();

		IVBODrawable* drawable = (IVBODrawable*) ds->factoryFunc();
		bool succ = drawable->initAfterDeserialization();
		revAssert(succ);
		ds->simpleComp->setDrawable(drawable);

		ds->mainWindow->bindPropertiesPanel(ds->simpleComp->getParent());
		ds->mainWindow->refreshPanels();

		delete ds;
		return succ;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::shadowTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.shadowTypeCB->currentText();

	ShadowSourceComponent* component;
	if (name != NONE_TXT) {
		component = new ShadowSourceComponent();
	} else {
		component = nullptr;
	}

	struct ShadowStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		ShadowSourceComponent* component;
	};

	ShadowStruct* s = new ShadowStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changeShadow", [s] () -> bool {

		s->mainWindow->clearPropertiesPanel();

		if (s->gameObject->getShadowComponent()) {
			rev::ShadowManager::ref().removeComponent(s->gameObject->getShadowComponent());
			delete s->gameObject->getShadowComponent();
		}
		s->gameObject->setShadowComponent(s->component);
		if (s->component) {
			s->component->setParent(s->gameObject);
			rev::ShadowManager::ref().addComponent(s->component);
		}

		s->mainWindow->bindPropertiesPanel(s->gameObject);
		s->mainWindow->refreshPanels();
		delete s;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::physicsTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.physicsTypeCB->currentText();

	RigidBodyComponent* component;
	if (name != NONE_TXT) {
		component = new RigidBodyComponent();
	} else {
		component = nullptr;
	}

	struct PhysicsStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		RigidBodyComponent* component;
	};

	PhysicsStruct* ps = new PhysicsStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changePhysics", [ps] () -> bool {

		ps->mainWindow->clearPropertiesPanel();

		if (ps->gameObject->getPhysicsComponent()) {
			rev::PhysicsManager::ref().removeComponent(ps->gameObject->getPhysicsComponent());
			delete ps->gameObject->getPhysicsComponent();
		}
		ps->gameObject->setPhysicsComponent(ps->component);
		if (ps->component) {
			ps->component->setParent(ps->gameObject);
			rev::PhysicsManager::ref().addComponent(ps->component);
		}

		ps->mainWindow->bindPropertiesPanel(ps->gameObject);
		ps->mainWindow->refreshPanels();
		delete ps;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::collisionTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.collisionTypeCB->currentText();

	CollisionComponent* component;
	if (name != NONE_TXT) {
		component = new CollisionComponent();
	} else {
		component = nullptr;
	}

	struct CollisionStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		CollisionComponent* component;
	};

	CollisionStruct* cs = new CollisionStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changeCollision", [cs] () -> bool {

		cs->mainWindow->clearPropertiesPanel();

		if (cs->gameObject->getCollisionComponent()) {
			rev::CollisionManager::ref().removeComponent(cs->gameObject->getCollisionComponent());
			delete cs->gameObject->getCollisionComponent();
		}
		cs->gameObject->setCollisionComponent(cs->component);
		if (cs->component) {
			cs->component->setParent(cs->gameObject);
			rev::CollisionManager::ref().addComponent(cs->component);
		}

		cs->mainWindow->bindPropertiesPanel(cs->gameObject);
		cs->mainWindow->refreshPanels();
		delete cs;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::scriptTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.scriptTypeCB->currentText();

	ScriptingComponent* component;
	if (name != NONE_TXT) {
		component = new ScriptingComponent();
	} else {
		component = nullptr;
	}

	struct ScriptingStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		ScriptingComponent* component;
	};

	ScriptingStruct* ss = new ScriptingStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changeScripting", [ss] () -> bool {

		ScriptingWidget* scriptingWidget = ss->mainWindow->getScriptingWidget();

		ss->mainWindow->clearPropertiesPanel();
		scriptingWidget->removeAllScriptingComponents();

		if (ss->gameObject->getScriptingComponent()) {
			rev::ScriptingManager::ref().removeComponent(ss->gameObject->getScriptingComponent());
			delete ss->gameObject->getScriptingComponent();
		}
		ss->gameObject->setScriptingComponent(ss->component);
		if (ss->component) {
			ss->component->setParent(ss->gameObject);
			rev::ScriptingManager::ref().addComponent(ss->component);
		}

		scriptingWidget->addAllScriptingComponents();
		ss->mainWindow->bindPropertiesPanel(ss->gameObject);
		ss->mainWindow->refreshPanels();
		delete ss;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::lightTypeCBChanged(int index) {
	if (!canHandleCBEvents) {
		return;
	}
	REV_TRACE_FUNCTION;

	QString name = uiWidget.lightTypeCB->currentText();
	ILightComponent* component = nullptr;
	if (index != INDEX_NONE) {
		BindableFactoryFunc factoryFunc = getFactoryFunc(lightVector, name.toUtf8().data());
		revAssert(factoryFunc);
		component = (ILightComponent*) factoryFunc();
		bool succ = component->initAfterDeserialization();
		revAssert(succ);
	}

	struct LightStruct {
		GameEditorWindow* mainWindow;
		GameObject* gameObject;
		ILightComponent* component;
	};

	LightStruct* s = new LightStruct { mainWindow, gameObject, component };
	EventManager::ref().fireEngineEvent("NewObjectWidget,changeLight", [this,s] () -> bool {

		s->mainWindow->clearPropertiesPanel();

		if (s->gameObject->getLightComponent()) {
			rev::LightingManager::ref().removeComponent(s->gameObject->getLightComponent());
			delete s->gameObject->getLightComponent();
		}
		s->gameObject->setLightComponent(s->component);
		if (s->component) {
			s->component->setParent(s->gameObject);
			rev::LightingManager::ref().addComponent(s->component);
		}

		s->mainWindow->bindPropertiesPanel(s->gameObject);
		s->mainWindow->refreshPanels();
		loadLightComponent();
		delete s;
		return true;
	});

	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::setTitle(const char* title) {
	uiWidget.titleLabel->setText(title);
}

int NewObjectWidget::getIndexOf(const NameFactoryVector& vec, IBindable* bindable) {
	int index = 0;
	for (auto& val : vec) {
		if (val.name == bindable->getBindableName()) {
			return index;
		}
		index++;
	}
	return -1;
}

BindableFactoryFunc NewObjectWidget::getFactoryFunc(const NameFactoryVector& vec,
		const char* name) {
	REV_DEBUG_MSG("Trying to find bindable: " << name);
	for (auto& val : vec) {
		if (val.name == name) {
			return val.factoryFunc;
		}
	}
	REV_DEBUG_MSG("Not found :(");
	return nullptr;
}

void NewObjectWidget::loadLightComponent() {
	REV_TRACE_FUNCTION;
	ILightComponent* component = gameObject->getLightComponent();
	if (!component) {
		canHandleCBEvents = false;
		uiWidget.lightTypeCB->setCurrentIndex(INDEX_NONE);
		canHandleCBEvents = true;
		return;
	}

	int index = getIndexOf(lightVector, component);
	revAssert(index >= 0);
	index++;
	canHandleCBEvents = false;
	uiWidget.lightTypeCB->setCurrentIndex(index);
	canHandleCBEvents = true;
}

void NewObjectWidget::loadScriptingComponent() {
	if (!gameObject->getScriptingComponent()) {
		canHandleCBEvents = false;
		uiWidget.scriptTypeCB->setCurrentIndex(INDEX_NONE);
		canHandleCBEvents = true;
	} else {
		canHandleCBEvents = false;
		uiWidget.scriptTypeCB->setCurrentIndex(1);
		canHandleCBEvents = true;
	}
}

void NewObjectWidget::loadPhysicsComponent() {
	if (!gameObject->getPhysicsComponent()) {
		canHandleCBEvents = false;
		uiWidget.physicsTypeCB->setCurrentIndex(INDEX_NONE);
		canHandleCBEvents = true;
	} else {
		canHandleCBEvents = false;
		uiWidget.physicsTypeCB->setCurrentIndex(1);
		canHandleCBEvents = true;
	}
}

void NewObjectWidget::loadShadowComponent() {
	if (!gameObject->getShadowComponent()) {
		canHandleCBEvents = false;
		uiWidget.physicsTypeCB->setCurrentIndex(INDEX_NONE);
		canHandleCBEvents = true;
	} else {
		canHandleCBEvents = false;
		uiWidget.physicsTypeCB->setCurrentIndex(1);
		canHandleCBEvents = true;
	}
}

void NewObjectWidget::loadCollisionComponent() {
	if (!gameObject->getCollisionComponent()) {
		canHandleCBEvents = false;
		uiWidget.collisionTypeCB->setCurrentIndex(INDEX_NONE);
		canHandleCBEvents = true;
	} else {
		canHandleCBEvents = false;
		uiWidget.collisionTypeCB->setCurrentIndex(1);
		canHandleCBEvents = true;
	}
}

void NewObjectWidget::loadVisual3DComponent() {
	REV_TRACE_FUNCTION;
	IVisual3DComponent* component = gameObject->getVisual3DComponent();
	if (!component) {
		canHandleCBEvents = false;
		uiWidget.visualTypeCB->setCurrentIndex(INDEX_NONE);
		uiWidget.effectCB->setEnabled(false);
		uiWidget.drawableCB->setEnabled(false);
		canHandleCBEvents = true;
		return;
	}

	Simple3DComponent* simpleComp = dynamic_cast<Simple3DComponent*>(component);
	if (simpleComp) {
		IEffect* effect = simpleComp->getEffect();
		IVBODrawable* drawable = simpleComp->getDrawable();
		int effectIndex = getIndexOf(effectVector, effect);
		revAssert(effectIndex >= 0);
		int drawableIndex = getIndexOf(drawableVector, drawable);
		revAssert(drawableIndex >= 0);

		int index = getIndexOf(visual3DVector, simpleComp);
		revAssert(index >= 0);
		index++;
		canHandleCBEvents = false;
		uiWidget.visualTypeCB->setCurrentIndex(index);
		uiWidget.effectCB->setEnabled(true);
		uiWidget.drawableCB->setEnabled(true);
		uiWidget.effectCB->setCurrentIndex(effectIndex);
		uiWidget.drawableCB->setCurrentIndex(drawableIndex);
		canHandleCBEvents = true;
	} else {
		int index = getIndexOf(visual3DVector, component);
		revAssert(index >= 0);
		index++;
		canHandleCBEvents = false;
		uiWidget.visualTypeCB->setCurrentIndex(index);
		uiWidget.effectCB->setEnabled(false);
		uiWidget.drawableCB->setEnabled(false);
		canHandleCBEvents = true;
	}
}

void NewObjectWidget::activateEditObjectMode(GameObject* go) {
	setEditMode(true);
	setTitle(EDIT_TITLE);
	gameObject = go;

	if (saveStateBinder) {
		delete saveStateBinder;
	}
	saveStateBinder = new SaveStateBinder(gameObject);
	rev::Renderer3D::ref().getPicker().pickGameObject(gameObject);
	loadVisual3DComponent();
	loadScriptingComponent();
	loadCollisionComponent();
	loadPhysicsComponent();
	loadLightComponent();
	loadShadowComponent();

	saveSceneState = mainWindow->isSceneSaved();
	mainWindow->setSceneSaved(false);
	scrollToSelected->start(100);
}

void NewObjectWidget::repickNewOrEditedObject() {
	if (!blockPicking) {
		return;
	}
	REV_DEBUG_MSG("repickNewOrEditedObject");

	revAssert(gameObject);
	GameObject* go = rev::Renderer3D::ref().getPicker().getPickedGameObject();
	if (go != gameObject) {
		rev::Renderer3D::ref().getPicker().pickGameObject(gameObject);
		mainWindow->scrollObjectTableToSelectedItem();
	}
}

void NewObjectWidget::activateNewObjectMode() {
	setNewMode(true);
	setTitle(NEW_TITLE);

	GameObjectTableModel* model = mainWindow->getGameWidgetsController()->getObjectListModel();
	GameObjectTableModel::GameObjectVector& goVector = model->getDataVector();
	int counter = 0;
	for (int i = 0; i < goVector.size(); i++) {
		GameObject*& go = goVector[i];
		if (StringUtil::startsWith(go->getName(), NEW_OBJECT_PREFIX)) {
			counter++;
		}
	}
	std::ostringstream oss;
	oss << NEW_OBJECT_PREFIX << (counter + 1);
	std::string name = oss.str();

	gameObject = new GameObject(name);

	ICamera* camera = CameraManager::ref().getCamera(0);
	gameObject->setPosition(
			camera->getPosition() + REV_NEW_OBJECT_DISTANCE * camera->getDirection());

	gameObject->setVisual3DComponent(createDefaultSimple3DComp());
	rev::Engine::ref().addGameObject(gameObject);
	rev::Renderer3D::ref().getPicker().pickGameObject(gameObject);
	loadVisual3DComponent();
	loadScriptingComponent();
	loadCollisionComponent();
	loadPhysicsComponent();
	loadLightComponent();
	loadShadowComponent();

	saveSceneState = mainWindow->isSceneSaved();
	mainWindow->setSceneSaved(false);
	mainWindow->refreshPanels();
	scrollToSelected->start(100);
}

Simple3DComponent* NewObjectWidget::createDefaultSimple3DComp() {
	BindableFactoryFunc effectFactory = getFactoryFunc(effectVector, "PhongShadingEffect");
	revAssert(effectFactory);
	IEffect* effect = (IEffect*) effectFactory();
	bool succ = effect->initAfterDeserialization();
	revAssert(succ);
	if (!succ) {
		REV_ERROR_MSG("Failed to initialize effect: '" << effect->getBindableName());
		delete effect;
		delete gameObject;
		gameObject = nullptr;
		return nullptr;
	}

	BindableFactoryFunc drawableFactory = getFactoryFunc(drawableVector, "VBOCubeVNT");
	revAssert(drawableFactory);
	IVBODrawable* drawable = (IVBODrawable*) drawableFactory();
	succ = drawable->initAfterDeserialization();
	revAssert(succ);
	if (!succ) {
		REV_ERROR_MSG("Failed to initialize drawable: '" << drawable->getBindableName());
		delete effect;
		delete drawable;
		delete gameObject;
		gameObject = nullptr;
		return nullptr;
	}
	return new Simple3DComponent(effect, true, drawable, true);
}

void NewObjectWidget::scrollToSelectedSlot() {
	mainWindow->scrollObjectTableToSelectedItem();
}

void NewObjectWidget::okActionPerformed() {
	REV_TRACE_FUNCTION;

	if (isEditMode()) {
		mainWindow->setNewPanelVisible(false, true);
		delete saveStateBinder;
		saveStateBinder = nullptr;
	} else {
		mainWindow->setNewPanelVisible(false, false);
	}
}

void NewObjectWidget::cancelActionPerformed() {

	mainWindow->setSceneSaved(saveSceneState);

	if (isNewMode()) {
		mainWindow->setNewPanelVisible(false, false);
		mainWindow->clearPropertiesPanel();

		gameObject->destroy();
		gameObject = nullptr;
	} else { //	if (isEditMode()) {
		mainWindow->setNewPanelVisible(false, true);
		struct SaveStateStruct {
			GameObject* gameObject;
			SaveStateBinder* binder;
			GameEditorWindow* mainWindow;

			~SaveStateStruct() {
				delete binder;
			}
		};

		mainWindow->clearPropertiesPanel();
		mainWindow->getScriptingWidget()->removeAllScriptingComponents();

		SaveStateStruct* sss = new SaveStateStruct { gameObject, saveStateBinder, mainWindow };

		EventManager::ref().fireEngineEvent("NewObjectWidget,restoreEditedObject",
				[sss] () -> bool {
					Engine::ref().silentRemoveGameObject(sss->gameObject);
					sss->binder->resore(sss->gameObject);
					Engine::ref().silentAddGameObject(sss->gameObject);

					rev::Renderer3D::ref().getPicker().pickGameObject(sss->gameObject);
					sss->mainWindow->getScriptingWidget()->addAllScriptingComponents();

					delete sss;
					return true;
				});

		saveStateBinder = nullptr;
	}
}

