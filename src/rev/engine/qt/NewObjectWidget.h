/*
 * NewObjectWidget.h
 *
 *  Created on: 17-03-2013
 *      Author: Revers
 */

#ifndef NEWOBJECTWIDGET_H_
#define NEWOBJECTWIDGET_H_

#include <vector>
#include <QWidget>
#include <QTimer>
#include "ui_NewObjectWidget.h"

namespace rev {

class GameEditorWindow;
class GameObject;
class IEffect;
class IVBODrawable;
class IBindable;
class SaveStateBinder;
class Simple3DComponent;

class NewObjectWidget: public QWidget {
Q_OBJECT

	typedef IBindable* (*BindableFactoryFunc)();
	struct NameFactoryStruct {
		std::string name;
		BindableFactoryFunc factoryFunc;
	};
	typedef std::vector<NameFactoryStruct> NameFactoryVector;

	NameFactoryVector visual3DVector;
	NameFactoryVector effectVector;
	NameFactoryVector drawableVector;
	NameFactoryVector lightVector;

	QTimer* scrollToSelected = nullptr;
	GameEditorWindow* mainWindow;
	Ui::NewObjectWidget uiWidget;
	GameObject* gameObject = nullptr;
	SaveStateBinder* saveStateBinder = nullptr;
	bool editMode = false;
	bool canHandleCBEvents = false;
	bool blockPicking = true;
	bool saveSceneState = true;

public:
	NewObjectWidget(GameEditorWindow* mainWindow);

	virtual ~NewObjectWidget();

	void activateEditObjectMode(GameObject* go);
	void activateNewObjectMode();
	void repickNewOrEditedObject();
	void init();

	bool isEditMode() {
		return editMode;
	}
	bool isNewMode() {
		return !editMode;
	}
	void okAction() {
		okActionPerformed();
	}
	void cancelAction() {
		cancelActionPerformed();
	}

private slots:
	void okActionPerformed();
	void cancelActionPerformed();
	void visualTypeCBChanged(int index);
	void effectCBChanged(int index);
	void drawableCBChanged(int index);
	void lightTypeCBChanged(int index);
	void shadowTypeCBChanged(int index);
	void physicsTypeCBChanged(int index);
	void scriptTypeCBChanged(int index);
	void collisionTypeCBChanged(int index);
	void scrollToSelectedSlot();

private:
	void setupActions();
	void setTitle(const char* title);
	void loadVisual3DComponent();
	void loadScriptingComponent();
	void loadPhysicsComponent();
	void loadLightComponent();
	void loadShadowComponent();
	void loadCollisionComponent();
	Simple3DComponent* createDefaultSimple3DComp();

	int getIndexOf(const NameFactoryVector& vec, IBindable* bindable);
	BindableFactoryFunc getFactoryFunc(const NameFactoryVector& vec, const char* name);
	void setEditMode(bool editMode) {
		this->editMode = editMode;
	}
	void setNewMode(bool newMode) {
		this->editMode = !newMode;
	}
};

} /* namespace rev */
#endif /* NEWOBJECTWIDGET_H_ */
