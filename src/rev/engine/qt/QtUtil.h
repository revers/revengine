/*
 * QtUtil.h
 *
 *  Created on: 15-03-2013
 *      Author: Revers
 */

#ifndef QTUTIL_H_
#define QTUTIL_H_

#include <rev/engine/events/RevMouseState.h>

namespace rev {

	namespace QtUtil {
		rev::MouseState getMouseState();
	}

} /* namespace rev */
#endif /* QTUTIL_H_ */
