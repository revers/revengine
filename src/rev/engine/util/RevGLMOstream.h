/*
 * RevGLMOstream.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVGLMOSTREAM_H_
#define REVGLMOSTREAM_H_

#include <ostream>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace rev {

	template<typename T>
	inline std::ostream& operator<<(std::ostream& out, const glm::detail::tvec2<T>& v) {
		out << "vec2(" << v.x << ", " << v.y << ")";
		return out;
	}

	template<typename T>
	inline std::ostream& operator<<(std::ostream& out, const glm::detail::tvec3<T>& v) {
		out << "vec3(" << v.x << ", " << v.y << ", " << v.z << ")";
		return out;
	}

	template<typename T>
	inline std::ostream& operator<<(std::ostream& out, const glm::detail::tvec4<T>& v) {
		out << "vec4(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
		return out;
	}

	template<typename T>
	inline std::ostream& operator<<(std::ostream& out, const glm::detail::tquat<T>& q) {
		out << "quat(" << q.x << ", " << q.y << ", " << q.z << ", " << q.w << ")";
		return out;
	}

} /* namespace rev */

#endif /* REVGLMOSTREAM_H_ */
