/*
 * RevCharSetStringTokenizer.h
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#ifndef REVCHARSETSTRINGTOKENIZER_H_
#define REVCHARSETSTRINGTOKENIZER_H_

#include <string>

namespace rev {

    class CharSetStringTokenizer {
    private:
        void skipDelimiter();
        bool isDelimiter(char c);

        std::string token;
        std::string::const_iterator currPos;
        std::string buffer;
        std::string delimiter;

    public:
        CharSetStringTokenizer(const std::string& str,
                const std::string& delimiter);

        /**
         * returns "" if there are no more tokens.
         */
        std::string next();
    };

} /* namespace rev */
#endif /* REVCHARSETSTRINGTOKENIZER_H_ */
