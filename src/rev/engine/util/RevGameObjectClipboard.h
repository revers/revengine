/*
 * RevGameObjectClipboard.h
 *
 *  Created on: 16-04-2013
 *      Author: Revers
 */

#ifndef REVGAMEOBJECTCLIPBOARD_H_
#define REVGAMEOBJECTCLIPBOARD_H_

namespace rev {

	class GameObject;

	class GameObjectClipboard {
		GameObject* gameObject = nullptr;

	public:
		GameObjectClipboard();
		~GameObjectClipboard();

		void copy(GameObject* go);
		void cut(GameObject* go);
		GameObject* paste();

		bool isEmpty() {
			return gameObject == nullptr;
		}
		void clear();
	};

} /* namespace rev */
#endif /* REVGAMEOBJECTCLIPBOARD_H_ */
