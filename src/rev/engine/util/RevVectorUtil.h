/*
 * RevVectorUtil.h
 *
 *  Created on: 06-04-2013
 *      Author: Revers
 */

#ifndef REVVECTORUTIL_H_
#define REVVECTORUTIL_H_

#include <vector>
#include <algorithm>

namespace rev {

	class VectorUtil {
		VectorUtil() = delete;
		VectorUtil(const VectorUtil&) = delete;
		~VectorUtil() = delete;

	public:

		/**
		 * This method swaps the last element of the vector "v" with its "i"-th element,
		 * and then calls v.pop() to remove the last element.
		 *
		 * @return true - when v.size() > 0 after calling this method.
		 */
		template<typename T>
		static inline bool fastRemoveIndex(std::vector<T>& v, int i) {
			if (i < v.size() - 1) {
				// swap with the last element:
				v[i] = v[v.size() - 1];
				// and remove the last element:
				v.pop_back();
				return true;
			}
			v.pop_back();
			return false;
		}

		/**
		 * This method swaps the last element of the vector "v" with the element "value",
		 * and then calls v.pop() to remove the last element.
		 */
		template<typename T>
		static inline void fastRemoveValue(std::vector<T>& v, const T& value) {
			for (int i = 0; i < v.size(); i++) {
				if (value == v[i]) {
					fastRemoveIndex(v, i);
					break;
				}
			}
		}

		/**
		 * Removes element from the vector "v" using standard STL <algorithm> method.
		 */
		template<typename T>
		static inline void removeValue(std::vector<T>& v, const T& value) {
			v.erase(std::remove(v.begin(), v.end(), value), v.end());
		}

	};

} /* namespace rev */

#endif /* REVVECTORUTIL_H_ */
