/*
 * RevGameObjectClipboard.cpp
 *
 *  Created on: 16-04-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/events/RevEventManager.h>
#include "RevGameObjectClipboard.h"

using namespace rev;

GameObjectClipboard::GameObjectClipboard() {
}

GameObjectClipboard::~GameObjectClipboard() {
	// FIXME: another problem when deleteing GameObject (with VBODRawable) in Qt's
	// destructor:
	//clear();
}

void GameObjectClipboard::clear() {
	if (gameObject) {
		delete gameObject;
		gameObject = nullptr;
	}
}

void GameObjectClipboard::copy(GameObject* go) {
	clear();
	gameObject = static_cast<GameObject*>(go->makeDeepCopy());
	revAssert(gameObject);
}

void GameObjectClipboard::cut(GameObject* go) {
	clear();
	gameObject = go;
	EventManager::ref().fireEngineEvent("GameObjectClipboard::cut", [go] () -> bool {
		Engine::ref().silentRemoveGameObject(go);
		return true;
	});
}

GameObject* GameObjectClipboard::paste() {
	if (!gameObject) {
		return nullptr;
	}
	GameObject* go = static_cast<GameObject*>(gameObject->makeDeepCopy());
	revAssert(go);

	Engine::ref().addGameObject(go);
//	EventManager::ref().fireEngineEvent("GameObjectClipboard::paste()", [go] () -> bool {
//		return true;
//	});

	return go;
}

