/*
 * RevCharSetStringTokenizer.cpp
 *
 *  Created on: 08-02-2013
 *      Author: Revers
 */

#include "RevCharSetStringTokenizer.h"

using namespace rev;

CharSetStringTokenizer::CharSetStringTokenizer(const std::string& str,
        const std::string& delimiter) :
        token(""), buffer(str), delimiter(delimiter) {
    currPos = buffer.begin();
}

std::string CharSetStringTokenizer::next() {
    if (buffer.size() <= 0) {
        // skip if buffer is empty
        return "";
    }

    // reset token string
    token.clear();

    // skip leading delimiters
    skipDelimiter();

    // append each char to token string until it meets delimiter
    while (currPos != buffer.end() && !isDelimiter(*currPos)) {
        token += *currPos;
        ++currPos;
    }
    return token;
}

void CharSetStringTokenizer::skipDelimiter() {
    while (currPos != buffer.end() && isDelimiter(*currPos)) {
        ++currPos;
    }
}

bool CharSetStringTokenizer::isDelimiter(char c) {
    return (delimiter.find(c) != std::string::npos);
}
