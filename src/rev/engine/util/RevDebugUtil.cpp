/*
 * RevDebugUtil.cpp
 *
 *  Created on: 09-03-2013
 *      Author: Revers
 */

#include <sstream>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include "RevDebugUtil.h"

using namespace rev;
using namespace std;

void rev::DebugUtil::showMessage(const char* msg, IBindable* bindablePtr) {
	rev::GameObject* go = dynamic_cast<rev::GameObject*>(bindablePtr);
	if (go) {
		REV_WARN_MSG(msg << ": " << go->getName() << " (" << go->getId() << ")");
	} else {
		REV_WARN_MSG(msg << ": " << bindablePtr->getBindableName());
	}
}

std::string rev::DebugUtil::getBindableName(const IBindable* b) {
	const rev::GameObject* go = dynamic_cast<const rev::GameObject*>(b);

	if (go) {
		ostringstream result;
		result << go->getName() << " (" << go->getId() << ")";
		return result.str();
	} else {
		return b->getBindableName();
	}
}
