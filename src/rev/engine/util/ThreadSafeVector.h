/*
 * ThreadSafeVector.h
 *
 *  Created on: 11-03-2013
 *      Author: Revers
 */

#ifndef THREADSAFEVECTOR_H_
#define THREADSAFEVECTOR_H_

#include <vector>
#include <rev/engine/concurrency/RevMutex.h>

namespace rev {

	template<typename T>
	class ThreadSafeVector {
		std::vector<T> vec;
		mutable rev::Mutex mutex;

	public:
		ThreadSafeVector() {
		}
		~ThreadSafeVector() {
		}

		T& operator[](int index) {
			MutexGuard guard(&mutex);
			return vec[index];
		}

		void erase(int i) {
			MutexGuard guard(&mutex);
			vec.erase(vec.begin() + i);
		}

		const T& operator[](int index) const {
			MutexGuard guard(&mutex);
			return vec[index];
		}

		void push_back(const T& val) {
			MutexGuard guard(&mutex);
			vec.push_back(val);
		}

		bool empty() const {
			MutexGuard guard(&mutex);
			return vec.empty();
		}

		void clear() {
			MutexGuard guard(&mutex);
			vec.clear();
		}

		int size() const {
			MutexGuard guard(&mutex);
			return (int) vec.size();
		}

	};

} /* namespace rev */
#endif /* THREADSAFEVECTOR_H_ */
