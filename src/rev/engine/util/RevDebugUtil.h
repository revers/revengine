/*
 * RevDebugUtil.h
 *
 *  Created on: 09-03-2013
 *      Author: Revers
 */

#ifndef REVDEBUGUTIL_H_
#define REVDEBUGUTIL_H_

#include <string>

namespace rev {
	class IBindable;

	class DebugUtil {
		DebugUtil() = delete;
		DebugUtil(const DebugUtil&) = delete;
		~DebugUtil() = delete;

	public:
		static void showMessage(const char* msg, IBindable* bindablePtr);

		/**
		 * @param bindablePtr - should be subclass of IBindable.
		 */
		static void showMessage(const char* msg, void* bindablePtr) {
			showMessage(msg, static_cast<IBindable*>(bindablePtr));
		}

		/**
		 * @return IBindable::getBindableName() or
		 * 		GameObject::getName() + GameObject::getId()
		 * 		if bindable is an instance of GameObject class.
		 */
		static std::string getBindableName(const IBindable* b);
	};

} /* namespace rev */
#endif /* REVDEBUGUTIL_H_ */
