#include <memory>
#include <cmath>
#include <ctime>
#include <random>
#include <iostream>
#include <gtest/gtest.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevArrayUtil.h>

#include <rev/common/test/RevTestUtil.hpp>
#include <rev/common/RevDataGenerator.h>
#include <rev/engine/math/RevMathHelper.h>

using namespace glm;
using namespace rev;
using namespace std;

void printMat(const glm::mat4& m) {
    cout << endl;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << m[j][i] << ", ";
        }
        cout << endl;
    }
}

void printVec(const glm::vec3& m) {
	cout << endl;
	for (int i = 0; i < 3; i++) {
		cout << m[i] << ", ";
	}
	cout << endl;
}

void printVec(const glm::vec4& m) {
	cout << endl;
	for (int i = 0; i < 4; i++) {
		cout << m[i] << ", ";
	}
	cout << endl;
}

bool almostEqual(const glm::mat4& m1, const glm::mat4& m2) {
	const float* farr1 = glm::value_ptr(m1);
	const float* farr2 = glm::value_ptr(m2);

	for (int i = 0; i < 16; i++) {
		if (fabs(farr1[i] - farr2[i]) > 0.0001f) {
			return false;
		}
	}

	return true;
}

TEST(DecomposeTest, TestTRS) {
    TestUtil::printHeader();

    TestUtil::printTitle("TestTRS", "");
	
	std::mt19937 engine((int) time(0));
	std::uniform_real_distribution<float> dis(0.1f, 120.0f);

	for (int i = 0; i < 100; i++) {
		mat4 t = glm::translate(dis(engine), dis(engine), dis(engine));
		vec3 axis(dis(engine), dis(engine), dis(engine));
		mat4 r = glm::rotate(dis(engine), glm::normalize(axis));
		mat4 s = glm::scale(dis(engine), dis(engine), dis(engine));

		glm::mat4 m = t * r * s;
		printMat(m);

		vec3 vt;
		vec3 vs;
		mat4 rr;
		MathHelper::decomposeTRS(m, vs, rr, vt);

		bool tequal = almostEqual(t, glm::translate(vt));
		bool requal = almostEqual(r, rr);
		bool sequal = almostEqual(s, glm::scale(vs));
		
		cout << "tequal = " << boolalpha << tequal << endl;
		cout << "requal = " << boolalpha << requal << endl;
		cout << "sequal = " << boolalpha << sequal << endl;
		printVec(vt);
		printVec(vs);
		
		ASSERT_TRUE(tequal && requal && sequal);
	}
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
