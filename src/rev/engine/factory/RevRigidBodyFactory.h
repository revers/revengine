/*
 * RevRigidBodyFactory.h
 *
 *  Created on: 27-05-2013
 *      Author: Revers
 */

#ifndef REVRIGIDBODYFACTORY_H_
#define REVRIGIDBODYFACTORY_H_

#include <rev/engine/binding/RevIBindable.h>
#include <rev/engine/math/RevReal.h>
#include <random>

namespace rev {

	class GameObject;

	class RigidBodyFactory: public IBindable {
		static RigidBodyFactory* rigidBodyFactory;

		std::mt19937 engine;

		rev::rvec3 minPos = rev::rvec3(-15, 5, -15);
		rev::rvec3 maxPos = rev::rvec3(15, 25, 15);
		rev::rvec3 minSize = rev::rvec3(0.5, 0.5, 0.5);
		rev::rvec3 maxSize = rev::rvec3(4.5, 1.5, 1.5);
		real minRadius = real(0.5);
		real maxRadius = real(1.5);

		RigidBodyFactory();

		RigidBodyFactory(const RigidBodyFactory&) = delete;
		~RigidBodyFactory();

		GLSLProgram* phongShadingProgram = nullptr;

	public:
		DECLARE_BINDABLE_SINGLETON(RigidBodyFactory)

		static void createSingleton();
		static void destroySingleton();

		static RigidBodyFactory& ref() {
			return *rigidBodyFactory;
		}

		static RigidBodyFactory* getInstance() {
			return rigidBodyFactory;
		}

		bool init();
		static void initScripting();

		GameObject* createSphere(const rev::rvec3& position,
				const rev::rquat& orientation, real radius,
				const rev::rvec3& velocity);

		GameObject* createBox(const rev::rvec3& position,
				const rev::rquat& orientation,
				const rev::rvec3& halfSize,
				const rev::rvec3& velocity);

		GameObject* createRandomSphere();
		GameObject* createRandomBox();

		void setSphereState(GameObject* sphere,
				const rev::rvec3& position,
				const rev::rquat& orientation,
				real radius, const rev::rvec3& velocity);

		void setBoxState(GameObject* box,
				const rev::rvec3& position,
				const rev::rquat& orientation,
				const rev::rvec3& halfSize,
				const rev::rvec3& velocity);

		void setRandomSphereState(GameObject* sphere);
		void setRandomBoxState(GameObject* box);

	private:
		real rand(real min, real max);
		rev::rvec3 rand(const rev::rvec3& min, const rev::rvec3& max);
		rev::rquat randQuat();

	private:
		static v8::Handle<v8::Value> jsCreateRandomBox(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsCreateRandomSphere(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsSetRandomState(const v8::Arguments& args);

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVRIGIDBODYFACTORY_H_ */
