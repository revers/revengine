/*
 * RevPPSEventListener.cpp
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#include <rev/engine/RevGameObject.h>
#include <rev/engine/effects/RevPointSpheresEffect.h>
#include <rev/engine/drawables/RevVBOPointSpheresVC.h>
#include <rev/engine/components/RevPhysicsPPSComponent.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include "RevPPSEventListener.h"

using namespace rev;

PPSEventListener::PPSEventListener() {
}

void PPSEventListener::update() {
	GameObject* parent = getParent();
	Simple3DComponent* simple3D = static_cast<Simple3DComponent*>(parent->getVisual3DComponent());
	VBOPointSpheresVC* pointsVBO = static_cast<VBOPointSpheresVC*>(simple3D->getDrawable());
	PhysicsPPSComponent* physics = static_cast<PhysicsPPSComponent*>(parent->getPhysicsComponent());

	for (int i = 0; i < physics->getParticlesCount(); i++) {
		pointsVBO->setPoint(i, physics->getParticlePosition(i));
	}
	pointsVBO->reinit();
}
