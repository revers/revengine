/*
 * RevPPSEventListener.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVPPSEVENTLISTENER_H_
#define REVPPSEVENTLISTENER_H_

#include <rev/engine/components/RevIEventListenerComponent.h>

namespace rev {

	class PPSEventListener: public IEventListenerComponent {
	public:
		DECLARE_WEAK_BINDABLE(PPSEventListener)

		PPSEventListener();
		virtual ~PPSEventListener() {
		}

		void update() override;
	};

} /* namespace rev */
#endif /* REVPPSEVENTLISTENER_H_ */
