/*
 * RevRigidBodyFactory.cpp
 *
 *  Created on: 27-05-2013
 *      Author: Revers
 */

#include <ctime>
#include <sstream>
#include <glm/gtc/matrix_transform.hpp>

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/gl/RevGLSLProgram.h>

#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/drawables/RevVBOSphereVN.h>
#include <rev/engine/drawables/RevVBOCuboidVNT.h>
#include <rev/engine/effects/RevPhongShadingEffect.h>
#include <rev/engine/collision/RevCollisionBox.h>
#include <rev/engine/collision/RevCollisionSphere.h>
#include <rev/engine/components/RevSimple3DComponent.h>
#include <rev/engine/components/RevCollisionComponent.h>
#include <rev/engine/components/RevRigidBodyComponent.h>
#include <rev/engine/physics/RevInertiaTensorUtil.h>

#include "RevRigidBodyFactory.h"

using namespace std;
using namespace rev;
using namespace glm;
using namespace v8;

rev::RigidBodyFactory* rev::RigidBodyFactory::rigidBodyFactory = nullptr;

IMPLEMENT_BINDABLE_SINGLETON_FULL(
		RigidBodyFactory,
		RigidBodyFactory::getInstance(),
		RigidBodyFactory::initScripting)

RigidBodyFactory::RigidBodyFactory() :
		engine(time(0)) {
}

void RigidBodyFactory::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(!rigidBodyFactory);
	rigidBodyFactory = new RigidBodyFactory();
	REV_TRACE_FUNCTION_OUT;
}

void RigidBodyFactory::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	assert(rigidBodyFactory);
	delete rigidBodyFactory;
	rigidBodyFactory = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

RigidBodyFactory::~RigidBodyFactory() {
	if (phongShadingProgram) {
		delete phongShadingProgram;
	}
}

bool RigidBodyFactory::init() {
	phongShadingProgram = PhongShadingEffect::createGLSLProgram();
	return phongShadingProgram != nullptr;
}

void RigidBodyFactory::initScripting() {
	Handle<ObjectTemplate> proto = getFunctionTemplate()->PrototypeTemplate();
	proto->Set(String::New("createRandomBox"),
			FunctionTemplate::New(jsCreateRandomBox)->GetFunction());
	proto->Set(String::New("createRandomSphere"),
			FunctionTemplate::New(jsCreateRandomSphere)->GetFunction());
	proto->Set(String::New("setRandomState"),
			FunctionTemplate::New(jsSetRandomState)->GetFunction());
}

rev::rvec3 RigidBodyFactory::rand(const rev::rvec3& min, const rev::rvec3& max) {
	return rev::rvec3(rand(min.x, max.x), rand(min.y, max.y), rand(min.z, max.z));
}

real RigidBodyFactory::rand(real min, real max) {
	std::uniform_real_distribution<real> dis(min, max);
	return dis(engine);
}

rev::rquat RigidBodyFactory::randQuat() {
	std::uniform_real_distribution<real> dis(0.0, 1.0);
	rev::rquat q(dis(engine), dis(engine), dis(engine), dis(engine));
	return glm::normalize(q);
}

GameObject* RigidBodyFactory::createSphere(const rev::rvec3& position,
		const rev::rquat& orientation, real radius, const rev::rvec3& velocity) {
	GameObject* gameObject = new GameObject();

	VBOSphereVN* drawable = new VBOSphereVN(radius, 10);
	PhongShadingEffect* phongShadingEffect = new PhongShadingEffect(phongShadingProgram);
	IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, true);
	gameObject->setVisual3DComponent(visualComponent);

	CollisionComponent* collision = new CollisionComponent();
	collision->setBoundingSphere(*drawable->getBoundingSphere());
	collision->setGeometry(new CollisionSphere(float(radius)));
	gameObject->setCollisionComponent(collision);

	RigidBodyComponent* body = new RigidBodyComponent();
	gameObject->setPhysicsComponent(body);

	body->setPosition(position);
	body->setOrientation(orientation);
	body->setVelocity(velocity);
	body->setAngularVelocity(rvec3(0, 0, 0));

	real mass = real(4.0 * 0.3333333 * R_PI) * radius * radius * radius;
	body->setMass(mass);

	rmat3 tensor = InertiaTensorUtil::getSphereTensor(radius, mass);
	body->setInertiaTensor(tensor);

	body->setLinearDamping(0.95);
	body->setAngularDamping(0.8);
	body->clearAccumulators();
	body->setConstantAcceleration(0, -10.0, 0);

	//body->setCanSleep(false);
	body->setAwake();

	//body->calculateDerivedData();

	return gameObject;
}

GameObject* RigidBodyFactory::createBox(const rev::rvec3& position,
		const rev::rquat& orientation, const rev::rvec3& halfSize, const rev::rvec3& velocity) {
	GameObject* gameObject = new GameObject();

	float xLength = float(halfSize.x * 2.0);
	float yLength = float(halfSize.y * 2.0);
	float zLength = float(halfSize.z * 2.0);
	VBOCuboidVNT* drawable = new VBOCuboidVNT(xLength, yLength, zLength);

	PhongShadingEffect* phongShadingEffect = new PhongShadingEffect(phongShadingProgram);
	IVisual3DComponent* visualComponent = new rev::Simple3DComponent(
			phongShadingEffect, true, drawable, true);
	gameObject->setVisual3DComponent(visualComponent);

	CollisionComponent* collision = new CollisionComponent();
	collision->setBoundingSphere(*drawable->getBoundingSphere());

	collision->setGeometry(new CollisionBox(xLength, yLength, zLength));
	gameObject->setCollisionComponent(collision);

	RigidBodyComponent* body = new RigidBodyComponent();
	gameObject->setPhysicsComponent(body);

	body->setPosition(position);
	body->setOrientation(orientation);
	body->setVelocity(velocity);
	body->setAngularVelocity(rvec3(0, 0, 0));

	real mass = halfSize.x * halfSize.y * halfSize.z * 8.0;
	body->setMass(mass);

	rmat3 tensor = InertiaTensorUtil::getBoxTensor(halfSize, mass);
	body->setInertiaTensor(tensor);

	body->setLinearDamping(0.95);
	body->setAngularDamping(0.8);
	body->clearAccumulators();
	body->setConstantAcceleration(0, -10.0, 0);

	//body->setCanSleep(false);
	body->setAwake();

	//body->calculateDerivedData();

	return gameObject;
}

void RigidBodyFactory::setSphereState(GameObject* sphere, const rev::rvec3& position,
		const rev::rquat& orientation, real radius, const rev::rvec3& velocity) {

	Simple3DComponent* visualComponent =
			static_cast<Simple3DComponent*>(sphere->getVisual3DComponent());

	VBOSphereVN* drawable = static_cast<VBOSphereVN*>(visualComponent->getDrawable());
	drawable->setRadius(float(radius));

	CollisionComponent* collision = sphere->getCollisionComponent();
	collision->setBoundingSphere(*drawable->getBoundingSphere());
	CollisionSphere* collSphere = static_cast<CollisionSphere*>(collision->getGeometry());
	collSphere->setRadius(float(radius));

	RigidBodyComponent* body = static_cast<RigidBodyComponent*>(sphere->getPhysicsComponent());

	body->setPosition(position);
	body->setOrientation(orientation);
	body->setVelocity(velocity);
	body->setAngularVelocity(rvec3(0, 0, 0));

	real mass = real(4.0 * 0.3333333 * R_PI) * radius * radius * radius;
	body->setMass(mass);

	rmat3 tensor = InertiaTensorUtil::getSphereTensor(radius, mass);
	body->setInertiaTensor(tensor);

	body->setLinearDamping(0.95);
	body->setAngularDamping(0.8);
	body->clearAccumulators();
	body->setConstantAcceleration(0, -10.0, 0);

	//body->setCanSleep(false);
	body->setAwake();
}

void RigidBodyFactory::setBoxState(GameObject* box, const rev::rvec3& position,
		const rev::rquat& orientation, const rev::rvec3& halfSize,
		const rev::rvec3& velocity) {

	float xLength = float(halfSize.x * 2.0);
	float yLength = float(halfSize.y * 2.0);
	float zLength = float(halfSize.z * 2.0);

	Simple3DComponent* visualComponent =
			static_cast<Simple3DComponent*>(box->getVisual3DComponent());

	VBOCuboidVNT* drawable = static_cast<VBOCuboidVNT*>(visualComponent->getDrawable());
	drawable->setLengths(xLength, yLength, zLength);

	CollisionComponent* collision = box->getCollisionComponent();
	collision->setBoundingSphere(*drawable->getBoundingSphere());
	CollisionBox* collBox = static_cast<CollisionBox*>(collision->getGeometry());
	collBox->setSize(vec3(xLength, yLength, zLength));

	RigidBodyComponent* body = static_cast<RigidBodyComponent*>(box->getPhysicsComponent());

	body->setPosition(position);
	body->setOrientation(orientation);
	body->setVelocity(velocity);
	body->setAngularVelocity(rvec3(0, 0, 0));

	real mass = halfSize.x * halfSize.y * halfSize.z * 8.0;
	body->setMass(mass);

	rmat3 tensor = InertiaTensorUtil::getBoxTensor(halfSize, mass);
	body->setInertiaTensor(tensor);

	body->setLinearDamping(0.95);
	body->setAngularDamping(0.8);
	body->clearAccumulators();
	body->setConstantAcceleration(0, -10.0, 0);

	//body->setCanSleep(false);
	body->setAwake();
}

GameObject* RigidBodyFactory::createRandomSphere() {
	return createSphere(
			rand(minPos, maxPos),
			randQuat(),
			rand(minRadius, maxRadius),
			rev::rvec3(0));
}

GameObject* RigidBodyFactory::createRandomBox() {
	return createBox(
			rand(minPos, maxPos),
			randQuat(),
			rand(minSize, maxSize),
			rev::rvec3(0));
}

void RigidBodyFactory::setRandomSphereState(GameObject* sphere) {
	setSphereState(sphere,
			rand(minPos, maxPos),
			randQuat(),
			rand(minRadius, maxRadius),
			rev::rvec3(0));
}

void RigidBodyFactory::setRandomBoxState(GameObject* box) {
	setBoxState(box,
			rand(minPos, maxPos),
			randQuat(),
			rand(minSize, maxSize),
			rev::rvec3(0));
}

v8::Handle<v8::Value> RigidBodyFactory::jsCreateRandomBox(const v8::Arguments& args) {
	if (args.Length() != 0) {
		std::ostringstream oss;
		oss << "Wrong number of argument: " << args.Length() << "; Expected: 0";
		return ThrowException(String::New(oss.str().c_str()));
	}

	GameObject* box = RigidBodyFactory::ref().createRandomBox();
	box->setSaveable(false);
	Engine::ref().addGameObject(box);

	return box->wrap();
}

v8::Handle<v8::Value> RigidBodyFactory::jsCreateRandomSphere(const v8::Arguments& args) {
	if (args.Length() != 0) {
		std::ostringstream oss;
		oss << "Wrong number of argument: " << args.Length() << "; Expected: 0";
		return ThrowException(String::New(oss.str().c_str()));
	}

	GameObject* sphere = RigidBodyFactory::ref().createRandomSphere();
	sphere->setSaveable(false);
	Engine::ref().addGameObject(sphere);

	return sphere->wrap();
}

v8::Handle<v8::Value> RigidBodyFactory::jsSetRandomState(const v8::Arguments& args) {
	if (args.Length() != 1) {
		std::ostringstream oss;
		oss << "Wrong number of argument: " << args.Length() << "; Expected: 1";
		return ThrowException(String::New(oss.str().c_str()));
	}

	Local<Object> argObject = Local<Object>::Cast(args[0]);
	Local<External> wrap = Local<External>::Cast(argObject->GetInternalField(0));
	GameObject* obj = static_cast<GameObject*>(wrap->Value());

	CollisionComponent* collision = obj->getCollisionComponent();
	CollisionGeometry* geometry = collision->getGeometry();

	switch (geometry->getType()) {
	case GeometryType::SPHERE: {
		RigidBodyFactory::ref().setRandomSphereState(obj);
		break;
	}
	case GeometryType::BOX: {
		RigidBodyFactory::ref().setRandomBoxState(obj);
		break;
	}
	case GeometryType::PLANE: {
		revAssertMsg(false, "Plane is currently not handled");
		break;
	}
	default: {
		revAssertMsg(false, "Unknown type");
		break;
	}
	}

	return v8::Undefined();
}

void RigidBodyFactory::bind(IBinder& binder) {
	binder.bindSimple(minPos);
	binder.bindSimple(maxPos);
	binder.bindSimple(minSize);
	binder.bindSimple(maxSize);
	binder.bindSimple(minRadius);
	binder.bindSimple(maxRadius);
}
