/*
 * RevMathHelper.cpp
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#include <glm/gtx/norm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include "RevMathHelper.h"

using namespace rev;
using namespace glm;

const glm::vec3 MathHelper::POSITIVE_X(1.0, 0.0, 0.0);
const glm::vec3 MathHelper::POSITIVE_Y(0.0, 1.0, 0.0);
const glm::vec3 MathHelper::POSITIVE_Z(0.0, 0.0, 1.0);

const glm::vec3 MathHelper::NEGATIVE_X(-1.0, 0.0, 0.0);
const glm::vec3 MathHelper::NEGATIVE_Y(0.0, -1.0, 0.0);
const glm::vec3 MathHelper::NEGATIVE_Z(0.0, 0.0, -1.0);

const glm::vec3 MathHelper::ZERO(0.0, 0.0, 0.0);
const glm::vec3 MathHelper::ONE(1.0, 1.0, 1.0);
const glm::mat4 MathHelper::IDENTITY_MATRIX(1.0);

/**
 * http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/#How_do_I_create_a_quaternion_in_C____
 */
glm::quat MathHelper::rotationBetweenVectors(glm::vec3 start, glm::vec3 dest) {
	using namespace glm;

	start = normalize(start);
	dest = normalize(dest);

	float cosTheta = dot(start, dest);
	vec3 rotationAxis;

	if (cosTheta < -1 + 0.001f) {
		// special case when vectors in opposite directions:
		// there is no "ideal" rotation axis
		// So guess one; any will do as long as it's perpendicular to start
		rotationAxis = cross(vec3(0.0f, 0.0f, 1.0f), start);
		if (length2(rotationAxis) < 0.01) // bad luck, they were parallel, try again!
			rotationAxis = cross(vec3(1.0f, 0.0f, 0.0f), start);

		rotationAxis = normalize(rotationAxis);
		return angleAxis(180.0f, rotationAxis);
	}

	rotationAxis = cross(start, dest);

	float s = sqrt((1 + cosTheta) * 2);
	float invs = 1 / s;

	return quat(
			s * 0.5f,
			rotationAxis.x * invs,
			rotationAxis.y * invs,
			rotationAxis.z * invs);
}

/**
 * Decomposes matrix M such that T * R * S = M, where T is translation matrix,
 * R is rotation matrix and S is scaling matrix.
 * http://code.google.com/p/assimp-net/source/browse/trunk/AssimpNet/Matrix4x4.cs
 * (this method is exact to at least 0.0001f)
 *
 * | 1  0  0  T1 | | R11 R12 R13 0 | | a 0 0 0 |   | aR11 bR12 cR13 T1 |
 * | 0  1  0  T2 |.| R21 R22 R23 0 |.| 0 b 0 0 | = | aR21 bR22 cR23 T2 |
 * | 0  0  1  T3 | | R31 R32 R33 0 | | 0 0 c 0 |   | aR31 bR32 cR33 T3 |
 * | 0  0  0   1 | |  0   0   0  1 | | 0 0 0 1 |   |  0    0    0    1 |
 *
 * @param m (in) matrix to decompose
 * @param scaling (out) scaling vector
 * @param rotation (out) rotation matrix
 * @param translation (out) translation vector
 */
void MathHelper::decomposeTRS(const glm::mat4& m, glm::vec3& scaling,
		glm::mat4& rotation, glm::vec3& translation) {
	// Extract the translation
	translation.x = m[3][0];
	translation.y = m[3][1];
	translation.z = m[3][2];

	// Extract col vectors of the matrix
	glm::vec3 col1(m[0][0], m[0][1], m[0][2]);
	glm::vec3 col2(m[1][0], m[1][1], m[1][2]);
	glm::vec3 col3(m[2][0], m[2][1], m[2][2]);

	//Extract the scaling factors
	scaling.x = glm::length(col1);
	scaling.y = glm::length(col2);
	scaling.z = glm::length(col3);

	// Handle negative scaling
	if (glm::determinant(m) < 0) {
		scaling.x = -scaling.x;
		scaling.y = -scaling.y;
		scaling.z = -scaling.z;
	}

	// Remove scaling from the matrix
	if (scaling.x != 0) {
		col1 /= scaling.x;
	}

	if (scaling.y != 0) {
		col2 /= scaling.y;
	}

	if (scaling.z != 0) {
		col3 /= scaling.z;
	}

	rotation[0][0] = col1.x;
	rotation[0][1] = col1.y;
	rotation[0][2] = col1.z;
	rotation[0][3] = 0.0;

	rotation[1][0] = col2.x;
	rotation[1][1] = col2.y;
	rotation[1][2] = col2.z;
	rotation[1][3] = 0.0;

	rotation[2][0] = col3.x;
	rotation[2][1] = col3.y;
	rotation[2][2] = col3.z;
	rotation[2][3] = 0.0;

	rotation[3][0] = 0.0;
	rotation[3][1] = 0.0;
	rotation[3][2] = 0.0;
	rotation[3][3] = 1.0;
}

/**
 * Decompose matrix M, such that M = QR, where Q is an orthogonal matrix, and R is some matrix.
 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
 *
 * @param M matrix to decompose.
 * @param Q out parameter
 * @param R out parameter
 */
void MathHelper::decomposeQR(const glm::mat4& M, glm::mat4& Q,
		glm::mat4& R) {
	double mag, alpha;
	glm::vec4 u, v;
	glm::mat4 P(1.0);
	glm::mat4 I(1.0);

	Q = glm::mat4(1.0);
	R = M;

	for (int i = 0; i < 4; i++) {
		u = glm::vec4(0.0);
		v = glm::vec4(0.0);

		mag = 0.0;
		for (int j = i; j < 4; j++) {
			u[j] = R[j][i];
			mag += u[j] * u[j];
		}
		mag = sqrt(mag);

		alpha = u[i] < 0 ? mag : -mag;

		mag = 0.0;
		for (int j = i; j < 4; j++) {
			v[j] = j == i ? u[j] + alpha : u[j];
			mag += v[j] * v[j];
		}
		mag = sqrt(mag);

		if (mag < EPSILON_D)
			continue;

		for (int j = i; j < 4; j++)
			v[j] /= mag;

		P = I - (multTranspose(v, v)) * 2.0;

		R = P * R;
		Q = Q * P;
	}
}

/**
 * Decompose matrix M, such that M = RQ, where Q is an orthogonal matrix, and R is some matrix.
 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
 *
 * @param M matrix to decompose.
 * @param Q out parameter
 * @param R out parameter
 */
void MathHelper::decomposeRQ(const glm::mat4& M, glm::mat4& Q,
		glm::mat4& R) {
	double mag, alpha;

	glm::vec4 u, v;
	glm::mat4 P(1.0), I(1.0);

	Q = glm::mat4(1.0);
	R = M;

	for (int i = 0; i < 4; i++) {
		u = glm::vec4(0.0);
		v = glm::vec4(0.0);

		mag = 0.0;
		for (int j = i; j < 4; j++) {
			u[j] = R[j][i];
			mag += u[j] * u[j];
		}
		mag = sqrt(mag);

		alpha = u[i] < 0 ? mag : -mag;

		mag = 0.0;
		for (int j = i; j < 4; j++) {
			v[j] = j == i ? u[j] + alpha : u[j];
			mag += v[j] * v[j];
		}
		mag = sqrt(mag);

		if (mag < EPSILON_D)
			continue;

		for (int j = i; j < 4; j++)
			v[j] /= mag;

		P = I - (multTranspose(v, v)) * 2.0;

		R = R * P;
		Q = P * Q;

	}
}

//glm::mat4 MathHelper::multTranspose(const glm::vec4& A,
//		const glm::vec4& B) {
//	glm::mat4 R;
//	for (int j = 0; j < 4; j++) {
//		for (int i = 0; i < 4; i++) {
//			R[j][i] = A[j] * B[i];
//		}
//	}
//	return R;
//}

/**
 * Decomposes glm::perspective() matrix.
 *
 * @param p      [in]  a glm::perspective() matrix.
 * @param fovy   [out] angle
 * @param aspect [out] aspect ratio (width / height)
 * @param near_  [out] near plane
 * @param far_   [out] far plane
 */
void MathHelper::decomposePerspective(const glm::mat4& p,
		float& fovy, float& aspect, float& near_, float& far_) {
	double nearD = (double) p[3][2] / ((double) p[2][2] - 1.0);
	double farD = (double) p[3][2] / ((double) p[2][2] + 1.0);

	double rightD = nearD / (double) p[0][0];
	double arD = rightD * ((double) p[1][1] / nearD);
	double fovyD = glm::degrees(atan(1.0 / (double) p[1][1]) * 2.0);

	fovy = (float) fovyD;
	aspect = (float) arD;
	near_ = (float) nearD;
	far_ = (float) farD;
}

/**
 *  http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/index.htm
 */
glm::vec3 MathHelper::rotate(const glm::vec3& v, const glm::quat& q) {
	glm::quat p(0.0, v.x, v.y, v.z);
	p = q * p * glm::conjugate(q);
	return glm::vec3(p.x, p.y, p.z);
}

glm::mat4 MathHelper::getRotationFromAxisX(const glm::vec3& axisX) {
	float angle = glm::degrees(acos(glm::dot(axisX, MathHelper::POSITIVE_X)));
	if (angle == 0.0f || angle == 180.0f) {
		// axisX is parallel to MathHelper::POSITIVE_X.
		return mat4(1.0);
	}
	/*
	 * http://stackoverflow.com/a/7785719/245148
	 * Cross product will point upward or downward depending on an angle
	 * between two vectors. If the angle is less than 180 degrees then
	 * it will point upward, otherwise it will point downward.
	 *
	 * Hence acos with its 0-180 degree range will be sufficient
	 * for whole range of 0-360 degree rotations.
	 */
	glm::vec3 axis = glm::cross(axisX, MathHelper::POSITIVE_X);
	return glm::rotate(angle, axis);
}

glm::mat4 MathHelper::getRotationFromAxisY(const glm::vec3& axisY) {
	float angle = glm::degrees(acos(glm::dot(axisY, MathHelper::POSITIVE_Y)));
	if (angle == 0.0f || angle == 180.0f) {
		// axisY is parallel to MathHelper::POSITIVE_Y.
		return mat4(1.0);
	}
	/*
	 * http://stackoverflow.com/a/7785719/245148
	 * Cross product will point upward or downward depending on an angle
	 * between two vectors. If the angle is less than 180 degrees then
	 * it will point upward, otherwise it will point downward.
	 *
	 * Hence acos with its 0-180 degree range will be sufficient
	 * for whole range of 0-360 degree rotations.
	 */
	glm::vec3 axis = glm::cross(axisY, MathHelper::POSITIVE_Y);
	return glm::rotate(angle, axis);
}

glm::mat4 MathHelper::getRotationFromAxisZ(const glm::vec3& axisZ) {
	float angle = glm::degrees(acos(glm::dot(axisZ, MathHelper::POSITIVE_Z)));
	if (angle == 0.0f || angle == 180.0f) {
		// axisZ is parallel to MathHelper::POSITIVE_Z.
		return mat4(1.0);
	}
	/*
	 * http://stackoverflow.com/a/7785719/245148
	 * Cross product will point upward or downward depending on an angle
	 * between two vectors. If the angle is less than 180 degrees then
	 * it will point upward, otherwise it will point downward.
	 *
	 * Hence acos with its 0-180 degree range will be sufficient
	 * for whole range of 0-360 degree rotations.
	 */
	glm::vec3 axis = glm::cross(axisZ, MathHelper::POSITIVE_Z);
	return glm::rotate(angle, axis);
}

/**
 * http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/
 */
glm::quat MathHelper::getRotationQuat(const glm::vec3& a, const glm::vec3& b) {
	float angle = glm::degrees(acos(glm::dot(a, b)));
	if (angle == 0.0f || angle == 180.0f) {
		// vectors are parallel.
		return quat(); // unit quaternion.
	}
	glm::vec3 axis = glm::cross(a, b);

	return glm::angleAxis(angle, axis);
}

glm::mat4 MathHelper::getRotationMat(const glm::vec3& a, const glm::vec3& b) {
	float angle = glm::degrees(acos(glm::dot(a, b)));
	if (angle == 0.0f || angle == 180.0f) {
		// vectors are parallel.
		return mat4(1.0f); // unit matrix
	}
	glm::vec3 axis = glm::cross(a, b);
	return glm::rotate(angle, axis);
}
