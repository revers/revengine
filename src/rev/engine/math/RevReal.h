/*
 * RevReal.h
 *
 *  Created on: 01-05-2013
 *      Author: Revers
 */

#ifndef REVREAL_H_
#define REVREAL_H_

#include <float.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <rev/engine/config/RevEngineConfig.h>

namespace rev {

#ifdef REV_ENGINE_REAL_FLOAT
	typedef float real;
	/**
	 * Defines the highest value for the real number.
	 */
#define REAL_MAX FLT_MAX

#define real_sqrt sqrtf
#define real_abs fabsf
#define real_sin sinf
#define real_cos cosf
#define real_exp expf
#define real_pow powf
#define real_fmod fmodf

	/**
	 * Defines the number e on which 1+e == 1
	 */
#define real_epsilon FLT_EPSILON

#define R_PI 3.14159f
#elif defined(REV_ENGINE_REAL_DOUBLE)
	typedef double real;

	/**
	 * Defines the highest value for the real number.
	 */
#define REAL_MAX DBL_MAX
#define real_sqrt sqrt
#define real_abs fabs
#define real_sin sin
#define real_cos cos
#define real_exp exp
#define real_pow pow
#define real_fmod fmod

	/**
	 * Defines the number e on which 1+e == 1
	 */
#define real_epsilon DBL_EPSILON

#define R_PI 3.14159265358979
#endif

	/**
	 * Real vector 2D.
	 */
	typedef glm::detail::tvec2<real> rvec2;

	/**
	 * Real vector 3D.
	 */
	typedef glm::detail::tvec3<real> rvec3;

	/**
	 * Real vector 4D.
	 */
	typedef glm::detail::tvec4<real> rvec4;

	/**
	 * Real quaternion.
	 */
	typedef glm::detail::tquat<real> rquat;

	/**
	 * Real matrix 2x2.
	 */
	typedef glm::detail::tmat2x2<real> rmat2;

	/**
	 * Real matrix 3x3.
	 */
	typedef glm::detail::tmat3x3<real> rmat3;

	/**
	 * Real matrix 4x4.
	 */
	typedef glm::detail::tmat4x4<real> rmat4;
}

#endif /* REVREAL_H_ */
