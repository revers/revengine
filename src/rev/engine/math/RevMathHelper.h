/*
 * RevMathHelper.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVMATHHELPER_H_
#define REVMATHHELPER_H_

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <rev/gl/RevGLDefines.h>

#define EPSILON_F 0.00001f
#define EPSILON_D 0.0000000001

namespace rev {

	inline bool almostEqual(float x, float y) {
		return fabs(x - y) < EPSILON_F;
	}
	inline bool almostEqual(double x, double y) {
		return fabs(x - y) < EPSILON_D;
	}

	class MathHelper {
		MathHelper() {
		}
		~MathHelper() {
		}

	public:
		static const glm::vec3 POSITIVE_Z;
		static const glm::vec3 NEGATIVE_Z;
		static const glm::vec3 POSITIVE_Y;
		static const glm::vec3 NEGATIVE_Y;
		static const glm::vec3 POSITIVE_X;
		static const glm::vec3 NEGATIVE_X;

		static const glm::vec3 ZERO;
		static const glm::vec3 ONE;

		static const glm::mat4 IDENTITY_MATRIX;

		/**
		 * http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/#How_do_I_create_a_quaternion_in_C____
		 */
		static glm::quat rotationBetweenVectors(glm::vec3 start, glm::vec3 dest);

		/**
		 * If "v" is normal of a plane, then the return value of this function
		 * is vector lying on the plane (parallel to the plane).
		 */
		static glm::vec3 anyOrthogonal(const glm::vec3& v) {
			if (!almostEqual(v.x, v.y) || !almostEqual(v.y, v.z)) {
				// simple componenent permutation
				return glm::cross(v, glm::vec3(v.z, v.x, v.y));
			} else {
				// cross product with versor
				return glm::cross(v, glm::vec3(1.0f, 0.0f, 0.0f));
			}
		}

		/**
		 * Decomposes glm::perspective() matrix.
		 *
		 * @param p      [in]  a glm::perspective() matrix.
		 * @param fovy   [out] angle
		 * @param aspect [out] aspect ratio (width / height)
		 * @param near_  [out] near plane
		 * @param far_   [out] far plane
		 */
		static void decomposePerspective(const glm::mat4& p,
				float& fovy, float& aspect, float& near_, float& far_);

		/**
		 * Decomposes matrix M such that T * R * S = M, where T is translation matrix,
		 * R is rotation matrix and S is scaling matrix.
		 * http://code.google.com/p/assimp-net/source/browse/trunk/AssimpNet/Matrix4x4.cs
		 * (this method is exact to at least 0.0001f)
		 *
		 * | 1  0  0  T1 | | R11 R12 R13 0 | | a 0 0 0 |   | aR11 bR12 cR13 T1 |
		 * | 0  1  0  T2 |.| R21 R22 R23 0 |.| 0 b 0 0 | = | aR21 bR22 cR23 T2 |
		 * | 0  0  1  T3 | | R31 R32 R33 0 | | 0 0 c 0 |   | aR31 bR32 cR33 T3 |
		 * | 0  0  0   1 | |  0   0   0  1 | | 0 0 0 1 |   |  0    0    0    1 |
		 *
		 * @param m (in) matrix to decompose
		 * @param scaling (out) scaling vector
		 * @param rotation (out) rotation matrix
		 * @param translation (out) translation vector
		 */
		static void decomposeTRS(const glm::mat4& m, glm::vec3& scaling,
				glm::mat4& rotation, glm::vec3& translation);

		/**
		 * Decompose matrix M, such that M = QR, where Q is an orthogonal matrix, and R is some matrix.
		 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
		 *
		 * @param M matrix to decompose.
		 * @param Q out parameter
		 * @param R out parameter
		 */
		static void decomposeQR(const glm::mat4& M, glm::mat4& Q, glm::mat4& R);

		/**
		 * Decompose matrix M, such that M = RQ, where Q is an orthogonal matrix, and R is some matrix.
		 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
		 *
		 * @param M matrix to decompose.
		 * @param Q out parameter
		 * @param R out parameter
		 */
		static void decomposeRQ(const glm::mat4& M, glm::mat4& Q, glm::mat4& R);

		/**
		 * Creates quaternion from axis and angle (in radians).
		 */
		static glm::quat fromAxisAngle(const glm::vec3& axis, float angle) {
			glm::vec3 a = glm::normalize(axis);
			float s = sin(angle * 0.5);
			return glm::quat(cos(angle * 0.5), a.x * s, a.y * s, a.z * s);
		}

		/**
		 * Transforms vector v by rotation quaternion q.
		 * IMPORTANT: q should be normalized!
		 *
		 * http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/index.htm
		 */
		static glm::vec3 rotate(const glm::vec3& v, const glm::quat& q);

		/**
		 * Newton's binomial coefficient.
		 */
		template<typename T>
		static T biCoefficient(int n, int k) {
			if (k > n - k) {
				k = n - k;
			}

			T c(1);
			for (int i = 0; i < k; i++) {
				c = c * T(n - i);
				c = c / T(i + 1);
			}
			return c;
		}

		/**
		 * Transform the vector "v" by the transformational inverse
		 * of the matrix "m".
		 *
		 * This function works only when m = T * R, where T is
		 * a translation matrix and R is a rotation matrix.
		 *
		 */
		template<typename T>
		static inline glm::detail::tvec3<T> transformInverse(
				const glm::detail::tmat4x4<T>& m,
				const glm::detail::tvec3<T>& v) {
			/*
			 * T * R * v = u
			 * R * v = T' * u
			 * v = R' * T' * u
			 *
			 * R' = inverse(R)
			 * T' = inverse(T)
			 */
			glm::detail::tvec3<T> tmp(v.x - m[3][0], v.y - m[3][1], v.z - m[3][2]);
			return glm::detail::tvec3<T>(
					m[0][0] * tmp.x + m[0][1] * tmp.y + m[0][2] * tmp.z,
					m[1][0] * tmp.x + m[1][1] * tmp.y + m[1][2] * tmp.z,
					m[2][0] * tmp.x + m[2][1] * tmp.y + m[2][2] * tmp.z);
		}

		/**
		 * Transforms given vector "v" by rotation sub-matrix of the matrix "m".
		 */
		template<typename T>
		static inline glm::detail::tvec3<T> transformDirection(const glm::detail::tmat4x4<T>& m,
				const glm::detail::tvec3<T>& v) {
			return glm::detail::tvec3<T>(
					m[0][0] * v.x + m[1][0] * v.y + m[2][0] * v.z,
					m[0][1] * v.x + m[1][1] * v.y + m[2][1] * v.z,
					m[0][2] * v.x + m[1][2] * v.y + m[2][2] * v.z);
		}

		/**
		 * Transforms given vector "v" by transposed (inverted)
		 * rotation sub-matrix of the matrix "m".
		 */
		template<typename T>
		static inline glm::detail::tvec3<T> transformInverseDirection(
				const glm::detail::tmat4x4<T>& m,
				const glm::detail::tvec3<T>& v) {
			return glm::detail::tvec3<T>(
					m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
					m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
					m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z);
		}

		/**
		 * Transforms given vector "v" by transposed matrix "m".
		 */
		template<typename T>
		static inline glm::detail::tvec3<T> transformTranspose(
				const glm::detail::tmat3x3<T>& m,
				const glm::detail::tvec3<T>& v) {
			return glm::detail::tvec3<T>(
					m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
					m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
					m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z);
		}

		/**
		 * Sets the matrix to be a skew symmetric matrix based on
		 * the given vector. The skew symmetric matrix is the equivalent
		 * of the vector product. So if a,b are vectors. a x b = A_s b
		 * where A_s is the skew symmetric form of a.
		 */
		template<typename T>
		static inline glm::detail::tvec3<T> setSkewSymmetric(
				glm::detail::tmat3x3<T>& m,
				const glm::detail::tvec3<T>& v) {
			m[0][0] = T(0);
			m[0][1] = v.z;
			m[0][2] = -v.y;

			m[1][0] = -v.z;
			m[1][1] = T(0);
			m[1][2] = v.x;

			m[2][0] = v.y;
			m[2][1] = -v.x;
			m[2][2] = T(0);
		}

		/**
		 * Gets rotation quaternion between two vectors.
		 *
		 * IMPORTANT: vectors "a" and "b" should be normalized!
		 */
		static glm::quat getRotationQuat(const glm::vec3& a, const glm::vec3& b);

		/**
		 * Gets rotation matrix between two vectors.
		 *
		 * IMPORTANT: vectors "a" and "b" should be normalized!
		 */
		static glm::mat4 getRotationMat(const glm::vec3& a, const glm::vec3& b);

		/**
		 * Creates rotation matrix from transformed axis X.
		 *
		 * IMPORTANT: axisX should be normalized!
		 */
		static glm::mat4 getRotationFromAxisX(const glm::vec3& axisX);

		/**
		 * Creates rotation matrix from transformed axis Y.
		 *
		 * IMPORTANT: axisY should be normalized!
		 */
		static glm::mat4 getRotationFromAxisY(const glm::vec3& axisY);

		/**
		 * Creates rotation matrix from transformed axis Z.
		 *
		 * IMPORTANT: axisZ should be normalized!
		 */
		static glm::mat4 getRotationFromAxisZ(const glm::vec3& axisZ);

	private:
		static glm::mat4 multTranspose(const glm::vec4& A, const glm::vec4& B) {
			return glm::mat4(
					A[0] * B[0], A[0] * B[1], A[0] * B[2], A[0] * B[3],
					A[1] * B[0], A[1] * B[1], A[1] * B[2], A[1] * B[3],
					A[2] * B[0], A[2] * B[1], A[2] * B[2], A[2] * B[3],
					A[3] * B[0], A[3] * B[1], A[3] * B[2], A[3] * B[3]);
		}

	};

	/**
	 * Multiplies 4D matrix with 3D vector.
	 */
	template<typename T>
	inline glm::detail::tvec3<T> mul(const glm::detail::tmat4x4<T>& m,
			const glm::detail::tvec3<T>& v) {

		return glm::detail::tvec3<T>(
				m[0][0] * v.x + m[1][0] * v.y + m[2][0] * v.z + m[3][0],
				m[0][1] * v.x + m[1][1] * v.y + m[2][1] * v.z + m[3][1],
				m[0][2] * v.x + m[1][2] * v.y + m[2][2] * v.z + m[3][2]);
	}

} /* namespace rev */
#endif /* REVMATHHELPER_H_ */

