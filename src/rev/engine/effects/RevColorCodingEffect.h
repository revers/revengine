/*
 * RevColorCodingEffect.h
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#ifndef REVCOLORCODINGEFFECT_H_
#define REVCOLORCODINGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>

#include "RevIEffect.h"

#define COLOR_CODING_EFFECT "Color Coding"

namespace rev {

    class ColorCodingEffect: public IEffect {
        int drawIndex = 0;
        int objectIndex = 0;

        int objectIndexLocation = -1;
        int drawIndexLocation = -1;
        int mvpMatrixLocation = -1;

    public:
        DECLARE_BINDABLE(ColorCodingEffect)

        ColorCodingEffect(rev::GLSLProgram* program);
        ColorCodingEffect();

        const char* getName() override {
            return COLOR_CODING_EFFECT;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

        int getDrawIndex() const {
            return drawIndex;
        }

        void setDrawIndex(int drawIndex) {
            this->drawIndex = drawIndex;
        }

        int getObjectIndex() const {
            return objectIndex;
        }

        void setObjectIndex(int objectIndex) {
            this->objectIndex = objectIndex;
        }

    private:
        void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVCOLORCODINGEFFECT_H_ */
