/*
 * RevStripesTextureEffect.cpp
 *
 *  Created on: 05-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevStripesTextureEffect.h"

using namespace rev;
using namespace glm;

#define STRIPES_TEXTURE_SHADER_FILE "shaders/StripesTexture.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(StripesTextureEffect, nullptr)

rev::IBindable* StripesTextureEffect::staticCreateBindable() {
	return new StripesTextureEffect(nullptr);
}

bool StripesTextureEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

StripesTextureEffect::StripesTextureEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

StripesTextureEffect::StripesTextureEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void StripesTextureEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("LightPosition");
	lightColorLocation = program->getUniformLocation("LightColor");
	specularLocation = program->getUniformLocation("Specular");
	ambientLocation = program->getUniformLocation("Ambient");
	stripeColorLocation = program->getUniformLocation("StripeColor");
	backColorLocation = program->getUniformLocation("BackColor");
	widthLocation = program->getUniformLocation("Width");
	fuzzLocation = program->getUniformLocation("Fuzz");
	scaleLocation = program->getUniformLocation("Scale");
	kdLocation = program->getUniformLocation("Kd");
	mvMatrixLocation = program->getUniformLocation("MVMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpMatrixLocation = program->getUniformLocation("MVPMatrix");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& specularLocation >= 0
			&& ambientLocation >= 0
			&& stripeColorLocation >= 0
			&& backColorLocation >= 0
			&& widthLocation >= 0
			&& fuzzLocation >= 0
			&& scaleLocation >= 0
			&& kdLocation >= 0
			&& mvMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpMatrixLocation >= 0);
}

void StripesTextureEffect::setDefaultValues() {
	lightPosition = glm::vec3(100.0f, 100.0f, 100.0f);
	lightColor = rev::color3(1.0f, 1.0f, 1.0f);
	specular = rev::color3(0.95f, 0.95f, 0.95f);
	ambient = rev::color3(0.3f, 0.3f, 0.3f);
	stripeColor = rev::color3(0.9f, 0.9f, 0.9f);
	backColor = rev::color3(0.0f, 0.0f, 0.0f);
	scale = 10.0f;
	fuzz = 0.1f;
	width = 0.5f;
	kd = 0.5f;
}

void StripesTextureEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();
	setProperty(lightPositionLocation, lightPosition);
	setProperty(lightColorLocation, lightColor);
	setProperty(specularLocation, specular);
	setProperty(ambientLocation, ambient);
	setProperty(stripeColorLocation, stripeColor);
	setProperty(backColorLocation, backColor);
	setProperty(widthLocation, width);
	setProperty(fuzzLocation, fuzz);
	setProperty(scaleLocation, scale);
	setProperty(kdLocation, kd);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpMatrixLocation, projectionMatrix * mv);
}

rev::GLSLProgram* StripesTextureEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(STRIPES_TEXTURE_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 2, "VertexTexCoord");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void StripesTextureEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(width);
	binder.bindSimple(fuzz);
	binder.bindSimple(scale);
	binder.bindSimple(kd);
	binder.bindSimple(lightColor);
	binder.bindSimple(specular);
	binder.bindSimple(ambient);
	binder.bindSimple(stripeColor);
	binder.bindSimple(backColor);
}
