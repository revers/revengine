/*
 * RevControlPointsEffect.cpp
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevControlPointsEffect.h"

using namespace rev;
using namespace glm;

#define CONTROL_POINTS_SHADER_FILE "shaders/ControlPoints.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(ControlPointsEffect, nullptr)

rev::IBindable* ControlPointsEffect::staticCreateBindable() {
	return new ControlPointsEffect(nullptr);
}

bool ControlPointsEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

ControlPointsEffect::ControlPointsEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

ControlPointsEffect::ControlPointsEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void ControlPointsEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	pointScale = camera.getScreenWidth() / tanf(camera.getCameraAngle() * halfViewRadianFactor);

	setProperty(pointRadiusLocation, pointRadius);
	setProperty(pointScaleLocation, pointScale);
	setProperty(highlightColorLocation, highlightColor);
	setProperty(highlightPositionLocation, highlightPosition);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvLocation, mv);
	setProperty(mvpLocation, projectionMatrix * mv);
}

void ControlPointsEffect::findAttributeLocations() {
	mvLocation = program->getUniformLocation("ModelViewMatrix");
	mvpLocation = program->getUniformLocation("MVPMatrix");
	pointRadiusLocation = program->getUniformLocation("PointRadius");
	pointScaleLocation = program->getUniformLocation("PointScale");
	highlightColorLocation = program->getUniformLocation("HighlightColor");
	highlightPositionLocation = program->getUniformLocation("HighlightPosition");

	revAssert(mvLocation >= 0
			&& mvpLocation >= 0
			&& pointRadiusLocation >= 0
			&& pointScaleLocation >= 0
			&& highlightColorLocation >= 0
			&& highlightPositionLocation >= 0);
}

void ControlPointsEffect::setDefaultValues() {
	highlightColor = rev::color3(0, 1, 0);
	setInvalidHighlightPosition();
	pointRadius = 0.1f;
	float fov = 60.0f;
	int screenWidth = 800;
	pointScale = (float) screenWidth / tanf(fov * halfViewRadianFactor);
}

GLSLProgram* ControlPointsEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(CONTROL_POINTS_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexColor");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ControlPointsEffect::bind(IBinder& binder) {
	binder.bindSimple(highlightColor);
	binder.bindInfo(this, "highlightPosition", highlightPosition);
	binder.bindInfo(this, "pointRadius", pointRadius);
	binder.bindInfo(this, "pointScale", pointScale);

}
