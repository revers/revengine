/*
 * RevNormalVisualizer.cpp
 *
 *  Created on: 11-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevNormalVisualizerEffect.h"

using namespace rev;
using namespace glm;

#define NORMAL_VISUALIZER_SHADER_FILE "shaders/NormalVisualizer.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(NormalVisualizerEffect, nullptr)

rev::IBindable* NormalVisualizerEffect::staticCreateBindable() {
	return new NormalVisualizerEffect(nullptr);
}

bool NormalVisualizerEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

NormalVisualizerEffect::NormalVisualizerEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

NormalVisualizerEffect::NormalVisualizerEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void NormalVisualizerEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	setProperty(colorLocation, color);
	setProperty(lengthLocation, length);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvpLocation, projectionMatrix * mv);
}

void NormalVisualizerEffect::findAttributeLocations() {
	colorLocation = program->getUniformLocation("Color");
	lengthLocation = program->getUniformLocation("Length");
	mvpLocation = program->getUniformLocation("MVP");

	revAssert(colorLocation >= 0
			&& lengthLocation >= 0
			&& mvpLocation >= 0);
}

void NormalVisualizerEffect::setDefaultValues() {
	color = rev::color3(1.0, 0.0, 0.55);
	length = 1.0;
}

GLSLProgram* NormalVisualizerEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(NORMAL_VISUALIZER_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void NormalVisualizerEffect::bind(IBinder& binder) {
	binder.bindSimple(length);
	binder.bindSimple(color);
}
