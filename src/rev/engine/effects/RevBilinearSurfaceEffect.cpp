/*
 * RevBilinearSurfaceEffect.cpp
 *
 *  Created on: 18-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevBilinearSurfaceEffect.h"
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

#define BILINEAR_SURFACE_SHADER_FILE "shaders/BilinearSurface.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(BilinearSurfaceEffect, nullptr)

rev::IBindable* BilinearSurfaceEffect::staticCreateBindable() {
	return new BilinearSurfaceEffect(nullptr);
}

bool BilinearSurfaceEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

BilinearSurfaceEffect::BilinearSurfaceEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

BilinearSurfaceEffect::BilinearSurfaceEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void BilinearSurfaceEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();
	setProperty(ambientColorLocation, ambientColor);
	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(lightColorLocation, lightColor);
	setProperty(lightPositionLocation, viewMatrix * lightPosition);
	setProperty(shininessLocation, shininess);
	setProperty("C00", C[0][0]);
	setProperty("C10", C[1][0]);
	setProperty("C01", C[0][1]);
	setProperty("C11", C[1][1]);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(modelViewMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void BilinearSurfaceEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("LightPosition");
	lightColorLocation = program->getUniformLocation("LightColor");
	diffuseColorLocation = program->getUniformLocation("Kd");
	ambientColorLocation = program->getUniformLocation("Ka");
	specularColorLocation = program->getUniformLocation("Ks");
	shininessLocation = program->getUniformLocation("Shininess");

	modelViewMatrixLocation = program->getUniformLocation("ModelViewMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& diffuseColorLocation >= 0
			&& ambientColorLocation >= 0
			&& specularColorLocation >= 0
			&& shininessLocation >= 0
			&& modelViewMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpLocation >= 0);
}

void BilinearSurfaceEffect::setDefaultValues() {
	lightPosition = glm::vec4(100.0f, 100.0f, 100.0f, 1.0f);
	lightColor = rev::color3(1.0f, 1.0f, 1.0f);
	diffuseColor = rev::color3(0.9f, 0.9f, 0.9f);
	specularColor = rev::color3(0.95f, 0.95f, 0.95f);
	ambientColor = rev::color3(0.1f, 0.1f, 0.1f);
	shininess = 100.0f;

	M[0][0] = -1.0;
	M[0][1] = 1.0;
	M[1][0] = 1.0;
	M[1][1] = 0.0;

	vec3 shift(0.5, 0.5, 0.5);
	setPoints(vec3(0, 0, 1) - shift,
			vec3(1, 0, 0) - shift,
			vec3(1, 1, 1) - shift,
			vec3(0, 1, 0) - shift);
//    setPoints(vec3(0, 0, 1) - shift,
//            vec3(0, 1, 0)  - shift,
//            vec3(1, 0, 0) - shift,
//            vec3(1, 1, 1) - shift);

//    setPoints(vec3(0, 0, 1),
//            vec3(1, 1, 1),
//            vec3(1, 0, 0),
//            vec3(0, 1, 0));

}

GLSLProgram* BilinearSurfaceEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(BILINEAR_SURFACE_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 2, "TexCoord");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void BilinearSurfaceEffect::calculateC() {
	// U(u) = (u, 1)
	// W(w) = (w, 1)
	// P(u, w) = U(u) * M * P * M^T * W(w)^T
	// P(u, w) = U(u) * M * T * W(w)^T
	// P(u, w) = U(u) * C * W(w)^T
	glm::vec3 T[2][2];

	// T = P  * M^T
	const int dim = 2;
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {
			T[i][j] = glm::vec3(0);
			for (int k = 0; k < dim; k++) {
				T[i][j] += P[i][k] * M[j][k];
			}
		}
	}

	// C = M * T
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {
			C[i][j] = glm::vec3(0);
			for (int k = 0; k < dim; k++) {
				C[i][j] += M[i][k] * T[k][j];
			}
		}
	}
}

void BilinearSurfaceEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(lightColor);
	binder.bindSimple(diffuseColor);
	binder.bindSimple(ambientColor);
	binder.bindSimple(specularColor);
	binder.bindSimple(shininess);
}
