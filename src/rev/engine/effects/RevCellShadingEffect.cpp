/*
 * RevCellShadingEffect.cpp
 *
 *  Created on: 28 cze 2013
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevCellShadingEffect.h"
#include <rev/engine/RevRenderer3D.h>
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace glm;

#define CELL_SHADING_SHADER_FILE "shaders/CellShading.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(CellShadingEffect, nullptr)

rev::IBindable* CellShadingEffect::staticCreateBindable() {
	return new CellShadingEffect(nullptr);
}

bool CellShadingEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

CellShadingEffect::CellShadingEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

CellShadingEffect::CellShadingEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void CellShadingEffect::apply(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix, const ICamera& camera) {
	program->use();
	setProperty(ambientColorLocation, ambientColor);
	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(lightPositionLocation, viewMatrix * lightPosition);
	setProperty(shininessLocation, shininess);
	setProperty(antiAliasLocation, antiAlias);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void CellShadingEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("LightPosition");
	diffuseColorLocation = program->getUniformLocation("Kd");
	ambientColorLocation = program->getUniformLocation("Ka");
	specularColorLocation = program->getUniformLocation("Ks");
	shininessLocation = program->getUniformLocation("Shininess");
	antiAliasLocation = program->getUniformLocation("AntiAlias");

	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");

	revAssert(lightPositionLocation >= 0
			&& diffuseColorLocation >= 0 && ambientColorLocation >= 0
			&& specularColorLocation >= 0 && shininessLocation >= 0
			&& normalMatrixLocation >= 0 && mvpLocation >= 0
			&& antiAliasLocation >= 0);
}

void CellShadingEffect::setDefaultValues() {
	lightPosition = glm::vec4(100.0f, 100.0f, 100.0f, 1.0f);
	diffuseColor = rev::color3(0.9f, 0.9f, 0.9f);
	specularColor = rev::color3(0.95f, 0.95f, 0.95f);
	ambientColor = rev::color3(0.1f, 0.1f, 0.1f);
	antiAlias = true;
	shininess = 100.0f;
}

GLSLProgram* CellShadingEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(CELL_SHADING_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void CellShadingEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(diffuseColor);
	binder.bindSimple(ambientColor);
	binder.bindSimple(specularColor);
	binder.bindSimple(shininess);
	binder.bindSimple(antiAlias);
}

