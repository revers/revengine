/*
 * RevDebugEffect.cpp
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevDebugEffect.h"

using namespace rev;
using namespace glm;

#define DEBUG_SHADER_FILE "shaders/Debug.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(DebugEffect, nullptr)

rev::IBindable* DebugEffect::staticCreateBindable() {
	return new DebugEffect(nullptr);
}

bool DebugEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

DebugEffect::DebugEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

DebugEffect::DebugEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void DebugEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	setProperty(colorLocation, color);
	mat3 rot = mat3(viewMatrix);
	setProperty(lightDirectionLocation, rot * lightDirection);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void DebugEffect::findAttributeLocations() {
	colorLocation = program->getUniformLocation("Color");
	lightDirectionLocation = program->getUniformLocation("LightDirection");

	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");

	revAssert(colorLocation >= 0
			&& mvpLocation >= 0
			&& lightDirectionLocation >= 0
			&& normalMatrixLocation >= 0);
}

void DebugEffect::setDefaultValues() {
	color = rev::color3(1.0, 0.67, 0.2);
	lightDirection = glm::normalize(glm::vec3(0.6, -1.0, 1.0));
}

GLSLProgram* DebugEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(DEBUG_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void DebugEffect::bind(IBinder& binder) {
	binder.bindSimple(lightDirection);
	binder.bindSimple(color);
}
