/*
 * RevPointSpheresEffect.cpp
 *
 *  Created on: 11-04-2013
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevPointSpheresEffect.h"

using namespace rev;
using namespace glm;

#define POINT_SPHERES_SHADER_FILE "shaders/PointSpheres.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(PointSpheresEffect, nullptr)

rev::IBindable* PointSpheresEffect::staticCreateBindable() {
	return new PointSpheresEffect(nullptr);
}

bool PointSpheresEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

PointSpheresEffect::PointSpheresEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

PointSpheresEffect::PointSpheresEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void PointSpheresEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	pointScale = camera.getScreenWidth() / tanf(camera.getCameraAngle() * halfViewRadianFactor);

	setProperty(pointRadiusLocation, pointRadius);
	setProperty(pointScaleLocation, pointScale);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvLocation, mv);
	setProperty(mvpLocation, projectionMatrix * mv);
}

void PointSpheresEffect::findAttributeLocations() {
	mvLocation = program->getUniformLocation("ModelViewMatrix");
	mvpLocation = program->getUniformLocation("MVPMatrix");
	pointRadiusLocation = program->getUniformLocation("PointRadius");
	pointScaleLocation = program->getUniformLocation("PointScale");

	revAssert(mvLocation >= 0
			&& mvpLocation >= 0
			&& pointRadiusLocation >= 0
			&& pointScaleLocation >= 0);
}

void PointSpheresEffect::setDefaultValues() {
	pointRadius = 0.1f;
	float fov = 60.0f;
	int screenWidth = 800;
	pointScale = (float) screenWidth / tanf(fov * halfViewRadianFactor);
}

GLSLProgram* PointSpheresEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(POINT_SPHERES_SHADER_FILE);
		if (!program->compileShaderGLSLFile(path.c_str())) {
			REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexColor");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void PointSpheresEffect::bind(IBinder& binder) {
	binder.bindSimple(pointRadius);
	binder.bindInfo(this, "PointScale", pointScale);
}
