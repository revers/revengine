/*
 * RevStencilOutlineEffect.h
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */

#ifndef REVSTENCILOUTLINEEFFECT_H_
#define REVSTENCILOUTLINEEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>

#include "RevIEffect.h"

#define FLAT_COLOR_EFFECT "Flat Color"

namespace rev {

    class StencilOutlineEffect: public IEffect {
        glm::mat4 scaleMatrix;
        rev::color3 color;
        int mvpMatrixLocation = -1;
        int colorLocation = -1;
        int scaleMatrixLocation = -1;

    public:
        DECLARE_BINDABLE(StencilOutlineEffect)

        StencilOutlineEffect(rev::GLSLProgram* program);
        StencilOutlineEffect();

        const char* getName() override {
            return FLAT_COLOR_EFFECT;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

        const rev::color3& getColor() const {
            return color;
        }

        void setColor(const rev::color3& color) {
            this->color = color;
        }

        const glm::mat4& getScaleMatrix() const {
            return scaleMatrix;
        }

        void setScaleMatrix(const glm::mat4& scaleMatrix) {
            this->scaleMatrix = scaleMatrix;
        }

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVSTENCILOUTLINEEFFECT_H_ */
