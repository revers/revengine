/*
 * RevPointSpheresEffect.h
 *
 *  Created on: 11-04-2013
 *      Author: Revers
 */

#ifndef REVPOINTSPHERESEFFECT_H_
#define REVPOINTSPHERESEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define POINT_SPHERES_EFFECT "PointSpheres"

namespace rev {

    class PointSpheresEffect: public IEffect {
        float pointRadius = 1.0f;
        float pointScale = 1.0f;

        const float halfViewRadianFactor = 0.5f * 3.1415926535f / 180.0f;
        int mvpLocation = -1;
        int mvLocation = -1;
        int pointRadiusLocation = -1;
        int pointScaleLocation = -1;

    public:
        DECLARE_BINDABLE(PointSpheresEffect)

        PointSpheresEffect(rev::GLSLProgram* program);
        PointSpheresEffect();

        const char* getName() override {
            return POINT_SPHERES_EFFECT;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        void setPointRadius(float pointRadius) {
        	this->pointRadius = pointRadius;
        }

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVPOINTSPHERESEFFECT_H_ */
