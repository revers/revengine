/*
 * RevBilinearSurfaceEffect.h
 *
 *  Created on: 18-12-2012
 *      Author: Revers
 */

#ifndef REVBILINEARSURFACEEFFECT_H_
#define REVBILINEARSURFACEEFFECT_H_
#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define BILINEAR_SURFACE_EFFECT "Bilinear Surface"

namespace rev {

    /**
     * This shader require object with following vertex attributes:
     * 1. position
     * 2. normal
     */
    class BilinearSurfaceEffect: public IEffect {
        glm::vec4 lightPosition;
        rev::color3 lightColor;
        rev::color3 diffuseColor; // Diffuse reflectivity
        rev::color3 ambientColor; // Ambient reflectivity
        rev::color3 specularColor; // Specular reflectivity
        float shininess; // Specular shininess factor

        int lightPositionLocation = -1;
        int lightColorLocation = -1;
        int diffuseColorLocation = -1;
        int ambientColorLocation = -1;
        int specularColorLocation = -1;
        int shininessLocation = -1;
        int modelViewMatrixLocation = -1;
        int normalMatrixLocation = -1;
        int mvpLocation = -1;

        glm::vec3 P[2][2];
        glm::vec3 C[2][2];
        float M[2][2];

    public:
        DECLARE_BINDABLE(BilinearSurfaceEffect)

        BilinearSurfaceEffect(rev::GLSLProgram* program);
        BilinearSurfaceEffect();

        const char* getName() override {
            return BILINEAR_SURFACE_EFFECT;
        }

        const rev::color3& getAmbientColor() const {
            return ambientColor;
        }

        void setAmbientColor(const rev::color3& ambientColor) {
            this->ambientColor = ambientColor;
        }

        const rev::color3& getDiffuseColor() const {
            return diffuseColor;
        }

        void setDiffuseColor(const rev::color3& diffuseColor) {
            this->diffuseColor = diffuseColor;
        }

        const rev::color3& getLightColor() const {
            return lightColor;
        }

        void setLightColor(const rev::color3& lightColor) {
            this->lightColor = lightColor;
        }

        const glm::vec4& getLightPosition() const {
            return lightPosition;
        }

        void setLightPosition(const glm::vec4& lightPosition) {
            this->lightPosition = lightPosition;
        }

        float getShininess() const {
            return shininess;
        }

        void setShininess(float shininess) {
            this->shininess = shininess;
        }

        const rev::color3& getSpecularColor() const {
            return specularColor;
        }

        void setSpecularColor(const rev::color3& specularColor) {
            this->specularColor = specularColor;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

        const glm::vec3& getP00() const {
            return P[0][0];
        }

        void setP00(const glm::vec3& p00) {
            P[0][0] = p00;
            calculateC();
        }

        const glm::vec3& getP01() const {
            return P[0][1];
        }

        void setP01(const glm::vec3& p01) {
            P[0][1] = p01;
            calculateC();
        }

        const glm::vec3& getP10() const {
            return P[1][0];
        }

        void setP10(const glm::vec3& p10) {
            P[1][0] = p10;
            calculateC();
        }

        const glm::vec3& getP11() const {
            return P[1][1];
        }

        void setP11(const glm::vec3& p11) {
            P[1][1] = p11;
            calculateC();
        }

        void setPoints(const glm::vec3& p00,
                const glm::vec3& p01,
                const glm::vec3& p10,
                const glm::vec3& p11) {
            P[0][0] = p00;
            P[0][1] = p01;
            P[1][0] = p10;
            P[1][1] = p11;
            calculateC();
        }

        void setBasisMatrix(float m00, float m01,
                float m10, float m11) {
            M[0][0] = m00;
            M[0][1] = m01;
            M[1][0] = m10;
            M[1][1] = m11;
            calculateC();
        }

    private:
        void setDefaultValues();
		void findAttributeLocations();
        void calculateC();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVBILINEARSURFACEEFFECT_H_ */
