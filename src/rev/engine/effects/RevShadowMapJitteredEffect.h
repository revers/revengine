/*
 * RevShadowMapJitteredJitteredEffect.h
 *
 *  Created on: 28 lip 2013
 *      Author: Revers
 */

#ifndef REVSHADOWMAPJITTEREDEFFECT_H_
#define REVSHADOWMAPJITTEREDEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define SHADOW_MAP_JITTEREDEFFECT "Phong Shading"

namespace rev {

    class ShadowMapJitteredEffect: public IEffect {
        glm::vec4 lightPosition;
        rev::color3 lightColor;
        rev::color3 diffuseColor; // Diffuse reflectivity
        rev::color3 ambientColor; // Ambient reflectivity
        rev::color3 specularColor; // Specular reflectivity
        float shininess; // Specular shininess factor
        float radius = 7.0f;

        int lightPositionLocation = -1;
        int lightColorLocation = -1;
        int diffuseColorLocation = -1;
        int ambientColorLocation = -1;
        int specularColorLocation = -1;
        int shininessLocation = -1;

        int modelViewMatrixLocation = -1;
        int normalMatrixLocation = -1;
        int mvpLocation = -1;

        int shadowMatrixLocation = -1;
        int shadowMapLocation = -1;

        int offsetTexLocation = -1;
        int radiusLocation = -1;
        int offsetTexSizeLocation = -1;

    public:
        DECLARE_BINDABLE(ShadowMapJitteredEffect)

        ShadowMapJitteredEffect(rev::GLSLProgram* program);
        ShadowMapJitteredEffect();

        const char* getName() override {
            return SHADOW_MAP_JITTEREDEFFECT;
        }

        const rev::color3& getAmbientColor() const {
            return ambientColor;
        }

        void setAmbientColor(const rev::color3& ambientColor) {
            this->ambientColor = ambientColor;
        }

        const rev::color3& getDiffuseColor() const {
            return diffuseColor;
        }

        void setDiffuseColor(const rev::color3& diffuseColor) {
            this->diffuseColor = diffuseColor;
        }

        const rev::color3& getLightColor() const {
            return lightColor;
        }

        void setLightColor(const rev::color3& lightColor) {
            this->lightColor = lightColor;
        }

        const glm::vec4& getLightPosition() const {
            return lightPosition;
        }

        void setLightPosition(const glm::vec4& lightPosition) {
            this->lightPosition = lightPosition;
        }

        float getShininess() const {
            return shininess;
        }

        void setShininess(float shininess) {
            this->shininess = shininess;
        }

        const rev::color3& getSpecularColor() const {
            return specularColor;
        }

        void setSpecularColor(const rev::color3& specularColor) {
            this->specularColor = specularColor;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVSHADOWMAPJITTEREDEFFECT_H_ */
