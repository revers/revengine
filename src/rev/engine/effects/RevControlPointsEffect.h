/*
 * RevControlPointsEffect.h
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */

#ifndef REVCONTROLPOINTSEFFECT_H_
#define REVCONTROLPOINTSEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define CONTROL_POINTS_EFFECT "ControlPoints"

namespace rev {

	class ControlPointsEffect: public IEffect {
		float pointRadius = 1.0f;
		float pointScale = 1.0f;
		rev::color3 highlightColor = rev::color3(0, 1, 0);
		glm::vec3 highlightPosition;

		const float halfViewRadianFactor = 0.5f * 3.1415926535f / 180.0f;
		int mvpLocation = -1;
		int mvLocation = -1;
		int pointRadiusLocation = -1;
		int pointScaleLocation = -1;
		int highlightColorLocation = -1;
		int highlightPositionLocation = -1;

	public:
		DECLARE_BINDABLE(ControlPointsEffect)

		ControlPointsEffect(rev::GLSLProgram* program);
		ControlPointsEffect();

		const char* getName() override {
			return CONTROL_POINTS_EFFECT;
		}

		virtual void apply(
				const glm::mat4& modelMatrix,
				const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix,
				const ICamera& camera) override;

		void setPointRadius(float pointRadius) {
			this->pointRadius = pointRadius;
		}
		const rev::color3& getHighlightColor() const {
			return highlightColor;
		}
		void setHighlightColor(const rev::color3& highlightColor) {
			this->highlightColor = highlightColor;
		}
		const glm::vec3& getHighlightPosition() const {
			return highlightPosition;
		}
		void setHighlightPosition(const glm::vec3& highlightPosition) {
			this->highlightPosition = highlightPosition;
		}

		/**
		 * Sets some highly improbable highlight position.
		 */
		void setInvalidHighlightPosition() {
			this->highlightPosition = glm::vec3(-99999999, -9999999, -9999999);
		}

		static rev::GLSLProgram* createGLSLProgram();

	private:
		void setDefaultValues();
		void findAttributeLocations();
		bool initAfterDeserialization() override;

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCONTROLPOINTSEFFECT_H_ */
