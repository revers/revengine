/*
 * RevDeferredLightingEffect.h
 *
 *  Created on: 2 sie 2013
 *      Author: Revers
 */

#ifndef REVDEFERREDLIGHTINGEFFECT_H_
#define REVDEFERREDLIGHTINGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define DEFERRED_LIGHTING_EFFECT "DeferredLighting"

namespace rev {

class DeferredLightingEffect: public IEffect {
	rev::color3 diffuseColor;
	rev::color3 ambientColor;

	int diffuseColorLocation = -1;
	int ambientColorLocation = -1;
	int mvpLocation = -1;
	int lightTexLocation = -1;
	int lightViewportLocation = -1;

public:
	DECLARE_BINDABLE(DeferredLightingEffect)

	DeferredLightingEffect(rev::GLSLProgram* program);
	DeferredLightingEffect();

	const char* getName() override {
		return DEFERRED_LIGHTING_EFFECT;
	}

	const rev::color3& getAmbientColor() const {
		return ambientColor;
	}

	void setAmbientColor(const rev::color3& ambientColor) {
		this->ambientColor = ambientColor;
	}

	const rev::color3& getDiffuseColor() const {
		return diffuseColor;
	}

	void setDiffuseColor(const rev::color3& diffuseColor) {
		this->diffuseColor = diffuseColor;
	}

	virtual void apply(
			const glm::mat4& modelMatrix,
			const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix,
			const ICamera& camera) override;

	static rev::GLSLProgram* createGLSLProgram();

private:
	void setDefaultValues();
	void findAttributeLocations();
	bool initAfterDeserialization() override;

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVDEFERREDLIGHTINGEFFECT_H_ */
