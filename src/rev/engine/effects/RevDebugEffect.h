/*
 * RevDebugEffect.h
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */

#ifndef REVDEBUGEFFECT_H_
#define REVDEBUGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define DEBUG_EFFECT "Debug"

namespace rev {

    class DebugEffect: public IEffect {
        rev::color3 color;
        glm::vec3 lightDirection;

        int colorLocation = -1;
        int mvpLocation = -1;
        int lightDirectionLocation = -1;
        int normalMatrixLocation = -1;

    public:
        DECLARE_BINDABLE(DebugEffect)

        DebugEffect(rev::GLSLProgram* program);
        DebugEffect();

        const char* getName() override {
            return DEBUG_EFFECT;
        }

        const rev::color3& getColor() const {
            return color;
        }

        void setColor(const rev::color3& color) {
            this->color = color;
        }

        const glm::vec3& getLightDirection() const {
            return lightDirection;
        }

        void setLightDirection(const glm::vec3& lightDirection) {
            this->lightDirection = lightDirection;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVDEBUGEFFECT_H_ */
