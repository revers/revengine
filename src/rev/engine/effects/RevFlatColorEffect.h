/*
 * RevFlatColorEffect.h
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */

#ifndef REVFLATCOLOREFFECT_H_
#define REVFLATCOLOREFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>

#include "RevIEffect.h"

#define FLAT_COLOR_EFFECT "Flat Color"

namespace rev {

	class FlatColorEffect: public IEffect {
		int mvpMatrixLocation = -1;

	public:
		DECLARE_BINDABLE(FlatColorEffect)

		FlatColorEffect(rev::GLSLProgram* program);
		FlatColorEffect();

		const char* getName() override {
			return FLAT_COLOR_EFFECT;
		}

		virtual void apply(
				const glm::mat4& modelMatrix,
				const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix,
				const ICamera& camera) override;

		static rev::GLSLProgram* createGLSLProgram();

	private:
		void findAttributeLocations();
		bool initAfterDeserialization() override;

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVFLATCOLOREFFECT_H_ */
