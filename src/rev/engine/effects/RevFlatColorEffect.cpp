/*
 * RevFlatColorEffect.cpp
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevFlatColorEffect.h"

using namespace rev;
using namespace glm;

#define FLAT_COLOR_SHADER_FILE "shaders/FlatColor.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(FlatColorEffect, nullptr)

rev::IBindable* FlatColorEffect::staticCreateBindable() {
	return new FlatColorEffect(nullptr);
}

bool FlatColorEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

FlatColorEffect::FlatColorEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	if (program) {
		findAttributeLocations();
	}
}

FlatColorEffect::FlatColorEffect() :
		IEffect(createGLSLProgram(), true) {
	findAttributeLocations();
}

void FlatColorEffect::findAttributeLocations() {
	mvpMatrixLocation = program->getUniformLocation("MVPMatrix");
	revAssert(mvpMatrixLocation >= 0);
}

void FlatColorEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvpMatrixLocation, projectionMatrix * mv);
}

rev::GLSLProgram* FlatColorEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(FLAT_COLOR_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexColor");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void FlatColorEffect::bind(IBinder& binder) {
}
