/*
 * RevIEffect.h
 *
 *  Created on: 28-11-2012
 *      Author: Revers
 */

#ifndef REVIEFFECT_H_
#define REVIEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>
#include <rev/engine/camera/RevICamera.h>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

    class IEffect: public IBindable {
    protected:
    	bool owner = false;
        GLSLProgram* program;

        IEffect() {
        }

    public:
        IEffect(GLSLProgram* program, bool owner = true) :
                program(program), owner(owner) {
        }

        virtual ~IEffect() {
            if (owner) {
                delete program;
            }
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) = 0;

        virtual const char* getName() = 0;

        GLSLProgram* getProgram() {
            return program;
        }

        template<typename T>
        void setProperty(const char* name, const T& val) {
            program->setUniform(name, val);
        }

        template<typename T>
        void setProperty(int location, const T& val) {
            program->setUniform(location, val);
        }
    };
}

#endif /* REVIEFFECT_H_ */
