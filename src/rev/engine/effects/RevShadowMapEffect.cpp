/*
 * RevShadowMapEffect.cpp
 *
 *  Created on: 28 lip 2013
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/shadow/RevShadowManager.h>
#include "RevShadowMapEffect.h"

using namespace rev;
using namespace glm;

#define SHADOW_MAP_SHADER_FILE "shaders/ShadowMap.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(ShadowMapEffect, nullptr)

rev::IBindable* ShadowMapEffect::staticCreateBindable() {
	return new ShadowMapEffect(nullptr);
}

bool ShadowMapEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

ShadowMapEffect::ShadowMapEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

ShadowMapEffect::ShadowMapEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void ShadowMapEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	ShadowManager::ref().bindDepthTexture();

	program->use();
	setProperty(shadowMapLocation, 0);
	setProperty(ambientColorLocation, ambientColor);
	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(lightColorLocation, lightColor);
	setProperty(lightPositionLocation, viewMatrix * lightPosition);
	setProperty(shininessLocation, shininess);

	mat4 shadowMatrix = ShadowManager::ref().getBiasedViewProjMatrix() * modelMatrix;
	setProperty(shadowMatrixLocation, shadowMatrix);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(modelViewMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void ShadowMapEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("Light.Position");
	lightColorLocation = program->getUniformLocation("Light.Intensity");
	diffuseColorLocation = program->getUniformLocation("Material.Kd");
	ambientColorLocation = program->getUniformLocation("Material.Ka");
	specularColorLocation = program->getUniformLocation("Material.Ks");
	shininessLocation = program->getUniformLocation("Material.Shininess");

	modelViewMatrixLocation = program->getUniformLocation("ModelViewMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");

	shadowMatrixLocation = program->getUniformLocation("ShadowMatrix");
	shadowMapLocation = program->getUniformLocation("ShadowMap");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& diffuseColorLocation >= 0
			&& ambientColorLocation >= 0
			&& specularColorLocation >= 0
			&& shininessLocation >= 0
			&& modelViewMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpLocation >= 0
			&& shadowMatrixLocation >= 0
			&& shadowMapLocation >= 0);
}

void ShadowMapEffect::setDefaultValues() {
	lightPosition = glm::vec4(100.0f, 100.0f, 100.0f, 1.0f);
	lightColor = rev::color3(1.0f, 1.0f, 1.0f);
	diffuseColor = rev::color3(0.9f, 0.9f, 0.9f);
	specularColor = rev::color3(0.95f, 0.95f, 0.95f);
	ambientColor = rev::color3(0.1f, 0.1f, 0.1f);
	shininess = 100.0f;
}

GLSLProgram* ShadowMapEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(SHADOW_MAP_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ShadowMapEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(lightColor);
	binder.bindSimple(diffuseColor);
	binder.bindSimple(ambientColor);
	binder.bindSimple(specularColor);
	binder.bindSimple(shininess);
}
