/*
 * RevCheckboardTextureEffect.h
 *
 *  Created on: 06-12-2012
 *      Author: Revers
 */

#ifndef REVCHECKBOARDTEXTUREEFFECT_H_
#define REVCHECKBOARDTEXTUREEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>

#include "RevIEffect.h"

#define CHECKBOARD_TEXTURE_EFFECT "Checkboard Texture"

namespace rev {

	class CheckboardTextureEffect: public IEffect {
		glm::vec3 lightPosition;
		rev::color3 lightColor;
		rev::color3 specularColor;
		rev::color3 ambientColor;
		rev::color3 colorA;
		rev::color3 colorB;
		rev::color3 avgColor;
		float frequency;

		int lightPositionLocation = -1;
		int lightColorLocation = -1;
		int specularColorLocation = -1;
		int ambientColorLocation = -1;
		int colorALocation = -1;
		int colorBLocation = -1;
		int avgColorLocation = -1;
		int frequencyLocation = -1;

		int mvMatrixLocation = -1;
		int normalMatrixLocation = -1;
		int mvpMatrixLocation = -1;

	public:
		DECLARE_BINDABLE(CheckboardTextureEffect)

		CheckboardTextureEffect(rev::GLSLProgram* program);
		CheckboardTextureEffect();

		const char* getName() override {
			return CHECKBOARD_TEXTURE_EFFECT;
		}

		virtual void apply(
				const glm::mat4& modelMatrix,
				const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix,
				const ICamera& camera) override;

		const rev::color3& getAmbientColor() const {
			return ambientColor;
		}

		void setAmbientColor(const rev::color3& ambientColor) {
			this->ambientColor = ambientColor;
		}

		const rev::color3& getLightColor() const {
			return lightColor;
		}

		void setLightColor(const rev::color3& lightColor) {
			this->lightColor = lightColor;
		}

		const glm::vec3& getLightPosition() const {
			return lightPosition;
		}

		void setLightPosition(const glm::vec3& lightPosition) {
			this->lightPosition = lightPosition;
		}

		const rev::color3& getSpecularColor() const {
			return specularColor;
		}

		void setSpecularColor(const rev::color3& specularColor) {
			this->specularColor = specularColor;
		}

		const rev::color3& getColorA() const {
			return colorA;
		}

		void setColorA(const rev::color3& colorA) {
			this->colorA = colorA;
		}

		const rev::color3& getColorB() const {
			return colorB;
		}

		void setColorB(const rev::color3& colorB) {
			this->colorB = colorB;
		}

		const rev::color3& getAvgColor() const {
			return avgColor;
		}

		void setAvgColor(const rev::color3& avgColor) {
			this->avgColor = avgColor;
		}

		float getFrequency() const {
			return frequency;
		}

		void setFrequency(float frequency) {
			this->frequency = frequency;
		}

		static rev::GLSLProgram* createGLSLProgram();

	private:
		void setDefaultValues();
		void findAttributeLocations();
		bool initAfterDeserialization() override;

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCHECKBOARDTEXTUREEFFECT_H_ */
