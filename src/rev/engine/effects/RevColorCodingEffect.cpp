/*
 * RevColorCodingEffect.cpp
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevColorCodingEffect.h"

using namespace rev;
using namespace glm;

#define COLOR_CODING_SHADER_FILE "shaders/ColorCoding.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(ColorCodingEffect, nullptr)

rev::IBindable* ColorCodingEffect::staticCreateBindable() {
	return new ColorCodingEffect(nullptr);
}

bool ColorCodingEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

ColorCodingEffect::ColorCodingEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	if (program) {
		findAttributeLocations();
	}
}

ColorCodingEffect::ColorCodingEffect() :
		IEffect(createGLSLProgram(), true) {
	findAttributeLocations();
}

void ColorCodingEffect::findAttributeLocations() {
	objectIndexLocation = program->getUniformLocation("ObjectIndex");
	drawIndexLocation = program->getUniformLocation("DrawIndex");
	mvpMatrixLocation = program->getUniformLocation("MVPMatrix");

	revAssert(objectIndexLocation >= 0
			&& drawIndexLocation >= 0
			&& mvpMatrixLocation >= 0);
}

void ColorCodingEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvpMatrixLocation, projectionMatrix * mv);
	setProperty(objectIndexLocation, objectIndex);
	setProperty(drawIndexLocation, drawIndex);
}

rev::GLSLProgram* ColorCodingEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(COLOR_CODING_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void ColorCodingEffect::bind(IBinder& binder) {
}
