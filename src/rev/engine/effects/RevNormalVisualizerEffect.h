/*
 * RevNormalVisualizer.h
 *
 *  Created on: 11-12-2012
 *      Author: Revers
 */

#ifndef REVNORMALVISUALIZEREFFECT_H_
#define REVNORMALVISUALIZEREFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define NORMAL_VISUALIZER_EFFECT "Normal Visualizer"

namespace rev {

    /**
     * This shader require object with following vertex attributes:
     * 1. position
     * 2. normal
     */
    class NormalVisualizerEffect: public IEffect {
        rev::color3 color;
        float length;

        int colorLocation = -1;
        int lengthLocation = -1;
        int mvpLocation = -1;

    public:
        DECLARE_BINDABLE(NormalVisualizerEffect)

        NormalVisualizerEffect(rev::GLSLProgram* program);
        NormalVisualizerEffect();

        const char* getName() override {
            return NORMAL_VISUALIZER_EFFECT;
        }

        const rev::color3& getColor() const {
            return color;
        }

        void setColor(const rev::color3& color) {
            this->color = color;
        }

        float getLength() const {
            return length;
        }

        void setLength(float length) {
            this->length = length;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVNORMALVISUALIZEREFFECT_H_ */
