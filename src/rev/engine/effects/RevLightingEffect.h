/*
 * RevLightingEffect.h
 *
 *  Created on: 22 lip 2013
 *      Author: Revers
 *
 * Based on http://ogldev.atspace.co.uk/www/tutorial21/tutorial21.html
 */

#ifndef REVLIGHTINGEFFECT_H_
#define REVLIGHTINGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define LIGHTING_EFFECT "Lighting"

#define MAX_POINT_LIGHTS 8
#define MAX_SPOT_LIGHTS 8

namespace rev {

class LightingEffect: public IEffect {

	float specularPower = 200.0f;
	float specularIntensity = 0.5f;

	GLuint mvpLocation;
	GLuint modelMatrixLocation;
	//GLuint samplerLocation;
	GLuint eyeWorldPosLocation;
	GLuint specularIntensityLocation;
	GLuint specularPowerLocation;
	GLuint numPointLightsLocation;
	GLuint numSpotLightsLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Direction;
	} dirLightLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		struct {
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} pointLightsLocation[MAX_POINT_LIGHTS];

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		GLuint Direction;
		GLuint Cutoff;
		struct {
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} spotLightsLocation[MAX_SPOT_LIGHTS];

public:
	DECLARE_BINDABLE(LightingEffect)

	LightingEffect(rev::GLSLProgram* program);
	LightingEffect();

	const char* getName() override {
		return LIGHTING_EFFECT;
	}

	virtual void apply(
			const glm::mat4& modelMatrix,
			const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix,
			const ICamera& camera) override;

	static rev::GLSLProgram* createGLSLProgram();

private:
	void findAttributeLocations();
	bool initAfterDeserialization() override;

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVLIGHTINGEFFECT_H_ */
