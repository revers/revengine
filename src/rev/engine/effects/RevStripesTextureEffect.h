/*
 * RevStripesTextureEffect.h
 *
 *  Created on: 05-12-2012
 *      Author: Revers
 */

#ifndef REVSTRIPESTEXTUREEFFECT_H_
#define REVSTRIPESTEXTUREEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>

#include "RevIEffect.h"

#define STRIPES_TEXTURE_EFFECT "Stripes Texture"

namespace rev {

    class StripesTextureEffect: public IEffect {
        glm::vec3 lightPosition;
        rev::color3 lightColor;
        rev::color3 specular;
        rev::color3 ambient;
        rev::color3 stripeColor;
        rev::color3 backColor;
        float width;
        float fuzz;
        float scale;
        float kd;

        int lightPositionLocation = -1;
        int lightColorLocation = -1;
        int specularLocation = -1;
        int ambientLocation = -1;
        int stripeColorLocation = -1;
        int backColorLocation = -1;
        int widthLocation = -1;
        int fuzzLocation = -1;
        int scaleLocation = -1;
        int kdLocation = -1;
        int mvMatrixLocation = -1;
        int normalMatrixLocation = -1;
        int mvpMatrixLocation = -1;

    public:
        DECLARE_BINDABLE(StripesTextureEffect)

        StripesTextureEffect(rev::GLSLProgram* program);
        StripesTextureEffect();

        const char* getName() override {
            return STRIPES_TEXTURE_EFFECT;
        }

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        const rev::color3& getAmbient() const {
            return ambient;
        }

        void setAmbient(const rev::color3& ambient) {
            this->ambient = ambient;
        }

        const rev::color3& getBackColor() const {
            return backColor;
        }

        void setBackColor(const rev::color3& backColor) {
            this->backColor = backColor;
        }

        float getFuzz() const {
            return fuzz;
        }

        void setFuzz(float fuzz) {
            this->fuzz = fuzz;
        }

        float getKd() const {
            return kd;
        }

        void setKd(float kd) {
            this->kd = kd;
        }

        const rev::color3& getLightColor() const {
            return lightColor;
        }

        void setLightColor(const rev::color3& lightColor) {
            this->lightColor = lightColor;
        }

        const glm::vec3& getLightPosition() const {
            return lightPosition;
        }

        void setLightPosition(const glm::vec3& lightPosition) {
            this->lightPosition = lightPosition;
        }

        float getScale() const {
            return scale;
        }

        void setScale(float scale) {
            this->scale = scale;
        }

        const rev::color3& getSpecular() const {
            return specular;
        }

        void setSpecular(const rev::color3& specular) {
            this->specular = specular;
        }

        const rev::color3& getStripeColor() const {
            return stripeColor;
        }

        void setStripeColor(const rev::color3& stripeColor) {
            this->stripeColor = stripeColor;
        }

        float getWidth() const {
            return width;
        }

        void setWidth(float width) {
            this->width = width;
        }

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVSTRIPESTEXTUREEFFECT_H_ */
