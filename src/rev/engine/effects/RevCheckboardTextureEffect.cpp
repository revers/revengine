/*
 * RevCheckboardTextureEffect.cpp
 *
 *  Created on: 06-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevCheckboardTextureEffect.h"

using namespace rev;
using namespace glm;

#define CHECKBOARD_TEXTURE_SHADER_FILE "shaders/CheckboardTexture.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(CheckboardTextureEffect, nullptr)

rev::IBindable* CheckboardTextureEffect::staticCreateBindable() {
	return new CheckboardTextureEffect(nullptr);
}

bool CheckboardTextureEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

CheckboardTextureEffect::CheckboardTextureEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

CheckboardTextureEffect::CheckboardTextureEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void CheckboardTextureEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("LightPosition");
	lightColorLocation = program->getUniformLocation("LightColor");
	specularColorLocation = program->getUniformLocation("SpecularColor");
	ambientColorLocation = program->getUniformLocation("AmbientColor");
	colorALocation = program->getUniformLocation("ColorA");
	colorBLocation = program->getUniformLocation("ColorB");
	avgColorLocation = program->getUniformLocation("AvgColor");
	frequencyLocation = program->getUniformLocation("Frequency");

	mvMatrixLocation = program->getUniformLocation("MVMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpMatrixLocation = program->getUniformLocation("MVPMatrix");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& specularColorLocation >= 0
			&& ambientColorLocation >= 0
			&& colorALocation >= 0
			&& colorBLocation >= 0
			&& avgColorLocation >= 0
			&& frequencyLocation >= 0
			&& mvMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpMatrixLocation >= 0);
}

void CheckboardTextureEffect::setDefaultValues() {
	lightPosition = glm::vec3(100.0f, 100.0f, 100.0f);
	lightColor = rev::color3(1.0f, 1.0f, 1.0f);
	specularColor = rev::color3(0.95f, 0.95f, 0.95f);
	ambientColor = rev::color3(0.3f, 0.3f, 0.3f);
	colorA = rev::color3(0.9f, 0.9f, 0.9f);
	colorB = rev::color3(0.0f, 0.0f, 0.0f);
	frequency = 15.0f;
	avgColor = (colorA + colorB) * 0.5f;
}

void CheckboardTextureEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();
	setProperty(lightPositionLocation, lightPosition);
	setProperty(lightColorLocation, lightColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(ambientColorLocation, ambientColor);
	setProperty(colorALocation, colorA);
	setProperty(colorBLocation, colorB);
	setProperty(avgColorLocation, avgColor);
	setProperty(frequencyLocation, frequency);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpMatrixLocation, projectionMatrix * mv);
}

rev::GLSLProgram* CheckboardTextureEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(CHECKBOARD_TEXTURE_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 2, "VertexTexCoord");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void CheckboardTextureEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(lightColor);
	binder.bindSimple(specularColor);
	binder.bindSimple(ambientColor);
	binder.bindSimple(colorA);
	binder.bindSimple(colorB);
	binder.bindSimple(avgColor);
	binder.bindSimple(frequency);
}
