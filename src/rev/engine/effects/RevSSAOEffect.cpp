/*
 * RevSSAOEffect.cpp
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include "RevSSAOEffect.h"

using namespace rev;
using namespace glm;

#define SSAO_SHADER_FILE "shaders/SSAO.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(SSAOEffect, nullptr)

rev::IBindable* SSAOEffect::staticCreateBindable() {
	return new SSAOEffect(nullptr);
}

bool SSAOEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

SSAOEffect::SSAOEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

SSAOEffect::SSAOEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void SSAOEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();
	setProperty(ambientColorLocation, ambientColor);
	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(lightColorLocation, lightColor);
	setProperty(lightPositionLocation, viewMatrix * lightPosition);
	setProperty(shininessLocation, shininess);
	setProperty(ssaoTexLocation, 0);

	SSAORenderer& ssaoRenderer = LightingManager::ref().getSSAORenderer();
	GLuint ssaoTex = ssaoRenderer.getSSAOTexture();

	glActiveTexture(GL_TEXTURE0);
	int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	if (contextIndex == GLContextManager::MAIN_CONTEXT) {
		glBindTexture(GL_TEXTURE_2D, ssaoTex);
	} else {
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(modelViewMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void SSAOEffect::findAttributeLocations() {
	lightPositionLocation = program->getUniformLocation("LightPosition");
	lightColorLocation = program->getUniformLocation("LightColor");
	diffuseColorLocation = program->getUniformLocation("Kd");
	ambientColorLocation = program->getUniformLocation("Ka");
	specularColorLocation = program->getUniformLocation("Ks");
	shininessLocation = program->getUniformLocation("Shininess");

	modelViewMatrixLocation = program->getUniformLocation("ModelViewMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");
	ssaoTexLocation = program->getUniformLocation("SSAOTexture");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& diffuseColorLocation >= 0
			&& ambientColorLocation >= 0
			&& specularColorLocation >= 0
			&& shininessLocation >= 0
			&& modelViewMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpLocation >= 0
			&& ssaoTexLocation >= 0);
}

void SSAOEffect::setDefaultValues() {
	lightPosition = glm::vec4(100.0f, 100.0f, 100.0f, 1.0f);
	lightColor = rev::color3(1.0f, 1.0f, 1.0f);
	diffuseColor = rev::color3(0.9f, 0.9f, 0.9f);
	specularColor = rev::color3(0.95f, 0.95f, 0.95f);
	ambientColor = rev::color3(0.1f, 0.1f, 0.1f);
	shininess = 100.0f;
}

GLSLProgram* SSAOEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(SSAO_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void SSAOEffect::bind(IBinder& binder) {
	binder.bindSimple(lightPosition);
	binder.bindSimple(lightColor);
	binder.bindSimple(diffuseColor);
	binder.bindSimple(ambientColor);
	binder.bindSimple(specularColor);
	binder.bindSimple(shininess);
}
