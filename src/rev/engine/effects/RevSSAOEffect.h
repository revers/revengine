/*
 * RevSSAOEffect.h
 *
 *  Created on: 4 sie 2013
 *      Author: Revers
 */

#ifndef REVSSAOEFFECT_H_
#define REVSSAOEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define SSAO_EFFECT "Phong Shading"

namespace rev {

class SSAOEffect: public IEffect {
	glm::vec4 lightPosition;
	rev::color3 lightColor;
	rev::color3 diffuseColor; // Diffuse reflectivity
	rev::color3 ambientColor; // Ambient reflectivity
	rev::color3 specularColor; // Specular reflectivity
	float shininess; // Specular shininess factor

	int lightPositionLocation = -1;
	int lightColorLocation = -1;
	int diffuseColorLocation = -1;
	int ambientColorLocation = -1;
	int specularColorLocation = -1;
	int shininessLocation = -1;
	int modelViewMatrixLocation = -1;
	int normalMatrixLocation = -1;
	int mvpLocation = -1;
	int ssaoTexLocation = -1;

public:
	DECLARE_BINDABLE(SSAOEffect)

	SSAOEffect(rev::GLSLProgram* program);
	SSAOEffect();

	const char* getName() override {
		return SSAO_EFFECT;
	}

	const rev::color3& getAmbientColor() const {
		return ambientColor;
	}

	void setAmbientColor(const rev::color3& ambientColor) {
		this->ambientColor = ambientColor;
	}

	const rev::color3& getDiffuseColor() const {
		return diffuseColor;
	}

	void setDiffuseColor(const rev::color3& diffuseColor) {
		this->diffuseColor = diffuseColor;
	}

	const rev::color3& getLightColor() const {
		return lightColor;
	}

	void setLightColor(const rev::color3& lightColor) {
		this->lightColor = lightColor;
	}

	const glm::vec4& getLightPosition() const {
		return lightPosition;
	}

	void setLightPosition(const glm::vec4& lightPosition) {
		this->lightPosition = lightPosition;
	}

	float getShininess() const {
		return shininess;
	}

	void setShininess(float shininess) {
		this->shininess = shininess;
	}

	const rev::color3& getSpecularColor() const {
		return specularColor;
	}

	void setSpecularColor(const rev::color3& specularColor) {
		this->specularColor = specularColor;
	}

	virtual void apply(
			const glm::mat4& modelMatrix,
			const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix,
			const ICamera& camera) override;

	static rev::GLSLProgram* createGLSLProgram();

private:
	void setDefaultValues();
	void findAttributeLocations();
	bool initAfterDeserialization() override;

protected:
	void bind(IBinder& binder) override;
};

} /* namespace rev */
#endif /* REVSSAOEFFECT_H_ */
