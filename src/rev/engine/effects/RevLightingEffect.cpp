/*
 * RevLightingEffect.cpp
 *
 *  Created on: 22 lip 2013
 *      Author: Revers
 *
 * Based on http://ogldev.atspace.co.uk/www/tutorial21/tutorial21.html
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include "RevLightingEffect.h"

using namespace rev;
using namespace glm;

#define LIGHTING_SHADER_FILE "shaders/Lighting.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(LightingEffect, nullptr)

rev::IBindable* LightingEffect::staticCreateBindable() {
	return new LightingEffect(nullptr);
}

bool LightingEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

LightingEffect::LightingEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	if (program) {
		findAttributeLocations();
	}
}

LightingEffect::LightingEffect() :
		IEffect(createGLSLProgram(), true) {
	findAttributeLocations();
}

void LightingEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;
	program->setUniform(mvpLocation, mvp);
	program->setUniform(modelMatrixLocation, modelMatrix);
	//program->setUniform(samplerLocation, 0);
	program->setUniform(eyeWorldPosLocation, camera.getPosition());
	program->setUniform(specularPowerLocation, specularPower);
	program->setUniform(specularIntensityLocation, specularIntensity);

	DirectionalLightVect& dirVect = LightingManager::ref().getDirectionalLightVect();
	if (dirVect.size() > 0) {
		DirectionalLightComponent* dirLight = dirVect[0];
		if (!dirLight->isEnabled()) {
			program->setUniform(dirLightLocation.AmbientIntensity, 0.0f);
			program->setUniform(dirLightLocation.DiffuseIntensity, 0.0f);
			program->setUniform(dirLightLocation.Direction, vec3(0.0f));
		} else {
			mat4 m = dirLight->getParent()->getModelMatrix();
			mat3 rot { vec3(m[0]), vec3(m[1]), vec3(m[2]) };

			program->setUniform(dirLightLocation.Color, dirLight->getColor());
			program->setUniform(dirLightLocation.AmbientIntensity, dirLight->getAmbientIntensity());
			program->setUniform(dirLightLocation.DiffuseIntensity, dirLight->getDiffuseIntensity());
			vec3 dir = glm::normalize(rot * dirLight->getDirection());
			program->setUniform(dirLightLocation.Direction, dir);
		}
	} else {
		program->setUniform(dirLightLocation.AmbientIntensity, 0.0f);
		program->setUniform(dirLightLocation.DiffuseIntensity, 0.0f);
		program->setUniform(dirLightLocation.Direction, vec3(0.0f));
	}

	PointLightVect& pointVect = LightingManager::ref().getPointLightVect();
	int i;
	for (i = 0; i < pointVect.size() && i < MAX_POINT_LIGHTS; i++) {
		PointLightComponent* pointLight = pointVect[i];
		auto& pointLightLoc = pointLightsLocation[i];
		if (!pointLight->isEnabled()) {
			program->setUniform(pointLightLoc.Atten.Exp, 999999.0f);
			continue;
		}
		program->setUniform(pointLightLoc.Color, pointLight->getColor());
		program->setUniform(pointLightLoc.AmbientIntensity, pointLight->getAmbientIntensity());
		program->setUniform(pointLightLoc.DiffuseIntensity, pointLight->getDiffuseIntensity());
		program->setUniform(pointLightLoc.Position, pointLight->getParent()->getPosition());
		program->setUniform(pointLightLoc.Atten.Constant, pointLight->getConstantAttenuation());
		program->setUniform(pointLightLoc.Atten.Linear, pointLight->getLinearAttenuation());
		program->setUniform(pointLightLoc.Atten.Exp, pointLight->getExpAttenuation());
	}
	program->setUniform(numPointLightsLocation, i);

	SpotLightVect& spotVect = LightingManager::ref().getSpotLightVect();
	for (i = 0; i < spotVect.size() && i < MAX_SPOT_LIGHTS; i++) {
		SpotLightComponent* spotLight = spotVect[i];
		auto& spotLightLoc = spotLightsLocation[i];

		if (!spotLight->isEnabled()) {
			program->setUniform(spotLightLoc.Cutoff, 1.0f);
			continue;
		}
		mat4 m = spotLight->getParent()->getModelMatrix();
		mat3 rot { vec3(m[0]), vec3(m[1]), vec3(m[2]) };

		program->setUniform(spotLightLoc.Color, spotLight->getColor());
		program->setUniform(spotLightLoc.AmbientIntensity, spotLight->getAmbientIntensity());
		program->setUniform(spotLightLoc.DiffuseIntensity, spotLight->getDiffuseIntensity());
		program->setUniform(spotLightLoc.Position, spotLight->getParent()->getPosition());
		vec3 dir = glm::normalize(rot * spotLight->getDirection());
		program->setUniform(spotLightLoc.Direction, dir);
		program->setUniform(spotLightLoc.Cutoff, spotLight->getCutoff());
		program->setUniform(spotLightLoc.Atten.Constant, spotLight->getConstantAttenuation());
		program->setUniform(spotLightLoc.Atten.Linear, spotLight->getLinearAttenuation());
		program->setUniform(spotLightLoc.Atten.Exp, spotLight->getExpAttenuation());
	}
	program->setUniform(numSpotLightsLocation, i);

}

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define INVALID_UNIFORM_LOCATION 0xFFFFFFFF
#if defined(WIN32) && !defined(__MINGW32__)
#define SNPRINTF _snprintf_s
#else
#define SNPRINTF snprintf
#endif

void LightingEffect::findAttributeLocations() {
	mvpLocation = program->getUniformLocation("MVP");
	modelMatrixLocation = program->getUniformLocation("ModelMatrix");
	//samplerLocation = program->getUniformLocation("Sampler");
	eyeWorldPosLocation = program->getUniformLocation("EyeWorldPos");
	dirLightLocation.Color = program->getUniformLocation("DirectionalLight.Base.Color");
	dirLightLocation.AmbientIntensity = program->getUniformLocation(
			"DirectionalLight.Base.AmbientIntensity");
	dirLightLocation.Direction = program->getUniformLocation("DirectionalLight.Direction");
	dirLightLocation.DiffuseIntensity = program->getUniformLocation(
			"DirectionalLight.Base.DiffuseIntensity");
	specularIntensityLocation = program->getUniformLocation("SpecularIntensity");
	specularPowerLocation = program->getUniformLocation("SpecularPower");
	numPointLightsLocation = program->getUniformLocation("NumPointLights");
	numSpotLightsLocation = program->getUniformLocation("NumSpotLights");

	if (dirLightLocation.AmbientIntensity == INVALID_UNIFORM_LOCATION ||
			mvpLocation == INVALID_UNIFORM_LOCATION ||
			modelMatrixLocation == INVALID_UNIFORM_LOCATION ||
			//samplerLocation == INVALID_UNIFORM_LOCATION ||
			eyeWorldPosLocation == INVALID_UNIFORM_LOCATION ||
			dirLightLocation.Color == INVALID_UNIFORM_LOCATION ||
			dirLightLocation.DiffuseIntensity == INVALID_UNIFORM_LOCATION ||
			dirLightLocation.Direction == INVALID_UNIFORM_LOCATION ||
			specularIntensityLocation == INVALID_UNIFORM_LOCATION ||
			specularPowerLocation == INVALID_UNIFORM_LOCATION ||
			numPointLightsLocation == INVALID_UNIFORM_LOCATION ||
			numSpotLightsLocation == INVALID_UNIFORM_LOCATION) {
		revAssert(false);
		return;
	}

	for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(pointLightsLocation); i++) {
		char name[128];
		memset(name, 0, sizeof(name));
		SNPRINTF(name, sizeof(name), "PointLights[%d].Base.Color", i);
		pointLightsLocation[i].Color = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Base.AmbientIntensity", i);
		pointLightsLocation[i].AmbientIntensity = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Position", i);
		pointLightsLocation[i].Position = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Base.DiffuseIntensity", i);
		pointLightsLocation[i].DiffuseIntensity = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Atten.Constant", i);
		pointLightsLocation[i].Atten.Constant = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Atten.Linear", i);
		pointLightsLocation[i].Atten.Linear = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "PointLights[%d].Atten.Exp", i);
		pointLightsLocation[i].Atten.Exp = program->getUniformLocation(name);

		if (pointLightsLocation[i].Color == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].AmbientIntensity == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].Position == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].DiffuseIntensity == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].Atten.Constant == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].Atten.Linear == INVALID_UNIFORM_LOCATION
				|| pointLightsLocation[i].Atten.Exp == INVALID_UNIFORM_LOCATION) {
			revAssert(false);
			return;
		}
	}

	for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(spotLightsLocation); i++) {
		char name[128];
		memset(name, 0, sizeof(name));
		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Base.Color", i);
		spotLightsLocation[i].Color = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Base.AmbientIntensity", i);
		spotLightsLocation[i].AmbientIntensity = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Position", i);
		spotLightsLocation[i].Position = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Direction", i);
		spotLightsLocation[i].Direction = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Cutoff", i);
		spotLightsLocation[i].Cutoff = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Base.DiffuseIntensity", i);
		spotLightsLocation[i].DiffuseIntensity = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Atten.Constant", i);
		spotLightsLocation[i].Atten.Constant = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Atten.Linear", i);
		spotLightsLocation[i].Atten.Linear = program->getUniformLocation(name);

		SNPRINTF(name, sizeof(name), "SpotLights[%d].Base.Atten.Exp", i);
		spotLightsLocation[i].Atten.Exp = program->getUniformLocation(name);

		if (spotLightsLocation[i].Color == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].AmbientIntensity == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Position == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Direction == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Cutoff == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].DiffuseIntensity == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Atten.Constant == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Atten.Linear == INVALID_UNIFORM_LOCATION
				|| spotLightsLocation[i].Atten.Exp == INVALID_UNIFORM_LOCATION) {
			revAssert(false);
			return;
		}
	}
}

GLSLProgram* LightingEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(LIGHTING_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void LightingEffect::bind(IBinder& binder) {
	binder.bindSimple(specularPower);
	binder.bindSimple(specularIntensity);
}
