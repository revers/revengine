/*
 * RevStencilOutlineEffect.cpp
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include "RevStencilOutlineEffect.h"

using namespace rev;
using namespace glm;

#define FLAT_COLOR_SHADER_FILE "shaders/StencilOutline.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(StencilOutlineEffect, nullptr)

rev::IBindable* StencilOutlineEffect::staticCreateBindable() {
	return new StencilOutlineEffect(nullptr);
}

bool StencilOutlineEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

StencilOutlineEffect::StencilOutlineEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

StencilOutlineEffect::StencilOutlineEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void StencilOutlineEffect::findAttributeLocations() {
	mvpMatrixLocation = program->getUniformLocation("MVPMatrix");
	colorLocation = program->getUniformLocation("Color");
	scaleMatrixLocation = program->getUniformLocation("ScaleMatrix");

	revAssert(mvpMatrixLocation >= 0
			&& colorLocation >= 0
			&& scaleMatrixLocation >= 0);
}

void StencilOutlineEffect::setDefaultValues() {
	color = color3(0.0, 1.0, 0.0);
	scaleMatrix = mat4(1.0);
}

void StencilOutlineEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvpMatrixLocation, projectionMatrix * mv);
	setProperty(colorLocation, color);
	setProperty(scaleMatrixLocation, scaleMatrix);
}

rev::GLSLProgram* StencilOutlineEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(FLAT_COLOR_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void StencilOutlineEffect::bind(IBinder& binder) {
	binder.bindSimple(color);
	binder.bindSimple(scaleMatrix);
}
