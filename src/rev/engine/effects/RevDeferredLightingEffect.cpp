/*
 * RevDeferredLightingEffect.cpp
 *
 *  Created on: 2 sie 2013
 *      Author: Revers
 */

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/engine/glcontext/RevGLContextManager.h>
#include <rev/engine/lighting/RevLightingManager.h>
#include <rev/engine/lighting/RevDeferredRenderer.h>
#include <rev/engine/RevRenderer3D.h>
#include "RevDeferredLightingEffect.h"

using namespace rev;
using namespace glm;

#define DEFERRED_LIGHTING_SHADER_FILE "shaders/DeferredLighting.glsl"

IMPLEMENT_BINDABLE_WITHOUT_CREATE(DeferredLightingEffect, nullptr)

rev::IBindable* DeferredLightingEffect::staticCreateBindable() {
	return new DeferredLightingEffect(nullptr);
}

bool DeferredLightingEffect::initAfterDeserialization() {
	IEffect::program = createGLSLProgram();
	if (!IEffect::program) {
		return false;
	}
	IEffect::owner = true;
	findAttributeLocations();
	return true;
}

DeferredLightingEffect::DeferredLightingEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
	if (program) {
		findAttributeLocations();
	}
}

DeferredLightingEffect::DeferredLightingEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
	findAttributeLocations();
}

void DeferredLightingEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(ambientColorLocation, ambientColor);

	int contextIndex = GLContextManager::ref().getCurrentContextIndex();
	Renderer3D& renderer = Renderer3D::ref();
	vec2 size(renderer.getWidth(contextIndex), renderer.getHeight(contextIndex));
	setProperty(lightViewportLocation, size);

	glActiveTexture(GL_TEXTURE0);
	if (contextIndex == GLContextManager::MAIN_CONTEXT) {
		GLuint lightTex = LightingManager::ref().getDeferredRenderer().getLightTexture();
		glBindTexture(GL_TEXTURE_2D, lightTex);
	} else {
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	setProperty(lightTexLocation, 0);

	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(mvpLocation, projectionMatrix * mv);
}

void DeferredLightingEffect::findAttributeLocations() {
	diffuseColorLocation = program->getUniformLocation("DiffuseColor");
	ambientColorLocation = program->getUniformLocation("AmbientColor");
	mvpLocation = program->getUniformLocation("MVP");
	lightTexLocation = program->getUniformLocation("LightTexture");
	lightViewportLocation = program->getUniformLocation("ViewportSize");

	revAssert(diffuseColorLocation >= 0
			&& ambientColorLocation >= 0
			&& mvpLocation >= 0
			&& lightTexLocation >= 0
			&& lightViewportLocation >= 0);
}

void DeferredLightingEffect::setDefaultValues() {
	diffuseColor = rev::color3(0.6, 0.6, 0.6);
	ambientColor = rev::color3(0.2, 0.2, 0.2);
}

GLSLProgram* DeferredLightingEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(DEFERRED_LIGHTING_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}

void DeferredLightingEffect::bind(IBinder& binder) {
	binder.bindSimple(ambientColor);
	binder.bindSimple(diffuseColor);
}
