/*
 * RevCellShadingEffect.h
 *
 *  Created on: 28 cze 2013
 *      Author: Revers
 */

#ifndef REVCELLSHADINGEFFECT_H_
#define REVCELLSHADINGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevColor.h>
#include <rev/gl/RevGLSLProgram.h>
#include "RevIEffect.h"

#define CELL_SHADING_EFFECT "Phong Shading"

namespace rev {

    class CellShadingEffect: public IEffect {
        glm::vec4 lightPosition;
        rev::color3 diffuseColor; // Diffuse reflectivity
        rev::color3 ambientColor; // Ambient reflectivity
        rev::color3 specularColor; // Specular reflectivity
        float shininess; // Specular shininess factor
        bool antiAlias;

        int lightPositionLocation = -1;
        int diffuseColorLocation = -1;
        int ambientColorLocation = -1;
        int specularColorLocation = -1;
        int shininessLocation = -1;
        int normalMatrixLocation = -1;
        int antiAliasLocation = -1;
        int mvpLocation = -1;

    public:
        DECLARE_BINDABLE(CellShadingEffect)

        CellShadingEffect(rev::GLSLProgram* program);
        CellShadingEffect();

        const char* getName() override {
            return CELL_SHADING_EFFECT;
        }

        const rev::color3& getAmbientColor() const {
            return ambientColor;
        }

        void setAmbientColor(const rev::color3& ambientColor) {
            this->ambientColor = ambientColor;
        }

        const rev::color3& getDiffuseColor() const {
            return diffuseColor;
        }

        void setDiffuseColor(const rev::color3& diffuseColor) {
            this->diffuseColor = diffuseColor;
        }

        const glm::vec4& getLightPosition() const {
            return lightPosition;
        }

        void setLightPosition(const glm::vec4& lightPosition) {
            this->lightPosition = lightPosition;
        }

        float getShininess() const {
            return shininess;
        }

        void setShininess(float shininess) {
            this->shininess = shininess;
        }

        const rev::color3& getSpecularColor() const {
            return specularColor;
        }

        void setSpecularColor(const rev::color3& specularColor) {
            this->specularColor = specularColor;
        }

		bool isAntiAlias() const {
			return antiAlias;
		}

		void setAntiAlias(bool antiAlias) {
			this->antiAlias = antiAlias;
		}

        virtual void apply(
                const glm::mat4& modelMatrix,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                const ICamera& camera) override;

        static rev::GLSLProgram* createGLSLProgram();

    private:
        void setDefaultValues();
		void findAttributeLocations();
        bool initAfterDeserialization() override;

    protected:
        void bind(IBinder& binder) override;
    };

} /* namespace rev */
#endif /* REVCELLSHADINGEFFECT_H_ */
