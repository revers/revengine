/*
 * RevXZPlaneObstacle.h
 *
 *  Created on: 29-04-2013
 *      Author: Revers
 */

#ifndef REVXZPLANEOBSTACLE_H_
#define REVXZPLANEOBSTACLE_H_

#include "RevObstacle.h"

/**
 * http://gamedev.stackexchange.com/questions/26501/how-does-a-collision-engine-work
 */
namespace rev {

	class XZPlaneObstacle: public Obstacle {
	public:
		real y;

		XZPlaneObstacle(real y, real frictionCoeff = 0.1, real reflectionCoeff = 2.0) :
				Obstacle(frictionCoeff, reflectionCoeff), y(y) {
		}

		bool isPenetrated(const PhysicsState& nextState, const PhysicsState& state,
				real margin, rvec3* normal) override {
			bool result = nextState.x.y < y + margin;

			if (result && normal) {
				*normal = rvec3(0, 1, 0);
			}
			return result;
		}
	};

} /* namespace rev */
#endif /* REVXZPLANEOBSTACLE_H_ */
