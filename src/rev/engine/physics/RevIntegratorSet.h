/*
 * RevIntegratorSet.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVINTEGRATORSET_H_
#define REVINTEGRATORSET_H_

#include "RevIIntegrator.h"

namespace rev {

	class IntegratorSet {
		IntegratorSet() = delete;
		~IntegratorSet() = delete;

	public:
		static IIntegrator* getEuler();
		static IIntegrator* getVerlet();
		static IIntegrator* getRungeKutta4();
	};

} /* namespace rev */
#endif /* REVINTEGRATORSET_H_ */
