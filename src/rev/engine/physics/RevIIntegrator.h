/*
 * RevIIntegrator.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVIINTEGRATOR_H_
#define REVIINTEGRATOR_H_

#include "RevPhysicsState.h"
#include "RevIAccelerator.h"

namespace rev {

	/**
	 * IIntegrator
	 */
	class IIntegrator {
	public:

		virtual ~IIntegrator() {
		}

		virtual PhysicsState integrate(const PhysicsState& state, real dt,
				IAccelerator& accel, void* data) = 0;
	};

} /* namespace rev */

#endif /* REVIINTEGRATOR_H_ */
