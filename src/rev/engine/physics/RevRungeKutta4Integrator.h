/*
 * RevRungeKutta4Integrator.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 *
 *  Based on http://gafferongames.com/game-physics/integration-basics/
 */

#ifndef REVRUNGEKUTTA4INTEGRATOR_H_
#define REVRUNGEKUTTA4INTEGRATOR_H_

#include "RevIIntegrator.h"

namespace rev {

	/**
	 * Derivative
	 */
	struct Derivative {

		/**
		 * Derivative of position: velocity.
		 */
		rvec3 dx;

		/**
		 * Derivative of velocity: acceleration.
		 */
		rvec3 dv;

		Derivative() :
				dx(0), dv(0) {
		}

		Derivative(const rvec3& dx, const rvec3& dv) :
				dx(dx), dv(dv) {
		}
	};

	/**
	 * RungeKutta4Integrator
	 */
	class RungeKutta4Integrator: public IIntegrator {
	public:

		PhysicsState integrate(const PhysicsState& state, real dt,
				IAccelerator& accel, void* data) override {
			PhysicsState nextState;

			Derivative a = evaluate(state, dt, accel, data);
			Derivative b = evaluate(state, dt * real(0.5), a, accel, data);
			Derivative c = evaluate(state, dt * real(0.5), b, accel, data);
			Derivative d = evaluate(state, dt, c, accel, data);

			const rvec3 dxdt = real(1.0 / 6.0)
					* (a.dx + real(2.0) * (b.dx + c.dx) + d.dx);
			const rvec3 dvdt = real(1.0 / 6.0)
					* (a.dv + real(2.0) * (b.dv + c.dv) + d.dv);

			nextState.x = state.x + dxdt * dt;
			nextState.v = state.v + dvdt * dt;
			nextState.prevX = state.x;

			return nextState;
		}

	private:
		inline Derivative evaluate(const PhysicsState& initial, real dt,
				IAccelerator& accel, void* data) {
			Derivative output;
			output.dx = initial.v;
			output.dv = accel.acceleration(initial, dt, data);
			return output;
		}

		inline Derivative evaluate(const PhysicsState& initial, real dt,
				const Derivative& d, IAccelerator& accel, void* data) {
			PhysicsState state;
			state.x = initial.x + d.dx * dt;
			state.v = initial.v + d.dv * dt;
			Derivative output;
			output.dx = state.v;
			output.dv = accel.acceleration(state, dt, data);
			return output;
		}

	};

} /* namespace rev */
#endif /* REVRUNGEKUTTA4INTEGRATOR_H_ */
