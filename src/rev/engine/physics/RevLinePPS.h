/*
 * RevLinePPS.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVLINEPPS_H_
#define REVLINEPPS_H_

#include "RevPointParticleSet.h"

namespace rev {

	class LinePPS: public PointParticleSet {

	private:
		real k = real(20.5);

	public:
		LinePPS() {
			PointParticleSet::addParticle(rvec3(-5, 0, 0), rvec3(5, 0, 0), 1.0, 0.2);
			PointParticleSet::addParticle(rvec3(5, 5, 5), rvec3(5, 5, 5), 1.0, 0.2);
		}

		rvec3 acceleration(const PhysicsState& state, real dt, void* data) override {
			return -k * state.x - real(0.1) * state.v;
		}
	};

} /* namespace rev */

#endif /* REVLINEPPS_H_ */
