/*
 * RevInertiaTensorUtil.h
 *
 *  Created on: 26-05-2013
 *      Author: Revers
 */

#ifndef REVINERTIATENSORUTIL_H_
#define REVINERTIATENSORUTIL_H_

#include <glm/glm.hpp>
#include <rev/engine/math/RevMathHelper.h>

namespace rev {

	class InertiaTensorUtil {
		InertiaTensorUtil() = delete;
		~InertiaTensorUtil() = delete;

	public:
		template<typename T>
		static inline glm::detail::tmat3x3<T> getBoxTensor(
				const glm::detail::tvec3<T>& halfSizes, T mass) {

			glm::detail::tvec3<T> squares = halfSizes * halfSizes;
			glm::detail::tmat3x3<T> result;

			setInertiaTensorCoeffs(result,
					T(1.0 / 3.0) * mass * (squares.y + squares.z),
					T(1.0 / 3.0) * mass * (squares.x + squares.z),
					T(1.0 / 3.0) * mass * (squares.x + squares.y));

			return result;
		}

		template<typename T>
		static inline glm::detail::tmat3x3<T> getSphereTensor(T radius, T mass) {
			return glm::detail::tmat3x3<T>(T(2.0 / 5.0) * mass * radius * radius);
		}

	private:
		/**
		 * Sets the value of the matrix from inertia tensor values.
		 */
		template<typename T>
		static inline void setInertiaTensorCoeffs(
				glm::detail::tmat3x3<T>& m, T ix, T iy, T iz,
				T ixy = T(0), T ixz = T(0), T iyz = T(0)) {
			m[0][0] = ix;
			m[1][0] = m[0][1] = -ixy;
			m[2][0] = m[0][2] = -ixz;
			m[1][1] = iy;
			m[2][1] = m[1][2] = -iyz;
			m[2][2] = iz;
		}

	};

} /* namespace rev */
#endif /* REVINERTIATENSORUTIL_H_ */
