/*
 * RevEulerIntegrator.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVEULERINTEGRATOR_H_
#define REVEULERINTEGRATOR_H_

#include "RevIIntegrator.h"

namespace rev {

	class EulerIntegrator: public IIntegrator {
	public:

		PhysicsState integrate(const PhysicsState& state, real dt,
				IAccelerator& accel, void* data) override {
			PhysicsState nextState;
			rvec3 a = accel.acceleration(state, dt, data);
			nextState.v = state.v + a * dt;
			nextState.x = state.x + nextState.v * dt;
			nextState.prevX = state.x;

			return nextState;
		}

	};

} /* namespace rev */

#endif /* REVEULERINTEGRATOR_H_ */
