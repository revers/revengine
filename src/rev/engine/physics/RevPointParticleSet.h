/*
 * RevPointParticleSet.h
 *
 *  Created on: 27-04-2013
 *      Author: Revers
 */

#ifndef REVPOINTPARTICLESET_H_
#define REVPOINTPARTICLESET_H_

#include <vector>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include "RevPointParticle.h"
#include "RevIAccelerator.h"
#include "RevIIntegrator.h"

namespace rev {

	class PointParticleSet: public IAccelerator {
	public:
		typedef std::vector<PointParticle> PointParticleVector;

	private:
		PointParticleVector particles;
		IIntegrator* integrator = nullptr;

	public:
		PointParticleSet() {
		}
		virtual ~PointParticleSet() {
		}

		PointParticle& addParticle(const rvec3& position, const rvec3& velocity,
				real mass = 1.0, real radius = 1.0) {
			particles.push_back(PointParticle(position, velocity, mass, radius));
			return particles[particles.size() - 1];
		}

		PointParticle& getParticle(int index) {
			revAssert(index >= 0 && index < particles.size());
			return particles[index];
		}

		PointParticleVector& getPointParticleVector() {
			return particles;
		}

		int getPointParticlesCount() {
			return particles.size();
		}

		const PointParticle& getParticle(int index) const {
			revAssert(index >= 0 && index < particles.size());
			return particles[index];
		}

		IIntegrator* getIntegrator() {
			return integrator;
		}

		void setIntegrator(IIntegrator* integrator) {
			this->integrator = integrator;
		}

		virtual void step(real timestep) {
			beforeStep(timestep);
			prepareStep(timestep);
			afterStepPreparation(timestep);
			applyStep();
			afterStep(timestep);
		}

		rvec3 getCenterOfMass() const {
			rvec3 result(0);

			for (int i = 0; i < particles.size(); i++) {
				const PointParticle& p = particles[i];
				result += p.state.x;
			}
			result /= real(particles.size());

			return result;
		}

		void zeroAvgSpeed() {
			int frozenCount = 0;
			rvec3 avgVelocity(0);
			for (int i = 0; i < particles.size(); i++) {
				PointParticle& p = particles[i];
				if (p.frozen) {
					frozenCount++;
				} else {
					avgVelocity += p.state.v;
				}
			}

			avgVelocity /= real(particles.size() - frozenCount);
			for (int i = 0; i < particles.size(); i++) {
				PointParticle& p = particles[i];
				if (!p.frozen) {
					p.state.v -= avgVelocity;
				}
			}
		}

	protected:
		virtual void beforeStep(real timestep) {
		}
		virtual void afterStepPreparation(real timestep) {
		}
		virtual void afterStep(real timestep) {
		}

		void prepareStep(real timestep) {
			for (int i = 0; i < particles.size(); i++) {
				PointParticle& p = particles[i];
				if (p.frozen) {
					continue;
				}
				p.nextState = integrator->integrate(p.state, timestep, *this, &i);
			}
		}

		void applyStep() {
			for (PointParticle& p : particles) {
				if (p.frozen) {
					continue;
				}
				p.state = p.nextState;
			}
		}

		inline real sqr(const real& a) const {
			return a * a;
		}
	};

} /* namespace rev */

#endif /* REVPOINTPARTICLESET_H_ */
