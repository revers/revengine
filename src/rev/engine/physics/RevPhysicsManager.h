/*
 * RevPhysicsManager.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef REVPHYSICSMANAGER_H_
#define REVPHYSICSMANAGER_H_

#include <vector>
#include <rev/engine/components/RevIPhysicsComponent.h>
#include "RevRigidBodyCollisionResolver.h"

// Make sure its the same as RevCollisionManager.h: REV_MAX_POTENTIAL_CONTACTS
#define REV_MAX_RB_POTENTIAL_CONTACTS 2048

namespace rev {

	typedef std::vector<IPhysicsComponent*> PhysicsComponentVect;

	class PhysicsManager: public IBindable {
		static PhysicsManager* physicsManager;

		PhysicsManager();
		PhysicsManager(const PhysicsManager&) = delete;

	private:
		PhysicsComponentVect components;
		RigidBodyCollisionResolver rigidBodyResolver;
		CollisionContact* rigidBodyContacts[REV_MAX_RB_POTENTIAL_CONTACTS];
		int rigidBodyContactsSize = 0;

		real totalTime = real(0);
		real accumulator = real(0);
		const real dt = real(0.1);
		bool paused = false;

		int numComponents = 0;

	public:
		DECLARE_BINDABLE_SINGLETON(PhysicsManager)

		~PhysicsManager() {
		}

		static void createSingleton();
		static void destroySingleton();

		static PhysicsManager& ref() {
			return *physicsManager;
		}

		static PhysicsManager* getInstance() {
			return physicsManager;
		}

		void resolveContacts(CollisionContact* contactArray, int numContacts);

		/**
		 * PhysicsManager is NOT owner of added component.
		 */
		void addComponent(IPhysicsComponent* comp) {
			components.push_back(comp);
			numComponents = components.size();
		}

		void removeComponent(IPhysicsComponent* comp) {
			components.erase(std::remove(components.begin(), components.end(), comp),
					components.end());
			numComponents = components.size();
		}

		void removeAllComponents() {
			components.clear();
			numComponents = components.size();
		}

		PhysicsComponentVect& getComponents() {
			return components;
		}

		void update();

		bool isPaused() const {
			return paused;
		}

		void setPaused(bool paused) {
			this->paused = paused;
		}

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVPHYSICSMANAGER_H_ */
