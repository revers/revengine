/*
 * RevRigidBodyCollisionResolver.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>

#include <rev/engine/math/RevMathHelper.h>
#include "RevRigidBodyCollisionResolver.h"

using namespace rev;
using namespace glm;

void RigidBodyCollisionResolver::resolveContacts(CollisionContact** contactArray,
		int numContacts, real duration) {

	// Make sure we have something to do.
	if (numContacts == 0) {
		return;
	}
//	REV_WARN_MSG(a2s(R(numContacts), R(velocityIterations), R(positionIterations),
//			R(velocityEpsilon), R(positionEpsilon), R(velocityIterationsUsed),
//			R(positionIterationsUsed)) << "\nisValid = " << (isValid() ? "true" : "false"));

	if (!isValid()) {
		return;
	}

	this->numContacts = numContacts;
	// Prepare the contacts for processing
	int contactIndex = 0;
	CollisionContact** lastContact = contactArray + numContacts;
	for (CollisionContact** contact = contactArray; contact < lastContact; contact++) {
		RigidBodyContact& c = contacts[contactIndex++];
		c.set(**contact);

		// Calculate the internal contact data (inertia, basis, etc).
		c.calculateInternals(duration);
	}

	// Resolve the interpenetration problems with the contacts.
	adjustPositions(duration);

	// Resolve the velocity problems with the contacts.
	adjustVelocities(duration);
}

void RigidBodyCollisionResolver::adjustVelocities(real duration) {
	rvec3 velocityChange[2], rotationChange[2];
	rvec3 deltaVel;

	// iteratively handle impacts in order of severity.
	velocityIterationsUsed = 0;
	while (velocityIterationsUsed < velocityIterations) {
		// Find contact with maximum magnitude of probable velocity change.
		real max = velocityEpsilon;
		int index = numContacts;
		for (int i = 0; i < numContacts; i++) {
			if (contacts[i].desiredDeltaVelocity > max) {
				max = contacts[i].desiredDeltaVelocity;
				index = i;
			}
		}
		if (index == numContacts) {
			break;
		}

		// Match the awake state at the contact
		contacts[index].matchAwakeState();

		// Do the resolution on the contact that came out top.
		contacts[index].applyVelocityChange(velocityChange, rotationChange);

		// With the change in velocity of the two bodies, the update of
		// contact velocities means that some of the relative closing
		// velocities need recomputing.
		for (int i = 0; i < numContacts; i++) {
			// Check each body in the contact
			for (int b = 0; b < 2; b++) {
				if (contacts[i].body[b]) {
					// Check for a match with each body in the newly
					// resolved contact
					for (int d = 0; d < 2; d++) {
						if (contacts[i].body[b] == contacts[index].body[d]) {
							deltaVel = velocityChange[d]
									+ cross(rotationChange[d],
											contacts[i].relativeContactPosition[b]);

							// The sign of the change is negative if we're dealing
							// with the second body in a contact.
							contacts[i].contactVelocity +=
									MathHelper::transformTranspose(contacts[i].contactToWorld,
											deltaVel) * real(b ? -1 : 1);
							contacts[i].calculateDesiredDeltaVelocity(duration);
						} // end if
					} // end for
				} // end if
			} // end for
		} // end for
		velocityIterationsUsed++;
	}
}

void RigidBodyCollisionResolver::adjustPositions(real duration) {
	int i, index;
	rvec3 linearChange[2], angularChange[2];
	real max;
	rvec3 deltaPosition;

	// iteratively resolve interpenetrations in order of severity.
	positionIterationsUsed = 0;
	while (positionIterationsUsed < positionIterations) {

		// Find biggest penetration
		max = positionEpsilon;
		index = numContacts;
		for (i = 0; i < numContacts; i++) {
			if (contacts[i].penetration > max) {
				max = contacts[i].penetration;
				index = i;
			}
		}
		if (index == numContacts) {
			break;
		}

		// Match the awake state at the contact
		contacts[index].matchAwakeState();

		// Resolve the penetration.
		contacts[index].applyPositionChange(
				linearChange,
				angularChange,
				max);

		// Again this action may have changed the penetration of other
		// bodies, so we update contacts.
		for (i = 0; i < numContacts; i++) {
			// Check each body in the contact
			for (int b = 0; b < 2; b++) {
				if (contacts[i].body[b]) {
					// Check for a match with each body in the newly
					// resolved contact
					for (int d = 0; d < 2; d++) {
						if (contacts[i].body[b] == contacts[index].body[d]) {
							deltaPosition = linearChange[d]
									+ cross(angularChange[d],
											contacts[i].relativeContactPosition[b]);

							// The sign of the change is positive if we're
							// dealing with the second body in a contact
							// and negative otherwise (because we're
							// subtracting the resolution)..
							contacts[i].penetration +=
									dot(deltaPosition, contacts[i].contactNormal)
											* real(b ? 1 : -1);
						} // end if
					} // end for
				} // end if
			} // end for
		} // end for
		positionIterationsUsed++;
	}
}
