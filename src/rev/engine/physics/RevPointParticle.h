/*
 * RevPointParticle.h
 *
 *  Created on: 27-04-2013
 *      Author: Revers
 */

#ifndef REVPOINTPARTICLE_H_
#define REVPOINTPARTICLE_H_


#include "RevPhysicsState.h"

namespace rev {

	class PointParticle {
		friend class PointParticleSet;

	private:
		PhysicsState state;
		PhysicsState nextState;

		real mass = 1.0;
		real radius = 1.0;
		bool frozen = false;

	public:
		PointParticle() {
		}

		PointParticle(const rvec3& position, const rvec3& velocity,
				real mass = 1.0, real radius = 1.0) :
				state(position, velocity), mass(mass), radius(radius) {
		}

		bool isFrozen() const {
			return frozen;
		}

		void setFrozen(bool frozen) {
			this->frozen = frozen;
		}

		real getMass() const {
			return mass;
		}

		void setMass(real mass) {
			this->mass = mass;
		}

		real getRadius() const {
			return radius;
		}

		void setRadius(real radius) {
			this->radius = radius;
		}

		const PhysicsState& getState() const {
			return state;
		}

		void setState(const PhysicsState& state) {
			this->state = state;
		}

		const PhysicsState& getNextState() const {
			return nextState;
		}

		void setNextState(const PhysicsState& nextState) {
			this->nextState = nextState;
		}
	};

} /* namespace rev */

#endif /* REVPOINTPARTICLE_H_ */
