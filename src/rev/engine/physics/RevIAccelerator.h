/*
 * RevIAccelerator.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVIACCELERATOR_H_
#define REVIACCELERATOR_H_

#include "RevPhysicsState.h"

namespace rev {

	/**
	 * IAccelerator
	 */
	class IAccelerator {
	public:

		virtual ~IAccelerator() {
		}

		virtual rvec3 acceleration(const PhysicsState& state, real dt, void* data) = 0;
	};

} /* namespace rev */

#endif /* REVIACCELERATOR_H_ */
