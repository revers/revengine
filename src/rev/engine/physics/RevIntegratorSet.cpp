/*
 * RevIntegratorSet.cpp
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#include "RevEulerIntegrator.h"
#include "RevVerletIntegrator.h"
#include "RevRungeKutta4Integrator.h"
#include "RevIntegratorSet.h"

using namespace rev;

IIntegrator* IntegratorSet::getEuler() {
	static EulerIntegrator eulerIntegrator;
	return &eulerIntegrator;
}

IIntegrator* IntegratorSet::getVerlet() {
	static VerletIntegrator verletIntegrator;
	return &verletIntegrator;
}

IIntegrator* IntegratorSet::getRungeKutta4() {
	static RungeKutta4Integrator rungeKutta4Integrator;
	return &rungeKutta4Integrator;
}
