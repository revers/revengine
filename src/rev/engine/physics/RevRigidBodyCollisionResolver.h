/*
 * RevRigidBodyCollisionResolver.h
 *
 *  Created on: 25-05-2013
 *      Author: Revers
 */

#ifndef REVRIGIDBODYCOLLISIONRESOLVER_H_
#define REVRIGIDBODYCOLLISIONRESOLVER_H_

#include <rev/engine/math/RevReal.h>
#include "RevRigidBodyContact.h"

namespace rev {

	class RigidBodyCollisionResolver {
	private:
		RigidBodyContact contacts[REV_MAX_COLLISION_CONTACS];
		int numContacts = 0;

		/**
		 * Holds the number of iterations to perform when resolving
		 * velocity.
		 */
		int velocityIterations = 0;

		/**
		 * Holds the number of iterations to perform when resolving
		 * position.
		 */
		int positionIterations = 0;

		/**
		 * To avoid instability velocities smaller
		 * than this value are considered to be zero. Too small and the
		 * simulation may be unstable, too large and the bodies may
		 * interpenetrate visually. A good starting point is the default
		 * of 0.01.
		 */
		real velocityEpsilon = real(0.01);

		/**
		 * To avoid instability penetrations
		 * smaller than this value are considered to be not interpenetrating.
		 * Too small and the simulation may be unstable, too large and the
		 * bodies may interpenetrate visually. A good starting point is
		 * the default of 0.01.
		 */
		real positionEpsilon = real(0.01);
		;

		/**
		 * Stores the number of velocity iterations used in the
		 * last call to resolve contacts.
		 */
		int velocityIterationsUsed = 0;

		/**
		 * Stores the number of position iterations used in the
		 * last call to resolve contacts.
		 */
		int positionIterationsUsed = 0;

	public:
		~RigidBodyCollisionResolver() {
		}

		RigidBodyCollisionResolver() {
		}


		RigidBodyCollisionResolver(int iterations, real velocityEpsilon,
				real positionEpsilon) {
			setIterations(iterations, iterations);
			setEpsilon(velocityEpsilon, positionEpsilon);
		}

		RigidBodyCollisionResolver(int velocityIterations, int positionIterations,
				real velocityEpsilon, real positionEpsilon) {
			setIterations(velocityIterations, positionIterations);
			setEpsilon(velocityEpsilon, positionEpsilon);
		}

		void setIterations(int iterations) {
			setIterations(iterations, iterations);
		}

		void setIterations(int velocityIterations, int positionIterations) {
			this->velocityIterations = velocityIterations;
			this->positionIterations = positionIterations;
		}

		void setEpsilon(real velocityEpsilon, real positionEpsilon) {
			this->velocityEpsilon = velocityEpsilon;
			this->positionEpsilon = positionEpsilon;
		}

		/**
		 * Returns true if the resolver has valid settings and is ready to go.
		 */
		bool isValid() {
			return (velocityIterations > 0)
					&& (positionIterations > 0)
					&& (positionEpsilon >= real(0.0))
					&& (positionEpsilon >= real(0.0));
		}

		void resolveContacts(CollisionContact** contactArray,
				int numContacts, real duration);

	protected:
		/**
		 * Resolves the velocity issues with the given array of constraints,
		 * using the given number of iterations.
		 */
		void adjustVelocities(real duration);

		/**
		 * Resolves the positional issues with the given array of constraints,
		 * using the given number of iterations.
		 */
		void adjustPositions(real duration);
	};

} /* namespace rev */
#endif /* REVRIGIDBODYCOLLISIONRESOLVER_H_ */
