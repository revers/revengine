/*
 * RevVerletIntegrator.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVVERLETINTEGRATOR_H_
#define REVVERLETINTEGRATOR_H_

#include "RevIIntegrator.h"

namespace rev {

	class VerletIntegrator: public IIntegrator {
	public:
		PhysicsState integrate(const PhysicsState& state, real dt,
				IAccelerator& accel, void* data) override {
			PhysicsState nextState;

			rvec3 a = accel.acceleration(state, dt, data);
			nextState.x = real(2.0) * state.x - state.prevX + a * dt * dt;
			nextState.v = (nextState.x - state.prevX) / (real(2.0) * dt);
			nextState.prevX = state.x;

			return nextState;
		}

	};

} /* namespace rev */
#endif /* REVVERLETINTEGRATOR_H_ */
