/*
 * RevPhysicsState.h
 *
 *  Created on: 28-04-2013
 *      Author: Revers
 */

#ifndef REVPHYSICSSTATE_H_
#define REVPHYSICSSTATE_H_

#include <rev/engine/math/RevReal.h>

namespace rev {

	struct PhysicsState {

		/**
		 * Position.
		 */
		rvec3 x;

		/**
		 * Previous position. For Verlet's integrator.
		 */
		rvec3 prevX;

		/**
		 * Velocity.
		 */
		rvec3 v;

		PhysicsState() :
				x(0), prevX(0), v(0) {
		}

		PhysicsState(const rvec3& x, const rvec3& v) :
				x(x), prevX(x), v(v) {
		}

		PhysicsState(const rvec3& x, const rvec3& prevX, const rvec3& v) :
				x(x), prevX(prevX), v(v) {
		}
	};

} /* namespace rev */

#endif /* REVPHYSICSSTATE_H_ */
