/*
 * RevObstacle.h
 *
 *  Created on: 29-04-2013
 *      Author: Revers
 */

#ifndef REVOBSTACLE_H_
#define REVOBSTACLE_H_

#include "RevPhysicsState.h"

namespace rev {

	class Obstacle {
	public:
		real frictionCoeff = 0.1;
		real reflectionCoeff = 1.0;

		Obstacle(real frictionCoeff, real reflectionCoeff) :
				frictionCoeff(frictionCoeff), reflectionCoeff(reflectionCoeff) {
		}
		virtual ~Obstacle() {
		}

		virtual bool isPenetrated(const PhysicsState& nextState, const PhysicsState& state,
				real margin, vec3_t* normal) = 0;
	};

} /* namespace rev */

#endif /* REVOBSTACLE_H_ */
