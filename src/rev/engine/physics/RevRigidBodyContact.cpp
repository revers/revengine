/*
 * RevRigidBodyContact.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/components/RevIPhysicsComponent.h>
#include "RevRigidBodyContact.h"

using namespace rev;
using namespace glm;

RigidBodyContact::RigidBodyContact(const CollisionContact& contact) :
		contactPoint(contact.contactPoint), contactNormal(contact.contactNormal),
				penetration(penetration) {
	set(contact);
}

/*
 * FIXME: contact.first przeciez zawsze != nullptr.
 * Tylko contac.second potencjalnie moze byc nullptr.
 * @see RevCollisionDetector.cpp
 */
void RigidBodyContact::set(const CollisionContact& contact) {
	contactPoint = contact.contactPoint;
	contactNormal = contact.contactNormal;
	penetration = contact.penetration;

	IPhysicsComponent* firstPhysics = nullptr;
	if (contact.first != nullptr) {
		firstPhysics = contact.first->getParent()->getPhysicsComponent();
	}
	IPhysicsComponent* secondPhysics = nullptr;
	if (contact.second != nullptr) {
		secondPhysics = contact.second->getParent()->getPhysicsComponent();
	}

	revAssert(firstPhysics != nullptr || secondPhysics != nullptr);
	revAssert(firstPhysics == nullptr
			|| dynamic_cast<RigidBodyComponent*>(firstPhysics) != nullptr);
	revAssert(secondPhysics == nullptr
			|| dynamic_cast<RigidBodyComponent*>(secondPhysics) != nullptr);

	first = static_cast<RigidBodyComponent*>(firstPhysics);
	second = static_cast<RigidBodyComponent*>(secondPhysics);

	if (!first) {
		swapBodies();
	}
	friction = first->friction;
	restitution = first->restitution;
	if (second) {
		friction = (first->friction + second->friction) * real(0.5);
		restitution = (first->restitution + second->restitution) * real(0.5);
	}
}

void RigidBodyContact::swapBodies() {
	contactNormal *= real(-1);

	RigidBodyComponent* temp = first;
	first = second;
	second = temp;
}

void RigidBodyContact::matchAwakeState() {
	// Collisions with the world never cause a body to wake up.
	if (!body[1]) {
		return;
	}

	bool body0awake = body[0]->getAwake();
	bool body1awake = body[1]->getAwake();

	// Wake up only the sleeping one
	if (body0awake ^ body1awake) {
		if (body0awake) {
			body[1]->setAwake();
		} else {
			body[0]->setAwake();
		}
	}
}

/*
 * Constructs an arbitrary orthonormal basis for the contact.  This is
 * stored as a 3x3 matrix, where each vector is a column (in other
 * words the matrix transforms contact space into world space). The x
 * direction is generated from the contact normal, and the y and z
 * directionss are set so they are at right angles to it.
 */
inline void RigidBodyContact::calculateContactBasis() {
	rvec3 contactTangent[2];

	// Check whether the Z-axis is nearer to the X or Y axis
	if (real_abs(contactNormal.x) > real_abs(contactNormal.y)) {
		// Scaling factor to ensure the results are normalised
		const real s = real(1.0) / real_sqrt(contactNormal.z * contactNormal.z
				+ contactNormal.x * contactNormal.x);

		// The new X-axis is at right angles to the world Y-axis
		contactTangent[0].x = contactNormal.z * s;
		contactTangent[0].y = 0;
		contactTangent[0].z = -contactNormal.x * s;

		// The new Y-axis is at right angles to the new X- and Z- axes
		contactTangent[1].x = contactNormal.y * contactTangent[0].x;
		contactTangent[1].y = contactNormal.z * contactTangent[0].x
				- contactNormal.x * contactTangent[0].z;
		contactTangent[1].z = -contactNormal.y * contactTangent[0].x;
	} else {
		// Scaling factor to ensure the results are normalised
		const real s = real(1.0) / real_sqrt(contactNormal.z * contactNormal.z
				+ contactNormal.y * contactNormal.y);

		// The new X-axis is at right angles to the world X-axis
		contactTangent[0].x = 0;
		contactTangent[0].y = -contactNormal.z * s;
		contactTangent[0].z = contactNormal.y * s;

		// The new Y-axis is at right angles to the new X- and Z- axes
		contactTangent[1].x = contactNormal.y * contactTangent[0].z
				- contactNormal.z * contactTangent[0].y;
		contactTangent[1].y = -contactNormal.x * contactTangent[0].z;
		contactTangent[1].z = contactNormal.x * contactTangent[0].y;
	}

	// Make a matrix from the three vectors.
//	contactToWorld.setComponents(
//			contactNormal,
//			contactTangent[0],
//			contactTangent[1]);
	contactToWorld[0] = contactNormal;
	contactToWorld[1] = contactTangent[0];
	contactToWorld[2] = contactTangent[1];
}

rvec3 RigidBodyContact::calculateLocalVelocity(int bodyIndex, real duration) {
	RigidBodyComponent* thisBody = body[bodyIndex];

	// Work out the velocity of the contact point.
	rvec3 velocity = cross(thisBody->getAngularVelocity(), relativeContactPosition[bodyIndex]);
	velocity += thisBody->getVelocity();

	// Turn the velocity into contact-coordinates.
	rvec3 contactVelocity = MathHelper::transformTranspose(contactToWorld, velocity);
	//contactToWorld.transformTranspose(velocity);

	// Calculate the ammount of velocity that is due to forces without
	// reactions.
	rvec3 accVelocity = thisBody->getLastFrameAcceleration() * duration;

	// Calculate the velocity in contact-coordinates.
	accVelocity = MathHelper::transformTranspose(contactToWorld, accVelocity);
	//contactToWorld.transformTranspose(accVelocity);

	// We ignore any component of acceleration in the contact normal
	// direction, we are only interested in planar acceleration
	accVelocity.x = 0;

	// Add the planar velocities - if there's enough friction they will
	// be removed during velocity resolution
	contactVelocity += accVelocity;

	// And return it
	return contactVelocity;
}

void RigidBodyContact::calculateDesiredDeltaVelocity(real duration) {
	const static real velocityLimit = real(0.25);

	// Calculate the acceleration induced velocity accumulated this frame
	real velocityFromAcc = 0;

	if (body[0]->getAwake()) {
		velocityFromAcc += dot(body[0]->getLastFrameAcceleration() * duration, contactNormal);
	}

	if (body[1] && body[1]->getAwake()) {
		velocityFromAcc -= dot(body[1]->getLastFrameAcceleration() * duration, contactNormal);
	}

	// If the velocity is very slow, limit the restitution
	real thisRestitution = restitution;
	if (real_abs(contactVelocity.x) < velocityLimit) {
		thisRestitution = real(0.0);
	}

	// Combine the bounce velocity with the removed
	// acceleration velocity.
	desiredDeltaVelocity = -contactVelocity.x
			- thisRestitution * (contactVelocity.x - velocityFromAcc);
}

void RigidBodyContact::calculateInternals(real duration) {
//	// Check if the first object is NULL, and swap if it is.
//	if (!body[0]) {
//		swapBodies();
//	}
//	assert(body[0]);

	// Calculate an set of axis at the contact point.
	calculateContactBasis();

	// Store the relative position of the contact relative to each body
	relativeContactPosition[0] = contactPoint - body[0]->getPosition();
	if (body[1]) {
		relativeContactPosition[1] = contactPoint - body[1]->getPosition();
	}

	// Find the relative velocity of the bodies at the contact point.
	contactVelocity = calculateLocalVelocity(0, duration);
	if (body[1]) {
		contactVelocity -= calculateLocalVelocity(1, duration);
	}

	// Calculate the desired change in velocity for resolution
	calculateDesiredDeltaVelocity(duration);
}

void RigidBodyContact::applyVelocityChange(rvec3 velocityChange[2],
		rvec3 rotationChange[2]) {
	// Get hold of the inverse mass and inverse inertia tensor, both in
	// world coordinates.
	rmat3 inverseInertiaTensor[2];
	inverseInertiaTensor[0] = body[0]->getInverseInertiaTensorWorld();
	if (body[1]) {
		inverseInertiaTensor[1] = body[1]->getInverseInertiaTensorWorld();
	}

	// We will calculate the impulse for each contact axis
	rvec3 impulseContact;

	if (friction == real(0.0)) {
		// Use the short format for frictionless contacts
		impulseContact = calculateFrictionlessImpulse(inverseInertiaTensor);
	} else {
		// Otherwise we may have impulses that aren't in the direction of the
		// contact, so we need the more complex version.
		impulseContact = calculateFrictionImpulse(inverseInertiaTensor);
	}

	// Convert impulse to world coordinates
	rvec3 impulse = contactToWorld * impulseContact;

	// Split in the impulse into linear and rotational components
	rvec3 impulsiveTorque = cross(relativeContactPosition[0], impulse);
	rotationChange[0] = inverseInertiaTensor[0] * impulsiveTorque;
	velocityChange[0] = rvec3(0);
	//	addScaledVector(velocityChange[0], impulse, body[0]->getInverseMass());
	velocityChange[0] += impulse * body[0]->getInverseMass();

	// Apply the changes
	body[0]->addVelocity(velocityChange[0]);
	body[0]->addAngularVelocity(rotationChange[0]);

	if (body[1]) {
		// Work out body one's linear and angular changes
		rvec3 impulsiveTorque = cross(impulse, relativeContactPosition[1]);
		rotationChange[1] = inverseInertiaTensor[1] * impulsiveTorque;
		velocityChange[1] = rvec3(0);
		//addScaledVector(velocityChange[1], impulse, -body[1]->getInverseMass());
		velocityChange[1] += impulse * (-body[1]->getInverseMass());

		// And apply them.
		body[1]->addVelocity(velocityChange[1]);
		body[1]->addAngularVelocity(rotationChange[1]);
	}
}

inline rvec3 RigidBodyContact::calculateFrictionlessImpulse(rmat3* inverseInertiaTensor) {
	rvec3 impulseContact;

	// Build a vector that shows the change in velocity in
	// world space for a unit impulse in the direction of the contact
	// normal.
	rvec3 deltaVelWorld = cross(relativeContactPosition[0], contactNormal);
	deltaVelWorld = inverseInertiaTensor[0] * deltaVelWorld;
	deltaVelWorld = cross(deltaVelWorld, relativeContactPosition[0]);

	// Work out the change in velocity in contact coordiantes.
	real deltaVelocity = dot(deltaVelWorld, contactNormal);

	// Add the linear component of velocity change
	deltaVelocity += body[0]->getInverseMass();

	// Check if we need to the second body's data
	if (body[1]) {
		// Go through the same transformation sequence again
		rvec3 deltaVelWorld = cross(relativeContactPosition[1], contactNormal);
		deltaVelWorld = inverseInertiaTensor[1] * deltaVelWorld;
		deltaVelWorld = cross(deltaVelWorld, relativeContactPosition[1]);

		// Add the change in velocity due to rotation
		deltaVelocity += dot(deltaVelWorld, contactNormal);

		// Add the change in velocity due to linear motion
		deltaVelocity += body[1]->getInverseMass();
	}

	// Calculate the required size of the impulse
	impulseContact.x = desiredDeltaVelocity / deltaVelocity;
	impulseContact.y = 0;
	impulseContact.z = 0;
	return impulseContact;
}

inline rvec3 RigidBodyContact::calculateFrictionImpulse(rmat3* inverseInertiaTensor) {
	rvec3 impulseContact;
	real inverseMass = body[0]->getInverseMass();

	// The equivalent of a cross product in matrices is multiplication
	// by a skew symmetric matrix - we build the matrix for converting
	// between linear and angular quantities.
	rmat3 impulseToTorque;
	MathHelper::setSkewSymmetric(impulseToTorque, relativeContactPosition[0]);

	// Build the matrix to convert contact impulse to change in velocity
	// in world coordinates.
	rmat3 deltaVelWorld = impulseToTorque;
	deltaVelWorld *= inverseInertiaTensor[0];
	deltaVelWorld *= impulseToTorque;
	deltaVelWorld *= real(-1);

	// Check if we need to add body two's data
	if (body[1]) {
		// Set the cross product matrix
		MathHelper::setSkewSymmetric(impulseToTorque, relativeContactPosition[1]);

		// Calculate the velocity change matrix
		rmat3 deltaVelWorld2 = impulseToTorque;
		deltaVelWorld2 *= inverseInertiaTensor[1];
		deltaVelWorld2 *= impulseToTorque;
		deltaVelWorld2 *= -1;

		// Add to the total delta velocity.
		deltaVelWorld += deltaVelWorld2;

		// Add to the inverse mass
		inverseMass += body[1]->getInverseMass();
	}

	// Do a change of basis to convert into contact coordinates.
	rmat3 deltaVelocity = glm::transpose(contactToWorld);
	deltaVelocity *= deltaVelWorld;
	deltaVelocity *= contactToWorld;

	// Add in the linear velocity change
//	deltaVelocity.data[0] += inverseMass;
//	deltaVelocity.data[4] += inverseMass;
//	deltaVelocity.data[8] += inverseMass;

	deltaVelocity[0][0] += inverseMass;
	deltaVelocity[1][1] += inverseMass;
	deltaVelocity[2][2] += inverseMass;

	// Invert to get the impulse needed per unit velocity
	rmat3 impulseMatrix = glm::inverse(deltaVelocity);

	// Find the target velocities to kill
	rvec3 velKill(desiredDeltaVelocity,
			-contactVelocity.y,
			-contactVelocity.z);

	// Find the impulse to kill target velocities
	impulseContact = impulseMatrix * velKill;

	// Check for exceeding friction
	real planarImpulse = real_sqrt(impulseContact.y * impulseContact.y
			+ impulseContact.z * impulseContact.z);

	if (planarImpulse > impulseContact.x * friction) {
		// We need to use dynamic friction
		impulseContact.y /= planarImpulse;
		impulseContact.z /= planarImpulse;

//		impulseContact.x = deltaVelocity.data[0]
//				+ deltaVelocity.data[1] * friction * impulseContact.y
//				+ deltaVelocity.data[2] * friction * impulseContact.z;
		impulseContact.x = deltaVelocity[0][0]
				+ deltaVelocity[1][0] * friction * impulseContact.y
				+ deltaVelocity[2][0] * friction * impulseContact.z;

		impulseContact.x = desiredDeltaVelocity / impulseContact.x;
		impulseContact.y *= friction * impulseContact.x;
		impulseContact.z *= friction * impulseContact.x;
	}
	return impulseContact;
}

/**
 * Adds the given vector to this, scaled by the given amount.
 * This is used to update the orientation quaternion by a rotation
 * and time.
 */
inline void addScaledVector(rquat& qu, const rvec3& vector, real scale) {
	rquat q(real(0), vector.x * scale, vector.y * scale, vector.z * scale);
	q = q * qu;
	qu.w += q.w * real(0.5);
	qu.x += q.x * real(0.5);
	qu.y += q.y * real(0.5);
	qu.z += q.z * real(0.5);
}

void RigidBodyContact::applyPositionChange(rvec3 linearChange[2],
		rvec3 angularChange[2], real penetration) {
	//REV_TRACE_FUNCTION;

	const real angularLimit = real(0.2);
	real angularMove[2];
	real linearMove[2];

	real totalInertia = 0;
	real linearInertia[2];
	real angularInertia[2];

	// We need to work out the inertia of each object in the direction
	// of the contact normal, due to angular inertia only.
	for (int i = 0; i < 2; i++) {
		if (body[i]) {
			rmat3 inverseInertiaTensor = body[i]->getInverseInertiaTensorWorld();

			// Use the same procedure as for calculating frictionless
			// velocity change to work out the angular inertia.
			rvec3 angularInertiaWorld = cross(relativeContactPosition[i], contactNormal);
			angularInertiaWorld = inverseInertiaTensor * angularInertiaWorld;
			angularInertiaWorld = cross(angularInertiaWorld, relativeContactPosition[i]);
			angularInertia[i] = dot(angularInertiaWorld, contactNormal);

			// The linear component is simply the inverse mass
			linearInertia[i] = body[i]->getInverseMass();

			// Keep track of the total inertia from all components
			totalInertia += linearInertia[i] + angularInertia[i];

			// We break the loop here so that the totalInertia value is
			// completely calculated (by both iterations) before
			// continuing.
		}
	}
	// Loop through again calculating and applying the changes
	for (int i = 0; i < 2; i++) {
		if (body[i]) {
			// The linear and angular movements required are in proportion to
			// the two inverse inertias.
			real sign = (i == 0) ? 1 : -1;
			angularMove[i] =
					sign * penetration * (angularInertia[i] / totalInertia);
			linearMove[i] =
					sign * penetration * (linearInertia[i] / totalInertia);

			// To avoid angular projections that are too great (when mass is large
			// but inertia tensor is small) limit the angular move.
			rvec3 projection = relativeContactPosition[i];
//			addScaledVector(projection, contactNormal,
//					glm::dot(-relativeContactPosition[i], contactNormal));
			projection += contactNormal * glm::dot(-relativeContactPosition[i], contactNormal);

			// Use the small angle approximation for the sine of the angle (i.e.
			// the magnitude would be sine(angularLimit) * projection.magnitude
			// but we approximate sine(angularLimit) to angularLimit).
			real maxMagnitude = angularLimit * glm::length(projection);

			if (angularMove[i] < -maxMagnitude) {
				real totalMove = angularMove[i] + linearMove[i];
				angularMove[i] = -maxMagnitude;
				linearMove[i] = totalMove - angularMove[i];
			} else if (angularMove[i] > maxMagnitude) {
				real totalMove = angularMove[i] + linearMove[i];
				angularMove[i] = maxMagnitude;
				linearMove[i] = totalMove - angularMove[i];
			}

			// We have the linear amount of movement required by turning
			// the rigid body (in angularMove[i]). We now need to
			// calculate the desired rotation to achieve that.
			if (angularMove[i] == 0) {
				// Easy case - no angular movement means no rotation.
				angularChange[i] = rvec3(0);			//.clear();
			} else {
				// Work out the direction we'd like to rotate in.
				rvec3 targetAngularDirection = glm::cross(relativeContactPosition[i],
						contactNormal);

				rmat3 inverseInertiaTensor = body[i]->getInverseInertiaTensorWorld();

				// Work out the direction we'd need to rotate to achieve that
				angularChange[i] = (inverseInertiaTensor * targetAngularDirection)
						* (angularMove[i] / angularInertia[i]);
			}

			// Velocity change is easier - it is just the linear movement
			// along the contact normal.
			linearChange[i] = contactNormal * linearMove[i];

			// Now we can start to apply the values we've calculated.
			// Apply the linear movement
			rvec3 pos = body[i]->getPosition();
			//addScaledVector(pos, contactNormal, linearMove[i]);
			pos += contactNormal * linearMove[i];
			body[i]->setPosition(pos);

			// And the change in orientation
			rquat q = body[i]->getOrientation();
			addScaledVector(q, angularChange[i], real(1.0));
			body[i]->setOrientation(q);

			// We need to calculate the derived data for any body that is
			// asleep, so that the changes are reflected in the object's
			// data. Otherwise the resolution will not change the position
			// of the object, and the next collision detection round will
			// have the same penetration.
			if (!body[i]->getAwake()) {
				body[i]->calculateDerivedData();
			}
		}
	}
}
