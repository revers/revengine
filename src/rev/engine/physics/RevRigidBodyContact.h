/*
 * RevRigidBodyContact.h
 *
 *  Created on: 25-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVRIGIDBODYCONTACT_H_
#define REVRIGIDBODYCONTACT_H_

#include <rev/engine/math/RevReal.h>
#include <rev/engine/collision/RevCollisionContact.h>
#include <rev/engine/components/RevRigidBodyComponent.h>

namespace rev {

	class RigidBodyCollisionResolver;

	struct RigidBodyContact {
		friend class RigidBodyCollisionResolver;

	public:
		/**
		 * Holds the bodies that are involved in the contact. The
		 * second of these can be nullptr, for contacts with the scenery.
		 */
		union {
			RigidBodyComponent* body[2];
			struct {
				RigidBodyComponent* first;
				RigidBodyComponent* second;
			};
		};

		/**
		 * Holds the position of the contact in world coordinates.
		 */
		rvec3 contactPoint;

		/**
		 * Holds the direction of the contact in world coordinates.
		 */
		rvec3 contactNormal;

		/**
		 * Holds the depth of penetration at the contact point. If both
		 * bodies are specified then the contact point should be midway
		 * between the inter-penetrating points.
		 */
		real penetration = real(0.0);

	protected:
		/**
		 * Holds the lateral friction coefficient at the contact.
		 */
		real friction;

		/**
		 * Holds the normal restitution coefficient at the contact.
		 */
		real restitution;

		/**
		 * A transform matrix that converts co-ordinates in the contact's
		 * frame of reference to world co-ordinates. The columns of this
		 * matrix form an orthonormal set of vectors.
		 */
		rmat3 contactToWorld;

		/**
		 * Holds the closing velocity at the point of contact. This is set
		 * when the calculateInternals function is run.
		 */
		rvec3 contactVelocity;

		/**
		 * Holds the required change in velocity for this contact to be
		 * resolved.
		 */
		real desiredDeltaVelocity;

		/**
		 * Holds the world space position of the contact point relative to
		 * center of each body. This is set when the calculateInternals
		 * function is run.
		 */
		rvec3 relativeContactPosition[2];

	public:
		RigidBodyContact() :
				first(nullptr), second(nullptr) {
		}
		RigidBodyContact(const CollisionContact& contact);

		void set(const CollisionContact& contact);

	private:
		/**
		 * Reverses the contact. This involves swapping the two rigid bodies
		 * and reversing the contact normal. The internal values should then
		 * be recalculated using calculateInternals (this is not done
		 * automatically).
		 */
		void swapBodies();

		/**
		 * Calculates internal data from state data. This is called before
		 * the resolution algorithm tries to do any resolution. It should
		 * never need to be called manually.
		 */
		void calculateInternals(real duration);

		/**
		 * Updates the awake state of rigid bodies that are taking
		 * place in the given contact. A body will be made awake if it
		 * is in contact with a body that is awake.
		 */
		void matchAwakeState();

		/**
		 * Calculates and sets the internal value for the desired delta
		 * velocity.
		 */
		void calculateDesiredDeltaVelocity(real duration);

		/**
		 * Calculates and returns the velocity of the contact
		 * point on the given body.
		 */
		rvec3 calculateLocalVelocity(int bodyIndex, real duration);

		/**
		 * Calculates an orthonormal basis for the contact point, based on
		 * the primary friction direction (for anisotropic friction) or
		 * a random orientation (for isotropic friction).
		 */
		void calculateContactBasis();

		/**
		 * Applies an impulse to the given body, returning the
		 * change in velocities.
		 */
		void applyImpulse(const rvec3& impulse, RigidBodyComponent* body,
				rvec3* velocityChange, rvec3* rotationChange);

		/**
		 * Performs an inertia-weighted impulse based resolution of this
		 * contact alone.
		 */
		void applyVelocityChange(rvec3 velocityChange[2],
				rvec3 rotationChange[2]);

		/**
		 * Performs an inertia weighted penetration resolution of this
		 * contact alone.
		 */
		void applyPositionChange(rvec3 linearChange[2],
				rvec3 angularChange[2],
				real penetration);

		/**
		 * Calculates the impulse needed to resolve this contact,
		 * given that the contact has no friction. A pair of inertia
		 * tensors - one for each contact object - is specified to
		 * save calculation time: the calling function has access to
		 * these anyway.
		 */
		rvec3 calculateFrictionlessImpulse(rmat3* inverseInertiaTensor);

		/**
		 * Calculates the impulse needed to resolve this contact,
		 * given that the contact has a non-zero coefficient of
		 * friction. A pair of inertia tensors - one for each contact
		 * object - is specified to save calculation time: the calling
		 * function has access to these anyway.
		 */
		rvec3 calculateFrictionImpulse(rmat3* inverseInertiaTensor);

	};

} /* namespace rev */

#endif /* REVRIGIDBODYCONTACT_H_ */
