/*
 * RevPhysicsManager.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/RevGameObject.h>
#include "RevPhysicsManager.h"

using namespace rev;

rev::PhysicsManager* rev::PhysicsManager::physicsManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(PhysicsManager)

PhysicsManager::PhysicsManager() :
		dt(0.001) {
}

void PhysicsManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!physicsManager);
	physicsManager = new PhysicsManager();
	REV_TRACE_FUNCTION_OUT;
}

void PhysicsManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(physicsManager);

	delete physicsManager;
	physicsManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void PhysicsManager::resolveContacts(CollisionContact* contactArray, int numContacts) {
	real frameTime = real(Engine::ref().getCurrentFrameTime()) * real(0.001);

	//--------------------------------------------------------
	// TODO: temporary solution?
	rigidBodyContactsSize = 0;
	CollisionContact* lastContact = contactArray + numContacts;
	for (CollisionContact* contact = contactArray; contact < lastContact; contact++) {
		IPhysicsComponent* firstPhysics = nullptr;
		if (contact->first != nullptr) {
			firstPhysics = contact->first->getParent()->getPhysicsComponent();
		}
		IPhysicsComponent* secondPhysics = nullptr;
		if (contact->second != nullptr) {
			secondPhysics = contact->second->getParent()->getPhysicsComponent();
		}

		if (firstPhysics != nullptr || secondPhysics != nullptr) {
			rigidBodyContacts[rigidBodyContactsSize++] = contact;
		}
	}
	rigidBodyResolver.setIterations(rigidBodyContactsSize * 2);
	rigidBodyResolver.resolveContacts(rigidBodyContacts, rigidBodyContactsSize, frameTime);
	//========================================================

	// rigidBodyResolver.setIterations(numContacts * 2);
	// rigidBodyResolver.resolveContacts(contactArray, numContacts, frameTime);
}

/**
 * http://gafferongames.com/game-physics/fix-your-timestep/
 */
void PhysicsManager::update() {
	if (paused) {
		return;
	}
	// time in seconds:
	real frameTime = real(Engine::ref().getCurrentFrameTime()) * real(0.001);

	accumulator += frameTime;
	// REV_TRACE_MSG(R(accumulator));

	while (accumulator >= dt) {
		for (IPhysicsComponent*& c : components) {
			c->update(dt);
		}
		accumulator -= dt;
		totalTime += dt;
	}
}

void PhysicsManager::bind(IBinder& binder) {
	binder.bindSimple(paused);
	binder.bindInfo(this, NVP(numComponents));
}
