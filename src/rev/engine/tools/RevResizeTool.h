/*
 * RevResizeTool.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVRESIZETOOL_H_
#define REVRESIZETOOL_H_

#include "RevITool.h"

namespace rev {

	class GameObject;

	class ResizeTool: public ITool {
		GameObject* pickedGameObject = nullptr;
		int lastMouseY = 0;

	public:
		ResizeTool();
		~ResizeTool();

		void mousePressed(int x, int y, int contextIndex) override;
		void mouseDrag(int x, int y, int contextIndex) override;
		void mouseReleased(int x, int y, int contextIndex) override;
		const char* getToolName() override {
			return "ResizeTool";
		}

	private:
		GameObject* getPickedGameObject();
	};

} /* namespace rev */
#endif /* REVRESIZETOOL_H_ */
