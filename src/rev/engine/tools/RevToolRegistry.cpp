/*
 * RevToolRegistry.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include "RevToolRegistry.h"

using namespace rev;

void ToolRegistry::setCurrentTool(ITool* currentTool) {
	this->currentTool = currentTool;
	for (ToolSwitchedCallbackWrapper& wrapper: callbackVector) {
		wrapper.callback(currentTool);
	}
}

int ToolRegistry::addToolSwitchedCallback(ToolSwitchedCallback callback) {
	int id = callbackId++;
	callbackVector.push_back({id, callback});
	return id;
}

void ToolRegistry::removeToolSwitchedCallback(int id) {
	callbackVector.erase(std::remove(callbackVector.begin(), callbackVector.end(), id),
			callbackVector.end());
}
