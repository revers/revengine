/*
 * RevITool.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVITOOL_H_
#define REVITOOL_H_

#include <vector>
#include <rev/engine/events/RevKeyEvent.h>

namespace rev {

	class ITool;
	typedef std::function<void(ITool*)> ToolChangedCallback;

	class ITool {
		struct ToolChangedCallbackWrapper {
			int id;
			ToolChangedCallback callback;
			bool operator==(int id) {
				return this->id == id;
			}
		};
		typedef std::vector<ToolChangedCallbackWrapper> ToolChangedCallbackVector;

		ToolChangedCallbackVector callbackVector;
		int callbackId = 0;

	public:
		virtual ~ITool() {
		}

		virtual void mousePressed(int x, int y, int contextIndex) {
		}
		virtual void mouseDrag(int x, int y, int contextIndex) {
		}
		virtual void mouseMoved(int x, int y, int contextIndex) {
		}
		virtual void mouseReleased(int x, int y, int contextIndex) {
		}
		virtual void keyPressed(const KeyEvent& e) {
		}
		virtual void keyReleased(const KeyEvent& e) {
		}
		virtual const char* getToolName() = 0;

		/**
		 * @return id of the callback function. It is needed for
		 * removeToolChangedCallback() function.
		 */
		int addToolChangedCallback(ToolChangedCallback callback);

		/**
		 * @param id - id returned by addToolChangedCallback()
		 */
		void removeToolChangedCallback(int id);

	protected:
		void fireToolChangedCallback();
	};

} /* namespace rev */

#endif /* REVITOOL_H_ */
