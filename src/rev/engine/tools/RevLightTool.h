/*
 * RevLightTool.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVLIGHTTOOL_H_
#define REVLIGHTTOOL_H_

#include "RevITool.h"

namespace rev {

	class LightTool: public ITool {
	public:
		LightTool() {
		}

		const char* getToolName() override {
			return "LightTool";
		}
	};

} /* namespace rev */
#endif /* REVLIGHTTOOL_H_ */
