/*
 * RevMoveLinesComponent.cpp
 *
 *  Created on: 15-04-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/engine/util/RevGlmOstream.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevGameObject.h>
#include "RevMoveLinesComponent.h"

using namespace rev;
using namespace glm;

MoveLinesComponent::MoveLinesComponent() :
		lineA(vec3(-10, 0, 0), vec3(10, 0, 0), color3(1, 0, 0)),
				lineB(vec3(0, 0, -10), vec3(0, 0, 10), color3(0, 0, 1)),
				dashedLineA(vec3(-10, 0, 2), vec3(10, 0, 2), color3(1, 1, 1)),
				dashedLineB(vec3(2, 0, -10), vec3(2, 0, 10), color3(1, 1, 1)) {
	parentObject = new GameObject();
	setParent(parentObject);
}

MoveLinesComponent::~MoveLinesComponent() {
	delete parentObject;
}

RenderPrimitiveType MoveLinesComponent::getRenderPrimitiveType() const {
	return lineA.getRenderPrimitiveType();
}

void MoveLinesComponent::applyEffect(const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const ICamera& camera) {
	effect.apply(modelMatrix, viewMatrix, projectionMatrix, camera);
}

void MoveLinesComponent::render() {
	lineA.render();
	lineB.render();
	dashedLineA.render();
	dashedLineB.render();
}

const rev::BoundingSphere* MoveLinesComponent::getBoundingSphere() {
	return lineA.getBoundingSphere();
}

int MoveLinesComponent::getPrimitiveCount() {
	return lineA.getPrimitiveCount() + lineB.getPrimitiveCount()
			+ dashedLineA.getPrimitiveCount() + dashedLineB.getPrimitiveCount();
}

void MoveLinesComponent::setCenter(const glm::vec3& center) {
	this->center = center;
	if (mode == Mode::XZ) {
		lineA.setP1(vec3(center.x - 500, center.y, center.z));
		lineA.setP2(vec3(center.x + 500, center.y, center.z));
		lineA.setColor(color3(1, 0, 0));

		lineB.setP1(vec3(center.x, center.y, center.z - 500));
		lineB.setP2(vec3(center.x, center.y, center.z + 500));
		lineB.setColor(color3(0, 0, 1));
	} else if (mode == Mode::XY) {
		lineA.setP1(vec3(center.x - 500, center.y, center.z));
		lineA.setP2(vec3(center.x + 500, center.y, center.z));
		lineA.setColor(color3(1, 0, 0));

		lineB.setP1(vec3(center.x, center.y - 500, center.z));
		lineB.setP2(vec3(center.x, center.y + 500, center.z));
		lineB.setColor(color3(0, 1, 0));
	} else if (mode == Mode::YZ) {
		lineA.setP1(vec3(center.x, center.y - 500, center.z));
		lineA.setP2(vec3(center.x, center.y + 500, center.z));
		lineA.setColor(color3(0, 1, 0));

		lineB.setP1(vec3(center.x, center.y, center.z - 500));
		lineB.setP2(vec3(center.x, center.y, center.z + 500));
		lineB.setColor(color3(0, 0, 1));
	}
	lineA.reinit();
	lineB.reinit();
}

void MoveLinesComponent::setPosition(const glm::vec3& position) {
	this->position = position;

	if (mode == Mode::XZ) {
		dashedLineA.setP1(vec3(position.x, position.y, position.z));
		dashedLineA.setP2(vec3(center.x, position.y, position.z));

		dashedLineB.setP1(vec3(position.x, position.y, position.z));
		dashedLineB.setP2(vec3(position.x, position.y, center.z));
	} else if (mode == Mode::XY) {
		dashedLineA.setP1(vec3(position.x, position.y, position.z));
		dashedLineA.setP2(vec3(center.x, position.y, position.z));

		dashedLineB.setP1(vec3(position.x, position.y, position.z));
		dashedLineB.setP2(vec3(position.x, center.y, position.z));
	} else if (mode == Mode::YZ) {
		dashedLineA.setP1(vec3(position.x, position.y, position.z));
		dashedLineA.setP2(vec3(position.x, center.y, position.z));

		dashedLineB.setP1(vec3(position.x, position.y, position.z));
		dashedLineB.setP2(vec3(position.x, position.y, center.z));
	}

	dashedLineA.reinit();
	dashedLineB.reinit();
}

void MoveLinesComponent::setMode(Mode mode) {
	this->mode = mode;
	setCenter(center);
	setPosition(position);
}
