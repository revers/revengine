/*
 * RevTripleCircleComponent.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVTRIPLECIRCLECOMPONENT_H_
#define REVTRIPLECIRCLECOMPONENT_H_

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/drawables/RevVBOCircleVC.h>
#include <rev/engine/effects/RevFlatColorEffect.h>

namespace rev {

	class GameObject;

	class TripleCircleComponent: public IVisual3DComponent {
		VBOCircleVC redCircle;
		VBOCircleVC greenCircle;
		VBOCircleVC blueCircle;
		FlatColorEffect effect;
		GameObject* parentObject;

		bool redOnly = false;
		bool greenOnly = false;
		bool blueOnly = false;

	public:
		DECLARE_WEAK_BINDABLE(TripleCircleComponent)

		TripleCircleComponent();
		~TripleCircleComponent();

		void applyEffect(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix, const ICamera& camera) override;

		void render() override;

		const rev::BoundingSphere* getBoundingSphere() override;

		int getPrimitiveCount() override;

		RenderPrimitiveType getRenderPrimitiveType() const override;

		void setRotation(const glm::quat& rotation);
		void setRadius(float radius);
		void setPosition(const glm::vec3& position);

		glm::vec3 getRedNormal();
		glm::vec3 getGreenNormal();
		glm::vec3 getBlueNormal();

		bool isBlueOnly() const {
			return blueOnly;
		}
		void setBlueOnly(bool blueOnly) {
			this->blueOnly = blueOnly;
		}
		bool isGreenOnly() const {
			return greenOnly;
		}
		void setGreenOnly(bool greenOnly) {
			this->greenOnly = greenOnly;
		}
		bool isRedOnly() const {
			return redOnly;
		}
		void setRedOnly(bool redOnly) {
			this->redOnly = redOnly;
		}
		void setAllVisible() {
			redOnly = false;
			greenOnly = false;
			blueOnly = false;
		}

	};

} /* namespace rev */
#endif /* REVTRIPLECIRCLECOMPONENT_H_ */
