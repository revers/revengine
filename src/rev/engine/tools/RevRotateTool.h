/*
 * RevRotateTool.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVROTATETOOL_H_
#define REVROTATETOOL_H_

#include <glm/gtx/quaternion.hpp>
#include "RevITool.h"
#include "RevTripleCircleComponent.h"

namespace rev {

class GameObject;

class RotateTool: public ITool {
	TripleCircleComponent* component = nullptr;
	GameObject* pickedGameObject = nullptr;
	glm::vec3 stVec;
	glm::vec3 enVec;
	glm::quat lastRotation;
	bool redOnly = false;
	bool greenOnly = false;
	bool blueOnly = false;
	bool mouseButtonPressed = false;
	bool keyButtonPressed = false;
	bool componentAdded = false;

public:
	RotateTool();
	~RotateTool();

	void mousePressed(int x, int y, int contextIndex) override;
	void mouseDrag(int x, int y, int contextIndex) override;
	void mouseReleased(int x, int y, int contextIndex) override;
	void keyPressed(const KeyEvent& e) override;
	void keyReleased(const KeyEvent& e) override;
	const char* getToolName() override {
		return "RotateTool";
	}

private:
	void mapToSphere(int x, int y, int contextIndex, glm::vec3& v);
	GameObject* getPickedGameObject();

	glm::vec3 getNearPlanePoint(int x, int y, int contextIndex);
};

} /* namespace rev */
#endif /* REVROTATETOOL_H_ */
