/*
 * RevMoveTool.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>

#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/picking/RevColorCodingPicker.h>
#include "RevMoveTool.h"

using namespace rev;
using namespace glm;

MoveTool::MoveTool() {
	component = new MoveLinesComponent();
	component->setDebugElement(true);
}

MoveTool::~MoveTool() {
	// FIXME: same problem with destroying VBODrawable in this stage
//	if (component) {
//		delete component;
//	}
}

GameObject* MoveTool::getPickedGameObject() {
	ColorCodingPicker& picker = Renderer3D::ref().getPicker();
	pickedGameObject = picker.getPickedGameObject();
	return pickedGameObject;
}

void MoveTool::setPosition(int x, int y, int contextIndex) {
	ICamera* camera = CameraManager::ref().getCamera(contextIndex);
	Ray ray = camera->getRay(x, y);
	float t;
	if (plane.intersect(ray, &t)) {
		vec3 pos = ray.o + t * ray.d;
		pickedGameObject->setPosition(pos);
	}
	component->setPosition(pickedGameObject->getPosition());
}

void MoveTool::mousePressed(int x, int y, int contextIndex) {
	if (!getPickedGameObject()) {
		return;
	}
	mouseButtonPressed = true;
	if (component->getMode() == MoveLinesComponent::Mode::XZ) {
		plane = Plane(vec3(0, 1, 0), pickedGameObject->getPosition());
	} else if (component->getMode() == MoveLinesComponent::Mode::XY) {
		plane = Plane(vec3(0, 0, 1), pickedGameObject->getPosition());
	} else if (component->getMode() == MoveLinesComponent::Mode::YZ) {
		plane = Plane(vec3(1, 0, 0), pickedGameObject->getPosition());
	}

	component->setCenter(pickedGameObject->getPosition());
	setPosition(x, y, contextIndex);
	if (!componentAdded) {
		Renderer3D::ref().addComponent(component);
		componentAdded = true;
	}
}

void MoveTool::mouseDrag(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}

	setPosition(x, y, contextIndex);
	fireToolChangedCallback();
}

void MoveTool::mouseReleased(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}
	mouseButtonPressed = false;
	if (componentAdded && !keyButtonPressed) {
		Renderer3D::ref().removeComponent(component);
		componentAdded = false;
	}
}

void MoveTool::keyPressed(const KeyEvent& e) {
	if (!getPickedGameObject()) {
		return;
	}
	if (!mouseButtonPressed) {
		component->setCenter(pickedGameObject->getPosition());
		component->setPosition(pickedGameObject->getPosition());
	}
	bool pressed = false;
	if (e.getKey() == Key::KEY_I || e.getKey() == Key::KEY_Q) {
		pressed = true;
		component->setMode(MoveLinesComponent::Mode::XZ);
	} else if (e.getKey() == Key::KEY_O || e.getKey() == Key::KEY_W) {
		pressed = true;
		component->setMode(MoveLinesComponent::Mode::XY);
	} else if (e.getKey() == Key::KEY_P || e.getKey() == Key::KEY_E) {
		pressed = true;
		component->setMode(MoveLinesComponent::Mode::YZ);
	}
	if (pressed && !componentAdded) {
		Renderer3D::ref().addComponent(component);
		componentAdded = true;
	}
}

void MoveTool::keyReleased(const KeyEvent& e) {
	bool released = false;
	if (e.getKey() == Key::KEY_I || e.getKey() == Key::KEY_Q) {
		released = true;
		component->setMode(MoveLinesComponent::Mode::XZ);
	} else if (e.getKey() == Key::KEY_O || e.getKey() == Key::KEY_W) {
		released = true;
		component->setMode(MoveLinesComponent::Mode::XZ);
	} else if (e.getKey() == Key::KEY_P || e.getKey() == Key::KEY_E) {
		released = true;
		component->setMode(MoveLinesComponent::Mode::XZ);
	}

	if (released) {
		keyButtonPressed = false;
		if (componentAdded && !mouseButtonPressed) {
			Renderer3D::ref().removeComponent(component);
			componentAdded = false;
		}
	}
}
