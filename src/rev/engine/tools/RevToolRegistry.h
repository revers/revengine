/*
 * RevToolRegistry.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVTOOLREGISTRY_H_
#define REVTOOLREGISTRY_H_

#include <vector>
#include <functional>
#include "RevITool.h"

namespace rev {

	typedef std::function<void(ITool*)> ToolSwitchedCallback;

	class ToolRegistry {

		ToolRegistry(const ToolRegistry&) = delete;
		ToolRegistry(ToolRegistry&&) = delete;
		ToolRegistry& operator=(const ToolRegistry&) = delete;

		struct ToolSwitchedCallbackWrapper {
			int id;
			ToolSwitchedCallback callback;
			bool operator==(int id) {
				return this->id == id;
			}
		};
		typedef std::vector<ToolSwitchedCallbackWrapper> ToolSwitchedCallbackVector;

		ToolSwitchedCallbackVector callbackVector;
		ITool* currentTool = nullptr;
		int callbackId = 0;

	public:
		ToolRegistry() {
		}

		ITool* getCurrentTool() {
			return currentTool;
		}
		void setCurrentTool(ITool* currentTool);

		/**
		 * @return id of the callback function. It is needed for
		 * removeToolSwitchedCallback() function.
		 */
		int addToolSwitchedCallback(ToolSwitchedCallback callback);

		/**
		 * @param id - id returned by addToolSwitchedCallback()
		 */
		void removeToolSwitchedCallback(int id);
	};

} /* namespace rev */
#endif /* REVTOOLREGISTRY_H_ */
