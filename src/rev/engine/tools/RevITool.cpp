/*
 * RevITool.cpp
 *
 *  Created on: 16-04-2013
 *      Author: Revers
 */

#include "RevITool.h"

using namespace rev;

int ITool::addToolChangedCallback(ToolChangedCallback callback) {
	int id = callbackId++;
	callbackVector.push_back( { id, callback });
	return id;
}

void ITool::removeToolChangedCallback(int id) {
	callbackVector.erase(std::remove(callbackVector.begin(), callbackVector.end(), id),
			callbackVector.end());
}

void ITool::fireToolChangedCallback() {
	for (ToolChangedCallbackWrapper& wrapper: callbackVector) {
			wrapper.callback(this);
		}
}
