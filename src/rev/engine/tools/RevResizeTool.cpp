/*
 * RevResizeTool.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>

#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/picking/RevColorCodingPicker.h>
#include "RevResizeTool.h"

using namespace rev;
using namespace glm;

ResizeTool::ResizeTool() {
}

ResizeTool::~ResizeTool() {
}

GameObject* ResizeTool::getPickedGameObject() {
	ColorCodingPicker& picker = Renderer3D::ref().getPicker();
	pickedGameObject = picker.getPickedGameObject();
	return pickedGameObject;
}

void ResizeTool::mousePressed(int x, int y, int contextIndex) {
	if (!getPickedGameObject()) {
		return;
	}

	lastMouseY = y;
}

void ResizeTool::mouseDrag(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}

	int diffY = lastMouseY - y;
	const float scaleFactor = 0.03f;
	if (diffY < 0) {
		float s = 1.0f + scaleFactor * (-diffY);
		vec3 scale = pickedGameObject->getScaling() / s;
		pickedGameObject->setScaling(scale);
	} else if (diffY > 0) {
		float s = 1.0f + scaleFactor * (diffY);
		vec3 scale = pickedGameObject->getScaling() * s;
		pickedGameObject->setScaling(scale);
	}

	lastMouseY = y;
	fireToolChangedCallback();
}

void ResizeTool::mouseReleased(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}
}

