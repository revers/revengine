/*
 * RevMoveLinesComponent.h
 *
 *  Created on: 15-04-2013
 *      Author: Revers
 */

#ifndef REVMOVELINESCOMPONENT_H_
#define REVMOVELINESCOMPONENT_H_

#include <glm/glm.hpp>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/drawables/RevVBOLineVC.h>
#include <rev/engine/drawables/RevVBODashedLineVC.h>
#include <rev/engine/effects/RevFlatColorEffect.h>

namespace rev {

	class GameObject;

	class MoveLinesComponent: public IVisual3DComponent {
	public:
		enum class Mode {
			XZ, XY, YZ
		};

	private:
		VBOLineVC lineA;
		VBOLineVC lineB;
		VBODashedLineVC dashedLineA;
		VBODashedLineVC dashedLineB;

		FlatColorEffect effect;
		GameObject* parentObject;

		glm::vec3 center;
		glm::vec3 position;
		Mode mode = Mode::XZ;

	public:
		DECLARE_WEAK_BINDABLE(MoveLinesComponent)

		MoveLinesComponent();
		~MoveLinesComponent();

		void applyEffect(const glm::mat4& modelMatrix, const glm::mat4& viewMatrix,
				const glm::mat4& projectionMatrix, const ICamera& camera) override;

		void render() override;

		const rev::BoundingSphere* getBoundingSphere() override;

		int getPrimitiveCount() override;

		RenderPrimitiveType getRenderPrimitiveType() const override;

		const glm::vec3& getCenter() const {
			return center;
		}
		void setCenter(const glm::vec3& center);

		const glm::vec3& getPosition() const {
			return position;
		}
		void setPosition(const glm::vec3& position);

		Mode getMode() const {
			return mode;
		}
		void setMode(Mode mode);
	};

} /* namespace rev */
#endif /* REVMOVELINESCOMPONENT_H_ */
