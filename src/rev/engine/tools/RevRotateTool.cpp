/*
 * RevRotateTool.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>

#include <rev/engine/util/RevGLMOstream.h>
#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/RevRenderer3D.h>
#include <rev/engine/camera/RevCameraManager.h>
#include <rev/engine/picking/RevColorCodingPicker.h>
#include "RevRotateTool.h"

using namespace rev;
using namespace glm;

RotateTool::RotateTool() {
	component = new TripleCircleComponent();
	component->setDebugElement(true);
}

RotateTool::~RotateTool() {
	// FIXME: same problem with destroying VBODrawable in this stage
//	if (component) {
//		delete component;
//	}
}

GameObject* RotateTool::getPickedGameObject() {
	ColorCodingPicker& picker = Renderer3D::ref().getPicker();
	pickedGameObject = picker.getPickedGameObject();
	return pickedGameObject;
}

#define MAX(x, y) ((x) > (y) ? (x) : (y))

void RotateTool::mousePressed(int x, int y, int contextIndex) {
	if (!getPickedGameObject()) {
		return;
	}
	mouseButtonPressed = true;

	ICamera* camera = CameraManager::ref().getCamera(contextIndex);

	const BoundingSphere* bs = pickedGameObject->getVisual3DComponent()->getBoundingSphere();
	component->setPosition(bs->c + pickedGameObject->getPosition());

	const vec3& sv = pickedGameObject->getScaling();
	float s = MAX(sv.x, MAX(sv.y, sv.z));
	component->setRadius(bs->r * s);
	component->setRotation(pickedGameObject->getOrientation());
	if (!componentAdded) {
		Renderer3D::ref().addComponent(component);
		componentAdded = true;
	}
	lastRotation = pickedGameObject->getOrientation();

	mapToSphere(x, y, contextIndex, stVec);
}

void RotateTool::mouseDrag(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}

	mapToSphere(x, y, contextIndex, enVec);
	float angle = acos(std::min(1.0f, glm::dot(stVec, enVec)));

	if (!redOnly && !greenOnly && !blueOnly) {
		angle *= 2.0f;
		vec3 axis = glm::cross(enVec, stVec);
		stVec = enVec;

		quat rotation = MathHelper::fromAxisAngle(axis, angle);
		quat rot = rotation * pickedGameObject->getOrientation();
		rot = glm::normalize(rot);

		pickedGameObject->setOrientation(rot);
		component->setRotation(rot);
	} else {
		vec3 axis = glm::cross(enVec, stVec);
		quat rotation = MathHelper::fromAxisAngle(axis, angle);
		quat rot = rotation * lastRotation;
		rot = glm::normalize(rot);

		pickedGameObject->setOrientation(rot);
		component->setRotation(rot);
	}

	fireToolChangedCallback();
}

void RotateTool::mouseReleased(int x, int y, int contextIndex) {
	if (!pickedGameObject) {
		return;
	}
	mouseButtonPressed = false;
	if (componentAdded && !keyButtonPressed) {
		Renderer3D::ref().removeComponent(component);
		componentAdded = false;
	}
}

void RotateTool::mapToSphere(int x, int y, int contextIndex, glm::vec3& v) {
	ICamera* camera = CameraManager::ref().getCamera(contextIndex);

	// FIXME: Why this not work?
	// vec3 vec = camera->getNearPlanePoint(x, y);
	vec3 vec = getNearPlanePoint(x, y, contextIndex);

	const BoundingSphere* bs = pickedGameObject->getVisual3DComponent()->getBoundingSphere();
	vec3 center = bs->c + pickedGameObject->getPosition();

	if (!redOnly && !greenOnly && !blueOnly) {
		v = glm::normalize(vec - center);
		return;
	}

	vec3 b;
	if (redOnly) {
		b = glm::normalize(component->getRedNormal());
	} else if (greenOnly) {
		b = glm::normalize(component->getGreenNormal());
	} else if (blueOnly) {
		b = glm::normalize(component->getBlueNormal());
	}
	v = vec - center;
	// http://www.euclideanspace.com/maths/geometry/elements/plane/lineOnPlane/index.htm
	vec3 proj = glm::cross(b, glm::cross(v, b));
	v = glm::normalize(proj);
}

void RotateTool::keyPressed(const KeyEvent& e) {
	if (!getPickedGameObject()) {
		return;
	}
	if (!mouseButtonPressed) {
		const BoundingSphere* bs = pickedGameObject->getVisual3DComponent()->getBoundingSphere();
		component->setPosition(bs->c + pickedGameObject->getPosition());

		const vec3& sv = pickedGameObject->getScaling();
		float s = MAX(sv.x, MAX(sv.y, sv.z));
		component->setRadius(bs->r * s);
		component->setRotation(pickedGameObject->getOrientation());
	}
	if (e.getKey() == Key::KEY_I || e.getKey() == Key::KEY_Q) {
		redOnly = true;
		component->setRedOnly(true);
		keyButtonPressed = true;
	} else if (e.getKey() == Key::KEY_O || e.getKey() == Key::KEY_W) {
		greenOnly = true;
		component->setGreenOnly(true);
		keyButtonPressed = true;
	} else if (e.getKey() == Key::KEY_P || e.getKey() == Key::KEY_E) {
		blueOnly = true;
		component->setBlueOnly(true);
		keyButtonPressed = true;
	}

	if ((redOnly || greenOnly || blueOnly) && !componentAdded) {
		Renderer3D::ref().addComponent(component);
		componentAdded = true;
	}
}

void RotateTool::keyReleased(const KeyEvent& e) {
	if (e.getKey() == Key::KEY_I || e.getKey() == Key::KEY_Q) {
		redOnly = false;
		component->setRedOnly(false);
	} else if (e.getKey() == Key::KEY_O || e.getKey() == Key::KEY_W) {
		greenOnly = false;
		component->setGreenOnly(false);
	} else if (e.getKey() == Key::KEY_P || e.getKey() == Key::KEY_E) {
		blueOnly = false;
		component->setBlueOnly(false);
	}

	if (!redOnly && !greenOnly && !blueOnly) {
		keyButtonPressed = false;
		if (componentAdded && !mouseButtonPressed) {
			Renderer3D::ref().removeComponent(component);
			componentAdded = false;
		}
	}
}

glm::vec3 RotateTool::getNearPlanePoint(int x, int y, int contextIndex) {
	ICamera* camera = CameraManager::ref().getCamera(contextIndex);
	int screenWidth = camera->getScreenWidth();
	int screenHeight = camera->getScreenHeight();
	const ViewFrustum& frustum = camera->getViewFrustum();

	vec3 nearTopLeft = Plane::intersection(frustum.nearPlane, frustum.leftPlane,
			frustum.topPlane);
	vec3 nearTopRight = Plane::intersection(frustum.nearPlane, frustum.rightPlane,
			frustum.topPlane);
	vec3 nearBottomLeft = Plane::intersection(frustum.nearPlane, frustum.leftPlane,
			frustum.bottomPlane);

	vec3 horDirection = nearTopRight - nearTopLeft;
	float horLength = glm::length(horDirection);
	horDirection /= horLength;

	vec3 verDirection = nearBottomLeft - nearTopLeft;
	float verLength = glm::length(verDirection);
	verDirection /= verLength;

	float xPercent = (float) x / screenWidth;
	float yPercent = (float) y / screenHeight;

	float horStep = horLength * xPercent;
	float verStep = verLength * yPercent;

	vec3 point = nearTopLeft + horStep * horDirection;
	point += verStep * verDirection;

	return point;

}
