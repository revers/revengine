/*
 * RevTripleCircleComponent.cpp
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#include <rev/engine/math/RevMathHelper.h>
#include <rev/engine/RevGameObject.h>
#include "RevTripleCircleComponent.h"

using namespace rev;
using namespace glm;

TripleCircleComponent::TripleCircleComponent() :
		redCircle(glm::vec3(0), 1, glm::vec3(0, 1, 0), rev::color3(1, 0, 0)), greenCircle(
				glm::vec3(0), 1, glm::vec3(0, 0, 1), rev::color3(0, 1, 0)), blueCircle(
				glm::vec3(0), 1, glm::vec3(1, 0, 0), rev::color3(0, 0, 1)) {
	parentObject = new GameObject();
	setParent(parentObject);
}

TripleCircleComponent::~TripleCircleComponent() {
	delete parentObject;
}

RenderPrimitiveType TripleCircleComponent::getRenderPrimitiveType() const {
	return redCircle.getRenderPrimitiveType();
}

void TripleCircleComponent::applyEffect(const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const ICamera& camera) {
	effect.apply(modelMatrix, viewMatrix, projectionMatrix, camera);
}

void TripleCircleComponent::render() {
	if (redOnly) {
		redCircle.render();
	} else if (greenOnly) {
		greenCircle.render();
	} else if (blueOnly) {
		blueCircle.render();
	} else {
		redCircle.render();
		greenCircle.render();
		blueCircle.render();
	}
}

const rev::BoundingSphere* TripleCircleComponent::getBoundingSphere() {
	return redCircle.getBoundingSphere();
}

int TripleCircleComponent::getPrimitiveCount() {
	return redCircle.getPrimitiveCount() + greenCircle.getPrimitiveCount()
			+ blueCircle.getPrimitiveCount();
}

glm::vec3 TripleCircleComponent::getRedNormal() {
	return MathHelper::rotate(redCircle.getNormal(), parentObject->getOrientation());
}

glm::vec3 TripleCircleComponent::getGreenNormal() {
	return MathHelper::rotate(greenCircle.getNormal(), parentObject->getOrientation());
}

glm::vec3 TripleCircleComponent::getBlueNormal() {
	return MathHelper::rotate(blueCircle.getNormal(), parentObject->getOrientation());
}

void TripleCircleComponent::setRotation(const glm::quat& rotation) {
	parentObject->setOrientation(rotation);
}

void TripleCircleComponent::setRadius(float radius) {
	parentObject->setScaling(glm::vec3(radius + 0.1));
}

void TripleCircleComponent::setPosition(const glm::vec3& position) {
	parentObject->setPosition(position);
}
