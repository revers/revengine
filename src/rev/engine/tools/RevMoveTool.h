/*
 * RevMoveTool.h
 *
 *  Created on: 14-04-2013
 *      Author: Revers
 */

#ifndef REVMOVETOOL_H_
#define REVMOVETOOL_H_

#include "RevITool.h"
#include "RevMoveLinesComponent.h"
#include <rev/engine/collision/primitives/RevPlane.h>

namespace rev {

	class GameObject;

	class MoveTool: public ITool {
		MoveLinesComponent* component = nullptr;
		GameObject* pickedGameObject = nullptr;
		bool mouseButtonPressed = false;
		bool keyButtonPressed = false;
		bool componentAdded = false;
		Plane plane = Plane(glm::vec3(0, 1, 0), glm::vec3(0, 0, 0));

	public:
		MoveTool();
		~MoveTool();

		void mousePressed(int x, int y, int contextIndex) override;
		void mouseDrag(int x, int y, int contextIndex) override;
		void mouseReleased(int x, int y, int contextIndex) override;
		void keyPressed(const KeyEvent& e) override;
		void keyReleased(const KeyEvent& e) override;
		const char* getToolName() override {
			return "MoveTool";
		}

	private:
		void setPosition(int x, int y, int contextIndex);
		GameObject* getPickedGameObject();
	};

} /* namespace rev */
#endif /* REVMOVETOOL_H_ */
