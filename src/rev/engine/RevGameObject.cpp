/*
 * RevGameObject.cpp
 *
 *  Created on: 26-11-2012
 *      Author: Revers
 */

#include <cstring>
#include <sstream>
#include <rev/common/RevDelete.hpp>
#include "RevGameObject.h"
#include <rev/common/RevAssert.h>
#include <rev/engine/components/RevIVisual3DComponent.h>
#include <rev/engine/components/RevIEventListenerComponent.h>
#include <rev/engine/components/RevScriptingComponent.h>
#include <rev/engine/components/RevIPhysicsComponent.h>
#include <rev/engine/components/RevCollisionComponent.h>
#include <rev/engine/components/light/RevILightComponent.h>
#include <rev/engine/components/RevShadowSourceComponent.h>

using namespace rev;
using namespace v8;
using namespace std;

int rev::GameObject::staticGlobalId = 0;

const glm::vec3 GameObject::FORWARD(0.0, 0.0, 1.0);

IMPLEMENT_BINDABLE_FULL(GameObject, GameObject::initScripting)

GameObject::GameObject() :
		scaling(1.0, 1.0, 1.0) {
	id = staticGlobalId++;

	std::ostringstream oss;
	oss << "obj_" << id;
	name = oss.str();
}

GameObject::GameObject(const std::string& name) :
		name(name), scaling(1.0, 1.0, 1.0) {
	id = staticGlobalId++;
}

GameObject::~GameObject() {
	DEL(visual3DComponent);
	DEL(eventListenerComponent);
	DEL(physicsComponent);
	DEL(scriptingComponent);
	destroy();
}

void GameObject::setEventListenerComponent(IEventListenerComponent* eventListenerComponent) {
	this->eventListenerComponent = eventListenerComponent;
	if (eventListenerComponent) {
		eventListenerComponent->setParent(this);
	}
}

void GameObject::setPhysicsComponent(IPhysicsComponent* physicsComponent) {
	this->physicsComponent = physicsComponent;
	if (physicsComponent) {
		physicsComponent->setParent(this);
	}
}

void GameObject::setVisual3DComponent(IVisual3DComponent* visual3DComponent) {
	this->visual3DComponent = visual3DComponent;
	if (visual3DComponent) {
		visual3DComponent->setParent(this);
	}
}

void GameObject::setScriptingComponent(ScriptingComponent* scriptingComponent) {
	this->scriptingComponent = scriptingComponent;
	if (scriptingComponent) {
		scriptingComponent->setParent(this);
	}
}

void GameObject::setCollisionComponent(CollisionComponent* collisionComponent) {
	this->collisionComponent = collisionComponent;
	if (collisionComponent) {
		collisionComponent->setParent(this);
	}
}

void GameObject::setLightComponent(ILightComponent* lightComponent) {
	this->lightComponent = lightComponent;
	if (lightComponent) {
		lightComponent->setParent(this);
	}
}

void GameObject::setShadowComponent(ShadowSourceComponent* shadowComponent) {
	this->shadowComponent = shadowComponent;
	if (shadowComponent) {
		shadowComponent->setParent(this);
	}
}

void GameObject::initScripting() {
	Handle<ObjectTemplate> proto = getFunctionTemplate()->PrototypeTemplate();
	proto->Set(String::New("rotate"), FunctionTemplate::New(jsRotate)->GetFunction());
	proto->Set(String::New("translate"), FunctionTemplate::New(jsTranslate)->GetFunction());
	// alias for translate:
	proto->Set(String::New("move"), FunctionTemplate::New(jsTranslate)->GetFunction());
	proto->Set(String::New("scale"), FunctionTemplate::New(jsScale)->GetFunction());
}

v8::Handle<v8::Value> GameObject::jsRotate(const v8::Arguments& args) {
	if (args.Length() != 3) {
		// todo: throw exception.
		return v8::Undefined();
	}
	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	GameObject* obj = static_cast<GameObject*>(wrap->Value());

	float yaw = ScriptType<float>::castFromJS(args[0]);
	float pitch = ScriptType<float>::castFromJS(args[1]);
	float roll = ScriptType<float>::castFromJS(args[2]);
	obj->rotate(yaw, pitch, roll);

	return v8::Undefined();
}

v8::Handle<v8::Value> GameObject::jsScale(const v8::Arguments& args) {
	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	GameObject* obj = static_cast<GameObject*>(wrap->Value());

	if (args.Length() == 3) {
		float x = ScriptType<float>::castFromJS(args[0]);
		float y = ScriptType<float>::castFromJS(args[1]);
		float z = ScriptType<float>::castFromJS(args[2]);
		obj->scale(x, y, z);
	} else if (args.Length() == 1) {
		float scaleFactor = ScriptType<float>::castFromJS(args[0]);
		obj->scale(scaleFactor, scaleFactor, scaleFactor);
	}

	return v8::Undefined();
}

v8::Handle<v8::Value> GameObject::jsTranslate(const v8::Arguments& args) {
	if (args.Length() != 3) {
		// todo: throw exception.
		return v8::Undefined();
	}
	Local<Object> self = args.Holder();
	Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
	GameObject* obj = static_cast<GameObject*>(wrap->Value());

	float x = ScriptType<float>::castFromJS(args[0]);
	float y = ScriptType<float>::castFromJS(args[1]);
	float z = ScriptType<float>::castFromJS(args[2]);
	obj->translate(x, y, z);

	return v8::Undefined();
}

std::string GameObject::toString() const {
	std::ostringstream oss;
	oss << *this;
	return oss.str();
}

void GameObject::bind(IBinder& binder) {
	binder.bindSimple(name);
	binder.bindSimple(visible);
	binder.bindSimple(position);
	binder.bindSimple(orientation);
	binder.bindSimple(scaling);
	binder.bindSimple(saveable);

	binder.bindSimpleObjExpanded(eventListenerComponent);
	binder.bindSimpleObjExpanded(collisionComponent);
	binder.bindSimpleObjExpanded(physicsComponent);
	binder.bindSimpleObjExpanded(visual3DComponent);
	binder.bindSimpleObjExpanded(scriptingComponent);
	binder.bindSimpleObjExpanded(lightComponent);
	binder.bindSimpleObjExpanded(shadowComponent);
}
