/*
 * RevGLContextManager.h
 *
 *  Created on: 30-01-2013
 *      Author: Revers
 */

#ifndef REVGLCONTEXTMANAGER_H_
#define REVGLCONTEXTMANAGER_H_

#include <vector>
#include <map>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/engine/config/RevEngineConfig.h>

#include <rev/common/RevAssert.h>

namespace rev {

class GLContextManager {
public:

	enum ContextIndex {
		MAIN_CONTEXT,
		SECOND_CONTEXT,
		THIRD_CONTEXT,
		FOURTH_CONTEXT,
		SIXTH_CONTEXT
	};

#ifdef REV_ENGINE_WINDOWS
	typedef HGLRC HandleType;
	struct ContextStruct {
		HandleType context = 0;
		HDC deviceContext = 0;
	};

#else
	typedef GLXContext HandleType;
	struct ContextStruct {
		HandleType context = 0;
		Display* display = 0;
		GLXDrawable drawable = 0;
	};
#endif

private:
	typedef std::map<HandleType, int> HandleToIndexMap;
	typedef std::vector<unsigned int> UintVector;

	static GLContextManager* glContextManager;

	HandleToIndexMap handleToIndexMap;
	ContextStruct handleArray[REV_ENGINE_MAX_CONTEXTS];
	bool contextUsed[REV_ENGINE_MAX_CONTEXTS];
	UintVector vaosToDestroy[REV_ENGINE_MAX_CONTEXTS];
	int currentContextIndex = 0;

	GLContextManager() {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			contextUsed[i] = false;
		}
	}

public:
	virtual ~GLContextManager() {
	}

	static void createSingleton();
	static void destroySingleton();

	static GLContextManager& ref() {
		return *glContextManager;
	}

	static GLContextManager* getInstance() {
		return glContextManager;
	}

	/**
	 * This function assumes that vaoArray.length = REV_ENGINE_MAX_CONTEXTS.
	 */
	void scheduleVAOsToDestroy(const unsigned int* vaoArray) {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			vaosToDestroy[i].push_back(vaoArray[i]);
		}
	}

	void scheduleVAOsToDestroy(const unsigned int* vaoArray, int multiplier) {
		for (int i = 0; i < REV_ENGINE_MAX_CONTEXTS; i++) {
			for (int j = 0; j < multiplier; j++) {
				vaosToDestroy[i].push_back(vaoArray[i * multiplier + j]);
			}
		}
	}

	void setCurrentContextIndex(int index) {
		revAssert(index >= 0 && index < REV_ENGINE_MAX_CONTEXTS
				&& contextUsed[index] == true);

		currentContextIndex = index;
		destroyVAOs(currentContextIndex);
	}

	/**
	 * Returns GL context index. First registered context has 0 index, second has 1, etc.
	 */
	int getCurrentContextIndex() {
		return currentContextIndex;
	}

	int getRegisteredContextsCount() {
		return handleToIndexMap.size();
	}

	void useContext(int index);

	void useMainContext();

	/**
	 * Registers current OpenGL context.
	 * We must explicitly set context index because
	 * I had problems with context initialization order in Qt
	 * (main context wasn't initialized as first).
	 *
	 * For convenience use GLContextManager::ContextIndex enum
	 * values as indices.
	 */
	void registerCurrentContext(int contextIndex);

	void unregisterContext(HandleType ctx);

	void unregisterContextIndex(int index) {
		revAssert(index >= 0 && index < handleToIndexMap.size());
		unregisterContext(handleArray[index].context);
		contextUsed[index] = false;
	}

	void setActiveContextCurrent() {
		// moze jakies assercje czy istnieje
		currentContextIndex = handleToIndexMap[getCurrentContextHandle()];
	}

	HandleType getCurrentContextHandle() {
#ifdef REV_ENGINE_WINDOWS
		return (HandleType) wglGetCurrentContext();
#else
		return (HandleType) glXGetCurrentContext();
#endif
	}

private:
	void destroyVAOs(int contextIndex);
};

} /* namespace rev */
#endif /* REVGLCONTEXTMANAGER_H_ */
