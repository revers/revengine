/*
 * RevGLContextManager.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Revers
 */

#include <iostream>
#include <windef.h>
#include <rev/gl/RevGLAssert.h>

#include <rev/common/RevErrorStream.h>
#include "RevGLContextManager.h"

using namespace rev;
using namespace std;

rev::GLContextManager* rev::GLContextManager::glContextManager = nullptr;

void GLContextManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!glContextManager);
	glContextManager = new GLContextManager();

	REV_TRACE_FUNCTION_OUT;
}

void GLContextManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(glContextManager);
	delete glContextManager;
	glContextManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void GLContextManager::useContext(int index) {
	if (currentContextIndex == index) {
		return;
	}

	revAssert(index >= 0 && index < handleToIndexMap.size());

	const ContextStruct& ctx = handleArray[index];

#ifdef REV_ENGINE_WINDOWS
	bool succ = (bool) wglMakeCurrent(ctx.deviceContext, ctx.context);
	glAssert;

	if (!succ) {
		DWORD Werr = GetLastError();
		revAssertMsg(succ, "Error code: " << Werr);
	}
#else
	handleArray[index].context = ctx;
	handleArray[index].display = glXGetCurrentDisplay();
	handleArray[index].drawable = glXGetCurrentDrawable();

	bool succ = (bool)glXMakeCurrent(ctx.display, ctx.drawable, ctx.context);
	glAssert;
	revAssert(succ);
#endif

	currentContextIndex = index;
}

void GLContextManager::useMainContext() {
	useContext(MAIN_CONTEXT);
}

void GLContextManager::registerCurrentContext(int index) {
	HandleType ctx = getCurrentContextHandle();
	revAssert(handleToIndexMap.find(ctx) == handleToIndexMap.cend());

	revAssert(index >= 0 && index < REV_ENGINE_MAX_CONTEXTS);
	revAssertMsg(contextUsed[index] == false, "Context index already used!");

	REV_INFO_MSG("Registering context " << index << " (" << ctx << ")...");

#ifdef REV_ENGINE_WINDOWS
	handleArray[index].context = ctx;
	handleArray[index].deviceContext = wglGetCurrentDC();
#else
	handleArray[index].context = ctx;
	handleArray[index].display = glXGetCurrentDisplay();
	handleArray[index].drawable = glXGetCurrentDrawable();
#endif
	handleToIndexMap[ctx] = index;
	contextUsed[index] = true;
	currentContextIndex = index;
}

void GLContextManager::unregisterContext(HandleType ctx) {
	HandleToIndexMap::const_iterator it = handleToIndexMap.find(ctx);
	revAssert(it != handleToIndexMap.cend());

	int index = it->second;
	REV_INFO_MSG(
			"Unregistering context " << index << " (" << ctx << ")...");

	handleToIndexMap.erase(it);
}

void GLContextManager::destroyVAOs(int contextIndex) {
	UintVector& v = vaosToDestroy[contextIndex];
	if (v.empty()) {
		return;
	}
	glDeleteVertexArrays(v.size(), v.data());
	v.clear();
}
