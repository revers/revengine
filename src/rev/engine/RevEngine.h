/*
 * RevEngine.h
 *
 *  Created on: 27-11-2012
 *      Author: Revers
 */

#ifndef REVENGINE_H_
#define REVENGINE_H_

#include <vector>
#include <iostream>

#include <rev/engine/binding/RevIBindable.h>
#include <rev/common/RevTimer.h>
#include <rev/common/RevAssert.h>

namespace rev {
	class GameObject;
}

namespace rev {

	typedef std::vector<GameObject*> GameObjectVect;
	typedef GameObjectVect::iterator GameObjectVectIter;

	class Engine: public IBindable {
		static Engine* engine;

		bool running = false;
		rev::Timer timer;
		double lastTimeMs = 0;
		float currentTimeMs = 0;

		GameObjectVect gameObjects;

		Engine();
		Engine(const Engine&);

	public:
		DECLARE_BINDABLE_SINGLETON(Engine)

		~Engine();

		static void createSingleton();
		static void destroySingleton();

		static Engine& ref() {
			return *engine;
		}

		static Engine* getInstance() {
			return engine;
		}

		bool isRunning() {
			return running;
		}

		void start();
		void stop();
		void update();

		/**
		 * @return current frame time (ms).
		 */
		float getCurrentFrameTime() {
			return currentTimeMs;
		}

		/**
		 * Engine is owner of added game object.
		 */
		void addGameObject(GameObject* obj);

		/**
		 * This method "silently" adds GameObject to the Engine.
		 * It won't trigger any events.
		 */
		void silentAddGameObject(GameObject* obj);

		/**
		 * This method "silently" removes GameObject from the Engine.
		 * It won't trigger any events.
		 * IMPORTANT: It won't delete the GameObject as well.
		 */
		void silentRemoveGameObject(GameObject* obj);

		GameObject* getGameObject(const char* name);
		GameObject* getGameObject(const std::string& name);
		GameObject* getGameObject(int id);

		bool destroyGameObject(int id);

		/**
		 * IMPORTANT: DO NOT MODIFY THIS LIST!!
		 */
		const GameObjectVect& getGameObjects() {
			return gameObjects;
		}

		void removeAllObjectsAndEvents();
		static void initScripting();

	private:
		void removeGameObjectComponents(GameObject* obj);
		void removeAllGameObjects();

	protected:
		void bind(IBinder& binder) override;

	private:
		static v8::Handle<v8::Value> jsGetGameObject(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsGetFrameTime(const v8::Arguments& args);
		static v8::Handle<v8::Value> jsRemoveGameObject(const v8::Arguments& args);
	};

} /* namespace rev */
#endif /* REVENGINE_H_ */
