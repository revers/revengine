/*
 * RevIntersectionTests.cpp
 *
 *  Created on: 18-05-2013
 *      Author: Revers
 */

#include <cmath>
#include "RevIntersectionTests.h"

using namespace rev;
using namespace glm;

bool IntersectionTests::sphereAndHalfSpace(const CollisionSphere& sphere,
		const CollisionPlane& plane) {
	// Find the distance from the origin
	float ballDistance = dot(plane.normal, sphere.getCenter()) - sphere.radius;

	// Check for the intersection
	return ballDistance <= plane.distance;
}

bool IntersectionTests::sphereAndSphere(const CollisionSphere& one,
		const CollisionSphere& two) {
	// Find the vector between the objects
	vec3 midline = one.getPosition() - two.getPosition();

	// See if it is large enough.
	return dot(midline, midline) < (one.radius + two.radius) * (one.radius + two.radius);
}

static inline float transformToAxis(const CollisionBox& box, const vec3& axis) {
	return box.getHalfSize().x * fabsf(dot(axis, box.getAxis(0))) +
			box.getHalfSize().y * fabsf(dot(axis, box.getAxis(1))) +
			box.getHalfSize().z * fabsf(dot(axis, box.getAxis(2)));
}

/**
 * This function checks if the two boxes overlap
 * along the given axis. The final parameter toCenter
 * is used to pass in the vector between the boxes center
 * points, to avoid having to recalculate it each time.
 */
static inline bool overlapOnAxis(
		const CollisionBox& one,
		const CollisionBox& two,
		const vec3& axis,
		const vec3& toCenter) {

	// Project the half-size of one onto axis
	float oneProject = transformToAxis(one, axis);
	float twoProject = transformToAxis(two, axis);

	// Project this onto the axis
	float distance = fabsf(dot(toCenter, axis));

	// Check for overlap
	return (distance < oneProject + twoProject);
}

// This preprocessor definition is only used as a convenience
// in the boxAndBox intersection  method.
#define TEST_OVERLAP(axis) overlapOnAxis(one, two, (axis), toCenter)

bool IntersectionTests::boxAndBox(const CollisionBox& one, const CollisionBox& two) {
	// Find the vector between the two centers
	vec3 toCenter = two.getPosition() - one.getPosition();

	return (
			// Check on box one's axes first
			TEST_OVERLAP(one.getAxis(0)) &&
			TEST_OVERLAP(one.getAxis(1)) &&
			TEST_OVERLAP(one.getAxis(2)) &&

			// And on two's
			TEST_OVERLAP(two.getAxis(0)) &&
			TEST_OVERLAP(two.getAxis(1)) &&
			TEST_OVERLAP(two.getAxis(2)) &&

			// Now on the cross products
			TEST_OVERLAP(cross(one.getAxis(0), two.getAxis(0))) &&
			TEST_OVERLAP(cross(one.getAxis(0), two.getAxis(1))) &&
			TEST_OVERLAP(cross(one.getAxis(0), two.getAxis(2))) &&
			TEST_OVERLAP(cross(one.getAxis(1), two.getAxis(0))) &&
			TEST_OVERLAP(cross(one.getAxis(1), two.getAxis(1))) &&
			TEST_OVERLAP(cross(one.getAxis(1), two.getAxis(2))) &&
			TEST_OVERLAP(cross(one.getAxis(2), two.getAxis(0))) &&
			TEST_OVERLAP(cross(one.getAxis(2), two.getAxis(1))) &&
			TEST_OVERLAP(cross(one.getAxis(2), two.getAxis(2)))
	);
}
#undef TEST_OVERLAP

bool IntersectionTests::boxAndHalfSpace(const CollisionBox& box,
		const CollisionPlane& plane) {
	// Work out the projected radius of the box onto the plane direction
	float projectedRadius = transformToAxis(box, plane.normal);

	// Work out how far the box is from the origin
	float boxDistance = dot(plane.normal, box.getPosition()) - projectedRadius;

	// Check for the intersection
	return boxDistance <= plane.distance;
}
