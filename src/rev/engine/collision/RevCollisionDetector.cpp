/*
 * RevCollisionDetector.cpp
 *
 *  Created on: 18-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#include <cmath>
#include <cfloat>
#include <rev/engine/RevGameObject.h>
#include <rev/engine/math/RevMathHelper.h>
#include "RevCollisionDetector.h"
#include "RevIntersectionTests.h"
#include "RevPotentialContact.h"

using namespace rev;
using namespace glm;

int CollisionDetector::sphereAndTruePlane(const CollisionSphere& sphere,
		const CollisionPlane& plane, CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Make sure we have contacts
	if (contactsLeft <= 0) {
		return 0;
	}

	// Cache the sphere position
	vec3 position = sphere.getPosition();

	// Find the distance from the plane
	float centerDistance = dot(plane.normal, position) - plane.distance;

	// Check if we're within radius
	if (centerDistance * centerDistance > sphere.radius * sphere.radius) {
		return 0;
	}

	// Check which side of the plane we're on
	vec3 normal = plane.normal;
	float penetration = -centerDistance;
	if (centerDistance < 0) {
		normal *= -1;
		penetration = -penetration;
	}
	penetration += sphere.radius;

	// Create the contact - it has a normal in the plane direction.
	CollisionContact* contact = contacts;
	contact->contactNormal = normal;
	contact->penetration = penetration;
	contact->contactPoint = position - plane.normal * centerDistance;

	// FIXME: tutaj byl nullptr. czy dobrze robie przekazujac Plane?
	// contact->setBodyData(firstBody, nullptr);
	contact->setBodyData(firstBody, secondBody);

	addContacts(1);
	return 1;
}

int CollisionDetector::sphereAndHalfSpace(
		const CollisionSphere& sphere,
		const CollisionPlane& plane,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Make sure we have contacts
	if (contactsLeft <= 0) {
		return 0;
	}

	// Cache the sphere position
	vec3 position = sphere.getPosition();

	// Find the distance from the plane
	float ballDistance = dot(plane.normal, position) -
			sphere.radius - plane.distance;

	if (ballDistance >= 0) {
		return 0;
	}

	// Create the contact - it has a normal in the plane direction.
	CollisionContact* contact = contacts;
	contact->contactNormal = plane.normal;
	contact->penetration = -ballDistance;
	contact->contactPoint =
			position - plane.normal * (ballDistance + sphere.radius);

	// FIXME: ???
	//	contact->setBodyData(sphere.body, nullptr,
	//			friction, restitution);
	contact->setBodyData(firstBody, secondBody);

	addContacts(1);
	return 1;
}

int CollisionDetector::sphereAndSphere(
		const CollisionSphere& one,
		const CollisionSphere& two,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Make sure we have contacts
	if (contactsLeft <= 0) {
		return 0;
	}

	// Cache the sphere positions
	vec3 positionOne = one.getPosition();
	vec3 positionTwo = two.getPosition();

	// Find the vector between the objects
	vec3 midline = positionOne - positionTwo;
	float size = length(midline);

	// See if it is large enough.
	if (size <= 0.0f || size >= one.radius + two.radius) {
		return 0;
	}

	// We manually create the normal, because we have the
	// size to hand.
	vec3 normal = midline * (float(1.0) / size);

	CollisionContact* contact = contacts;
	contact->contactNormal = normal;
	contact->contactPoint = positionOne + midline * float(0.5);
	contact->penetration = (one.radius + two.radius - size);
	contact->setBodyData(firstBody, secondBody);

	addContacts(1);
	return 1;
}

static inline float transformToAxis(const CollisionBox& box, const vec3& axis) {
	return box.getHalfSize().x * fabsf(dot(axis, box.getAxis(0))) +
			box.getHalfSize().y * fabsf(dot(axis, box.getAxis(1))) +
			box.getHalfSize().z * fabsf(dot(axis, box.getAxis(2)));
}

/*
 * This function checks if the two boxes overlap
 * along the given axis, returning the ammount of overlap.
 * The final parameter toCenter
 * is used to pass in the vector between the boxes center
 * points, to avoid having to recalculate it each time.
 */
static inline float penetrationOnAxis(
		const CollisionBox& one,
		const CollisionBox& two,
		const vec3& axis,
		const vec3& toCenter) {

	// Project the half-size of one onto axis
	float oneProject = transformToAxis(one, axis);
	float twoProject = transformToAxis(two, axis);

	// Project this onto the axis
	float distance = fabsf(dot(toCenter, axis));

	// Return the overlap (i.e. positive indicates
	// overlap, negative indicates separation).
	return oneProject + twoProject - distance;
}

static inline bool tryAxis(
		const CollisionBox& one,
		const CollisionBox& two,
		const vec3& axis,
		const vec3& toCenter,
		int index,
		// These values may be updated
		float& smallestPenetration,
		int& smallestCase) {

	vec3 axisLocal(axis);
	// Make sure we have a normalized axis, and don't check almost parallel axes
	if (dot(axisLocal, axisLocal) < 0.0001) {
		return true;
	}
	axisLocal = normalize(axisLocal);

	float penetration = penetrationOnAxis(one, two, axisLocal, toCenter);

	if (penetration < 0) {
		return false;
	}
	if (penetration < smallestPenetration) {
		smallestPenetration = penetration;
		smallestCase = index;
	}
	return true;
}

void CollisionDetector::fillPointFaceBoxBox(
		const CollisionBox& one,
		const CollisionBox& two,
		const vec3& toCenter,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody,
		int best,
		float pen) {

	// This method is called when we know that a vertex from
	// box two is in contact with box one.

	CollisionContact* contact = contacts;

	// We know which axis the collision is on (i.e. best),
	// but we need to work out which of the two faces on
	// this axis.
	vec3 normal = one.getAxis(best);
	if (dot(one.getAxis(best), toCenter) > 0) {
		normal = normal * -1.0f;
	}

	// Work out which vertex of box two we're colliding with.
	// Using toCenter doesn't work!
	vec3 vertex = two.halfSize;
	if (dot(two.getAxis(0), normal) < 0) {
		vertex.x = -vertex.x;
	}
	if (dot(two.getAxis(1), normal) < 0) {
		vertex.y = -vertex.y;
	}
	if (dot(two.getAxis(2), normal) < 0) {
		vertex.z = -vertex.z;
	}

	// Create the contact data
	contact->contactNormal = normal;
	contact->penetration = pen;
	contact->contactPoint = mul(two.getTransform(), vertex);
	contact->setBodyData(firstBody, secondBody);
}

static inline vec3 contactPoint(
		const vec3& pOne,
		const vec3& dOne,
		float oneSize,
		const vec3& pTwo,
		const vec3& dTwo,
		float twoSize,
		// If this is true, and the contact point is outside
		// the edge (in the case of an edge-face contact) then
		// we use one's midpoint, otherwise we use two's.
		bool useOne) {

	float smOne = dot(dOne, dOne);
	float smTwo = dot(dTwo, dTwo);
	float dpOneTwo = dot(dTwo, dOne);

	vec3 toSt = pOne - pTwo;
	float dpStaOne = dot(dOne, toSt);
	float dpStaTwo = dot(dTwo, toSt);

	float denom = smOne * smTwo - dpOneTwo * dpOneTwo;

	// Zero denominator indicates parrallel lines
	if (fabsf(denom) < 0.0001f) {
		return useOne ? pOne : pTwo;
	}

	float mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
	float mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

	// If either of the edges has the nearest point out
	// of bounds, then the edges aren't crossed, we have
	// an edge-face contact. Our point is on the edge, which
	// we know from the useOne parameter.
	if (mua > oneSize ||
			mua < -oneSize ||
			mub > twoSize ||
			mub < -twoSize) {
		return useOne ? pOne : pTwo;
	} else {
		vec3 cOne = pOne + dOne * mua;
		vec3 cTwo = pTwo + dTwo * mub;

		return cOne * float(0.5) + cTwo * float(0.5);
	}
}

// This preprocessor definition is only used as a convenience
// in the boxAndBox contact generation method.
#define CHECK_OVERLAP(axis, index) \
    if (!tryAxis(one, two, (axis), toCenter, (index), pen, best)) return 0;

int CollisionDetector::boxAndBox(
		const CollisionBox& one,
		const CollisionBox& two,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	//if (!IntersectionTests::boxAndBox(one, two)) return 0;

	// Find the vector between the two centers
	vec3 toCenter = two.getPosition() - one.getPosition();

	// We start assuming there is no contact
	float pen = FLT_MAX;
	int best = 0xffffff;

	// Now we check each axes, returning if it gives us
	// a separating axis, and keeping track of the axis with
	// the smallest penetration otherwise.
	CHECK_OVERLAP(one.getAxis(0), 0);
	CHECK_OVERLAP(one.getAxis(1), 1);
	CHECK_OVERLAP(one.getAxis(2), 2);

	CHECK_OVERLAP(two.getAxis(0), 3);
	CHECK_OVERLAP(two.getAxis(1), 4);
	CHECK_OVERLAP(two.getAxis(2), 5);

	// Store the best axis-major, in case we run into almost
	// parallel edge collisions later
	int bestSingleAxis = best;

	CHECK_OVERLAP(cross(one.getAxis(0), two.getAxis(0)), 6);
	CHECK_OVERLAP(cross(one.getAxis(0), two.getAxis(1)), 7);
	CHECK_OVERLAP(cross(one.getAxis(0), two.getAxis(2)), 8);
	CHECK_OVERLAP(cross(one.getAxis(1), two.getAxis(0)), 9);
	CHECK_OVERLAP(cross(one.getAxis(1), two.getAxis(1)), 10);
	CHECK_OVERLAP(cross(one.getAxis(1), two.getAxis(2)), 11);
	CHECK_OVERLAP(cross(one.getAxis(2), two.getAxis(0)), 12);
	CHECK_OVERLAP(cross(one.getAxis(2), two.getAxis(1)), 13);
	CHECK_OVERLAP(cross(one.getAxis(2), two.getAxis(2)), 14);

	// Make sure we've got a result.
	assert(best != 0xffffff);

	// We now know there's a collision, and we know which
	// of the axes gave the smallest penetration. We now
	// can deal with it in different ways depending on
	// the case.
	if (best < 3) {
		// We've got a vertex of box two on a face of box one.
		fillPointFaceBoxBox(one, two, toCenter, firstBody, secondBody, best, pen);
		addContacts(1);
		return 1;
	} else if (best < 6) {
		// We've got a vertex of box one on a face of box two.
		// We use the same algorithm as above, but swap around
		// one and two (and therefore also the vector between their
		// centers).
		fillPointFaceBoxBox(two, one, toCenter * -1.0f, firstBody, secondBody,
				best - 3, pen);
		addContacts(1);
		return 1;
	} else {
		// We've got an edge-edge contact. Find out which axes
		best -= 6;
		int oneAxisIndex = best / 3;
		int twoAxisIndex = best % 3;
		vec3 oneAxis = one.getAxis(oneAxisIndex);
		vec3 twoAxis = two.getAxis(twoAxisIndex);
		vec3 axis = cross(oneAxis, twoAxis);
		axis = normalize(axis);

		// The axis should point from box one to box two.
		if (dot(axis, toCenter) > 0) {
			axis = axis * -1.0f;
		}

		// We have the axes, but not the edges: each axis has 4 edges parallel
		// to it, we need to find which of the 4 for each object. We do
		// that by finding the point in the center of the edge. We know
		// its component in the direction of the box's collision axis is zero
		// (its a mid-point) and we determine which of the extremes in each
		// of the other axes is closest.
		vec3 ptOnOneEdge = one.halfSize;
		vec3 ptOnTwoEdge = two.halfSize;
		for (int i = 0; i < 3; i++) {
			if (i == oneAxisIndex) {
				ptOnOneEdge[i] = 0;
			} else if (dot(one.getAxis(i), axis) > 0) {
				ptOnOneEdge[i] = -ptOnOneEdge[i];
			}

			if (i == twoAxisIndex) {
				ptOnTwoEdge[i] = 0;
			} else if (dot(two.getAxis(i), axis) < 0) {
				ptOnTwoEdge[i] = -ptOnTwoEdge[i];
			}
		}

		// Move them into world coordinates (they are already oriented
		// correctly, since they have been derived from the axes).
//		ptOnOneEdge = one.transform * ptOnOneEdge;
//		ptOnTwoEdge = two.transform * ptOnTwoEdge;
		ptOnOneEdge = mul(one.transform, ptOnOneEdge);
		ptOnTwoEdge = mul(two.transform, ptOnTwoEdge);

		// So we have a point and a direction for the colliding edges.
		// We need to find out point of closest approach of the two
		// line-segments.
		vec3 vertex = contactPoint(
				ptOnOneEdge, oneAxis, one.halfSize[oneAxisIndex],
				ptOnTwoEdge, twoAxis, two.halfSize[twoAxisIndex],
				bestSingleAxis > 2);

		// We can fill the contact.
		CollisionContact* contact = contacts;

		contact->penetration = pen;
		contact->contactNormal = axis;
		contact->contactPoint = vertex;
		contact->setBodyData(firstBody, secondBody);
		addContacts(1);
		return 1;
	}
	return 0;
}
#undef CHECK_OVERLAP

int CollisionDetector::boxAndPoint(
		const CollisionBox& box,
		const vec3& point,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Transform the point into box coordinates
	vec3 relPt = MathHelper::transformInverse(box.transform, point);
	// = box.transform.transformInverse(point);

	vec3 normal;

	// Check each axis, looking for the axis on which the
	// penetration is least deep.
	float min_depth = box.halfSize.x - fabsf(relPt.x);
	if (min_depth < 0) {
		return 0;
	}
	normal = box.getAxis(0) * float((relPt.x < 0) ? -1 : 1);

	float depth = box.halfSize.y - fabsf(relPt.y);
	if (depth < 0) {
		return 0;
	} else if (depth < min_depth) {
		min_depth = depth;
		normal = box.getAxis(1) * float((relPt.y < 0) ? -1 : 1);
	}

	depth = box.halfSize.z - fabsf(relPt.z);
	if (depth < 0) {
		return 0;
	} else if (depth < min_depth) {
		min_depth = depth;
		normal = box.getAxis(2) * float((relPt.z < 0) ? -1 : 1);
	}

	// Compile the contact
	CollisionContact* contact = contacts;
	contact->contactNormal = normal;
	contact->contactPoint = point;
	contact->penetration = min_depth;

	// Note that we don't know what rigid body the point
	// belongs to, so we just use nullptr. Where this is called
	// this value can be left, or filled in.
	// FIXME: ???
//	contact->setBodyData(box.body, nullptr,
//			friction, restitution);
	contact->setBodyData(firstBody, secondBody);

	addContacts(1);
	return 1;
}

int CollisionDetector::boxAndSphere(
		const CollisionBox& box,
		const CollisionSphere& sphere,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Transform the center of the sphere into box coordinates
	vec3 center = sphere.getPosition();
	vec3 relCenter = MathHelper::transformInverse(box.transform, center);
	//= box.transform.transformInverse(center);

	// Early out check to see if we can exclude the contact
	if (fabsf(relCenter.x) - sphere.radius > box.halfSize.x ||
			fabsf(relCenter.y) - sphere.radius > box.halfSize.y ||
			fabsf(relCenter.z) - sphere.radius > box.halfSize.z) {
		return 0;
	}

	vec3 closestPt(0, 0, 0);
	float dist;

	// Clamp each coordinate to the box.
	dist = relCenter.x;
	if (dist > box.halfSize.x) {
		dist = box.halfSize.x;
	}
	if (dist < -box.halfSize.x) {
		dist = -box.halfSize.x;
	}
	closestPt.x = dist;

	dist = relCenter.y;
	if (dist > box.halfSize.y) {
		dist = box.halfSize.y;
	}
	if (dist < -box.halfSize.y) {
		dist = -box.halfSize.y;
	}
	closestPt.y = dist;

	dist = relCenter.z;
	if (dist > box.halfSize.z) {
		dist = box.halfSize.z;
	}
	if (dist < -box.halfSize.z) {
		dist = -box.halfSize.z;
	}
	closestPt.z = dist;

	// Check we're in contact
	vec3 v = closestPt - relCenter;
	dist = dot(v, v);
	if (dist > sphere.radius * sphere.radius) {
		return 0;
	}

	// Compile the contact
	vec3 closestPtWorld = mul(box.transform, closestPt);
	//box.transform.transform(closestPt);

	CollisionContact* contact = contacts;
	contact->contactNormal = (closestPtWorld - center);
	contact->contactNormal = normalize(contact->contactNormal);
	contact->contactPoint = closestPtWorld;
	contact->penetration = sphere.radius - sqrtf(dist);
	contact->setBodyData(firstBody, secondBody);

	addContacts(1);
	return 1;
}

int CollisionDetector::boxAndHalfSpace(
		const CollisionBox& box,
		const CollisionPlane& plane,
		CollisionComponent* firstBody,
		CollisionComponent* secondBody) {

	// Make sure we have contacts
	if (contactsLeft <= 0) {
		return 0;
	}

	// Check for intersection
	if (!IntersectionTests::boxAndHalfSpace(box, plane)) {
		return 0;
	}

	// We have an intersection, so find the intersection points. We can make
	// do with only checking vertices. If the box is resting on a plane
	// or on an edge, it will be reported as four or two contact points.

	// Go through each combination of + and - for each half-size
	static float mults[8][3] = { { 1, 1, 1 }, { -1, 1, 1 }, { 1, -1, 1 }, { -1, -1, 1 },
			{ 1, 1, -1 }, { -1, 1, -1 }, { 1, -1, -1 }, { -1, -1, -1 } };

	CollisionContact* contact = contacts;
	int contactsUsed = 0;
	for (int i = 0; i < 8; i++) {

		// Calculate the position of each vertex
		vec3 vertexPos(mults[i][0], mults[i][1], mults[i][2]);
		//vertexPos.componentProductUpdate(box.halfSize);
		vertexPos.x *= box.halfSize.x;
		vertexPos.y *= box.halfSize.y;
		vertexPos.z *= box.halfSize.z;
		vertexPos = mul(box.transform, vertexPos);
		//box.transform.transform(vertexPos);

		// Calculate the distance from the plane
		float vertexDistance = dot(vertexPos, plane.normal);

		// Compare this to the plane's distance
		if (vertexDistance <= plane.distance) {
			// Create the contact data.

			// The contact point is halfway between the vertex and the
			// plane - we multiply the direction by half the separation
			// distance and add the vertex location.
			contact->contactPoint = plane.normal;
			contact->contactPoint *= (vertexDistance - plane.distance);
			contact->contactPoint = vertexPos;
			contact->contactNormal = plane.normal;
			contact->penetration = plane.distance - vertexDistance;

			// Write the appropriate data
			// FIXME: ??
//			contact->setBodyData(box.body, nullptr,
//					friction, restitution);
			contact->setBodyData(firstBody, secondBody);

			// Move onto the next contact
			contact++;
			contactsUsed++;
			if (contactsUsed == contactsLeft) {
				return contactsUsed;
			}
		}
	}

	addContacts(contactsUsed);
	return contactsUsed;
}

void CollisionDetector::generateContacts(PotentialContact* potentialContacts, int size) {
	reset(REV_MAX_COLLISION_CONTACS);

	PotentialContact* potentialContact = potentialContacts;
	for (int i = 0; i < size; i++) {

		CollisionGeometry* firstGeometry = potentialContact->first->getGeometry();
		CollisionGeometry* secondGeometry = potentialContact->second->getGeometry();
		if (!firstGeometry || !secondGeometry) {
			continue;
		}

		GameObject* firstParent = potentialContact->first->getParent();
		GameObject* secondParent = potentialContact->second->getParent();

		firstGeometry->calculateInternals(firstParent->getModelMatrix());
		secondGeometry->calculateInternals(secondParent->getModelMatrix());

		unsigned type = unsigned(firstGeometry->getType()) | unsigned(secondGeometry->getType());
		switch (type) {
		case MASK_SPHERE_SPHERE: {
			sphereAndSphere(*static_cast<CollisionSphere*>(firstGeometry),
					*static_cast<CollisionSphere*>(secondGeometry),
					potentialContact->first,
					potentialContact->second);
			break;
		}
		case MASK_BOX_BOX: {
			boxAndBox(*static_cast<CollisionBox*>(firstGeometry),
					*static_cast<CollisionBox*>(secondGeometry),
					potentialContact->first,
					potentialContact->second);
			break;
		}
		case MASK_PLANE_PLANE: {
			revAssertTODO("TODO: Plane-Plane collision.");
			break;
		}
		case MASK_SPHERE_BOX: {
			if (firstGeometry->getType() == GeometryType::BOX) {
				boxAndSphere(*static_cast<CollisionBox*>(firstGeometry),
						*static_cast<CollisionSphere*>(secondGeometry),
						potentialContact->first,
						potentialContact->second);
			} else {
				boxAndSphere(*static_cast<CollisionBox*>(secondGeometry),
						*static_cast<CollisionSphere*>(firstGeometry),
						potentialContact->second,
						potentialContact->first);
			}
			break;
		}
		case MASK_SPHERE_PLANE: {
			if (firstGeometry->getType() == GeometryType::SPHERE) {
				sphereAndHalfSpace(*static_cast<CollisionSphere*>(firstGeometry),
						*static_cast<CollisionPlane*>(secondGeometry),
						potentialContact->first,
						potentialContact->second);
			} else {
				sphereAndHalfSpace(*static_cast<CollisionSphere*>(secondGeometry),
						*static_cast<CollisionPlane*>(firstGeometry),
						potentialContact->second,
						potentialContact->first);
			}
			break;
		}
		case MASK_BOX_PLANE: {
			if (firstGeometry->getType() == GeometryType::BOX) {
				boxAndHalfSpace(*static_cast<CollisionBox*>(firstGeometry),
						*static_cast<CollisionPlane*>(secondGeometry),
						potentialContact->first,
						potentialContact->second);
			} else {
				boxAndHalfSpace(*static_cast<CollisionBox*>(secondGeometry),
						*static_cast<CollisionPlane*>(firstGeometry),
						potentialContact->second,
						potentialContact->first);
			}
			break;
		}
		default: {
			revAssert(false);
			break;
		}
		}

		potentialContact++;
	}
}
