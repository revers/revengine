/*
 * RevQuadTree.h
 *
 *  Created on: 13-05-2013
 *      Author: Revers
 */

#ifndef REVQUADTREE_H_
#define REVQUADTREE_H_

#define REV_QUAD_TREE_USE_VECTOR

#include <vector>

#include <glm/glm.hpp>
#include "RevPotentialContact.h"

namespace rev {

	class CollisionComponent;
	typedef std::vector<CollisionComponent*> CollisionCompVector;

	struct QuadTreeNode {
		QuadTreeNode* parent;

		/**
		 * CollisionComponent* count in this node and in its sub-nodes.
		 */
		int size = 0;
		union {
			QuadTreeNode* child[4];
			struct {
				QuadTreeNode* NE;
				QuadTreeNode* NW;
				QuadTreeNode* SE;
				QuadTreeNode* SW;
			};
		};

		glm::vec2 position;
		CollisionCompVector compVector;

		QuadTreeNode(const glm::vec2& position = glm::vec2(0), QuadTreeNode* parent = nullptr);
		~QuadTreeNode();

		/**
		 *                  |
		 *    child[0] (NE) | child[1] (NW)
		 *   ---------------+----------------> X
		 *    child[2] (SE) | child[3] (SW)
		 *                  |
		 *                  v
		 *                  Z
		 */
		int getChildIndex(const glm::vec2& object) const {
			int index = 0;
			if (object.x > position.x) {
				index += 1;
			}
			if (object.y > position.y) {
				index += 2;
			}
			return index;
		}

		bool isLeaf() const {
			// if any child is nullptr:
			return child[0] == nullptr;
		}

		void clear() {
			size = 0;
			compVector.clear();
			if (isLeaf()) {
				return;
			}
			NE->clear();
			NW->clear();
			SE->clear();
			SW->clear();
		}
	};

	class QuadTree {
		static const int COLLISION_BUFFER_SIZE = 1024;
		CollisionComponent* collisionBuffer[COLLISION_BUFFER_SIZE];

		QuadTreeNode root;

	public:
		glm::vec2 size;
		glm::vec2 center;
		int treeDepth;
		int leavesCount;

	public:
		QuadTree(const glm::vec2& size, const glm::vec2& center = glm::vec2(0),
				int treeDepth = 2);
		~QuadTree();

		void insert(CollisionComponent* cc) {
			insert(&root, cc);
		}

		int getPotentialContacts(PotentialContact* contacts, int limit) const;

		QuadTreeNode* getRoot() {
			return &root;
		}

		void clear();

	private:
		void create(QuadTreeNode* node, int depth, const glm::vec2& size, const glm::vec2& center);
		void insert(QuadTreeNode* node, CollisionComponent* cc);
		int getPotentialContacts(const QuadTreeNode* node, PotentialContact* contacts,
				int limit, int bufferOffset) const;
	};

} /* namespace rev */
#endif /* REVQUADTREE_H_ */
