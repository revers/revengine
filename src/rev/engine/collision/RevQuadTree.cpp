/*
 * RevQuadTree.cpp
 *
 *  Created on: 13-05-2013
 *      Author: Revers
 */

#include <cmath>
#include <cstring>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/engine/util/RevGLMOstream.h>
#include "RevQuadTree.h"

using namespace glm;
using namespace rev;

QuadTreeNode::QuadTreeNode(const vec2& position, QuadTreeNode* parent) :
		position(position), parent(parent) {
	child[0] = nullptr;
	child[1] = nullptr;
	child[2] = nullptr;
	child[3] = nullptr;
}

QuadTreeNode::~QuadTreeNode() {
	if (!isLeaf()) {
		delete child[0];
		delete child[1];
		delete child[2];
		delete child[3];
	}
}

QuadTree::QuadTree(const vec2& size, const vec2& center, int treeDepth) :
		size(size), center(center), treeDepth(treeDepth) {
	leavesCount = (int) pow(4.0, treeDepth);

	create(&root, 0, size, center);
}

QuadTree::~QuadTree() {
}

void QuadTree::create(QuadTreeNode* node, int depth, const vec2& size,
		const vec2& center) {
	node->position = center;

	if (depth < treeDepth) {
		float xStep = size.x / 4.0f;
		float yStep = size.y / 4.0f;
		float xMin = center.x - 2.0f * xStep;
		float yMin = center.y - 2.0f * yStep;

		vec2 child0Center(xMin + xStep, yMin + yStep);
		vec2 child1Center(xMin + 3.0f * xStep, yMin + yStep);
		vec2 child2Center(xMin + xStep, yMin + 3.0f * yStep);
		vec2 child3Center(xMin + 3.0f * xStep, yMin + 3.0f * yStep);

		node->child[0] = new QuadTreeNode(child0Center, node);
		node->child[1] = new QuadTreeNode(child1Center, node);
		node->child[2] = new QuadTreeNode(child2Center, node);
		node->child[3] = new QuadTreeNode(child3Center, node);

		vec2 newSize = size / 2.0f;
		int newDepth = depth + 1;
		create(node->child[0], newDepth, newSize, child0Center);
		create(node->child[1], newDepth, newSize, child1Center);
		create(node->child[2], newDepth, newSize, child2Center);
		create(node->child[3], newDepth, newSize, child3Center);
	}
}

void QuadTree::insert(QuadTreeNode* node, CollisionComponent* cc) {
	node->size++;
	if (node->isLeaf()) {
		node->compVector.push_back(cc);
		return;
	}
	BoundingSphere sphere = cc->getWorldBoundingSphere();

	// points of square:
	vec2 ne(sphere.c.x - sphere.r, sphere.c.z + sphere.r);
	vec2 nw(sphere.c.x + sphere.r, sphere.c.z + sphere.r);
	vec2 se(sphere.c.x - sphere.r, sphere.c.z - sphere.r);
	vec2 sw(sphere.c.x + sphere.r, sphere.c.z - sphere.r);

	unsigned index = (unsigned) (node->getChildIndex(ne));
	unsigned mask1 = 1 << index;
	unsigned mask2 = 1 << (unsigned) (node->getChildIndex(nw));
	unsigned mask3 = 1 << (unsigned) (node->getChildIndex(se));
	unsigned mask4 = 1 << (unsigned) (node->getChildIndex(sw));
	unsigned mask = mask1 | mask2 | mask3 | mask4;

	if (mask != mask1) {
		// Doesen't fit whole in any children.
		node->compVector.push_back(cc);
	} else {
		// Insert to the child where it fits whole.
		insert(node->child[index], cc);
	}
}

void QuadTree::clear() {
	root.clear();
}

int QuadTree::getPotentialContacts(const QuadTreeNode* node, PotentialContact* contacts,
		int limit, int bufferOffset) const {
	if (node->size == 0 || limit == 0 || bufferOffset >= COLLISION_BUFFER_SIZE) {
		revAssert(limit != 0);
		revAssert(bufferOffset < COLLISION_BUFFER_SIZE);
		return 0;
	}

	if (node->compVector.size() > 0) {
		CollisionComponent* const * data = node->compVector.data();
		int dataSize = node->compVector.size();
		memcpy((void*) (collisionBuffer + bufferOffset), (const void*) data,
				sizeof(CollisionComponent*) * dataSize);
		bufferOffset += dataSize;
	}

	if (node->compVector.size() == node->size) {
		if (bufferOffset <= 1) {
			return 0;
		}

		int count = 0;
		for (int i = 0; i < bufferOffset; i++) {
			CollisionComponent* ccA = const_cast<CollisionComponent*>(collisionBuffer[i]);
			BoundingSphere bsA = ccA->getWorldBoundingSphere();

			for (int j = i + 1; j < bufferOffset; j++) {
				CollisionComponent* ccB = const_cast<CollisionComponent*>(collisionBuffer[j]);
				BoundingSphere bsB = ccB->getWorldBoundingSphere();

				if (bsA.overlaps(&bsB)) {
					PotentialContact* contact = contacts + count;
					contact->first = ccA;
					contact->second = ccB;
					count++;
					limit--;
				}
				if (limit == 0) {
					revAssert(limit != 0);
					return count;
				}
			}
		}
		return count;
	} else {
		int count = getPotentialContacts(node->NE, contacts, limit, bufferOffset);
		count += getPotentialContacts(node->NW, contacts + count, limit - count, bufferOffset);
		count += getPotentialContacts(node->SE, contacts + count, limit - count, bufferOffset);
		count += getPotentialContacts(node->SW, contacts + count, limit - count, bufferOffset);

		return count;
	}
}

int QuadTree::getPotentialContacts(PotentialContact* contacts, int limit) const {
	return getPotentialContacts(&root, contacts, limit, 0);
}
