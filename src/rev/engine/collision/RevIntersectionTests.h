/*
 * RevIntersectionTests.h
 *
 *  Created on: 18-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVINTERSECTIONTESTS_H_
#define REVINTERSECTIONTESTS_H_

#include "RevCollisionSphere.h"
#include "RevCollisionBox.h"
#include "RevCollisionPlane.h"

namespace rev {

	class IntersectionTests {
		IntersectionTests() = delete;

	public:
		static bool sphereAndHalfSpace(
				const CollisionSphere& sphere,
				const CollisionPlane& plane);

		static bool sphereAndSphere(
				const CollisionSphere& one,
				const CollisionSphere& two);

		static bool boxAndBox(
				const CollisionBox& one,
				const CollisionBox& two);

		static bool boxAndHalfSpace(
				const CollisionBox& box,
				const CollisionPlane& plane);

	};

} /* namespace rev */
#endif /* REVINTERSECTIONTESTS_H_ */
