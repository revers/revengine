/*
 * RevCollisionManager.cpp
 *
 *  Created on: 09-05-2013
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/engine/RevEngine.h>
#include <rev/engine/physics/RevPhysicsManager.h>
#include "RevCollisionManager.h"
#include "RevBoundingSphereHierarchy.h"

using namespace rev;

rev::CollisionManager* rev::CollisionManager::collisionManager = nullptr;

IMPLEMENT_BINDABLE_SINGLETON(CollisionManager)

CollisionManager::CollisionManager() {
	quadTree = new QuadTree(glm::vec2(300, 300), glm::vec2(0), 3);

	/* Debug */
	ptrQuadTreeSize = &quadTree->size;
	ptrQuadTreeCenter = &quadTree->center;
	ptrQuadTreeDepth = &quadTree->treeDepth;
	ptrQuadTreeLeavesCount = &quadTree->leavesCount;
}

CollisionManager::~CollisionManager() {
	delete quadTree;
}

void CollisionManager::createSingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(!collisionManager);
	collisionManager = new CollisionManager();
	REV_TRACE_FUNCTION_OUT;
}

void CollisionManager::destroySingleton() {
	REV_TRACE_FUNCTION_IN;
	revAssert(collisionManager);

	delete collisionManager;
	collisionManager = nullptr;
	REV_TRACE_FUNCTION_OUT;
}

void CollisionManager::recreateQuadTree() {
	quadTree->clear();
	for (auto& c : components) {
		quadTree->insert(c);
	}
}

void CollisionManager::update() {
	if (paused) {
		return;
	}
	recreateQuadTree();
	potentialContactsSize = quadTree->getPotentialContacts(potentialContacts,
			REV_MAX_POTENTIAL_CONTACTS);

	collisionDetector.generateContacts(potentialContacts, potentialContactsSize);
	contacts = collisionDetector.getContactsCount();

	PhysicsManager::ref().resolveContacts(collisionDetector.getContacts(),
			collisionDetector.getContactsCount());
}

void CollisionManager::bind(IBinder& binder) {
	binder.bindSimple(paused);
	binder.bindInfo(this, "Potential contacts", potentialContactsSize);
	binder.bindInfo(this, "Contacts", contacts);
	binder.bind(this, "QT Size", MEMBER_PTR_WRAPPER(ptrQuadTreeSize), false, true, false);
	binder.bind(this, "QT Center", MEMBER_PTR_WRAPPER(ptrQuadTreeCenter), false, true, false);
	binder.bind(this, "QT Depth", MEMBER_PTR_WRAPPER(ptrQuadTreeDepth), false, true, false);
	binder.bind(this, "QT Leaves", MEMBER_PTR_WRAPPER(ptrQuadTreeLeavesCount), false, true, false);
}
