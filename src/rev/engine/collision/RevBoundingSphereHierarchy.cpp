/*
 * RevBoundingSphereHierarchy.cpp
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#include "RevBoundingSphereHierarchy.h"
//#include "RevCollisionManager.h"

using namespace rev;

void BSHNode::insert(CollisionComponent* newBody) {
	const BoundingSphere& newVolume = newBody->getBoundingSphere();
	// If we are a leaf, then the only option is to spawn two
	// new children and place the new body in one.
	if (isLeaf()) {
		// Child one is a copy of us.
		firstChild = new BSHNode(this, volume, body);

		// Child two holds the new body
		secondChild = new BSHNode(this, newVolume, newBody);

		// And we now loose the body (we're no longer a leaf)
		this->body = nullptr;

		// We need to recalculate our bounding volume
		recalculateBoundingVolume();
	} else if (!firstChild /* && !secondChild */) {
		// If we have no body (we are not a leaf) and no children:
		this->volume = newVolume;
		this->body = newBody;
	} else {
		// Otherwise we need to work out which child gets to keep
		// the inserted body. We give it to whoever would grow the
		// least to incorporate it.
		if (firstChild->volume.getGrowth(newVolume) <
				secondChild->volume.getGrowth(newVolume)) {
			firstChild->insert(newBody);
		} else {
			secondChild->insert(newBody);
		}
	}
}

BSHNode::~BSHNode() {
	// If we don't have a parent, then we ignore the sibling
	// processing
	if (parent) {
		// Find our sibling
		BSHNode *sibling;
		if (parent->firstChild == this) {
			sibling = parent->secondChild;
		} else {
			sibling = parent->firstChild;
		}

		// Write its data to our parent
		parent->volume = sibling->volume;
		parent->body = sibling->body;
		parent->firstChild = sibling->firstChild;
		parent->secondChild = sibling->secondChild;

		// Delete the sibling (we blank its parent and
		// children to avoid processing/deleting them)
		sibling->parent = nullptr;
		sibling->body = nullptr;
		sibling->firstChild = nullptr;
		sibling->secondChild = nullptr;
		delete sibling;

		// Recalculate the parent's bounding volume
		parent->recalculateBoundingVolume();
	}

	// Delete our children (again we remove their
	// parent data so we don't try to process their siblings
	// as they are deleted).
	if (firstChild) {
		firstChild->parent = nullptr;
		delete firstChild;
	}
	if (secondChild) {
		secondChild->parent = nullptr;
		delete secondChild;
	}
}

void BSHNode::recalculateBoundingVolume(bool recurse) {
	if (isLeaf()) {
		return;
	}

	// Use the bounding volume combining constructor.
	volume = BoundingSphere(firstChild->volume, secondChild->volume);

	// Recurse up the tree
	if (parent) {
		parent->recalculateBoundingVolume(true);
	}
}

int BSHNode::getPotentialContacts(PotentialContact* contacts, int limit) const {
	// Early out if we don't have the room for contacts, or
	// if we're a leaf node.
	if (isLeaf() || limit == 0 || firstChild == nullptr) {
		return 0;
	}

	// Get the potential contacts of one of our children with
	// the other
	return firstChild->getPotentialContactsWith(secondChild, contacts, limit);
}

int BSHNode::getPotentialContactsWith(
		const BSHNode* other,
		PotentialContact* contacts,
		int limit) const {
	// Early out if we don't overlap or if we have no room
	// to report contacts
	if (!overlaps(other) || limit == 0) {
		return 0;
	}

	// If we're both at leaf nodes, then we have a potential contact
	if (isLeaf() && other->isLeaf()) {
		contacts->first = body;
		contacts->second = other->body;
		return 1;
	}

	// Determine which node to descend into. If either is
	// a leaf, then we descend the other. If both are branches,
	// then we use the one with the largest size.
	if (other->isLeaf() ||
			(!isLeaf() && volume.getSize() >= other->volume.getSize())) {
		// Recurse into ourself
		int count = firstChild->getPotentialContactsWith(
				other, contacts, limit);

		// Check we have enough slots to do the other side too
		if (limit > count) {
			return count + secondChild->getPotentialContactsWith(
					other, contacts + count, limit - count);
		} else {
			return count;
		}
	} else {
		// Recurse into the other node
		int count = getPotentialContactsWith(other->firstChild, contacts, limit);

		// Check we have enough slots to do the other side too
		if (limit > count) {
			return count + getPotentialContactsWith(
					other->secondChild, contacts + count, limit - count);
		} else {
			return count;
		}
	}
}

//void* BSHNode::operator new(unsigned int size) {
//	return CollisionManager::ref().bshNodePool.alloc(size);
//}
//
//void BSHNode::operator delete(void* p) {
//	CollisionManager::ref().bshNodePool.free(p);
//}
