/*
 * RevCollisionBox.cpp
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#include "RevCollisionBox.h"

using namespace rev;

IMPLEMENT_BINDABLE(CollisionBox)

void CollisionBox::bind(IBinder& binder) {
	CollisionGeometry::bind(binder);
	binder.bindSimplePtr(center);
	binder.bindSimple(halfSize);
}
