/*
 * RevCollisionPlane.h
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#ifndef REVCOLLISIONPLANE_H_
#define REVCOLLISIONPLANE_H_

#include "RevCollisionGeometry.h"

namespace rev {

	class CollisionPlane: public CollisionGeometry {
		friend class IntersectionTests;
		friend class CollisionDetector;

		glm::vec3 normal = glm::vec3(0, 1, 0);
		float distance = 0.0;

	public:
		DECLARE_BINDABLE(CollisionPlane)

		CollisionPlane() {
			CollisionGeometry::moveable = false;
		}

		CollisionPlane(const glm::vec3& normal, float distance) :
				normal(normal), distance(distance) {
		}

		GeometryType getType() override {
			return GeometryType::PLANE;
		}

		void setNormal(const glm::vec3& normal) {
			this->normal = normal;
		}

		const glm::vec3& getNormal() const {
			return normal;
		}

		void setDistance(float distance) {
			this->distance = distance;
		}

		float getDistance() {
			return distance;
		}

	protected:
		void bind(IBinder& binder) override;
		void bindableValueChanged(void* ptr) override;
	};

} /* namespace rev */
#endif /* REVCOLLISIONPLANE_H_ */
