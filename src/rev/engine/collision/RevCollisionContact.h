/*
 * RevCollisionContact.h
 *
 *  Created on: 18-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVCOLLISIONCONTACT_H_
#define REVCOLLISIONCONTACT_H_

#include <glm/glm.hpp>
#include <rev/engine/components/RevCollisionComponent.h>

#define REV_MAX_COLLISION_CONTACS 2048

namespace rev {

	struct CollisionContact {
		/**
		 * Holds the bodies that are involved in the contact. The
		 * second of these can be nullptr, for contacts with the scenery.
		 */
		union {
			CollisionComponent* body[2];
			struct {
				CollisionComponent* first;
				CollisionComponent* second;
			};
		};

		/**
		 * Holds the position of the contact in world coordinates.
		 */
		glm::vec3 contactPoint;

		/**
		 * Holds the direction of the contact in world coordinates.
		 */
		glm::vec3 contactNormal;

		/**
		 * Holds the depth of penetration at the contact point. If both
		 * bodies are specified then the contact point should be midway
		 * between the inter-penetrating points.
		 */
		float penetration = 0.0f;

		void setBodyData(CollisionComponent* first, CollisionComponent* second) {
			this->first = first;
			this->second = second;
		}
	};

} /* namespace rev */

#endif /* REVCOLLISIONCONTACT_H_ */
