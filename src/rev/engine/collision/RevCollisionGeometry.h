/*
 * RevCollisionGeometry.h
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVCOLLISIONPRIMITIVE_H_
#define REVCOLLISIONPRIMITIVE_H_

#include <glm/glm.hpp>
#include <rev/engine/binding/RevIBindable.h>

namespace rev {

	class IntersectionTests;
	class CollisionDetector;

	enum class GeometryType {
		SPHERE = 0x01,
		BOX = 0x02,
		PLANE = 0x04
	};

	class CollisionGeometry: public IBindable {
		friend class IntersectionTests;
		friend class CollisionDetector;

	protected:
		/**
		 * The offset of this primitive from the given rigid body.
		 */
		glm::mat4 localTransform;

		/**
		 * The resultant transform of the primitive. This is
		 * calculated by combining the offset of the primitive
		 * with the transform of the rigid body.
		 */
		glm::mat4 transform;

		/**
		 * Determines whether this geometry is dynamic (true) or static (false).
		 * Static elements are for example buildings, ground, walls etc.
		 */
		bool moveable = true;

	public:
		CollisionGeometry() {
		}
		virtual ~CollisionGeometry() {
		}

		/**
		 * Calculates the internals for the primitive.
		 */
		void calculateInternals(const glm::mat4& ownerTransform) {
			transform = ownerTransform * localTransform;
		}

		/**
		 * This is a convenience function to allow access to the
		 * axis vectors in the transform for this primitive.
		 */
		glm::vec3 getAxis(int index) const {
			const glm::vec4& v = transform[index];
			return glm::vec3(v.x, v.y, v.z);
		}

		glm::vec3 getPosition() const {
			return getAxis(3);
		}

		/**
		 * Returns the resultant transform of the primitive, calculated from
		 * the combined offset of the primitive and the transform
		 * (orientation + position) of the rigid body to which it is
		 * attached.
		 */
		const glm::mat4& getTransform() const {
			return transform;
		}

		bool isMoveable() const {
			return moveable;
		}

		void setMoveable(bool moveable) {
			this->moveable = moveable;
		}

		virtual GeometryType getType() = 0;

	protected:
		void bind(IBinder& binder) override {
			binder.bindSimple(moveable);
		}
	};

} /* namespace rev */
#endif /* REVCOLLISIONPRIMITIVE_H_ */
