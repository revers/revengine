/*
 * RevCollisionDetector.h
 *
 *  Created on: 18-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVCOLLISIONDETECTOR_H_
#define REVCOLLISIONDETECTOR_H_

#include "RevCollisionContact.h"

namespace rev {

	class CollisionSphere;
	class CollisionBox;
	class CollisionPlane;
	class PotentialContact;

	class CollisionDetector {
		static const unsigned int MASK_SPHERE_SPHERE = unsigned(GeometryType::SPHERE);
		static const unsigned int MASK_BOX_BOX = unsigned(GeometryType::BOX);
		static const unsigned int MASK_PLANE_PLANE = unsigned(GeometryType::PLANE);
		static const unsigned int MASK_SPHERE_BOX = unsigned(GeometryType::SPHERE)
				| unsigned(GeometryType::BOX);
		static const unsigned int MASK_SPHERE_PLANE = unsigned(GeometryType::SPHERE)
				| unsigned(GeometryType::PLANE);
		static const unsigned int MASK_BOX_PLANE = unsigned(GeometryType::BOX)
				| unsigned(GeometryType::PLANE);

		CollisionContact collisionContacts[REV_MAX_COLLISION_CONTACS];

		/**
		 * Holds the contact array to write into.
		 */
		CollisionContact* contacts;

		/**
		 * Holds the maximum number of contacts the array can take.
		 */
		int contactsLeft;

		/**
		 * Holds the number of contacts found so far.
		 */
		int contactCount;

	public:
		CollisionDetector() {
		}

		void generateContacts(PotentialContact* potentialContacts, int size);

		CollisionContact* getContacts() {
			return collisionContacts;
		}

		int getContactsCount() const {
			return contactCount;
		}

	private:
		/**
		 * Checks if there are more contacts available in the contact
		 * data.
		 */
		bool hasMoreContacts() {
			return contactsLeft > 0;
		}

		/**
		 * Resets the data so that it has no used contacts recorded.
		 */
		void reset(int maxContacts) {
			contactsLeft = maxContacts;
			contactCount = 0;
			contacts = collisionContacts;
		}

		/**
		 * Notifies the data that the given number of contacts have
		 * been added.
		 */
		void addContacts(int count) {
			// Reduce the number of contacts remaining, add number used
			contactsLeft -= count;
			contactCount += count;

			// Move the array forward
			contacts += count;
		}

		int sphereAndHalfSpace(
				const CollisionSphere& sphere,
				const CollisionPlane& plane,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		int sphereAndTruePlane(
				const CollisionSphere& sphere,
				const CollisionPlane& plane,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		int sphereAndSphere(
				const CollisionSphere& one,
				const CollisionSphere& two,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		/**
		 * Does a collision test on a collision box and a plane representing
		 * a half-space (i.e. the normal of the plane
		 * points out of the half-space).
		 */
		int boxAndHalfSpace(
				const CollisionBox& box,
				const CollisionPlane& plane,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		int boxAndBox(
				const CollisionBox& one,
				const CollisionBox& two,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		int boxAndPoint(
				const CollisionBox& box,
				const glm::vec3& point,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		int boxAndSphere(
				const CollisionBox& box,
				const CollisionSphere& sphere,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody);

		void fillPointFaceBoxBox(
				const CollisionBox& one,
				const CollisionBox& two,
				const glm::vec3& toCenter,
				CollisionComponent* firstBody,
				CollisionComponent* secondBody,
				int best,
				float pen);

	};

} /* namespace rev */
#endif /* REVCOLLISIONDETECTOR_H_ */
