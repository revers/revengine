/*
 * RevBoundingSphereHierarchy.h
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVBOUNDINGSPHEREHIERARCHY_H_
#define REVBOUNDINGSPHEREHIERARCHY_H_

#include "primitives/RevBoundingSphere.h"
#include "RevPotentialContact.h"

namespace rev {

	/**
	 * A base class for nodes in a bounding sphere hierarchy.
	 *
	 * This class uses a binary tree to store the bounding
	 * volumes.
	 */
	class BSHNode {
	public:
		/**
		 * Holds the child nodes of this node.
		 */
		union {
			BSHNode* children[2];
			struct {
				BSHNode* firstChild;
				BSHNode* secondChild;
			};
		};

		/**
		 * Holds a single bounding volume encompassing all the
		 * descendants of this node.
		 */
		BoundingSphere volume;

		/**
		 * Holds the rigid body at this node of the hierarchy.
		 * Only leaf nodes can have a rigid body defined (see isLeaf).
		 */
		CollisionComponent* body;

		/**
		 * Holds the node immediately above us in the tree.
		 */
		BSHNode* parent;

		/**
		 * Creates a new node in the hierarchy with the given parameters.
		 */
		BSHNode(BSHNode* parent, const BoundingSphere& volume,
				CollisionComponent* body = nullptr) :
				parent(parent), volume(volume), body(body),
						firstChild(nullptr), secondChild(nullptr) {
		}

		BSHNode() :
				parent(nullptr), body(nullptr), firstChild(nullptr), secondChild(nullptr) {
		}

		/**
		 * Deletes this node, removing it first from the hierarchy, along
		 * with its associated
		 * rigid body and child nodes. This method deletes the node
		 * and all its children (but obviously not the rigid bodies). This
		 * also has the effect of deleting the sibling of this node, and
		 * changing the parent node so that it contains the data currently
		 * in that sibling. Finally it forces the hierarchy above the
		 * current node to reconsider its bounding volume.
		 */
		~BSHNode();

		/**
		 * Checks if this node is at the bottom of the hierarchy.
		 */
		bool isLeaf() const {
			return (body != nullptr);
		}

		/**
		 * Checks the potential contacts from this node downwards in
		 * the hierarchy, writing them to the given array (up to the
		 * given limit). Returns the number of potential contacts it
		 * found.
		 */
		int getPotentialContacts(PotentialContact* contacts, int limit) const;

		/**
		 * Inserts the given rigid body, with the given bounding volume,
		 * into the hierarchy. This may involve the creation of
		 * further bounding volume nodes.
		 */
		void insert(CollisionComponent* body);

	protected:

		/**
		 * Checks for overlapping between nodes in the hierarchy. Note
		 * that any bounding volume should have an overlaps method implemented
		 * that checks for overlapping with another object of its own type.
		 */
		bool overlaps(const BSHNode* other) const {
			return volume.overlaps(&other->volume);
		}

		/**
		 * Checks the potential contacts between this node and the given
		 * other node, writing them to the given array (up to the
		 * given limit). Returns the number of potential contacts it
		 * found.
		 */
		int getPotentialContactsWith(
				const BSHNode *other,
				PotentialContact* contacts,
				int limit) const;

		/**
		 * For non-leaf nodes, this method recalculates the bounding volume
		 * based on the bounding volumes of its children.
		 */
		void recalculateBoundingVolume(bool recurse = true);
	};

} /* namespace rev */
#endif /* REVBOUNDINGSPHEREHIERARCHY_H_ */
