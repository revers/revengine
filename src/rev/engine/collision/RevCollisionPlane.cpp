/*
 * RevCollisionPlane.cpp
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#include "RevCollisionPlane.h"

using namespace rev;

IMPLEMENT_BINDABLE(CollisionPlane)

void CollisionPlane::bind(IBinder& binder) {
	CollisionGeometry::bind(binder);
	binder.bindSimple(normal);
	binder.bindSimple(distance);
}

void CollisionPlane::bindableValueChanged(void* ptr) {
	if (ptr == &normal) {
		normal = glm::normalize(normal);
	}
}
