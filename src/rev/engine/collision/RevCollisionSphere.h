/*
 * RevCollisionSphere.h
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#ifndef REVCOLLISIONSPHERE_H_
#define REVCOLLISIONSPHERE_H_

#include "RevCollisionGeometry.h"

namespace rev {

	class CollisionSphere: public CollisionGeometry {
		friend class IntersectionTests;
		friend class CollisionDetector;

		glm::vec3* center = (glm::vec3*) (&CollisionGeometry::localTransform[3]);
		float radius = 1.0;

	public:
		DECLARE_BINDABLE(CollisionSphere)

		CollisionSphere() {
		}

		CollisionSphere(float radius) :
				radius(radius) {
		}

		CollisionSphere(float radius, const glm::vec3& center) :
				radius(radius) {
			*(this->center) = center;
		}

		GeometryType getType() override {
			return GeometryType::SPHERE;
		}

		const glm::vec3& getCenter() const {
			return *center;
		}

		void setCenter(const glm::vec3 center) {
			*(this->center) = center;
		}

		float getRadius() const {
			return radius;
		}

		void setRadius(float radius) {
			this->radius = radius;
		}

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCOLLISIONSPHERE_H_ */
