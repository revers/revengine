/*
 * RevCollisionManager.h
 *
 *  Created on: 09-05-2013
 *      Author: Revers
 */

#ifndef REVCOLLISIONMANAGER_H_
#define REVCOLLISIONMANAGER_H_

#include <vector>
#include <rev/engine/components/RevCollisionComponent.h>
#include "RevQuadTree.h"
#include "RevCollisionDetector.h"

// Make sure its the same as RevCollisionManager.h: REV_MAX_POTENTIAL_CONTACTS
#define REV_MAX_POTENTIAL_CONTACTS 2048

namespace rev {

	typedef std::vector<CollisionComponent*> CollisionComponentVect;

	class CollisionManager: public IBindable {
		static CollisionManager* collisionManager;
		CollisionComponentVect components;

		CollisionManager();
		CollisionManager(const CollisionManager&) = delete;

	private:
		QuadTree* quadTree = nullptr;
		CollisionDetector collisionDetector;
		bool paused = false;

		PotentialContact potentialContacts[REV_MAX_POTENTIAL_CONTACTS];
		int potentialContactsSize = 0;

		/* Debug */
		glm::vec2* ptrQuadTreeSize;
		glm::vec2* ptrQuadTreeCenter;
		int* ptrQuadTreeDepth;
		int* ptrQuadTreeLeavesCount;
		int contacts = 0;

	public:
		DECLARE_BINDABLE_SINGLETON(CollisionManager)

		~CollisionManager();

		static void createSingleton();
		static void destroySingleton();

		static CollisionManager& ref() {
			return *collisionManager;
		}

		static CollisionManager* getInstance() {
			return collisionManager;
		}

		/**
		 * CollisionManager is NOT owner of added component.
		 */
		void addComponent(CollisionComponent* comp) {
			components.push_back(comp);
		}

		void removeComponent(CollisionComponent* comp) {
			components.erase(std::remove(components.begin(), components.end(), comp),
					components.end());
		}

		void removeAllComponents() {
			components.clear();
		}

		CollisionComponentVect& getComponents() {
			return components;
		}

		void update();

		bool isPaused() const {
			return paused;
		}

		void setPaused(bool paused) {
			this->paused = paused;
		}

	private:
		void recreateQuadTree();

	protected:
		void bind(IBinder& binder) override;

	};

} /* namespace rev */
#endif /* REVCOLLISIONMANAGER_H_ */
