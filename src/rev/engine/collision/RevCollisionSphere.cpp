/*
 * RevCollisionSphere.cpp
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#include "RevCollisionSphere.h"

using namespace rev;

IMPLEMENT_BINDABLE(CollisionSphere)

void CollisionSphere::bind(IBinder& binder) {
	CollisionGeometry::bind(binder);
	binder.bindSimplePtr(center);
	binder.bindSimple(radius);
}
