/*
 * RevViewFrustum.cpp
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#include "RevViewFrustum.h"

using namespace rev;

ViewFrustum::ViewFrustum() {
}

ViewFrustum::~ViewFrustum() {
}

void ViewFrustum::normalizePlanes() {
	leftPlane.normalize();
	rightPlane.normalize();
	bottomPlane.normalize();
	topPlane.normalize();
	farPlane.normalize();
	nearPlane.normalize();
}

/**
 * http://fgiesen.wordpress.com/2012/08/31/frustum-planes-from-the-projection-matrix/
 */
ViewFrustum ViewFrustum::fromProjectionMatrix(
		const glm::mat4& m) {
	ViewFrustum frustum;

	using namespace glm;

	frustum.leftPlane.n.x = m[0][3] + m[0][0];
	frustum.leftPlane.n.y = m[1][3] + m[1][0];
	frustum.leftPlane.n.z = m[2][3] + m[2][0];
	frustum.leftPlane.D = m[3][3] + m[3][0];

	frustum.rightPlane.n.x = m[0][3] - m[0][0];
	frustum.rightPlane.n.y = m[1][3] - m[1][0];
	frustum.rightPlane.n.z = m[2][3] - m[2][0];
	frustum.rightPlane.D = m[3][3] - m[3][0];

	frustum.bottomPlane.n.x = m[0][3] + m[0][1];
	frustum.bottomPlane.n.y = m[1][3] + m[1][1];
	frustum.bottomPlane.n.z = m[2][3] + m[2][1];
	frustum.bottomPlane.D = m[3][3] + m[3][1];

	frustum.topPlane.n.x = m[0][3] - m[0][1];
	frustum.topPlane.n.y = m[1][3] - m[1][1];
	frustum.topPlane.n.z = m[2][3] - m[2][1];
	frustum.topPlane.D = m[3][3] - m[3][1];

	frustum.farPlane.n.x = m[0][3] + m[0][2];
	frustum.farPlane.n.y = m[1][3] + m[1][2];
	frustum.farPlane.n.z = m[2][3] + m[2][2];
	frustum.farPlane.D = m[3][3] + m[3][2];

	frustum.nearPlane.n.x = m[0][3] - m[0][2];
	frustum.nearPlane.n.y = m[1][3] - m[1][2];
	frustum.nearPlane.n.z = m[2][3] - m[2][2];
	frustum.nearPlane.D = m[3][3] - m[3][2];

	frustum.normalizePlanes();

	return frustum;
}

bool ViewFrustum::isVisible(const Sphere& sphere) const {
	// NOTE: Conservative test, not exact

	if (Plane::distance(nearPlane, sphere.c) < -sphere.r)
		return false;

	if (Plane::distance(farPlane, sphere.c) < -sphere.r)
		return false;

	if (Plane::distance(leftPlane, sphere.c) < -sphere.r)
		return false;

	if (Plane::distance(rightPlane, sphere.c) < -sphere.r)
		return false;

	if (Plane::distance(topPlane, sphere.c) < -sphere.r)
		return false;

	if (Plane::distance(bottomPlane, sphere.c) < -sphere.r)
		return false;

	return true;
}
