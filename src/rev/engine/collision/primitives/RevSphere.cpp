/*
 * RevSphere.cpp
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 *
 * Based on MiniBall by Nicolas Capens
 * [ http://www.flipcode.com/archives/Smallest_Enclosing_Spheres.shtml ]
 */

#include <cmath>
#include <cstdlib>

#include <glm/gtx/norm.hpp>
#include "RevSphere.h"

using namespace rev;
using namespace glm;

namespace rev {
	const float radiusEpsilon = 1e-4f;   // NOTE: To avoid numerical inaccuracies
}

Sphere::Sphere() {
	r = 1;
}

Sphere::Sphere(const glm::vec3& O) {
	r = 0 + radiusEpsilon;
	c = O;
}

Sphere::Sphere(const glm::vec3& O, float R) {
	r = R;
	c = O;
}

Sphere::Sphere(const glm::vec3& O, const glm::vec3& A) {
	glm::vec3 a = A - O;

	glm::vec3 o = 0.5f * a;

	r = glm::length(o) + radiusEpsilon;
	c = O + o;
}

Sphere::Sphere(const glm::vec3& O, const glm::vec3& A, const glm::vec3& B) {
	glm::vec3 a = A - O;
	glm::vec3 b = B - O;

	using namespace glm;

	float Denominator = 2.0f * dot((cross(a, b)), cross(a, b));

	vec3 o = (dot(b, b) * cross(cross(a, b), a) +
			dot(a, a) * cross(b, cross(a, b))) / Denominator;

	r = length(o) + radiusEpsilon;
	c = O + o;
}

Sphere::Sphere(const glm::vec3& O, const glm::vec3& A, const glm::vec3& B,
		const glm::vec3& C) {
	using namespace glm;

	vec3 a = A - O;
	vec3 b = B - O;
	vec3 c = C - O;

	float denominator = 2.0f * determinant(
			mat3(a.x, b.x, c.x,
					a.y, b.y, c.y,
					a.z, b.z, c.z));

	glm::vec3 o = (dot(c, c) * cross(a, b) +
			dot(b, b) * cross(c, a) +
			dot(a, a) * cross(b, c)) / denominator;

	r = length(o) + radiusEpsilon;
	c = O + o;
}

float Sphere::distance(const glm::vec3& P) const {
	return glm::distance(P, c) - r;
}

float Sphere::distanceSqr(const glm::vec3& P) const {
	return glm::distance2(P, c) - r * r;
}

float Sphere::distance(const Sphere& S, const glm::vec3& P) {
	return glm::distance(P, S.c) - S.r;
}

float Sphere::distance(const glm::vec3& P, const Sphere& S) {
	return glm::distance(P, S.c) - S.r;
}

float Sphere::distanceSqr(const Sphere& S, const glm::vec3& P)
		{
	return glm::distance2(P, S.c) - S.r * S.r;
}

float Sphere::distanceSqr(const glm::vec3& P, const Sphere& S) {
	return glm::distance2(P, S.c) - S.r * S.r;
}

Sphere Sphere::recurseMini(glm::vec3* P[], unsigned int p, unsigned int b) {
	Sphere MB;

	switch (b) {
	case 0:
		MB = Sphere();
		break;
	case 1:
		MB = Sphere(*P[-1]);
		break;
	case 2:
		MB = Sphere(*P[-1], *P[-2]);
		break;
	case 3:
		MB = Sphere(*P[-1], *P[-2], *P[-3]);
		break;
	case 4:
		MB = Sphere(*P[-1], *P[-2], *P[-3], *P[-4]);
		return MB;
	}

	for (unsigned int i = 0; i < p; i++) {
		if (MB.distanceSqr(*P[i]) > 0) {   // Signed square distance to sphere
			for (unsigned int j = i; j > 0; j--)
					{
				glm::vec3 *T = P[j];
				P[j] = P[j - 1];
				P[j - 1] = T;
			}

			MB = recurseMini(P + 1, i, b + 1);
		}
	}

	return MB;
}

Sphere Sphere::miniBall(glm::vec3 P[], unsigned int p) {
	glm::vec3 **L = new glm::vec3*[p];

	for (unsigned int i = 0; i < p; i++) {
		L[i] = &P[i];
	}

	Sphere MB = recurseMini(L, p);

	delete[] L;

	return MB;
}

Sphere Sphere::smallBall(glm::vec3 P[], unsigned int p) {
	glm::vec3 center;
	float radius = -1;

	if (p > 0) {
		center = vec3(0);

		for (unsigned int i = 0; i < p; i++) {
			center += P[i];
		}

		center /= (float) p;

		for (int i = 0; i < p; i++) {
			float d2 = glm::distance2(P[i], center);

			if (d2 > radius)
				radius = d2;
		}

		radius = sqrtf(radius) + radiusEpsilon;
	}

	return Sphere(center, radius);
}

/**
 * http://sci.tuomastonteri.fi/programming/sse/example3
 */
bool Sphere::intersect(const Ray& ray, float* distance) {
	float a = dot(ray.d, ray.d);
	float b = dot(ray.d, (2.0f * (ray.o - this->c)));
	float c = dot(this->c, this->c) + dot(ray.o, ray.o) - 2.0f * dot(ray.o, this->c)
			- this->r * this->r;
	float D = b * b + (-4.0f) * a * c;

	// If ray can not intersect then stop
	if (D < 0) {
		return false;
	}
	D = sqrtf(D);

	// Ray can intersect the sphere, solve the closer hitpoint
	float t = (-0.5f) * (b + D) / a;
	if (t > 0.0f) {
		*distance = sqrtf(a) * t;
	} else {
		return false;
	}
	return true;
}
