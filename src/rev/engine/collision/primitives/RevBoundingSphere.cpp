/*
 * RevBoundingSphere.cpp
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 * Based on MiniBall by Nicolas Capens
 * [ http://www.flipcode.com/archives/Smallest_Enclosing_Spheres.shtml ]
 */

#include <cmath>
#include "RevBoundingSphere.h"

using namespace rev;

BoundingSphere::BoundingSphere() {
}

BoundingSphere::BoundingSphere(const Sphere& sphere) {
	c = sphere.c;
	r = sphere.r;
}

BoundingSphere::BoundingSphere(const BoundingSphere &one,
		const BoundingSphere &two) {
	glm::vec3 cOffset = two.c - one.c;
	float distance = glm::dot(cOffset, cOffset);
	float rDiff = two.r - one.r;

	// Check if the larger sphere encloses the small one
	if (rDiff * rDiff >= distance) {
		if (one.r > two.r) {
			c = one.c;
			r = one.r;
		} else {
			c = two.c;
			r = two.r;
		}
	} else {
		// Otherwise we need to work with partially
		// overlapping spheres

		distance = sqrtf(distance);
		r = (distance + one.r + two.r) * 0.5f;

		// The new c is based on one's c, moved towards
		// two's c by an ammount proportional to the spheres'
		// radii.
		c = one.c;
		if (distance > 0) {
			c += cOffset * ((r - one.r) / distance);
		}
	}
}

BoundingSphere::~BoundingSphere() {
}

BoundingSphere& BoundingSphere::operator=(const Sphere& sphere) {
	c = sphere.c;
	r = sphere.r;

	return *this;
}

bool BoundingSphere::overlaps(const BoundingSphere* other) const {
	glm::vec3 v = c - other->c;
	float distanceSquared = glm::dot(v, v);
	return distanceSquared < (r + other->r) * (r + other->r);
}

float BoundingSphere::getGrowth(const BoundingSphere& other) const {
	BoundingSphere newSphere(*this, other);

	// We return a value proportional to the change in surface
	// area of the sphere.
	return newSphere.r * newSphere.r - r * r;
}
