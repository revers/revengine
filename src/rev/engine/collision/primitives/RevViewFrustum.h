/*
 * RevViewFrustum.h
 *
 *  Created on: 12-05-2013
 *      Author: Revers
 */

#ifndef REVVIEWFRUSTUM_H_
#define REVVIEWFRUSTUM_H_

#include <glm/glm.hpp>
#include "RevPlane.h"
#include "RevSphere.h"

namespace rev {
    class ViewFrustum {
    public:
        Plane nearPlane;
        Plane farPlane;
        Plane leftPlane;
        Plane rightPlane;
        Plane topPlane;
        Plane bottomPlane;

        ViewFrustum();
        ViewFrustum(const ViewFrustum& viewFrustum) = default;

        ~ViewFrustum();

        ViewFrustum& operator=(const ViewFrustum& viewFrustum) = default;

        void normalizePlanes();

		bool isVisible(const Sphere& sphere) const;

        /**
         * http://fgiesen.wordpress.com/2012/08/31/frustum-planes-from-the-projection-matrix/
         */
        static ViewFrustum fromProjectionMatrix(const glm::mat4& m);
    };

}
#endif /* REVVIEWFRUSTUM_H_ */
