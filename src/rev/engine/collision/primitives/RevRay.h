/*
 * RevRay.h
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */

#ifndef REVRAY_H_
#define REVRAY_H_

#include <glm/glm.hpp>

namespace rev {

	class Ray {
	public:
		/**
		 * Origin.
		 */
		glm::vec3 o;

		/**
		 * Direction.
		 */
		glm::vec3 d;

		Ray() {
		}
		explicit Ray(const glm::vec3& origin, const glm::vec3& direction) :
				o(origin), d(direction) {
		}
		~Ray() {
		}

		static Ray fromOriginAndDirection(const glm::vec3& origin, const glm::vec3& direction) {
			return Ray(origin, direction);
		}

		static Ray fromTwoPoints(const glm::vec3& origin, const glm::vec3& secondPoint) {
			glm::vec3 dir = secondPoint - origin;
			return Ray(origin, glm::normalize(dir));
		}
	};

} /* namespace rev */
#endif /* REVRAY_H_ */
