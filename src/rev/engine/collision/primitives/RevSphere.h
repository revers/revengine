/*
 * RevSphere.h
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 *
 * Based on MiniBall by Nicolas Capens
 * [ http://www.flipcode.com/archives/Smallest_Enclosing_Spheres.shtml ]
 */

#ifndef REVSPHERE_H_
#define REVSPHERE_H_

#include <glm/glm.hpp>
#include "RevRay.h"

namespace rev {

    class Sphere {
    public:

    	/**
    	 * Center.
    	 */
        glm::vec3 c;

        /**
         * Radius.
         */
        float r;

        Sphere();
        Sphere(const Sphere& x) = default;

        /**
         * Sphere with center O and minimal possible radius.
         */
        Sphere(const glm::vec3& O);

        /**
         * Center and radius.
         */
        Sphere(const glm::vec3& O, float R);


        /**
         * Sphere through two points.
         */
        Sphere(const glm::vec3& O, const glm::vec3& A);

        /**
         * Sphere through three points.
         */
        Sphere(const glm::vec3& O, const glm::vec3& A, const glm::vec3& B);

        /**
         * Sphere through four points.
         */
        Sphere(const glm::vec3& O, const glm::vec3& A, const glm::vec3& B,
                const glm::vec3& C);

        Sphere& operator=(const Sphere& S) = default;

        /**
         * Distance from p to boundary of the Sphere.
         */
        float distance(const glm::vec3& P) const;

        /**
         * Square distance from p to boundary of the Sphere.
         */
        float distanceSqr(const glm::vec3& P) const;

        /**
         * Examines ray-sphere intersection.
         */
		bool intersect(const Ray& ray, float* distance);

        /**
         *  Distance from p to boundary of the Sphere.
         */
        static float distance(const Sphere& S, const glm::vec3& P);

        /**
         * Distance from p to boundary of the Sphere.
         */
        static float distance(const glm::vec3& P, const Sphere& S);

        /**
         * Square distance from p to boundary of the Sphere.
         */
        static float distanceSqr(const Sphere& S, const glm::vec3& P);

        /**
         * Square distance from p to boundary of the Sphere.
         */
        static float distanceSqr(const glm::vec3& P, const Sphere& S);

        /**
         * Smallest enclosing sphere.
         */
        static Sphere miniBall(glm::vec3 P[], unsigned int p);

        /**
         * Enclosing sphere approximation.
         */
        static Sphere smallBall(glm::vec3 P[], unsigned int p);

    private:
        static Sphere recurseMini(glm::vec3 *P[], unsigned int p,
                unsigned int b = 0);
    };

    extern const float radiusEpsilon;

}
#endif /* REVSPHERE_H_ */
