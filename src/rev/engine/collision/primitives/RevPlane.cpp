/*
 * RevPlane.cpp
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 *
 * Based on MiniBall by Nicolas Capens
 * [ http://www.flipcode.com/archives/Smallest_Enclosing_Spheres.shtml ]
 */

#include <cmath>
#include <rev/engine/math/RevMathHelper.h>
#include "RevPlane.h"

using namespace rev;

Plane& Plane::operator+() {
	return *this;
}

Plane Plane::operator-() const {
	return Plane(-n, -D);
}

float Plane::distance(const glm::vec3& P) const {
	return glm::dot(P, n) + D;
}

float Plane::distance(const glm::vec3& P, const Plane& p) {
	return glm::dot(P, p.n) + p.D;
}

float Plane::distance(const Plane& p, const glm::vec3& P) {
	return glm::dot(p.n, P) + p.D;
}

Plane& Plane::normalize() {
	float l = glm::length(n);

	n /= l;
	D /= l;

	return *this;
}

glm::vec3 Plane::intersection(const Plane& a, const Plane& b,
		const Plane& c) {
	glm::vec3 result;

	float denominator = c.n.x * b.n.y * a.n.z - b.n.x * c.n.y * a.n.z
			- c.n.x * a.n.y * b.n.z + a.n.x * c.n.y * b.n.z
			+ b.n.x * a.n.y * c.n.z - a.n.x * b.n.y * c.n.z;

	result.x = -(-c.n.y * b.D * a.n.z + b.n.y * c.D * a.n.z
			+ c.n.y * a.D * b.n.z - a.n.y * c.D * b.n.z - b.n.y * a.D * c.n.z
			+ a.n.y * b.D * c.n.z) / denominator;
	result.y = -(c.n.x * b.D * a.n.z - b.n.x * c.D * a.n.z - c.n.x * a.D * b.n.z
			+ a.n.x * c.D * b.n.z + b.n.x * a.D * c.n.z - a.n.x * b.D * c.n.z)
			/ denominator;
	result.z = -(c.n.x * b.n.y * a.D - b.n.x * c.n.y * a.D - c.n.x * a.n.y * b.D
			+ a.n.x * c.n.y * b.D + b.n.x * a.n.y * c.D - a.n.x * b.n.y * c.D)
			/ denominator;

	return result;
}

// http://blogs.warwick.ac.uk/nickforrington/entry/raytracing_intersection_with/
bool Plane::intersect(const Ray& ray, float* t) {
	float dn = glm::dot(ray.d, n);
	if (almostEqual(dn, 0.0f)) {
		return false;
	}
	float numerator = -D - glm::dot(ray.o, n);
	*t = numerator / dn;
	return true;
}
