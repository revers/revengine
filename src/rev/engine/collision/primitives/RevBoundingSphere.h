/*
 * RevBoundingSphere.h
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 *
 * Based on Cyclone Physics Engine by Ian Millington
 * (http://procyclone.com/).
 */

#ifndef REVBOUNDINGSPHERE_H_
#define REVBOUNDINGSPHERE_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLDefines.h>
#include "RevSphere.h"

namespace rev {

	class BoundingSphere: public Sphere {
	public:
		BoundingSphere();
		BoundingSphere(const BoundingSphere& sphere) = default;
		BoundingSphere(const Sphere& sphere);

		/**
		 * Center and radius.
		 */
		BoundingSphere(const glm::vec3& c, float r) : Sphere(c, r) {
		}

		/**
		 * Creates a bounding sphere to enclose the two given bounding
		 * spheres.
		 */
		BoundingSphere(const BoundingSphere& one, const BoundingSphere& two);

		/**
		 * Checks if the bounding sphere overlaps with the other given
		 * bounding sphere.
		 */
		bool overlaps(const BoundingSphere* other) const;

		/**
		 * Reports how much this bounding sphere would have to grow
		 * by to incorporate the given bounding sphere. Note that this
		 * calculation returns a value not in any particular units (i.e.
		 * its not a volume growth). In fact the best implementation
		 * takes into account the growth in surface area (after the
		 * Goldsmith-Salmon algorithm for tree construction).
		 */
		float getGrowth(const BoundingSphere& other) const;

		/**
		 * Returns the volume of this bounding volume. This is used
		 * to calculate how to recurse into the bounding volume tree.
		 * For a bounding sphere it is a simple calculation.
		 */
		float getSize() const {
			return 1.333333f * float(PI) * r * r * r;
		}

		~BoundingSphere();

		BoundingSphere& operator=(const BoundingSphere& sphere) = default;
		BoundingSphere& operator=(const Sphere& sphere);

	};
} /* namespace rev */
#endif /* REVBOUNDINGSPHERE_H_ */
