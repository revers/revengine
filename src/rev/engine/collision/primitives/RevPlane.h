/*
 * RevPlane.h
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 *
 * Based on MiniBall by Nicolas Capens
 * [ http://www.flipcode.com/archives/Smallest_Enclosing_Spheres.shtml ]
 */

#ifndef REVPLANE_H_
#define REVPLANE_H_

#include <glm/glm.hpp>
#include "RevRay.h"

namespace rev {

    /**
     * http://www.lighthouse3d.com/tutorials/maths/plane/
     *  nx * x + ny * y + nz * z + D = 0;
     */
    class Plane {
    public:

        /**
         * Normal vector.
         */
        glm::vec3 n;

        /**
         * Distance to origin along normal.
         */
        float D;

        Plane();

        Plane(const Plane& p) = default;

        /**
         * Normal and distance to origin.
         */
        Plane(const glm::vec3& n, float D);

        /**
         * Normal and point on plane.
         */
        Plane(const glm::vec3& n, const glm::vec3& P);

        /**
         * Through three points.
         */
        Plane(const glm::vec3& P0, const glm::vec3& P1, const glm::vec3& P2);

        /**
         * Plane equation.
         */
        Plane(float A, float B, float C, float D);

        /**
         * Normalize the Plane equation.
         */
        Plane& normalize();

        Plane& operator=(const Plane& p) = default;

        Plane& operator+();

        /**
         * Flip normal.
         */
        Plane operator-() const;

		bool intersect(const Ray& ray, float* t);

        /**
         * Oriented distance between point and plane.
         */
        float distance(const glm::vec3& P) const;

        /**
         * Oriented distance between point and plane.
         */
        static float distance(const glm::vec3& P, const Plane& p);

        /**
         * Oriented distance between plane and point.
         */
        static float distance(const Plane& p, const glm::vec3& P);

        /**
         * Intersection of three planes. This function
         * assumes that there is one intersection point.
         * It is only valid then.
         */
        static glm::vec3 intersection(const Plane& a, const Plane& b,
                const Plane& c);
    };

    //-------------------------------------------------------------------
    // Inline functions
    //===================================================================

    inline Plane::Plane() {
    }

    inline Plane::Plane(const glm::vec3& p_n, float p_D) {
        n = p_n;
        D = p_D;
    }

    inline Plane::Plane(const glm::vec3& p_n, const glm::vec3& P) {
        n = p_n;
        D = -glm::dot(n, P);
    }

    inline Plane::Plane(const glm::vec3& P0, const glm::vec3& P1,
            const glm::vec3& P2) {
        n = glm::cross((P1 - P0), (P2 - P0));
        D = -glm::dot(n, P0);
    }

    inline Plane::Plane(float p_A, float p_B, float p_C, float p_D) {
        n[0] = p_A;
        n[1] = p_B;
        n[2] = p_C;
        D = p_D;
    }

} /* namespace rev */
#endif /* REVPLANE_H_ */
