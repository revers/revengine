/*
 * RevCollisionBox.h
 *
 *  Created on: 16-05-2013
 *      Author: Revers
 */

#ifndef REVCOLLISIONBOX_H_
#define REVCOLLISIONBOX_H_

#include "RevCollisionGeometry.h"

namespace rev {

	class CollisionBox: public CollisionGeometry {
		friend class IntersectionTests;
		friend class CollisionDetector;

		glm::vec3* center = (glm::vec3*) (&CollisionGeometry::localTransform[3]);
		/**
		 * Holds the half-sizes of the box along each of its local axes.
		 */
		glm::vec3 halfSize = glm::vec3(0.5);

	public:
		DECLARE_BINDABLE(CollisionBox)

		CollisionBox() {
		}

		CollisionBox(float a, float b, float c) :
				halfSize(a / 2, b / 2, c / 2) {
		}

		CollisionBox(float a, float b, float c, const glm::vec3& center) :
				halfSize(a / 2, b / 2, c / 2) {
			*(this->center) = center;
		}

		GeometryType getType() override {
			return GeometryType::BOX;
		}

		const glm::vec3& getCenter() const {
			return *center;
		}

		void setCenter(const glm::vec3 center) {
			*(this->center) = center;
		}

		const glm::vec3& getHalfSize() const {
			return halfSize;
		}

		void setHalfSize(const glm::vec3& halfSize) {
			this->halfSize = halfSize;
		}

		void setSize(const glm::vec3& size) {
			halfSize.x = size.x * 0.5f;
			halfSize.y = size.y * 0.5f;
			halfSize.z = size.z * 0.5f;
		}

	protected:
		void bind(IBinder& binder) override;
	};

} /* namespace rev */
#endif /* REVCOLLISIONBOX_H_ */
