/*
 * RevPotentialContact.h
 *
 *  Created on: 15-05-2013
 *      Author: Revers
 */

#ifndef REVPOTENTIALCONTACT_H_
#define REVPOTENTIALCONTACT_H_

#include <rev/engine/components/RevCollisionComponent.h>

namespace rev {

	/**
	 * Stores a potential contact to check later.
	 */
	struct PotentialContact {
		/**
		 * Holds the bodies that might be in contact.
		 */
		union {
			CollisionComponent* body[2];
			struct {
				CollisionComponent* first;
				CollisionComponent* second;
			};
		};
	};

} /* namespace rev */

#endif /* REVPOTENTIALCONTACT_H_ */
