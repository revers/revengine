#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;

uniform mat4 MVP;

void main() {
	gl_Position = MVP * vec4(VertexPosition, 1.0);
}

//---------------------------------------------------------------------
//-- Fragment
#version 400

void main() {
	// Do nothing, depth will be written automatically
}
