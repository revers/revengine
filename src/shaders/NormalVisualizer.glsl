/**
 * Phong Shading Shader
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexNormal;

out vec3 Normal;

void main() {
    Normal = VertexNormal;
    gl_Position = vec4(VertexPosition, 1);
}
//---------------------------------------------------------------------
//-- Geometry
#version 330

uniform float Length;
uniform mat4 MVP;

in vec3 Normal[];

layout(triangles) in;
layout(line_strip, max_vertices = 9) out;

void main() {

    for (int i = 0; i < gl_in.length(); i++) {
        gl_Position = MVP * gl_in[i].gl_Position;
        EmitVertex();

        gl_Position = MVP * (gl_in[i].gl_Position + vec4(Normal[i], 0.0) * Length);
        EmitVertex();

        EndPrimitive();
    }
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

uniform vec3 Color = vec3(1.0, 0.0, 0.0);

out vec4 FragColor;

void main() {
    FragColor = vec4(Color, 1.0);
}
