/*
 * ControlPoints.glsl
 *
 *  Created on: 13-04-2013
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330
in vec3 VertexPosition;
in vec3 VertexColor;

uniform float PointRadius; // point size in world space
uniform float PointScale; // scale to calculate size in pixels
uniform mat4 MVPMatrix;
uniform mat4 ModelViewMatrix;
uniform vec3 HighlightColor = vec3(0, 1, 0);
uniform vec3 HighlightPosition = vec3(-999, -999, -999);

out vec3 Color;
out vec3 PosEye; // position of center in eye space

void main() {
	gl_Position = MVPMatrix * vec4(VertexPosition, 1.0);

	if (VertexPosition == HighlightPosition) {
		Color = HighlightColor;
	} else {
		Color = VertexColor;
	}
	// calculate window-space point size
	PosEye = vec3(ModelViewMatrix * vec4(VertexPosition, 1.0));
	float dist = length(PosEye);
	gl_PointSize = PointRadius * (PointScale / dist);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec3 Color;
in vec3 PosEye;

uniform float PointRadius; // point size in world space

out vec4 FragColor;

void main() {
	const vec3 lightDir = vec3(0.577, 0.577, 0.577);
	const float shininess = 70.0;

	// calculate normal from texture coordinates
	vec3 n;
	n.xy = gl_PointCoord * vec2(2.0, -2.0) + vec2(-1.0, 1.0);
	float mag = dot(n.xy, n.xy);
	if (mag > 1.0) {
		discard; // kill pixels outside circle
	}
	n.z = sqrt(1.0 - mag);

	// point on surface of sphere in eye space
	vec3 spherePosEye = PosEye + n * PointRadius;

	// calculate lighting
	float diffuse = max(0.0, dot(lightDir, n));
	vec3 v = normalize(-spherePosEye);
	vec3 h = normalize(lightDir + v);
	float specular = pow(max(0.0, dot(n, h)), shininess);

	FragColor = vec4(Color * diffuse + specular, 1.0);
}

