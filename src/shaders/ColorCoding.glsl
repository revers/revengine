/*
 * ColorCodingPicking.glsl
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;

uniform mat4 MVPMatrix;

void main() {
	gl_Position = MVPMatrix * vec4(VertexPosition, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

out vec4 FragColor;

uniform int DrawIndex;
uniform int ObjectIndex;

void main() {
	FragColor = vec4(ObjectIndex, DrawIndex, 0, 1.0);
}


