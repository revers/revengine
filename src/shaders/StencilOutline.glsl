/*
 * StencilOutline.glsl
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;

uniform mat4 MVPMatrix;
uniform mat4 ScaleMatrix;

void main() {
    gl_Position = MVPMatrix * ScaleMatrix * vec4(VertexPosition, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

uniform vec3 Color;

out vec4 FragColor;

void main() {
    FragColor = vec4(Color, 1.0);
}

