#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexInitVel; // Particle initial velocity
in float StartTime;    // Particle "birth" time

out float Transp;  // Transparency of the particle

uniform float Time;  // Animation time
uniform vec3 Gravity = vec3(0.0, -0.05, 0.0);  // world coords
uniform float ParticleLifetime = 3.5;  // Max particle lifetime
uniform float Period = 5.0;
uniform float ParticleSize = 10.0;

uniform mat4 MVP;

void main() {
	// Assume the initial position is (0,0,0).
	vec3 pos = vec3(0.0);
	Transp = 0.0;

	// Particle dosen't exist until the start time
	float time = mod(Time, Period);

	if (time > StartTime) {
		float t = time - StartTime;

		if (t < ParticleLifetime) {
			pos = VertexInitVel * t + Gravity * t * t;
			Transp = 1.0 - t / ParticleLifetime;
		}
	}

	// Draw at the current position
	gl_Position = MVP * vec4(pos, 1.0);
	gl_PointSize = ParticleSize;
}

//---------------------------------------------------------------------
//-- Fragment
#version 330

in float Transp;
uniform sampler2D ParticleTex;

out vec4 FragColor;

void main() {
	FragColor = texture(ParticleTex, gl_PointCoord);
	FragColor.a *= Transp;
}
