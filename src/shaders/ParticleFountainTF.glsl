#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 400

subroutine void RenderPassType();
subroutine uniform RenderPassType RenderPass;

in vec3 VertexPosition;
in vec3 VertexVelocity;
in float VertexStartTime;
in vec3 VertexInitialVelocity;

out vec3 Position;   // To transform feedback
out vec3 Velocity;   // To transform feedback
out float StartTime; // To transform feedback
out float Transp;    // To fragment shader

uniform float Time;  // Simulation time
uniform float H;     // Elapsed time between frames
uniform vec3 Accel;  // Particle acceleration
uniform float ParticleLifetime;  // Particle lifespan
uniform float ParticleSize = 10.0;

uniform mat4 MVP;

subroutine(RenderPassType)
void update() {

	// Update position & velocity for next frame
	Position = VertexPosition;
	Velocity = VertexVelocity;
	StartTime = VertexStartTime;

	if (Time >= StartTime) {

		float age = Time - StartTime;
		if (age > ParticleLifetime) {
			// The particle is past it's lifetime, recycle.
			Position = vec3(0.0);
			Velocity = VertexInitialVelocity;
			StartTime = Time;
		} else {
			// The particle is alive, update.
			Position += Velocity * H;
			Velocity += Accel * H;
		}
	}
}

subroutine(RenderPassType)
void render() {
	float age = Time - VertexStartTime;
	Transp = 1.0 - age / ParticleLifetime;
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	gl_PointSize = ParticleSize;
}

void main() {
	// This will call either render() or update()
	RenderPass();
}

//---------------------------------------------------------------------
//-- Fragment
#version 400

in float Transp;
uniform sampler2D ParticleTex;

out vec4 FragColor;

void main() {
	FragColor = texture(ParticleTex, gl_PointCoord);
	FragColor.a *= Transp;
}
