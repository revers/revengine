/*
 * CellShading.glsl
 *  Created on: 28 cze 2013
 *
 *  Shader by Philip Rideout:
 *  http://prideout.net/blog/?p=22
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexNormal;

out vec3 EyespaceNormal;
out vec3 Diffuse;

uniform vec3 Kd; // Diffuse reflectivity
uniform mat3 NormalMatrix;
uniform mat4 MVP;

void main() {
	EyespaceNormal = normalize(NormalMatrix * VertexNormal);
	Diffuse = Kd;
	gl_Position = MVP * vec4(VertexPosition, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec3 Diffuse;
in vec3 EyespaceNormal;

uniform vec4 LightPosition;
uniform vec3 Ka; // Ambient reflectivity
uniform vec3 Ks; // Specular reflectivity

uniform float Shininess; // Specular shininess factor
uniform bool AntiAlias = true;

out vec4 FragColor;

float stepmix(float edge0, float edge1, float E, float x) {
	float T = clamp(0.5 * (x - edge0 + E) / E, 0.0, 1.0);
	return mix(edge0, edge1, T);
}

vec3 aaCellShading() {
	vec3 N = normalize(EyespaceNormal);
	vec3 L = normalize(LightPosition.xyz);
	vec3 Eye = vec3(0, 0, 1);
	vec3 H = normalize(L + Eye);

	float df = max(0.0, dot(N, L));
	float sf = max(0.0, dot(N, H));
	sf = pow(sf, Shininess);

	const float A = 0.1;
	const float B = 0.3;
	const float C = 0.6;
	const float D = 1.0;
	float E = fwidth(df);

	if (df > A - E && df < A + E) {
		df = stepmix(A, B, E, df);
	} else if (df > B - E && df < B + E) {
		df = stepmix(B, C, E, df);
	} else if (df > C - E && df < C + E) {
		df = stepmix(C, D, E, df);
	} else if (df < A) {
		df = 0.0;
	} else if (df < B) {
		df = B;
	} else if (df < C) {
		df = C;
	} else {
		df = D;
	}

	E = fwidth(sf);
	if (sf > 0.5 - E && sf < 0.5 + E) {
		sf = smoothstep(0.5 - E, 0.5 + E, sf);
	} else {
		sf = step(0.5, sf);
	}

	return Ka + df * Diffuse + sf * Ks;
}

vec3 cellShading() {
	vec3 N = normalize(EyespaceNormal);
	vec3 L = normalize(LightPosition.xyz);
	vec3 E = vec3(0, 0, 1);
	vec3 H = normalize(L + E);

	const float A = 0.1;
	const float B = 0.3;
	const float C = 0.6;
	const float D = 1.0;

	float df = max(0.0, dot(N, L));
	if (df < A) {
		df = 0.0;
	} else if (df < B) {
		df = B;
	} else if (df < C) {
		df = C;
	} else {
		df = D;
	}
	float sf = max(0.0, dot(N, H));
	sf = pow(sf, Shininess);
	sf = step(0.5, sf);

	return Ka + df * Diffuse + sf * Ks;
}

void main() {
	if (AntiAlias) {
		FragColor = vec4(aaCellShading(), 1.0);
	} else {
		FragColor = vec4(cellShading(), 1.0);
	}
}
