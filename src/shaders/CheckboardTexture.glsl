/*
 * CheckboardTexture.glsl
 *
 *  Created on: 06-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 140

uniform vec3 LightPosition;
uniform vec3 LightColor;
uniform vec3 EyePosition;
uniform vec3 SpecularColor;
uniform vec3 AmbientColor;

uniform float Kd;
uniform mat4 MVMatrix;
uniform mat4 MVPMatrix;
uniform mat3 NormalMatrix;

in vec4 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexCoord;

out vec3 DiffuseColor;
out vec3 SpecularColorColor;
out vec2 TexCoord;

void main() {
	vec3 ecPosition = vec3(MVMatrix * VertexPosition);
	vec3 tnorm = normalize(NormalMatrix * VertexNormal);
	vec3 lightVec = normalize(LightPosition - ecPosition);
	vec3 viewVec = normalize(EyePosition - ecPosition);
	vec3 hvec = normalize(viewVec + lightVec);

	float spec = clamp(dot(hvec, tnorm), 0.0, 1.0);
	spec = pow(spec, 16.0);

	DiffuseColor = LightColor * vec3(Kd * dot(lightVec, tnorm));
	DiffuseColor = clamp(AmbientColor + DiffuseColor, 0.0, 1.0);
	SpecularColorColor = clamp((LightColor * SpecularColor * spec), 0.0, 1.0);

	TexCoord = VertexTexCoord;
	gl_Position = MVPMatrix * VertexPosition;
}
//---------------------------------------------------------------------
//-- Fragment

#version 140

uniform vec3 ColorA;
uniform vec3 ColorB;
uniform vec3 AvgColor;
uniform float Frequency;

in vec3 DiffuseColor;
in vec3 SpecularColorColor;
in vec2 TexCoord;

out vec4 FragColor;

void main() {
	vec3 color;
	// Determine the width of the projection of one pixel into s-t space
	vec2 fw = fwidth(TexCoord);
	// Determine the amount of fuzziness
	vec2 fuzz = fw * Frequency * 2.0;
	float fuzzMax = max(fuzz.s, fuzz.t);
	// Determine the position in the checkerboard pattern
	vec2 checkPos = fract(TexCoord * Frequency);
	if (fuzzMax < 0.5) {
		// If the filter width is small enough,
		// compute the pattern color
		vec2 p = smoothstep(vec2(0.5), fuzz + vec2(0.5), checkPos)
				+ (1.0 - smoothstep(vec2(0.0), fuzz, checkPos));
		color = mix(ColorA, ColorB, p.x * p.y + (1.0 - p.x) * (1.0 - p.y));
		// Fade in the average color when we get close to the limit
		color = mix(color, AvgColor, smoothstep(0.125, 0.5, fuzzMax));
	} else {
		// Otherwise, use only the average color
		color = AvgColor;
	}
	FragColor = vec4(color * DiffuseColor + SpecularColorColor, 1.0);
}

