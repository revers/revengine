#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexNormal;
in vec3 VertexTexCoord;
in vec3 VertexInitialVelocity; // world coords
in float StartTime;

out vec3 Position;
out vec3 Normal;

uniform float Time;
uniform vec3 Gravity;  // world coords
uniform float ParticleLifetime;
uniform float Period = 5.0;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;

void main() {
	// Work in camera coordinates
	Position = (ModelViewMatrix * vec4(VertexPosition, 1.0)).xyz;
	Normal = NormalMatrix * VertexNormal;
	vec3 g = NormalMatrix * Gravity;
	vec3 v0 = NormalMatrix * VertexInitialVelocity;

	// Particle dosen't exist until the start time
	float time = mod(Time, Period);

	if (time > StartTime) {
		float t = time - StartTime;

		if (t < ParticleLifetime) {
			Position += v0 * t + Gravity * t * t;
		}
	}

	// Draw at the current position
	gl_Position = ProjectionMatrix * vec4(Position, 1.0);
}

//---------------------------------------------------------------------
//-- Fragment
#version 330

uniform vec4 LightPosition;  // Light position in eye coords.

struct MaterialInfo {
	vec3 Ka;            // Ambient reflectivity
	vec3 Kd;            // Diffuse reflectivity
	vec3 Ks;            // Specular reflectivity
	float Shininess;    // Specular shininess factor
};
uniform MaterialInfo Material;

in vec3 Position;
in vec3 Normal;

out vec4 FragColor;

vec3 phongModel(vec3 pos, vec3 norm) {
	vec3 s = normalize(vec3(LightPosition) - pos);
	vec3 v = normalize(-pos.xyz);
	vec3 r = reflect(-s, norm);
	vec3 ambient = Material.Ka;

	float sDotN = max(dot(s, norm), 0.0);
	vec3 diffuse = Material.Kd * sDotN;

	vec3 spec = vec3(0.0);
	if (sDotN > 0.0) {
		spec = Material.Ks * pow(max(dot(r, v), 0.0), Material.Shininess);
	}

	return ambient + diffuse + spec;
}

void main() {
	FragColor = vec4(phongModel(Position, Normal), 1.0);
}
