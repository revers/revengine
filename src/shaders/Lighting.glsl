#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330
//layout(location = 0) in vec3 Position;
//layout(location = 1) in vec2 TexCoord;
//layout(location = 2) in vec3 Normal;
in vec3 VertexPosition;
in vec3 VertexNormal;

uniform mat4 MVP;
uniform mat4 ModelMatrix;

//out vec2 TexCoord;
out vec3 Normal;
out vec3 Position;

void main() {
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	//TexCoord = TexCoord;
	Normal = (ModelMatrix * vec4(VertexNormal, 0.0)).xyz;
	Position = (ModelMatrix * vec4(VertexPosition, 1.0)).xyz;
}

//---------------------------------------------------------------------
//-- Fragment
#version 330

const int MAX_POINT_LIGHTS = 8;
const int MAX_SPOT_LIGHTS = 8;

//in vec2 TexCoord;
in vec3 Normal;
in vec3 Position;

out vec4 FragColor;

struct BaseLight {
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

struct DirectionalLightStruct {
	BaseLight Base;
	vec3 Direction;
};

struct Attenuation {
	float Constant;
	float Linear;
	float Exp;
};

struct PointLight {
	BaseLight Base;
	vec3 Position;
	Attenuation Atten;
};

struct SpotLight {
	PointLight Base;
	vec3 Direction;
	float Cutoff;
};

uniform int NumPointLights;
uniform int NumSpotLights;
uniform DirectionalLightStruct DirectionalLight;
uniform PointLight PointLights[MAX_POINT_LIGHTS];
uniform SpotLight SpotLights[MAX_SPOT_LIGHTS];
//uniform sampler2D Sampler;
uniform vec3 EyeWorldPos;
uniform float SpecularIntensity;
uniform float SpecularPower;

vec4 calcLightInternal(BaseLight light, vec3 lightDirection, vec3 normal) {
	vec4 ambientColor = vec4(light.Color, 1.0f) * light.AmbientIntensity;
	float diffuseFactor = dot(normal, -lightDirection);
	vec4 diffuseColor = vec4(0, 0, 0, 0);
	vec4 specularColor = vec4(0, 0, 0, 0);

	if (diffuseFactor > 0) {
		diffuseColor = vec4(light.Color, 1.0f) * light.DiffuseIntensity * diffuseFactor;
		vec3 vertexToEye = normalize(EyeWorldPos - Position);
		vec3 lightReflect = normalize(reflect(lightDirection, normal));
		float specularFactor = dot(vertexToEye, lightReflect);
		specularFactor = pow(specularFactor, SpecularPower);
		if (specularFactor > 0) {
			specularColor = vec4(light.Color, 1.0f) *
					SpecularIntensity * specularFactor;
		}
	}
	return (ambientColor + diffuseColor + specularColor);
}

vec4 calcDirectionalLight(vec3 normal) {
	return calcLightInternal(DirectionalLight.Base, DirectionalLight.Direction, normal);
}

vec4 calcPointLight(PointLight light, vec3 normal) {
	vec3 lightDirection = Position - light.Position;
	float dist = length(lightDirection);
	lightDirection = normalize(lightDirection);
	vec4 color = calcLightInternal(light.Base, lightDirection, normal);

	float attenuation = light.Atten.Constant + light.Atten.Linear * dist
			+ light.Atten.Exp * dist * dist;
	return color / attenuation;
}

vec4 calcSpotLight(SpotLight light, vec3 normal) {
	vec3 lightToPixel = normalize(Position - light.Base.Position);
	float spotFactor = dot(lightToPixel, light.Direction);

	if (spotFactor > light.Cutoff) {
		vec4 color = calcPointLight(light.Base, normal);
		return color * (1.0 - (1.0 - spotFactor) * 1.0 / (1.0 - light.Cutoff));
	} else {
		return vec4(0, 0, 0, 0);
	}
}

void main() {
	vec3 normal = normalize(Normal);
	vec4 totalLight = calcDirectionalLight(normal);

	for (int i = 0; i < NumPointLights; i++) {
		totalLight += calcPointLight(PointLights[i], normal);
	}

	for (int i = 0; i < NumSpotLights; i++) {
		totalLight += calcSpotLight(SpotLights[i], normal);
	}

	FragColor = totalLight;
	//FragColor = texture2D(Sampler, TexCoord.xy) * totalLight;
}
