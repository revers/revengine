/*
 * Debug.glsl
 *
 *  Created on: 16-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexNormal;

out vec3 Normal;

uniform mat3 NormalMatrix;
uniform mat4 MVP;

void main() {
	Normal = normalize(NormalMatrix * VertexNormal);

	gl_Position = MVP * vec4(VertexPosition, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec3 Normal;

uniform vec3 LightDirection;
uniform vec3 Color;

out vec4 FragColor;

void main() {
	vec3 lighting = vec3(0.2, 0.2, 0.2);

	vec3 lightDir = normalize(LightDirection);
	vec3 normal = normalize(Normal);

	lighting += clamp(dot(lightDir, normal), 0.0, 1.0);

	vec3 col = clamp(lighting, 0.0, 1.0);

	FragColor = vec4(col * Color, 1.0);
}
