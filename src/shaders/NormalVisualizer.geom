/*
 * NormalVizualizer.geom
 *
 *  Created on: 26-01-2013
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$

//-- Geometry
#version 330

uniform float Length;
uniform mat4 MVP;

in vec3 Normal[];

layout(triangles) in;
layout(line_strip, max_vertices = 9) out;

void main() {

    for (int i = 0; i < gl_in.length(); i++) {
        gl_Position = MVP * gl_in[i].gl_Position;
        EmitVertex();

        gl_Position = MVP * (gl_in[i].gl_Position + vec4(Normal[i], 0.0) * Length);
        EmitVertex();

        EndPrimitive();
    }
}




