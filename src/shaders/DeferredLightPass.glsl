#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;

uniform mat4 MVP;

uniform vec2 ViewportSize;
out vec4 LightPos;

void main() {
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	LightPos = gl_Position;
}

//---------------------------------------------------------------------
//-- Fragment
#version 400

in vec4 LightPos;

uniform mat4 InvViewProjection;
uniform sampler2D DepthTexture;
uniform sampler2D NormalTexture;

uniform vec3 LightColor;
uniform vec3 LightPosition;
uniform float LightAttenuation;

uniform vec2 ViewportSize;

layout(location = 0) out vec4 FragColor;

// Calculate the 2D screen position of a position vector
vec2 postProjToScreen(vec4 position) {
	vec2 screenPos = position.xy / position.w;
	return screenPos * 0.5 + 0.5;
}

// Calculate the size of one half of a pixel, to convert
// between texels and pixels
vec2 halfPixel() {
	return vec2(0.5) / ViewportSize;
}

void main() {
	// Find the pixel coordinates of the input position in the depth
	// and normal textures
	vec2 texCoord = postProjToScreen(LightPos) + halfPixel();
	// Extract the depth for this pixel from the depth map
	vec4 depth = texture(DepthTexture, texCoord);

	// Recreate the position with the UV coordinates and depth value
	vec4 position;
	position.xy = LightPos.xy / LightPos.w;
	position.z = depth.x * 2.0 - 1.0;
	position.w = 1.0;

	// Transform position from screen space to world space
	//position = mul(position, InvViewProjection);
	position = InvViewProjection * position;
	position.xyz /= position.w;

	// Extract the normal from the normal map and move from
	// 0 to 1 range to -1 to 1 range
	vec3 normal = (texture(NormalTexture, texCoord).xyz - 0.5) * 2.0;

	// Perform the lighting calculations for a point light
	vec3 lightDirection = normalize(LightPosition - position.xyz);
	float lighting = clamp(dot(normal, lightDirection), 0.0, 1.0);

	// Attenuate the light to simulate a point light
	float d = distance(LightPosition, position.xyz);
	float att = 1.0 - pow(clamp(d / LightAttenuation, 0.0, 1.0), 6.0);

	FragColor = vec4(LightColor * lighting * att, 0.1);
}
