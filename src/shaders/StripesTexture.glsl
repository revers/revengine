/*
 * StripesTextureEffect.glsl
 *
 *  Created on: 05-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 140
uniform vec3 LightPosition;
uniform vec3 LightColor;
uniform vec3 EyePosition;
uniform vec3 Specular;
uniform vec3 Ambient;

uniform float Kd;

uniform mat4 MVMatrix;
uniform mat4 MVPMatrix;
uniform mat3 NormalMatrix;

in vec4 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexCoord;

out vec3 DiffuseColor;
out vec3 SpecularColor;
out float TexCoord;

void main() {
	vec3 ecPosition = vec3(MVMatrix * VertexPosition);
	vec3 tnorm = normalize(NormalMatrix * VertexNormal);
	vec3 lightVec = normalize(LightPosition - ecPosition);
	vec3 viewVec = normalize(EyePosition - ecPosition);
	vec3 hvec = normalize(viewVec + lightVec);

	float spec = clamp(dot(hvec, tnorm), 0.0, 1.0);
	spec = pow(spec, 16.0);

	DiffuseColor = LightColor * vec3(Kd * dot(lightVec, tnorm));
	DiffuseColor = clamp(Ambient + DiffuseColor, 0.0, 1.0);
	SpecularColor = clamp((LightColor * Specular * spec), 0.0, 1.0);

	TexCoord = VertexTexCoord.t;
	gl_Position = MVPMatrix * VertexPosition;
}
//---------------------------------------------------------------------
//-- Fragment
#version 140

uniform vec3 StripeColor;
uniform vec3 BackColor;

uniform float Width;
uniform float Fuzz;
uniform float Scale;

in vec3 DiffuseColor;
in vec3 SpecularColor;
in float TexCoord;

out vec4 FragColor;

void main() {
	float scaledT = fract(TexCoord * Scale);
	float frac1 = clamp(scaledT / Fuzz, 0.0, 1.0);
	float frac2 = clamp((scaledT - Width) / Fuzz, 0.0, 1.0);

	frac1 = frac1 * (1.0 - frac2);
	frac1 = frac1 * frac1 * (3.0 - (2.0 * frac1));

	vec3 finalColor = mix(BackColor, StripeColor, frac1);
	finalColor = finalColor * DiffuseColor + SpecularColor;

	FragColor = vec4(finalColor, 1.0);
}
