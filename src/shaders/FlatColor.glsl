/*
 * FlatColor.glsl
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexColor;

uniform mat4 MVPMatrix;

out vec3 Color;

void main() {
    gl_Position = MVPMatrix * vec4(VertexPosition, 1.0);

    Color = VertexColor;
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec3 Color;
out vec4 FragColor;

void main() {
    FragColor = vec4(Color, 1.0);
}

