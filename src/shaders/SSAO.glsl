/**
 * Phong Shading Shader
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec3 VertexNormal;

out vec4 HomogeneusPosition;
out vec3 Position;
out vec3 Normal;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;

uniform mat4 MVP;

void main() {
    Normal = normalize(NormalMatrix * VertexNormal);
    Position = vec3(ModelViewMatrix * vec4(VertexPosition, 1.0));

    gl_Position = MVP * vec4(VertexPosition, 1.0);
    HomogeneusPosition = gl_Position;
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec4 HomogeneusPosition;
in vec3 Position;
in vec3 Normal;

uniform sampler2D SSAOTexture;
uniform vec4 LightPosition;
uniform vec3 LightColor;
uniform vec3 Kd; // Diffuse reflectivity
uniform vec3 Ka; // Ambient reflectivity
uniform vec3 Ks; // Specular reflectivity

uniform float Shininess; // Specular shininess factor

out vec4 FragColor;

// Calculate the 2D screenposition of a position vectore
vec2 postProjToScreen(vec4 position) {
	vec2 screenPos = position.xy / position.w;
	return 0.5 * (vec2(screenPos.x, screenPos.y) + vec2(1.0));
}

vec3 ads() {
    vec3 s = normalize(vec3(LightPosition) - Position);
    vec3 v = normalize(vec3(-Position));
    vec3 r = reflect(-s, Normal);

    return LightColor * (Ka +
            Kd * max(dot(s, Normal), 0.0) +
            Ks * pow(max(dot(r, v), 0.0), Shininess));
}

void main() {
	vec3 col = ads();
	vec2 texCoord = postProjToScreen(HomogeneusPosition);
	float ao = texture(SSAOTexture, texCoord).x;
	col = clamp(col - ao, 0.0, 1.0);

	// Apply gamma correction
	col =  pow(col, vec3(1.0 / 2.2));
    FragColor = vec4(col, 1.0);
}
