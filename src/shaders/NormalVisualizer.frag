/*
 * NormalVizualizer.frag
 *
 *  Created on: 26-01-2013
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$

//-- Fragment
#version 330

uniform vec3 Color = vec3(1.0, 0.0, 0.0);

out vec4 FragColor;

void main() {
    FragColor = vec4(Color, 1.0);
}
