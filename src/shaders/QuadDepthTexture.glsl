#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
	TexCoord = VertexTexCoord;
	gl_Position = vec4(VertexPosition, 1.0);
}

//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec2 TexCoord;

uniform sampler2D Tex;

layout(location = 0) out vec4 FragColor;

void main() {
	const float minVal = 0.88;

	vec4 p = texture(Tex, TexCoord);
	float val = (p.x - minVal) / (1.0 - minVal);
	// float val = 1.0 - (1.0 - p.x) * 25.0;

	FragColor = vec4(vec3(val), 1.0);
}
