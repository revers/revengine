/*
 * BilinearSurface.glsl
 *
 *  Created on: 18-12-2012
 *      Author: Revers
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330
in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 TexCoord;
out vec3 Position;
out vec3 Normal;
uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
// uniform mat4 ProjectionMatrix;
uniform mat4 MVP;
uniform vec3 C00;
uniform vec3 C01;
uniform vec3 C10;
uniform vec3 C11;

vec3 eval(float u, float w) {
	// U * (C * W^T)
	return u * (w * C00 + C01) + (w * C10 + C11);
}

vec3 normal(float u, float w) {
	vec3 du = C00 * w + C01;
	vec3 dw = C00 * u + C10;
	return normalize(cross(dw, du));
}

void main() {
	vec3 n = normal(TexCoord.x, TexCoord.y);
	vec3 v = eval(TexCoord.x, TexCoord.y) * 10;
	Normal = normalize(NormalMatrix * n);
	Position = vec3(ModelViewMatrix * vec4(v, 1.0));

	gl_Position = MVP * vec4(v, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 330
in vec3 Position;
in vec3 Normal;
uniform vec4 LightPosition;
uniform vec3 LightColor;
uniform vec3 Kd; // Diffuse reflectivity
uniform vec3 Ka; // Ambient reflectivity
uniform vec3 Ks; // Specular reflectivity
uniform float Shininess; // Specular shininess factor

out vec4 FragColor;

vec3 ads() {
	vec3 s = normalize(vec3(LightPosition) - Position);
	vec3 v = normalize(vec3(-Position));
	vec3 r = reflect(-s, Normal);

	return LightColor
			* (Ka + Kd * max(dot(s, Normal), 0.0) + Ks * pow(max(dot(r, v), 0.0), Shininess));
}

void main() {
	FragColor = vec4(ads(), 1.0);
}

