#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 330

in vec3 VertexPosition;

uniform mat4 MVP;

out vec4 Position;

void main() {
	gl_Position = MVP * vec4(VertexPosition, 1.0);
	Position = gl_Position;
}
//---------------------------------------------------------------------
//-- Fragment
#version 330

in vec3 Normal;

uniform sampler2D LightTexture;
uniform vec3 AmbientColor;
uniform vec3 DiffuseColor;
uniform vec2 ViewportSize;

in vec4 Position;
out vec4 FragColor;

// Calculate the 2D screenposition of a position vectore
vec2 postProjToScreen(vec4 position) {
	vec2 screenPos = position.xy / position.w;
	return 0.5 * (vec2(screenPos.x, screenPos.y) + vec2(1.0));
}

// Calculate the size of one half of a pixel, to convert
// between texels and pixels
vec2 halfPixel() {
	return vec2(0.5) / ViewportSize;
	// return 0.5 / vec2(viewportWidth, viewportHeight);
}

void main() {
	// Extract lighting value from light map
	vec2 texCoord = postProjToScreen(Position) + halfPixel();
	vec4 light = texture(LightTexture, texCoord);

	light.xyz += AmbientColor;

	FragColor = vec4(DiffuseColor * light.xyz, 1.0);
}
